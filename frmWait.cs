﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWait : Form
    {
        public Exception error = null;
        public object result = null;

        public frmWait(string message, DoWorkEventHandler do_work, bool has_progress, bool can_cancel, object work_param)
        {
            InitializeComponent();

            lblMessage.Text = message;
            pbProgress.Visible = has_progress;
            btnCancel.Visible = can_cancel;
            if (!has_progress) Height -= pbProgress.Height;
            if (!can_cancel) Height -= btnCancel.Height;

            bwThread.WorkerReportsProgress = has_progress;
            bwThread.WorkerSupportsCancellation = can_cancel;
            bwThread.DoWork += do_work;
            bwThread.RunWorkerAsync(work_param);            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bwThread.CancelAsync();
            btnCancel.Enabled = false;
        }

        private void bwThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            error = e.Error;
            if (error == null) result = e.Result;
            Close();
        }

        private void bwThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null) lblMessage.Text = e.UserState.ToString();
            pbProgress.Value = e.ProgressPercentage;
        }

        private void Test(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker thread = sender as BackgroundWorker;
            System.Threading.Thread.Sleep(100);
            if (thread.CancellationPending)
            {
                thread.ReportProgress(25, "canceling 1...");
                System.Threading.Thread.Sleep(100);
                return;
            }
            thread.ReportProgress(25, "message 1");
            System.Threading.Thread.Sleep(100);
            if (thread.CancellationPending)
            {
                thread.ReportProgress(65, "canceling 2...");
                System.Threading.Thread.Sleep(100);
                return;
            }
            thread.ReportProgress(65, "message 2");
            System.Threading.Thread.Sleep(100);
        }
    }
}
