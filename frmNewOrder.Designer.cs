﻿namespace InteGr8
{
    partial class frmNewOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewOrder));
            this.label1 = new System.Windows.Forms.Label();
            this.tbShipmentNumber = new System.Windows.Forms.TextBox();
            this.btnNewOrderOK = new System.Windows.Forms.Button();
            this.btnNewOrderCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please enter the shipment ID";
            // 
            // tbShipmentNumber
            // 
            this.tbShipmentNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbShipmentNumber.Location = new System.Drawing.Point(59, 70);
            this.tbShipmentNumber.Name = "tbShipmentNumber";
            this.tbShipmentNumber.Size = new System.Drawing.Size(188, 20);
            this.tbShipmentNumber.TabIndex = 1;
            this.tbShipmentNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbShipmentNumber_KeyPress);
            // 
            // btnNewOrderOK
            // 
            this.btnNewOrderOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnNewOrderOK.Location = new System.Drawing.Point(120, 129);
            this.btnNewOrderOK.Name = "btnNewOrderOK";
            this.btnNewOrderOK.Size = new System.Drawing.Size(75, 23);
            this.btnNewOrderOK.TabIndex = 2;
            this.btnNewOrderOK.Text = "OK";
            this.btnNewOrderOK.UseVisualStyleBackColor = true;
            this.btnNewOrderOK.Click += new System.EventHandler(this.btnNewOrderOK_Click);
            // 
            // btnNewOrderCancel
            // 
            this.btnNewOrderCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNewOrderCancel.Location = new System.Drawing.Point(201, 129);
            this.btnNewOrderCancel.Name = "btnNewOrderCancel";
            this.btnNewOrderCancel.Size = new System.Drawing.Size(75, 23);
            this.btnNewOrderCancel.TabIndex = 3;
            this.btnNewOrderCancel.Text = "Cancel";
            this.btnNewOrderCancel.UseVisualStyleBackColor = true;
            this.btnNewOrderCancel.Click += new System.EventHandler(this.btnNewOrderCancel_Click);
            // 
            // frmNewOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 164);
            this.Controls.Add(this.btnNewOrderCancel);
            this.Controls.Add(this.btnNewOrderOK);
            this.Controls.Add(this.tbShipmentNumber);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmNewOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New shipment";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbShipmentNumber;
        private System.Windows.Forms.Button btnNewOrderOK;
        private System.Windows.Forms.Button btnNewOrderCancel;
    }
}