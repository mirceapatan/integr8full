﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmWizard_Options : Form
    {
        public frmWizard_Options()
        {
            InitializeComponent();

            rbInternationalYes.Checked = Template.International;
            rbMPSYes.Checked = Template.MPS;
            rbMPSPackageLevel.Checked = Template.MPS && Template.MPSPackageLevel;
            rbShipmentsYes.Checked = Template.SaveShipments;

            rbInternationalNo.Checked = !rbInternationalYes.Checked;
            rbMPSNo.Checked = !rbMPSYes.Checked;
            rbMPSShipmentLevel.Checked = !rbMPSPackageLevel.Checked;
            rbShipmentsNo.Checked = !rbShipmentsYes.Checked;

            if (rbMPSYes.Checked) rbMPSYes_CheckedChanged(this, null);
            else rbMPSNo_CheckedChanged(this, null);
        }

        private void rbMPSYes_CheckedChanged(object sender, EventArgs e)
        {
            rbMPSPackageLevel.Enabled = true;
            rbMPSShipmentLevel.Enabled = true;
        }

        private void rbMPSNo_CheckedChanged(object sender, EventArgs e)
        {
            rbMPSPackageLevel.Enabled = false;
            rbMPSShipmentLevel.Enabled = false;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Template.International = rbInternationalYes.Checked;
            Template.MPS = rbMPSYes.Checked;
            Template.MPSPackageLevel = rbMPSYes.Checked && rbMPSPackageLevel.Checked;
            Template.SaveShipments = rbShipmentsYes.Checked;

            DialogResult = DialogResult.OK;
        }

        private void frmWizard_Options_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_Options");
            e.Cancel = true;
        }
    }
}
