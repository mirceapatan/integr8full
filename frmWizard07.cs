﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard07 : Form
    {
        private bool custom_binding = false;
        private FieldLink link = null;

        public frmWizard07()
        {
            InitializeComponent();
        }

        private void frmWizard07_Load(object sender, EventArgs e)
        {
            // populate the Products table combobox with the ODBC catalog tables
            cbProductsTable.Items.Add("(none)");
            using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
            {
                Conn.Open();
                foreach (DataRow row in Conn.GetSchema("Tables").Rows)
                    cbProductsTable.Items.Add(row["TABLE_NAME"]);
            }


            // check that the link is OrdersOneToMany type and each field have the same link
            foreach (FieldBinding binding in InteGr8_Main.template.Bindings)
                if (binding.Field.FieldCategory == Data.FieldsTableDataTable.FieldCategory.OrderProducts && binding.Link != null)
                    if (link == null) link = binding.Link;
                    else if (!binding.Link.Equals(link)) custom_binding = true;
            if (link != null && link.Type != FieldLink.LinkType.Orders && link.Type != FieldLink.LinkType.OrdersOneToMany) custom_binding = true;
            if (custom_binding)
            {
                foreach (Control control in this.Controls)
                    if (control is ComboBox) control.Enabled = false;
                MessageBox.Show(this, "Order Products configuration was customized and cannot be edited in this page. You will be able to edit in the summary page. Please click Next to skip this page.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (link == null) cbProductsTable.SelectedIndex = 0;
            else
            {
                int index = cbProductsTable.FindStringExact(link.FieldTable);
                if (index == -1)
                {
                    cbProductsTable.SelectedIndex = 0;
                    MessageBox.Show(this, "Order Products table '" + link.FieldTable + "' not found in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else cbProductsTable.SelectedIndex = index;
                if (link.Type != FieldLink.LinkType.Orders && link.OrdersTableFieldFK != null)
                {
                    cbProductsForeignKey.SelectedIndex = cbProductsForeignKey.FindStringExact(link.OrdersTableFieldFK);
                    if (cbProductsForeignKey.SelectedIndex == -1)
                    {
                        MessageBox.Show(this, "Order Products table Order # foreign key column '" + link.OrdersTableFieldFK + "' not found in the Order Products table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
            if (InteGr8_Main.template.Find("Product Description") != null) cbDescription.Text = InteGr8_Main.template.Find("Product Description").Value;
            if (InteGr8_Main.template.Find("Product Is Document") != null) cbIsDocument.Text = InteGr8_Main.template.Find("Product Is Document").Value;
            if (InteGr8_Main.template.Find("Product Manufacture Country") != null) cbManufactureCountry.Text = InteGr8_Main.template.Find("Product Manufacture Country").Value;
            if (InteGr8_Main.template.Find("Product Quantity") != null) cbQuantity.Text = InteGr8_Main.template.Find("Product Quantity").Value;
            if (InteGr8_Main.template.Find("Product Quantity MU") != null) cbQuantityMU.Text = InteGr8_Main.template.Find("Product Quantity MU").Value;
            if (InteGr8_Main.template.Find("Product Unit Weight") != null) cbUnitWeight.Text = InteGr8_Main.template.Find("Product Weight").Value;
            //if (InteGr8_Main.template.Find("Product Unit Weight Type") != null) cbWeightType.Text = InteGr8_Main.template.Find("Product Weight Type").Value;
            if (InteGr8_Main.template.Find("Product Unit Value") != null) cbUnitValue.Text = InteGr8_Main.template.Find("Product Unit Value").Value;
            //if (InteGr8_Main.template.Find("Product Unit Value Currency") != null) cbValueCurrency.Text = InteGr8_Main.template.Find("Product Unit Value Currency").Value;
            if (InteGr8_Main.template.Find("Product Harmonized Code") != null) cbHarmonizedCode.Text = InteGr8_Main.template.Find("Product Harmonized Code").Value;
        }

        private void cbProductsTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // clear all column comboboxes
            cbProductsForeignKey.Items.Clear();
            cbDescription.Items.Clear();
            cbIsDocument.Items.Clear();
            cbManufactureCountry.Items.Clear();
            cbQuantity.Items.Clear();
            cbQuantityMU.Items.Clear();
            cbUnitWeight.Items.Clear();
            //cbWeightType.Items.Clear();
            cbUnitValue.Items.Clear();
            //cbValueCurrency.Items.Clear();
            cbHarmonizedCode.Items.Clear();

            cbProductsForeignKey.SelectedIndex = -1;

            // if link is needed or not (Orders left join Packages)
            bool link = cbProductsTable.SelectedIndex > 0 && !cbProductsTable.Text.Equals(InteGr8_Main.template.Orders);
            cbProductsForeignKey.Enabled = link;

            // read the Packages table columns and populate all the packages comboboxes
            if (cbProductsTable.SelectedIndex > 0)
            {
                string[] columns = null;
                using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
                {
                    Conn.Open();
                    using (OdbcDataReader dr = new OdbcCommand("Select Top 0 * From " + cbProductsTable.Text, Conn).ExecuteReader(CommandBehavior.SingleRow))
                    {
                        columns = new string[dr.FieldCount];
                        for (int k = 0; k < dr.FieldCount; k++)
                            columns[k] = "[" + dr.GetName(k) + "]";
                    }
                }

                if (columns != null)
                {
                    cbProductsForeignKey.Items.AddRange(columns);
                    cbDescription.Items.AddRange(columns);
                    cbIsDocument.Items.AddRange(columns);
                    cbIsDocument.Items.AddRange(new string[] { "-- 0 --", "-- 1 --" });
                    cbManufactureCountry.Items.AddRange(columns);
                    cbQuantity.Items.AddRange(columns);
                    cbQuantity.Items.AddRange(new string[] { "-- 1.00 --" });
                    cbQuantityMU.Items.AddRange(columns);
                    cbQuantityMU.Items.AddRange(new string[] { "-- PCS --" });
                    cbUnitWeight.Items.AddRange(columns);
                    //cbWeightType.Items.AddRange(columns);
                    //cbWeightType.Items.AddRange(new string[] { "-- LBS --", "-- KGS --" });
                    cbUnitValue.Items.AddRange(columns);
                    cbUnitValue.Items.AddRange(new string[] { "-- 1.00 --" });
                    //cbValueCurrency.Items.AddRange(columns);
                    //cbValueCurrency.Items.AddRange(new string[] { "-- USD --", "-- CAD --" });
                    cbHarmonizedCode.Items.AddRange(columns);
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (cbProductsForeignKey.Enabled && cbProductsForeignKey.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please choose the Order # foreign key column from the Products table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // validate
            // TODO: check the Product # PK in the Products table
            // TODO: check the Order # FK
            // TODO: check the Product # FK in the Link table (if defined)
            // TODO: check the uniqueness of the two FKs in the Link table (if defined)

            if (!custom_binding)
            {
                if (cbProductsTable.SelectedIndex == 0) link = null;
                else if (cbProductsTable.Text.Equals(InteGr8_Main.template.Orders))
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.Orders;
                    link.FieldTable = cbProductsTable.Text;
                }
                else
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.OrdersOneToMany;
                    link.FieldTable = cbProductsTable.Text;
                    link.OrdersTableFieldFK = cbProductsForeignKey.Text;
                }

                InteGr8_Main.template.UpdateBinding("Product Description", cbDescription.FindStringExact(cbDescription.Text) == -1, cbDescription.Text.Trim().Equals("") ? null : cbDescription.Text, link);
                InteGr8_Main.template.UpdateBinding("Product Is Document", cbIsDocument.FindStringExact(cbIsDocument.Text) == -1, cbIsDocument.Text.Trim().Equals("") ? null : cbIsDocument.Text, link);
                InteGr8_Main.template.UpdateBinding("Product Country Manufacture", cbManufactureCountry.FindStringExact(cbManufactureCountry.Text) == -1, cbManufactureCountry.Text.Trim().Equals("") ? null : cbManufactureCountry.Text, link);
                InteGr8_Main.template.UpdateBinding("Product Quantity", cbQuantity.FindStringExact(cbQuantity.Text) == -1, cbQuantity.Text.Trim().Equals("") ? null : cbQuantity.Text, link);
                InteGr8_Main.template.UpdateBinding("Product Quantity MU", cbQuantityMU.FindStringExact(cbQuantityMU.Text) == -1, cbQuantityMU.Text.Trim().Equals("") ? null : cbQuantityMU.Text, link);
                InteGr8_Main.template.UpdateBinding("Product Unit Weight", cbUnitWeight.FindStringExact(cbUnitWeight.Text) == -1, cbUnitWeight.Text.Trim().Equals("") ? null : cbUnitWeight.Text, link);
                //InteGr8_Main.template.UpdateBinding("Product Unit Weight Type", cbWeightType.FindStringExact(cbWeightType.Text) == -1, cbWeightType.Text.Trim().Equals("") ? null : cbWeightType.Text, link);
                InteGr8_Main.template.UpdateBinding("Product Unit Value", cbUnitValue.FindStringExact(cbUnitValue.Text) == -1, cbUnitValue.Text.Trim().Equals("") ? null : cbUnitValue.Text, link);
                //InteGr8_Main.template.UpdateBinding("Product Unit Value Currency", cbValueCurrency.FindStringExact(cbValueCurrency.Text) == -1, cbValueCurrency.Text.Trim().Equals("") ? null : cbValueCurrency.Text, link);
                InteGr8_Main.template.UpdateBinding("Product Harmonized Code", cbHarmonizedCode.FindStringExact(cbHarmonizedCode.Text) == -1, cbHarmonizedCode.Text.Trim().Equals("") ? null : cbHarmonizedCode.Text, link);
            }
            DialogResult = DialogResult.OK;
        }

        private void frmWizard07_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK) return;
            if (DialogResult == DialogResult.No)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            DialogResult = DialogResult.Abort;
        }

        private void frmWizard07_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_07");
            e.Cancel = true;
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (cbProductsTable.SelectedIndex <= 0 || cbProductsTable.Text.Trim().Equals("")) return;
            new frmWizard_DataPreview("Products", InteGr8_Main.template.Connection, "Select Top 100 * From " + cbProductsTable.Text).ShowDialog(this);
        }
    }
}
