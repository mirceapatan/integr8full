﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace InteGr8
{
    public class MyDateTimePicker : System.Windows.Forms.DateTimePicker
    {
        private  System.Drawing.Color _disabled_back_color;
        private Image _image;
        private  System.Drawing.Color _text_color = Color.Black;

        public MyDateTimePicker(): base()//System.Windows.Forms.DateTimePicker()
        {
            this.SetStyle(System.Windows.Forms.ControlStyles.UserPaint, true);
            _disabled_back_color = Color.FromKnownColor(KnownColor.Control);
        }
        //Gets or sets the background color of the control
        public override Color BackColor
        { get => base.BackColor; set => base.BackColor = value; }
        // Gets or sets the background color of the control when disabled
        public Color BackDisabledColor
        {
            get { return _disabled_back_color; }
            set { _disabled_back_color = value; }
        }
        // Gets or sets the Image next to the dropdownbutton
        public Image Image
        {
            get { return _image; }
            set { _image = value; Invalidate(); }
        }
        // Gets or sets the text color when calendar is not visible
        public Color TextColor
        {
            get { return _text_color; }
            set { _text_color = value; }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = this.CreateGraphics();
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            Rectangle ddb_rect = new Rectangle(ClientRectangle.Width - 17, 0, 17, ClientRectangle.Height);
            Brush bb;
            ComboBoxState visual_state;
            if (this.Enabled)
            {
                bb = new SolidBrush(this.BackColor);
                visual_state = ComboBoxState.Normal;
            }
            else
            {
                bb = new SolidBrush(_disabled_back_color);
                visual_state = ComboBoxState.Disabled;
            }
            g.FillRectangle(bb, 0, 0, ClientRectangle.Width, ClientRectangle.Height);
            g.DrawString(this.Text, this.Font, new SolidBrush(TextColor), 5, 2);
            if (!(_image is null))
            {
                Rectangle im_rect = new Rectangle(ClientRectangle.Width - 40, 4, ClientRectangle.Height - 8, ClientRectangle.Height - 8);
                g.DrawImage(_image, im_rect);
            }
            ComboBoxRenderer.DrawDropDownButton(g, ddb_rect, visual_state);
            g.Dispose();
            bb.Dispose();
        }
    }
   
}
