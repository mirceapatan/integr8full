namespace InteGr8
{
    partial class frmWizardSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizardSummary));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lblHeaderLine = new System.Windows.Forms.Label();
            this.llAdvanced = new System.Windows.Forms.LinkLabel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.lblHeaderIcon = new System.Windows.Forms.Label();
            this.lblHeaderInfo = new System.Windows.Forms.Label();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.lblSendersTable = new System.Windows.Forms.Label();
            this.cbOrdersTable = new System.Windows.Forms.ComboBox();
            this.lblSendersPrimaryKey = new System.Windows.Forms.Label();
            this.cbOrderNumberColumn = new System.Windows.Forms.ComboBox();
            this.dgvBindings = new System.Windows.Forms.DataGridView();
            this.FieldColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TableColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LinkColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ColumnColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DefaultColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBindings)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.lblHeaderLine);
            this.pnlHeader.Controls.Add(this.llAdvanced);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.lblHeaderIcon);
            this.pnlHeader.Controls.Add(this.lblHeaderInfo);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(875, 88);
            this.pnlHeader.TabIndex = 115;
            // 
            // lblHeaderLine
            // 
            this.lblHeaderLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderLine.BackColor = System.Drawing.SystemColors.Control;
            this.lblHeaderLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeaderLine.Location = new System.Drawing.Point(0, 86);
            this.lblHeaderLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderLine.Name = "lblHeaderLine";
            this.lblHeaderLine.Size = new System.Drawing.Size(875, 2);
            this.lblHeaderLine.TabIndex = 116;
            // 
            // llAdvanced
            // 
            this.llAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdvanced.Image = global::InteGr8.Properties.Resources.wrench;
            this.llAdvanced.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llAdvanced.LinkArea = new System.Windows.Forms.LinkArea(6, 13);
            this.llAdvanced.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdvanced.Location = new System.Drawing.Point(771, 0);
            this.llAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.llAdvanced.Name = "llAdvanced";
            this.llAdvanced.Size = new System.Drawing.Size(104, 24);
            this.llAdvanced.TabIndex = 119;
            this.llAdvanced.TabStop = true;
            this.llAdvanced.Text = "      advanced mode";
            this.llAdvanced.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llAdvanced.UseCompatibleTextRendering = true;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(771, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Summary Review";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeaderIcon
            // 
            this.lblHeaderIcon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeaderIcon.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaderIcon.Image = global::InteGr8.Properties.Resources.data_next;
            this.lblHeaderIcon.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeaderIcon.Location = new System.Drawing.Point(0, 24);
            this.lblHeaderIcon.Name = "lblHeaderIcon";
            this.lblHeaderIcon.Padding = new System.Windows.Forms.Padding(5);
            this.lblHeaderIcon.Size = new System.Drawing.Size(64, 62);
            this.lblHeaderIcon.TabIndex = 115;
            // 
            // lblHeaderInfo
            // 
            this.lblHeaderInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderInfo.AutoEllipsis = true;
            this.lblHeaderInfo.Location = new System.Drawing.Point(68, 24);
            this.lblHeaderInfo.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderInfo.Name = "lblHeaderInfo";
            this.lblHeaderInfo.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.lblHeaderInfo.Size = new System.Drawing.Size(807, 62);
            this.lblHeaderInfo.TabIndex = 114;
            this.lblHeaderInfo.Text = "In this page you can review and change any of the fields and their mappings.";
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnPrev.Location = new System.Drawing.Point(707, 482);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 4;
            this.btnPrev.Text = "< Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Enabled = false;
            this.btnNext.Location = new System.Drawing.Point(788, 482);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(12, 482);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(125, 23);
            this.btnPreview.TabIndex = 3;
            this.btnPreview.Text = "Preview Field Table";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // lblSendersTable
            // 
            this.lblSendersTable.AutoSize = true;
            this.lblSendersTable.Location = new System.Drawing.Point(12, 106);
            this.lblSendersTable.Name = "lblSendersTable";
            this.lblSendersTable.Size = new System.Drawing.Size(67, 13);
            this.lblSendersTable.TabIndex = 120;
            this.lblSendersTable.Text = "Orders table:";
            // 
            // cbOrdersTable
            // 
            this.cbOrdersTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrdersTable.FormattingEnabled = true;
            this.cbOrdersTable.Location = new System.Drawing.Point(82, 103);
            this.cbOrdersTable.Name = "cbOrdersTable";
            this.cbOrdersTable.Size = new System.Drawing.Size(174, 21);
            this.cbOrdersTable.TabIndex = 117;
            // 
            // lblSendersPrimaryKey
            // 
            this.lblSendersPrimaryKey.AutoSize = true;
            this.lblSendersPrimaryKey.Location = new System.Drawing.Point(277, 106);
            this.lblSendersPrimaryKey.Name = "lblSendersPrimaryKey";
            this.lblSendersPrimaryKey.Size = new System.Drawing.Size(205, 13);
            this.lblSendersPrimaryKey.TabIndex = 121;
            this.lblSendersPrimaryKey.Text = "Orders table primary key column (Order #):";
            // 
            // cbOrderNumberColumn
            // 
            this.cbOrderNumberColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrderNumberColumn.FormattingEnabled = true;
            this.cbOrderNumberColumn.Location = new System.Drawing.Point(488, 103);
            this.cbOrderNumberColumn.Name = "cbOrderNumberColumn";
            this.cbOrderNumberColumn.Size = new System.Drawing.Size(174, 21);
            this.cbOrderNumberColumn.TabIndex = 118;
            // 
            // dgvBindings
            // 
            this.dgvBindings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBindings.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvBindings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBindings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FieldColumn,
            this.TableColumn,
            this.LinkColumn,
            this.ColumnColumn,
            this.DefaultColumn});
            this.dgvBindings.Enabled = false;
            this.dgvBindings.Location = new System.Drawing.Point(15, 136);
            this.dgvBindings.MultiSelect = false;
            this.dgvBindings.Name = "dgvBindings";
            this.dgvBindings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBindings.Size = new System.Drawing.Size(848, 337);
            this.dgvBindings.TabIndex = 119;
            // 
            // FieldColumn
            // 
            this.FieldColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FieldColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.FieldColumn.DisplayStyleForCurrentCellOnly = true;
            this.FieldColumn.FillWeight = 20F;
            this.FieldColumn.HeaderText = "2Ship Field";
            this.FieldColumn.Name = "FieldColumn";
            // 
            // TableColumn
            // 
            this.TableColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TableColumn.DisplayStyleForCurrentCellOnly = true;
            this.TableColumn.FillWeight = 20F;
            this.TableColumn.HeaderText = "Field Table";
            this.TableColumn.Name = "TableColumn";
            // 
            // LinkColumn
            // 
            this.LinkColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LinkColumn.FillWeight = 20F;
            this.LinkColumn.HeaderText = "Orders Link";
            this.LinkColumn.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.LinkColumn.Name = "LinkColumn";
            this.LinkColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LinkColumn.Text = "";
            this.LinkColumn.TrackVisitedState = false;
            // 
            // ColumnColumn
            // 
            this.ColumnColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnColumn.DisplayStyleForCurrentCellOnly = true;
            this.ColumnColumn.FillWeight = 20F;
            this.ColumnColumn.HeaderText = "Field Column";
            this.ColumnColumn.Name = "ColumnColumn";
            this.ColumnColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DefaultColumn
            // 
            this.DefaultColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DefaultColumn.FillWeight = 20F;
            this.DefaultColumn.HeaderText = "Default Value";
            this.DefaultColumn.MinimumWidth = 50;
            this.DefaultColumn.Name = "DefaultColumn";
            // 
            // frmWizardSummary
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(875, 517);
            this.Controls.Add(this.lblSendersTable);
            this.Controls.Add(this.cbOrdersTable);
            this.Controls.Add(this.lblSendersPrimaryKey);
            this.Controls.Add(this.cbOrderNumberColumn);
            this.Controls.Add(this.dgvBindings);
            this.Controls.Add("]���e�Z�k<�I�1�/�L�t+�%�u�:�t`���C��Y�����},s~t��1�>/-v�q��כ܅n��!ەh�(�/��������'��^}��hq]�t}�����`�~*m4�Q��D��54\�":4����n���I��8S'�־a�Rϻ9�^X�P�<��[%��G�ND׈k86�JW�.~i���������ѹ'ڱ���Dq�i�Ue�	.d吲Si�}O�շ��o�Hp1>�����k�K&r.�2{%Y��n���~�:�q��?�T	K��%{r��D]��m���TT��ԧz-��v)ii�p�s����4�5C5�
;��OD}�Dw2��p4�d:�_|^4xΨMӋ246{m�4q���r��W��߽�uoז[#�.v��}��T��!��W/�;����䊦�����)�z����߻P�aT6�W@����:�Â��wQۭW%y �4�}��Q���[k���o��:@1�|yA�gAF�l��dd@2U���I�>ۏ������}A����<s�*I]�y��4����������Z�۴�ͷ#�E?p���s���<���N��y"	�X�h�)!��ۮC����I;�e>w�Xw��*R{�|��%*������uI���6����3Ȱ��O}����%�wɍ�����	T?z��'�^��!�����������*��t��k�O�j����٬��?-��b���5����j�g�fY�?��x���@7��'j|Y��i�pOUň��ie��n᪽V�=��E��/�4.	]��˦F�ث6����u�)������xCtm����jKR[��� ���Xi\�9~JM�ǲ���B��M~:Ԍ�s{���?]~/D�$"d�����\o]-��e)�����!K\K��
O:�-D�T(����q�[�[��3��+x��m<;;L<B��dD�#����.������y{"[��߳V�^�a_r`�U���Jy|���E[���jl�<���]\=TX��K��_�Okj�r�?���{t�FGpQ�Lv��4Tw;5�f3�b�}Dӻ��|%/5�*Z�dt@(��r�	�qS=O�+6����Ӭ��T5���'�x����m�8>"��ձ�3�ak���>k�5�k$��׮-�lZH��^ӂM!-����w^��0���k��!�=ɶ@��1ےS����.�<?���#��'&�Ew.�o�F;Z'�F7��܏ua��k"k�_�E'�������\"���8����$�j[�Mݥ�ows�s��Cy�@���g^�Zf~̃���3��T-:p#ˬӿ�p�Oc�[���a�z�i²k�5��C����^t���\1�I�kXs:(�ٷ.�vF�jG$$gejB�����.[U��H����|���d��z�����"<!KO�N������bk�Jl�2�-���?~4y���dg�
<�ց眙lB�\�-H�t��,��K;pVu�ӍM"^NX��3BAK ܢ����ˈ�m{������D�i�N��g�w#XUw�%���J�Iw��� Zu�#�@����U�/������2��h	���Ec���m.��U�؁����p���׻��V�lʅ��˒+Cbyz����N%ځ�(�g<s~�4Abq���YG�ۃx���߿��/$�^�ڤ&h�wD���,ߐO�U�����j������unFc@ \MW��[������;���±���L(�9�^�#z`���D�T�3���ꐄ����p�����r�cn7,s���n�Y�������IW�l�N�??�m=�����Wg�.�7�s�ګ�kN>F�*���V?��?Yy��ɖқA c�:��?������V�o��w���H����\�R���h�o4��������&o�#W}��73�]�}R�0s~ս�vց�?�&͛�条��⤼1���;r�﯎����_�����A���Q)���:��(�F�����6@��edO�o�zS��]�Y� t*��^�-}nE�j����\��-\���/ؿ�e�H��MH'����#FLq-yռ�z|`fん"�����Z-������q�Z7f8�+�>��x�d��q�3�9�f��@(����l\���V�{��7��_�Z��X$�*/U	Z�8�[SӴ��@1 e�.�^���힔��p8%k�::�H�%YPJ�+�����P�F����
[N5x7ZE����w��O���_���U<giO� I-%S��F���]s�4	O�>E�SE��[����,#U���2�a,�����_aCͲ��l���n���E둰��t�S��^d@2��G�,�L�'쯎]l����Q��m�%�q���n�e�4�688��Kr�ɯ�j���f'