﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class UPSReturnServiceType : Form
    {
        //private readonly Data data;
        private readonly Data.OrdersTableRow order;
        public string returnService;
        public string returnEmail;
        public string returnFrom;
        public string returnSubject;
        public UPSReturnServiceType(string order)
        {
            
            InitializeComponent();
            if (InteGr8_Main.data == null || String.IsNullOrEmpty(order)) throw new ArgumentException();
            data = InteGr8_Main.data;
            this.order = data.OrdersTable.FindBy_Order__(order);
            if (this.order == null) throw new ArgumentException();
            cmbUPSReturnService.DataSource = data.UPSReturnServiceType;
        }

        private void lblEmailSubject_Click(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            returnService = cmbUPSReturnService.SelectedValue.ToString();
            if (returnService.Equals("8"))
            {
                returnFrom = tbFromName.Text;
                returnEmail = tbEmail.Text;
                returnSubject = tbEmailSubject.Text;
            }
            cmbUPSReturnService.DataSource = null;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void cmbUPSReturnService_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbUPSReturnService.DataSource != null)
            {
                if (cmbUPSReturnService.SelectedValue.Equals("8"))
                {
                    gbEmailDetails.Enabled = true;
                    tbFromName.Enabled = true;
                    tbEmail.Enabled = true;
                    tbEmailSubject.Enabled = true;
                }
                else
                {
                    gbEmailDetails.Enabled = false;
                    tbFromName.Enabled = false;
                    tbEmail.Enabled = false;
                    tbEmailSubject.Enabled = false;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            cmbUPSReturnService.DataSource = null;
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
