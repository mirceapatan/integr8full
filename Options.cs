﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;
using InteGr8.Properties;
using System.IO;

namespace InteGr8
{
    public partial class Options : Form
    {
        bool useautoprint = false;
        public Data data;
        string temp_user = "";
        Data.OrdersTableRow currentOrder = null;
        BindingSource PackageDimsBS = new BindingSource();
        Dictionary<string, string> chequeTypes = new Dictionary<string,string>{{"1","Cheque"},{"2","Post-Dated Cheque"},{"3", "Certified Cheque"}, {"4", "Money Order"}, {"5", "Bank Draft"}};
        Dictionary<string, string> upsConfirmationType = new Dictionary<string, string> { { "1", "Default" }, { "2", "Signature Required" }, { "3", "Adult Signature Required" } };
        Dictionary<string, string> CAPostMethodOfPayment = new Dictionary<string,string>{{"CSH", "Cash"}, {"CHQ", "Check"},{"MOCC", "Money Order / Certified Check"}};
        Dictionary<string, string> DangerousGoodsMode = new Dictionary<string,string> { {"0", "None"}, {"1", "Ground"}, {"2", "Air"} };
        Dictionary<string, string> DangerousGoodsClassification = new Dictionary<string, string> { { "0", "None" }, { "1", "FullyRegulated" }, { "2", "BiologicalSubstances_UN3373" }, { "3", "DryIce_UN1845"}, {"4", "Exempt500KG" }, { "5", "LimitedQuantity"} };
        Dictionary<string, string> ATSTemperature = new Dictionary<string, string> { {"0", "----------------------" }, {"HS", "Heated Service" }, { "KC", "Keep Cool" },{ "FR", "Cold Chain Keep Moving" }, { "TA", "Temperature Assure" }, { "TC", "Temperature Track" } };
        Dictionary<string, string> ATSTimeRelease = new Dictionary<string, string> { {"NA", "----------------------" }, { "By", "Deliver Before" }, { "NO", "Deliver After" }, { "DO", "Deliver On" }, {"BDR", "Deliver Between" } };

        Dictionary<string, string> caProvinces = new Dictionary<string, string> { {"AB", "AB - Alberta"},
{"BC", "BC - British Columbia"},
{"MB", "MB - Manitoba"},
{"NB", "NB - New Brunswick"},
{"NL", "NL - Newfoundland and Labrador"},
{"NT", "NT - Northwest Territories"},
{"NS", "NS - Nova Scotia"},
{"NU", "NU - Nunavut"},
{"ON", "ON - Ontario"},
{"PE", "PE - Prince Edward Island"},
{"QC", "QC - Quebec"},
{"SK", "SK - Saskatchewan"},
{"YT", "YT - Yukon"}
 };
        Dictionary<string, string> usStates = new Dictionary<string, string>() { {"AL", "AL - Alabama"},
{"AK", "AK - Alaska"},
{"AZ", "AZ - Arizona"},
{"AR", "AR - Arkansas"},
{"CA", "CA - California"},
{"CO", "CO - Colorado"},
{"CT", "CT - Connecticut"},
{"DE", "DE - Delaware"},
{"DC", "DC - Dist. of Columbia"},
{"FL", "FL - Florida"},
{"GA", "GA - Georgia"},
{"HI", "HI - Hawaii"},
{"ID", "ID - Idaho"},
{"IL", "IL - Illinois"},
{"IN", "IN - Indiana"},
{"IA", "IA - Iowa"},
{"KS", "KS - Kansas"},
{"KY", "KY - Kentucky"},
{"LA", "LA - Louisiana"},
{"ME", "ME - Maine"},
{"MD", "MD - Maryland"},
{"MA", "MA - Massachusetts"},
{"MI", "MI - Michigan"},
{"MN", "MN - Minnesota"},
{"MS", "MS - Mississippi"},
{"MO", "MO - Missouri"},
{"MT", "MT - Montana"},
{"NE", "NE - Nebraska"},
{"NV", "NV - Nevada"},
{"NH", "NH - New Hampshire"},
{"NJ", "NJ - New Jersey"},
{"NM", "NM - New Mexico"},
{"NY", "NY - New York"},
{"NC", "NC - North Carolina"},
{"ND", "ND - North Dakota"},
{"OH", "OH - Ohio"},
{"OK", "OK - Oklahoma"},
{"OR", "OR - Oregon"},
{"PA", "PA - Pennsylvania"},
{"RI", "RI - Rhode Island"},
{"SC", "SC - South Carolina"},
{"SD", "SD - South Dakota"},
{"TN", "TN - Tennessee"},
{"TX", "TX - Texas"},
{"UT", "UT - Utah"},
{"VT", "VT - Vermont"},
{"VA", "VA - Virginia"},
{"WA", "WA - Washington"},
{"WV", "WV - West Virginia"},
{"WI", "WI - Wisconsin"},
{"WY", "WY - Wyoming"}
 };
        Dictionary<string, string> countryCodes = new Dictionary<string, string>(){{"AF", "Afghanistan - AF"},
{"AL", "Albania - AL"},
{"DZ", "Algeria - DZ"},
{"AS", "American Samoa - AS"},
{"AD", "Andorra - AD"},
{"AO", "Angola - AO"},
{"AI", "Anguilla - AI"},
{"AQ", "Antarctica - AQ"},
{"AG", "Antigua and Barbuda - AG"},
{"AR", "Argentina - AR"},
{"AM", "Armenia - AM"},
{"AW", "Aruba - AW"},
{"AU", "Australia - AU"},
{"AT", "Austria - AT"},
{"AZ", "Azerbaijan - AZ"},
{"BS", "Bahamas - BS"},
{"BH", "Bahrain - BH"},
{"BD", "Bangladesh - BD"},
{"BB", "Barbados - BB"},
{"BY", "Belarus - BY"},
{"BE", "Belgium - BE"},
{"BZ", "Belize - BZ"},
{"BJ", "Benin - BJ"},
{"BM", "Bermuda - BM"},
{"BT", "Bhutan - BT"},
{"BO", "Bolivia -  Plurinational State of - BO"},
{"BQ", "Bonaire -  Sint Eustatius and Saba - BQ"},
{"BA", "Bosnia and Herzegovina - BA"},
{"BW", "Botswana - BW"},
{"BV", "Bouvet Island - BV"},
{"BR", "Brazil - BR"},
{"IO", "British Indian Ocean Territory - IO"},
{"BN", "Brunei Darussalam - BN"},
{"BG", "Bulgaria - BG"},
{"BF", "Burkina Faso - BF"},
{"BI", "Burundi - BI"},
{"KH", "Cambodia - KH"},
{"CM", "Cameroon - CM"},
{"CA", "Canada - CA"},
{"CV", "Cape Verde - CV"},
{"KY", "Cayman Islands - KY"},
{"CF", "Central African Republic - CF"},
{"TD", "Chad - TD"},
{"CL", "Chile - CL"},
{"CN", "China - CN"},
{"CX", "Christmas Island - CX"},
{"CC", "Cocos (Keeling) Islands - CC"},
{"CO", "Colombia - CO"},
{"KM", "Comoros - KM"},
{"CG", "Congo - CG"},
{"CD", "Congo -  the Democratic Republic - CD"},
{"CK", "Cook Islands - CK"},
{"CR", "Costa Rica - CR"},
{"CI", "Cote d'Ivoire - CI"},
{"HR", "Croatia - HR"},
{"CU", "Cuba - CU"},
{"CW", "Curacao - CW"},
{"CY", "Cyprus - CY"},
{"CZ", "Czech Republic - CZ"},
{"DK", "Denmark - DK"},
{"DJ", "Djibouti - DJ"},
{"DM", "Dominica - DM"},
{"DO", "Dominican Republic - DO"},
{"EC", "Ecuador - EC"},
{"EG", "Egypt - EG"},
{"SV", "El Salvador - SV"},
{"GQ", "Equatorial Guinea - GQ"},
{"ER", "Eritrea - ER"},
{"EE", "Estonia - EE"},
{"ET", "Ethiopia - ET"},
{"FK", "Falkland Islands (Malvinas) - FK"},
{"FO", "Faroe Islands - FO"},
{"FJ", "Fiji - FJ"},
{"FI", "Finland - FI"},
{"FR", "France - FR"},
{"GF", "French Guiana - GF"},
{"PF", "French Polynesia - PF"},
{"TF", "French Southern Territories - TF"},
{"GA", "Gabon - GA"},
{"GM", "Gambia - GM"},
{"GE", "Georgia - GE"},
{"DE", "Germany - DE"},
{"GH", "Ghana - GH"},
{"GI", "Gibraltar - GI"},
{"GR", "Greece - GR"},
{"GL", "Greenland - GL"},
{"GD", "Grenada - GD"},
{"GP", "Guadeloupe - GP"},
{"GU", "Guam - GU"},
{"GT", "Guatemala - GT"},
{"GG", "Guernsey - GG"},
{"GN", "Guinea - GN"},
{"GW", "Guinea-Bissau - GW"},
{"GY", "Guyana - GY"},
{"HT", "Haiti - HT"},
{"HM", "Heard Island and McDonald Islands - HM"},
{"HN", "Honduras - HN"},
{"HK", "Hong Kong - HK"},
{"HU", "Hungary - HU"},
{"IS", "Iceland - IS"},
{"IN", "India - IN"},
{"ID", "Indonesia - ID"},
{"IR", "Iran - IR"},
{"IQ", "Iraq - IQ"},
{"IE", "Ireland - IE"},
{"IM", "Isle of Man - IM"},
{"IL", "Israel - IL"},
{"IT", "Italy - IT"},
{"JM", "Jamaica - JM"},
{"JP", "Japan - JP"},
{"JE", "Jersey - JE"},
{"JO", "Jordan - JO"},
{"KZ", "Kazakhstan - KZ"},
{"KE", "Kenya - KE"},
{"KI", "Kiribati - KI"},
{"KW", "Kuwait - KW"},
{"KG", "Kyrgyzstan - KG"},
{"LA", "Laos - LA"},
{"LV", "Latvia - LV"},
{"LB", "Lebanon - LB"},
{"LS", "Lesotho - LS"},
{"LR", "Liberia - LR"},
{"LY", "Libya - LY"},
{"LI", "Liechtenstein - LI"},
{"LT", "Lithuania - LT"},
{"LU", "Luxembourg - LU"},
{"MO", "Macao - MO"},
{"MK", "Macedonia - MK"},
{"MG", "Madagascar - MG"},
{"MW", "Malawi - MW"},
{"MY", "Malaysia - MY"},
{"MV", "Maldives - MV"},
{"ML", "Mali - ML"},
{"MT", "Malta - MT"},
{"MH", "Marshall Islands - MH"},
{"MQ", "Martinique - MQ"},
{"MR", "Mauritania - MR"},
{"MU", "Mauritius - MU"},
{"YT", "Mayotte - YT"},
{"MX", "Mexico - MX"},
{"FM", "Micronesia - FM"},
{"MD", "Moldova - MD"},
{"MC", "Monaco - MC"},
{"MN", "Mongolia - MN"},
{"ME", "Montenegro - ME"},
{"MS", "Montserrat - MS"},
{"MA", "Morocco - MA"},
{"MZ", "Mozambique - MZ"},
{"MM", "Myanmar - MM"},
{"NA", "Namibia - NA"},
{"NR", "Nauru - NR"},
{"NP", "Nepal - NP"},
{"NL", "Netherlands - NL"},
{"NC", "New Caledonia - NC"},
{"NZ", "New Zealand - NZ"},
{"NI", "Nicaragua - NI"},
{"NE", "Niger - NE"},
{"NG", "Nigeria - NG"},
{"NU", "Niue - NU"},
{"NF", "Norfolk Island - NF"},
{"MP", "Northern Mariana Islands - MP"},
{"NO", "Norway - NO"},
{"OM", "Oman - OM"},
{"PK", "Pakistan - PK"},
{"PW", "Palau - PW"},
{"PS", "Palestine Authority - PS"},
{"PA", "Panama - PA"},
{"PG", "Papua New Guinea - PG"},
{"PY", "Paraguay - PY"},
{"PE", "Peru - PE"},
{"PH", "Philippines - PH"},
{"PN", "Pitcairn - PN"},
{"PL", "Poland - PL"},
{"PT", "Portugal - PT"},
{"PR", "Puerto Rico - PR"},
{"QA", "Qatar - QA"},
{"RE", "R�union - RE"},
{"RO", "Romania - RO"},
{"RU", "Russian Federation - RU"},
{"RW", "Rwanda - RW"},
{"BL", "Saint Barth�lemy - BL"},
{"KN", "Saint Kitts and Nevis - KN"},
{"LC", "Saint Lucia - LC"},
{"MF", "Saint Martin (French part) - MF"},
{"PM", "Saint Pierre and Miquelon - PM"},
{"VC", "Saint Vincent and the Grenadines - VC"},
{"WS", "Samoa - WS"},
{"SM", "San Marino - SM"},
{"ST", "Sao Tome and Principe - ST"},
{"SA", "Saudi Arabia - SA"},
{"SN", "Senegal - SN"},
{"RS", "Serbia - RS"},
{"SC", "Seychelles - SC"},
{"SL", "Sierra Leone - SL"},
{"SG", "Singapore - SG"},
{"SX", "Sint Maarten (Dutch part) - SX"},
{"SK", "Slovakia - SK"},
{"SI", "Slovenia - SI"},
{"SB", "Solomon Islands - SB"},
{"SO", "Somalia - SO"},
{"ZA", "South Africa - ZA"},
{"GS", "South Georgia and the South Sandwich Islands - GS"},
{"KR", "South Korea - KR"},
{"SS", "South Sudan - SS"},
{"ES", "Spain - ES"},
{"LK", "Sri Lanka - LK"},
{"SD", "Sudan - SD"},
{"SR", "Suriname - SR"},
{"SJ", "Svalbard and Jan Mayen - SJ"},
{"SZ", "Swaziland - SZ"},
{"SE", "Sweden - SE"},
{"CH", "Switzerland - CH"},
{"SY", "Syrian Arab Republic - SY"},
{"TW", "Taiwan - TW"},
{"TJ", "Tajikistan - TJ"},
{"TZ", "Tanzania - TZ"},
{"TH", "Thailand - TH"},
{"TL", "Timor-Leste - TL"},
{"TG", "Togo - TG"},
{"TK", "Tokelau - TK"},
{"TO", "Tonga - TO"},
{"TT", "Trinidad and Tobago - TT"},
{"TN", "Tunisia - TN"},
{"TR", "Turkey - TR"},
{"TM", "Turkmenistan - TM"},
{"TC", "Turks and Caicos Islands - TC"},
{"TV", "Tuvalu - TV"},
{"UG", "Uganda - UG"},
{"UA", "Ukraine - UA"},
{"AE", "United Arab Emirates - AE"},
{"GB", "United Kingdom - GB"},
{"US", "United States - US"},
{"UM", "United States Minor Outlying Islands - UM"},
{"UY", "Uruguay - UY"},
{"UZ", "Uzbekistan - UZ"},
{"VU", "Vanuatu - VU"},
{"VA", "Vatican City State - VA"},
{"VE", "Venezuela - VE"},
{"VN", "Viet Nam - VN"},
{"VG", "Virgin Islands - British - VG"},
{"VI", "Virgin Islands -  U.S. - VI"},
{"WF", "Wallis and Futuna - WF"},
{"EH", "Western Sahara - EH"},
{"YE", "Yemen - YE"},
{"ZM", "Zambia - ZM"},
{"ZW", "Zimbabwe - ZW"}
};      

        public Options(string user, Data.OrdersTableRow order)
        {
            InitializeComponent();
            temp_user = user;
            currentOrder = order;
        }
        private void Options_Load(object sender, EventArgs e)
        {
            //read all the printers
            string[] printers = new string[PrinterSettings.InstalledPrinters.Count];
            PrinterSettings.InstalledPrinters.CopyTo(printers, 0);
            cbboxLabelsPrinter.Items.AddRange(printers);
            cbboxCIPrinter.Items.AddRange(printers);
            cbboxBOLPrinter.Items.AddRange(printers);
            cbboxReturnLabelPrinter.Items.AddRange(printers);
            cbboxPackSlipPrinter.Items.AddRange(printers);
            cbboxContentsLabelPrinter.Items.AddRange(printers);

            // load the settings from app.config
            useautoprint = Template.UseAutoprint;
            if (Template.AutoprintLabelsFolder != null && Template.AutoprintLabelsFolder != "")
            {
                txtLabelsFolder.Text = Template.AutoprintLabelsFolder;
                cbboxLabelsPrinter.Text = Template.AutoprintLabelsPrinter;
                chbox_labels.Checked = true;
            }

            if (Template.AutoprintCIFolder != null && Template.AutoprintCIFolder != "")
            {
                txtCIFolder.Text = Template.AutoprintCIFolder;
                cbboxCIPrinter.Text = Template.AutoprintCIPrinter;
                chbox_CI.Checked = true;
            }

            if (Template.AutoprintBOLFolder != null && Template.AutoprintBOLFolder != "")
            {
                txtBOLFolder.Text = Template.AutoprintBOLFolder;
                cbboxBOLPrinter.Text = Template.AutoprintBOLPrinter;
                chbox_bol.Checked = true;
            }

            if (Template.AutoprintReturnLabelFolder != null && Template.AutoprintReturnLabelFolder != "")
            {
                txtReturnLabelFolder.Text = Template.AutoprintReturnLabelFolder;
                cbboxReturnLabelPrinter.Text = Template.AutoprintReturnLabelPrinter;
                chbox_rlabels.Checked = true;
            }

            if (Template.AutoprintPackSlipFolder != null && Template.AutoprintPackSlipFolder != "")
            {
                txtPackSlipFolder.Text = Template.AutoprintPackSlipFolder;
                cbboxPackSlipPrinter.Text = Template.AutoprintPackSlipPrinter;
                chbox_packslip.Checked = true;
            }

            if (Template.AutoprintContentsLabelFolder != null && Template.AutoprintContentsLabelFolder != "")
            {
                txtContentsLabelFolder.Text = Template.AutoprintContentsLabelFolder;
                cbboxContentsLabelPrinter.Text = Template.AutoprintContentsLabelPrinter;
                chbox_contentsLabel.Checked = true;
            }
            
            this.data = InteGr8_Main.data;
            InitPickUpChb();
           PackageDimsBS.DataSource = data.PackageDimensions;

            Data.CarriersRow carrier_row = data.Carriers.FindByCode("999");
            if (carrier_row == null)
            {
                TabControl.TabPages.Remove(TabControl.TabPages["MyCarrier"]);
            }

            if (Settings.Default.Backup_delete_houre != null && Settings.Default.Backup_delete_houre >= delete_hour.MinDate && Settings.Default.Backup_delete_houre <= delete_hour.MaxDate)
            {
                delete_hour.Value = Settings.Default.Backup_delete_houre;
            }

            BindingSource bs = new BindingSource();
            bs.DataSource = data.Emails;
            dataGridView1.DataSource = bs;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Email address(es) for saving into DB errors";

            foreach(Data.MyCarrierNamesRow r in data.MyCarrierNames)
            {
                ListBoxItem item = new ListBoxItem();
                item.Set(r.Name, r.SCAC, r.Default);
                listbox_carriers.Items.Add(item);
            }
            if (String.IsNullOrEmpty(Properties.Settings.Default.ShipmentOptions) && currentOrder == null)
            {
                //if there is a ShipmentOprionsFile but no order selected, show the ShipmentOptionstab, but empty
                TabControl.TabPages.Remove(ShipmentOptions);
            }
            else
            {
                if (currentOrder != null)
                {
                    if (!String.IsNullOrEmpty(currentOrder["Carrier"].ToString()))
                    {
                        string carrier = currentOrder["Carrier"].ToString();
                        Data.CarriersRow c = data.Carriers.FindByCode(carrier);
                        if (c != null)
                        {
                            switch (c.Name)
                            {
                                case "FedEx":
                                    {
                                        //tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        //tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        SetPackageOptionsLayout("FedEx");
                                        break;
                                    }
                                case "Purolator":
                                    {
                                        //tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        //tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        SetPackageOptionsLayout("Purolator");
                                        break;
                                    }
                                case "Loomis":
                                    {
                                        if (c.Carrier_Code.Equals("10"))
                                        {
                                            tFedExShipOptions.TabPages.Remove(PackageOptions);
                                            tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                            //tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                            tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                            tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                            tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        }
                                        else if (c.Carrier_Code.Equals("30"))
                                        {
                                            tFedExShipOptions.TabPages.Remove(PackageOptions);
                                            tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                            tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                            //tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                            tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                            tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                            tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        }
                                        break;
                                    }
                                case "UPS":
                                    {
                                        //tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        //tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        SetPackageOptionsLayout("UPS");
                                        break;
                                    }
                                case "DHL":
                                    {
                                        tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        break;
                                    }
                                case "Canada Post":
                                    {
                                        tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        //tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        break;
                                    }
                                case "Canpar":
                                    {
                                        tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        //tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        break;
                                    }
                                case "Dicom":
                                    {
                                        tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        //tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        break;
                                    }
                                case "ICS":
                                    {
                                        tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        //tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        break;
                                    }
                                case "Nationex":
                                    {
                                        tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        //tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        break;
                                    }
                                case "ATSHealthcare":
                                    {
                                        tFedExShipOptions.TabPages.Remove(PackageOptions);
                                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                                        //tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                                        break;
                                    }

                            }
                            ReadShipmentOptions();
                            AlignOptionControls();
                        }
                        else
                        {
                            tFedExShipOptions.TabPages.Remove(PackageOptions);
                            tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                            tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                            tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                            tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                            tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                            tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                            tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                            tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                            tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                            tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                            tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                        }
                    }
                    else
                    {
                        tFedExShipOptions.TabPages.Remove(PackageOptions);
                        tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                        tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                        tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                        tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                        tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                        tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                        tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                        tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                        tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                        tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                        tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                    }
                }
                else
                {
                    tFedExShipOptions.TabPages.Remove(PackageOptions);
                    tFedExShipOptions.TabPages.Remove(DicomShipmentOptions);
                    tFedExShipOptions.TabPages.Remove(CanparShipmentOptions);
                    tFedExShipOptions.TabPages.Remove(CAPostShipmentOptions);
                    tFedExShipOptions.TabPages.Remove(UPSShipmentOptions);
                    tFedExShipOptions.TabPages.Remove(LoomisShipmentOptions);
                    tFedExShipOptions.TabPages.Remove(PuroShipmentOptions);
                    tFedExShipOptions.TabPages.Remove(FedExShipmentOptions);
                    tFedExShipOptions.TabPages.Remove(Loomis30ShipOptions);
                    tFedExShipOptions.TabPages.Remove(ICSShipOptions);
                    tFedExShipOptions.TabPages.Remove(NationexShipOptions);
                    tFedExShipOptions.TabPages.Remove(ATSShipOptions);
                }

            }
            if (String.IsNullOrEmpty(Properties.Settings.Default.PackageDimensions))
            {
                TabControl.TabPages.Remove(PackageDimensions);
            }
            else
            {
                LoadPackageDimensions();
            }
        }

        private void SetPackageOptionsLayout(string carrierName)
        {
            switch (carrierName)
            {
                case "FedEx":
                    {
                        pPuroPackageOptions.Visible = false;
                        pUPSPackageOptions.Visible = false;
                        break;
                    }
                case "Purolator":
                    {
                        pFedExPackageOptions.Visible = false;
                        int y = pFedExPackageOptions.Height;
                        pPuroPackageOptions.Location = new Point(pPuroPackageOptions.Location.X, pPuroPackageOptions.Location.Y - y);
                        pUPSPackageOptions.Visible = false;
                        break;
                    }
                case "UPS":
                    {
                        pFedExPackageOptions.Visible = false;
                        pPuroPackageOptions.Visible = false;
                        int y = pFedExPackageOptions.Height + pPuroPackageOptions.Height;
                        pUPSPackageOptions.Location = new Point(pUPSPackageOptions.Location.X, pUPSPackageOptions.Location.Y - y);
                        break;
                    }
            }
        }

        private void AlignOptionControls()
        {
            if (!cbCOD.Checked)
            {
                CODPanel.Visible = false;
                int y = CODPanel.Height + 5;
                cbHoldAtLocation.Location = new Point(cbHoldAtLocation.Location.X, cbHoldAtLocation.Location.Y - y);
                HoldAtLocationPanel.Location = new Point(HoldAtLocationPanel.Location.X, HoldAtLocationPanel.Location.Y - y);
                cbDryIce.Location = new Point(cbDryIce.Location.X, cbDryIce.Location.Y - y);
                DryIcePanel.Location = new Point(DryIcePanel.Location.X, DryIcePanel.Location.Y - y);
                cbInsideDelivery.Location = new Point(cbInsideDelivery.Location.X, cbInsideDelivery.Location.Y - y);
                cbInsidePickup.Location = new Point(cbInsidePickup.Location.X, cbInsidePickup.Location.Y - y);
                cbSaturdayDelivery.Location = new Point(cbSaturdayDelivery.Location.X, cbSaturdayDelivery.Location.Y - y);
                cbSaturdayPickup.Location = new Point(cbSaturdayPickup.Location.X, cbSaturdayPickup.Location.Y - y);
                cbHomeDelivery.Location = new Point(cbHomeDelivery.Location.X, cbHomeDelivery.Location.Y - y);
                HomeDeliveryPanel.Location = new Point(HomeDeliveryPanel.Location.X, HomeDeliveryPanel.Location.Y - y);
                cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y - y);
                SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y - y);
            }
            if (!cbHoldAtLocation.Checked)
            {
                HoldAtLocationPanel.Visible = false;
                int y = HoldAtLocationPanel.Height + 5;
                cbDryIce.Location = new Point(cbDryIce.Location.X, cbDryIce.Location.Y - y);
                DryIcePanel.Location = new Point(DryIcePanel.Location.X, DryIcePanel.Location.Y - y);
                cbInsideDelivery.Location = new Point(cbInsideDelivery.Location.X, cbInsideDelivery.Location.Y - y);
                cbInsidePickup.Location = new Point(cbInsidePickup.Location.X, cbInsidePickup.Location.Y - y);
                cbSaturdayDelivery.Location = new Point(cbSaturdayDelivery.Location.X, cbSaturdayDelivery.Location.Y - y);
                cbSaturdayPickup.Location = new Point(cbSaturdayPickup.Location.X, cbSaturdayPickup.Location.Y - y);
                cbHomeDelivery.Location = new Point(cbHomeDelivery.Location.X, cbHomeDelivery.Location.Y - y);
                HomeDeliveryPanel.Location = new Point(HomeDeliveryPanel.Location.X, HomeDeliveryPanel.Location.Y - y);
                cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y - y);
                SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y - y);
            }
            if (!cbDryIce.Checked)
            {
                DryIcePanel.Visible = false;
                int y = DryIcePanel.Height + 5;
                cbInsideDelivery.Location = new Point(cbInsideDelivery.Location.X, cbInsideDelivery.Location.Y - y);
                cbInsidePickup.Location = new Point(cbInsidePickup.Location.X, cbInsidePickup.Location.Y - y);
                cbSaturdayDelivery.Location = new Point(cbSaturdayDelivery.Location.X, cbSaturdayDelivery.Location.Y - y);
                cbSaturdayPickup.Location = new Point(cbSaturdayPickup.Location.X, cbSaturdayPickup.Location.Y - y);
                cbHomeDelivery.Location = new Point(cbHomeDelivery.Location.X, cbHomeDelivery.Location.Y - y);
                HomeDeliveryPanel.Location = new Point(HomeDeliveryPanel.Location.X, HomeDeliveryPanel.Location.Y - y);
                cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y - y);
                SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y - y);
            }
            if (!cbHomeDelivery.Checked)
            {
                HomeDeliveryPanel.Visible = false;
                int y = HomeDeliveryPanel.Height + 5;
                cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y - y);
                SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y - y);
            }
            if (!cbSignatureOption.Checked)
            {
                SignatureOptionPanel.Visible = false;
            }
            if (!cbExpressCheque.Checked)
            {
                pExpressCheque.Visible = false;
                int y = pExpressCheque.Height + 5;
                cbHoldForPickUp.Location = new Point(cbHoldForPickUp.Location.X, cbHoldForPickUp.Location.Y - y);
                cbPuroSaturdayDelivery.Location = new Point(cbPuroSaturdayDelivery.Location.X, cbPuroSaturdayDelivery.Location.Y - y);
                cbPuroSaturdayPickup.Location = new Point(cbPuroSaturdayPickup.Location.X, cbPuroSaturdayPickup.Location.Y - y);
                cbPuroExceptionHandling.Location = new Point(cbPuroExceptionHandling.Location.X, cbPuroExceptionHandling.Location.Y - y);
                cbChainOfSignature.Location = new Point(cbChainOfSignature.Location.X, cbChainOfSignature.Location.Y - y);
                cbPuroDangGoods.Location = new Point(cbPuroDangGoods.Location.X, cbPuroDangGoods.Location.Y - y);
                pPuroDangGoods.Location = new Point(pPuroDangGoods.Location.X, pPuroDangGoods.Location.Y - y);
            }
            if (!cbPuroDangGoods.Checked)
            {
                pPuroDangGoods.Visible = false;
            }
            if (!cbLoomisReturnCheck.Checked)
            {
                pLoomisReturnCheck.Visible = false;
                int y = pLoomisReturnCheck.Height + 5;
                cbLoomisSaturdayDelivery.Location = new Point(cbLoomisSaturdayDelivery.Location.X, cbLoomisSaturdayDelivery.Location.Y - y);
                cbLoomisNoSignatureRequired.Location = new Point(cbLoomisNoSignatureRequired.Location.X, cbLoomisNoSignatureRequired.Location.Y - y);
                labelAdditionalInfo.Location = new Point(labelAdditionalInfo.Location.X, labelAdditionalInfo.Location.Y - y);
                labelAdditionalInfo1.Location = new Point(labelAdditionalInfo1.Location.X, labelAdditionalInfo1.Location.Y - y);
                tbLoomisAdditionalInfo1.Location = new Point(tbLoomisAdditionalInfo1.Location.X, tbLoomisAdditionalInfo1.Location.Y - y);
                labelAdditionalInfo2.Location = new Point(labelAdditionalInfo2.Location.X, labelAdditionalInfo2.Location.Y - y);
                tbLoomisAdditionalInfo2.Location = new Point(tbLoomisAdditionalInfo2.Location.X, tbLoomisAdditionalInfo2.Location.Y - y);
                labelInstructions.Location = new Point(labelInstructions.Location.X, labelInstructions.Location.Y - y);
                rtbLoomisInstructions.Location = new Point(rtbLoomisInstructions.Location.X, rtbLoomisInstructions.Location.Y - y);
            }
            else
            {

            }
            if (!cbUPSDeliveryConfirmation.Checked)
            {
                pUPSDeliveryConfirmation.Visible = false;
                int y = pUPSDeliveryConfirmation.Height + 5;
                cbUPSLargePackage.Location = new Point(cbUPSLargePackage.Location.X, cbUPSLargePackage.Location.Y - y);
            }
            if (!cbCAPostCOD.Checked)
            {
                pCAPostCOD.Visible = false;
                int y = pCAPostCOD.Height;
                labelCAPostNonDeliveryHandling.Location = new Point(labelCAPostNonDeliveryHandling.Location.X, labelCAPostNonDeliveryHandling.Location.Y - y);
                cbCaPostCardForPickup.Location = new Point(cbCaPostCardForPickup.Location.X, cbCaPostCardForPickup.Location.Y - y);
                cbCAPostDoNotSafeDrop.Location = new Point(cbCAPostDoNotSafeDrop.Location.X, cbCAPostDoNotSafeDrop.Location.Y - y);
                cbCAPostLeaveAtDoor.Location = new Point(cbCAPostLeaveAtDoor.Location.X, cbCAPostLeaveAtDoor.Location.Y - y);
            }
            if (!cbCanparCOD.Checked)
            {
                labelCanparCODAmount.Visible = false;
                tbCanparCODAmount.Visible = false;
            }
            //Package level
            if (!cbPackageDryIce.Checked)
            {
                pDryIcePackageLevel.Visible = false;
                int y = pDryIcePackageLevel.Height;
                cbPackageNonStandardContainer.Location = new Point(cbPackageNonStandardContainer.Location.X, cbPackageNonStandardContainer.Location.Y - y);
            
            }
            if (!cbUPSPackageCOD.Checked)
            {
                pUPSPackageCOD.Visible = false;
                int y = pUPSPackageCOD.Height;
                cbUPSPackageDeliveryConfirmation.Location = new Point(cbUPSPackageDeliveryConfirmation.Location.X, cbUPSPackageDeliveryConfirmation.Location.Y - y);
                labelUPSPackageConfirmationType.Location = new Point(labelUPSPackageConfirmationType.Location.X, labelUPSPackageConfirmationType.Location.Y - y);
                cbUPSPackageDeliveryConfirmationType.Location = new Point(cbUPSPackageDeliveryConfirmationType.Location.X, cbUPSPackageDeliveryConfirmationType.Location.Y - y);

            }
            if (!cbUPSPackageDeliveryConfirmation.Checked)
            {
                labelUPSPackageConfirmationType.Visible = false;
                cbUPSPackageDeliveryConfirmationType.Visible = false;
            }
            if (!cbATSTR.Checked)
            {
                dtpATSStartDate.Visible = false;
                dtpATSEndDate.Visible = false;
                cbATSReleaseOptions.Visible = false;
            }
        }

        private void LoadPackageDimensions()
        {
            try
            {

               
                cbpackageDimensions.DataSource = PackageDimsBS;
                cbpackageDimensions.DisplayMember = "PackageName";
                cbpackageDimensions.ValueMember = "PackageDimCode";
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ReadShipmentOptions()
        {
            try
            {
                cbCountry.DataSource = new BindingSource(countryCodes, null);
                cbCountry.ValueMember = "Key";
                cbCountry.DisplayMember = "Value";
                cbHoldCountry.DataSource = new BindingSource(countryCodes, null);
                cbHoldCountry.ValueMember = "Key";
                cbHoldCountry.DisplayMember = "Value";
                cbPuroCountry.DataSource = new BindingSource(countryCodes, null);
                cbPuroCountry.ValueMember = "Key";
                cbPuroCountry.DisplayMember = "Value";
                cbPuroDGMode.DataSource = new BindingSource(DangerousGoodsMode, null);
                cbPuroDGMode.ValueMember = "Key";
                cbPuroDGMode.DisplayMember = "Value";
                cbPuroDGClass.DataSource = new BindingSource(DangerousGoodsClassification, null);
                cbPuroDGClass.ValueMember = "Key";
                cbPuroDGClass.DisplayMember = "Value";
                cbCAPostCODCountry.DataSource = new BindingSource(countryCodes, null);
                cbCAPostCODCountry.ValueMember = "Key";
                cbCAPostCODCountry.DisplayMember = "Value";
                cbMethodOfPayment.DataSource = new BindingSource(chequeTypes, null);
                cbMethodOfPayment.ValueMember = "Key";
                cbMethodOfPayment.DisplayMember = "Value";
                cbLoomisMethodOfPayment.DataSource = new BindingSource(chequeTypes, null);
                cbLoomisMethodOfPayment.ValueMember = "Key";
                cbLoomisMethodOfPayment.DisplayMember = "Value";
                cbUPSDeliveryConfirmationType.DataSource = new BindingSource(upsConfirmationType, null);
                cbUPSDeliveryConfirmationType.ValueMember = "Key";
                cbUPSDeliveryConfirmationType.DisplayMember = "Value";
                cbUPSPackageDeliveryConfirmationType.DataSource = new BindingSource(upsConfirmationType, null);
                cbUPSPackageDeliveryConfirmationType.ValueMember = "Key";
                cbUPSPackageDeliveryConfirmationType.DisplayMember = "Value";
                cbCAPostMethodOfPayment.DataSource = new BindingSource(CAPostMethodOfPayment, null);
                cbCAPostMethodOfPayment.ValueMember = "Key";
                cbCAPostMethodOfPayment.DisplayMember = "Value";
                cbATSTemp.DataSource = new BindingSource(ATSTemperature, null);
                cbATSTemp.ValueMember = "Key";
                cbATSTemp.DisplayMember = "Value";
                cbATSReleaseOptions.DataSource = new BindingSource(ATSTimeRelease, null);
                cbATSReleaseOptions.ValueMember = "Key";
                cbATSReleaseOptions.DisplayMember = "Value";
                //read all shipment options from file, and load the values to the ShipmentOptions page
                if (data.ShipmentOptions == null)
                {
                    data.ShipmentOptions.ReadXml(Properties.Settings.Default.ShipmentOptions);
                }
                else
                {
                    Data.ShipmentOptionsRow d = data.ShipmentOptions.FindByOptionCode("1070");
                    cbCOD.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1074");
                    tbFedExAmount.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1075");
                    cbFedExCollectionType.SelectedItem = String.IsNullOrEmpty(d["Value"].ToString()) ? "" : d["Value"];
                    d = data.ShipmentOptions.FindByOptionCode("1086");
                    cbFedExCurrency.SelectedItem = String.IsNullOrEmpty(d["Value"].ToString()) ? "" : d["Value"];
                    d = data.ShipmentOptions.FindByOptionCode("1087");
                    cbTranspCharges.SelectedItem = String.IsNullOrEmpty(d["Value"].ToString()) ? "" : d["Value"];
                    d = data.ShipmentOptions.FindByOptionCode("1088");
                    cbReferenceIndicator.SelectedItem = String.IsNullOrEmpty(d["Value"].ToString()) ? "" : d["Value"];
                    d = data.ShipmentOptions.FindByOptionCode("1077");
                    tbReturnContact.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1078");
                    tbReturnCompany.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1079");
                    cbCountry.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1080");
                    cbStateProvince.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1081");
                    tbZIP.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1082");
                    tbCity.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1083");
                    tbReturnAddress1.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1084");
                    tbReturnAddress2.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1085");
                    tbReturnTel.Text = d["Value"].ToString();


                    d = data.ShipmentOptions.FindByOptionCode("1100");
                    cbHoldAtLocation.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1102");
                    tbHoldContact.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1103");
                    tbHoldCompany.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1104");
                    cbHoldCountry.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1105");
                    cbHoldState.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1106");
                    tbHoldZIP.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1107");
                    tbHoldCity.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1108");
                    tbHoldAddress1.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1109");
                    tbHoldAddress2.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1110");
                    tbHoldPhone.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1111");
                    tbHoldEmail.Text = d["Value"].ToString();



                    d = data.ShipmentOptions.FindByOptionCode("1120");
                    cbDryIce.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1121");
                    tbDryIceWeight.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1122");
                    cbDryIceUnit.SelectedItem = d["Value"].ToString();

                    d = data.ShipmentOptions.FindByOptionCode("1128");
                    cbHomeDelivery.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1127");
                    cbHomeDeliveryType.SelectedValue = d["Value"].ToString();
                    //if (cbHomeDeliveryType.SelectedValue.ToString().Equals("DATE_CERTAIN"))
                    //{
                    d = data.ShipmentOptions.FindByOptionCode("1129");
                    dtpHomeDeliveryDate.Value = String.IsNullOrEmpty(d["Value"].ToString()) ? DateTime.Today : Convert.ToDateTime(d["Value"]);
                    //}
                    d = data.ShipmentOptions.FindByOptionCode("1144");
                    tbHomeDeliveryPhone.Text = d["Value"].ToString();


                    d = data.ShipmentOptions.FindByOptionCode("1141");
                    cbSignatureOption.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1142");
                    cbSignatureRequired.SelectedValue = d["Value"];
                    d = data.ShipmentOptions.FindByOptionCode("1143");
                    tbReleaseNumber.Text = d["Value"].ToString();


                    d = data.ShipmentOptions.FindByOptionCode("1125");
                    cbSaturdayDelivery.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("1126");
                    cbSaturdayPickup.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    //Puro
                    d = data.ShipmentOptions.FindByOptionCode("1044");
                    cbOSNR.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1904");
                    cbPuroSignatureRequired.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("1900");
                    cbExpressCheque.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("1901");
                    cbMethodOfPayment.SelectedText = d["Value"].ToString();

                    d = data.ShipmentOptions.FindByOptionCode("1902");
                    tbAmount.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1905");
                    tbCompany.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1906");
                    tbName.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1907");
                    cbPuroCountry.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1908");
                    cbPuroState.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1909");
                    tbPuroCity.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1910");
                    tbPuroAddress1.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1911");
                    tbPuroAddress2.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1912");
                    tbPuroZIP.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1913");
                    tbPuroPhone.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1914");
                    tbPuroEmail.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1915");
                    tbDepartment.Text = d["Value"].ToString();

                    d = data.ShipmentOptions.FindByOptionCode("1916");
                    cbHoldForPickUp.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1917");
                    cbPuroSaturdayDelivery.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1918");
                    cbPuroSaturdayPickup.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1045");
                    cbPuroExceptionHandling.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1919");
                    cbChainOfSignature.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    Data.ShipmentOptionsRow PuroDGMode = data.ShipmentOptions.FindByOptionCode("1094");
                    Data.ShipmentOptionsRow PuroDGClass = data.ShipmentOptions.FindByOptionCode("1037");
                    if (!PuroDGMode["Value"].ToString().Equals("0") || !PuroDGClass["Value"].ToString().Equals("0"))
                    {
                        cbPuroDangGoods.Checked = true;
                        d = data.ShipmentOptions.FindByOptionCode("1094");
                        cbPuroDGMode.SelectedValue = d["Value"].ToString();
                        d = data.ShipmentOptions.FindByOptionCode("1037");
                        cbPuroDGClass.SelectedValue = d["Value"].ToString();
                    }


                    //Loomis
                    d = data.ShipmentOptions.FindByOptionCode("1936");
                    cbLoomisNotPackaged.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1937");
                    cbLoomisFragile.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("1920");
                    cbLoomisReturnCheck.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1921");
                    cbLoomisMethodOfPayment.SelectedText = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1922");
                    tbLoomisAmount.Text = d["Value"].ToString();

                    d = data.ShipmentOptions.FindByOptionCode("1938");
                    cbLoomisSaturdayDelivery.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1942");
                    cbLoomisNoSignatureRequired.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("1941");
                    tbLoomisAdditionalInfo1.Text = d["Value"].ToString().IndexOf('-') < 0 ? "" : d["Value"].ToString().Split('-')[0];
                    tbLoomisAdditionalInfo2.Text = d["Value"].ToString().IndexOf('-') < 0 ? "" : d["Value"].ToString().Split('-')[1];

                    d = data.ShipmentOptions.FindByOptionCode("1943");
                    rtbLoomisInstructions.Text = d["Value"].ToString();

                    //Loomis30
                    d = data.ShipmentOptions.FindByOptionCode("5151");
                    cbLDangGoods.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("5150");
                    cbLFragile.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("5152");
                    cbLSatDel.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("5153");
                    cbLNonStdPack.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("5157");
                    cbLNSR.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("5158");
                    cbLSpecialHandling.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("5154");
                    rtbLInstr.Text = d["Value"].ToString();

                    //UPS
                    d = data.ShipmentOptions.FindByOptionCode("1216");
                    cbUPSSaturdayDelivery.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1217");
                    cbUPSSaturdayPickup.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1218");
                    cbUPSDeliveryConfirmation.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1219");
                    cbUPSDeliveryConfirmationType.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1227");
                    cbUPSLargePackage.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    //CA Post
                    d = data.ShipmentOptions.FindByOptionCode("1191");
                    cbCAPostAge18.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1210");
                    cbCAPostAge19.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1190");
                    cbCAPostSignatureRequired.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1196");
                    cbCAPostDeliveryConfirmation.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1211");
                    cbCAPostUnpackaged.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    d = data.ShipmentOptions.FindByOptionCode("1197");
                    cbCAPostCOD.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1198");
                    tbCAPostAmount.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1199");
                    cbCAPostMethodOfPayment.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1200");
                    tbCAPostCODName.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1201");
                    tbCAPostCODCompany.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1202");
                    tbDepartment.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1203");
                    cbCAPostCODCountry.SelectedValue = "CA"; //CAPost only delivers to CA, there is no use for other countries
                    cbCAPostCODState.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1204");
                    tbCAPostCODCity.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1205");
                    tbCAPostCODAddress1.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1206");
                    tbCAPostCODAddress2.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1207");
                    tbCAPostCODZIP.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1208");
                    tbCAPostCODEmail.Text = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1209");
                    tbCAPostCODPhone.Text = d["Value"].ToString();

                    d = data.ShipmentOptions.FindByOptionCode("1192");
                    cbCaPostCardForPickup.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1193");
                    cbCAPostDoNotSafeDrop.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1194");
                    cbCAPostLeaveAtDoor.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    //Canpar

                    d = data.ShipmentOptions.FindByOptionCode("1417");
                    cbCanparSaturdayDelivery.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1418");
                    cbCanparExtraCare.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1416");
                    cbCanparCOD.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1419");
                    tbCanparCODAmount.Text = d["Value"].ToString();

                    //Dicom
                    d = data.ShipmentOptions.FindByOptionCode("1449");
                    cbDicomDangerousGoods.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1450");
                    cbDicomWeekend.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1451");
                    cbDicomOvernight.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1447");
                    cbDicomSignatureRequired.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    //ICS
                    d = data.ShipmentOptions.FindByOptionCode("1453");
                    cbICSNSR.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1454");
                    tbICSSI.Text = d["Value"].ToString();

                    //Nationex
                    d = data.ShipmentOptions.FindByOptionCode("1481");
                    cbNationexDG.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1482");
                    cbNationexNC.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1483");
                    cbNationexFP.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1485");
                    cbNationexRR.Checked = (d["Value"].ToString().Equals("1")) ? true : false;

                    //ATSHealthcare
                    d = data.ShipmentOptions.FindByOptionCode("1582");
                    cbATSBackDoor.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1584");
                    cbATSDG.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1587");
                    cbATSHardcopyPOD.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1588");
                    cbATSAppt.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1590");
                    cbATSTailGate.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1591");
                    cbATSTR.Checked = (d["Value"].ToString().Equals("1")) ? true : false;
                    d = data.ShipmentOptions.FindByOptionCode("1592");
                    cbATSTemp.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1593");
                    cbATSReleaseOptions.SelectedValue = d["Value"].ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1594");
                    if (!String.IsNullOrEmpty(d["Value"].ToString()))
                    {
                        dtpATSStartDate.Value = Convert.ToDateTime(d["Value"]);
                    }

                    d = data.ShipmentOptions.FindByOptionCode("1595");
                    if (!String.IsNullOrEmpty(d["Value"].ToString()))
                    {
                       dtpATSEndDate.Value = Convert.ToDateTime(d["Value"]);
                    }

                }
            }
            catch (Exception ex)
            {
                Utils.SendError(ex.Message);
            }
        }

        private void InitPickUpChb()
        {
            chbMonday.Checked = Template.PickUpMonday;
            chbTuesday.Checked = Template.PickUpTuesday;
            chbWednesday.Checked = Template.PickUpWednesday;
            chbThursday.Checked = Template.PickUpThursday;
            chbFriday.Checked = Template.PickUpFriday;
            chbSaturday.Checked = Template.PickUpSaturday;
            chbSunday.Checked = Template.PickUpSunday;
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            if (Utils.IsAdobe(true))
            {
                useautoprint = false;
                if (chbox_labels.Checked)
                {
                    if (!Directory.Exists(txtLabelsFolder.Text))
                    {
                        MessageBox.Show("Incorrect autoprint labels folder.", "InteGr8 Options");
                        return;
                    }
                    bool ok = false;
                    foreach (string printer in PrinterSettings.InstalledPrinters)
                    {
                        if (printer.Equals(cbboxLabelsPrinter.Text))
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        MessageBox.Show("Incorrect autoprint labels printer.", "InteGr8 Options");
                        return;
                    }
                    if (!useautoprint)
                    {
                        useautoprint = true;
                    }
                    Template.AutoprintLabelsFolder = txtLabelsFolder.Text;
                    Template.AutoprintLabelsPrinter = cbboxLabelsPrinter.Text;
                }
                else
                {
                    Template.AutoprintLabelsFolder = "";
                    Template.AutoprintLabelsPrinter = "";
                }
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintLabelsFolder), temp_user);
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintLabelsPrinter), temp_user);

                if (chbox_CI.Checked)
                {
                    if (!Directory.Exists(txtCIFolder.Text))
                    {
                        MessageBox.Show("Incorrect autoprint Commercial Invoice folder.", "InteGr8 Options");
                        return;
                    }
                    bool ok = false;
                    foreach (string printer in PrinterSettings.InstalledPrinters)
                    {
                        if (printer.Equals(cbboxCIPrinter.Text))
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        MessageBox.Show("Incorrect autoprint Commercial Invoice printer.", "InteGr8 Options");
                        return;
                    }
                    if (!useautoprint)
                    {
                        useautoprint = true;
                    }
                    Template.AutoprintCIFolder = txtCIFolder.Text;
                    Template.AutoprintCIPrinter = cbboxCIPrinter.Text;
                }
                else
                {
                    Template.AutoprintCIFolder = "";
                    Template.AutoprintCIPrinter = "";
                }
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintCIFolder), temp_user);
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintCIPrinter), temp_user);

                if (chbox_bol.Checked)
                {
                    if (!Directory.Exists(txtBOLFolder.Text))
                    {
                        MessageBox.Show("Incorrect autoprint BOL folder.", "InteGr8 Options");
                        return;
                    }
                    bool ok = false;
                    foreach (string printer in PrinterSettings.InstalledPrinters)
                    {
                        if (printer.Equals(cbboxBOLPrinter.Text))
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        MessageBox.Show("Incorrect autoprint BOL printer.", "InteGr8 Options");
                        return;
                    }
                    if (!useautoprint)
                    {
                        useautoprint = true;
                    }
                    Template.AutoprintBOLFolder = txtBOLFolder.Text;
                    Template.AutoprintBOLPrinter = cbboxBOLPrinter.Text;
                }
                else
                {
                    Template.AutoprintBOLFolder = "";
                    Template.AutoprintBOLPrinter = "";
                }
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintBOLFolder), temp_user);
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintBOLPrinter), temp_user);

                if (chbox_rlabels.Checked)
                {
                    if (!Directory.Exists(txtReturnLabelFolder.Text))
                    {
                        MessageBox.Show("Incorrect autoprint Return Label folder.", "InteGr8 Options");
                        return;
                    }
                    bool ok = false;
                    foreach (string printer in PrinterSettings.InstalledPrinters)
                    {
                        if (printer.Equals(cbboxReturnLabelPrinter.Text))
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        MessageBox.Show("Incorrect autoprint Return Label printer.", "InteGr8 Options");
                        return;
                    }
                    if (!useautoprint)
                    {
                        useautoprint = true;
                    }
                    Template.AutoprintReturnLabelFolder = txtReturnLabelFolder.Text;
                    Template.AutoprintReturnLabelPrinter = cbboxReturnLabelPrinter.Text;
                }
                else
                {
                    Template.AutoprintReturnLabelFolder = "";
                    Template.AutoprintReturnLabelPrinter = "";
                }
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintReturnLabelFolder), temp_user);
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintReturnLabelPrinter), temp_user);

                if (chbox_packslip.Checked)
                {
                    if (!Directory.Exists(txtPackSlipFolder.Text))
                    {
                        MessageBox.Show("Incorrect autoprint Packslip folder.", "InteGr8 Options");
                        return;
                    }
                    bool ok = false;
                    foreach (string printer in PrinterSettings.InstalledPrinters)
                    {
                        if (printer.Equals(cbboxPackSlipPrinter.Text))
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        MessageBox.Show("Incorrect autoprint Packslip printer.", "InteGr8 Options");
                        return;
                    }
                    if (!useautoprint)
                    {
                        useautoprint = true;
                    }
                    Template.AutoprintPackSlipFolder = txtPackSlipFolder.Text;
                    Template.AutoprintPackSlipPrinter = cbboxPackSlipPrinter.Text;
                }
                else
                {
                    Template.AutoprintPackSlipFolder = "";
                    Template.AutoprintPackSlipPrinter = "";
                }
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintPackSlipFolder), temp_user);
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintPackSlipPrinter), temp_user);

                if (chbox_contentsLabel.Checked)
                {
                    if (!Directory.Exists(txtContentsLabelFolder.Text))
                    {
                        MessageBox.Show("Incorrect autoprint Contents Contents Label folder.", "InteGr8 Options");
                        return;
                    }
                    bool ok = false;
                    foreach (string printer in PrinterSettings.InstalledPrinters)
                    {
                        if (printer.Equals(cbboxContentsLabelPrinter.Text))
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        MessageBox.Show("Incorrect autoprint Contents Contents Label printer.", "InteGr8 Options");
                        return;
                    }
                    if (!useautoprint)
                    {
                        useautoprint = true;
                    }
                    Template.AutoprintContentsLabelFolder = txtContentsLabelFolder.Text;
                    Template.AutoprintContentsLabelPrinter = cbboxContentsLabelPrinter.Text;
                }
                else
                {
                    Template.AutoprintContentsLabelFolder = "";
                    Template.AutoprintContentsLabelPrinter = "";
                }
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintContentsLabelFolder), temp_user);
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.AutoprintContentsLabelPrinter), temp_user);

                Template.UseAutoprint = useautoprint;
                BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.UseAutoprint), temp_user);

                Settings.Default.Save();
            }
            
            data.PickUpDays[0] = chbMonday.Checked;
            data.PickUpDays[1] = chbTuesday.Checked;
            data.PickUpDays[2] = chbWednesday.Checked;
            data.PickUpDays[3] = chbThursday.Checked;
            data.PickUpDays[4] = chbFriday.Checked;
            data.PickUpDays[5] = chbSaturday.Checked;
            data.PickUpDays[6] = chbSunday.Checked;
            InteGr8_Main.data = this.data;

            Template.PickUpMonday = chbMonday.Checked;
            Template.PickUpTuesday = chbTuesday.Checked;
            Template.PickUpWednesday = chbWednesday.Checked;
            Template.PickUpThursday = chbThursday.Checked;
            Template.PickUpFriday = chbFriday.Checked;
            Template.PickUpSaturday = chbSaturday.Checked;
            Template.PickUpSunday = chbSunday.Checked;

            BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.PickUpMonday), temp_user);
            BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.PickUpTuesday), temp_user);
            BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.PickUpWednesday), temp_user);
            BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.PickUpThursday), temp_user);
            BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.PickUpFriday), temp_user);
            BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.PickUpSaturday), temp_user);
            BackupFile.SaveSetting(MemberInfoGetting.GetMemberName(() => Template.PickUpSunday), temp_user);

            Settings.Default.Save();
            Settings.Default.Backup_delete_houre = delete_hour.Value;

            List<string> list = new List<string>();

            dataGridView1.EndEdit();
            data.Emails.AcceptChanges();

            foreach (DataGridViewRow col in dataGridView1.Rows)
            {
                if (!col.IsNewRow)
                {
                    list.Add(col.Cells[1].Value.ToString());
                }
            }

            List<string> list_carriers = new List<string>();

            foreach (ListBoxItem item in listbox_carriers.Items)
            {
                list_carriers.Add(item.GetText() + "," + item.GetScac() + "," + item.GetValue().ToString());
            }


            //foreach (Data.ShipmentOptionsRow d in data.ShipmentOptions)
            //{
                
            //}



            //Shipment Options
            //if (TabControl.SelectedTab == ShipmentOptions)
            {
                //get the options for the current carrier
                //get the values for all shipment options from the page
                //FedEx
                Data.ShipmentOptionsRow d = null;
                if (cbCOD.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1070");
                    d["Value"] = 1;
                    d = data.ShipmentOptions.FindByOptionCode("1074");
                    d["Value"] = tbFedExAmount.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1075");
                    d["Value"] = cbFedExCollectionType.SelectedItem.ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1086");
                    d["Value"] = cbFedExCurrency.SelectedItem.ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1087");
                    d["Value"] = cbTranspCharges.SelectedItem.ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1088");
                    d["Value"] = cbReferenceIndicator.SelectedItem.ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1077");
                    d["Value"] = tbReturnContact.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1078");
                    d["Value"] = tbReturnCompany.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1079");
                    d["Value"] = cbCountry.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1080");
                    d["Value"] = cbStateProvince.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1081");
                    d["Value"] = tbZIP.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1082");
                    d["Value"] = tbCity.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1083");
                    d["Value"] = tbReturnAddress1.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1084");
                    d["Value"] = tbReturnAddress2.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1085");
                    d["Value"] = tbReturnTel.Text;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1070");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1074");
                    d["Value"] = "";// tbFedExAmount.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1075");
                    d["Value"] = null;// cbFedExCollectionType.SelectedItem.ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1086");
                    d["Value"] = null;// cbFedExCurrency.SelectedItem.ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1087");
                    d["Value"] = null;// cbTranspCharges.SelectedItem.ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1088");
                    d["Value"] = null;//cbReferenceIndicator.SelectedItem.ToString();
                    d = data.ShipmentOptions.FindByOptionCode("1077");
                    d["Value"] = "";// tbReturnContact.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1078");
                    d["Value"] = "";// tbReturnCompany.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1079");
                    d["Value"] = cbCountry.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1080");
                    d["Value"] = null; // cbStateProvince.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1081");
                    d["Value"] = "";// tbZIP.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1082");
                    d["Value"] = "";// tbCity.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1083");
                    d["Value"] = "";// tbReturnAddress1.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1084");
                    d["Value"] = "";// tbReturnAddress2.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1085");
                    d["Value"] = "";// tbReturnTel.Text;
                }
                if (cbHoldAtLocation.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1100");
                    d["Value"] = 1;
                    d = data.ShipmentOptions.FindByOptionCode("1102");
                    d["Value"] = tbHoldContact.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1103");
                    d["Value"] = tbHoldCompany.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1104");
                    d["Value"] = cbHoldCountry.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1105");
                    d["Value"] = cbHoldState.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1106");
                    d["Value"] = tbHoldZIP.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1107");
                    d["Value"] = tbHoldCity.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1108");
                    d["Value"] = tbHoldAddress1.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1109");
                    d["Value"] = tbHoldAddress2.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1110");
                    d["Value"] = tbHoldPhone.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1111");
                    d["Value"] = tbHoldEmail.Text;

                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1100");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1102");
                    d["Value"] = "";// tbHoldContact.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1103");
                    d["Value"] = "";// tbHoldCompany.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1104");
                    d["Value"] = cbHoldCountry.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1105");
                    d["Value"] = null;// cbHoldState.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1106");
                    d["Value"] = "";// tbHoldZIP.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1107");
                    d["Value"] = "";// tbHoldCity.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1108");
                    d["Value"] = "";// tbHoldAddress1.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1109");
                    d["Value"] = "";// tbHoldAddress2.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1110");
                    d["Value"] = "";// tbHoldPhone.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1111");
                    d["Value"] = "";// tbHoldEmail.Text;
                }
                if (cbDryIce.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1120");
                    d["Value"] = 1;
                    d = data.ShipmentOptions.FindByOptionCode("1121");
                    d["Value"] = tbDryIceWeight.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1122");
                    d["Value"] = cbDryIceUnit.SelectedItem;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1120");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1121");
                    d["Value"] = "";// tbDryIceWeight.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1122");
                    d["Value"] = null;// cbDryIceUnit.SelectedItem;
                }
                if (cbHomeDelivery.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1128");
                    d["Value"] = 1;
                    d = data.ShipmentOptions.FindByOptionCode("1127");
                    d["Value"] = cbHomeDeliveryType.SelectedItem;
                    if (cbHomeDeliveryType.SelectedItem.ToString().Equals("DATE_CERTAIN"))
                    {
                        d = data.ShipmentOptions.FindByOptionCode("1129");
                        d["Value"] = dtpHomeDeliveryDate.Value.ToString("mm/dd/yyyy");
                    }
                    d = data.ShipmentOptions.FindByOptionCode("1144");
                    d["Value"] = tbHomeDeliveryPhone.Text;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1128");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1127");
                    d["Value"] = null; // cbHomeDeliveryType.SelectedItem;
                    d = data.ShipmentOptions.FindByOptionCode("1129");
                    d["Value"] = ""; //dtpHomeDeliveryDate.Value.ToString("mm/dd/yyyy");
                    d = data.ShipmentOptions.FindByOptionCode("1144");
                    d["Value"] = "";// tbHomeDeliveryPhone.Text;
                }
                if (cbSignatureOption.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1141");
                    d["Value"] = 1;
                    d = data.ShipmentOptions.FindByOptionCode("1142");
                    d["Value"] = cbSignatureRequired.SelectedItem;
                    d = data.ShipmentOptions.FindByOptionCode("1143");
                    d["Value"] = tbReleaseNumber.Text;

                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1141");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1142");
                    d["Value"] = null;// cbSignatureRequired.SelectedItem;
                    d = data.ShipmentOptions.FindByOptionCode("1143");
                    d["Value"] = "";// tbReleaseNumber.Text;
                }
                if (cbSaturdayDelivery.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1125");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1125");
                    d["Value"] = 0;
                
                }
                if (cbSaturdayPickup.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1126");
                    d["Value"] = 1;
                }
                {
                    d = data.ShipmentOptions.FindByOptionCode("1126");
                    d["Value"] = 0;
                
                }
                //Puro
                if (cbOSNR.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1044");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1044");
                    d["Value"] = 0;
                
                }
                if (cbPuroSignatureRequired.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1904");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1904");
                    d["Value"] = 0;
                
                }
                if (cbExpressCheque.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1900");
                    d["Value"] = 1;

                    d = data.ShipmentOptions.FindByOptionCode("1901");
                    d["Value"] = cbMethodOfPayment.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1902");
                    d["Value"] = tbAmount.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1905");
                    d["Value"] = tbCompany.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1906");
                    d["Value"] = tbName.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1907");
                    d["Value"] = cbPuroCountry.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1908");
                    d["Value"] = cbPuroState.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1909");
                    d["Value"] = tbPuroCity.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1910");
                    d["Value"] = tbPuroAddress1.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1911");
                    d["Value"] = tbPuroAddress2.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1912");
                    d["Value"] = tbPuroZIP.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1913");
                    d["Value"] = tbPuroPhone.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1914");
                    d["Value"] = tbPuroEmail.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1915");
                    d["Value"] = tbDepartment.Text;

                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1900");
                    d["Value"] = 0;

                    d = data.ShipmentOptions.FindByOptionCode("1901");
                    d["Value"] = null;// cbMethodOfPayment.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1902");
                    d["Value"] = "";// tbAmount.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1905");
                    d["Value"] = "";// tbCompany.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1906");
                    d["Value"] = "";// tbName.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1907");
                    d["Value"] = null;// cbPuroCountry.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1908");
                    d["Value"] = "";// cbPuroState.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1909");
                    d["Value"] = "";// tbPuroCity.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1910");
                    d["Value"] = "";// tbPuroAddress1.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1911");
                    d["Value"] = "";// tbPuroAddress2.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1912");
                    d["Value"] = "";// tbPuroZIP.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1913");
                    d["Value"] = "";// tbPuroPhone.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1914");
                    d["Value"] = "";// tbPuroEmail.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1915");
                    d["Value"] = "";// tbDepartment.Text;

                }
                if (cbHoldForPickUp.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1916");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1916");
                    d["Value"] = 0;
                
                }
                if (cbPuroSaturdayDelivery.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1917");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1917");
                    d["Value"] = 0;
                
                }
                if (cbPuroSaturdayPickup.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1918");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1918");
                    d["Value"] = 0;
                
                }
                if (cbPuroExceptionHandling.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1045");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1045");
                    d["Value"] = 0;
                
                }
                if (cbChainOfSignature.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1919");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1919");
                    d["Value"] = 0;
                
                }
                if (cbPuroDangGoods.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1094");
                    d["Value"] = cbPuroDGMode.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1037");
                    d["Value"] = cbPuroDGClass.SelectedValue;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1094");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1037");
                    d["Value"] = 0;

                }

                //Loomis
                if (cbLoomisNotPackaged.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1936");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1936");
                    d["Value"] = 0;
                
                }
                if (cbLoomisFragile.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1937");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1937");
                    d["Value"] = 0;
                
                }
                if (cbLoomisReturnCheck.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1920");
                    d["Value"] = 1;
                    d = data.ShipmentOptions.FindByOptionCode("1921");
                    d["Value"] = cbLoomisMethodOfPayment.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1922");
                    d["Value"] = tbLoomisAmount.Text;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1920");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1921");
                    d["Value"] = null;// cbLoomisMethodOfPayment.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1922");
                    d["Value"] = "";// tbLoomisAmount.Text;
                    
                }
                if (cbLoomisSaturdayDelivery.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1938");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1938");
                    d["Value"] = 0;
                
                }
                if (cbLoomisNoSignatureRequired.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1942");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1942");
                    d["Value"] = 0;
                
                }
                if (!String.IsNullOrEmpty(tbLoomisAdditionalInfo1.Text))
                {
                    d = data.ShipmentOptions.FindByOptionCode("1941");
                    d["Value"] = tbLoomisAdditionalInfo1.Text + (String.IsNullOrEmpty(tbLoomisAdditionalInfo2.Text) ? "" : " - " + tbLoomisAdditionalInfo2.Text);
                }
                if (!String.IsNullOrEmpty(rtbLoomisInstructions.Text))
                {
                    d = data.ShipmentOptions.FindByOptionCode("1943");
                    d["Value"] = rtbLoomisInstructions.Text;
                }

                //Loomis30
                if (cbLDangGoods.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("5151");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("5151");
                    d["Value"] = 0;

                }
                if (cbLFragile.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("5150");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("5150");
                    d["Value"] = 0;

                }
               
                if (cbLSatDel.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("5152");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("5152");
                    d["Value"] = 0;

                }
                if (cbLNonStdPack.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("5153");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("5153");
                    d["Value"] = 0;

                }
                if (cbLNSR.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("5157");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("5157");
                    d["Value"] = 0;

                }
                if (cbLSpecialHandling.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("5158");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("5158");
                    d["Value"] = 0;

                }
                if (!String.IsNullOrEmpty(rtbLInstr.Text))
                {
                    d = data.ShipmentOptions.FindByOptionCode("5154");
                    d["Value"] = rtbLoomisInstructions.Text;
                }

                //UPS
                if (cbUPSSaturdayDelivery.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1216");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1216");
                    d["Value"] = 0;
                
                }
                if (cbUPSSaturdayPickup.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1217");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1217");
                    d["Value"] = 0;
                
                }
                if (cbUPSDeliveryConfirmation.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1218");
                    d["Value"] = 1;
                    d = data.ShipmentOptions.FindByOptionCode("1219");
                    d["Value"] = cbUPSDeliveryConfirmationType.SelectedValue;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1218");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1219");
                    d["Value"] = null;// cbUPSDeliveryConfirmationType.SelectedValue;
              
                }
                if (cbUPSLargePackage.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1227");
                    d["Value"] = 1;
                }
                else 
                {
                    d = data.ShipmentOptions.FindByOptionCode("1227");
                    d["Value"] = 0;
                }

                //CA Post
                if (cbCAPostAge18.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1191");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1191");
                    d["Value"] = 0;
                
                }
                if (cbCAPostAge19.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1210");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1210");
                    d["Value"] = 0;
                
                }
                if (cbCAPostSignatureRequired.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1190");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1190");
                    d["Value"] = 0;
                }
                if (cbCAPostDeliveryConfirmation.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1196");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1196");
                    d["Value"] = 0;
                
                }
                if (cbCAPostUnpackaged.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1211");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1211");
                    d["Value"] = 0;
                
                }
                if (cbCAPostCOD.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1197");
                    d["Value"] = 1;
                    d = data.ShipmentOptions.FindByOptionCode("1198");
                    d["Value"] = tbCAPostAmount.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1199");
                    d["Value"] = cbCAPostMethodOfPayment.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1200");
                    d["Value"] = tbCAPostCODName.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1201");
                    d["Value"] = tbCAPostCODCompany.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1202");
                    d["Value"] = tbDepartment.Text;

                    d = data.ShipmentOptions.FindByOptionCode("1203");
                    d["Value"] = cbCAPostCODState.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1204");
                    d["Value"] = tbCAPostCODCity.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1205");
                    d["Value"] = tbCAPostCODAddress1.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1206");
                    d["Value"] = tbCAPostCODAddress2.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1207");
                    d["Value"] = tbCAPostCODZIP.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1208");
                    d["Value"] = tbCAPostCODEmail.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1209");
                    d["Value"] = tbCAPostCODPhone.Text;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1197");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1198");
                    d["Value"] = "";// tbCAPostAmount.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1199");
                    d["Value"] = null;// cbCAPostMethodOfPayment.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1200");
                    d["Value"] = "";// tbCAPostCODName.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1201");
                    d["Value"] = "";// tbCAPostCODCompany.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1202");
                    d["Value"] = "";// tbDepartment.Text;

                    d = data.ShipmentOptions.FindByOptionCode("1203");
                    d["Value"] = null;// cbCAPostCODState.SelectedValue;
                    d = data.ShipmentOptions.FindByOptionCode("1204");
                    d["Value"] = "";// tbCAPostCODCity.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1205");
                    d["Value"] = "";// tbCAPostCODAddress1.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1206");
                    d["Value"] = "";// tbCAPostCODAddress2.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1207");
                    d["Value"] = "";// tbCAPostCODZIP.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1208");
                    d["Value"] = "";// tbCAPostCODEmail.Text;
                    d = data.ShipmentOptions.FindByOptionCode("1209");
                    d["Value"] = "";// tbCAPostCODPhone.Text;
                }
                if (cbCaPostCardForPickup.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1192");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1192");
                    d["Value"] = 0;
                
                }
                if (cbCAPostDoNotSafeDrop.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1193");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1193");
                    d["Value"] = 0;
                
                }
                if (cbCAPostLeaveAtDoor.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1194");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1194");
                    d["Value"] = 0;
                
                }
                //Canpar
                if (cbCanparSaturdayDelivery.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1417");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1417");
                    d["Value"] = 0;
                
                }
                if (cbCanparExtraCare.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1418");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1418");
                    d["Value"] = 0;
                
                }
                if (cbCanparCOD.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1416");
                    d["Value"] = 1;
                    d = data.ShipmentOptions.FindByOptionCode("1419");
                    d["Value"] = tbCanparCODAmount.Text;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1416");
                    d["Value"] = 0;
                    d = data.ShipmentOptions.FindByOptionCode("1419");
                    d["Value"] = "";// tbCanparCODAmount.Text;
                    
                }
                //Dicom
                if (cbDicomDangerousGoods.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1449");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1449");
                    d["Value"] = 0;
                }
                if (cbDicomWeekend.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1450");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1450");
                    d["Value"] = 0;
                }
                if (cbDicomOvernight.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1451");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1451");
                    d["Value"] = 0;
                }
                if (cbDicomSignatureRequired.Checked)
                {
                    d = data.ShipmentOptions.FindByOptionCode("1447");
                    d["Value"] = 1;
                }
                else
                {
                    d = data.ShipmentOptions.FindByOptionCode("1447");
                    d["Value"] = 0;
                }

                //ICS
                d = data.ShipmentOptions.FindByOptionCode("1453");
                d["Value"] = cbICSNSR.Checked ? 1 : 0;
                d = data.ShipmentOptions.FindByOptionCode("1454");
                d["Value"] = tbICSSI.Text;

                //Nationex
                d = data.ShipmentOptions.FindByOptionCode("1481");
                d["Value"] =  cbNationexDG.Checked  ? 1 : 0;
                d = data.ShipmentOptions.FindByOptionCode("1482");
                d["Value"] = cbNationexNC.Checked  ? 1 : 0;
                d = data.ShipmentOptions.FindByOptionCode("1483");
                d["Value"] = cbNationexFP.Checked ? 1 : 0;
                d = data.ShipmentOptions.FindByOptionCode("1485");
                d["Value"] = cbNationexRR.Checked  ? 1 : 0;

                //ATSHealthcare
                d = data.ShipmentOptions.FindByOptionCode("1582");
                d["Value"] = cbATSBackDoor.Checked ? 1 : 0;
                d = data.ShipmentOptions.FindByOptionCode("1584");
                d["Value"]= cbATSDG.Checked ? 1 : 0;
                d = data.ShipmentOptions.FindByOptionCode("1587");
                d["Value"]= cbATSHardcopyPOD.Checked  ? 1 :0;
                d = data.ShipmentOptions.FindByOptionCode("1588");
                d["Value"]= cbATSAppt.Checked  ? 1 : 0;
                d = data.ShipmentOptions.FindByOptionCode("1590");
                d["Value"]=cbATSTailGate.Checked ? 1 : 0;
                d = data.ShipmentOptions.FindByOptionCode("1591");
                d["Value"]=cbATSTR.Checked ? 1 : 0;
                d = data.ShipmentOptions.FindByOptionCode("1592");
                d["Value"]=cbATSTemp.SelectedValue;
                d = data.ShipmentOptions.FindByOptionCode("1593");
                if (cbATSReleaseOptions.Visible == true)
                {
                    d["Value"] = cbATSReleaseOptions.SelectedValue.ToString();
                }
                else
                {
                    d["Value"] = 0;
                }
                d = data.ShipmentOptions.FindByOptionCode("1594");
                if (cbATSReleaseOptions.Visible == true && !String.IsNullOrEmpty(dtpATSStartDate.Value.ToString()))
                {
                    d["Value"] = dtpATSStartDate.Value.ToString();
                }
                else
                {
                    d["Value"] = "";
                }
                d = data.ShipmentOptions.FindByOptionCode("1595");
                if (cbATSReleaseOptions.Visible == true && !String.IsNullOrEmpty(dtpATSEndDate.Value.ToString()))
                {
                    d["Value"]=dtpATSEndDate.Value.ToString();
                }
                else
                {
                    d["Value"] = "";
                }


                //Package level options
                //these are saved on a different button action
                //write to xml the changes to the shipment options - and also the package level ones
                File.WriteAllText(Properties.Settings.Default.ShipmentOptions, "");
                data.ShipmentOptions.WriteXml(Properties.Settings.Default.ShipmentOptions);
                //Package level options need a PackageOptions table - they are different for each package

                //check all options for all carriers to add to backup
            }
            
            BackupFile.SaveSettingList("Emails", list, temp_user);
            BackupFile.LoadSettingList("Emails", temp_user);

            BackupFile.SaveSettingList("Carriers", list_carriers, temp_user);
            BackupFile.LoadSettingList("Carriers", temp_user);

            Close();
        }

        private void btnLabelsFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog.Description = "Please select a folder to be used for the labels.";
            FolderBrowserDialog.SelectedPath = txtLabelsFolder.Text;
            if (FolderBrowserDialog.ShowDialog(this) == DialogResult.OK)
            {
                txtLabelsFolder.Text = FolderBrowserDialog.SelectedPath;
            }
        }

        private void btnCIFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog.Description = "Please select a folder to be used for the Commercial Invoice.";
            FolderBrowserDialog.SelectedPath = txtCIFolder.Text;
            if (FolderBrowserDialog.ShowDialog(this) == DialogResult.OK)
            {
                txtCIFolder.Text = FolderBrowserDialog.SelectedPath;
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {

        }

        private void chbox_labels_CheckedChanged(object sender, EventArgs e)
        {
            if (chbox_labels.Checked)
            {
                chbox_labels.Text = "Enabled";
                txtLabelsFolder.Enabled = true;
                cbboxLabelsPrinter.Enabled = true;
                btnLabelsFolder.Enabled = true;
            }
            else
            {
                chbox_labels.Text = "Disabled";
                txtLabelsFolder.Enabled = false;
                cbboxLabelsPrinter.Enabled = false;
                btnLabelsFolder.Enabled = false;
            }
        }

        private void btnBOLFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog.Description = "Please select a folder to be used for the BOL.";
            FolderBrowserDialog.SelectedPath = txtBOLFolder.Text;
            if (FolderBrowserDialog.ShowDialog(this) == DialogResult.OK)
            {
                txtBOLFolder.Text = FolderBrowserDialog.SelectedPath;
            }
        }

        private void btnReturnLabelFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog.Description = "Please select a folder to be used for the Return Label.";
            FolderBrowserDialog.SelectedPath = txtReturnLabelFolder.Text;
            if (FolderBrowserDialog.ShowDialog(this) == DialogResult.OK)
            {
                txtReturnLabelFolder.Text = FolderBrowserDialog.SelectedPath;
            }
        }

        private void btnPackSlipFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog.Description = "Please select a folder to be used for the PackSlip.";
            FolderBrowserDialog.SelectedPath = txtPackSlipFolder.Text;
            if (FolderBrowserDialog.ShowDialog(this) == DialogResult.OK)
            {
                txtPackSlipFolder.Text = FolderBrowserDialog.SelectedPath;
            }
        }

        private void btnContentsLabelFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog.Description = "Please select a folder to be used for the PackSlip.";
            FolderBrowserDialog.SelectedPath = txtContentsLabelFolder.Text;
            if (FolderBrowserDialog.ShowDialog(this) == DialogResult.OK)
            {
                txtContentsLabelFolder.Text = FolderBrowserDialog.SelectedPath;
            }
        }

        private void chbox_CI_CheckedChanged(object sender, EventArgs e)
        {
            if (chbox_CI.Checked)
            {
                chbox_CI.Text = "Enabled";
                txtCIFolder.Enabled = true;
                cbboxCIPrinter.Enabled = true;
                btnCIFolder.Enabled = true;
            }
            else
            {
                chbox_CI.Text = "Disabled";
                txtCIFolder.Enabled = false;
                cbboxCIPrinter.Enabled = false;
                btnCIFolder.Enabled = false;
            }
        }

        private void chbox_bol_CheckedChanged(object sender, EventArgs e)
        {
            if (chbox_bol.Checked)
            {
                chbox_bol.Text = "Enabled";
                txtBOLFolder.Enabled = true;
                cbboxBOLPrinter.Enabled = true;
                btnBOLFolder.Enabled = true;
            }
            else
            {
                chbox_bol.Text = "Disabled";
                txtBOLFolder.Enabled = false;
                cbboxBOLPrinter.Enabled = false;
                btnBOLFolder.Enabled = false;
            }
        }

        private void chbox_rlabels_CheckedChanged(object sender, EventArgs e)
        {
            if (chbox_rlabels.Checked)
            {
                chbox_rlabels.Text = "Enabled";
                txtReturnLabelFolder.Enabled = true;
                cbboxReturnLabelPrinter.Enabled = true;
                btnReturnLabelFolder.Enabled = true;
            }
            else
            {
                chbox_rlabels.Text = "Disabled";
                txtReturnLabelFolder.Enabled = false;
                cbboxReturnLabelPrinter.Enabled = false;
                btnReturnLabelFolder.Enabled = false;
            }
        }

        private void chbox_packslip_CheckedChanged(object sender, EventArgs e)
        {
            if (chbox_packslip.Checked)
            {
                chbox_packslip.Text = "Enabled";
                txtPackSlipFolder.Enabled = true;
                cbboxPackSlipPrinter.Enabled = true;
                btnPackSlipFolder.Enabled = true;
            }
            else
            {
                chbox_packslip.Text = "Disabled";
                txtPackSlipFolder.Enabled = false;
                cbboxPackSlipPrinter.Enabled = false;
                btnPackSlipFolder.Enabled = false;
            }
        }

        private void chbox_contentsLabel_CheckedChanged(object sender, EventArgs e)
        {
            if (chbox_contentsLabel.Checked)
            {
                chbox_contentsLabel.Text = "Enabled";
                txtContentsLabelFolder.Enabled = true;
                cbboxContentsLabelPrinter.Enabled = true;
                btnContentsLabelFolder.Enabled = true;
            }
            else
            {
                chbox_contentsLabel.Text = "Disabled";
                txtContentsLabelFolder.Enabled = false;
                cbboxContentsLabelPrinter.Enabled = false;
                btnContentsLabelFolder.Enabled = false;
            }
        }

        private void btn_edit_mycarrier_Click(object sender, EventArgs e)
        {
            MyCarrier c = new MyCarrier(data);
            c.ShowDialog();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (listbox_carriers.SelectedItem != null)
            {
                int index = listbox_carriers.SelectedIndex;
                listbox_carriers.Items.RemoveAt(index);
                error.Clear();
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            error.Clear();
            if (txt_carrier_name.Text.Replace(" ","") != "")
            {
                if (!listbox_carriers.Items.Contains(txt_carrier_name.Text))
                {
                    ListBoxItem item = new ListBoxItem();
                    item.Set(txt_carrier_name.Text, txt_carrier_scac_code.Text != null ? txt_carrier_scac_code.Text : "", false);
                    listbox_carriers.Items.Add(item);
                }
                else
                {
                    error.SetError(txt_carrier_name, "Carrier already exists");
                }
                txt_carrier_name.Text = "";
                txt_carrier_scac_code.Text = "";
            }
        }

        bool ok = true;

        private void checkbox_default_CheckedChanged(object sender, EventArgs e)
        {
            if (ok)
            {
                if (!listbox_carriers.Items.Contains(txt_carrier_name.Text))
                {
                    foreach (ListBoxItem item in listbox_carriers.Items)
                    {
                        if (listbox_carriers.Items.IndexOf(item) != listbox_carriers.SelectedIndex)
                        {
                            item.Set(item.GetText(), item.GetScac(), false);
                        }
                        else
                        {
                            item.Set(item.GetText(), item.GetScac(), true);
                        }
                    }
                }
                else
                {
                    checkbox_default.Checked = false;
                }
            }
        }

        private void listbox_carriers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listbox_carriers.SelectedItem != null)
            {
                ok = false;
                checkbox_default.Checked = (bool)((ListBoxItem)listbox_carriers.SelectedItem).GetValue();
                ok = true;
            }
            else
            {
                ok = false;
                checkbox_default.Checked = false;
                ok = true;
            }
        }

        private void txt_carrier_scac_code_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txt_carrier_scac_code.Text.Length == 4 && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        private void LoomisShipmentOptions_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbpackageDimensions_SelectedIndexChanged(object sender, EventArgs e)
        {
            Data.PackageDimensionsRow p = data.PackageDimensions.FindByCode(cbpackageDimensions.SelectedValue as string);
            tbPackageDimCode.Text = p["PackageDimCode"].ToString();
            tbPackageDimName.Text = p["PackageName"].ToString();
            tbPackageLength.Text = p["Length"].ToString();
            tbPackageWidth.Text = p["Width"].ToString();
            tbPackageHeight.Text = p["Height"].ToString();
        }

        private void btnAddPackageDim_Click(object sender, EventArgs e)
        {
            try
            {
                //PackageDimsBS.SuspendBinding();
                //PackageDimsBS.ResetBindings(false);
                //PackageDimsBS.RaiseListChangedEvents = false;
                data.PackageDimensions.AddPackageDimensionsRow(tbPackageDimCode.Text, tbPackageDimCode.Text + " - " + tbPackageDimName.Text, tbPackageLength.Text, tbPackageWidth.Text, tbPackageHeight.Text);
                //PackageDimsBS.RaiseListChangedEvents = true;
                //PackageDimsBS.ResumeBinding();
                File.WriteAllText(Properties.Settings.Default.PackageDimensions, "");
                data.PackageDimensions.WriteXml(Properties.Settings.Default.PackageDimensions);
                tbPackageDimCode.Text = "";
                tbPackageDimName.Text = "";
                tbPackageLength.Text = "0";
                tbPackageWidth.Text = "0";
                tbPackageHeight.Text = "0";

            }
            catch (Exception ex)
            {
                Utils.SendError(ex.Message + " \n" + ex.StackTrace);
            }
        }

        private void btnSavePackageDims_Click(object sender, EventArgs e)
        {
            try
            {
                //if (PackageDimsBS.Position >= 0)
                //{
                //    PackageDimsBS.SuspendBinding();
                //    PackageDimsBS.ResetBindings(false);
                //    PackageDimsBS.RaiseListChangedEvents = false;
                //}
                Data.PackageDimensionsRow r = data.PackageDimensions.FindByCode(cbpackageDimensions.SelectedValue as string);
                r["PackageDimCode"] = tbPackageDimCode.Text;
                r["PackageName"] = tbPackageDimCode.Text + " - " + tbPackageDimName.Text;
                r["Length"] = tbPackageLength.Text;
                r["Width"] = tbPackageWidth.Text;
                r["Height"] = tbPackageHeight.Text;
                
                //PackageDimsBS.ResetBindings(true);
                //PackageDimsBS.RaiseListChangedEvents = true;
                //PackageDimsBS.ResumeBinding();
                File.WriteAllText(Properties.Settings.Default.PackageDimensions, "");
                data.PackageDimensions.WriteXml(Properties.Settings.Default.PackageDimensions);
                tbPackageDimCode.Text = "";
                tbPackageDimName.Text = "";
                tbPackageLength.Text = "0";
                tbPackageWidth.Text = "0";
                tbPackageHeight.Text = "0";

            }
            catch (Exception ex)
            {
                Utils.SendError(ex.Message + " \n" + ex.StackTrace);
            }
        }

        private void cbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCountry.SelectedIndex >= 0)
            {
                string countryCode = cbCountry.SelectedValue as string;

                switch (countryCode)
                {
                    case "US": {
                        cbStateProvince.DataSource = new BindingSource(usStates, null);
                        cbStateProvince.ValueMember = "Key";
                        cbStateProvince.DisplayMember = "Value";
                        break; }
                    case "CA": {
                        cbStateProvince.DataSource = new BindingSource(caProvinces, null);
                        cbStateProvince.ValueMember = "Key";
                        cbStateProvince.DisplayMember = "Value";
                        break; }
                    default: {
                        cbStateProvince.DataSource = null;
                        break;
                    }
                }
                 } 
            else return;
        }

        private void cbHoldCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbHoldCountry.SelectedIndex >= 0)
            {
                string countryCode = cbHoldCountry.SelectedValue as string;

                switch (countryCode)
                {
                    case "US":
                        {
                            cbHoldState.DataSource = new BindingSource(usStates, null);
                            cbHoldState.ValueMember = "Key";
                            cbHoldState.DisplayMember = "Value";
                            break;
                        }
                    case "CA":
                        {
                            cbHoldState.DataSource = new BindingSource(caProvinces, null);
                            cbHoldState.ValueMember = "Key";
                            cbHoldState.DisplayMember = "Value";
                            break;
                        }
                    default:
                        {
                            cbHoldState.DataSource = null;
                            break;
                        }
                }
            }
            else return;
        }

       private void cbCAPostCODCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCAPostCODCountry.SelectedIndex >= 0)
            {
                string countryCode = cbCAPostCODCountry.SelectedValue as string;

                switch (countryCode)
                {
                    case "US":
                        {
                            cbCAPostCODState.DataSource = new BindingSource(usStates, null);
                            cbCAPostCODState.ValueMember = "Key";
                            cbCAPostCODState.DisplayMember = "Value";
                            break;
                        }
                    case "CA":
                        {
                            cbCAPostCODState.DataSource = new BindingSource(caProvinces, null);
                            cbCAPostCODState.ValueMember = "Key";
                            cbCAPostCODState.DisplayMember = "Value";
                            break;
                        }
                    default:
                        {
                            cbCAPostCODState.DataSource = null;
                            break;
                        }
                }
            }
            else return;
        }

        private void cbPuroCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPuroCountry.SelectedIndex >= 0)
            {
                string countryCode = cbPuroCountry.SelectedValue as string;

                switch (countryCode)
                {
                    case "US":
                        {
                            cbPuroState.DataSource = new BindingSource(usStates, null);
                            cbPuroState.ValueMember = "Key";
                            cbPuroState.DisplayMember = "Value";
                            break;
                        }
                    case "CA":
                        {
                            cbPuroState.DataSource = new BindingSource(caProvinces, null);
                            cbPuroState.ValueMember = "Key";
                            cbPuroState.DisplayMember = "Value";
                            break;
                        }
                    default:
                        {
                            cbPuroState.DataSource = null;
                            break;
                        }
                }
            }
            else return;
        }

        private void cbCOD_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCOD.Checked)
            {
                CODPanel.Visible = true;
                if (cbHoldAtLocation.Location.Y < (cbCOD.Location.Y + CODPanel.Height))
                {
                    int y = CODPanel.Height + 5;
                    cbHoldAtLocation.Location = new Point(cbHoldAtLocation.Location.X, cbHoldAtLocation.Location.Y + y);
                    HoldAtLocationPanel.Location = new Point(HoldAtLocationPanel.Location.X, HoldAtLocationPanel.Location.Y + y);
                    cbDryIce.Location = new Point(cbDryIce.Location.X, cbDryIce.Location.Y + y);
                    DryIcePanel.Location = new Point(DryIcePanel.Location.X, DryIcePanel.Location.Y + y);
                    cbInsideDelivery.Location = new Point(cbInsideDelivery.Location.X, cbInsideDelivery.Location.Y + y);
                    cbInsidePickup.Location = new Point(cbInsidePickup.Location.X, cbInsidePickup.Location.Y + y);
                    cbSaturdayDelivery.Location = new Point(cbSaturdayDelivery.Location.X, cbSaturdayDelivery.Location.Y + y);
                    cbSaturdayPickup.Location = new Point(cbSaturdayPickup.Location.X, cbSaturdayPickup.Location.Y + y);
                    cbHomeDelivery.Location = new Point(cbHomeDelivery.Location.X, cbHomeDelivery.Location.Y + y);
                    HomeDeliveryPanel.Location = new Point(HomeDeliveryPanel.Location.X, HomeDeliveryPanel.Location.Y + y);
                    cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y + y);
                    SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y + y);
                }
            }
            else
            {
                tbFedExAmount.Text = "";
                cbFedExCurrency.SelectedItem = null;
                cbFedExCollectionType.SelectedItem = null;
                cbTranspCharges.SelectedItem = null;
                cbReferenceIndicator.SelectedItem = null;
                tbReturnContact.Text = "";
                tbReturnCompany.Text = "";
                tbReturnAddress1.Text = "";
                tbReturnAddress2.Text = "";
                cbCountry.SelectedIndex = 0;
                tbCity.Text = "";
                tbZIP.Text = "";
                tbReturnTel.Text = "";
                
                CODPanel.Visible = false;
                int y = CODPanel.Height + 5;
                cbHoldAtLocation.Location = new Point(cbHoldAtLocation.Location.X, cbHoldAtLocation.Location.Y - y);
                HoldAtLocationPanel.Location = new Point(HoldAtLocationPanel.Location.X, HoldAtLocationPanel.Location.Y - y);
                cbDryIce.Location = new Point(cbDryIce.Location.X, cbDryIce.Location.Y - y);
                DryIcePanel.Location = new Point(DryIcePanel.Location.X, DryIcePanel.Location.Y - y);
                cbInsideDelivery.Location = new Point(cbInsideDelivery.Location.X, cbInsideDelivery.Location.Y - y);
                cbInsidePickup.Location = new Point(cbInsidePickup.Location.X, cbInsidePickup.Location.Y - y);
                cbSaturdayDelivery.Location = new Point(cbSaturdayDelivery.Location.X, cbSaturdayDelivery.Location.Y - y);
                cbSaturdayPickup.Location = new Point(cbSaturdayPickup.Location.X, cbSaturdayPickup.Location.Y - y);
                cbHomeDelivery.Location = new Point(cbHomeDelivery.Location.X, cbHomeDelivery.Location.Y - y);
                HomeDeliveryPanel.Location = new Point(HomeDeliveryPanel.Location.X, HomeDeliveryPanel.Location.Y - y);
                cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y - y);
                SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y - y);
            }
        }

        private void cbHoldAtLocation_CheckedChanged(object sender, EventArgs e)
        {
            if (cbHoldAtLocation.Checked)
            {
                HoldAtLocationPanel.Visible = true;
                if (cbDryIce.Location.Y < cbHoldAtLocation.Location.Y + HoldAtLocationPanel.Height)
                {
                    int y = HoldAtLocationPanel.Height + 5;
                    cbDryIce.Location = new Point(cbDryIce.Location.X, cbDryIce.Location.Y + y);
                    DryIcePanel.Location = new Point(DryIcePanel.Location.X, DryIcePanel.Location.Y + y);
                    cbInsideDelivery.Location = new Point(cbInsideDelivery.Location.X, cbInsideDelivery.Location.Y + y);
                    cbInsidePickup.Location = new Point(cbInsidePickup.Location.X, cbInsidePickup.Location.Y + y);
                    cbSaturdayDelivery.Location = new Point(cbSaturdayDelivery.Location.X, cbSaturdayDelivery.Location.Y + y);
                    cbSaturdayPickup.Location = new Point(cbSaturdayPickup.Location.X, cbSaturdayPickup.Location.Y + y);
                    cbHomeDelivery.Location = new Point(cbHomeDelivery.Location.X, cbHomeDelivery.Location.Y + y);
                    HomeDeliveryPanel.Location = new Point(HomeDeliveryPanel.Location.X, HomeDeliveryPanel.Location.Y + y);
                    cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y + y);
                    SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y + y);
                }
            }
            else
            {
                tbHoldContact.Text = "";
                tbHoldCompany.Text = "";
                cbHoldCountry.SelectedIndex = 0;
                tbHoldCity.Text = "";
                tbHoldAddress1.Text = "";
                tbHoldAddress2.Text = "";
                tbHoldZIP.Text = "";
                tbHoldPhone.Text = "";
                tbHoldEmail.Text = "";

                HoldAtLocationPanel.Visible = false;
                int y = HoldAtLocationPanel.Height + 5;
                cbDryIce.Location = new Point(cbDryIce.Location.X, cbDryIce.Location.Y - y);
                DryIcePanel.Location = new Point(DryIcePanel.Location.X, DryIcePanel.Location.Y - y);
                cbInsideDelivery.Location = new Point(cbInsideDelivery.Location.X, cbInsideDelivery.Location.Y - y);
                cbInsidePickup.Location = new Point(cbInsidePickup.Location.X, cbInsidePickup.Location.Y - y);
                cbSaturdayDelivery.Location = new Point(cbSaturdayDelivery.Location.X, cbSaturdayDelivery.Location.Y - y);
                cbSaturdayPickup.Location = new Point(cbSaturdayPickup.Location.X, cbSaturdayPickup.Location.Y - y);
                cbHomeDelivery.Location = new Point(cbHomeDelivery.Location.X, cbHomeDelivery.Location.Y - y);
                HomeDeliveryPanel.Location = new Point(HomeDeliveryPanel.Location.X, HomeDeliveryPanel.Location.Y - y);
                cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y - y);
                SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y - y);
            }
        }

        private void cbDryIce_CheckedChanged(object sender, EventArgs e)
        {
            if (cbDryIce.Checked)
            {
                DryIcePanel.Visible = true;
                if (cbInsideDelivery.Location.Y < cbDryIce.Location.Y + DryIcePanel.Height)
                {
                    int y = DryIcePanel.Height + 5;
                    cbInsideDelivery.Location = new Point(cbInsideDelivery.Location.X, cbInsideDelivery.Location.Y + y);
                    cbInsidePickup.Location = new Point(cbInsidePickup.Location.X, cbInsidePickup.Location.Y + y);
                    cbSaturdayDelivery.Location = new Point(cbSaturdayDelivery.Location.X, cbSaturdayDelivery.Location.Y + y);
                    cbSaturdayPickup.Location = new Point(cbSaturdayPickup.Location.X, cbSaturdayPickup.Location.Y + y);
                    cbHomeDelivery.Location = new Point(cbHomeDelivery.Location.X, cbHomeDelivery.Location.Y + y);
                    HomeDeliveryPanel.Location = new Point(HomeDeliveryPanel.Location.X, HomeDeliveryPanel.Location.Y + y);
                    cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y + y);
                    SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y + y);
                }
            }
            else
            {
                tbDryIceWeight.Text = "";
                cbDryIceUnit.SelectedItem = null;
                DryIcePanel.Visible = false;
                int y = DryIcePanel.Height + 5;
                cbInsideDelivery.Location = new Point(cbInsideDelivery.Location.X, cbInsideDelivery.Location.Y - y);
                cbInsidePickup.Location = new Point(cbInsidePickup.Location.X, cbInsidePickup.Location.Y - y);
                cbSaturdayDelivery.Location = new Point(cbSaturdayDelivery.Location.X, cbSaturdayDelivery.Location.Y - y);
                cbSaturdayPickup.Location = new Point(cbSaturdayPickup.Location.X, cbSaturdayPickup.Location.Y - y);
                cbHomeDelivery.Location = new Point(cbHomeDelivery.Location.X, cbHomeDelivery.Location.Y - y);
                HomeDeliveryPanel.Location = new Point(HomeDeliveryPanel.Location.X, HomeDeliveryPanel.Location.Y - y);
                cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y - y);
                SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y - y);
            }
        }

        private void cbHomeDelivery_CheckedChanged(object sender, EventArgs e)
        {
            if (cbHomeDelivery.Checked)
            {
                HomeDeliveryPanel.Visible = true;
                if (cbSignatureOption.Location.Y < cbHomeDelivery.Location.Y + HomeDeliveryPanel.Height)
                {
                    int y = HomeDeliveryPanel.Height + 5;
                    cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y + y);
                    SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y + y);
                }
            }
            else
            {
                cbHomeDeliveryType.SelectedItem = null;
                tbHomeDeliveryPhone.Text = "";
                dtpHomeDeliveryDate.Value = DateTime.Today;
                HomeDeliveryPanel.Visible = false;
                int y = HomeDeliveryPanel.Height + 5;
                cbSignatureOption.Location = new Point(cbSignatureOption.Location.X, cbSignatureOption.Location.Y - y);
                SignatureOptionPanel.Location = new Point(SignatureOptionPanel.Location.X, SignatureOptionPanel.Location.Y - y);
            
            }
        }

        private void cbSignatureOption_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSignatureOption.Checked)
            {
                SignatureOptionPanel.Visible = true;
            }
            else
            {
                cbSignatureRequired.SelectedItem = null;
                tbReleaseNumber.Text = "";
                SignatureOptionPanel.Visible = false;
            }
        }

        private void cbExpressCheque_CheckedChanged(object sender, EventArgs e)
        {
            if (cbExpressCheque.Checked)
            {
                pExpressCheque.Visible = true;
                if (cbHoldForPickUp.Location.Y < (cbExpressCheque.Location.Y + pExpressCheque.Height)) 
                {
                    int y = pExpressCheque.Height + 5;
                    cbHoldForPickUp.Location = new Point(cbHoldForPickUp.Location.X, cbHoldForPickUp.Location.Y + y);
                    cbPuroSaturdayDelivery.Location = new Point(cbPuroSaturdayDelivery.Location.X, cbPuroSaturdayDelivery.Location.Y + y);
                    cbPuroSaturdayPickup.Location = new Point(cbPuroSaturdayPickup.Location.X, cbPuroSaturdayPickup.Location.Y + y);
                    cbPuroExceptionHandling.Location = new Point(cbPuroExceptionHandling.Location.X, cbPuroExceptionHandling.Location.Y + y);
                    cbChainOfSignature.Location = new Point(cbChainOfSignature.Location.X, cbChainOfSignature.Location.Y + y);
                    cbPuroDangGoods.Location = new Point(cbPuroDangGoods.Location.X, cbPuroDangGoods.Location.Y + y);
                    pPuroDangGoods.Location = new Point(pPuroDangGoods.Location.X, pPuroDangGoods.Location.Y + y);
                }
            }
            else
            {
                pExpressCheque.Visible = false;
                //when the option is unselected, clear the details fields values
                tbAmount.Text = "";
                cbMethodOfPayment.SelectedIndex = 0;
                tbName.Text = "";
                tbCompany.Text = "";
                tbDepartment.Text = "";
                cbPuroCountry.SelectedIndex = 0;
                tbPuroCity.Text = "";
                tbPuroAddress1.Text = "";
                tbPuroAddress2.Text = "";
                tbPuroZIP.Text = "";
                tbPuroEmail.Text = "";
                tbPuroPhone.Text = "";

                int y = pExpressCheque.Height + 5;
                cbHoldForPickUp.Location = new Point(cbHoldForPickUp.Location.X, cbHoldForPickUp.Location.Y - y);
                cbPuroSaturdayDelivery.Location = new Point(cbPuroSaturdayDelivery.Location.X, cbPuroSaturdayDelivery.Location.Y - y);
                cbPuroSaturdayPickup.Location = new Point(cbPuroSaturdayPickup.Location.X, cbPuroSaturdayPickup.Location.Y - y);
                cbPuroExceptionHandling.Location = new Point(cbPuroExceptionHandling.Location.X, cbPuroExceptionHandling.Location.Y - y);
                cbChainOfSignature.Location = new Point(cbChainOfSignature.Location.X, cbChainOfSignature.Location.Y - y);
                cbPuroDangGoods.Location = new Point(cbPuroDangGoods.Location.X, cbPuroDangGoods.Location.Y - y);
                pPuroDangGoods.Location = new Point(pPuroDangGoods.Location.X, pPuroDangGoods.Location.Y - y);
        
            }
        }

        private void cbLoomisReturnCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (cbLoomisReturnCheck.Checked)
            {
                pLoomisReturnCheck.Visible = true;
                if (cbLoomisSaturdayDelivery.Location.Y < cbLoomisReturnCheck.Location.Y + pLoomisReturnCheck.Height)
                {
                    int y = pLoomisReturnCheck.Height + 5;
                    cbLoomisSaturdayDelivery.Location = new Point(cbLoomisSaturdayDelivery.Location.X, cbLoomisSaturdayDelivery.Location.Y + y);
                    cbLoomisNoSignatureRequired.Location = new Point(cbLoomisNoSignatureRequired.Location.X, cbLoomisNoSignatureRequired.Location.Y + y);
                    labelAdditionalInfo.Location = new Point(labelAdditionalInfo.Location.X, labelAdditionalInfo.Location.Y + y);
                    labelAdditionalInfo1.Location = new Point(labelAdditionalInfo1.Location.X, labelAdditionalInfo1.Location.Y + y);
                    tbLoomisAdditionalInfo1.Location = new Point(tbLoomisAdditionalInfo1.Location.X, tbLoomisAdditionalInfo1.Location.Y + y);
                    labelAdditionalInfo2.Location = new Point(labelAdditionalInfo2.Location.X, labelAdditionalInfo2.Location.Y + y);
                    tbLoomisAdditionalInfo2.Location = new Point(tbLoomisAdditionalInfo2.Location.X, tbLoomisAdditionalInfo2.Location.Y + y);
                    labelInstructions.Location = new Point(labelInstructions.Location.X, labelInstructions.Location.Y + y);
                    rtbLoomisInstructions.Location = new Point(rtbLoomisInstructions.Location.X, rtbLoomisInstructions.Location.Y + y);
                }
            }
            else
            {
                tbLoomisAmount.Text = "";
                cbLoomisMethodOfPayment.SelectedIndex = 0;
                pLoomisReturnCheck.Visible = false;
                int y = pLoomisReturnCheck.Height + 5;
                cbLoomisSaturdayDelivery.Location = new Point(cbLoomisSaturdayDelivery.Location.X, cbLoomisSaturdayDelivery.Location.Y - y);
                cbLoomisNoSignatureRequired.Location = new Point(cbLoomisNoSignatureRequired.Location.X, cbLoomisNoSignatureRequired.Location.Y - y);
                labelAdditionalInfo.Location = new Point(labelAdditionalInfo.Location.X, labelAdditionalInfo.Location.Y - y);
                labelAdditionalInfo1.Location = new Point(labelAdditionalInfo1.Location.X, labelAdditionalInfo1.Location.Y - y);
                tbLoomisAdditionalInfo1.Location = new Point(tbLoomisAdditionalInfo1.Location.X, tbLoomisAdditionalInfo1.Location.Y - y);
                labelAdditionalInfo2.Location = new Point(labelAdditionalInfo2.Location.X, labelAdditionalInfo2.Location.Y - y);
                tbLoomisAdditionalInfo2.Location = new Point(tbLoomisAdditionalInfo2.Location.X, tbLoomisAdditionalInfo2.Location.Y - y);
                labelInstructions.Location = new Point(labelInstructions.Location.X, labelInstructions.Location.Y - y);
                rtbLoomisInstructions.Location = new Point(rtbLoomisInstructions.Location.X, rtbLoomisInstructions.Location.Y - y);
            }
        }

        private void cbUPSDeliveryConfirmation_CheckedChanged(object sender, EventArgs e)
        {
            if (cbUPSDeliveryConfirmation.Checked)
            {
                pUPSDeliveryConfirmation.Visible = true;
                if (cbUPSLargePackage.Location.Y < cbUPSDeliveryConfirmation.Location.Y + pUPSDeliveryConfirmation.Height)
                {
                    int y = pUPSDeliveryConfirmation.Height + 5;
                    cbUPSLargePackage.Location = new Point(cbUPSLargePackage.Location.X, cbUPSLargePackage.Location.Y + y);
                }
            }
            else
            {
                cbUPSDeliveryConfirmationType.SelectedIndex = 0;
                pUPSDeliveryConfirmation.Visible = false;
                int y = pUPSDeliveryConfirmation.Height + 5;
                cbUPSLargePackage.Location = new Point(cbUPSLargePackage.Location.X, cbUPSLargePackage.Location.Y - y);
            }
        }

        private void cbCAPostCOD_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCAPostCOD.Checked)
            {
                pCAPostCOD.Visible = true;
                if (labelCAPostNonDeliveryHandling.Location.Y < cbCAPostCOD.Location.Y + pCAPostCOD.Height)
                {
                    int y = pCAPostCOD.Height;
                    labelCAPostNonDeliveryHandling.Location = new Point(labelCAPostNonDeliveryHandling.Location.X, labelCAPostNonDeliveryHandling.Location.Y + y);
                    cbCaPostCardForPickup.Location = new Point(cbCaPostCardForPickup.Location.X, cbCaPostCardForPickup.Location.Y + y);
                    cbCAPostDoNotSafeDrop.Location = new Point(cbCAPostDoNotSafeDrop.Location.X, cbCAPostDoNotSafeDrop.Location.Y + y);
                    cbCAPostLeaveAtDoor.Location = new Point(cbCAPostLeaveAtDoor.Location.X, cbCAPostLeaveAtDoor.Location.Y + y);
                }
            }
            else
            {
                tbCAPostAmount.Text = "";
                cbCAPostMethodOfPayment.SelectedIndex = 0;
                tbCAPostCODName.Text = "";
                cbCAPostCODCountry.SelectedIndex = 0;
                tbCAPostCODCompany.Text = "";
                tbCAPostCODCity.Text = "";
                tbCAPostCODAddress1.Text = "";
                tbCAPostCODAddress2.Text = "";
                tbCAPostCODZIP.Text = "";
                tbCAPostCODEmail.Text = "";
                tbCAPostCODPhone.Text = "";
                pCAPostCOD.Visible = false;
                int y = pCAPostCOD.Height;
                labelCAPostNonDeliveryHandling.Location = new Point(labelCAPostNonDeliveryHandling.Location.X, labelCAPostNonDeliveryHandling.Location.Y - y);
                cbCaPostCardForPickup.Location = new Point(cbCaPostCardForPickup.Location.X, cbCaPostCardForPickup.Location.Y - y);
                cbCAPostDoNotSafeDrop.Location = new Point(cbCAPostDoNotSafeDrop.Location.X, cbCAPostDoNotSafeDrop.Location.Y - y);
                cbCAPostLeaveAtDoor.Location = new Point(cbCAPostLeaveAtDoor.Location.X, cbCAPostLeaveAtDoor.Location.Y - y);
            }
        }

        private void cbCanparCOD_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCanparCOD.Checked)
            {
                labelCanparCODAmount.Visible = true;
                tbCanparCODAmount.Visible = true;
            }
            else
            {
                labelCanparCODAmount.Visible = false;
                tbCanparCODAmount.Visible = false;
            }
        }

        private void cbPackageDryIce_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPackageDryIce.Checked)
            {
                pDryIcePackageLevel.Visible = true;
                int y = pDryIcePackageLevel.Height;
                cbPackageNonStandardContainer.Location = new Point(cbPackageNonStandardContainer.Location.X, cbPackageNonStandardContainer.Location.Y + y);
            }
            else
            {
                pDryIcePackageLevel.Visible = false;
                int y = pDryIcePackageLevel.Height;
                cbPackageNonStandardContainer.Location = new Point(cbPackageNonStandardContainer.Location.X, cbPackageNonStandardContainer.Location.Y - y);
            }
        }

        private void cbUPSPackageCOD_CheckedChanged(object sender, EventArgs e)
        {
            if (cbUPSPackageCOD.Checked)
            {
                pUPSPackageCOD.Visible = true;
                int y = pUPSPackageCOD.Height;
                cbUPSPackageDeliveryConfirmation.Location = new Point(cbUPSPackageDeliveryConfirmation.Location.X, cbUPSPackageDeliveryConfirmation.Location.Y + y);
                labelUPSPackageConfirmationType.Location = new Point(labelUPSPackageConfirmationType.Location.X, labelUPSPackageConfirmationType.Location.Y + y);
                cbUPSPackageDeliveryConfirmationType.Location = new Point(cbUPSPackageDeliveryConfirmationType.Location.X, cbUPSPackageDeliveryConfirmationType.Location.Y + y);

            }
            else
            {
                pUPSPackageCOD.Visible = false;
                int y = pUPSPackageCOD.Height;
                cbUPSPackageDeliveryConfirmation.Location = new Point(cbUPSPackageDeliveryConfirmation.Location.X, cbUPSPackageDeliveryConfirmation.Location.Y - y);
                labelUPSPackageConfirmationType.Location = new Point(labelUPSPackageConfirmationType.Location.X, labelUPSPackageConfirmationType.Location.Y - y);
                cbUPSPackageDeliveryConfirmationType.Location = new Point(cbUPSPackageDeliveryConfirmationType.Location.X, cbUPSPackageDeliveryConfirmationType.Location.Y - y);

            }
        }

        private void cbUPSPackageDeliveryConfirmation_CheckedChanged(object sender, EventArgs e)
        {
            if (cbUPSPackageDeliveryConfirmation.Checked)
            {
                labelUPSPackageConfirmationType.Visible = true;
                cbUPSPackageDeliveryConfirmationType.Visible = true;
            }
            else
            {
                labelUPSPackageConfirmationType.Visible = false;
                cbUPSPackageDeliveryConfirmationType.Visible = false;
            }
        }

        private void btnSavePackageOption_Click(object sender, EventArgs e)
        {
            string carrierCode = currentOrder["Carrier"].ToString();
            string carrierName = data.Carriers.FindByCode(carrierCode).Name;
            string index = cbPackageIndex.SelectedItem.ToString();
            switch (carrierName)
            {
                case "FedEx":
                    {
                        Data.ShipmentOptionsRow d = null;
                       if(cbPackageDryIce.Checked)
                       {
                        d = data.ShipmentOptions.FindByOptionCode_Index("1134", index);
                        if (d == null)
                        {
                            data.ShipmentOptions.AddShipmentOptionsRow("1134", carrierCode, 
                                (cbPackageDryIce.Checked ? "1" : "0"), index);
                        }
                        else
                        {
                            d["Value"] = cbPackageDryIce.Checked ? "1" : "0";
                        }
                        d = data.ShipmentOptions.FindByOptionCode_Index("1135", index);
                        if (d == null)
                        {
                            data.ShipmentOptions.AddShipmentOptionsRow("1135", carrierCode,
                                tbPackageDryIceWeight.Text, index);
                        }
                        else
                        {
                            d["Value"] = tbPackageDryIceWeight.Text;
                        }
                        d = data.ShipmentOptions.FindByOptionCode_Index("1136", index);
                        if (d == null)
                        {
                            data.ShipmentOptions.AddShipmentOptionsRow("1136", carrierCode,
                                cbPackageDryIceUnit.SelectedItem.ToString(), index);
                        }
                        else
                        {
                            d["Value"] = cbPackageDryIceUnit.SelectedItem.ToString();
                        }
                    }
                        d = data.ShipmentOptions.FindByOptionCode_Index("1140", index);
                        if (d == null)
                        {
                            data.ShipmentOptions.AddShipmentOptionsRow("1140", carrierCode,
                                (cbPackageNonStandardContainer.Checked ? "1" : "0"), index);
                        }
                        else
                        {
                            d["Value"] = cbPackageNonStandardContainer.Checked ? "1" : "0";
                        }
                        break;
                    }
                case "Purolator":
                    {
                        Data.ShipmentOptionsRow d = data.ShipmentOptions.FindByOptionCode_Index("1005", index);
                        if (d == null)
                        {
                            data.ShipmentOptions.AddShipmentOptionsRow("1005", carrierCode,
                                (cbPuroExceptionHandlingPackage.Checked? "1" : "0"), index);
                        }
                        else
                        {
                            d["Value"] = cbPuroExceptionHandlingPackage.Checked ? "1" : "0";
                        }
                        break;
                    }
                case "UPS":
                    {
                        Data.ShipmentOptionsRow d = data.ShipmentOptions.FindByOptionCode_Index("1226", index);
                        if (d == null)
                        {
                            data.ShipmentOptions.AddShipmentOptionsRow("1226", carrierCode,
                                (cbUPSPackageAdditionalHandling.Checked ? "1" : "0"), index);
                        }
                        else
                        {
                            d["Value"] = cbUPSPackageAdditionalHandling.Checked ? "1" : "0";
                        }

                        if (cbUPSPackageCOD.Checked)
                        {

                            d = data.ShipmentOptions.FindByOptionCode_Index("1220", index);
                            if (d == null)
                            {
                                data.ShipmentOptions.AddShipmentOptionsRow("1220", carrierCode,
                                    (cbUPSPackageCOD.Checked ? "1" : "0"), index);
                            }
                            else
                            {
                                d["Value"] = cbUPSPackageCOD.Checked ? "1" : "0";
                            }

                            d = data.ShipmentOptions.FindByOptionCode_Index("1221", index);
                            if (d == null)
                            {
                                data.ShipmentOptions.AddShipmentOptionsRow("1221", carrierCode,
                                    tbUPSPackageCODAmount.Text, index);
                            }
                            else
                            {
                                d["Value"] = tbUPSPackageCODAmount.Text;
                            }
                            d = data.ShipmentOptions.FindByOptionCode_Index("1222", index);
                            if (d == null)
                            {
                                data.ShipmentOptions.AddShipmentOptionsRow("1222", carrierCode,
                                    cbUPSPackageCODCurrency.SelectedItem.ToString(), index);
                            }
                            else
                            {
                                d["Value"] = cbUPSPackageCODCurrency.SelectedItem.ToString();
                            }
                            d = data.ShipmentOptions.FindByOptionCode_Index("1223", index);
                            if (d == null)
                            {
                                data.ShipmentOptions.AddShipmentOptionsRow("1223", carrierCode,
                                    (cbUPSCollectionTypePackage.SelectedItem.ToString().Equals("0 - Check, Cashiers Check or Money Order") ? "0" : "8"), index);
                            }
                            else
                            {
                                d["Value"] = cbUPSCollectionTypePackage.SelectedItem.ToString().Equals("0 - Check, Cashiers Check or Money Order") ? "0" : "8";
                            }
                        }
                        if (cbUPSPackageDeliveryConfirmation.Checked)
                        {
                            d = data.ShipmentOptions.FindByOptionCode_Index("1224", index);
                            if (d == null)
                            {
                                data.ShipmentOptions.AddShipmentOptionsRow("1224", carrierCode,
                                    (cbUPSPackageDeliveryConfirmation.Checked ? "1" : "0"), index);
                            }
                            else
                            {
                                d["Value"] = cbUPSPackageDeliveryConfirmation.Checked ? "1" : "0";
                            }

                            d = data.ShipmentOptions.FindByOptionCode_Index("1225", index);
                            if (d == null)
                            {
                                data.ShipmentOptions.AddShipmentOptionsRow("1225", carrierCode,
                                    cbUPSPackageDeliveryConfirmationType.SelectedItem.ToString(), index);
                            }
                            else
                            {
                                d["Value"] = cbUPSPackageDeliveryConfirmationType.SelectedItem.ToString();
                            }
                        }
                        break;
                    }
            }
        }

        private void cbPackageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPackageIndex.SelectedItem != null)
            {
                string carrierCode = currentOrder["Carrier"].ToString();
                string carrierName = data.Carriers.FindByCode(carrierCode).Name;
                string index = cbPackageIndex.SelectedItem.ToString();
                switch (carrierName)
                {
                    case "FedEx":
                        {
                            Data.ShipmentOptionsRow d = null;
                            if (cbPackageDryIce.Checked)
                            {
                                d = data.ShipmentOptions.FindByOptionCode_Index("1134", index);
                                if (d != null)
                                {
                                    cbPackageDryIce.Checked = d["Value"].ToString().Equals("1") ? true : false;
                                }
                                d = data.ShipmentOptions.FindByOptionCode_Index("1135", index);
                                if (d != null)
                                {
                                    tbPackageDryIceWeight.Text = d["Value"].ToString();
                                }
                              
                                d = data.ShipmentOptions.FindByOptionCode_Index("1136", index);
                                if (d != null)
                                {
                                    if(! String.IsNullOrEmpty(d["Value"].ToString()))
                                    cbPackageDryIceUnit.SelectedItem = d["Value"].ToString();
                                }
                            }
                            d = data.ShipmentOptions.FindByOptionCode_Index("1140", index);
                            if (d != null)
                            {
                                cbPackageNonStandardContainer.Checked = d["Value"].ToString().Equals("1") ? true : false;
                            }
                            break;
                        }
                    case "Purolator":
                        {
                            Data.ShipmentOptionsRow d = data.ShipmentOptions.FindByOptionCode_Index("1005", index);
                            if (d != null)
                            {
                                cbPuroExceptionHandlingPackage.Checked = d["Value"].ToString().Equals("1") ? true: false;
                            }
                            break;
                        }
                    case "UPS":
                        {
                            Data.ShipmentOptionsRow d = data.ShipmentOptions.FindByOptionCode_Index("1226", index);
                            if (d != null)
                            {
                                cbUPSPackageAdditionalHandling.Checked = d["Value"].ToString().Equals("1") ? true : false;
                            }

                                d = data.ShipmentOptions.FindByOptionCode_Index("1220", index);
                                if (d != null)
                                {
                                    cbUPSPackageCOD.Checked = d["Value"].ToString().Equals("1") ? true : false;
                                }
                                d = data.ShipmentOptions.FindByOptionCode_Index("1221", index);
                                if (d != null)
                                {
                                    tbUPSPackageCODAmount.Text = d["Value"].ToString();
                                }
                                d = data.ShipmentOptions.FindByOptionCode_Index("1222", index);
                                if (d != null)
                                {
                                    if (!String.IsNullOrEmpty(d["Value"].ToString()))
                                    {
                                        cbUPSPackageCODCurrency.SelectedItem = d["Value"].ToString();
                                    }
                                }
                                d = data.ShipmentOptions.FindByOptionCode_Index("1223", index);
                                if (d == null)
                                {
                                    if (!String.IsNullOrEmpty(d["Value"].ToString()))
                                    {
                                        cbUPSCollectionTypePackage.SelectedItem = d["Value"].ToString();
                                    }
                                }
                                d = data.ShipmentOptions.FindByOptionCode_Index("1224", index);
                                if (d == null)
                                {
                                    cbUPSDeliveryConfirmation.Checked = d["Value"].ToString().Equals("1") ? true : false;
                                }

                                d = data.ShipmentOptions.FindByOptionCode_Index("1225", index);
                                if (d == null)
                                {
                                    if (!String.IsNullOrEmpty(d["Value"].ToString()))
                                    {
                                        cbUPSDeliveryConfirmationType.SelectedItem = d["Value"].ToString();
                                    }
                                }
                            
                            break;
                        }
                }
            }
        }

        private void cbPuroDangGoods_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPuroDangGoods.Checked)
            {
                pPuroDangGoods.Visible = true;
            }
            else
            {
                pPuroDangGoods.Visible = false;
            }
        }

        private void cbATSTR_CheckedChanged(object sender, EventArgs e)
        {
            if (cbATSTR.Checked)
            {
                cbATSReleaseOptions.Visible = true;
                dtpATSEndDate.Visible = true;
                dtpATSStartDate.Visible = true;
            }
            else
            {
                cbATSReleaseOptions.Visible = false;
                dtpATSEndDate.Visible = false;
                dtpATSStartDate.Visible = false;
            }
        }
    }

    public class ListBoxItem
    {
        private string Text = "";
        private string Scac = "";
        private object Value = false;

        
        public override string ToString() 
        { 
            return this.Text + "/" + this.Scac; 
        }

        public string GetText()
        {
            return this.Text;
        }

        public object GetValue()
        {
            return this.Value;
        }

        public string GetScac()
        {
            return this.Scac;
        }

        public void Set(string text, string scac, object value)
        {
            this.Text = text;
            this.Scac = scac;
            this.Value = value;
        }
    }
}
