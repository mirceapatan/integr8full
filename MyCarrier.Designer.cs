﻿namespace InteGr8
{
    partial class MyCarrier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_list_tarrif = new System.Windows.Forms.Label();
            this.label_discount = new System.Windows.Forms.Label();
            this.label_discount_type = new System.Windows.Forms.Label();
            this.label_fuel = new System.Windows.Forms.Label();
            this.label_fuel_type = new System.Windows.Forms.Label();
            this.label_total_other_surcharges = new System.Windows.Forms.Label();
            this.label_tracking_number = new System.Windows.Forms.Label();
            this.label_billing_account = new System.Windows.Forms.Label();
            this.label_bar_code_symbology = new System.Windows.Forms.Label();
            this.label_pro_number = new System.Windows.Forms.Label();
            this.label_package_type = new System.Windows.Forms.Label();
            this.txt_tacking_number = new System.Windows.Forms.TextBox();
            this.txt_pro_number = new System.Windows.Forms.TextBox();
            this.txt_billing_account = new System.Windows.Forms.TextBox();
            this.cb_bare_code_symbology = new System.Windows.Forms.ComboBox();
            this.groupBox_package_type = new System.Windows.Forms.GroupBox();
            this.radiob_same_day = new System.Windows.Forms.RadioButton();
            this.radiob_ltl = new System.Windows.Forms.RadioButton();
            this.radiob_small_package = new System.Windows.Forms.RadioButton();
            this.groupbox_fuel_type = new System.Windows.Forms.GroupBox();
            this.radiob_fuel_type_procent = new System.Windows.Forms.RadioButton();
            this.radiob_fuel_type_diez = new System.Windows.Forms.RadioButton();
            this.groupbox_discount_type = new System.Windows.Forms.GroupBox();
            this.radiob_discount_type_procent = new System.Windows.Forms.RadioButton();
            this.radiob_discount_type_diez = new System.Windows.Forms.RadioButton();
            this.checkbox_reuse = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.txt_list_tarrif = new System.Windows.Forms.TextBox();
            this.txt_discount = new System.Windows.Forms.TextBox();
            this.txt_fuel = new System.Windows.Forms.TextBox();
            this.txt_total_other_surcharges = new System.Windows.Forms.TextBox();
            this.label_bottom = new System.Windows.Forms.Label();
            this.label_carrier_name = new System.Windows.Forms.Label();
            this.txt_service_name = new System.Windows.Forms.TextBox();
            this.label_service_name = new System.Windows.Forms.Label();
            this.label_packaging = new System.Windows.Forms.Label();
            this.txt_packaging = new System.Windows.Forms.TextBox();
            this.link_label_special_services = new System.Windows.Forms.LinkLabel();
            this.panel_special_services = new System.Windows.Forms.Panel();
            this.checkBox_tradeshow = new System.Windows.Forms.CheckBox();
            this.textBox_tradeshow = new System.Windows.Forms.TextBox();
            this.checkBox_special_equipment_needed = new System.Windows.Forms.CheckBox();
            this.textBox_special_equipment_needed = new System.Windows.Forms.TextBox();
            this.checkBox_saturday_pickup_or_delivery = new System.Windows.Forms.CheckBox();
            this.textBox_saturday_pickup_or_delivery = new System.Windows.Forms.TextBox();
            this.checkBox_residential_delivery = new System.Windows.Forms.CheckBox();
            this.textBox_residential_delivery = new System.Windows.Forms.TextBox();
            this.checkBox_pallet = new System.Windows.Forms.CheckBox();
            this.textBox_pallet = new System.Windows.Forms.TextBox();
            this.checkBox_premium_air_freight = new System.Windows.Forms.CheckBox();
            this.textBox_premium_air_freight = new System.Windows.Forms.TextBox();
            this.checkBox_marking_and_tagging = new System.Windows.Forms.CheckBox();
            this.textBox_marking_and_tagging = new System.Windows.Forms.TextBox();
            this.checkBox_liquor_permit = new System.Windows.Forms.CheckBox();
            this.textBox_liquor_permit = new System.Windows.Forms.TextBox();
            this.checkBox_lift_gate_origin = new System.Windows.Forms.CheckBox();
            this.textBox_lift_gate_origin = new System.Windows.Forms.TextBox();
            this.checkBox_lift = new System.Windows.Forms.CheckBox();
            this.textBox_lift = new System.Windows.Forms.TextBox();
            this.checkBox_inside_pickup = new System.Windows.Forms.CheckBox();
            this.textBox_inside_pickup = new System.Windows.Forms.TextBox();
            this.checkBox_holiday_delivery = new System.Windows.Forms.CheckBox();
            this.textBox_holiday_delivery = new System.Windows.Forms.TextBox();
            this.checkBox_guaranteed = new System.Windows.Forms.CheckBox();
            this.textBox_guaranteed = new System.Windows.Forms.TextBox();
            this.checkBox_expedited_shipment = new System.Windows.Forms.CheckBox();
            this.textBox_expedited_shipment = new System.Windows.Forms.TextBox();
            this.checkBox_emergency_delivery = new System.Windows.Forms.CheckBox();
            this.textBox_emergency_delivery = new System.Windows.Forms.TextBox();
            this.checkBox_dry_run = new System.Windows.Forms.CheckBox();
            this.textBox_dry_run = new System.Windows.Forms.TextBox();
            this.checkBox_driver_load = new System.Windows.Forms.CheckBox();
            this.textBox_driver_load = new System.Windows.Forms.TextBox();
            this.checkBox_driver_load_and_unload = new System.Windows.Forms.CheckBox();
            this.textBox_driver_load_and_unload = new System.Windows.Forms.TextBox();
            this.checkBox_cross_dock = new System.Windows.Forms.CheckBox();
            this.textBox_cross_dock = new System.Windows.Forms.TextBox();
            this.checkBox_commercial_pickup = new System.Windows.Forms.CheckBox();
            this.textBox_commercial_pickup = new System.Windows.Forms.TextBox();
            this.checkBox_bobtail = new System.Windows.Forms.CheckBox();
            this.textBox_bobtail = new System.Windows.Forms.TextBox();
            this.checkBox_bag_liner = new System.Windows.Forms.CheckBox();
            this.textBox_bag_liner = new System.Windows.Forms.TextBox();
            this.checkBox_appointment = new System.Windows.Forms.CheckBox();
            this.textBox_appointment = new System.Windows.Forms.TextBox();
            this.checkBox_live_loading_or_unloading = new System.Windows.Forms.CheckBox();
            this.textBox_live_loading_or_unloading = new System.Windows.Forms.TextBox();
            this.checkBox_team_required = new System.Windows.Forms.CheckBox();
            this.textBox_team_required = new System.Windows.Forms.TextBox();
            this.checkBox_stop_off = new System.Windows.Forms.CheckBox();
            this.textBox_stop_off = new System.Windows.Forms.TextBox();
            this.checkBox_residential_pickup = new System.Windows.Forms.CheckBox();
            this.textBox_residential_pickup = new System.Windows.Forms.TextBox();
            this.checkBox_protective = new System.Windows.Forms.CheckBox();
            this.textBox_protective = new System.Windows.Forms.TextBox();
            this.checkBox_notification = new System.Windows.Forms.CheckBox();
            this.textBox_notification = new System.Windows.Forms.TextBox();
            this.checkBox_limited_delivery_or_military = new System.Windows.Forms.CheckBox();
            this.textBox_limited_delivery_or_military = new System.Windows.Forms.TextBox();
            this.checkBox_lift_gate_destination = new System.Windows.Forms.CheckBox();
            this.textBox_lift_gate_destination = new System.Windows.Forms.TextBox();
            this.checkBox_limited_access_delivery = new System.Windows.Forms.CheckBox();
            this.textBox_limited_access_delivery = new System.Windows.Forms.TextBox();
            this.checkBox_inside_delivery = new System.Windows.Forms.CheckBox();
            this.textBox_inside_delivery = new System.Windows.Forms.TextBox();
            this.checkBox_handling_and_labeling = new System.Windows.Forms.CheckBox();
            this.textBox_handling_and_labeling = new System.Windows.Forms.TextBox();
            this.checkBox_forklift = new System.Windows.Forms.CheckBox();
            this.textBox_forklift = new System.Windows.Forms.TextBox();
            this.checkBox_expedited_same_dy_snc = new System.Windows.Forms.CheckBox();
            this.textBox_expedited_same_dy_snc = new System.Windows.Forms.TextBox();
            this.checkBox_driver_unload_and_count = new System.Windows.Forms.CheckBox();
            this.textBox_driver_unload_and_count = new System.Windows.Forms.TextBox();
            this.checkBox_driver_unload = new System.Windows.Forms.CheckBox();
            this.textBox_driver_unload = new System.Windows.Forms.TextBox();
            this.checkBox_drop_pull = new System.Windows.Forms.CheckBox();
            this.textBox_drop_pull = new System.Windows.Forms.TextBox();
            this.checkBox_driver_load_and_count = new System.Windows.Forms.CheckBox();
            this.textBox_driver_load_and_count = new System.Windows.Forms.TextBox();
            this.checkBox_crane_permits = new System.Windows.Forms.CheckBox();
            this.textBox_crane_permits = new System.Windows.Forms.TextBox();
            this.checkBox_cross_border = new System.Windows.Forms.CheckBox();
            this.textBox_cross_border = new System.Windows.Forms.TextBox();
            this.checkBox_commercial_delivery = new System.Windows.Forms.CheckBox();
            this.textBox_commercial_delivery = new System.Windows.Forms.TextBox();
            this.checkBox_bulkhead = new System.Windows.Forms.CheckBox();
            this.textBox_bulkhead = new System.Windows.Forms.TextBox();
            this.checkBox_air_bag = new System.Windows.Forms.CheckBox();
            this.textBox_air_bag = new System.Windows.Forms.TextBox();
            this.checkBox_after_hours_delivery = new System.Windows.Forms.CheckBox();
            this.textBox_after_hours_delivery = new System.Windows.Forms.TextBox();
            this.checkBox_fuel_surcharge = new System.Windows.Forms.CheckBox();
            this.textBox_fuel_surcharge = new System.Windows.Forms.TextBox();
            this.checkBox_hazmat = new System.Windows.Forms.CheckBox();
            this.textBox_hazmat = new System.Windows.Forms.TextBox();
            this.checkBox_temp_control = new System.Windows.Forms.CheckBox();
            this.textBox_temp_control = new System.Windows.Forms.TextBox();
            this.checkBox_reefer = new System.Windows.Forms.CheckBox();
            this.textBox_reefer = new System.Windows.Forms.TextBox();
            this.checkBox_heat = new System.Windows.Forms.CheckBox();
            this.textBox_heat = new System.Windows.Forms.TextBox();
            this.checkBox_pallet_exchange = new System.Windows.Forms.CheckBox();
            this.textBox_pallet_exchange = new System.Windows.Forms.TextBox();
            this.checkBox_driver_assistance = new System.Windows.Forms.CheckBox();
            this.textBox_driver_assistance = new System.Windows.Forms.TextBox();
            this.checkBox_lumper = new System.Windows.Forms.CheckBox();
            this.textBox_lumper = new System.Windows.Forms.TextBox();
            this.checkBox_tailgate_charge = new System.Windows.Forms.CheckBox();
            this.textBox_tailgate_charge = new System.Windows.Forms.TextBox();
            this.checkBox_delivery_appointment = new System.Windows.Forms.CheckBox();
            this.textBox_delivery_appointment = new System.Windows.Forms.TextBox();
            this.checkBox_pickup_appointment = new System.Windows.Forms.CheckBox();
            this.textBox_pickup_appointment = new System.Windows.Forms.TextBox();
            this.checkBox_guaranteed_delivery = new System.Windows.Forms.CheckBox();
            this.textBox_guaranteed_delivery = new System.Windows.Forms.TextBox();
            this.cb_carrier_name = new System.Windows.Forms.ComboBox();
            this.label_scac = new System.Windows.Forms.Label();
            this.txt_scac = new System.Windows.Forms.TextBox();
            this.groupBox_package_type.SuspendLayout();
            this.groupbox_fuel_type.SuspendLayout();
            this.groupbox_discount_type.SuspendLayout();
            this.panel_special_services.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_list_tarrif
            // 
            this.label_list_tarrif.AutoSize = true;
            this.label_list_tarrif.Location = new System.Drawing.Point(106, 670);
            this.label_list_tarrif.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_list_tarrif.Name = "label_list_tarrif";
            this.label_list_tarrif.Size = new System.Drawing.Size(68, 17);
            this.label_list_tarrif.TabIndex = 1;
            this.label_list_tarrif.Text = "List Tarrif";
            // 
            // label_discount
            // 
            this.label_discount.AutoSize = true;
            this.label_discount.Location = new System.Drawing.Point(109, 711);
            this.label_discount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_discount.Name = "label_discount";
            this.label_discount.Size = new System.Drawing.Size(63, 17);
            this.label_discount.TabIndex = 2;
            this.label_discount.Text = "Discount";
            // 
            // label_discount_type
            // 
            this.label_discount_type.AutoSize = true;
            this.label_discount_type.Location = new System.Drawing.Point(91, 754);
            this.label_discount_type.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_discount_type.Name = "label_discount_type";
            this.label_discount_type.Size = new System.Drawing.Size(99, 17);
            this.label_discount_type.TabIndex = 3;
            this.label_discount_type.Text = "Discount Type";
            // 
            // label_fuel
            // 
            this.label_fuel.AutoSize = true;
            this.label_fuel.Location = new System.Drawing.Point(123, 795);
            this.label_fuel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_fuel.Name = "label_fuel";
            this.label_fuel.Size = new System.Drawing.Size(35, 17);
            this.label_fuel.TabIndex = 4;
            this.label_fuel.Text = "Fuel";
            // 
            // label_fuel_type
            // 
            this.label_fuel_type.AutoSize = true;
            this.label_fuel_type.Location = new System.Drawing.Point(105, 834);
            this.label_fuel_type.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_fuel_type.Name = "label_fuel_type";
            this.label_fuel_type.Size = new System.Drawing.Size(71, 17);
            this.label_fuel_type.TabIndex = 5;
            this.label_fuel_type.Text = "Fuel Type";
            // 
            // label_total_other_surcharges
            // 
            this.label_total_other_surcharges.AutoSize = true;
            this.label_total_other_surcharges.Location = new System.Drawing.Point(60, 884);
            this.label_total_other_surcharges.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_total_other_surcharges.Name = "label_total_other_surcharges";
            this.label_total_other_surcharges.Size = new System.Drawing.Size(161, 17);
            this.label_total_other_surcharges.TabIndex = 6;
            this.label_total_other_surcharges.Text = "Total Other Surcharges ";
            // 
            // label_tracking_number
            // 
            this.label_tracking_number.AutoSize = true;
            this.label_tracking_number.Location = new System.Drawing.Point(91, 157);
            this.label_tracking_number.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_tracking_number.Name = "label_tracking_number";
            this.label_tracking_number.Size = new System.Drawing.Size(75, 17);
            this.label_tracking_number.TabIndex = 7;
            this.label_tracking_number.Text = "Tracking #";
            // 
            // label_billing_account
            // 
            this.label_billing_account.AutoSize = true;
            this.label_billing_account.Location = new System.Drawing.Point(86, 473);
            this.label_billing_account.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_billing_account.Name = "label_billing_account";
            this.label_billing_account.Size = new System.Drawing.Size(109, 17);
            this.label_billing_account.TabIndex = 8;
            this.label_billing_account.Text = "Bill to Account #";
            // 
            // label_bar_code_symbology
            // 
            this.label_bar_code_symbology.AutoSize = true;
            this.label_bar_code_symbology.Location = new System.Drawing.Point(42, 926);
            this.label_bar_code_symbology.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_bar_code_symbology.Name = "label_bar_code_symbology";
            this.label_bar_code_symbology.Size = new System.Drawing.Size(197, 17);
            this.label_bar_code_symbology.TabIndex = 9;
            this.label_bar_code_symbology.Text = "Bar Code symbology on Label";
            // 
            // label_pro_number
            // 
            this.label_pro_number.AutoSize = true;
            this.label_pro_number.Location = new System.Drawing.Point(109, 158);
            this.label_pro_number.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_pro_number.Name = "label_pro_number";
            this.label_pro_number.Size = new System.Drawing.Size(42, 17);
            this.label_pro_number.TabIndex = 10;
            this.label_pro_number.Text = "Pro #";
            // 
            // label_package_type
            // 
            this.label_package_type.AutoSize = true;
            this.label_package_type.Location = new System.Drawing.Point(84, 85);
            this.label_package_type.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_package_type.Name = "label_package_type";
            this.label_package_type.Size = new System.Drawing.Size(99, 17);
            this.label_package_type.TabIndex = 11;
            this.label_package_type.Text = "Package Type";
            // 
            // txt_tacking_number
            // 
            this.txt_tacking_number.Location = new System.Drawing.Point(256, 153);
            this.txt_tacking_number.Margin = new System.Windows.Forms.Padding(4);
            this.txt_tacking_number.Name = "txt_tacking_number";
            this.txt_tacking_number.Size = new System.Drawing.Size(265, 22);
            this.txt_tacking_number.TabIndex = 17;
            // 
            // txt_pro_number
            // 
            this.txt_pro_number.Location = new System.Drawing.Point(256, 153);
            this.txt_pro_number.Margin = new System.Windows.Forms.Padding(4);
            this.txt_pro_number.Name = "txt_pro_number";
            this.txt_pro_number.Size = new System.Drawing.Size(265, 22);
            this.txt_pro_number.TabIndex = 18;
            // 
            // txt_billing_account
            // 
            this.txt_billing_account.Location = new System.Drawing.Point(256, 471);
            this.txt_billing_account.Margin = new System.Windows.Forms.Padding(4);
            this.txt_billing_account.Name = "txt_billing_account";
            this.txt_billing_account.Size = new System.Drawing.Size(265, 22);
            this.txt_billing_account.TabIndex = 19;
            // 
            // cb_bare_code_symbology
            // 
            this.cb_bare_code_symbology.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_bare_code_symbology.FormattingEnabled = true;
            this.cb_bare_code_symbology.Location = new System.Drawing.Point(256, 922);
            this.cb_bare_code_symbology.Margin = new System.Windows.Forms.Padding(4);
            this.cb_bare_code_symbology.Name = "cb_bare_code_symbology";
            this.cb_bare_code_symbology.Size = new System.Drawing.Size(265, 24);
            this.cb_bare_code_symbology.TabIndex = 20;
            // 
            // groupBox_package_type
            // 
            this.groupBox_package_type.Controls.Add(this.radiob_same_day);
            this.groupBox_package_type.Controls.Add(this.radiob_ltl);
            this.groupBox_package_type.Controls.Add(this.radiob_small_package);
            this.groupBox_package_type.Location = new System.Drawing.Point(256, 32);
            this.groupBox_package_type.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox_package_type.Name = "groupBox_package_type";
            this.groupBox_package_type.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox_package_type.Size = new System.Drawing.Size(265, 110);
            this.groupBox_package_type.TabIndex = 21;
            this.groupBox_package_type.TabStop = false;
            // 
            // radiob_same_day
            // 
            this.radiob_same_day.AutoSize = true;
            this.radiob_same_day.Location = new System.Drawing.Point(10, 20);
            this.radiob_same_day.Margin = new System.Windows.Forms.Padding(4);
            this.radiob_same_day.Name = "radiob_same_day";
            this.radiob_same_day.Size = new System.Drawing.Size(157, 21);
            this.radiob_same_day.TabIndex = 2;
            this.radiob_same_day.Text = "Same Day Shipment";
            this.radiob_same_day.UseVisualStyleBackColor = true;
            // 
            // radiob_ltl
            // 
            this.radiob_ltl.AutoSize = true;
            this.radiob_ltl.Checked = true;
            this.radiob_ltl.Location = new System.Drawing.Point(10, 79);
            this.radiob_ltl.Margin = new System.Windows.Forms.Padding(4);
            this.radiob_ltl.Name = "radiob_ltl";
            this.radiob_ltl.Size = new System.Drawing.Size(117, 21);
            this.radiob_ltl.TabIndex = 1;
            this.radiob_ltl.TabStop = true;
            this.radiob_ltl.Text = "LTL Shipment";
            this.radiob_ltl.UseVisualStyleBackColor = true;
            this.radiob_ltl.CheckedChanged += new System.EventHandler(this.radiob_ltl_CheckedChanged);
            // 
            // radiob_small_package
            // 
            this.radiob_small_package.AutoSize = true;
            this.radiob_small_package.Location = new System.Drawing.Point(10, 49);
            this.radiob_small_package.Margin = new System.Windows.Forms.Padding(4);
            this.radiob_small_package.Name = "radiob_small_package";
            this.radiob_small_package.Size = new System.Drawing.Size(185, 21);
            this.radiob_small_package.TabIndex = 0;
            this.radiob_small_package.Text = "Small Package Shipment";
            this.radiob_small_package.UseVisualStyleBackColor = true;
            // 
            // groupbox_fuel_type
            // 
            this.groupbox_fuel_type.Controls.Add(this.radiob_fuel_type_procent);
            this.groupbox_fuel_type.Controls.Add(this.radiob_fuel_type_diez);
            this.groupbox_fuel_type.Location = new System.Drawing.Point(256, 819);
            this.groupbox_fuel_type.Margin = new System.Windows.Forms.Padding(4);
            this.groupbox_fuel_type.Name = "groupbox_fuel_type";
            this.groupbox_fuel_type.Padding = new System.Windows.Forms.Padding(4);
            this.groupbox_fuel_type.Size = new System.Drawing.Size(267, 41);
            this.groupbox_fuel_type.TabIndex = 22;
            this.groupbox_fuel_type.TabStop = false;
            // 
            // radiob_fuel_type_procent
            // 
            this.radiob_fuel_type_procent.AutoSize = true;
            this.radiob_fuel_type_procent.Checked = true;
            this.radiob_fuel_type_procent.Location = new System.Drawing.Point(148, 14);
            this.radiob_fuel_type_procent.Margin = new System.Windows.Forms.Padding(4);
            this.radiob_fuel_type_procent.Name = "radiob_fuel_type_procent";
            this.radiob_fuel_type_procent.Size = new System.Drawing.Size(41, 21);
            this.radiob_fuel_type_procent.TabIndex = 1;
            this.radiob_fuel_type_procent.TabStop = true;
            this.radiob_fuel_type_procent.Text = "%";
            this.radiob_fuel_type_procent.UseVisualStyleBackColor = true;
            // 
            // radiob_fuel_type_diez
            // 
            this.radiob_fuel_type_diez.AutoSize = true;
            this.radiob_fuel_type_diez.Location = new System.Drawing.Point(11, 12);
            this.radiob_fuel_type_diez.Margin = new System.Windows.Forms.Padding(4);
            this.radiob_fuel_type_diez.Name = "radiob_fuel_type_diez";
            this.radiob_fuel_type_diez.Size = new System.Drawing.Size(37, 21);
            this.radiob_fuel_type_diez.TabIndex = 0;
            this.radiob_fuel_type_diez.Text = "#";
            this.radiob_fuel_type_diez.UseVisualStyleBackColor = true;
            // 
            // groupbox_discount_type
            // 
            this.groupbox_discount_type.Controls.Add(this.radiob_discount_type_procent);
            this.groupbox_discount_type.Controls.Add(this.radiob_discount_type_diez);
            this.groupbox_discount_type.Location = new System.Drawing.Point(256, 739);
            this.groupbox_discount_type.Margin = new System.Windows.Forms.Padding(4);
            this.groupbox_discount_type.Name = "groupbox_discount_type";
            this.groupbox_discount_type.Padding = new System.Windows.Forms.Padding(4);
            this.groupbox_discount_type.Size = new System.Drawing.Size(267, 41);
            this.groupbox_discount_type.TabIndex = 23;
            this.groupbox_discount_type.TabStop = false;
            // 
            // radiob_discount_type_procent
            // 
            this.radiob_discount_type_procent.AutoSize = true;
            this.radiob_discount_type_procent.Checked = true;
            this.radiob_discount_type_procent.Location = new System.Drawing.Point(148, 12);
            this.radiob_discount_type_procent.Margin = new System.Windows.Forms.Padding(4);
            this.radiob_discount_type_procent.Name = "radiob_discount_type_procent";
            this.radiob_discount_type_procent.Size = new System.Drawing.Size(41, 21);
            this.radiob_discount_type_procent.TabIndex = 1;
            this.radiob_discount_type_procent.TabStop = true;
            this.radiob_discount_type_procent.Text = "%";
            this.radiob_discount_type_procent.UseVisualStyleBackColor = true;
            // 
            // radiob_discount_type_diez
            // 
            this.radiob_discount_type_diez.AutoSize = true;
            this.radiob_discount_type_diez.Location = new System.Drawing.Point(11, 12);
            this.radiob_discount_type_diez.Margin = new System.Windows.Forms.Padding(4);
            this.radiob_discount_type_diez.Name = "radiob_discount_type_diez";
            this.radiob_discount_type_diez.Size = new System.Drawing.Size(37, 21);
            this.radiob_discount_type_diez.TabIndex = 0;
            this.radiob_discount_type_diez.Text = "#";
            this.radiob_discount_type_diez.UseVisualStyleBackColor = true;
            // 
            // checkbox_reuse
            // 
            this.checkbox_reuse.AutoSize = true;
            this.checkbox_reuse.Location = new System.Drawing.Point(60, 972);
            this.checkbox_reuse.Margin = new System.Windows.Forms.Padding(4);
            this.checkbox_reuse.Name = "checkbox_reuse";
            this.checkbox_reuse.Size = new System.Drawing.Size(139, 21);
            this.checkbox_reuse.TabIndex = 24;
            this.checkbox_reuse.Text = "Use for all orders";
            this.checkbox_reuse.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 17);
            this.label1.TabIndex = 25;
            this.label1.Text = "This information is required for \"My Carrier\":";
            // 
            // btn_cancel
            // 
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Location = new System.Drawing.Point(423, 967);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(4);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(100, 28);
            this.btn_cancel.TabIndex = 26;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_ok.Location = new System.Drawing.Point(315, 967);
            this.btn_ok.Margin = new System.Windows.Forms.Padding(4);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(100, 28);
            this.btn_ok.TabIndex = 27;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // txt_list_tarrif
            // 
            this.txt_list_tarrif.Location = new System.Drawing.Point(256, 669);
            this.txt_list_tarrif.Margin = new System.Windows.Forms.Padding(4);
            this.txt_list_tarrif.Name = "txt_list_tarrif";
            this.txt_list_tarrif.Size = new System.Drawing.Size(265, 22);
            this.txt_list_tarrif.TabIndex = 28;
            this.txt_list_tarrif.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_list_tarrif_KeyPress);
            // 
            // txt_discount
            // 
            this.txt_discount.Location = new System.Drawing.Point(256, 708);
            this.txt_discount.Margin = new System.Windows.Forms.Padding(4);
            this.txt_discount.Name = "txt_discount";
            this.txt_discount.Size = new System.Drawing.Size(265, 22);
            this.txt_discount.TabIndex = 29;
            this.txt_discount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_discount_KeyPress);
            // 
            // txt_fuel
            // 
            this.txt_fuel.Location = new System.Drawing.Point(256, 792);
            this.txt_fuel.Margin = new System.Windows.Forms.Padding(4);
            this.txt_fuel.Name = "txt_fuel";
            this.txt_fuel.Size = new System.Drawing.Size(265, 22);
            this.txt_fuel.TabIndex = 30;
            this.txt_fuel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_fuel_KeyPress);
            // 
            // txt_total_other_surcharges
            // 
            this.txt_total_other_surcharges.Location = new System.Drawing.Point(256, 880);
            this.txt_total_other_surcharges.Margin = new System.Windows.Forms.Padding(4);
            this.txt_total_other_surcharges.Name = "txt_total_other_surcharges";
            this.txt_total_other_surcharges.Size = new System.Drawing.Size(265, 22);
            this.txt_total_other_surcharges.TabIndex = 31;
            this.txt_total_other_surcharges.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_total_other_surcharges_KeyPress);
            // 
            // label_bottom
            // 
            this.label_bottom.AutoSize = true;
            this.label_bottom.Location = new System.Drawing.Point(268, 1002);
            this.label_bottom.Name = "label_bottom";
            this.label_bottom.Size = new System.Drawing.Size(0, 17);
            this.label_bottom.TabIndex = 32;
            // 
            // label_carrier_name
            // 
            this.label_carrier_name.AutoSize = true;
            this.label_carrier_name.Location = new System.Drawing.Point(94, 516);
            this.label_carrier_name.Name = "label_carrier_name";
            this.label_carrier_name.Size = new System.Drawing.Size(92, 17);
            this.label_carrier_name.TabIndex = 33;
            this.label_carrier_name.Text = "Carrier Name";
            // 
            // txt_service_name
            // 
            this.txt_service_name.Location = new System.Drawing.Point(256, 593);
            this.txt_service_name.Name = "txt_service_name";
            this.txt_service_name.Size = new System.Drawing.Size(265, 22);
            this.txt_service_name.TabIndex = 35;
            this.txt_service_name.Text = "My Service";
            // 
            // label_service_name
            // 
            this.label_service_name.AutoSize = true;
            this.label_service_name.Location = new System.Drawing.Point(92, 596);
            this.label_service_name.Name = "label_service_name";
            this.label_service_name.Size = new System.Drawing.Size(96, 17);
            this.label_service_name.TabIndex = 36;
            this.label_service_name.Text = "Service Name";
            // 
            // label_packaging
            // 
            this.label_packaging.AutoSize = true;
            this.label_packaging.Location = new System.Drawing.Point(103, 632);
            this.label_packaging.Name = "label_packaging";
            this.label_packaging.Size = new System.Drawing.Size(74, 17);
            this.label_packaging.TabIndex = 37;
            this.label_packaging.Text = "Packaging";
            // 
            // txt_packaging
            // 
            this.txt_packaging.Location = new System.Drawing.Point(256, 630);
            this.txt_packaging.Name = "txt_packaging";
            this.txt_packaging.Size = new System.Drawing.Size(265, 22);
            this.txt_packaging.TabIndex = 38;
            this.txt_packaging.Text = "My Package";
            // 
            // link_label_special_services
            // 
            this.link_label_special_services.AutoSize = true;
            this.link_label_special_services.Location = new System.Drawing.Point(83, 181);
            this.link_label_special_services.Name = "link_label_special_services";
            this.link_label_special_services.Size = new System.Drawing.Size(112, 17);
            this.link_label_special_services.TabIndex = 39;
            this.link_label_special_services.TabStop = true;
            this.link_label_special_services.Text = "Special Services";
            this.link_label_special_services.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_label_special_services_LinkClicked);
            // 
            // panel_special_services
            // 
            this.panel_special_services.AutoScroll = true;
            this.panel_special_services.Controls.Add(this.checkBox_tradeshow);
            this.panel_special_services.Controls.Add(this.textBox_tradeshow);
            this.panel_special_services.Controls.Add(this.checkBox_special_equipment_needed);
            this.panel_special_services.Controls.Add(this.textBox_special_equipment_needed);
            this.panel_special_services.Controls.Add(this.checkBox_saturday_pickup_or_delivery);
            this.panel_special_services.Controls.Add(this.textBox_saturday_pickup_or_delivery);
            this.panel_special_services.Controls.Add(this.checkBox_residential_delivery);
            this.panel_special_services.Controls.Add(this.textBox_residential_delivery);
            this.panel_special_services.Controls.Add(this.checkBox_pallet);
            this.panel_special_services.Controls.Add(this.textBox_pallet);
            this.panel_special_services.Controls.Add(this.checkBox_premium_air_freight);
            this.panel_special_services.Controls.Add(this.textBox_premium_air_freight);
            this.panel_special_services.Controls.Add(this.checkBox_marking_and_tagging);
            this.panel_special_services.Controls.Add(this.textBox_marking_and_tagging);
            this.panel_special_services.Controls.Add(this.checkBox_liquor_permit);
            this.panel_special_services.Controls.Add(this.textBox_liquor_permit);
            this.panel_special_services.Controls.Add(this.checkBox_lift_gate_origin);
            this.panel_special_services.Controls.Add(this.textBox_lift_gate_origin);
            this.panel_special_services.Controls.Add(this.checkBox_lift);
            this.panel_special_services.Controls.Add(this.textBox_lift);
            this.panel_special_services.Controls.Add(this.checkBox_inside_pickup);
            this.panel_special_services.Controls.Add(this.textBox_inside_pickup);
            this.panel_special_services.Controls.Add(this.checkBox_holiday_delivery);
            this.panel_special_services.Controls.Add(this.textBox_holiday_delivery);
            this.panel_special_services.Controls.Add(this.checkBox_guaranteed);
            this.panel_special_services.Controls.Add(this.textBox_guaranteed);
            this.panel_special_services.Controls.Add(this.checkBox_expedited_shipment);
            this.panel_special_services.Controls.Add(this.textBox_expedited_shipment);
            this.panel_special_services.Controls.Add(this.checkBox_emergency_delivery);
            this.panel_special_services.Controls.Add(this.textBox_emergency_delivery);
            this.panel_special_services.Controls.Add(this.checkBox_dry_run);
            this.panel_special_services.Controls.Add(this.textBox_dry_run);
            this.panel_special_services.Controls.Add(this.checkBox_driver_load);
            this.panel_special_services.Controls.Add(this.textBox_driver_load);
            this.panel_special_services.Controls.Add(this.checkBox_driver_load_and_unload);
            this.panel_special_services.Controls.Add(this.textBox_driver_load_and_unload);
            this.panel_special_services.Controls.Add(this.checkBox_cross_dock);
            this.panel_special_services.Controls.Add(this.textBox_cross_dock);
            this.panel_special_services.Controls.Add(this.checkBox_commercial_pickup);
            this.panel_special_services.Controls.Add(this.textBox_commercial_pickup);
            this.panel_special_services.Controls.Add(this.checkBox_bobtail);
            this.panel_special_services.Controls.Add(this.textBox_bobtail);
            this.panel_special_services.Controls.Add(this.checkBox_bag_liner);
            this.panel_special_services.Controls.Add(this.textBox_bag_liner);
            this.panel_special_services.Controls.Add(this.checkBox_appointment);
            this.panel_special_services.Controls.Add(this.textBox_appointment);
            this.panel_special_services.Controls.Add(this.checkBox_live_loading_or_unloading);
            this.panel_special_services.Controls.Add(this.textBox_live_loading_or_unloading);
            this.panel_special_services.Controls.Add(this.checkBox_team_required);
            this.panel_special_services.Controls.Add(this.textBox_team_required);
            this.panel_special_services.Controls.Add(this.checkBox_stop_off);
            this.panel_special_services.Controls.Add(this.textBox_stop_off);
            this.panel_special_services.Controls.Add(this.checkBox_residential_pickup);
            this.panel_special_services.Controls.Add(this.textBox_residential_pickup);
            this.panel_special_services.Controls.Add(this.checkBox_protective);
            this.panel_special_services.Controls.Add(this.textBox_protective);
            this.panel_special_services.Controls.Add(this.checkBox_notification);
            this.panel_special_services.Controls.Add(this.textBox_notification);
            this.panel_special_services.Controls.Add(this.checkBox_limited_delivery_or_military);
            this.panel_special_services.Controls.Add(this.textBox_limited_delivery_or_military);
            this.panel_special_services.Controls.Add(this.checkBox_lift_gate_destination);
            this.panel_special_services.Controls.Add(this.textBox_lift_gate_destination);
            this.panel_special_services.Controls.Add(this.checkBox_limited_access_delivery);
            this.panel_special_services.Controls.Add(this.textBox_limited_access_delivery);
            this.panel_special_services.Controls.Add(this.checkBox_inside_delivery);
            this.panel_special_services.Controls.Add(this.textBox_inside_delivery);
            this.panel_special_services.Controls.Add(this.checkBox_handling_and_labeling);
            this.panel_special_services.Controls.Add(this.textBox_handling_and_labeling);
            this.panel_special_services.Controls.Add(this.checkBox_forklift);
            this.panel_special_services.Controls.Add(this.textBox_forklift);
            this.panel_special_services.Controls.Add(this.checkBox_expedited_same_dy_snc);
            this.panel_special_services.Controls.Add(this.textBox_expedited_same_dy_snc);
            this.panel_special_services.Controls.Add(this.checkBox_driver_unload_and_count);
            this.panel_special_services.Controls.Add(this.textBox_driver_unload_and_count);
            this.panel_special_services.Controls.Add(this.checkBox_driver_unload);
            this.panel_special_services.Controls.Add(this.textBox_driver_unload);
            this.panel_special_services.Controls.Add(this.checkBox_drop_pull);
            this.panel_special_services.Controls.Add(this.textBox_drop_pull);
            this.panel_special_services.Controls.Add(this.checkBox_driver_load_and_count);
            this.panel_special_services.Controls.Add(this.textBox_driver_load_and_count);
            this.panel_special_services.Controls.Add(this.checkBox_crane_permits);
            this.panel_special_services.Controls.Add(this.textBox_crane_permits);
            this.panel_special_services.Controls.Add(this.checkBox_cross_border);
            this.panel_special_services.Controls.Add(this.textBox_cross_border);
            this.panel_special_services.Controls.Add(this.checkBox_commercial_delivery);
            this.panel_special_services.Controls.Add(this.textBox_commercial_delivery);
            this.panel_special_services.Controls.Add(this.checkBox_bulkhead);
            this.panel_special_services.Controls.Add(this.textBox_bulkhead);
            this.panel_special_services.Controls.Add(this.checkBox_air_bag);
            this.panel_special_services.Controls.Add(this.textBox_air_bag);
            this.panel_special_services.Controls.Add(this.checkBox_after_hours_delivery);
            this.panel_special_services.Controls.Add(this.textBox_after_hours_delivery);
            this.panel_special_services.Controls.Add(this.checkBox_fuel_surcharge);
            this.panel_special_services.Controls.Add(this.textBox_fuel_surcharge);
            this.panel_special_services.Controls.Add(this.checkBox_hazmat);
            this.panel_special_services.Controls.Add(this.textBox_hazmat);
            this.panel_special_services.Controls.Add(this.checkBox_temp_control);
            this.panel_special_services.Controls.Add(this.textBox_temp_control);
            this.panel_special_services.Controls.Add(this.checkBox_reefer);
            this.panel_special_services.Controls.Add(this.textBox_reefer);
            this.panel_special_services.Controls.Add(this.checkBox_heat);
            this.panel_special_services.Controls.Add(this.textBox_heat);
            this.panel_special_services.Controls.Add(this.checkBox_pallet_exchange);
            this.panel_special_services.Controls.Add(this.textBox_pallet_exchange);
            this.panel_special_services.Controls.Add(this.checkBox_driver_assistance);
            this.panel_special_services.Controls.Add(this.textBox_driver_assistance);
            this.panel_special_services.Controls.Add(this.checkBox_lumper);
            this.panel_special_services.Controls.Add(this.textBox_lumper);
            this.panel_special_services.Controls.Add(this.checkBox_tailgate_charge);
            this.panel_special_services.Controls.Add(this.textBox_tailgate_charge);
            this.panel_special_services.Controls.Add(this.checkBox_delivery_appointment);
            this.panel_special_services.Controls.Add(this.textBox_delivery_appointment);
            this.panel_special_services.Controls.Add(this.checkBox_pickup_appointment);
            this.panel_special_services.Controls.Add(this.textBox_pickup_appointment);
            this.panel_special_services.Controls.Add(this.checkBox_guaranteed_delivery);
            this.panel_special_services.Controls.Add(this.textBox_guaranteed_delivery);
            this.panel_special_services.Location = new System.Drawing.Point(87, 208);
            this.panel_special_services.Name = "panel_special_services";
            this.panel_special_services.Size = new System.Drawing.Size(434, 239);
            this.panel_special_services.TabIndex = 40;
            // 
            // checkBox_tradeshow
            // 
            this.checkBox_tradeshow.AutoSize = true;
            this.checkBox_tradeshow.Location = new System.Drawing.Point(70, 2463);
            this.checkBox_tradeshow.Name = "checkBox_tradeshow";
            this.checkBox_tradeshow.Size = new System.Drawing.Size(100, 21);
            this.checkBox_tradeshow.TabIndex = 115;
            this.checkBox_tradeshow.Text = "Tradeshow";
            this.checkBox_tradeshow.UseVisualStyleBackColor = true;
            // 
            // textBox_tradeshow
            // 
            this.textBox_tradeshow.Location = new System.Drawing.Point(282, 2463);
            this.textBox_tradeshow.Name = "textBox_tradeshow";
            this.textBox_tradeshow.Size = new System.Drawing.Size(58, 22);
            this.textBox_tradeshow.TabIndex = 114;
            this.textBox_tradeshow.Text = "0.00";
            // 
            // checkBox_special_equipment_needed
            // 
            this.checkBox_special_equipment_needed.AutoSize = true;
            this.checkBox_special_equipment_needed.Location = new System.Drawing.Point(70, 2420);
            this.checkBox_special_equipment_needed.Name = "checkBox_special_equipment_needed";
            this.checkBox_special_equipment_needed.Size = new System.Drawing.Size(201, 21);
            this.checkBox_special_equipment_needed.TabIndex = 113;
            this.checkBox_special_equipment_needed.Text = "Special Equipment Needed";
            this.checkBox_special_equipment_needed.UseVisualStyleBackColor = true;
            // 
            // textBox_special_equipment_needed
            // 
            this.textBox_special_equipment_needed.Location = new System.Drawing.Point(282, 2420);
            this.textBox_special_equipment_needed.Name = "textBox_special_equipment_needed";
            this.textBox_special_equipment_needed.Size = new System.Drawing.Size(58, 22);
            this.textBox_special_equipment_needed.TabIndex = 112;
            this.textBox_special_equipment_needed.Text = "0.00";
            // 
            // checkBox_saturday_pickup_or_delivery
            // 
            this.checkBox_saturday_pickup_or_delivery.AutoSize = true;
            this.checkBox_saturday_pickup_or_delivery.Location = new System.Drawing.Point(70, 2377);
            this.checkBox_saturday_pickup_or_delivery.Name = "checkBox_saturday_pickup_or_delivery";
            this.checkBox_saturday_pickup_or_delivery.Size = new System.Drawing.Size(205, 21);
            this.checkBox_saturday_pickup_or_delivery.TabIndex = 111;
            this.checkBox_saturday_pickup_or_delivery.Text = "Saturday Pickup or Delivery";
            this.checkBox_saturday_pickup_or_delivery.UseVisualStyleBackColor = true;
            // 
            // textBox_saturday_pickup_or_delivery
            // 
            this.textBox_saturday_pickup_or_delivery.Location = new System.Drawing.Point(282, 2377);
            this.textBox_saturday_pickup_or_delivery.Name = "textBox_saturday_pickup_or_delivery";
            this.textBox_saturday_pickup_or_delivery.Size = new System.Drawing.Size(58, 22);
            this.textBox_saturday_pickup_or_delivery.TabIndex = 110;
            this.textBox_saturday_pickup_or_delivery.Text = "0.00";
            // 
            // checkBox_residential_delivery
            // 
            this.checkBox_residential_delivery.AutoSize = true;
            this.checkBox_residential_delivery.Location = new System.Drawing.Point(70, 2334);
            this.checkBox_residential_delivery.Name = "checkBox_residential_delivery";
            this.checkBox_residential_delivery.Size = new System.Drawing.Size(155, 21);
            this.checkBox_residential_delivery.TabIndex = 109;
            this.checkBox_residential_delivery.Text = "Residential Delivery";
            this.checkBox_residential_delivery.UseVisualStyleBackColor = true;
            // 
            // textBox_residential_delivery
            // 
            this.textBox_residential_delivery.Location = new System.Drawing.Point(282, 2334);
            this.textBox_residential_delivery.Name = "textBox_residential_delivery";
            this.textBox_residential_delivery.Size = new System.Drawing.Size(58, 22);
            this.textBox_residential_delivery.TabIndex = 108;
            this.textBox_residential_delivery.Text = "0.00";
            // 
            // checkBox_pallet
            // 
            this.checkBox_pallet.AutoSize = true;
            this.checkBox_pallet.Location = new System.Drawing.Point(70, 2291);
            this.checkBox_pallet.Name = "checkBox_pallet";
            this.checkBox_pallet.Size = new System.Drawing.Size(65, 21);
            this.checkBox_pallet.TabIndex = 107;
            this.checkBox_pallet.Text = "Pallet";
            this.checkBox_pallet.UseVisualStyleBackColor = true;
            // 
            // textBox_pallet
            // 
            this.textBox_pallet.Location = new System.Drawing.Point(282, 2291);
            this.textBox_pallet.Name = "textBox_pallet";
            this.textBox_pallet.Size = new System.Drawing.Size(58, 22);
            this.textBox_pallet.TabIndex = 106;
            this.textBox_pallet.Text = "0.00";
            // 
            // checkBox_premium_air_freight
            // 
            this.checkBox_premium_air_freight.AutoSize = true;
            this.checkBox_premium_air_freight.Location = new System.Drawing.Point(70, 2248);
            this.checkBox_premium_air_freight.Name = "checkBox_premium_air_freight";
            this.checkBox_premium_air_freight.Size = new System.Drawing.Size(154, 21);
            this.checkBox_premium_air_freight.TabIndex = 105;
            this.checkBox_premium_air_freight.Text = "Premium Air Freight";
            this.checkBox_premium_air_freight.UseVisualStyleBackColor = true;
            // 
            // textBox_premium_air_freight
            // 
            this.textBox_premium_air_freight.Location = new System.Drawing.Point(282, 2248);
            this.textBox_premium_air_freight.Name = "textBox_premium_air_freight";
            this.textBox_premium_air_freight.Size = new System.Drawing.Size(58, 22);
            this.textBox_premium_air_freight.TabIndex = 104;
            this.textBox_premium_air_freight.Text = "0.00";
            // 
            // checkBox_marking_and_tagging
            // 
            this.checkBox_marking_and_tagging.AutoSize = true;
            this.checkBox_marking_and_tagging.Location = new System.Drawing.Point(70, 2205);
            this.checkBox_marking_and_tagging.Name = "checkBox_marking_and_tagging";
            this.checkBox_marking_and_tagging.Size = new System.Drawing.Size(165, 21);
            this.checkBox_marking_and_tagging.TabIndex = 103;
            this.checkBox_marking_and_tagging.Text = "Marking And Tagging";
            this.checkBox_marking_and_tagging.UseVisualStyleBackColor = true;
            // 
            // textBox_marking_and_tagging
            // 
            this.textBox_marking_and_tagging.Location = new System.Drawing.Point(282, 2205);
            this.textBox_marking_and_tagging.Name = "textBox_marking_and_tagging";
            this.textBox_marking_and_tagging.Size = new System.Drawing.Size(58, 22);
            this.textBox_marking_and_tagging.TabIndex = 102;
            this.textBox_marking_and_tagging.Text = "0.00";
            // 
            // checkBox_liquor_permit
            // 
            this.checkBox_liquor_permit.AutoSize = true;
            this.checkBox_liquor_permit.Location = new System.Drawing.Point(70, 2162);
            this.checkBox_liquor_permit.Name = "checkBox_liquor_permit";
            this.checkBox_liquor_permit.Size = new System.Drawing.Size(114, 21);
            this.checkBox_liquor_permit.TabIndex = 101;
            this.checkBox_liquor_permit.Text = "Liquor Permit";
            this.checkBox_liquor_permit.UseVisualStyleBackColor = true;
            // 
            // textBox_liquor_permit
            // 
            this.textBox_liquor_permit.Location = new System.Drawing.Point(282, 2162);
            this.textBox_liquor_permit.Name = "textBox_liquor_permit";
            this.textBox_liquor_permit.Size = new System.Drawing.Size(58, 22);
            this.textBox_liquor_permit.TabIndex = 100;
            this.textBox_liquor_permit.Text = "0.00";
            // 
            // checkBox_lift_gate_origin
            // 
            this.checkBox_lift_gate_origin.AutoSize = true;
            this.checkBox_lift_gate_origin.Location = new System.Drawing.Point(70, 2119);
            this.checkBox_lift_gate_origin.Name = "checkBox_lift_gate_origin";
            this.checkBox_lift_gate_origin.Size = new System.Drawing.Size(126, 21);
            this.checkBox_lift_gate_origin.TabIndex = 99;
            this.checkBox_lift_gate_origin.Text = "Lift Gate Origin";
            this.checkBox_lift_gate_origin.UseVisualStyleBackColor = true;
            // 
            // textBox_lift_gate_origin
            // 
            this.textBox_lift_gate_origin.Location = new System.Drawing.Point(282, 2119);
            this.textBox_lift_gate_origin.Name = "textBox_lift_gate_origin";
            this.textBox_lift_gate_origin.Size = new System.Drawing.Size(58, 22);
            this.textBox_lift_gate_origin.TabIndex = 98;
            this.textBox_lift_gate_origin.Text = "0.00";
            // 
            // checkBox_lift
            // 
            this.checkBox_lift.AutoSize = true;
            this.checkBox_lift.Location = new System.Drawing.Point(70, 2076);
            this.checkBox_lift.Name = "checkBox_lift";
            this.checkBox_lift.Size = new System.Drawing.Size(49, 21);
            this.checkBox_lift.TabIndex = 97;
            this.checkBox_lift.Text = "Lift";
            this.checkBox_lift.UseVisualStyleBackColor = true;
            // 
            // textBox_lift
            // 
            this.textBox_lift.Location = new System.Drawing.Point(282, 2076);
            this.textBox_lift.Name = "textBox_lift";
            this.textBox_lift.Size = new System.Drawing.Size(58, 22);
            this.textBox_lift.TabIndex = 96;
            this.textBox_lift.Text = "0.00";
            // 
            // checkBox_inside_pickup
            // 
            this.checkBox_inside_pickup.AutoSize = true;
            this.checkBox_inside_pickup.Location = new System.Drawing.Point(70, 2033);
            this.checkBox_inside_pickup.Name = "checkBox_inside_pickup";
            this.checkBox_inside_pickup.Size = new System.Drawing.Size(113, 21);
            this.checkBox_inside_pickup.TabIndex = 95;
            this.checkBox_inside_pickup.Text = "Inside Pickup";
            this.checkBox_inside_pickup.UseVisualStyleBackColor = true;
            // 
            // textBox_inside_pickup
            // 
            this.textBox_inside_pickup.Location = new System.Drawing.Point(282, 2033);
            this.textBox_inside_pickup.Name = "textBox_inside_pickup";
            this.textBox_inside_pickup.Size = new System.Drawing.Size(58, 22);
            this.textBox_inside_pickup.TabIndex = 94;
            this.textBox_inside_pickup.Text = "0.00";
            // 
            // checkBox_holiday_delivery
            // 
            this.checkBox_holiday_delivery.AutoSize = true;
            this.checkBox_holiday_delivery.Location = new System.Drawing.Point(70, 1990);
            this.checkBox_holiday_delivery.Name = "checkBox_holiday_delivery";
            this.checkBox_holiday_delivery.Size = new System.Drawing.Size(132, 21);
            this.checkBox_holiday_delivery.TabIndex = 93;
            this.checkBox_holiday_delivery.Text = "Holiday Delivery";
            this.checkBox_holiday_delivery.UseVisualStyleBackColor = true;
            // 
            // textBox_holiday_delivery
            // 
            this.textBox_holiday_delivery.Location = new System.Drawing.Point(282, 1990);
            this.textBox_holiday_delivery.Name = "textBox_holiday_delivery";
            this.textBox_holiday_delivery.Size = new System.Drawing.Size(58, 22);
            this.textBox_holiday_delivery.TabIndex = 92;
            this.textBox_holiday_delivery.Text = "0.00";
            // 
            // checkBox_guaranteed
            // 
            this.checkBox_guaranteed.AutoSize = true;
            this.checkBox_guaranteed.Location = new System.Drawing.Point(70, 1947);
            this.checkBox_guaranteed.Name = "checkBox_guaranteed";
            this.checkBox_guaranteed.Size = new System.Drawing.Size(106, 21);
            this.checkBox_guaranteed.TabIndex = 91;
            this.checkBox_guaranteed.Text = "Guaranteed";
            this.checkBox_guaranteed.UseVisualStyleBackColor = true;
            // 
            // textBox_guaranteed
            // 
            this.textBox_guaranteed.Location = new System.Drawing.Point(282, 1947);
            this.textBox_guaranteed.Name = "textBox_guaranteed";
            this.textBox_guaranteed.Size = new System.Drawing.Size(58, 22);
            this.textBox_guaranteed.TabIndex = 90;
            this.textBox_guaranteed.Text = "0.00";
            // 
            // checkBox_expedited_shipment
            // 
            this.checkBox_expedited_shipment.AutoSize = true;
            this.checkBox_expedited_shipment.Location = new System.Drawing.Point(70, 1904);
            this.checkBox_expedited_shipment.Name = "checkBox_expedited_shipment";
            this.checkBox_expedited_shipment.Size = new System.Drawing.Size(155, 21);
            this.checkBox_expedited_shipment.TabIndex = 89;
            this.checkBox_expedited_shipment.Text = "Expedited Shipment";
            this.checkBox_expedited_shipment.UseVisualStyleBackColor = true;
            // 
            // textBox_expedited_shipment
            // 
            this.textBox_expedited_shipment.Location = new System.Drawing.Point(282, 1904);
            this.textBox_expedited_shipment.Name = "textBox_expedited_shipment";
            this.textBox_expedited_shipment.Size = new System.Drawing.Size(58, 22);
            this.textBox_expedited_shipment.TabIndex = 88;
            this.textBox_expedited_shipment.Text = "0.00";
            // 
            // checkBox_emergency_delivery
            // 
            this.checkBox_emergency_delivery.AutoSize = true;
            this.checkBox_emergency_delivery.Location = new System.Drawing.Point(70, 1861);
            this.checkBox_emergency_delivery.Name = "checkBox_emergency_delivery";
            this.checkBox_emergency_delivery.Size = new System.Drawing.Size(156, 21);
            this.checkBox_emergency_delivery.TabIndex = 87;
            this.checkBox_emergency_delivery.Text = "Emergency Delivery";
            this.checkBox_emergency_delivery.UseVisualStyleBackColor = true;
            // 
            // textBox_emergency_delivery
            // 
            this.textBox_emergency_delivery.Location = new System.Drawing.Point(282, 1861);
            this.textBox_emergency_delivery.Name = "textBox_emergency_delivery";
            this.textBox_emergency_delivery.Size = new System.Drawing.Size(58, 22);
            this.textBox_emergency_delivery.TabIndex = 86;
            this.textBox_emergency_delivery.Text = "0.00";
            // 
            // checkBox_dry_run
            // 
            this.checkBox_dry_run.AutoSize = true;
            this.checkBox_dry_run.Location = new System.Drawing.Point(70, 1818);
            this.checkBox_dry_run.Name = "checkBox_dry_run";
            this.checkBox_dry_run.Size = new System.Drawing.Size(82, 21);
            this.checkBox_dry_run.TabIndex = 85;
            this.checkBox_dry_run.Text = "Dry Run";
            this.checkBox_dry_run.UseVisualStyleBackColor = true;
            // 
            // textBox_dry_run
            // 
            this.textBox_dry_run.Location = new System.Drawing.Point(282, 1818);
            this.textBox_dry_run.Name = "textBox_dry_run";
            this.textBox_dry_run.Size = new System.Drawing.Size(58, 22);
            this.textBox_dry_run.TabIndex = 84;
            this.textBox_dry_run.Text = "0.00";
            // 
            // checkBox_driver_load
            // 
            this.checkBox_driver_load.AutoSize = true;
            this.checkBox_driver_load.Location = new System.Drawing.Point(70, 1775);
            this.checkBox_driver_load.Name = "checkBox_driver_load";
            this.checkBox_driver_load.Size = new System.Drawing.Size(104, 21);
            this.checkBox_driver_load.TabIndex = 83;
            this.checkBox_driver_load.Text = "Driver Load";
            this.checkBox_driver_load.UseVisualStyleBackColor = true;
            // 
            // textBox_driver_load
            // 
            this.textBox_driver_load.Location = new System.Drawing.Point(282, 1775);
            this.textBox_driver_load.Name = "textBox_driver_load";
            this.textBox_driver_load.Size = new System.Drawing.Size(58, 22);
            this.textBox_driver_load.TabIndex = 82;
            this.textBox_driver_load.Text = "0.00";
            // 
            // checkBox_driver_load_and_unload
            // 
            this.checkBox_driver_load_and_unload.AutoSize = true;
            this.checkBox_driver_load_and_unload.Location = new System.Drawing.Point(70, 1732);
            this.checkBox_driver_load_and_unload.Name = "checkBox_driver_load_and_unload";
            this.checkBox_driver_load_and_unload.Size = new System.Drawing.Size(182, 21);
            this.checkBox_driver_load_and_unload.TabIndex = 81;
            this.checkBox_driver_load_and_unload.Text = "Driver Load And Unload";
            this.checkBox_driver_load_and_unload.UseVisualStyleBackColor = true;
            // 
            // textBox_driver_load_and_unload
            // 
            this.textBox_driver_load_and_unload.Location = new System.Drawing.Point(282, 1732);
            this.textBox_driver_load_and_unload.Name = "textBox_driver_load_and_unload";
            this.textBox_driver_load_and_unload.Size = new System.Drawing.Size(58, 22);
            this.textBox_driver_load_and_unload.TabIndex = 80;
            this.textBox_driver_load_and_unload.Text = "0.00";
            // 
            // checkBox_cross_dock
            // 
            this.checkBox_cross_dock.AutoSize = true;
            this.checkBox_cross_dock.Location = new System.Drawing.Point(70, 1689);
            this.checkBox_cross_dock.Name = "checkBox_cross_dock";
            this.checkBox_cross_dock.Size = new System.Drawing.Size(102, 21);
            this.checkBox_cross_dock.TabIndex = 79;
            this.checkBox_cross_dock.Text = "Cross Dock";
            this.checkBox_cross_dock.UseVisualStyleBackColor = true;
            // 
            // textBox_cross_dock
            // 
            this.textBox_cross_dock.Location = new System.Drawing.Point(282, 1689);
            this.textBox_cross_dock.Name = "textBox_cross_dock";
            this.textBox_cross_dock.Size = new System.Drawing.Size(58, 22);
            this.textBox_cross_dock.TabIndex = 78;
            this.textBox_cross_dock.Text = "0.00";
            // 
            // checkBox_commercial_pickup
            // 
            this.checkBox_commercial_pickup.AutoSize = true;
            this.checkBox_commercial_pickup.Location = new System.Drawing.Point(70, 1646);
            this.checkBox_commercial_pickup.Name = "checkBox_commercial_pickup";
            this.checkBox_commercial_pickup.Size = new System.Drawing.Size(149, 21);
            this.checkBox_commercial_pickup.TabIndex = 77;
            this.checkBox_commercial_pickup.Text = "Commercial Pickup";
            this.checkBox_commercial_pickup.UseVisualStyleBackColor = true;
            // 
            // textBox_commercial_pickup
            // 
            this.textBox_commercial_pickup.Location = new System.Drawing.Point(282, 1646);
            this.textBox_commercial_pickup.Name = "textBox_commercial_pickup";
            this.textBox_commercial_pickup.Size = new System.Drawing.Size(58, 22);
            this.textBox_commercial_pickup.TabIndex = 76;
            this.textBox_commercial_pickup.Text = "0.00";
            // 
            // checkBox_bobtail
            // 
            this.checkBox_bobtail.AutoSize = true;
            this.checkBox_bobtail.Location = new System.Drawing.Point(70, 1603);
            this.checkBox_bobtail.Name = "checkBox_bobtail";
            this.checkBox_bobtail.Size = new System.Drawing.Size(73, 21);
            this.checkBox_bobtail.TabIndex = 75;
            this.checkBox_bobtail.Text = "Bobtail";
            this.checkBox_bobtail.UseVisualStyleBackColor = true;
            // 
            // textBox_bobtail
            // 
            this.textBox_bobtail.Location = new System.Drawing.Point(282, 1603);
            this.textBox_bobtail.Name = "textBox_bobtail";
            this.textBox_bobtail.Size = new System.Drawing.Size(58, 22);
            this.textBox_bobtail.TabIndex = 74;
            this.textBox_bobtail.Text = "0.00";
            // 
            // checkBox_bag_liner
            // 
            this.checkBox_bag_liner.AutoSize = true;
            this.checkBox_bag_liner.Location = new System.Drawing.Point(70, 1560);
            this.checkBox_bag_liner.Name = "checkBox_bag_liner";
            this.checkBox_bag_liner.Size = new System.Drawing.Size(91, 21);
            this.checkBox_bag_liner.TabIndex = 73;
            this.checkBox_bag_liner.Text = "Bag Liner";
            this.checkBox_bag_liner.UseVisualStyleBackColor = true;
            // 
            // textBox_bag_liner
            // 
            this.textBox_bag_liner.Location = new System.Drawing.Point(282, 1560);
            this.textBox_bag_liner.Name = "textBox_bag_liner";
            this.textBox_bag_liner.Size = new System.Drawing.Size(58, 22);
            this.textBox_bag_liner.TabIndex = 72;
            this.textBox_bag_liner.Text = "0.00";
            // 
            // checkBox_appointment
            // 
            this.checkBox_appointment.AutoSize = true;
            this.checkBox_appointment.Location = new System.Drawing.Point(70, 1517);
            this.checkBox_appointment.Name = "checkBox_appointment";
            this.checkBox_appointment.Size = new System.Drawing.Size(109, 21);
            this.checkBox_appointment.TabIndex = 71;
            this.checkBox_appointment.Text = "Appointment";
            this.checkBox_appointment.UseVisualStyleBackColor = true;
            // 
            // textBox_appointment
            // 
            this.textBox_appointment.Location = new System.Drawing.Point(282, 1517);
            this.textBox_appointment.Name = "textBox_appointment";
            this.textBox_appointment.Size = new System.Drawing.Size(58, 22);
            this.textBox_appointment.TabIndex = 70;
            this.textBox_appointment.Text = "0.00";
            // 
            // checkBox_live_loading_or_unloading
            // 
            this.checkBox_live_loading_or_unloading.AutoSize = true;
            this.checkBox_live_loading_or_unloading.Location = new System.Drawing.Point(70, 1474);
            this.checkBox_live_loading_or_unloading.Name = "checkBox_live_loading_or_unloading";
            this.checkBox_live_loading_or_unloading.Size = new System.Drawing.Size(196, 21);
            this.checkBox_live_loading_or_unloading.TabIndex = 69;
            this.checkBox_live_loading_or_unloading.Text = "Live Loading or Unloading";
            this.checkBox_live_loading_or_unloading.UseVisualStyleBackColor = true;
            // 
            // textBox_live_loading_or_unloading
            // 
            this.textBox_live_loading_or_unloading.Location = new System.Drawing.Point(282, 1474);
            this.textBox_live_loading_or_unloading.Name = "textBox_live_loading_or_unloading";
            this.textBox_live_loading_or_unloading.Size = new System.Drawing.Size(58, 22);
            this.textBox_live_loading_or_unloading.TabIndex = 68;
            this.textBox_live_loading_or_unloading.Text = "0.00";
            // 
            // checkBox_team_required
            // 
            this.checkBox_team_required.AutoSize = true;
            this.checkBox_team_required.Location = new System.Drawing.Point(70, 1431);
            this.checkBox_team_required.Name = "checkBox_team_required";
            this.checkBox_team_required.Size = new System.Drawing.Size(132, 21);
            this.checkBox_team_required.TabIndex = 67;
            this.checkBox_team_required.Text = "Team  Required";
            this.checkBox_team_required.UseVisualStyleBackColor = true;
            // 
            // textBox_team_required
            // 
            this.textBox_team_required.Location = new System.Drawing.Point(282, 1431);
            this.textBox_team_required.Name = "textBox_team_required";
            this.textBox_team_required.Size = new System.Drawing.Size(58, 22);
            this.textBox_team_required.TabIndex = 66;
            this.textBox_team_required.Text = "0.00";
            // 
            // checkBox_stop_off
            // 
            this.checkBox_stop_off.AutoSize = true;
            this.checkBox_stop_off.Location = new System.Drawing.Point(70, 1388);
            this.checkBox_stop_off.Name = "checkBox_stop_off";
            this.checkBox_stop_off.Size = new System.Drawing.Size(82, 21);
            this.checkBox_stop_off.TabIndex = 65;
            this.checkBox_stop_off.Text = "Stop Off";
            this.checkBox_stop_off.UseVisualStyleBackColor = true;
            // 
            // textBox_stop_off
            // 
            this.textBox_stop_off.Location = new System.Drawing.Point(282, 1388);
            this.textBox_stop_off.Name = "textBox_stop_off";
            this.textBox_stop_off.Size = new System.Drawing.Size(58, 22);
            this.textBox_stop_off.TabIndex = 64;
            this.textBox_stop_off.Text = "0.00";
            // 
            // checkBox_residential_pickup
            // 
            this.checkBox_residential_pickup.AutoSize = true;
            this.checkBox_residential_pickup.Location = new System.Drawing.Point(70, 1345);
            this.checkBox_residential_pickup.Name = "checkBox_residential_pickup";
            this.checkBox_residential_pickup.Size = new System.Drawing.Size(146, 21);
            this.checkBox_residential_pickup.TabIndex = 63;
            this.checkBox_residential_pickup.Text = "Residential Pickup";
            this.checkBox_residential_pickup.UseVisualStyleBackColor = true;
            // 
            // textBox_residential_pickup
            // 
            this.textBox_residential_pickup.Location = new System.Drawing.Point(282, 1345);
            this.textBox_residential_pickup.Name = "textBox_residential_pickup";
            this.textBox_residential_pickup.Size = new System.Drawing.Size(58, 22);
            this.textBox_residential_pickup.TabIndex = 62;
            this.textBox_residential_pickup.Text = "0.00";
            // 
            // checkBox_protective
            // 
            this.checkBox_protective.AutoSize = true;
            this.checkBox_protective.Location = new System.Drawing.Point(70, 1302);
            this.checkBox_protective.Name = "checkBox_protective";
            this.checkBox_protective.Size = new System.Drawing.Size(93, 21);
            this.checkBox_protective.TabIndex = 61;
            this.checkBox_protective.Text = "Protective";
            this.checkBox_protective.UseVisualStyleBackColor = true;
            // 
            // textBox_protective
            // 
            this.textBox_protective.Location = new System.Drawing.Point(282, 1302);
            this.textBox_protective.Name = "textBox_protective";
            this.textBox_protective.Size = new System.Drawing.Size(58, 22);
            this.textBox_protective.TabIndex = 60;
            this.textBox_protective.Text = "0.00";
            // 
            // checkBox_notification
            // 
            this.checkBox_notification.AutoSize = true;
            this.checkBox_notification.Location = new System.Drawing.Point(70, 1259);
            this.checkBox_notification.Name = "checkBox_notification";
            this.checkBox_notification.Size = new System.Drawing.Size(100, 21);
            this.checkBox_notification.TabIndex = 59;
            this.checkBox_notification.Text = "Notification";
            this.checkBox_notification.UseVisualStyleBackColor = true;
            // 
            // textBox_notification
            // 
            this.textBox_notification.Location = new System.Drawing.Point(282, 1259);
            this.textBox_notification.Name = "textBox_notification";
            this.textBox_notification.Size = new System.Drawing.Size(58, 22);
            this.textBox_notification.TabIndex = 58;
            this.textBox_notification.Text = "0.00";
            // 
            // checkBox_limited_delivery_or_military
            // 
            this.checkBox_limited_delivery_or_military.AutoSize = true;
            this.checkBox_limited_delivery_or_military.Location = new System.Drawing.Point(70, 1216);
            this.checkBox_limited_delivery_or_military.Name = "checkBox_limited_delivery_or_military";
            this.checkBox_limited_delivery_or_military.Size = new System.Drawing.Size(195, 21);
            this.checkBox_limited_delivery_or_military.TabIndex = 57;
            this.checkBox_limited_delivery_or_military.Text = "Limited Delivery or Military";
            this.checkBox_limited_delivery_or_military.UseVisualStyleBackColor = true;
            // 
            // textBox_limited_delivery_or_military
            // 
            this.textBox_limited_delivery_or_military.Location = new System.Drawing.Point(282, 1216);
            this.textBox_limited_delivery_or_military.Name = "textBox_limited_delivery_or_military";
            this.textBox_limited_delivery_or_military.Size = new System.Drawing.Size(58, 22);
            this.textBox_limited_delivery_or_military.TabIndex = 56;
            this.textBox_limited_delivery_or_military.Text = "0.00";
            // 
            // checkBox_lift_gate_destination
            // 
            this.checkBox_lift_gate_destination.AutoSize = true;
            this.checkBox_lift_gate_destination.Location = new System.Drawing.Point(70, 1173);
            this.checkBox_lift_gate_destination.Name = "checkBox_lift_gate_destination";
            this.checkBox_lift_gate_destination.Size = new System.Drawing.Size(159, 21);
            this.checkBox_lift_gate_destination.TabIndex = 55;
            this.checkBox_lift_gate_destination.Text = "Lift Gate Destination";
            this.checkBox_lift_gate_destination.UseVisualStyleBackColor = true;
            // 
            // textBox_lift_gate_destination
            // 
            this.textBox_lift_gate_destination.Location = new System.Drawing.Point(282, 1173);
            this.textBox_lift_gate_destination.Name = "textBox_lift_gate_destination";
            this.textBox_lift_gate_destination.Size = new System.Drawing.Size(58, 22);
            this.textBox_lift_gate_destination.TabIndex = 54;
            this.textBox_lift_gate_destination.Text = "0.00";
            // 
            // checkBox_limited_access_delivery
            // 
            this.checkBox_limited_access_delivery.AutoSize = true;
            this.checkBox_limited_access_delivery.Location = new System.Drawing.Point(70, 1130);
            this.checkBox_limited_access_delivery.Name = "checkBox_limited_access_delivery";
            this.checkBox_limited_access_delivery.Size = new System.Drawing.Size(179, 21);
            this.checkBox_limited_access_delivery.TabIndex = 53;
            this.checkBox_limited_access_delivery.Text = "Limited Access Delivery";
            this.checkBox_limited_access_delivery.UseVisualStyleBackColor = true;
            // 
            // textBox_limited_access_delivery
            // 
            this.textBox_limited_access_delivery.Location = new System.Drawing.Point(282, 1130);
            this.textBox_limited_access_delivery.Name = "textBox_limited_access_delivery";
            this.textBox_limited_access_delivery.Size = new System.Drawing.Size(58, 22);
            this.textBox_limited_access_delivery.TabIndex = 52;
            this.textBox_limited_access_delivery.Text = "0.00";
            // 
            // checkBox_inside_delivery
            // 
            this.checkBox_inside_delivery.AutoSize = true;
            this.checkBox_inside_delivery.Location = new System.Drawing.Point(70, 1087);
            this.checkBox_inside_delivery.Name = "checkBox_inside_delivery";
            this.checkBox_inside_delivery.Size = new System.Drawing.Size(122, 21);
            this.checkBox_inside_delivery.TabIndex = 51;
            this.checkBox_inside_delivery.Text = "Inside Delivery";
            this.checkBox_inside_delivery.UseVisualStyleBackColor = true;
            // 
            // textBox_inside_delivery
            // 
            this.textBox_inside_delivery.Location = new System.Drawing.Point(282, 1087);
            this.textBox_inside_delivery.Name = "textBox_inside_delivery";
            this.textBox_inside_delivery.Size = new System.Drawing.Size(58, 22);
            this.textBox_inside_delivery.TabIndex = 50;
            this.textBox_inside_delivery.Text = "0.00";
            // 
            // checkBox_handling_and_labeling
            // 
            this.checkBox_handling_and_labeling.AutoSize = true;
            this.checkBox_handling_and_labeling.Location = new System.Drawing.Point(70, 1044);
            this.checkBox_handling_and_labeling.Name = "checkBox_handling_and_labeling";
            this.checkBox_handling_and_labeling.Size = new System.Drawing.Size(173, 21);
            this.checkBox_handling_and_labeling.TabIndex = 49;
            this.checkBox_handling_and_labeling.Text = "Handling And Labeling";
            this.checkBox_handling_and_labeling.UseVisualStyleBackColor = true;
            // 
            // textBox_handling_and_labeling
            // 
            this.textBox_handling_and_labeling.Location = new System.Drawing.Point(282, 1044);
            this.textBox_handling_and_labeling.Name = "textBox_handling_and_labeling";
            this.textBox_handling_and_labeling.Size = new System.Drawing.Size(58, 22);
            this.textBox_handling_and_labeling.TabIndex = 48;
            this.textBox_handling_and_labeling.Text = "0.00";
            // 
            // checkBox_forklift
            // 
            this.checkBox_forklift.AutoSize = true;
            this.checkBox_forklift.Location = new System.Drawing.Point(70, 1001);
            this.checkBox_forklift.Name = "checkBox_forklift";
            this.checkBox_forklift.Size = new System.Drawing.Size(72, 21);
            this.checkBox_forklift.TabIndex = 47;
            this.checkBox_forklift.Text = "Forklift";
            this.checkBox_forklift.UseVisualStyleBackColor = true;
            // 
            // textBox_forklift
            // 
            this.textBox_forklift.Location = new System.Drawing.Point(282, 1001);
            this.textBox_forklift.Name = "textBox_forklift";
            this.textBox_forklift.Size = new System.Drawing.Size(58, 22);
            this.textBox_forklift.TabIndex = 46;
            this.textBox_forklift.Text = "0.00";
            // 
            // checkBox_expedited_same_dy_snc
            // 
            this.checkBox_expedited_same_dy_snc.AutoSize = true;
            this.checkBox_expedited_same_dy_snc.Location = new System.Drawing.Point(70, 958);
            this.checkBox_expedited_same_dy_snc.Name = "checkBox_expedited_same_dy_snc";
            this.checkBox_expedited_same_dy_snc.Size = new System.Drawing.Size(187, 21);
            this.checkBox_expedited_same_dy_snc.TabIndex = 45;
            this.checkBox_expedited_same_dy_snc.Text = "Expedited Same DY SNC";
            this.checkBox_expedited_same_dy_snc.UseVisualStyleBackColor = true;
            // 
            // textBox_expedited_same_dy_snc
            // 
            this.textBox_expedited_same_dy_snc.Location = new System.Drawing.Point(282, 958);
            this.textBox_expedited_same_dy_snc.Name = "textBox_expedited_same_dy_snc";
            this.textBox_expedited_same_dy_snc.Size = new System.Drawing.Size(58, 22);
            this.textBox_expedited_same_dy_snc.TabIndex = 44;
            this.textBox_expedited_same_dy_snc.Text = "0.00";
            // 
            // checkBox_driver_unload_and_count
            // 
            this.checkBox_driver_unload_and_count.AutoSize = true;
            this.checkBox_driver_unload_and_count.Location = new System.Drawing.Point(70, 915);
            this.checkBox_driver_unload_and_count.Name = "checkBox_driver_unload_and_count";
            this.checkBox_driver_unload_and_count.Size = new System.Drawing.Size(187, 21);
            this.checkBox_driver_unload_and_count.TabIndex = 43;
            this.checkBox_driver_unload_and_count.Text = "Driver Unload And Count";
            this.checkBox_driver_unload_and_count.UseVisualStyleBackColor = true;
            // 
            // textBox_driver_unload_and_count
            // 
            this.textBox_driver_unload_and_count.Location = new System.Drawing.Point(282, 915);
            this.textBox_driver_unload_and_count.Name = "textBox_driver_unload_and_count";
            this.textBox_driver_unload_and_count.Size = new System.Drawing.Size(58, 22);
            this.textBox_driver_unload_and_count.TabIndex = 42;
            this.textBox_driver_unload_and_count.Text = "0.00";
            // 
            // checkBox_driver_unload
            // 
            this.checkBox_driver_unload.AutoSize = true;
            this.checkBox_driver_unload.Location = new System.Drawing.Point(70, 872);
            this.checkBox_driver_unload.Name = "checkBox_driver_unload";
            this.checkBox_driver_unload.Size = new System.Drawing.Size(117, 21);
            this.checkBox_driver_unload.TabIndex = 41;
            this.checkBox_driver_unload.Text = "Driver Unload";
            this.checkBox_driver_unload.UseVisualStyleBackColor = true;
            // 
            // textBox_driver_unload
            // 
            this.textBox_driver_unload.Location = new System.Drawing.Point(282, 872);
            this.textBox_driver_unload.Name = "textBox_driver_unload";
            this.textBox_driver_unload.Size = new System.Drawing.Size(58, 22);
            this.textBox_driver_unload.TabIndex = 40;
            this.textBox_driver_unload.Text = "0.00";
            // 
            // checkBox_drop_pull
            // 
            this.checkBox_drop_pull.AutoSize = true;
            this.checkBox_drop_pull.Location = new System.Drawing.Point(70, 829);
            this.checkBox_drop_pull.Name = "checkBox_drop_pull";
            this.checkBox_drop_pull.Size = new System.Drawing.Size(88, 21);
            this.checkBox_drop_pull.TabIndex = 39;
            this.checkBox_drop_pull.Text = "Drop Pull";
            this.checkBox_drop_pull.UseVisualStyleBackColor = true;
            // 
            // textBox_drop_pull
            // 
            this.textBox_drop_pull.Location = new System.Drawing.Point(282, 829);
            this.textBox_drop_pull.Name = "textBox_drop_pull";
            this.textBox_drop_pull.Size = new System.Drawing.Size(58, 22);
            this.textBox_drop_pull.TabIndex = 38;
            this.textBox_drop_pull.Text = "0.00";
            // 
            // checkBox_driver_load_and_count
            // 
            this.checkBox_driver_load_and_count.AutoSize = true;
            this.checkBox_driver_load_and_count.Location = new System.Drawing.Point(70, 786);
            this.checkBox_driver_load_and_count.Name = "checkBox_driver_load_and_count";
            this.checkBox_driver_load_and_count.Size = new System.Drawing.Size(174, 21);
            this.checkBox_driver_load_and_count.TabIndex = 37;
            this.checkBox_driver_load_and_count.Text = "Driver Load And Count";
            this.checkBox_driver_load_and_count.UseVisualStyleBackColor = true;
            // 
            // textBox_driver_load_and_count
            // 
            this.textBox_driver_load_and_count.Location = new System.Drawing.Point(282, 786);
            this.textBox_driver_load_and_count.Name = "textBox_driver_load_and_count";
            this.textBox_driver_load_and_count.Size = new System.Drawing.Size(58, 22);
            this.textBox_driver_load_and_count.TabIndex = 36;
            this.textBox_driver_load_and_count.Text = "0.00";
            // 
            // checkBox_crane_permits
            // 
            this.checkBox_crane_permits.AutoSize = true;
            this.checkBox_crane_permits.Location = new System.Drawing.Point(70, 743);
            this.checkBox_crane_permits.Name = "checkBox_crane_permits";
            this.checkBox_crane_permits.Size = new System.Drawing.Size(119, 21);
            this.checkBox_crane_permits.TabIndex = 35;
            this.checkBox_crane_permits.Text = "Crane Permits";
            this.checkBox_crane_permits.UseVisualStyleBackColor = true;
            // 
            // textBox_crane_permits
            // 
            this.textBox_crane_permits.Location = new System.Drawing.Point(282, 743);
            this.textBox_crane_permits.Name = "textBox_crane_permits";
            this.textBox_crane_permits.Size = new System.Drawing.Size(58, 22);
            this.textBox_crane_permits.TabIndex = 34;
            this.textBox_crane_permits.Text = "0.00";
            // 
            // checkBox_cross_border
            // 
            this.checkBox_cross_border.AutoSize = true;
            this.checkBox_cross_border.Location = new System.Drawing.Point(70, 700);
            this.checkBox_cross_border.Name = "checkBox_cross_border";
            this.checkBox_cross_border.Size = new System.Drawing.Size(113, 21);
            this.checkBox_cross_border.TabIndex = 33;
            this.checkBox_cross_border.Text = "Cross Border";
            this.checkBox_cross_border.UseVisualStyleBackColor = true;
            // 
            // textBox_cross_border
            // 
            this.textBox_cross_border.Location = new System.Drawing.Point(282, 700);
            this.textBox_cross_border.Name = "textBox_cross_border";
            this.textBox_cross_border.Size = new System.Drawing.Size(58, 22);
            this.textBox_cross_border.TabIndex = 32;
            this.textBox_cross_border.Text = "0.00";
            // 
            // checkBox_commercial_delivery
            // 
            this.checkBox_commercial_delivery.AutoSize = true;
            this.checkBox_commercial_delivery.Location = new System.Drawing.Point(70, 657);
            this.checkBox_commercial_delivery.Name = "checkBox_commercial_delivery";
            this.checkBox_commercial_delivery.Size = new System.Drawing.Size(158, 21);
            this.checkBox_commercial_delivery.TabIndex = 31;
            this.checkBox_commercial_delivery.Text = "Commercial Delivery";
            this.checkBox_commercial_delivery.UseVisualStyleBackColor = true;
            // 
            // textBox_commercial_delivery
            // 
            this.textBox_commercial_delivery.Location = new System.Drawing.Point(282, 657);
            this.textBox_commercial_delivery.Name = "textBox_commercial_delivery";
            this.textBox_commercial_delivery.Size = new System.Drawing.Size(58, 22);
            this.textBox_commercial_delivery.TabIndex = 30;
            this.textBox_commercial_delivery.Text = "0.00";
            // 
            // checkBox_bulkhead
            // 
            this.checkBox_bulkhead.AutoSize = true;
            this.checkBox_bulkhead.Location = new System.Drawing.Point(69, 614);
            this.checkBox_bulkhead.Name = "checkBox_bulkhead";
            this.checkBox_bulkhead.Size = new System.Drawing.Size(89, 21);
            this.checkBox_bulkhead.TabIndex = 29;
            this.checkBox_bulkhead.Text = "Bulkhead";
            this.checkBox_bulkhead.UseVisualStyleBackColor = true;
            // 
            // textBox_bulkhead
            // 
            this.textBox_bulkhead.Location = new System.Drawing.Point(282, 614);
            this.textBox_bulkhead.Name = "textBox_bulkhead";
            this.textBox_bulkhead.Size = new System.Drawing.Size(58, 22);
            this.textBox_bulkhead.TabIndex = 28;
            this.textBox_bulkhead.Text = "0.00";
            // 
            // checkBox_air_bag
            // 
            this.checkBox_air_bag.AutoSize = true;
            this.checkBox_air_bag.Location = new System.Drawing.Point(70, 571);
            this.checkBox_air_bag.Name = "checkBox_air_bag";
            this.checkBox_air_bag.Size = new System.Drawing.Size(76, 21);
            this.checkBox_air_bag.TabIndex = 27;
            this.checkBox_air_bag.Text = "Air Bag";
            this.checkBox_air_bag.UseVisualStyleBackColor = true;
            // 
            // textBox_air_bag
            // 
            this.textBox_air_bag.Location = new System.Drawing.Point(282, 571);
            this.textBox_air_bag.Name = "textBox_air_bag";
            this.textBox_air_bag.Size = new System.Drawing.Size(58, 22);
            this.textBox_air_bag.TabIndex = 26;
            this.textBox_air_bag.Text = "0.00";
            // 
            // checkBox_after_hours_delivery
            // 
            this.checkBox_after_hours_delivery.AutoSize = true;
            this.checkBox_after_hours_delivery.Location = new System.Drawing.Point(70, 528);
            this.checkBox_after_hours_delivery.Name = "checkBox_after_hours_delivery";
            this.checkBox_after_hours_delivery.Size = new System.Drawing.Size(157, 21);
            this.checkBox_after_hours_delivery.TabIndex = 25;
            this.checkBox_after_hours_delivery.Text = "After Hours Delivery";
            this.checkBox_after_hours_delivery.UseVisualStyleBackColor = true;
            // 
            // textBox_after_hours_delivery
            // 
            this.textBox_after_hours_delivery.Location = new System.Drawing.Point(282, 528);
            this.textBox_after_hours_delivery.Name = "textBox_after_hours_delivery";
            this.textBox_after_hours_delivery.Size = new System.Drawing.Size(58, 22);
            this.textBox_after_hours_delivery.TabIndex = 24;
            this.textBox_after_hours_delivery.Text = "0.00";
            // 
            // checkBox_fuel_surcharge
            // 
            this.checkBox_fuel_surcharge.AutoSize = true;
            this.checkBox_fuel_surcharge.Location = new System.Drawing.Point(70, 485);
            this.checkBox_fuel_surcharge.Name = "checkBox_fuel_surcharge";
            this.checkBox_fuel_surcharge.Size = new System.Drawing.Size(127, 21);
            this.checkBox_fuel_surcharge.TabIndex = 23;
            this.checkBox_fuel_surcharge.Text = "Fuel Surcharge";
            this.checkBox_fuel_surcharge.UseVisualStyleBackColor = true;
            // 
            // textBox_fuel_surcharge
            // 
            this.textBox_fuel_surcharge.Location = new System.Drawing.Point(282, 485);
            this.textBox_fuel_surcharge.Name = "textBox_fuel_surcharge";
            this.textBox_fuel_surcharge.Size = new System.Drawing.Size(58, 22);
            this.textBox_fuel_surcharge.TabIndex = 22;
            this.textBox_fuel_surcharge.Text = "0.00";
            // 
            // checkBox_hazmat
            // 
            this.checkBox_hazmat.AutoSize = true;
            this.checkBox_hazmat.Location = new System.Drawing.Point(70, 442);
            this.checkBox_hazmat.Name = "checkBox_hazmat";
            this.checkBox_hazmat.Size = new System.Drawing.Size(78, 21);
            this.checkBox_hazmat.TabIndex = 21;
            this.checkBox_hazmat.Text = "Hazmat";
            this.checkBox_hazmat.UseVisualStyleBackColor = true;
            // 
            // textBox_hazmat
            // 
            this.textBox_hazmat.Location = new System.Drawing.Point(282, 442);
            this.textBox_hazmat.Name = "textBox_hazmat";
            this.textBox_hazmat.Size = new System.Drawing.Size(58, 22);
            this.textBox_hazmat.TabIndex = 20;
            this.textBox_hazmat.Text = "0.00";
            // 
            // checkBox_temp_control
            // 
            this.checkBox_temp_control.AutoSize = true;
            this.checkBox_temp_control.Location = new System.Drawing.Point(70, 399);
            this.checkBox_temp_control.Name = "checkBox_temp_control";
            this.checkBox_temp_control.Size = new System.Drawing.Size(115, 21);
            this.checkBox_temp_control.TabIndex = 19;
            this.checkBox_temp_control.Text = "Temp Control";
            this.checkBox_temp_control.UseVisualStyleBackColor = true;
            // 
            // textBox_temp_control
            // 
            this.textBox_temp_control.Location = new System.Drawing.Point(282, 399);
            this.textBox_temp_control.Name = "textBox_temp_control";
            this.textBox_temp_control.Size = new System.Drawing.Size(58, 22);
            this.textBox_temp_control.TabIndex = 18;
            this.textBox_temp_control.Text = "0.00";
            // 
            // checkBox_reefer
            // 
            this.checkBox_reefer.AutoSize = true;
            this.checkBox_reefer.Location = new System.Drawing.Point(70, 356);
            this.checkBox_reefer.Name = "checkBox_reefer";
            this.checkBox_reefer.Size = new System.Drawing.Size(73, 21);
            this.checkBox_reefer.TabIndex = 17;
            this.checkBox_reefer.Text = "Reefer";
            this.checkBox_reefer.UseVisualStyleBackColor = true;
            // 
            // textBox_reefer
            // 
            this.textBox_reefer.Location = new System.Drawing.Point(282, 356);
            this.textBox_reefer.Name = "textBox_reefer";
            this.textBox_reefer.Size = new System.Drawing.Size(58, 22);
            this.textBox_reefer.TabIndex = 16;
            this.textBox_reefer.Text = "0.00";
            // 
            // checkBox_heat
            // 
            this.checkBox_heat.AutoSize = true;
            this.checkBox_heat.Location = new System.Drawing.Point(70, 313);
            this.checkBox_heat.Name = "checkBox_heat";
            this.checkBox_heat.Size = new System.Drawing.Size(60, 21);
            this.checkBox_heat.TabIndex = 15;
            this.checkBox_heat.Text = "Heat";
            this.checkBox_heat.UseVisualStyleBackColor = true;
            // 
            // textBox_heat
            // 
            this.textBox_heat.Location = new System.Drawing.Point(282, 313);
            this.textBox_heat.Name = "textBox_heat";
            this.textBox_heat.Size = new System.Drawing.Size(58, 22);
            this.textBox_heat.TabIndex = 14;
            this.textBox_heat.Text = "0.00";
            // 
            // checkBox_pallet_exchange
            // 
            this.checkBox_pallet_exchange.AutoSize = true;
            this.checkBox_pallet_exchange.Location = new System.Drawing.Point(70, 270);
            this.checkBox_pallet_exchange.Name = "checkBox_pallet_exchange";
            this.checkBox_pallet_exchange.Size = new System.Drawing.Size(131, 21);
            this.checkBox_pallet_exchange.TabIndex = 13;
            this.checkBox_pallet_exchange.Text = "Pallet Exchange";
            this.checkBox_pallet_exchange.UseVisualStyleBackColor = true;
            // 
            // textBox_pallet_exchange
            // 
            this.textBox_pallet_exchange.Location = new System.Drawing.Point(282, 270);
            this.textBox_pallet_exchange.Name = "textBox_pallet_exchange";
            this.textBox_pallet_exchange.Size = new System.Drawing.Size(58, 22);
            this.textBox_pallet_exchange.TabIndex = 12;
            this.textBox_pallet_exchange.Text = "0.00";
            // 
            // checkBox_driver_assistance
            // 
            this.checkBox_driver_assistance.AutoSize = true;
            this.checkBox_driver_assistance.Location = new System.Drawing.Point(70, 227);
            this.checkBox_driver_assistance.Name = "checkBox_driver_assistance";
            this.checkBox_driver_assistance.Size = new System.Drawing.Size(140, 21);
            this.checkBox_driver_assistance.TabIndex = 11;
            this.checkBox_driver_assistance.Text = "Driver Assistance";
            this.checkBox_driver_assistance.UseVisualStyleBackColor = true;
            // 
            // textBox_driver_assistance
            // 
            this.textBox_driver_assistance.Location = new System.Drawing.Point(282, 227);
            this.textBox_driver_assistance.Name = "textBox_driver_assistance";
            this.textBox_driver_assistance.Size = new System.Drawing.Size(58, 22);
            this.textBox_driver_assistance.TabIndex = 10;
            this.textBox_driver_assistance.Text = "0.00";
            // 
            // checkBox_lumper
            // 
            this.checkBox_lumper.AutoSize = true;
            this.checkBox_lumper.Location = new System.Drawing.Point(70, 184);
            this.checkBox_lumper.Name = "checkBox_lumper";
            this.checkBox_lumper.Size = new System.Drawing.Size(78, 21);
            this.checkBox_lumper.TabIndex = 9;
            this.checkBox_lumper.Text = "Lumper";
            this.checkBox_lumper.UseVisualStyleBackColor = true;
            // 
            // textBox_lumper
            // 
            this.textBox_lumper.Location = new System.Drawing.Point(282, 184);
            this.textBox_lumper.Name = "textBox_lumper";
            this.textBox_lumper.Size = new System.Drawing.Size(58, 22);
            this.textBox_lumper.TabIndex = 8;
            this.textBox_lumper.Text = "0.00";
            // 
            // checkBox_tailgate_charge
            // 
            this.checkBox_tailgate_charge.AutoSize = true;
            this.checkBox_tailgate_charge.Location = new System.Drawing.Point(70, 141);
            this.checkBox_tailgate_charge.Name = "checkBox_tailgate_charge";
            this.checkBox_tailgate_charge.Size = new System.Drawing.Size(129, 21);
            this.checkBox_tailgate_charge.TabIndex = 7;
            this.checkBox_tailgate_charge.Text = "Tailgate charge";
            this.checkBox_tailgate_charge.UseVisualStyleBackColor = true;
            // 
            // textBox_tailgate_charge
            // 
            this.textBox_tailgate_charge.Location = new System.Drawing.Point(282, 141);
            this.textBox_tailgate_charge.Name = "textBox_tailgate_charge";
            this.textBox_tailgate_charge.Size = new System.Drawing.Size(58, 22);
            this.textBox_tailgate_charge.TabIndex = 6;
            this.textBox_tailgate_charge.Text = "0.00";
            // 
            // checkBox_delivery_appointment
            // 
            this.checkBox_delivery_appointment.AutoSize = true;
            this.checkBox_delivery_appointment.Location = new System.Drawing.Point(70, 98);
            this.checkBox_delivery_appointment.Name = "checkBox_delivery_appointment";
            this.checkBox_delivery_appointment.Size = new System.Drawing.Size(163, 21);
            this.checkBox_delivery_appointment.TabIndex = 5;
            this.checkBox_delivery_appointment.Text = "Delivery appointment";
            this.checkBox_delivery_appointment.UseVisualStyleBackColor = true;
            // 
            // textBox_delivery_appointment
            // 
            this.textBox_delivery_appointment.Location = new System.Drawing.Point(282, 98);
            this.textBox_delivery_appointment.Name = "textBox_delivery_appointment";
            this.textBox_delivery_appointment.Size = new System.Drawing.Size(58, 22);
            this.textBox_delivery_appointment.TabIndex = 4;
            this.textBox_delivery_appointment.Text = "0.00";
            // 
            // checkBox_pickup_appointment
            // 
            this.checkBox_pickup_appointment.AutoSize = true;
            this.checkBox_pickup_appointment.Location = new System.Drawing.Point(70, 55);
            this.checkBox_pickup_appointment.Name = "checkBox_pickup_appointment";
            this.checkBox_pickup_appointment.Size = new System.Drawing.Size(154, 21);
            this.checkBox_pickup_appointment.TabIndex = 3;
            this.checkBox_pickup_appointment.Text = "Pickup appointment";
            this.checkBox_pickup_appointment.UseVisualStyleBackColor = true;
            // 
            // textBox_pickup_appointment
            // 
            this.textBox_pickup_appointment.Location = new System.Drawing.Point(282, 55);
            this.textBox_pickup_appointment.Name = "textBox_pickup_appointment";
            this.textBox_pickup_appointment.Size = new System.Drawing.Size(58, 22);
            this.textBox_pickup_appointment.TabIndex = 2;
            this.textBox_pickup_appointment.Text = "0.00";
            // 
            // checkBox_guaranteed_delivery
            // 
            this.checkBox_guaranteed_delivery.AutoSize = true;
            this.checkBox_guaranteed_delivery.Location = new System.Drawing.Point(70, 12);
            this.checkBox_guaranteed_delivery.Name = "checkBox_guaranteed_delivery";
            this.checkBox_guaranteed_delivery.Size = new System.Drawing.Size(161, 21);
            this.checkBox_guaranteed_delivery.TabIndex = 1;
            this.checkBox_guaranteed_delivery.Text = "Guaranteed Delivery";
            this.checkBox_guaranteed_delivery.UseVisualStyleBackColor = true;
            // 
            // textBox_guaranteed_delivery
            // 
            this.textBox_guaranteed_delivery.Location = new System.Drawing.Point(282, 12);
            this.textBox_guaranteed_delivery.Name = "textBox_guaranteed_delivery";
            this.textBox_guaranteed_delivery.Size = new System.Drawing.Size(58, 22);
            this.textBox_guaranteed_delivery.TabIndex = 0;
            this.textBox_guaranteed_delivery.Text = "0.00";
            // 
            // cb_carrier_name
            // 
            this.cb_carrier_name.FormattingEnabled = true;
            this.cb_carrier_name.Location = new System.Drawing.Point(256, 512);
            this.cb_carrier_name.Name = "cb_carrier_name";
            this.cb_carrier_name.Size = new System.Drawing.Size(265, 24);
            this.cb_carrier_name.TabIndex = 41;
            this.cb_carrier_name.SelectedIndexChanged += new System.EventHandler(this.cb_carrier_name_SelectedIndexChanged);
            // 
            // label_scac
            // 
            this.label_scac.AutoSize = true;
            this.label_scac.Location = new System.Drawing.Point(112, 559);
            this.label_scac.Name = "label_scac";
            this.label_scac.Size = new System.Drawing.Size(44, 17);
            this.label_scac.TabIndex = 42;
            this.label_scac.Text = "SCAC";
            // 
            // txt_scac
            // 
            this.txt_scac.Location = new System.Drawing.Point(256, 555);
            this.txt_scac.Name = "txt_scac";
            this.txt_scac.Size = new System.Drawing.Size(265, 22);
            this.txt_scac.TabIndex = 43;
            this.txt_scac.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_scac_KeyPress);
            // 
            // MyCarrier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(587, 745);
            this.Controls.Add(this.txt_scac);
            this.Controls.Add(this.label_scac);
            this.Controls.Add(this.cb_carrier_name);
            this.Controls.Add(this.panel_special_services);
            this.Controls.Add(this.link_label_special_services);
            this.Controls.Add(this.txt_packaging);
            this.Controls.Add(this.label_packaging);
            this.Controls.Add(this.label_service_name);
            this.Controls.Add(this.txt_service_name);
            this.Controls.Add(this.label_carrier_name);
            this.Controls.Add(this.label_bottom);
            this.Controls.Add(this.txt_total_other_surcharges);
            this.Controls.Add(this.txt_fuel);
            this.Controls.Add(this.txt_discount);
            this.Controls.Add(this.txt_list_tarrif);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkbox_reuse);
            this.Controls.Add(this.groupbox_discount_type);
            this.Controls.Add(this.groupbox_fuel_type);
            this.Controls.Add(this.groupBox_package_type);
            this.Controls.Add(this.cb_bare_code_symbology);
            this.Controls.Add(this.txt_billing_account);
            this.Controls.Add(this.txt_pro_number);
            this.Controls.Add(this.txt_tacking_number);
            this.Controls.Add(this.label_package_type);
            this.Controls.Add(this.label_pro_number);
            this.Controls.Add(this.label_bar_code_symbology);
            this.Controls.Add(this.label_billing_account);
            this.Controls.Add(this.label_tracking_number);
            this.Controls.Add(this.label_total_other_surcharges);
            this.Controls.Add(this.label_fuel_type);
            this.Controls.Add(this.label_fuel);
            this.Controls.Add(this.label_discount_type);
            this.Controls.Add(this.label_discount);
            this.Controls.Add(this.label_list_tarrif);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(605, 792);
            this.MinimumSize = new System.Drawing.Size(605, 792);
            this.Name = "MyCarrier";
            this.Text = "MyCarrier";
            this.Load += new System.EventHandler(this.MyCarrier_Load);
            this.groupBox_package_type.ResumeLayout(false);
            this.groupBox_package_type.PerformLayout();
            this.groupbox_fuel_type.ResumeLayout(false);
            this.groupbox_fuel_type.PerformLayout();
            this.groupbox_discount_type.ResumeLayout(false);
            this.groupbox_discount_type.PerformLayout();
            this.panel_special_services.ResumeLayout(false);
            this.panel_special_services.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_list_tarrif;
        private System.Windows.Forms.Label label_discount;
        private System.Windows.Forms.Label label_discount_type;
        private System.Windows.Forms.Label label_fuel;
        private System.Windows.Forms.Label label_fuel_type;
        private System.Windows.Forms.Label label_total_other_surcharges;
        private System.Windows.Forms.Label label_tracking_number;
        private System.Windows.Forms.Label label_billing_account;
        private System.Windows.Forms.Label label_bar_code_symbology;
        private System.Windows.Forms.Label label_pro_number;
        private System.Windows.Forms.Label label_package_type;
        private System.Windows.Forms.TextBox txt_tacking_number;
        private System.Windows.Forms.TextBox txt_pro_number;
        private System.Windows.Forms.TextBox txt_billing_account;
        private System.Windows.Forms.ComboBox cb_bare_code_symbology;
        private System.Windows.Forms.GroupBox groupBox_package_type;
        private System.Windows.Forms.RadioButton radiob_same_day;
        private System.Windows.Forms.RadioButton radiob_ltl;
        private System.Windows.Forms.RadioButton radiob_small_package;
        private System.Windows.Forms.GroupBox groupbox_fuel_type;
        private System.Windows.Forms.RadioButton radiob_fuel_type_procent;
        private System.Windows.Forms.RadioButton radiob_fuel_type_diez;
        private System.Windows.Forms.GroupBox groupbox_discount_type;
        private System.Windows.Forms.RadioButton radiob_discount_type_procent;
        private System.Windows.Forms.RadioButton radiob_discount_type_diez;
        private System.Windows.Forms.CheckBox checkbox_reuse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.TextBox txt_list_tarrif;
        private System.Windows.Forms.TextBox txt_discount;
        private System.Windows.Forms.TextBox txt_fuel;
        private System.Windows.Forms.TextBox txt_total_other_surcharges;
        private System.Windows.Forms.Label label_bottom;
        private System.Windows.Forms.Label label_carrier_name;
        private System.Windows.Forms.TextBox txt_service_name;
        private System.Windows.Forms.Label label_service_name;
        private System.Windows.Forms.Label label_packaging;
        private System.Windows.Forms.TextBox txt_packaging;
        private System.Windows.Forms.LinkLabel link_label_special_services;
        private System.Windows.Forms.Panel panel_special_services;
        private System.Windows.Forms.CheckBox checkBox_tradeshow;
        private System.Windows.Forms.TextBox textBox_tradeshow;
        private System.Windows.Forms.CheckBox checkBox_special_equipment_needed;
        private System.Windows.Forms.TextBox textBox_special_equipment_needed;
        private System.Windows.Forms.CheckBox checkBox_saturday_pickup_or_delivery;
        private System.Windows.Forms.TextBox textBox_saturday_pickup_or_delivery;
        private System.Windows.Forms.CheckBox checkBox_residential_delivery;
        private System.Windows.Forms.TextBox textBox_residential_delivery;
        private System.Windows.Forms.CheckBox checkBox_pallet;
        private System.Windows.Forms.TextBox textBox_pallet;
        private System.Windows.Forms.CheckBox checkBox_premium_air_freight;
        private System.Windows.Forms.TextBox textBox_premium_air_freight;
        private System.Windows.Forms.CheckBox checkBox_marking_and_tagging;
        private System.Windows.Forms.TextBox textBox_marking_and_tagging;
        private System.Windows.Forms.CheckBox checkBox_liquor_permit;
        private System.Windows.Forms.TextBox textBox_liquor_permit;
        private System.Windows.Forms.CheckBox checkBox_lift_gate_origin;
        private System.Windows.Forms.TextBox textBox_lift_gate_origin;
        private System.Windows.Forms.CheckBox checkBox_lift;
        private System.Windows.Forms.TextBox textBox_lift;
        private System.Windows.Forms.CheckBox checkBox_inside_pickup;
        private System.Windows.Forms.TextBox textBox_inside_pickup;
        private System.Windows.Forms.CheckBox checkBox_holiday_delivery;
        private System.Windows.Forms.TextBox textBox_holiday_delivery;
        private System.Windows.Forms.CheckBox checkBox_guaranteed;
        private System.Windows.Forms.TextBox textBox_guaranteed;
        private System.Windows.Forms.CheckBox checkBox_expedited_shipment;
        private System.Windows.Forms.TextBox textBox_expedited_shipment;
        private System.Windows.Forms.CheckBox checkBox_emergency_delivery;
        private System.Windows.Forms.TextBox textBox_emergency_delivery;
        private System.Windows.Forms.CheckBox checkBox_dry_run;
        private System.Windows.Forms.TextBox textBox_dry_run;
        private System.Windows.Forms.CheckBox checkBox_driver_load;
        private System.Windows.Forms.TextBox textBox_driver_load;
        private System.Windows.Forms.CheckBox checkBox_driver_load_and_unload;
        private System.Windows.Forms.TextBox textBox_driver_load_and_unload;
        private System.Windows.Forms.CheckBox checkBox_cross_dock;
        private System.Windows.Forms.TextBox textBox_cross_dock;
        private System.Windows.Forms.CheckBox checkBox_commercial_pickup;
        private System.Windows.Forms.TextBox textBox_commercial_pickup;
        private System.Windows.Forms.CheckBox checkBox_bobtail;
        private System.Windows.Forms.TextBox textBox_bobtail;
        private System.Windows.Forms.CheckBox checkBox_bag_liner;
        private System.Windows.Forms.TextBox textBox_bag_liner;
        private System.Windows.Forms.CheckBox checkBox_appointment;
        private System.Windows.Forms.TextBox textBox_appointment;
        private System.Windows.Forms.CheckBox checkBox_live_loading_or_unloading;
        private System.Windows.Forms.TextBox textBox_live_loading_or_unloading;
        private System.Windows.Forms.CheckBox checkBox_team_required;
        private System.Windows.Forms.TextBox textBox_team_required;
        private System.Windows.Forms.CheckBox checkBox_stop_off;
        private System.Windows.Forms.TextBox textBox_stop_off;
        private System.Windows.Forms.CheckBox checkBox_residential_pickup;
        private System.Windows.Forms.TextBox textBox_residential_pickup;
        private System.Windows.Forms.CheckBox checkBox_protective;
        private System.Windows.Forms.TextBox textBox_protective;
        private System.Windows.Forms.CheckBox checkBox_notification;
        private System.Windows.Forms.TextBox textBox_notification;
        private System.Windows.Forms.CheckBox checkBox_limited_delivery_or_military;
        private System.Windows.Forms.TextBox textBox_limited_delivery_or_military;
        private System.Windows.Forms.CheckBox checkBox_lift_gate_destination;
        private System.Windows.Forms.TextBox textBox_lift_gate_destination;
        private System.Windows.Forms.CheckBox checkBox_limited_access_delivery;
        private System.Windows.Forms.TextBox textBox_limited_access_delivery;
        private System.Windows.Forms.CheckBox checkBox_inside_delivery;
        private System.Windows.Forms.TextBox textBox_inside_delivery;
        private System.Windows.Forms.CheckBox checkBox_handling_and_labeling;
        private System.Windows.Forms.TextBox textBox_handling_and_labeling;
        private System.Windows.Forms.CheckBox checkBox_forklift;
        private System.Windows.Forms.TextBox textBox_forklift;
        private System.Windows.Forms.CheckBox checkBox_expedited_same_dy_snc;
        private System.Windows.Forms.TextBox textBox_expedited_same_dy_snc;
        private System.Windows.Forms.CheckBox checkBox_driver_unload_and_count;
        private System.Windows.Forms.TextBox textBox_driver_unload_and_count;
        private System.Windows.Forms.CheckBox checkBox_driver_unload;
        private System.Windows.Forms.TextBox textBox_driver_unload;
        private System.Windows.Forms.CheckBox checkBox_drop_pull;
        private System.Windows.Forms.TextBox textBox_drop_pull;
        private System.Windows.Forms.CheckBox checkBox_driver_load_and_count;
        private System.Windows.Forms.TextBox textBox_driver_load_and_count;
        private System.Windows.Forms.CheckBox checkBox_crane_permits;
        private System.Windows.Forms.TextBox textBox_crane_permits;
        private System.Windows.Forms.CheckBox checkBox_cross_border;
        private System.Windows.Forms.TextBox textBox_cross_border;
        private System.Windows.Forms.CheckBox checkBox_commercial_delivery;
        private System.Windows.Forms.TextBox textBox_commercial_delivery;
        private System.Windows.Forms.CheckBox checkBox_bulkhead;
        private System.Windows.Forms.TextBox textBox_bulkhead;
        private System.Windows.Forms.CheckBox checkBox_air_bag;
        private System.Windows.Forms.TextBox textBox_air_bag;
        private System.Windows.Forms.CheckBox checkBox_after_hours_delivery;
        private System.Windows.Forms.TextBox textBox_after_hours_delivery;
        private System.Windows.Forms.CheckBox checkBox_fuel_surcharge;
        private System.Windows.Forms.TextBox textBox_fuel_surcharge;
        private System.Windows.Forms.CheckBox checkBox_hazmat;
        private System.Windows.Forms.TextBox textBox_hazmat;
        private System.Windows.Forms.CheckBox checkBox_temp_control;
        private System.Windows.Forms.TextBox textBox_temp_control;
        private System.Windows.Forms.CheckBox checkBox_reefer;
        private System.Windows.Forms.TextBox textBox_reefer;
        private System.Windows.Forms.CheckBox checkBox_heat;
        private System.Windows.Forms.TextBox textBox_heat;
        private System.Windows.Forms.CheckBox checkBox_pallet_exchange;
        private System.Windows.Forms.TextBox textBox_pallet_exchange;
        private System.Windows.Forms.CheckBox checkBox_driver_assistance;
        private System.Windows.Forms.TextBox textBox_driver_assistance;
        private System.Windows.Forms.CheckBox checkBox_lumper;
        private System.Windows.Forms.TextBox textBox_lumper;
        private System.Windows.Forms.CheckBox checkBox_tailgate_charge;
        private System.Windows.Forms.TextBox textBox_tailgate_charge;
        private System.Windows.Forms.CheckBox checkBox_delivery_appointment;
        private System.Windows.Forms.TextBox textBox_delivery_appointment;
        private System.Windows.Forms.CheckBox checkBox_pickup_appointment;
        private System.Windows.Forms.TextBox textBox_pickup_appointment;
        private System.Windows.Forms.CheckBox checkBox_guaranteed_delivery;
        private System.Windows.Forms.TextBox textBox_guaranteed_delivery;
        private System.Windows.Forms.ComboBox cb_carrier_name;
        private System.Windows.Forms.Label label_scac;
        private System.Windows.Forms.TextBox txt_scac;
    }
}