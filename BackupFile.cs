﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.Odbc;
using System.Linq.Expressions;
using System.Reflection;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace InteGr8
{
    public static class BackupFile
    {
        static OdbcConnection Conn = null;
        static System.Data.DataSet data_temp = new System.Data.DataSet();
        static string UseMSBackup = ConfigurationManager.AppSettings["UseMSBackup"].ToString();
        static string BackupConn = ConfigurationManager.AppSettings["BackupConn"].ToString();
        private static string ReadColumn(string row, string column)
        {
            if (String.IsNullOrEmpty(row) || String.IsNullOrEmpty(column))
            {
                //throw new ArgumentException();
                return "";
            }
            string[] tags = row.Split('\t');
            foreach (string tag in tags)
            {
                if (tag.Split('|')[0].Equals(column))
                {
                    return tag.Split('|')[1];
                }
            }
            return "";
        }

        public static void LoadSettings(string userid)
        {
            System.IO.File.AppendAllText("Error.txt", "\r\n Loading settings \r\n");

            try
            {
                if (UseMSBackup.Equals("1"))
                {
                   
                        if (Conn != null)
                        {
                            Conn.Close();
                        }
                        Conn = new OdbcConnection(Template.BackupConn.ConnStr);
                        Conn.Open();
                    using (OdbcCommand Comm = new OdbcCommand())
                    {
                        System.IO.File.AppendAllText("Error.txt", "\r\n");
                        System.IO.File.AppendAllText("Error.txt", "********************************************************\r\n");

                        System.IO.File.AppendAllText("Error.txt", "Loading settings for user: " + userid + "on workstation: " + System.Environment.MachineName.ToString() + "\r\n");
                        System.IO.File.AppendAllText("Error.txt", "\r\n");

                        Comm.Connection = Conn;
                        Comm.CommandText = "select * from settings_table s where s.workstation = '" + System.Environment.MachineName.ToString() +"' and s.userid = '" + userid + "'";

                        //Comm.Parameters.AddWithValue("@p1", System.Environment.MachineName.ToString());
                        //Comm.Parameters.AddWithValue("@p2", userid);

                        System.IO.File.AppendAllText("Error.txt", "Command text:" + Comm.CommandText + " \r\n");
                        //System.IO.File.AppendAllText("Error.txt", "Parameters: " + Comm.Parameters[0].ParameterName + "with value " + Comm.Parameters[0].Value + "\r\n");
                        //System.IO.File.AppendAllText("Error.txt", "Parameters: " + Comm.Parameters[1].ParameterName + "with value " + Comm.Parameters[1].Value + "\r\n");

                        OdbcDataReader temp_data = Comm.ExecuteReader();

                        DataTable table = new DataTable();
                        table.Load(temp_data);
                        System.IO.File.AppendAllText("Error.txt", "Loaded :" + table.Rows.Count + " settings\r\n");

                        foreach (DataRow r in table.Rows)
                        {
                            if (r["name"].ToString() == "Emails")
                            {
                                continue;
                            }

                            FieldInfo field = typeof(Template).GetField(r["name"].ToString());
                            if (field != null)
                            {
                                System.IO.File.AppendAllText("Error.txt", r["name"].ToString() + " with value: " + r["value"].ToString() + "\r\n");

                                if (r["name"].ToString() == "UseAutoprint")
                                {
                                    if (r["value"].ToString() == "True" && !Utils.IsAdobe(false))
                                    {
                                        System.IO.File.AppendAllText("Error.txt", "Set autoprint, useAdobe value: " + Utils.IsAdobe(false) + "\r\n");

                                        field.SetValue(null, false);
                                    }
                                    else
                                    {
                                        field.SetValue(null, field.FieldType == typeof(System.Boolean) ? (r["value"].ToString() == "True" ? true : false) : r["value"]);
                                    }
                                }
                                else
                                {
                                    field.SetValue(null, field.FieldType == typeof(System.Boolean) ? (r["value"].ToString() == "True" ? true : false) : r["value"]);
                                }


                            }
                        }
                    }
                }
                else //MySQL backup
                {
                    System.IO.File.AppendAllText("Error.txt", "\r\n");
                    System.IO.File.AppendAllText("Error.txt", "********************************************************\r\n");

                    //MySqlConnection myConn = new MySqlConnection(BackupConn);//("Server=localhost;Database=backup;Uid=root;Pwd=!Sysadm27;");
                    //myConn.Open();
                    if (Conn != null)
                    {
                        Conn.Close();
                    }
                    Conn = new OdbcConnection(Template.BackupConn.ConnStr);
                    Conn.Open();
                    //using (MySqlCommand Comm = new MySqlCommand())
                    using (OdbcCommand Comm = new OdbcCommand())
                    {
                        System.IO.File.AppendAllText("Error.txt", "Loading settings for user: " + userid + "on workstation: " + System.Environment.MachineName.ToString() + "\r\n");
                        System.IO.File.AppendAllText("Error.txt", "\r\n");
                        Comm.Connection = Conn;//myConn;
                        Comm.CommandText = $@"select * from settings_table where workstation ='{System.Environment.MachineName}' and userid = '{userid}' ;";//"CALL LoadSettings(?,?);";//"select * from settings_table where workstation = ? and userid = ?";
                        //Comm.Parameters.Add(new MySqlParameter("@machineName", System.Environment.MachineName.ToString()));
                        //Comm.Parameters.Add(new MySqlParameter("@user", userid));
                        System.IO.File.AppendAllText("Error.txt", "Command text:" + Comm.CommandText + " \r\n");
                        //System.IO.File.AppendAllText("Error.txt", "Parameters: " + Comm.Parameters[0].ParameterName + "with value " + Comm.Parameters[0].Value + "\r\n");
                        // System.IO.File.AppendAllText("Error.txt", "Parameters: " + Comm.Parameters[1].ParameterName + "with value " + Comm.Parameters[1].Value + "\r\n");
                        //Comm.ExecuteNonQuery();
                        //MySqlDataReader temp_data = Comm.ExecuteReader();
                        OdbcDataReader temp_data = Comm.ExecuteReader();

                        DataTable table = new DataTable();
                        table.Load(temp_data);
                        System.IO.File.AppendAllText("Error.txt", "Loaded :" + table.Rows.Count + " settings\r\n");

                        foreach (DataRow r in table.Rows)
                        {
                            if (r["name"].ToString() == "Emails")
                            {
                                continue;
                            }

                            FieldInfo field = typeof(Template).GetField(r["name"].ToString());
                            if (field != null)
                            {
                                System.IO.File.AppendAllText("Error.txt", r["name"].ToString() + " with value: " + r["value"].ToString() + "\r\n");

                                if (r["name"].ToString() == "UseAutoprint")
                                {
                                    if (r["value"].ToString() == "True" && !Utils.IsAdobe(false))
                                    {
                                        System.IO.File.AppendAllText("Error.txt", "Set autoprint, useAdobe value: " + Utils.IsAdobe(false) + "\r\n");

                                        field.SetValue(null, false);
                                    }
                                    else
                                    {
                                        field.SetValue(null, field.FieldType == typeof(System.Boolean) ? (r["value"].ToString() == "True" ? true : false) : r["value"]);
                                    }
                                }
                                else
                                {
                                    field.SetValue(null, field.FieldType == typeof(System.Boolean) ? (r["value"].ToString() == "True" ? true : false) : r["value"]);
                                }


                            }
                        }
                    }
                    }
                }
            catch (Exception e2)
            {
                Utils.SendError("Error at loading settings from backup", e2);
            }
        }

        public static void SaveSetting(string name, string userid)
        {
            try
            {
                if (Conn != null)
                {
                    Conn.Close();
                }
                Conn = new OdbcConnection(Template.BackupConn.ConnStr);
                Conn.Open();
                
            }
            catch (Exception e1)
            {
                Utils.SendError("Error at opening ODBC connection at Save Settings", e1);
            }
            DataTable table = new DataTable();
            try
            {
                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;
                    Comm.CommandText = "select * from settings_table s where name = '" + name +"' and s.workstation = '" + System.Environment.MachineName.ToString() + "' and s.userid = '" + userid + "'";

                    //Comm.CommandText = "select * from settings_table where name = ? and workstation = ? and userid = ?";
                    //Comm.Parameters.Add(new OdbcParameter("", name));
                    //Comm.Parameters.Add(new OdbcParameter("", System.Environment.MachineName.ToString()));
                    //Comm.Parameters.Add(new OdbcParameter("", userid));
                    OdbcDataReader temp_data = Comm.ExecuteReader();

                    table.Load(temp_data);
                    //Comm.Dispose();
                    temp_data.Close();
                    temp_data.Dispose();
                }
            }
            catch (Exception e2)
            {
                Utils.SendError("Error at retrieving current settings from database at save Settings", e2);
            }
            try
            {
                using (OdbcCommand Comm2 = new OdbcCommand())
                {
                    Comm2.Connection = Conn;
                    if (table.Rows.Count > 0)
                    {
                        if (table.Rows[0]["value"].ToString() != typeof(Template).GetField(name).GetValue(null).ToString())
                        {
                            Comm2.Parameters.Clear();
                            Comm2.CommandText = "update settings_table set value = ? where name = ? and workstation = ? and userid = ?";
                            Comm2.Parameters.Add(new OdbcParameter("", typeof(Template).GetField(name).GetValue(null).ToString()));
                            Comm2.Parameters.Add(new OdbcParameter("", name));
                            Comm2.Parameters.Add(new OdbcParameter("", System.Environment.MachineName.ToString()));
                            Comm2.Parameters.Add(new OdbcParameter("", userid));
                            Comm2.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        Comm2.Parameters.Clear();
                        Comm2.CommandText = "insert into settings_table (name, value, workstation, userid, superuser) values (?, ?, ?, ?, ?)";
                        Comm2.Parameters.Add(new OdbcParameter("", name));
                        Comm2.Parameters.Add(new OdbcParameter("", typeof(Template).GetField(name).GetValue(null).ToString()));
                        Comm2.Parameters.Add(new OdbcParameter("", System.Environment.MachineName.ToString()));
                        Comm2.Parameters.Add(new OdbcParameter("", userid));
                        Comm2.Parameters.Add(new OdbcParameter("", "false"));
                        Comm2.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e3)
            {
                Utils.SendError("Error at saving settings", e3);
            }
        }

        public static void LoadSettingList(string name, string userid)
        {
            try
            {
                if (Conn != null)
                {
                    Conn.Close();
                }
                Conn = new OdbcConnection(Template.BackupConn.ConnStr);
                Conn.Open();

            }
            catch (Exception e1)
            {
                Utils.SendError("Error at opening ODBC connection at load settings list", e1);
            }
            DataTable table = new DataTable();
            try
            {
                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;
                    Comm.CommandText = "select * from settings_table where name = ? and userid = ?";
                    Comm.Parameters.Add(new OdbcParameter("", name));
                    Comm.Parameters.Add(new OdbcParameter("", userid));
                    OdbcDataReader temp_data = Comm.ExecuteReader();

                    table.Load(temp_data);
                    Comm.Parameters.Clear();
                    //Comm.Dispose();
                    temp_data.Close();
                    temp_data.Dispose();
                }
            }
            catch (Exception e2)
            {
                Utils.SendError("Error at loading settings list from database", e2);
            }
                if (name == "Emails")
                {
                    InteGr8_Main.data.Emails.Clear();
                    foreach (DataRow r in table.Rows)
                    {
                        InteGr8_Main.data.Emails.AddEmailsRow(userid, r["value"].ToString());
                    }
                    InteGr8_Main.data.Emails.AcceptChanges();
                }
                if (name == "Carriers")
                {
                    InteGr8_Main.data.MyCarrierNames.Clear();
                    foreach (DataRow r in table.Rows)
                    {
                        InteGr8_Main.data.MyCarrierNames.AddMyCarrierNamesRow(r["value"].ToString().Split(',')[0], Convert.ToBoolean(r["value"].ToString().Split(',')[2]), r["value"].ToString().Split(',')[1]);
                    }
                    InteGr8_Main.data.MyCarrierNames.AcceptChanges();
                }
           
        }

        public static void SaveSettingList(string name, List<string> list, string userid)
        {
            try
            {
                if (Conn != null)
                {
                    Conn.Close();
                }
                Conn = new OdbcConnection(Template.BackupConn.ConnStr);
                Conn.Open();

            }
            catch (Exception e1)
            {
                Utils.SendError("Error at opening ODBC connection at Save Settings List", e1);
            }
            DataTable table = new DataTable();
            try
            {
                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;

                    Comm.CommandText = "select * from settings_table where name = ? and userid = ?";
                    Comm.Parameters.Add(new OdbcParameter("", name));
                    Comm.Parameters.Add(new OdbcParameter("", userid));
                    OdbcDataReader temp_data = Comm.ExecuteReader();

                    table.Load(temp_data);
                    Comm.Parameters.Clear();
                    //Comm.Dispose();
                    temp_data.Close();
                    temp_data.Dispose();

                }
            }
            catch (Exception e2)
            {
                Utils.SendError("Exception at retrieving current settings list", e2);
            }
            try
            {
                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.CommandText = "delete from settings_table where name = ? and userid = ? and value like ?";

                    foreach (DataRow r in table.Rows)
                    {
                        if (list.Contains(r["value"].ToString()))
                        {
                            continue;
                        }
                        else
                        {
                            Comm.Parameters.Add(new OdbcParameter("", name));
                            Comm.Parameters.Add(new OdbcParameter("", userid));
                            Comm.Parameters.Add(new OdbcParameter("", r["value"].ToString()));
                            Comm.ExecuteNonQuery();
                            Comm.Parameters.Clear();
                        }
                    }
                    //Comm.Dispose();
                }
                using (OdbcCommand Comm2 = new OdbcCommand())
                {
                    Comm2.Connection = Conn;
                    foreach (string s in list)
                    {
                        DataRow[] r = table.Select("value = '" + s + "'");
                        if (r.Length == 0)
                        {
                            Comm2.Parameters.Clear();
                            Comm2.CommandText = "insert into settings_table (name, value, workstation, userid, superuser) values (?, ?, ?, ?, ?)";
                            Comm2.Parameters.Add(new OdbcParameter("", name));
                            Comm2.Parameters.Add(new OdbcParameter("", s));
                            Comm2.Parameters.Add(new OdbcParameter("", System.Environment.MachineName.ToString()));
                            Comm2.Parameters.Add(new OdbcParameter("", userid));
                            Comm2.Parameters.Add(new OdbcParameter("", "false"));
                            Comm2.ExecuteNonQuery();
                        }
                    }
                    //Comm.Dispose();
                }
            }
            catch (Exception e3)
            {
                Utils.SendError("Exception at saving settings list", e3);
            }
        }

        public static bool Load(Data data)
        {
            if (!Template.Backup_Orders)
            {
                return false;
            }
            //load backup-ed shipments only at app. startup, at click on reload, they are already loaded
            //the same at loading a new filtered order(scan-in one by one, and ship, then load next order)
            //System.IO.File.AppendAllText("Error.txt", "Load shipments from backup: " + DateTime.Now + "\r\n");
            if (data.ShipmentsTable != null)
            {
                if (data.ShipmentsTable.Rows.Count > 0)
                {
                    return true;
                }
            }
            else
            {
                return true;
            }

            data.ShipmentsTable.Clear();
            try
            {
                if (Conn != null)
                {
                    Conn.Close();
                }
                Conn = new OdbcConnection(Template.BackupConn.ConnStr);
                Conn.Open();

            }
            catch (Exception e1)
            {
                Utils.SendError("Error at opening ODBC connection at Load from backup", e1);
            }
            try
            {
                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;
                    Comm.CommandText = "select distinct table_name from shared_backup";
                    OdbcDataReader temp_data = Comm.ExecuteReader();
                    DataTable table = new DataTable();
                    table.Load(temp_data);
                    foreach (DataRow r in table.Rows)
                    {
                        if (!data_temp.Tables.Contains(r["table_name"].ToString()))
                        {
                            data_temp.Tables.Add(data.Tables[r["table_name"].ToString()].Copy());
                            data_temp.Tables[r["table_name"].ToString()].Clear();
                        }
                    }
                    Comm.CommandText = "SELECT * FROM shared_backup where status = 'saved'";
                    table.Clear();
                    temp_data.Close();
                    System.IO.File.AppendAllText("Error.txt", "Get all shipments: " + DateTime.Now + "\r\n");
          
                    temp_data = Comm.ExecuteReader();
                    System.IO.File.AppendAllText("Error.txt", "Restore all: " + DateTime.Now + "\r\n");
          
                    table.Load(temp_data);
                    foreach (DataRow r in table.Rows)
                    {
                        StringReader sr = new StringReader(r["xml"].ToString());
                        DataSet temp = new DataSet();
                        temp.ReadXml(sr);
                        if (data_temp.Tables[r["table_name"].ToString()].Columns.Count > temp.Tables[0].Columns.Count)
                        {
                            foreach (DataColumn c in data_temp.Tables[r["table_name"].ToString()].Columns)
                            {
                                if (!temp.Tables[0].Columns.Contains(c.ColumnName))
                                {
                                    temp.Tables[0].Columns.Add(c.ColumnName, c.DataType);
                                }
                            }
                        }
                        if (data_temp.Tables[r["table_name"].ToString()].Columns.Count < temp.Tables[0].Columns.Count)
                        {
                            foreach (DataColumn c in temp.Tables[0].Columns)
                            {
                                if (!data_temp.Tables[r["table_name"].ToString()].Columns.Contains(c.ColumnName))
                                {
                                    data_temp.Tables[r["table_name"].ToString()].Columns.Add(c.ColumnName, c.DataType);
                                }
                            }
                        }
                        foreach (DataRow rr in temp.Tables[0].Rows)
                        {
                            data_temp.Tables[temp.Tables[0].TableName].ImportRow(rr);
                        }
                    }
                }
            }
            catch (Exception e2)
            {
                Utils.SendError("Error at loading data from backup", e2);
            }

            foreach (DataTable t in data_temp.Tables)
            {
                if (data.Tables[t.TableName].Columns.Count != t.Columns.Count)
                {
                    foreach (DataColumn c in t.Columns)
                    {
                        if (!data.Tables[t.TableName].Columns.Contains(c.ColumnName))
                        {
                            data.Tables[t.TableName].Columns.Add(c.ColumnName, c.DataType);
                        }
                    }
                }
            }
            if (data_temp.Tables.Count > 0)
            {
                if (data_temp.Tables.Contains(data.ProductsSerialNumbers.TableName))
                {
                    if (data_temp.Tables[data.ProductsSerialNumbers.TableName].Rows.Count > 0)
                    {
                        data.ProductsSerialNumbers.Merge(data_temp.Tables[data.ProductsSerialNumbers.TableName]);
                    }
                }
                if (data_temp.Tables.Contains(data.ProductsInPackages.TableName))
                {
                    if (data_temp.Tables[data.ProductsInPackages.TableName].Rows.Count > 0)
                    {
                        data.ProductsInPackages.Merge(data_temp.Tables[data.ProductsInPackages.TableName]);
                    }
                }
                if (data_temp.Tables.Contains(data.ShipmentsTable.TableName))
                {
                    foreach (DataRow r in data_temp.Tables[data.ShipmentsTable.TableName].Rows)
                    {
                        Data.ShipmentsTableRow temp = data.ShipmentsTable.FindBy_Order__Package_Index(r["Order #"].ToString(), Convert.ToInt32(r["Package_Index"].ToString()));
                        if (temp == null)
                        {
                            data.ShipmentsTable.ImportRow(r);
                        }
                    }
                }
            }

            data_temp.Clear();
           // System.IO.File.AppendAllText("Error.txt", "Backup loaded: " + DateTime.Now + "\r\n");
          
            //if (Conn != null)
            //{
            //    Conn.Close();
            //}
            return true;
        }

        public static void AddShipment(string Table, System.Data.DataRow row, Data data)
        {
            if (!Template.Backup_Orders)
            {
                return;
            }
            try
            {
                if (Conn != null)
                {
                    Conn.Close();
                }
                Conn = new OdbcConnection(Template.BackupConn.ConnStr);
                Conn.Open();

            }
            catch (Exception e1)
            {
                Utils.SendError("Error at opening ODBC connection at add shipment to backup", e1);
            }
            try
            {
                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;
                    AddTable(Table, data);
                    data_temp.Clear();
                    data_temp.Tables[Table].ImportRow(row);
                    StringWriter sw = new StringWriter();
                    data_temp.Tables[Table].WriteXml(sw);
                    Comm.CommandText = "INSERT INTO shared_backup (xml, date, status, ord_id, station_name, table_name) values (?, CURRENT_TIMESTAMP, ?, ?, ?, ?)";
                    Comm.Parameters.Add(new OdbcParameter("", sw.ToString()));
                    Comm.Parameters.Add(new OdbcParameter("", "saved"));
                    Comm.Parameters.Add(new OdbcParameter("", row["Order #"].ToString()));
                    Comm.Parameters.Add(new OdbcParameter("", System.Environment.MachineName.ToString()));
                    Comm.Parameters.Add(new OdbcParameter("", Table));
                    //Comm.ExecuteNonQuery();
                   // System.IO.File.AppendAllText("Error.txt", " B1: " + DateTime.Now + "\r\n");
         
                    Comm.ExecuteNonQueryAsync();
                    Comm.CommandText = "INSERT INTO shared_backup2 (xml, date, status, ord_id, station_name, table_name) values (?, CURRENT_TIMESTAMP, ?, ?, ?, ?)";
                    //Comm.ExecuteNonQuery();
                   // System.IO.File.AppendAllText("Error.txt", " B2: " + DateTime.Now + "\r\n");
         
                    Comm.ExecuteNonQueryAsync();
                    //System.IO.File.AppendAllText("Error.txt", " U: " + DateTime.Now + "\r\n");
         
                    Comm.Parameters.Clear();
                    data_temp.Clear();
                }
            }
            catch (Exception e2)
            {
                Utils.SendError("Error at insert into shared backup and shared backup2", e2);
            }
        }

        public static void AddTable(string Table, Data data)
        {
            if (!data_temp.Tables.Contains(Table))
            {
                data_temp.Tables.Add(data.Tables[Table].Copy());
                data_temp.Tables[Table].Clear();
            }
            else
            {
                if (data_temp.Tables[Table].Rows.Count == 0)
                {
                    data_temp.Tables.Remove(Table);
                    data_temp.Tables.Add(data.Tables[Table].Copy());
                    data_temp.Tables[Table].Clear();
                }
            }
        }

        public static void RemoveShipment(string Table, string Status, string order_number, Data data)
        {
            if (!Template.Backup_Orders)
            {
                return;
            }
            try
            {
                if (Conn != null)
                {
                    Conn.Close();
                }
                Conn = new OdbcConnection(Template.BackupConn.ConnStr);
                Conn.Open();

            }
            catch (Exception e1)
            {
                Utils.SendError("Error at opening ODBC connection at remove shipment from backup", e1);
            }
            try
            {
                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;
                    AddTable(Table, data);
                    //data_temp.Clear();
                    //data_temp.Tables[Table].ImportRow(row);
                    //StringWriter sw = new StringWriter();
                    //data_temp.Tables[Table].WriteXml(sw);
                    Comm.CommandText = "update shared_backup set status = ? where ord_id = ? and table_name = ?";
                    Comm.Parameters.Add(new OdbcParameter("", Status));
                    Comm.Parameters.Add(new OdbcParameter("", order_number));
                    Comm.Parameters.Add(new OdbcParameter("", Table));
                    Comm.ExecuteNonQuery();
                    Comm.CommandText = "update shared_backup2 set status = ? where ord_id = ? and table_name = ?";
                    Comm.ExecuteNonQuery();
                    Comm.Parameters.Clear();
                    data_temp.Clear();
                }
            }
            catch (Exception e2)
            {
                Utils.SendError("Error at removing shipment from backup", e2);
            }
        }
    }

    public static class MemberInfoGetting
    {
        public static string GetMemberName<T>(Expression<Func<T>> memberExpression)
        {
            MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
            return expressionBody.Member.Name;
        }
    }
}