﻿namespace InteGr8
{
    partial class frmAddManualOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddManualOrder));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbPackaging = new System.Windows.Forms.ComboBox();
            this.PackagingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.data1 = new InteGr8.Data();
            this.label47 = new System.Windows.Forms.Label();
            this.cmbBillingType = new System.Windows.Forms.ComboBox();
            this.Billing_TypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label46 = new System.Windows.Forms.Label();
            this.cmbService = new System.Windows.Forms.ComboBox();
            this.servicesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.cmbCarrier = new System.Windows.Forms.ComboBox();
            this.carriersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.tbShipmentPONumber = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbShipmentRefInfo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbOrderNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnManualOrderCancel = new System.Windows.Forms.Button();
            this.btnManualOrderOK = new System.Windows.Forms.Button();
            this.gbProducts = new System.Windows.Forms.GroupBox();
            this.tbProductTotalValue = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.tbProductHarmonizedCode = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tbProductTotalWeight = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tbProductUnitValue = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tbProductUnitWeight = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tbProductMU = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tbProductQuantity = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.tbProductCountry = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cbProductIsDocument = new System.Windows.Forms.CheckBox();
            this.tbProductDescription = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbProductNo = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.gbSender = new System.Windows.Forms.GroupBox();
            this.tbSenderEmail = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbSenderTelNo = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbSenderCountry = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbSenderZIPPostalCode = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbSenderStateProv = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbSenderCity = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbSenderAddress2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbSenderAddress1 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbSenderCompany = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbSenderContact = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.gbPackages = new System.Windows.Forms.GroupBox();
            this.tbPackagePONumber = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tbPackageRefInfo = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.cmbPackageInsCurr = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tbPackageIns = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cmbPackageDimsUM = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tbPackageHeight = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tbPackageWidth = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tbPackageLength = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cmbPackageWeightUM = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbPackageWeight = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbPackageNo = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.gbRecipient = new System.Windows.Forms.GroupBox();
            this.tbRecEmail = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbRecResidential = new System.Windows.Forms.CheckBox();
            this.tbRecTelNo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbRecCountry = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbRecZIPPostalCode = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbRecStateProv = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbRecCity = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbRecAddress2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbRecAddress1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbRecCompany = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbRecContact = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PackagingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.data1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Billing_TypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carriersBindingSource)).BeginInit();
            this.gbProducts.SuspendLayout();
            this.gbSender.SuspendLayout();
            this.gbPackages.SuspendLayout();
            this.gbRecipient.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label49);
            this.panel1.Controls.Add(this.cmbPackaging);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.cmbBillingType);
            this.panel1.Controls.Add(this.label46);
            this.panel1.Controls.Add(this.cmbService);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cmbCarrier);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tbShipmentPONumber);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tbShipmentRefInfo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tbOrderNo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnManualOrderCancel);
            this.panel1.Controls.Add(this.btnManualOrderOK);
            this.panel1.Controls.Add(this.gbProducts);
            this.panel1.Controls.Add(this.gbSender);
            this.panel1.Controls.Add(this.gbPackages);
            this.panel1.Controls.Add(this.gbRecipient);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(885, 648);
            this.panel1.TabIndex = 0;
            // 
            // cmbPackaging
            // 
            this.cmbPackaging.DataSource = this.PackagingsBindingSource;
            this.cmbPackaging.DisplayMember = "Name";
            this.cmbPackaging.FormattingEnabled = true;
            this.cmbPackaging.Location = new System.Drawing.Point(715, 53);
            this.cmbPackaging.Name = "cmbPackaging";
            this.cmbPackaging.Size = new System.Drawing.Size(121, 21);
            this.cmbPackaging.TabIndex = 19;
            this.cmbPackaging.ValueMember = "Packaging_Code";
            // 
            // PackagingsBindingSource
            // 
            this.PackagingsBindingSource.DataMember = "Packagings";
            this.PackagingsBindingSource.DataSource = this.data1;
            // 
            // data1
            // 
            this.data1.DataSetName = "Data";
            this.data1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(641, 59);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(58, 13);
            this.label47.TabIndex = 18;
            this.label47.Text = "Packaging";
            // 
            // cmbBillingType
            // 
            this.cmbBillingType.DataSource = this.Billing_TypesBindingSource;
            this.cmbBillingType.DisplayMember = "Name";
            this.cmbBillingType.FormattingEnabled = true;
            this.cmbBillingType.Location = new System.Drawing.Point(487, 54);
            this.cmbBillingType.Name = "cmbBillingType";
            this.cmbBillingType.Size = new System.Drawing.Size(121, 21);
            this.cmbBillingType.TabIndex = 17;
            this.cmbBillingType.ValueMember = "Id";
            // 
            // Billing_TypesBindingSource
            // 
            this.Billing_TypesBindingSource.DataMember = "Billing_Types";
            this.Billing_TypesBindingSource.DataSource = this.data1;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(428, 57);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(34, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "Billing";
            // 
            // cmbService
            // 
            this.cmbService.DataSource = this.servicesBindingSource;
            this.cmbService.DisplayMember = "Name";
            this.cmbService.FormattingEnabled = true;
            this.cmbService.Location = new System.Drawing.Point(281, 56);
            this.cmbService.Name = "cmbService";
            this.cmbService.Size = new System.Drawing.Size(121, 21);
            this.cmbService.TabIndex = 15;
            this.cmbService.ValueMember = "Unique_Code";
            // 
            // servicesBindingSource
            // 
            this.servicesBindingSource.DataMember = "Services";
            this.servicesBindingSource.DataSource = this.data1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(210, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Service";
            // 
            // cmbCarrier
            // 
            this.cmbCarrier.DataSource = this.carriersBindingSource;
            this.cmbCarrier.DisplayMember = "Name";
            this.cmbCarrier.FormattingEnabled = true;
            this.cmbCarrier.Location = new System.Drawing.Point(63, 56);
            this.cmbCarrier.Name = "cmbCarrier";
            this.cmbCarrier.Size = new System.Drawing.Size(121, 21);
            this.cmbCarrier.TabIndex = 13;
            this.cmbCarrier.ValueMember = "Carrier_Code";
            // 
            // carriersBindingSource
            // 
            this.carriersBindingSource.DataMember = "Carriers";
            this.carriersBindingSource.DataSource = this.data1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Carrier";
            // 
            // tbShipmentPONumber
            // 
            this.tbShipmentPONumber.Location = new System.Drawing.Point(474, 12);
            this.tbShipmentPONumber.Name = "tbShipmentPONumber";
            this.tbShipmentPONumber.Size = new System.Drawing.Size(100, 20);
            this.tbShipmentPONumber.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(389, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "PO Number";
            // 
            // tbShipmentRefInfo
            // 
            this.tbShipmentRefInfo.Location = new System.Drawing.Point(281, 12);
            this.tbShipmentRefInfo.Name = "tbShipmentRefInfo";
            this.tbShipmentRefInfo.Size = new System.Drawing.Size(100, 20);
            this.tbShipmentRefInfo.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(186, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Reference info";
            // 
            // tbOrderNo
            // 
            this.tbOrderNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbOrderNo.Location = new System.Drawing.Point(63, 13);
            this.tbOrderNo.Name = "tbOrderNo";
            this.tbOrderNo.Size = new System.Drawing.Size(100, 20);
            this.tbOrderNo.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Order #";
            // 
            // btnManualOrderCancel
            // 
            this.btnManualOrderCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManualOrderCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnManualOrderCancel.Location = new System.Drawing.Point(790, 611);
            this.btnManualOrderCancel.Name = "btnManualOrderCancel";
            this.btnManualOrderCancel.Size = new System.Drawing.Size(75, 23);
            this.btnManualOrderCancel.TabIndex = 5;
            this.btnManualOrderCancel.Text = "Cancel";
            this.btnManualOrderCancel.UseVisualStyleBackColor = true;
            this.btnManualOrderCancel.Click += new System.EventHandler(this.btnManualOrderCancel_Click);
            // 
            // btnManualOrderOK
            // 
            this.btnManualOrderOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManualOrderOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnManualOrderOK.Location = new System.Drawing.Point(677, 611);
            this.btnManualOrderOK.Name = "btnManualOrderOK";
            this.btnManualOrderOK.Size = new System.Drawing.Size(75, 23);
            this.btnManualOrderOK.TabIndex = 4;
            this.btnManualOrderOK.Text = "OK";
            this.btnManualOrderOK.UseVisualStyleBackColor = true;
            this.btnManualOrderOK.Click += new System.EventHandler(this.btnManualOrderOK_Click);
            // 
            // gbProducts
            // 
            this.gbProducts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbProducts.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gbProducts.Controls.Add(this.tbProductTotalValue);
            this.gbProducts.Controls.Add(this.label48);
            this.gbProducts.Controls.Add(this.tbProductHarmonizedCode);
            this.gbProducts.Controls.Add(this.label45);
            this.gbProducts.Controls.Add(this.tbProductTotalWeight);
            this.gbProducts.Controls.Add(this.label44);
            this.gbProducts.Controls.Add(this.tbProductUnitValue);
            this.gbProducts.Controls.Add(this.label43);
            this.gbProducts.Controls.Add(this.tbProductUnitWeight);
            this.gbProducts.Controls.Add(this.label42);
            this.gbProducts.Controls.Add(this.tbProductMU);
            this.gbProducts.Controls.Add(this.label41);
            this.gbProducts.Controls.Add(this.tbProductQuantity);
            this.gbProducts.Controls.Add(this.label40);
            this.gbProducts.Controls.Add(this.tbProductCountry);
            this.gbProducts.Controls.Add(this.label39);
            this.gbProducts.Controls.Add(this.cbProductIsDocument);
            this.gbProducts.Controls.Add(this.tbProductDescription);
            this.gbProducts.Controls.Add(this.label38);
            this.gbProducts.Controls.Add(this.tbProductNo);
            this.gbProducts.Controls.Add(this.label37);
            this.gbProducts.Location = new System.Drawing.Point(431, 313);
            this.gbProducts.Name = "gbProducts";
            this.gbProducts.Size = new System.Drawing.Size(434, 259);
            this.gbProducts.TabIndex = 3;
            this.gbProducts.TabStop = false;
            this.gbProducts.Text = "Products / Commodities";
            // 
            // tbProductTotalValue
            // 
            this.tbProductTotalValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductTotalValue.Location = new System.Drawing.Point(257, 169);
            this.tbProductTotalValue.Name = "tbProductTotalValue";
            this.tbProductTotalValue.Size = new System.Drawing.Size(113, 20);
            this.tbProductTotalValue.TabIndex = 20;
            this.tbProductTotalValue.Text = "1";
            // 
            // label48
            // 
            this.label48.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(192, 173);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(61, 13);
            this.label48.TabIndex = 19;
            this.label48.Text = "Total Value";
            // 
            // tbProductHarmonizedCode
            // 
            this.tbProductHarmonizedCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductHarmonizedCode.Location = new System.Drawing.Point(116, 204);
            this.tbProductHarmonizedCode.Name = "tbProductHarmonizedCode";
            this.tbProductHarmonizedCode.Size = new System.Drawing.Size(268, 20);
            this.tbProductHarmonizedCode.TabIndex = 18;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(13, 207);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(91, 13);
            this.label45.TabIndex = 17;
            this.label45.Text = "Harmonized Code";
            // 
            // tbProductTotalWeight
            // 
            this.tbProductTotalWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductTotalWeight.Location = new System.Drawing.Point(77, 170);
            this.tbProductTotalWeight.Name = "tbProductTotalWeight";
            this.tbProductTotalWeight.Size = new System.Drawing.Size(100, 20);
            this.tbProductTotalWeight.TabIndex = 16;
            this.tbProductTotalWeight.Text = "1";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(10, 173);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(68, 13);
            this.label44.TabIndex = 15;
            this.label44.Text = "Total Weight";
            // 
            // tbProductUnitValue
            // 
            this.tbProductUnitValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductUnitValue.Location = new System.Drawing.Point(258, 137);
            this.tbProductUnitValue.Name = "tbProductUnitValue";
            this.tbProductUnitValue.Size = new System.Drawing.Size(112, 20);
            this.tbProductUnitValue.TabIndex = 14;
            this.tbProductUnitValue.Text = "1";
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(192, 140);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(56, 13);
            this.label43.TabIndex = 13;
            this.label43.Text = "Unit Value";
            // 
            // tbProductUnitWeight
            // 
            this.tbProductUnitWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductUnitWeight.Location = new System.Drawing.Point(77, 137);
            this.tbProductUnitWeight.Name = "tbProductUnitWeight";
            this.tbProductUnitWeight.Size = new System.Drawing.Size(100, 20);
            this.tbProductUnitWeight.TabIndex = 12;
            this.tbProductUnitWeight.Text = "1";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(13, 140);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(63, 13);
            this.label42.TabIndex = 11;
            this.label42.Text = "Unit Weight";
            // 
            // tbProductMU
            // 
            this.tbProductMU.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductMU.Location = new System.Drawing.Point(258, 108);
            this.tbProductMU.Name = "tbProductMU";
            this.tbProductMU.Size = new System.Drawing.Size(112, 20);
            this.tbProductMU.TabIndex = 10;
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(192, 114);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(24, 13);
            this.label41.TabIndex = 9;
            this.label41.Text = "MU";
            // 
            // tbProductQuantity
            // 
            this.tbProductQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductQuantity.Location = new System.Drawing.Point(77, 111);
            this.tbProductQuantity.Name = "tbProductQuantity";
            this.tbProductQuantity.Size = new System.Drawing.Size(100, 20);
            this.tbProductQuantity.TabIndex = 8;
            this.tbProductQuantity.Text = "1";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(13, 111);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(46, 13);
            this.label40.TabIndex = 7;
            this.label40.Text = "Quantity";
            // 
            // tbProductCountry
            // 
            this.tbProductCountry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductCountry.Location = new System.Drawing.Point(258, 79);
            this.tbProductCountry.Name = "tbProductCountry";
            this.tbProductCountry.Size = new System.Drawing.Size(126, 20);
            this.tbProductCountry.TabIndex = 6;
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(146, 82);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(105, 13);
            this.label39.TabIndex = 5;
            this.label39.Text = "Manufacture country";
            // 
            // cbProductIsDocument
            // 
            this.cbProductIsDocument.AutoSize = true;
            this.cbProductIsDocument.Location = new System.Drawing.Point(14, 82);
            this.cbProductIsDocument.Name = "cbProductIsDocument";
            this.cbProductIsDocument.Size = new System.Drawing.Size(86, 17);
            this.cbProductIsDocument.TabIndex = 4;
            this.cbProductIsDocument.Text = "Is Document";
            this.cbProductIsDocument.UseVisualStyleBackColor = true;
            // 
            // tbProductDescription
            // 
            this.tbProductDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductDescription.Location = new System.Drawing.Point(77, 52);
            this.tbProductDescription.Name = "tbProductDescription";
            this.tbProductDescription.Size = new System.Drawing.Size(307, 20);
            this.tbProductDescription.TabIndex = 3;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(11, 56);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(60, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Description";
            // 
            // tbProductNo
            // 
            this.tbProductNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbProductNo.Location = new System.Drawing.Point(77, 26);
            this.tbProductNo.Name = "tbProductNo";
            this.tbProductNo.Size = new System.Drawing.Size(100, 20);
            this.tbProductNo.TabIndex = 1;
            this.tbProductNo.Text = "1";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(10, 29);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(54, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Product #";
            // 
            // gbSender
            // 
            this.gbSender.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbSender.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gbSender.Controls.Add(this.tbSenderEmail);
            this.gbSender.Controls.Add(this.label25);
            this.gbSender.Controls.Add(this.tbSenderTelNo);
            this.gbSender.Controls.Add(this.label24);
            this.gbSender.Controls.Add(this.tbSenderCountry);
            this.gbSender.Controls.Add(this.label23);
            this.gbSender.Controls.Add(this.tbSenderZIPPostalCode);
            this.gbSender.Controls.Add(this.label22);
            this.gbSender.Controls.Add(this.tbSenderStateProv);
            this.gbSender.Controls.Add(this.label21);
            this.gbSender.Controls.Add(this.tbSenderCity);
            this.gbSender.Controls.Add(this.label20);
            this.gbSender.Controls.Add(this.tbSenderAddress2);
            this.gbSender.Controls.Add(this.label19);
            this.gbSender.Controls.Add(this.tbSenderAddress1);
            this.gbSender.Controls.Add(this.label18);
            this.gbSender.Controls.Add(this.tbSenderCompany);
            this.gbSender.Controls.Add(this.label17);
            this.gbSender.Controls.Add(this.tbSenderContact);
            this.gbSender.Controls.Add(this.label16);
            this.gbSender.Location = new System.Drawing.Point(12, 365);
            this.gbSender.Name = "gbSender";
            this.gbSender.Size = new System.Drawing.Size(413, 269);
            this.gbSender.TabIndex = 2;
            this.gbSender.TabStop = false;
            this.gbSender.Text = "Sender";
            // 
            // tbSenderEmail
            // 
            this.tbSenderEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderEmail.Location = new System.Drawing.Point(70, 242);
            this.tbSenderEmail.Name = "tbSenderEmail";
            this.tbSenderEmail.Size = new System.Drawing.Size(307, 20);
            this.tbSenderEmail.TabIndex = 19;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(10, 242);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(32, 13);
            this.label25.TabIndex = 18;
            this.label25.Text = "Email";
            // 
            // tbSenderTelNo
            // 
            this.tbSenderTelNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderTelNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbSenderTelNo.Location = new System.Drawing.Point(79, 207);
            this.tbSenderTelNo.Name = "tbSenderTelNo";
            this.tbSenderTelNo.Size = new System.Drawing.Size(129, 20);
            this.tbSenderTelNo.TabIndex = 17;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(10, 210);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 13);
            this.label24.TabIndex = 16;
            this.label24.Text = "Tel. number";
            // 
            // tbSenderCountry
            // 
            this.tbSenderCountry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderCountry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbSenderCountry.Location = new System.Drawing.Point(268, 173);
            this.tbSenderCountry.Name = "tbSenderCountry";
            this.tbSenderCountry.Size = new System.Drawing.Size(100, 20);
            this.tbSenderCountry.TabIndex = 15;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(216, 176);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(43, 13);
            this.label23.TabIndex = 14;
            this.label23.Text = "Country";
            // 
            // tbSenderZIPPostalCode
            // 
            this.tbSenderZIPPostalCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderZIPPostalCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbSenderZIPPostalCode.Location = new System.Drawing.Point(79, 173);
            this.tbSenderZIPPostalCode.Name = "tbSenderZIPPostalCode";
            this.tbSenderZIPPostalCode.Size = new System.Drawing.Size(129, 20);
            this.tbSenderZIPPostalCode.TabIndex = 13;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(10, 176);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Postal Code";
            // 
            // tbSenderStateProv
            // 
            this.tbSenderStateProv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderStateProv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbSenderStateProv.Location = new System.Drawing.Point(308, 137);
            this.tbSenderStateProv.Name = "tbSenderStateProv";
            this.tbSenderStateProv.Size = new System.Drawing.Size(60, 20);
            this.tbSenderStateProv.TabIndex = 11;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(216, 140);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(85, 13);
            this.label21.TabIndex = 10;
            this.label21.Text = "State / Province";
            // 
            // tbSenderCity
            // 
            this.tbSenderCity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbSenderCity.Location = new System.Drawing.Point(70, 137);
            this.tbSenderCity.Name = "tbSenderCity";
            this.tbSenderCity.Size = new System.Drawing.Size(137, 20);
            this.tbSenderCity.TabIndex = 9;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 140);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(24, 13);
            this.label20.TabIndex = 8;
            this.label20.Text = "City";
            // 
            // tbSenderAddress2
            // 
            this.tbSenderAddress2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderAddress2.Location = new System.Drawing.Point(69, 105);
            this.tbSenderAddress2.Name = "tbSenderAddress2";
            this.tbSenderAddress2.Size = new System.Drawing.Size(299, 20);
            this.tbSenderAddress2.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 108);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Address 2";
            // 
            // tbSenderAddress1
            // 
            this.tbSenderAddress1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderAddress1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbSenderAddress1.Location = new System.Drawing.Point(70, 79);
            this.tbSenderAddress1.Name = "tbSenderAddress1";
            this.tbSenderAddress1.Size = new System.Drawing.Size(299, 20);
            this.tbSenderAddress1.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(10, 82);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Address 1";
            // 
            // tbSenderCompany
            // 
            this.tbSenderCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderCompany.Location = new System.Drawing.Point(69, 53);
            this.tbSenderCompany.Name = "tbSenderCompany";
            this.tbSenderCompany.Size = new System.Drawing.Size(299, 20);
            this.tbSenderCompany.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Company";
            // 
            // tbSenderContact
            // 
            this.tbSenderContact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSenderContact.Location = new System.Drawing.Point(70, 22);
            this.tbSenderContact.Name = "tbSenderContact";
            this.tbSenderContact.Size = new System.Drawing.Size(299, 20);
            this.tbSenderContact.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Contact";
            // 
            // gbPackages
            // 
            this.gbPackages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPackages.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gbPackages.Controls.Add(this.tbPackagePONumber);
            this.gbPackages.Controls.Add(this.label36);
            this.gbPackages.Controls.Add(this.tbPackageRefInfo);
            this.gbPackages.Controls.Add(this.label35);
            this.gbPackages.Controls.Add(this.cmbPackageInsCurr);
            this.gbPackages.Controls.Add(this.label34);
            this.gbPackages.Controls.Add(this.tbPackageIns);
            this.gbPackages.Controls.Add(this.label33);
            this.gbPackages.Controls.Add(this.cmbPackageDimsUM);
            this.gbPackages.Controls.Add(this.label32);
            this.gbPackages.Controls.Add(this.tbPackageHeight);
            this.gbPackages.Controls.Add(this.label31);
            this.gbPackages.Controls.Add(this.tbPackageWidth);
            this.gbPackages.Controls.Add(this.label30);
            this.gbPackages.Controls.Add(this.tbPackageLength);
            this.gbPackages.Controls.Add(this.label29);
            this.gbPackages.Controls.Add(this.cmbPackageWeightUM);
            this.gbPackages.Controls.Add(this.label28);
            this.gbPackages.Controls.Add(this.tbPackageWeight);
            this.gbPackages.Controls.Add(this.label27);
            this.gbPackages.Controls.Add(this.tbPackageNo);
            this.gbPackages.Controls.Add(this.label26);
            this.gbPackages.Location = new System.Drawing.Point(431, 97);
            this.gbPackages.Name = "gbPackages";
            this.gbPackages.Size = new System.Drawing.Size(434, 210);
            this.gbPackages.TabIndex = 1;
            this.gbPackages.TabStop = false;
            this.gbPackages.Text = "Package";
            // 
            // tbPackagePONumber
            // 
            this.tbPackagePONumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPackagePONumber.Location = new System.Drawing.Point(284, 158);
            this.tbPackagePONumber.Name = "tbPackagePONumber";
            this.tbPackagePONumber.Size = new System.Drawing.Size(109, 20);
            this.tbPackagePONumber.TabIndex = 21;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(216, 162);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(62, 13);
            this.label36.TabIndex = 20;
            this.label36.Text = "PO Number";
            // 
            // tbPackageRefInfo
            // 
            this.tbPackageRefInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPackageRefInfo.Location = new System.Drawing.Point(96, 161);
            this.tbPackageRefInfo.Name = "tbPackageRefInfo";
            this.tbPackageRefInfo.Size = new System.Drawing.Size(109, 20);
            this.tbPackageRefInfo.TabIndex = 19;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(11, 161);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(80, 13);
            this.label35.TabIndex = 18;
            this.label35.Text = "Reference info.";
            // 
            // cmbPackageInsCurr
            // 
            this.cmbPackageInsCurr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPackageInsCurr.FormattingEnabled = true;
            this.cmbPackageInsCurr.Items.AddRange(new object[] {
            "CAD",
            "USD"});
            this.cmbPackageInsCurr.Location = new System.Drawing.Point(310, 123);
            this.cmbPackageInsCurr.Name = "cmbPackageInsCurr";
            this.cmbPackageInsCurr.Size = new System.Drawing.Size(83, 21);
            this.cmbPackageInsCurr.TabIndex = 17;
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(192, 131);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(99, 13);
            this.label34.TabIndex = 16;
            this.label34.Text = "Insurance Currency";
            // 
            // tbPackageIns
            // 
            this.tbPackageIns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPackageIns.Location = new System.Drawing.Point(74, 124);
            this.tbPackageIns.Name = "tbPackageIns";
            this.tbPackageIns.Size = new System.Drawing.Size(109, 20);
            this.tbPackageIns.TabIndex = 15;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(10, 131);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(54, 13);
            this.label33.TabIndex = 14;
            this.label33.Text = "Insurance";
            // 
            // cmbPackageDimsUM
            // 
            this.cmbPackageDimsUM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPackageDimsUM.FormattingEnabled = true;
            this.cmbPackageDimsUM.Items.AddRange(new object[] {
            "I",
            "CM"});
            this.cmbPackageDimsUM.Location = new System.Drawing.Point(364, 90);
            this.cmbPackageDimsUM.Name = "cmbPackageDimsUM";
            this.cmbPackageDimsUM.Size = new System.Drawing.Size(57, 21);
            this.cmbPackageDimsUM.TabIndex = 13;
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(307, 94);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 13);
            this.label32.TabIndex = 12;
            this.label32.Text = "Dims UM";
            // 
            // tbPackageHeight
            // 
            this.tbPackageHeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPackageHeight.Location = new System.Drawing.Point(258, 91);
            this.tbPackageHeight.Name = "tbPackageHeight";
            this.tbPackageHeight.Size = new System.Drawing.Size(42, 20);
            this.tbPackageHeight.TabIndex = 11;
            this.tbPackageHeight.Text = "0";
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(213, 94);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(38, 13);
            this.label31.TabIndex = 10;
            this.label31.Text = "Height";
            // 
            // tbPackageWidth
            // 
            this.tbPackageWidth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPackageWidth.Location = new System.Drawing.Point(163, 91);
            this.tbPackageWidth.Name = "tbPackageWidth";
            this.tbPackageWidth.Size = new System.Drawing.Size(42, 20);
            this.tbPackageWidth.TabIndex = 9;
            this.tbPackageWidth.Text = "0";
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(122, 94);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(35, 13);
            this.label30.TabIndex = 8;
            this.label30.Text = "Width";
            // 
            // tbPackageLength
            // 
            this.tbPackageLength.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPackageLength.Location = new System.Drawing.Point(74, 91);
            this.tbPackageLength.Name = "tbPackageLength";
            this.tbPackageLength.Size = new System.Drawing.Size(42, 20);
            this.tbPackageLength.TabIndex = 7;
            this.tbPackageLength.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(11, 94);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(40, 13);
            this.label29.TabIndex = 6;
            this.label29.Text = "Length";
            // 
            // cmbPackageWeightUM
            // 
            this.cmbPackageWeightUM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPackageWeightUM.FormattingEnabled = true;
            this.cmbPackageWeightUM.Items.AddRange(new object[] {
            "LBS",
            "KGS"});
            this.cmbPackageWeightUM.Location = new System.Drawing.Point(293, 60);
            this.cmbPackageWeightUM.Name = "cmbPackageWeightUM";
            this.cmbPackageWeightUM.Size = new System.Drawing.Size(100, 21);
            this.cmbPackageWeightUM.TabIndex = 5;
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(222, 63);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(61, 13);
            this.label28.TabIndex = 4;
            this.label28.Text = "Weight UM";
            // 
            // tbPackageWeight
            // 
            this.tbPackageWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPackageWeight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbPackageWeight.Location = new System.Drawing.Point(74, 60);
            this.tbPackageWeight.Name = "tbPackageWeight";
            this.tbPackageWeight.Size = new System.Drawing.Size(109, 20);
            this.tbPackageWeight.TabIndex = 3;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(10, 63);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "Weight";
            // 
            // tbPackageNo
            // 
            this.tbPackageNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPackageNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbPackageNo.Location = new System.Drawing.Point(74, 19);
            this.tbPackageNo.Name = "tbPackageNo";
            this.tbPackageNo.Size = new System.Drawing.Size(109, 20);
            this.tbPackageNo.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 20);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Package #";
            // 
            // gbRecipient
            // 
            this.gbRecipient.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbRecipient.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gbRecipient.Controls.Add(this.tbRecEmail);
            this.gbRecipient.Controls.Add(this.label15);
            this.gbRecipient.Controls.Add(this.cbRecResidential);
            this.gbRecipient.Controls.Add(this.tbRecTelNo);
            this.gbRecipient.Controls.Add(this.label14);
            this.gbRecipient.Controls.Add(this.tbRecCountry);
            this.gbRecipient.Controls.Add(this.label13);
            this.gbRecipient.Controls.Add(this.tbRecZIPPostalCode);
            this.gbRecipient.Controls.Add(this.label12);
            this.gbRecipient.Controls.Add(this.tbRecStateProv);
            this.gbRecipient.Controls.Add(this.label11);
            this.gbRecipient.Controls.Add(this.tbRecCity);
            this.gbRecipient.Controls.Add(this.label10);
            this.gbRecipient.Controls.Add(this.tbRecAddress2);
            this.gbRecipient.Controls.Add(this.label9);
            this.gbRecipient.Controls.Add(this.tbRecAddress1);
            this.gbRecipient.Controls.Add(this.label8);
            this.gbRecipient.Controls.Add(this.tbRecCompany);
            this.gbRecipient.Controls.Add(this.label7);
            this.gbRecipient.Controls.Add(this.tbRecContact);
            this.gbRecipient.Controls.Add(this.label6);
            this.gbRecipient.Location = new System.Drawing.Point(13, 97);
            this.gbRecipient.Name = "gbRecipient";
            this.gbRecipient.Size = new System.Drawing.Size(411, 262);
            this.gbRecipient.TabIndex = 0;
            this.gbRecipient.TabStop = false;
            this.gbRecipient.Text = "Recipient";
            // 
            // tbRecEmail
            // 
            this.tbRecEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecEmail.Location = new System.Drawing.Point(70, 229);
            this.tbRecEmail.Name = "tbRecEmail";
            this.tbRecEmail.Size = new System.Drawing.Size(296, 20);
            this.tbRecEmail.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 229);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Email";
            // 
            // cbRecResidential
            // 
            this.cbRecResidential.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbRecResidential.AutoSize = true;
            this.cbRecResidential.Location = new System.Drawing.Point(288, 189);
            this.cbRecResidential.Name = "cbRecResidential";
            this.cbRecResidential.Size = new System.Drawing.Size(78, 17);
            this.cbRecResidential.TabIndex = 19;
            this.cbRecResidential.Text = "Residential";
            this.cbRecResidential.UseVisualStyleBackColor = true;
            // 
            // tbRecTelNo
            // 
            this.tbRecTelNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecTelNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbRecTelNo.Location = new System.Drawing.Point(78, 190);
            this.tbRecTelNo.Name = "tbRecTelNo";
            this.tbRecTelNo.Size = new System.Drawing.Size(129, 20);
            this.tbRecTelNo.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 190);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Tel. number";
            // 
            // tbRecCountry
            // 
            this.tbRecCountry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecCountry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbRecCountry.Location = new System.Drawing.Point(269, 162);
            this.tbRecCountry.Name = "tbRecCountry";
            this.tbRecCountry.Size = new System.Drawing.Size(100, 20);
            this.tbRecCountry.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(219, 162);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Country";
            // 
            // tbRecZIPPostalCode
            // 
            this.tbRecZIPPostalCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecZIPPostalCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbRecZIPPostalCode.Location = new System.Drawing.Point(78, 163);
            this.tbRecZIPPostalCode.Name = "tbRecZIPPostalCode";
            this.tbRecZIPPostalCode.Size = new System.Drawing.Size(129, 20);
            this.tbRecZIPPostalCode.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 163);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Postal Code";
            // 
            // tbRecStateProv
            // 
            this.tbRecStateProv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecStateProv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbRecStateProv.Location = new System.Drawing.Point(308, 131);
            this.tbRecStateProv.Name = "tbRecStateProv";
            this.tbRecStateProv.Size = new System.Drawing.Size(60, 20);
            this.tbRecStateProv.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(216, 131);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "State / Province";
            // 
            // tbRecCity
            // 
            this.tbRecCity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbRecCity.Location = new System.Drawing.Point(70, 131);
            this.tbRecCity.Name = "tbRecCity";
            this.tbRecCity.Size = new System.Drawing.Size(137, 20);
            this.tbRecCity.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 131);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "City";
            // 
            // tbRecAddress2
            // 
            this.tbRecAddress2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecAddress2.Location = new System.Drawing.Point(70, 101);
            this.tbRecAddress2.Name = "tbRecAddress2";
            this.tbRecAddress2.Size = new System.Drawing.Size(298, 20);
            this.tbRecAddress2.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Address 2";
            // 
            // tbRecAddress1
            // 
            this.tbRecAddress1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecAddress1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbRecAddress1.Location = new System.Drawing.Point(70, 74);
            this.tbRecAddress1.Name = "tbRecAddress1";
            this.tbRecAddress1.Size = new System.Drawing.Size(298, 20);
            this.tbRecAddress1.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Address 1";
            // 
            // tbRecCompany
            // 
            this.tbRecCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecCompany.Location = new System.Drawing.Point(70, 47);
            this.tbRecCompany.Name = "tbRecCompany";
            this.tbRecCompany.Size = new System.Drawing.Size(298, 20);
            this.tbRecCompany.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Company";
            // 
            // tbRecContact
            // 
            this.tbRecContact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRecContact.Location = new System.Drawing.Point(70, 20);
            this.tbRecContact.Name = "tbRecContact";
            this.tbRecContact.Size = new System.Drawing.Size(298, 20);
            this.tbRecContact.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Contact";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(741, 12);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(84, 13);
            this.label49.TabIndex = 20;
            this.label49.Text = "Mandatory fields";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(713, 9);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(22, 20);
            this.textBox1.TabIndex = 21;
            this.textBox1.TabStop = false;
            // 
            // frmAddManualOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 648);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAddManualOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Order";
            this.Load += new System.EventHandler(this.frmAddManualOrder_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PackagingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.data1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Billing_TypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carriersBindingSource)).EndInit();
            this.gbProducts.ResumeLayout(false);
            this.gbProducts.PerformLayout();
            this.gbSender.ResumeLayout(false);
            this.gbSender.PerformLayout();
            this.gbPackages.ResumeLayout(false);
            this.gbPackages.PerformLayout();
            this.gbRecipient.ResumeLayout(false);
            this.gbRecipient.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox gbProducts;
        private System.Windows.Forms.GroupBox gbSender;
        private System.Windows.Forms.GroupBox gbPackages;
        private System.Windows.Forms.GroupBox gbRecipient;
        private System.Windows.Forms.Button btnManualOrderCancel;
        private System.Windows.Forms.Button btnManualOrderOK;
        private System.Windows.Forms.TextBox tbShipmentRefInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbOrderNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbCarrier;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbShipmentPONumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbService;
        private System.Windows.Forms.TextBox tbRecAddress1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbRecCompany;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbRecContact;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbRecAddress2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbRecZIPPostalCode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbRecStateProv;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbRecCity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbRecCountry;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox cbRecResidential;
        private System.Windows.Forms.TextBox tbRecTelNo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbRecEmail;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbSenderCompany;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbSenderContact;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbSenderAddress2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbSenderAddress1;
        private System.Windows.Forms.TextBox tbSenderCity;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbSenderStateProv;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbSenderZIPPostalCode;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbSenderCountry;
        private System.Windows.Forms.TextBox tbSenderTelNo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbSenderEmail;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbPackageWeightUM;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbPackageWeight;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbPackageNo;
        private System.Windows.Forms.TextBox tbPackageLength;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbPackageWidth;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tbPackageHeight;
        private System.Windows.Forms.ComboBox cmbPackageDimsUM;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cmbPackageInsCurr;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tbPackageIns;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbPackageRefInfo;
        private System.Windows.Forms.TextBox tbPackagePONumber;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbProductNo;
        private System.Windows.Forms.TextBox tbProductDescription;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbProductCountry;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.CheckBox cbProductIsDocument;
        private System.Windows.Forms.TextBox tbProductQuantity;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tbProductMU;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tbProductUnitValue;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbProductUnitWeight;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tbProductTotalWeight;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbProductHarmonizedCode;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.BindingSource carriersBindingSource;
        private Data data1;
        private System.Windows.Forms.BindingSource servicesBindingSource;
        private System.Windows.Forms.ComboBox cmbBillingType;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox cmbPackaging;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.BindingSource Billing_TypesBindingSource;
        private System.Windows.Forms.BindingSource PackagingsBindingSource;
        private System.Windows.Forms.TextBox tbProductTotalValue;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBox1;

    }
}