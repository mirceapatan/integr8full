﻿namespace InteGr8
{
    partial class frmSQLDesigner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSQLDesigner));
            this.TextSQLBuilder = new ActiveDatabaseSoftware.ActiveQueryBuilder.PlainTextSQLBuilder();
            this.QueryBuilderControl = new ActiveDatabaseSoftware.ActiveQueryBuilder.QueryBuilder();
            this.SyntaxProvider = new ActiveDatabaseSoftware.ActiveQueryBuilder.UniversalSyntaxProvider();
            this.txtSQL = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.OdbcProvider = new ActiveDatabaseSoftware.ActiveQueryBuilder.ODBCMetadataProvider();
            this.SuspendLayout();
            // 
            // TextSQLBuilder
            // 
            this.TextSQLBuilder.QueryBuilder = this.QueryBuilderControl;
            this.TextSQLBuilder.UseAltNames = false;
            this.TextSQLBuilder.SQLUpdated += new System.EventHandler(this.TextSQLBuilder_SQLUpdated);
            // 
            // QueryBuilderControl
            // 
            this.QueryBuilderControl.AddObjectFormOptions.MinimumSize = new System.Drawing.Size(430, 430);
            this.QueryBuilderControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.QueryBuilderControl.CriteriaListOptions.CriteriaListFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QueryBuilderControl.DiagramObjectColor = System.Drawing.SystemColors.Window;
            this.QueryBuilderControl.DiagramObjectFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QueryBuilderControl.FieldListOptions.DescriptionColumnOptions.Color = System.Drawing.Color.LightBlue;
            this.QueryBuilderControl.FieldListOptions.MarkColumnOptions.PKIcon = ((System.Drawing.Image)(resources.GetObject("resource.PKIcon")));
            this.QueryBuilderControl.FieldListOptions.NameColumnOptions.Color = System.Drawing.SystemColors.WindowText;
            this.QueryBuilderControl.FieldListOptions.NameColumnOptions.PKColor = System.Drawing.SystemColors.WindowText;
            this.QueryBuilderControl.FieldListOptions.TypeColumnOptions.Color = System.Drawing.SystemColors.GrayText;
            this.QueryBuilderControl.Location = new System.Drawing.Point(12, 12);
            this.QueryBuilderControl.MetadataProvider = this.OdbcProvider;
            this.QueryBuilderControl.MetadataTreeOptions.ImageList = null;
            this.QueryBuilderControl.MetadataTreeOptions.ProceduresNodeText = null;
            this.QueryBuilderControl.MetadataTreeOptions.SynonymsNodeText = null;
            this.QueryBuilderControl.MetadataTreeOptions.TablesNodeText = null;
            this.QueryBuilderControl.MetadataTreeOptions.ViewsNodeText = null;
            this.QueryBuilderControl.Name = "QueryBuilderControl";
            this.QueryBuilderControl.QueryStructureTreeOptions.ImageList = null;
            this.QueryBuilderControl.Size = new System.Drawing.Size(965, 354);
            this.QueryBuilderControl.SleepModeText = null;
            this.QueryBuilderControl.SnapSize = new System.Drawing.Size(5, 5);
            this.QueryBuilderControl.SyntaxProvider = this.SyntaxProvider;
            this.QueryBuilderControl.TabIndex = 0;
            this.QueryBuilderControl.TreeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QueryBuilderControl.UseAltNames = false;
            // 
            // SyntaxProvider
            // 
            this.SyntaxProvider.BuiltinFunctionNames = ((System.Collections.Generic.List<string>)(resources.GetObject("SyntaxProvider.BuiltinFunctionNames")));
            this.SyntaxProvider.PreferredServer = ActiveDatabaseSoftware.ActiveQueryBuilder.UniversalSyntaxServerType.Unknown;
            // 
            // txtSQL
            // 
            this.txtSQL.AcceptsReturn = true;
            this.txtSQL.AcceptsTab = true;
            this.txtSQL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSQL.Location = new System.Drawing.Point(12, 372);
            this.txtSQL.Multiline = true;
            this.txtSQL.Name = "txtSQL";
            this.txtSQL.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSQL.Size = new System.Drawing.Size(965, 120);
            this.txtSQL.TabIndex = 1;
            this.txtSQL.Validating += new System.ComponentModel.CancelEventHandler(this.txtSQL_Validating);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(902, 498);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.CausesValidation = false;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(821, 498);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // OdbcProvider
            // 
            this.OdbcProvider.Connection = null;
            // 
            // frmSQLDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 533);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtSQL);
            this.Controls.Add(this.QueryBuilderControl);
            this.Name = "frmSQLDesigner";
            this.Text = "frmSQLDesigner";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ActiveDatabaseSoftware.ActiveQueryBuilder.PlainTextSQLBuilder TextSQLBuilder;
        private ActiveDatabaseSoftware.ActiveQueryBuilder.QueryBuilder QueryBuilderControl;
        private ActiveDatabaseSoftware.ActiveQueryBuilder.UniversalSyntaxProvider SyntaxProvider;
        private System.Windows.Forms.TextBox txtSQL;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private ActiveDatabaseSoftware.ActiveQueryBuilder.ODBCMetadataProvider OdbcProvider;
    }
}