﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard06 : Form
    {
        private bool custom_binding = false;
        private FieldLink link = null;

        public frmWizard06()
        {
            InitializeComponent();
        }

        private void frmWizard06_Load(object sender, EventArgs e)
        {
            // populate the source table combobox with the ODBC catalog tables
            cbPackagesTable.Items.Add("(none)");
            using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
            {
                Conn.Open();
                foreach (DataRow row in Conn.GetSchema("Tables").Rows)
                    cbPackagesTable.Items.Add(row["TABLE_NAME"]);
            }

            // check that the link is OrdersOneToMany type and each field have the same link
            foreach (FieldBinding binding in InteGr8_Main.template.Bindings)
                if (binding.Field.FieldCategory == Data.FieldsTableDataTable.FieldCategory.OrderPackages && binding.Link != null)
                    if (link == null) link = binding.Link;
                    else if (!binding.Link.Equals(link)) custom_binding = true;
            if (link != null && link.Type != FieldLink.LinkType.Orders && link.Type != FieldLink.LinkType.OrdersOneToMany) custom_binding = true;
            if (custom_binding)
            {
                foreach (Control control in this.Controls)
                    if (control is ComboBox) control.Enabled = false;
                MessageBox.Show(this, "Order Packages configuration was customized and cannot be edited in this page. You will be able to edit in the summary page. Please click Next to skip this page.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (link == null) cbPackagesTable.SelectedIndex = 0;
            else
            {
                int index = cbPackagesTable.FindStringExact(link.FieldTable);
                if (index == -1)
                {
                    cbPackagesTable.SelectedIndex = 0;
                    MessageBox.Show(this, "Order Packages table '" + link.FieldTable + "' not found in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else cbPackagesTable.SelectedIndex = index;
                if (link.Type != FieldLink.LinkType.Orders && link.OrdersTableFieldFK != null)
                {
                    cbPackagesForeignKey.SelectedIndex = cbPackagesForeignKey.FindStringExact(link.OrdersTableFieldFK);
                    if (cbPackagesForeignKey.SelectedIndex == -1)
                    {
                        MessageBox.Show(this, "Order Packages table Order # foreign key column '" + link.OrdersTableFieldFK + "' not found in the Order Packages table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
            if (InteGr8_Main.template.Find("Package Weight") != null) cbWeight.Text = InteGr8_Main.template.Find("Package Weight").Value;
            if (InteGr8_Main.template.Find("Package Weight Type") != null) cbWeightType.Text = InteGr8_Main.template.Find("Package Weight Type").Value;
            if (InteGr8_Main.template.Find("Package Insurance Amount") != null) cbInsurance.Text = InteGr8_Main.template.Find("Package Insurance Amount").Value;
            if (InteGr8_Main.template.Find("Package Insurance Currency") != null) cbInsuranceCurrency.Text = InteGr8_Main.template.Find("Package Insurance Currency").Value;
            if (InteGr8_Main.template.Find("Package Length") != null) cbLength.Text = InteGr8_Main.template.Find("Package Length").Value;
            if (InteGr8_Main.template.Find("Package Width") != null) cbWidth.Text = InteGr8_Main.template.Find("Package Width").Value;
            if (InteGr8_Main.template.Find("Package Height") != null) cbHeight.Text = InteGr8_Main.template.Find("Package Height").Value;
            if (InteGr8_Main.template.Find("Package Dimensions Type") != null) cbDimensionsType.Text = InteGr8_Main.template.Find("Package Dimensions Type").Value;
            if (InteGr8_Main.template.Find("Package Reference Info 1") != null) cbReference.Text = InteGr8_Main.template.Find("Package Reference Info 1").Value;
        }

        private void cbPackagesTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // clear all column comboboxes
            cbPackagesForeignKey.Items.Clear();
            cbPackagesPrimaryKey.Items.Clear();

            cbWeight.Items.Clear();
            cbWeightType.Items.Clear();
            cbInsurance.Items.Clear();
            cbInsuranceCurrency.Items.Clear();
            cbLength.Items.Clear();
            cbWidth.Items.Clear();
            cbHeight.Items.Clear();
            cbDimensionsType.Items.Clear();
            cbReference.Items.Clear();

            cbPackagesForeignKey.SelectedIndex = -1;
            cbPackagesPrimaryKey.SelectedIndex = -1;

            // if link is needed or not (Orders left join Packages)
            bool link = cbPackagesTable.SelectedIndex > 0 && !cbPackagesTable.Text.Equals(InteGr8_Main.template.Orders);
            cbPackagesForeignKey.Enabled = link;
            cbPackagesPrimaryKey.Enabled = link;

            lblOrderNumberIsPK.Enabled = false;
            rbOrderNumberIsPKYes.Enabled = false;
            rbOrderNumberIsPKNo.Enabled = false;

            // read the Packages table columns and populate all the packages comboboxes
            if (cbPackagesTable.SelectedIndex > 0)
            {
                string[] columns = null;
                using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
                {
                    Conn.Open();
                    using (OdbcDataReader dr = new OdbcCommand("Select Top 0 * From " + cbPackagesTable.Text, Conn).ExecuteReader(CommandBehavior.SingleRow))
                    {
                        columns = new string[dr.FieldCount];
                        for (int k = 0; k < dr.FieldCount; k++)
                            columns[k] = "[" + dr.GetName(k) + "]";
                    }
                }

                if (columns != null)
                {
                    cbPackagesForeignKey.Items.AddRange(columns);
                    cbPackagesPrimaryKey.Items.AddRange(columns);

                    cbWeight.Items.AddRange(columns);
                    cbWeight.Items.AddRange(new string[] { "-- 1.00 --" });
                    cbWeightType.Items.AddRange(columns);
                    cbWeightType.Items.AddRange(new string[] { "-- LBS --", "-- KGS --" });
                    cbInsurance.Items.AddRange(columns);
                    cbInsurance.Items.AddRange(new string[] { "-- 1.00 --" });
                    cbInsuranceCurrency.Items.AddRange(columns);
                    cbInsuranceCurrency.Items.AddRange(new string[] { "-- USD --", "-- CAD --" });
                    cbLength.Items.AddRange(columns);
                    cbLength.Items.AddRange(new string[] { "-- 1.0 --" });
                    cbWidth.Items.AddRange(columns);
                    cbWidth.Items.AddRange(new string[] { "-- 1.0 --" });
                    cbHeight.Items.AddRange(columns);
                    cbHeight.Items.AddRange(new string[] { "-- 1.0 --" });
                    cbDimensionsType.Items.AddRange(columns);
                    cbDimensionsType.Items.AddRange(new string[] { "-- IN --", "-- CM --" });
                    cbReference.Items.AddRange(columns);
                }
            }
        }

        private void cbPackagesPrimaryKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool enabled = cbPackagesPrimaryKey.SelectedIndex != -1;
            lblOrderNumberIsPK.Enabled = enabled;
            rbOrderNumberIsPKYes.Enabled = enabled;
            rbOrderNumberIsPKNo.Enabled = enabled;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (cbPackagesForeignKey.Enabled && cbPackagesForeignKey.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please choose the Order # foreign key column in the Shipment Packages table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // validate
            // TODO: check that the Package # is unique
            // TODO: check the Order # foreign key

            if (!custom_binding)
            {
                if (cbPackagesTable.SelectedIndex == 0) link = null;
                else if (cbPackagesTable.Text.Equals(InteGr8_Main.template.Orders))
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.Orders;
                    link.FieldTable = cbPackagesTable.Text;
                }
                else
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.OrdersOneToMany;
                    link.FieldTable = cbPackagesTable.Text;
                    link.OrdersTableFieldFK = cbPackagesForeignKey.Text;
                }

                InteGr8_Main.template.UpdateBinding("Package Weight", cbWeight.FindStringExact(cbWeight.Text) == -1, cbWeight.Text.Trim().Equals("") ? null : cbWeight.Text, link);
                InteGr8_Main.template.UpdateBinding("Package Weight Type", cbWeightType.FindStringExact(cbWeightType.Text) == -1, cbWeightType.Text.Trim().Equals("") ? null : cbWeightType.Text, link);
                InteGr8_Main.template.UpdateBinding("Package Insurance Amount", cbInsurance.FindStringExact(cbInsurance.Text) == -1, cbInsurance.Text.Trim().Equals("") ? null : cbInsurance.Text, link);
                InteGr8_Main.template.UpdateBinding("Package Insurance Currency", cbInsuranceCurrency.FindStringExact(cbInsuranceCurrency.Text) == -1, cbInsuranceCurrency.Text.Trim().Equals("") ? null : cbInsuranceCurrency.Text, link);
                InteGr8_Main.template.UpdateBinding("Package Length", cbLength.FindStringExact(cbLength.Text) == -1, cbLength.Text.Trim().Equals("") ? null : cbLength.Text, link);
                InteGr8_Main.template.UpdateBinding("Package Width", cbWidth.FindStringExact(cbWidth.Text) == -1, cbWidth.Text.Trim().Equals("") ? null : cbWidth.Text, link);
                InteGr8_Main.template.UpdateBinding("Package Height", cbHeight.FindStringExact(cbHeight.Text) == -1, cbHeight.Text.Trim().Equals("") ? null : cbHeight.Text, link);
                InteGr8_Main.template.UpdateBinding("Package Dimensions Type", cbDimensionsType.FindStringExact(cbDimensionsType.Text) == -1, cbDimensionsType.Text.Trim().Equals("") ? null : cbDimensionsType.Text, link);
                InteGr8_Main.template.UpdateBinding("Package Reference Info 1", cbReference.FindStringExact(cbReference.Text) == -1, cbReference.Text.Trim().Equals("") ? null : cbReference.Text, link);
            }
            DialogResult = DialogResult.OK;
        }

        private void frmWizard06_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK) return;
            if (DialogResult == DialogResult.No)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            DialogResult = DialogResult.Abort;
        }

        private void frmWizard06_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_06");
            e.Cancel = true;
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (cbPackagesTable.SelectedIndex <= 0 || cbPackagesTable.Text.Trim().Equals("")) return;
            new frmWizard_DataPreview("Packages", InteGr8_Main.template.Connection, "Select Top 100 * From " + cbPackagesTable.Text).ShowDialog(this);
        }
    }
}
