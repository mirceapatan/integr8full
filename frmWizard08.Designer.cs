﻿namespace InteGr8
{
    partial class frmWizard08
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard08));
            this.lblHeaderLine = new System.Windows.Forms.Label();
            this.llAdvanced = new System.Windows.Forms.LinkLabel();
            this.cbErrorMessage = new System.Windows.Forms.ComboBox();
            this.lblErrorMessage = new System.Windows.Forms.Label();
            this.lblShipmentsMappingLine = new System.Windows.Forms.Label();
            this.cbRateZone = new System.Windows.Forms.ComboBox();
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.btnPreview = new System.Windows.Forms.Button();
            this.lblShipmentsMapping = new System.Windows.Forms.Label();
            this.btnPrev = new System.Windows.Forms.Button();
            this.cbShipmentsStatusColumn = new System.Windows.Forms.ComboBox();
            this.lblShipmentsStatusColumn = new System.Windows.Forms.Label();
            this.cbShipmentsOrderNumberColumn = new System.Windows.Forms.ComboBox();
            this.lblShipmentsOrderNumberColumn = new System.Windows.Forms.Label();
            this.lblShipmentsTable = new System.Windows.Forms.Label();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.lblHeaderIcon = new System.Windows.Forms.Label();
            this.lblHeaderInfo = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.cbShipmentsTable = new System.Windows.Forms.ComboBox();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.cbLabelURL = new System.Windows.Forms.ComboBox();
            this.lblErrorCodeMessage = new System.Windows.Forms.Label();
            this.cbErrorCode = new System.Windows.Forms.ComboBox();
            this.lblErrorCode = new System.Windows.Forms.Label();
            this.lblLabelURLMessage = new System.Windows.Forms.Label();
            this.lblRateZoneMessage = new System.Windows.Forms.Label();
            this.lblDeliveryMessage = new System.Windows.Forms.Label();
            this.lblBilledWeightMessage = new System.Windows.Forms.Label();
            this.lblCommercialInvoiceURLMessage = new System.Windows.Forms.Label();
            this.lblPriceMessage = new System.Windows.Forms.Label();
            this.lblRateZone = new System.Windows.Forms.Label();
            this.cbBilledWeight = new System.Windows.Forms.ComboBox();
            this.lblBilledWeight = new System.Windows.Forms.Label();
            this.cbDelivery = new System.Windows.Forms.ComboBox();
            this.lblDelivery = new System.Windows.Forms.Label();
            this.cbCurrency = new System.Windows.Forms.ComboBox();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.cbPrice = new System.Windows.Forms.ComboBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.cbCommercialInvoiceURL = new System.Windows.Forms.ComboBox();
            this.lblCommercialInvoiceURL = new System.Windows.Forms.Label();
            this.lblLabelURL = new System.Windows.Forms.Label();
            this.lblCurrencyMessage = new System.Windows.Forms.Label();
            this.lblNote = new System.Windows.Forms.Label();
            this.cbShipmentNumber = new System.Windows.Forms.ComboBox();
            this.lblShipmentNumber = new System.Windows.Forms.Label();
            this.lblShipmentsOrderNumberColumnMessage = new System.Windows.Forms.Label();
            this.lblShipmentsStatusColumnMessage = new System.Windows.Forms.Label();
            this.lblShipmentNumberMessage = new System.Windows.Forms.Label();
            this.lblStatusFilter = new System.Windows.Forms.Label();
            this.rbStatusFilterYes = new System.Windows.Forms.RadioButton();
            this.rbStatusFilterNo = new System.Windows.Forms.RadioButton();
            this.pnlHeader.SuspendLayout();
            this.pnlFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHeaderLine
            // 
            this.lblHeaderLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderLine.BackColor = System.Drawing.SystemColors.Control;
            this.lblHeaderLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeaderLine.Location = new System.Drawing.Point(0, 102);
            this.lblHeaderLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderLine.Name = "lblHeaderLine";
            this.lblHeaderLine.Size = new System.Drawing.Size(621, 2);
            this.lblHeaderLine.TabIndex = 116;
            // 
            // llAdvanced
            // 
            this.llAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdvanced.Image = global::InteGr8.Properties.Resources.wrench;
            this.llAdvanced.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llAdvanced.LinkArea = new System.Windows.Forms.LinkArea(6, 13);
            this.llAdvanced.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdvanced.Location = new System.Drawing.Point(517, 0);
            this.llAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.llAdvanced.Name = "llAdvanced";
            this.llAdvanced.Size = new System.Drawing.Size(104, 24);
            this.llAdvanced.TabIndex = 119;
            this.llAdvanced.TabStop = true;
            this.llAdvanced.Text = "      advanced mode";
            this.llAdvanced.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llAdvanced.UseCompatibleTextRendering = true;
            // 
            // cbErrorMessage
            // 
            this.cbErrorMessage.FormattingEnabled = true;
            this.cbErrorMessage.Location = new System.Drawing.Point(164, 496);
            this.cbErrorMessage.Name = "cbErrorMessage";
            this.cbErrorMessage.Size = new System.Drawing.Size(174, 21);
            this.cbErrorMessage.TabIndex = 14;
            // 
            // lblErrorMessage
            // 
            this.lblErrorMessage.AutoSize = true;
            this.lblErrorMessage.Location = new System.Drawing.Point(9, 499);
            this.lblErrorMessage.Name = "lblErrorMessage";
            this.lblErrorMessage.Size = new System.Drawing.Size(77, 13);
            this.lblErrorMessage.TabIndex = 271;
            this.lblErrorMessage.Text = "Error message:";
            // 
            // lblShipmentsMappingLine
            // 
            this.lblShipmentsMappingLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblShipmentsMappingLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblShipmentsMappingLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShipmentsMappingLine.Location = new System.Drawing.Point(12, 271);
            this.lblShipmentsMappingLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblShipmentsMappingLine.Name = "lblShipmentsMappingLine";
            this.lblShipmentsMappingLine.Size = new System.Drawing.Size(597, 2);
            this.lblShipmentsMappingLine.TabIndex = 253;
            // 
            // cbRateZone
            // 
            this.cbRateZone.FormattingEnabled = true;
            this.cbRateZone.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing"});
            this.cbRateZone.Location = new System.Drawing.Point(164, 443);
            this.cbRateZone.Name = "cbRateZone";
            this.cbRateZone.Size = new System.Drawing.Size(174, 21);
            this.cbRateZone.TabIndex = 12;
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFooterLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(12, 5);
            this.lblFooterLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(597, 2);
            this.lblFooterLine.TabIndex = 122;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(11, 18);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(108, 23);
            this.btnPreview.TabIndex = 15;
            this.btnPreview.Text = "Preview Shipments";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // lblShipmentsMapping
            // 
            this.lblShipmentsMapping.AutoSize = true;
            this.lblShipmentsMapping.Location = new System.Drawing.Point(9, 253);
            this.lblShipmentsMapping.Name = "lblShipmentsMapping";
            this.lblShipmentsMapping.Size = new System.Drawing.Size(524, 13);
            this.lblShipmentsMapping.TabIndex = 251;
            this.lblShipmentsMapping.Text = "Please map the following 2Ship shipment variables to your Shipments table columns" +
                ", or choose default values:";
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnPrev.Location = new System.Drawing.Point(453, 18);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 16;
            this.btnPrev.Text = "< Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // cbShipmentsStatusColumn
            // 
            this.cbShipmentsStatusColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbShipmentsStatusColumn.FormattingEnabled = true;
            this.cbShipmentsStatusColumn.Location = new System.Drawing.Point(220, 196);
            this.cbShipmentsStatusColumn.Name = "cbShipmentsStatusColumn";
            this.cbShipmentsStatusColumn.Size = new System.Drawing.Size(174, 21);
            this.cbShipmentsStatusColumn.TabIndex = 3;
            // 
            // lblShipmentsStatusColumn
            // 
            this.lblShipmentsStatusColumn.AutoSize = true;
            this.lblShipmentsStatusColumn.Location = new System.Drawing.Point(9, 199);
            this.lblShipmentsStatusColumn.Name = "lblShipmentsStatusColumn";
            this.lblShipmentsStatusColumn.Size = new System.Drawing.Size(130, 13);
            this.lblShipmentsStatusColumn.TabIndex = 247;
            this.lblShipmentsStatusColumn.Text = "Shipments Status Column:";
            // 
            // cbShipmentsOrderNumberColumn
            // 
            this.cbShipmentsOrderNumberColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbShipmentsOrderNumberColumn.FormattingEnabled = true;
            this.cbShipmentsOrderNumberColumn.Location = new System.Drawing.Point(220, 142);
            this.cbShipmentsOrderNumberColumn.Name = "cbShipmentsOrderNumberColumn";
            this.cbShipmentsOrderNumberColumn.Size = new System.Drawing.Size(174, 21);
            this.cbShipmentsOrderNumberColumn.TabIndex = 1;
            // 
            // lblShipmentsOrderNumberColumn
            // 
            this.lblShipmentsOrderNumberColumn.AutoSize = true;
            this.lblShipmentsOrderNumberColumn.Location = new System.Drawing.Point(9, 145);
            this.lblShipmentsOrderNumberColumn.Name = "lblShipmentsOrderNumberColumn";
            this.lblShipmentsOrderNumberColumn.Size = new System.Drawing.Size(195, 13);
            this.lblShipmentsOrderNumberColumn.TabIndex = 246;
            this.lblShipmentsOrderNumberColumn.Text = "Shipments Order # Foreign Key Column:";
            // 
            // lblShipmentsTable
            // 
            this.lblShipmentsTable.AutoSize = true;
            this.lblShipmentsTable.Location = new System.Drawing.Point(9, 118);
            this.lblShipmentsTable.Name = "lblShipmentsTable";
            this.lblShipmentsTable.Size = new System.Drawing.Size(89, 13);
            this.lblShipmentsTable.TabIndex = 245;
            this.lblShipmentsTable.Text = "Shipments Table:";
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.lblHeaderLine);
            this.pnlHeader.Controls.Add(this.llAdvanced);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.lblHeaderIcon);
            this.pnlHeader.Controls.Add(this.lblHeaderInfo);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(621, 104);
            this.pnlHeader.TabIndex = 243;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(517, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Shipments Level Export Information";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeaderIcon
            // 
            this.lblHeaderIcon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeaderIcon.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaderIcon.Image = global::InteGr8.Properties.Resources.data_next;
            this.lblHeaderIcon.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeaderIcon.Location = new System.Drawing.Point(0, 24);
            this.lblHeaderIcon.Name = "lblHeaderIcon";
            this.lblHeaderIcon.Padding = new System.Windows.Forms.Padding(5);
            this.lblHeaderIcon.Size = new System.Drawing.Size(64, 78);
            this.lblHeaderIcon.TabIndex = 115;
            // 
            // lblHeaderInfo
            // 
            this.lblHeaderInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderInfo.AutoEllipsis = true;
            this.lblHeaderInfo.Location = new System.Drawing.Point(68, 24);
            this.lblHeaderInfo.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderInfo.Name = "lblHeaderInfo";
            this.lblHeaderInfo.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.lblHeaderInfo.Size = new System.Drawing.Size(553, 78);
            this.lblHeaderInfo.TabIndex = 114;
            this.lblHeaderInfo.Text = resources.GetString("lblHeaderInfo.Text");
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(534, 18);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 17;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // cbShipmentsTable
            // 
            this.cbShipmentsTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbShipmentsTable.FormattingEnabled = true;
            this.cbShipmentsTable.Location = new System.Drawing.Point(220, 115);
            this.cbShipmentsTable.Name = "cbShipmentsTable";
            this.cbShipmentsTable.Size = new System.Drawing.Size(174, 21);
            this.cbShipmentsTable.TabIndex = 0;
            this.cbShipmentsTable.SelectedIndexChanged += new System.EventHandler(this.cbShipmentsTable_SelectedIndexChanged);
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.lblFooterLine);
            this.pnlFooter.Controls.Add(this.btnPreview);
            this.pnlFooter.Controls.Add(this.btnPrev);
            this.pnlFooter.Controls.Add(this.btnNext);
            this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFooter.Location = new System.Drawing.Point(0, 549);
            this.pnlFooter.Margin = new System.Windows.Forms.Padding(0);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Padding = new System.Windows.Forms.Padding(9);
            this.pnlFooter.Size = new System.Drawing.Size(621, 53);
            this.pnlFooter.TabIndex = 244;
            // 
            // cbLabelURL
            // 
            this.cbLabelURL.FormattingEnabled = true;
            this.cbLabelURL.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing"});
            this.cbLabelURL.Location = new System.Drawing.Point(164, 284);
            this.cbLabelURL.Name = "cbLabelURL";
            this.cbLabelURL.Size = new System.Drawing.Size(174, 21);
            this.cbLabelURL.TabIndex = 6;
            // 
            // lblErrorCodeMessage
            // 
            this.lblErrorCodeMessage.AutoSize = true;
            this.lblErrorCodeMessage.Location = new System.Drawing.Point(346, 472);
            this.lblErrorCodeMessage.Name = "lblErrorCodeMessage";
            this.lblErrorCodeMessage.Size = new System.Drawing.Size(262, 13);
            this.lblErrorCodeMessage.TabIndex = 268;
            this.lblErrorCodeMessage.Text = "2Ship, or carrier error code (for unsuccessfull shipping)";
            // 
            // cbErrorCode
            // 
            this.cbErrorCode.FormattingEnabled = true;
            this.cbErrorCode.Location = new System.Drawing.Point(164, 469);
            this.cbErrorCode.Name = "cbErrorCode";
            this.cbErrorCode.Size = new System.Drawing.Size(174, 21);
            this.cbErrorCode.TabIndex = 13;
            // 
            // lblErrorCode
            // 
            this.lblErrorCode.AutoSize = true;
            this.lblErrorCode.Location = new System.Drawing.Point(9, 472);
            this.lblErrorCode.Name = "lblErrorCode";
            this.lblErrorCode.Size = new System.Drawing.Size(59, 13);
            this.lblErrorCode.TabIndex = 267;
            this.lblErrorCode.Text = "Error code:";
            // 
            // lblLabelURLMessage
            // 
            this.lblLabelURLMessage.AutoSize = true;
            this.lblLabelURLMessage.Location = new System.Drawing.Point(346, 287);
            this.lblLabelURLMessage.Name = "lblLabelURLMessage";
            this.lblLabelURLMessage.Size = new System.Drawing.Size(157, 13);
            this.lblLabelURLMessage.TabIndex = 266;
            this.lblLabelURLMessage.Text = "2Ship URL for the label PDF file";
            // 
            // lblRateZoneMessage
            // 
            this.lblRateZoneMessage.AutoSize = true;
            this.lblRateZoneMessage.Location = new System.Drawing.Point(346, 448);
            this.lblRateZoneMessage.Name = "lblRateZoneMessage";
            this.lblRateZoneMessage.Size = new System.Drawing.Size(223, 13);
            this.lblRateZoneMessage.TabIndex = 265;
            this.lblRateZoneMessage.Text = "based on the origin and destination addresses";
            // 
            // lblDeliveryMessage
            // 
            this.lblDeliveryMessage.AutoSize = true;
            this.lblDeliveryMessage.Location = new System.Drawing.Point(346, 395);
            this.lblDeliveryMessage.Name = "lblDeliveryMessage";
            this.lblDeliveryMessage.Size = new System.Drawing.Size(214, 13);
            this.lblDeliveryMessage.TabIndex = 264;
            this.lblDeliveryMessage.Text = "free form delivery information from the carrier";
            // 
            // lblBilledWeightMessage
            // 
            this.lblBilledWeightMessage.AutoSize = true;
            this.lblBilledWeightMessage.Location = new System.Drawing.Point(346, 422);
            this.lblBilledWeightMessage.Name = "lblBilledWeightMessage";
            this.lblBilledWeightMessage.Size = new System.Drawing.Size(238, 13);
            this.lblBilledWeightMessage.TabIndex = 263;
            this.lblBilledWeightMessage.Text = "the weight used by the carier to bill your shipment";
            // 
            // lblCommercialInvoiceURLMessage
            // 
            this.lblCommercialInvoiceURLMessage.AutoSize = true;
            this.lblCommercialInvoiceURLMessage.Location = new System.Drawing.Point(346, 314);
            this.lblCommercialInvoiceURLMessage.Name = "lblCommercialInvoiceURLMessage";
            this.lblCommercialInvoiceURLMessage.Size = new System.Drawing.Size(226, 13);
            this.lblCommercialInvoiceURLMessage.TabIndex = 262;
            this.lblCommercialInvoiceURLMessage.Text = "2Ship URL for the CI PDF file (for international)";
            // 
            // lblPriceMessage
            // 
            this.lblPriceMessage.AutoSize = true;
            this.lblPriceMessage.Location = new System.Drawing.Point(346, 341);
            this.lblPriceMessage.Name = "lblPriceMessage";
            this.lblPriceMessage.Size = new System.Drawing.Size(118, 13);
            this.lblPriceMessage.TabIndex = 261;
            this.lblPriceMessage.Text = "your total shipment cost";
            // 
            // lblRateZone
            // 
            this.lblRateZone.AutoSize = true;
            this.lblRateZone.Location = new System.Drawing.Point(9, 446);
            this.lblRateZone.Name = "lblRateZone";
            this.lblRateZone.Size = new System.Drawing.Size(59, 13);
            this.lblRateZone.TabIndex = 260;
            this.lblRateZone.Text = "Rate zone:";
            // 
            // cbBilledWeight
            // 
            this.cbBilledWeight.FormattingEnabled = true;
            this.cbBilledWeight.Location = new System.Drawing.Point(164, 419);
            this.cbBilledWeight.Name = "cbBilledWeight";
            this.cbBilledWeight.Size = new System.Drawing.Size(174, 21);
            this.cbBilledWeight.TabIndex = 11;
            // 
            // lblBilledWeight
            // 
            this.lblBilledWeight.AutoSize = true;
            this.lblBilledWeight.Location = new System.Drawing.Point(9, 422);
            this.lblBilledWeight.Name = "lblBilledWeight";
            this.lblBilledWeight.Size = new System.Drawing.Size(69, 13);
            this.lblBilledWeight.TabIndex = 259;
            this.lblBilledWeight.Text = "Billed weight:";
            // 
            // cbDelivery
            // 
            this.cbDelivery.FormattingEnabled = true;
            this.cbDelivery.Location = new System.Drawing.Point(164, 392);
            this.cbDelivery.Name = "cbDelivery";
            this.cbDelivery.Size = new System.Drawing.Size(174, 21);
            this.cbDelivery.TabIndex = 10;
            // 
            // lblDelivery
            // 
            this.lblDelivery.AutoSize = true;
            this.lblDelivery.Location = new System.Drawing.Point(9, 395);
            this.lblDelivery.Name = "lblDelivery";
            this.lblDelivery.Size = new System.Drawing.Size(95, 13);
            this.lblDelivery.TabIndex = 258;
            this.lblDelivery.Text = "Shipment Delivery:";
            // 
            // cbCurrency
            // 
            this.cbCurrency.FormattingEnabled = true;
            this.cbCurrency.Location = new System.Drawing.Point(164, 365);
            this.cbCurrency.Name = "cbCurrency";
            this.cbCurrency.Size = new System.Drawing.Size(174, 21);
            this.cbCurrency.TabIndex = 9;
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(9, 368);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(78, 13);
            this.lblCurrency.TabIndex = 257;
            this.lblCurrency.Text = "Price currency:";
            // 
            // cbPrice
            // 
            this.cbPrice.FormattingEnabled = true;
            this.cbPrice.Items.AddRange(new object[] {
            "Customer Packaging",
            "Envelope (Letter)",
            "Small Pack"});
            this.cbPrice.Location = new System.Drawing.Point(164, 338);
            this.cbPrice.Name = "cbPrice";
            this.cbPrice.Size = new System.Drawing.Size(174, 21);
            this.cbPrice.TabIndex = 8;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(9, 341);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(81, 13);
            this.lblPrice.TabIndex = 256;
            this.lblPrice.Text = "Shipment Price:";
            // 
            // cbCommercialInvoiceURL
            // 
            this.cbCommercialInvoiceURL.FormattingEnabled = true;
            this.cbCommercialInvoiceURL.Items.AddRange(new object[] {
            "(none)",
            "Cheapest",
            "Fastest"});
            this.cbCommercialInvoiceURL.Location = new System.Drawing.Point(164, 311);
            this.cbCommercialInvoiceURL.Name = "cbCommercialInvoiceURL";
            this.cbCommercialInvoiceURL.Size = new System.Drawing.Size(174, 21);
            this.cbCommercialInvoiceURL.TabIndex = 7;
            // 
            // lblCommercialInvoiceURL
            // 
            this.lblCommercialInvoiceURL.AutoSize = true;
            this.lblCommercialInvoiceURL.Location = new System.Drawing.Point(9, 314);
            this.lblCommercialInvoiceURL.Name = "lblCommercialInvoiceURL";
            this.lblCommercialInvoiceURL.Size = new System.Drawing.Size(126, 13);
            this.lblCommercialInvoiceURL.TabIndex = 255;
            this.lblCommercialInvoiceURL.Text = "Commercial invoice URL:";
            // 
            // lblLabelURL
            // 
            this.lblLabelURL.AutoSize = true;
            this.lblLabelURL.Location = new System.Drawing.Point(9, 287);
            this.lblLabelURL.Name = "lblLabelURL";
            this.lblLabelURL.Size = new System.Drawing.Size(61, 13);
            this.lblLabelURL.TabIndex = 254;
            this.lblLabelURL.Text = "Label URL:";
            // 
            // lblCurrencyMessage
            // 
            this.lblCurrencyMessage.AutoSize = true;
            this.lblCurrencyMessage.Location = new System.Drawing.Point(346, 368);
            this.lblCurrencyMessage.Name = "lblCurrencyMessage";
            this.lblCurrencyMessage.Size = new System.Drawing.Size(154, 13);
            this.lblCurrencyMessage.TabIndex = 276;
            this.lblCurrencyMessage.Text = "three letters ISO currency code";
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNote.Location = new System.Drawing.Point(9, 529);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(491, 13);
            this.lblNote.TabIndex = 328;
            this.lblNote.Text = "Note: You will be able to add more variable mappings later, in the Summary Review" +
                " page of this wizard.";
            // 
            // cbShipmentNumber
            // 
            this.cbShipmentNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbShipmentNumber.FormattingEnabled = true;
            this.cbShipmentNumber.Location = new System.Drawing.Point(220, 169);
            this.cbShipmentNumber.Name = "cbShipmentNumber";
            this.cbShipmentNumber.Size = new System.Drawing.Size(174, 21);
            this.cbShipmentNumber.TabIndex = 2;
            // 
            // lblShipmentNumber
            // 
            this.lblShipmentNumber.AutoSize = true;
            this.lblShipmentNumber.Location = new System.Drawing.Point(9, 172);
            this.lblShipmentNumber.Name = "lblShipmentNumber";
            this.lblShipmentNumber.Size = new System.Drawing.Size(109, 13);
            this.lblShipmentNumber.TabIndex = 330;
            this.lblShipmentNumber.Text = "Shipments Id Column:";
            // 
            // lblShipmentsOrderNumberColumnMessage
            // 
            this.lblShipmentsOrderNumberColumnMessage.AutoSize = true;
            this.lblShipmentsOrderNumberColumnMessage.Location = new System.Drawing.Point(400, 145);
            this.lblShipmentsOrderNumberColumnMessage.Name = "lblShipmentsOrderNumberColumnMessage";
            this.lblShipmentsOrderNumberColumnMessage.Size = new System.Drawing.Size(135, 13);
            this.lblShipmentsOrderNumberColumnMessage.TabIndex = 331;
            this.lblShipmentsOrderNumberColumnMessage.Text = "optional, but recommended";
            // 
            // lblShipmentsStatusColumnMessage
            // 
            this.lblShipmentsStatusColumnMessage.AutoSize = true;
            this.lblShipmentsStatusColumnMessage.Location = new System.Drawing.Point(400, 199);
            this.lblShipmentsStatusColumnMessage.Name = "lblShipmentsStatusColumnMessage";
            this.lblShipmentsStatusColumnMessage.Size = new System.Drawing.Size(135, 13);
            this.lblShipmentsStatusColumnMessage.TabIndex = 332;
            this.lblShipmentsStatusColumnMessage.Text = "optional, but recommended";
            // 
            // lblShipmentNumberMessage
            // 
            this.lblShipmentNumberMessage.AutoSize = true;
            this.lblShipmentNumberMessage.Location = new System.Drawing.Point(400, 172);
            this.lblShipmentNumberMessage.Name = "lblShipmentNumberMessage";
            this.lblShipmentNumberMessage.Size = new System.Drawing.Size(211, 13);
            this.lblShipmentNumberMessage.TabIndex = 333;
            this.lblShipmentNumberMessage.Text = "required, uniquely assigned by 2Ship carrier";
            // 
            // lblStatusFilter
            // 
            this.lblStatusFilter.AutoSize = true;
            this.lblStatusFilter.Location = new System.Drawing.Point(9, 225);
            this.lblStatusFilter.Name = "lblStatusFilter";
            this.lblStatusFilter.Size = new System.Drawing.Size(201, 13);
            this.lblStatusFilter.TabIndex = 334;
            this.lblStatusFilter.Text = "Use the Status column to filter the orders:";
            // 
            // rbStatusFilterYes
            // 
            this.rbStatusFilterYes.AutoSize = true;
            this.rbStatusFilterYes.Location = new System.Drawing.Point(220, 223);
            this.rbStatusFilterYes.Name = "rbStatusFilterYes";
            this.rbStatusFilterYes.Size = new System.Drawing.Size(43, 17);
            this.rbStatusFilterYes.TabIndex = 4;
            this.rbStatusFilterYes.TabStop = true;
            this.rbStatusFilterYes.Text = "Yes";
            this.rbStatusFilterYes.UseVisualStyleBackColor = true;
            // 
            // rbStatusFilterNo
            // 
            this.rbStatusFilterNo.AutoSize = true;
            this.rbStatusFilterNo.Location = new System.Drawing.Point(269, 223);
            this.rbStatusFilterNo.Name = "rbStatusFilterNo";
            this.rbStatusFilterNo.Size = new System.Drawing.Size(39, 17);
            this.rbStatusFilterNo.TabIndex = 5;
            this.rbStatusFilterNo.TabStop = true;
            this.rbStatusFilterNo.Text = "No";
            this.rbStatusFilterNo.UseVisualStyleBackColor = true;
            // 
            // frmWizard08
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(621, 602);
            this.Controls.Add(this.rbStatusFilterNo);
            this.Controls.Add(this.rbStatusFilterYes);
            this.Controls.Add(this.lblStatusFilter);
            this.Controls.Add(this.lblShipmentNumberMessage);
            this.Controls.Add(this.lblShipmentsStatusColumnMessage);
            this.Controls.Add(this.lblShipmentsOrderNumberColumnMessage);
            this.Controls.Add(this.cbShipmentNumber);
            this.Controls.Add(this.lblShipmentNumber);
            this.Controls.Add(this.lblNote);
            this.Controls.Add(this.lblCurrencyMessage);
            this.Controls.Add(this.cbErrorMessage);
            this.Controls.Add(this.lblErrorMessage);
            this.Controls.Add(this.lblShipmentsMappingLine);
            this.Controls.Add(this.cbRateZone);
            this.Controls.Add(this.lblShipmentsMapping);
            this.Controls.Add(this.cbShipmentsStatusColumn);
            this.Controls.Add(this.lblShipmentsStatusColumn);
            this.Controls.Add(this.cbShipmentsOrderNumberColumn);
            this.Controls.Add(this.lblShipmentsOrderNumberColumn);
            this.Controls.Add(this.lblShipmentsTable);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.cbShipmentsTable);
            this.Controls.Add(this.pnlFooter);
            this.Controls.Add(this.cbLabelURL);
            this.Controls.Add(this.lblErrorCodeMessage);
            this.Controls.Add(this.cbErrorCode);
            this.Controls.Add(this.lblErrorCode);
            this.Controls.Add(this.lblLabelURLMessage);
            this.Controls.Add(this.lblRateZoneMessage);
            this.Controls.Add(this.lblDeliveryMessage);
            this.Controls.Add(this.lblBilledWeightMessage);
            this.Controls.Add(this.lblCommercialInvoiceURLMessage);
            this.Controls.Add(this.lblPriceMessage);
            this.Controls.Add(this.lblRateZone);
            this.Controls.Add(this.cbBilledWeight);
            this.Controls.Add(this.lblBilledWeight);
            this.Controls.Add(this.cbDelivery);
            this.Controls.Add(this.lblDelivery);
            this.Controls.Add(this.cbCurrency);
            this.Controls.Add(this.lblCurrency);
            this.Controls.Add(this.cbPrice);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.cbCommercialInvoiceURL);
            this.Controls.Add(this.lblCommercialInvoiceURL);
            this.Controls.Add(this.lblLabelURL);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWizard08";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 8 of 11";
            this.Load += new System.EventHandler(this.frmWizard08_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard08_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWizard08_FormClosing);
            this.pnlHeader.ResumeLayout(false);
            this.pnlFooter.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeaderLine;
        private System.Windows.Forms.LinkLabel llAdvanced;
        private System.Windows.Forms.ComboBox cbErrorMessage;
        private System.Windows.Forms.Label lblErrorMessage;
        private System.Windows.Forms.Label lblShipmentsMappingLine;
        private System.Windows.Forms.ComboBox cbRateZone;
        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Label lblShipmentsMapping;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.ComboBox cbShipmentsStatusColumn;
        private System.Windows.Forms.Label lblShipmentsStatusColumn;
        private System.Windows.Forms.ComboBox cbShipmentsOrderNumberColumn;
        private System.Windows.Forms.Label lblShipmentsOrderNumberColumn;
        private System.Windows.Forms.Label lblShipmentsTable;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label lblHeaderIcon;
        private System.Windows.Forms.Label lblHeaderInfo;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.ComboBox cbShipmentsTable;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.ComboBox cbLabelURL;
        private System.Windows.Forms.Label lblErrorCodeMessage;
        private System.Windows.Forms.ComboBox cbErrorCode;
        private System.Windows.Forms.Label lblErrorCode;
        private System.Windows.Forms.Label lblLabelURLMessage;
        private System.Windows.Forms.Label lblRateZoneMessage;
        private System.Windows.Forms.Label lblDeliveryMessage;
        private System.Windows.Forms.Label lblBilledWeightMessage;
        private System.Windows.Forms.Label lblCommercialInvoiceURLMessage;
        private System.Windows.Forms.Label lblPriceMessage;
        private System.Windows.Forms.Label lblRateZone;
        private System.Windows.Forms.ComboBox cbBilledWeight;
        private System.Windows.Forms.Label lblBilledWeight;
        private System.Windows.Forms.ComboBox cbDelivery;
        private System.Windows.Forms.Label lblDelivery;
        private System.Windows.Forms.ComboBox cbCurrency;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.ComboBox cbPrice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.ComboBox cbCommercialInvoiceURL;
        private System.Windows.Forms.Label lblCommercialInvoiceURL;
        private System.Windows.Forms.Label lblLabelURL;
        private System.Windows.Forms.Label lblCurrencyMessage;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.ComboBox cbShipmentNumber;
        private System.Windows.Forms.Label lblShipmentNumber;
        private System.Windows.Forms.Label lblShipmentsOrderNumberColumnMessage;
        private System.Windows.Forms.Label lblShipmentsStatusColumnMessage;
        private System.Windows.Forms.Label lblShipmentNumberMessage;
        private System.Windows.Forms.Label lblStatusFilter;
        private System.Windows.Forms.RadioButton rbStatusFilterYes;
        private System.Windows.Forms.RadioButton rbStatusFilterNo;
    }
}