﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmInputBox : Form
    {
        private static frmInputBox frm_input = null;

        public frmInputBox()
        {
            InitializeComponent();
        }

        public static string InputBox(IWin32Window Owner, string Name)
        {
            frm_input = new frmInputBox();
            frm_input.lblInput.Text = "Please enter the value for " + Name + ":";
            if (frm_input.ShowDialog(Owner) == DialogResult.OK) return frm_input.txtInput.Text;
            return "";
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {

        }
    }
}
