﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard_Filter : Form
    {
        // all the columns from all the tables in the Orders select statement
        private List<string> columns = new List<string>();

        #region Events

        public frmWizard_Filter()
        {
            InitializeComponent();

            lblManualEdit.Visible = false;
            pnlGrid.Top = 94;
            pnlGrid.Height = chkEdit.Top - 163; // -6 - 57 - 6 - 94;
            txtFilter.Top = pnlGrid.Bottom + 6;
            txtFilter.Height = 57;
            txtFilter.Anchor = AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            txtFilter.ReadOnly = true;

            // populate the operand comboboxes with all order columns
            List<string> tables = new List<string>();
            foreach (Binding binding in Template.Bindings)
                if (!binding.Field.Category.Equals("Shipments") && !tables.Contains(binding.Table.Name))
                {
                    //replace Quote with StartQuote and EndQuote
                    foreach (string column in Template.ReadConn.Get_Columns(binding.Table.Name))
                        columns.Add(Template.ReadConn.StartQuote + binding.Table.Name + Template.ReadConn.EndQuote + '.' + Template.ReadConn.StartQuote + column + Template.ReadConn.EndQuote);
                    tables.Add(binding.Table.Name);
                }
            Left_Operand_Column.Items.AddRange(columns.ToArray());
            Right_Operand_1_Column.Items.AddRange(columns.ToArray());
            Right_Operand_2_Column.Items.AddRange(columns.ToArray());

            // load existing filter
            chkEdit.Checked = Select_Filter.Is_Custom;
            if (chkEdit.Checked) chkEdit_CheckedChanged(this, null);

            Operator_Column.DataSource = Filter_Operator.All;
            Join_Type_Column.DataSource = new Filter_Expression_Join_Type[] { Filter_Expression_Join_Type.AND, Filter_Expression_Join_Type.OR, Filter_Expression_Join_Type.NONE };
            if (!Select_Filter.Is_Empty && !Select_Filter.Is_Custom)
                foreach (Filter_Expression expression in Select_Filter.Expressions)
                    if (expression.Is_Constant && !columns.Contains(expression.Right_Operand_1_No_Quotes))
                        Right_Operand_1_Column.Items.Add(expression.Right_Operand_1_No_Quotes);
            
            //data source must be set to the grid's binding source
            //setting only the grid's data source will cause internal errors
                       
            //dgvFilter.DataSource = Select_Filter.Expressions;
            filterExpressionBindingSource.DataSource = Select_Filter.Expressions;
            //dgvFilter.DataSource = filterExpressionBindingSource;
            Update_Filter_Text();
        }

        private void chkEdit_CheckedChanged(object sender, EventArgs e)
        {
            //save the user typed filter in expresions
            if (chkEdit.Checked)
            {
                lblManualEdit.Visible = true;
                pnlGrid.Visible = false;
                txtFilter.Top = lblManualEdit.Bottom + 6;
                txtFilter.Height = chkEdit.Top - 6 - txtFilter.Top;
                txtFilter.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
                txtFilter.ReadOnly = false;
                
            }
            else
            {
                try
                {
                lblManualEdit.Visible = false;
                pnlGrid.Visible = true;
                pnlGrid.Top = 94;
                pnlGrid.Height = chkEdit.Top - 163; // -6 - 57 - 6 - 94;
                txtFilter.Top = pnlGrid.Bottom + 6;
                txtFilter.Height = 57;
                txtFilter.Anchor = AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
                txtFilter.ReadOnly = true;
                
                //save the manual edited filter
                if (txtFilter.Text != null)
                    {
                        foreach (string txtLine in txtFilter.Lines)
                        {
                          if (txtLine.Trim().Length > 1)
                            {
                                String sFilter = txtLine;
                                int i = sFilter.LastIndexOf(")");
                                if (i > 0)
                                {
                                    sFilter.Insert(i, "   ");
                                }
                                Filter_Expression manualFilter = new Filter_Expression(sFilter);

                                String s = manualFilter.Right_Operand_1_No_Quotes.ToString();

                                //the right_operand_1 written by the user might not be in the right_operand_1_items list
                                Filter_Expression testFilter = manualFilter;
                                if (Left_Operand_Column.Items.Contains(manualFilter.Left_Operand))
                                {
                                    foreach (DataGridViewRow r in dgvFilter.SelectedRows)
                                    {
                                        if (r.DataBoundItem != null)
                                        {
                                            Select_Filter.Remove(new Filter_Expression(r.DataBoundItem.ToString()));
                                        }
                                    }
                                    if (!Right_Operand_1_Column.Items.Contains(manualFilter.Right_Operand_1_No_Quotes))
                                    {
                                        Right_Operand_1_Column.Items.Add(manualFilter.Right_Operand_1_No_Quotes);
                                    }
                                    
                                    Select_Filter.Add(manualFilter);
                                }
                                else
                                {
                                    MessageBox.Show("The table column name is not valid", "Warning", MessageBoxButtons.OK);
                                }
                            }
                        }
                        //data source must be set to the grid's binding source
                        //setting only the grid's data source will cause internal errors

                        filterExpressionBindingSource.DataSource = null;
                        filterExpressionBindingSource.DataSource = Select_Filter.Expressions;
                        dgvFilter.DataSource = filterExpressionBindingSource;
                    }
                    Select_Filter.Is_Custom = false;

                    //dgvFilter.DataSource = null;
                    //dgvFilter.DataSource = Select_Filter.Expressions;
                    Update_Filter_Text();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                }
            }
        }

        private void btnAddExpression_Click(object sender, EventArgs e)
        {
            Select_Filter.Add(new Filter_Expression(columns[0], Filter_Operator.Equal, "\'\'", null, Filter_Expression_Join_Type.AND));
            
            //dgvFilter.DataSource = null;
            //dgvFilter.DataSource = Select_Filter.Expressions;

            //data source must be set to the grid's binding source
            //setting only the grid's data source will cause internal errors
                       
            filterExpressionBindingSource.DataSource = null;
            filterExpressionBindingSource.DataSource = Select_Filter.Expressions;
            dgvFilter.DataSource = filterExpressionBindingSource;
            
            Update_Filter_Text();
        }

        private void btnRemoveExpression_Click(object sender, EventArgs e)
        {
            try{
                if (dgvFilter.Rows.Count > 0)
                {
                    //dgvFilter CurrentRow => NullReferenceException
                    if (dgvFilter.CurrentRow.DataBoundItem != null)
                    {
                        Select_Filter.Remove(dgvFilter.CurrentRow.DataBoundItem as Filter_Expression);
                    }
                        //data source must be set to the grid's binding source
                        //setting only the grid's data source will cause internal errors
                        filterExpressionBindingSource.DataSource = null;
                        filterExpressionBindingSource.DataSource = Select_Filter.Expressions;
                        dgvFilter.DataSource = filterExpressionBindingSource;

                        Update_Filter_Text();
                    }
                    else
                    {
                        //dgvFilter has no selected row => NullReferenceException
                        MessageBox.Show("There is no filter selected", "Warning", MessageBoxButtons.OK,MessageBoxIcon.Warning);

                    }
                }
          catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Validate_Filter()) return;
                string s = string.IsNullOrEmpty(Select_Filter.Filter_With_Status) ? "" : "\nWHERE\n" + Select_Filter.Filter_With_Status;
                new frmWizard_DataPreview("Orders Filter Test - Showing First 100 Filtered Records", Template.ReadConn.ConnStr, Orders_Select_Statement.Select.Replace("SELECT","SELECT TOP 100") + s + "ORDER BY 1").ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!Validate_Filter())
            {
                MessageBox.Show(this, "There are validation errors.", "InteGr8 Wizard Filter", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DialogResult = DialogResult.OK;
        }

        #endregion

        #region GridView events

        private void dgvFilter_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //if operator is like the right_operand_1 must be in the form '%1234%'
            foreach (DataGridViewRow r in dgvFilter.SelectedRows)
            {
                Filter_Expression f = new Filter_Expression(r.DataBoundItem.ToString());
                if (f.Operator.Equals(Filter_Operator.Like) && !f.Right_Operand_1_No_Quotes.Equals(""))
                {
                    int i = -1;
                    i = f.Right_Operand_1_No_Quotes.IndexOf('%');
                    if (i == -1)
                    {
                        Select_Filter.Remove(f);
                        //f.Right_Operand_1_No_Quotes = '%' + f.Right_Operand_1_No_Quotes + '%';
                        String likeOperand = f.Right_Operand_1_No_Quotes.ToString();

                        //the right_operand_1 written by the user, must be in the form '%1234%' in the right_operand_1_items list
                        //so the grid can load it
                        if (Right_Operand_1_Column.Items.Contains(likeOperand))
                        {
                            Right_Operand_1_Column.Items.Remove(likeOperand);
                        }
                        likeOperand = '%' + likeOperand + '%';
                        Right_Operand_1_Column.Items.Add(likeOperand);
                        
                        f.Right_Operand_1_No_Quotes = likeOperand;
                        Select_Filter.Add(f);
                        //data source must be set to the grid's binding source
                        //setting only the grid's data source will cause internal errors
                        filterExpressionBindingSource.DataSource = null;
                        filterExpressionBindingSource.DataSource = Select_Filter.Expressions;
                        dgvFilter.DataSource = filterExpressionBindingSource;
                    }
                }
            }
            Update_Filter_Text();
        }

        private void dgvFilter_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null && (dgvFilter.CurrentCellAddress.X == Right_Operand_1_Column.DisplayIndex || dgvFilter.CurrentCellAddress.X == Right_Operand_2_Column.DisplayIndex))
            {
                ComboBox cb = e.Control as ComboBox;
                string cell_value = cb.Text;
                cb.Items.Clear();
                cb.DropDownStyle = ComboBoxStyle.DropDown;
                cb.Items.AddRange(columns.ToArray());
                cb.Items.Add(cell_value);
                cb.Text = cell_value;
            }
        }

        private void dgvFilter_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            // add the new item to the gridview combo (the combo columns are never used, but will give errors otherwise)
            if (e.ColumnIndex == Right_Operand_1_Column.DisplayIndex && !Right_Operand_1_Column.Items.Contains(e.FormattedValue))
                Right_Operand_1_Column.Items.Add(e.FormattedValue);
            else if (e.ColumnIndex == Right_Operand_2_Column.DisplayIndex && !Right_Operand_2_Column.Items.Contains(e.FormattedValue))
                Right_Operand_2_Column.Items.Add(e.FormattedValue);
        }

        private void dgvFilter_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            Update_Filter_Text();
        }

        #endregion

        #region Private methods

        private void Update_Filter_Text()
        {
            if (!Validate_Filter()) return;
            txtFilter.Lines = Select_Filter.Filter_No_Status.Split('\n');
        }

        private bool Validate_Filter()
        {
            dgvFilter.EndEdit();
            bool valid = true;
            foreach (DataGridViewRow Row in dgvFilter.Rows)
                if (!Row.IsNewRow && Row.Cells[1].Value != null)
                {
                    //Row.ErrorText = ((Filter_Expression) Row.DataBoundItem).Validate();
                    if (valid && !String.IsNullOrEmpty(Row.ErrorText)) valid = false;
                }
            return valid;
        }

        #endregion

        private void frmWizard_Filter_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_Filter");
            e.Cancel = true;
        }

        
    }
}
