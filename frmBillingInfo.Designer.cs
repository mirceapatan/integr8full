﻿namespace InteGr8
{
    partial class frmBillingInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillingInfo));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbBillingAccount = new System.Windows.Forms.TextBox();
            this.tbBillingCompany = new System.Windows.Forms.TextBox();
            this.tbBillingContact = new System.Windows.Forms.TextBox();
            this.tbBillingCountry = new System.Windows.Forms.TextBox();
            this.tbBillingState = new System.Windows.Forms.TextBox();
            this.tbBillingCity = new System.Windows.Forms.TextBox();
            this.tbBillingAddress1 = new System.Windows.Forms.TextBox();
            this.tbBillingAddress2 = new System.Windows.Forms.TextBox();
            this.tbBillingZIP = new System.Windows.Forms.TextBox();
            this.tbBillingTel = new System.Windows.Forms.TextBox();
            this.tbBillingEmail = new System.Windows.Forms.TextBox();
            this.btnAddBillingInfo = new System.Windows.Forms.Button();
            this.btnCancelBilling = new System.Windows.Forms.Button();
            this.cbBillingAccountsList = new System.Windows.Forms.ComboBox();
            this.btnAddBillingAccount = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Billing account number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Billing company";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Billing contact";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Billing country";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(213, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Billing state";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 157);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Billing city";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Billing address 1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 230);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Billing address 2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 263);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Billing ZIP";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(205, 262);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Billing tel";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 296);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Billing email";
            // 
            // tbBillingAccount
            // 
            this.tbBillingAccount.Location = new System.Drawing.Point(142, 24);
            this.tbBillingAccount.Name = "tbBillingAccount";
            this.tbBillingAccount.Size = new System.Drawing.Size(251, 20);
            this.tbBillingAccount.TabIndex = 12;
            // 
            // tbBillingCompany
            // 
            this.tbBillingCompany.Location = new System.Drawing.Point(142, 54);
            this.tbBillingCompany.Name = "tbBillingCompany";
            this.tbBillingCompany.Size = new System.Drawing.Size(251, 20);
            this.tbBillingCompany.TabIndex = 13;
            // 
            // tbBillingContact
            // 
            this.tbBillingContact.Location = new System.Drawing.Point(142, 87);
            this.tbBillingContact.Name = "tbBillingContact";
            this.tbBillingContact.Size = new System.Drawing.Size(251, 20);
            this.tbBillingContact.TabIndex = 14;
            // 
            // tbBillingCountry
            // 
            this.tbBillingCountry.Location = new System.Drawing.Point(86, 119);
            this.tbBillingCountry.Name = "tbBillingCountry";
            this.tbBillingCountry.Size = new System.Drawing.Size(114, 20);
            this.tbBillingCountry.TabIndex = 15;
            // 
            // tbBillingState
            // 
            this.tbBillingState.Location = new System.Drawing.Point(279, 119);
            this.tbBillingState.Name = "tbBillingState";
            this.tbBillingState.Size = new System.Drawing.Size(114, 20);
            this.tbBillingState.TabIndex = 16;
            // 
            // tbBillingCity
            // 
            this.tbBillingCity.Location = new System.Drawing.Point(144, 157);
            this.tbBillingCity.Name = "tbBillingCity";
            this.tbBillingCity.Size = new System.Drawing.Size(251, 20);
            this.tbBillingCity.TabIndex = 17;
            // 
            // tbBillingAddress1
            // 
            this.tbBillingAddress1.Location = new System.Drawing.Point(144, 193);
            this.tbBillingAddress1.Name = "tbBillingAddress1";
            this.tbBillingAddress1.Size = new System.Drawing.Size(251, 20);
            this.tbBillingAddress1.TabIndex = 18;
            // 
            // tbBillingAddress2
            // 
            this.tbBillingAddress2.Location = new System.Drawing.Point(144, 227);
            this.tbBillingAddress2.Name = "tbBillingAddress2";
            this.tbBillingAddress2.Size = new System.Drawing.Size(251, 20);
            this.tbBillingAddress2.TabIndex = 19;
            // 
            // tbBillingZIP
            // 
            this.tbBillingZIP.Location = new System.Drawing.Point(77, 259);
            this.tbBillingZIP.Name = "tbBillingZIP";
            this.tbBillingZIP.Size = new System.Drawing.Size(123, 20);
            this.tbBillingZIP.TabIndex = 20;
            // 
            // tbBillingTel
            // 
            this.tbBillingTel.Location = new System.Drawing.Point(259, 259);
            this.tbBillingTel.Name = "tbBillingTel";
            this.tbBillingTel.Size = new System.Drawing.Size(136, 20);
            this.tbBillingTel.TabIndex = 21;
            // 
            // tbBillingEmail
            // 
            this.tbBillingEmail.Location = new System.Drawing.Point(144, 293);
            this.tbBillingEmail.Name = "tbBillingEmail";
            this.tbBillingEmail.Size = new System.Drawing.Size(251, 20);
            this.tbBillingEmail.TabIndex = 22;
            // 
            // btnAddBillingInfo
            // 
            this.btnAddBillingInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddBillingInfo.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAddBillingInfo.Location = new System.Drawing.Point(228, 435);
            this.btnAddBillingInfo.Name = "btnAddBillingInfo";
            this.btnAddBillingInfo.Size = new System.Drawing.Size(75, 23);
            this.btnAddBillingInfo.TabIndex = 23;
            this.btnAddBillingInfo.Tag = "Use the currently selected account to the current order";
            this.btnAddBillingInfo.Text = "OK";
            this.btnAddBillingInfo.UseVisualStyleBackColor = true;
            this.btnAddBillingInfo.Click += new System.EventHandler(this.btnAddBillingInfo_Click);
            // 
            // btnCancelBilling
            // 
            this.btnCancelBilling.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelBilling.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelBilling.Location = new System.Drawing.Point(340, 435);
            this.btnCancelBilling.Name = "btnCancelBilling";
            this.btnCancelBilling.Size = new System.Drawing.Size(75, 23);
            this.btnCancelBilling.TabIndex = 24;
            this.btnCancelBilling.Tag = "Cancel";
            this.btnCancelBilling.Text = "Cancel";
            this.btnCancelBilling.UseVisualStyleBackColor = true;
            this.btnCancelBilling.Click += new System.EventHandler(this.btnCancelBilling_Click);
            // 
            // cbBillingAccountsList
            // 
            this.cbBillingAccountsList.FormattingEnabled = true;
            this.cbBillingAccountsList.Location = new System.Drawing.Point(174, 20);
            this.cbBillingAccountsList.Name = "cbBillingAccountsList";
            this.cbBillingAccountsList.Size = new System.Drawing.Size(241, 21);
            this.cbBillingAccountsList.TabIndex = 25;
            this.cbBillingAccountsList.Tag = "The dropdown list of already introduced billing accounts";
            this.cbBillingAccountsList.SelectedIndexChanged += new System.EventHandler(this.cbBillingAccountsList_SelectedIndexChanged);
            // 
            // btnAddBillingAccount
            // 
            this.btnAddBillingAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddBillingAccount.Location = new System.Drawing.Point(96, 435);
            this.btnAddBillingAccount.Name = "btnAddBillingAccount";
            this.btnAddBillingAccount.Size = new System.Drawing.Size(83, 23);
            this.btnAddBillingAccount.TabIndex = 26;
            this.btnAddBillingAccount.Tag = "Add a new account to the billing accounts list";
            this.btnAddBillingAccount.Text = "Add account";
            this.btnAddBillingAccount.UseVisualStyleBackColor = true;
            this.btnAddBillingAccount.Click += new System.EventHandler(this.btnAddBillingAccount_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(18, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(150, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Select a Billing account :";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(18, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(161, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Or enter a new one below :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbBillingAccount);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbBillingCompany);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbBillingContact);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbBillingEmail);
            this.groupBox1.Controls.Add(this.tbBillingCountry);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tbBillingTel);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbBillingZIP);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tbBillingState);
            this.groupBox1.Controls.Add(this.tbBillingAddress2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbBillingAddress1);
            this.groupBox1.Controls.Add(this.tbBillingCity);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(21, 80);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(428, 323);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Billing account information";
            // 
            // frmBillingInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 470);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnAddBillingAccount);
            this.Controls.Add(this.cbBillingAccountsList);
            this.Controls.Add(this.btnCancelBilling);
            this.Controls.Add(this.btnAddBillingInfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBillingInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Billing Account Info";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbBillingAccount;
        private System.Windows.Forms.TextBox tbBillingCompany;
        private System.Windows.Forms.TextBox tbBillingContact;
        private System.Windows.Forms.TextBox tbBillingCountry;
        private System.Windows.Forms.TextBox tbBillingState;
        private System.Windows.Forms.TextBox tbBillingCity;
        private System.Windows.Forms.TextBox tbBillingAddress1;
        private System.Windows.Forms.TextBox tbBillingAddress2;
        private System.Windows.Forms.TextBox tbBillingZIP;
        private System.Windows.Forms.TextBox tbBillingTel;
        private System.Windows.Forms.TextBox tbBillingEmail;
        private System.Windows.Forms.Button btnAddBillingInfo;
        private System.Windows.Forms.Button btnCancelBilling;
        private System.Windows.Forms.ComboBox cbBillingAccountsList;
        private System.Windows.Forms.Button btnAddBillingAccount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}