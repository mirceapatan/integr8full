﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard_Tables : Form
    {
        public readonly TableType Type;

        public frmWizard_Tables(TableType Type, int Step)
        {
            InitializeComponent();

            if (Type.ToString() == TableType.Other) throw new ArgumentException();
            this.Type = Type;
            SplitContainer.SplitterDistance = SplitContainer.Width / 2;
            Text = "InteGr8 Template Wizard - Page " + Step;
            lblTables.Text = "Select the " + Type.ToString() + " table:";
            lblHeaderTitle.Text = Type.ToString() + " Table";

            LoadTables();
        }

        private void LoadTables()
        {
            lbTables.Items.Clear();
            lbTables.Items.AddRange((Type == TableType.ShipmentsType ? Template.WriteConn : Template.ReadConn).Get_Catalog(chkShowTables.Checked, chkShowViews.Checked).ToArray());

            if (lbTables.Items.Count == 0)
            {
                llDirectLink.Visible = false;
                llIndirectLink.Visible = false;
                btnPreview.Enabled = false;
                btnNext.Enabled = false;
                MessageBox.Show(this, "The are not tables in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                btnPreview.Enabled = true;
                btnNext.Enabled = true;
                btnNext.Enabled = true;
            }
            lbTables.SelectedIndex = 0;

            llDirectLink.Visible = false;
            llIndirectLink.Visible = false;

            Table Table = Template.FindTable(Type);
            if (Table != null)
            {
                lbTables.SelectedIndex = lbTables.FindStringExact(Table.Name);
                if (lbTables.SelectedIndex == -1)
                {
                    MessageBox.Show(this, "The " + Type.ToString() + " table '" + Table.Name + "' was not found in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void frmWizard02_Load(object sender, EventArgs e)
        {
            this.ActiveControl = lbTables;
            lbTables_SelectedIndexChanged(sender, e);
        }

        private void lbTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbColumns.Enabled = true;
            lbColumns.Items.Clear();

            List<Table> Tables = Template.FindTables(lbTables.SelectedItem.ToString());
            if (Type.ToString() != TableType.Orders)
            {
                llDirectLink.Visible = true;
                llIndirectLink.Visible = true;
                if (Tables.Count > 0)
                {
                    llDirectLink.Visible = true;
                    llIndirectLink.Visible = true;
                    //lbColumns.Enabled = false;
                    foreach (Table Table in Tables)
                        if (Table.Type.ToString() == TableType.Orders)
                        {
                            llDirectLink.Visible = false;
                            llIndirectLink.Visible = false;
                            break;
                        }
                }
            }

            // read the table columns
            IEnumerable<string> columns = (Type == TableType.ShipmentsType ? Template.WriteConn : Template.ReadConn).Get_Columns(lbTables.SelectedItem.ToString());
            lbColumns.Items.AddRange(columns.ToArray());
            if (lbColumns.Items.Count > 0)
            {
                if (Tables.Count > 0)
                    foreach (string Column in Tables[0].PK)
                        lbColumns.SelectedItems.Add(Column);
                else lbColumns.SelectedIndex = 0;
            }

            switch (Type.ToString())
            {
                case TableType.Senders:
                    llDirectLink.Tag = Template.FindLink(Template.Senders, Template.Orders);
                    //llIndirectLink.Tag = Template.FindIndirectLink(Template.Orders, Template.OrderSenders);
                    break;
                    
                case TableType.Recipients:
                    llDirectLink.Tag = Template.FindLink(Template.Recipients, Template.Orders);
                    //llIndirectLink.Tag = Template.FindIndirectLink(Template.Orders, Template.OrderRecipients);
                    break;

                case TableType.Packages:
                    llDirectLink.Tag = Template.FindLink(Template.Orders, Template.Packages);
                    //llIndirectLink.Tag = Template.FindIndirectLink(Template.Orders, Template.OrderPackages);
                    break;

                case TableType.Products:
                    llDirectLink.Tag = Template.FindLink(Template.Orders, Template.Products);
                    //llIndirectLink.Tag = Template.FindIndirectLink(Template.Orders, Template.OrderProduts);
                    break;

                case TableType.Shipments:
                    llDirectLink.Tag = Template.FindLink(Template.Orders, Template.Shipments);
                    //llIndirectLink.Tag = Template.FindIndirectLink(Template.Orders, Template.Shipments);
                    break;
            }
            if (llDirectLink.Tag != null || llIndirectLink.Tag != null)
            {
                llDirectLink.Visible = llIndirectLink.Tag == null;
                llIndirectLink.Visible = llDirectLink.Tag == null;
            }
        }

        private void llDirectLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmWizard_DirectLink link_editor;
            if (llDirectLink.Tag != null) link_editor = new frmWizard_DirectLink(llDirectLink.Tag as Link);
            else
            {
                List<string> PK = new List<string>();
                foreach (string Column in lbColumns.SelectedItems)
                    PK.Add(Column);
                Table Table = Template.CreateTable(lbTables.Text, Type, PK);
                //Template.AddTable(Table);
                Table Parent = Template.Orders;
                Table Child = Template.Orders;
                switch (Type.ToString())
                {
                    case TableType.Senders:
                    case TableType.Recipients:
                        Parent = Table;
                        break;
                    case TableType.Packages:
                    case TableType.Products:
                    case TableType.Shipments:
                        Child = Table;
                        break;
                }
                link_editor = new frmWizard_DirectLink(Parent, Child);
            }
            DialogResult result = link_editor.ShowDialog(this);
            if (result == DialogResult.OK) llDirectLink.Tag = link_editor.Link;
            else if (result == DialogResult.No) llDirectLink.Tag = null; // link removed

            llIndirectLink.Visible = llDirectLink.Tag == null;
        }

        private void llIndirectLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (lbTables.SelectedItem == null || lbTables.SelectedItem.ToString().Trim().Equals("")) return;
            new frmWizard_DataPreview(lbTables.SelectedItem.ToString(), Template.ReadConn.ConnStr, "Select Top 100 * From [" + lbTables.SelectedItem.ToString() + "] Order By 1").ShowDialog(this);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (lbTables.SelectedIndex == -1 || lbColumns.SelectedItems.Count == 0)
            {
                MessageBox.Show(this, "Please select the " + Type.ToString() + " table and the PK column(s).", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (llDirectLink.Visible && llDirectLink.Tag == null || llIndirectLink.Visible && llIndirectLink.Tag == null)
            {
                MessageBox.Show(this, "Please setup the table link.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            List<string> PK = new List<string>();
            foreach (string Column in lbColumns.SelectedItems)
                PK.Add(Column);
            Template.AddTable(Template.CreateTable(lbTables.SelectedItem.ToString(), Type, PK));
            if (llDirectLink.Tag != null) Template.AddLink(llDirectLink.Tag as Link);
            //else if (llIndirectLink.Tag != null) Template.AddIndirectLink(llIndirectLink.Tag as IndirectLink);
            
            DialogResult = DialogResult.OK;
        }

        private void frmWizard02_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK) return;
            if (DialogResult == DialogResult.No)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            DialogResult = DialogResult.Abort;
        }

        private void frmWizard02_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_" + Type.Type);
            e.Cancel = true;
        }

        private void chkShow_CheckedChanged(object sender, EventArgs e)
        {
            LoadTables();
        }
    }
}
