﻿namespace InteGr8
{
    partial class UPSReturnServiceType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UPSReturnServiceType));
            this.cmbUPSReturnService = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFromName = new System.Windows.Forms.TextBox();
            this.lblFromName = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.gbEmailDetails = new System.Windows.Forms.GroupBox();
            this.tbEmailSubject = new System.Windows.Forms.TextBox();
            this.lblEmailSubject = new System.Windows.Forms.Label();
            this.data = new InteGr8.Data();
            this.uPSReturnServiceTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbEmailDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.data)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uPSReturnServiceTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbUPSReturnService
            // 
            this.cmbUPSReturnService.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbUPSReturnService.DataSource = this.uPSReturnServiceTypeBindingSource;
            this.cmbUPSReturnService.DisplayMember = "Name";
            this.cmbUPSReturnService.FormattingEnabled = true;
            this.cmbUPSReturnService.Location = new System.Drawing.Point(79, 44);
            this.cmbUPSReturnService.Name = "cmbUPSReturnService";
            this.cmbUPSReturnService.Size = new System.Drawing.Size(232, 21);
            this.cmbUPSReturnService.TabIndex = 0;
            this.cmbUPSReturnService.ValueMember = "Key";
            this.cmbUPSReturnService.SelectedIndexChanged += new System.EventHandler(this.cmbUPSReturnService_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "UPS Return Service Type";
            // 
            // tbFromName
            // 
            this.tbFromName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFromName.Enabled = false;
            this.tbFromName.Location = new System.Drawing.Point(150, 19);
            this.tbFromName.Name = "tbFromName";
            this.tbFromName.Size = new System.Drawing.Size(100, 20);
            this.tbFromName.TabIndex = 2;
            // 
            // lblFromName
            // 
            this.lblFromName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFromName.AutoSize = true;
            this.lblFromName.Location = new System.Drawing.Point(17, 26);
            this.lblFromName.Name = "lblFromName";
            this.lblFromName.Size = new System.Drawing.Size(86, 13);
            this.lblFromName.TabIndex = 3;
            this.lblFromName.Text = "UPS From Name";
            // 
            // lblEmail
            // 
            this.lblEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(17, 69);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(98, 13);
            this.lblEmail.TabIndex = 4;
            this.lblEmail.Text = "UPS Email Address";
            // 
            // tbEmail
            // 
            this.tbEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmail.Enabled = false;
            this.tbEmail.Location = new System.Drawing.Point(150, 62);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(100, 20);
            this.tbEmail.TabIndex = 5;
            // 
            // gbEmailDetails
            // 
            this.gbEmailDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbEmailDetails.Controls.Add(this.tbEmailSubject);
            this.gbEmailDetails.Controls.Add(this.lblEmailSubject);
            this.gbEmailDetails.Controls.Add(this.tbEmail);
            this.gbEmailDetails.Controls.Add(this.lblEmail);
            this.gbEmailDetails.Controls.Add(this.lblFromName);
            this.gbEmailDetails.Controls.Add(this.tbFromName);
            this.gbEmailDetails.Enabled = false;
            this.gbEmailDetails.Location = new System.Drawing.Point(31, 93);
            this.gbEmailDetails.Name = "gbEmailDetails";
            this.gbEmailDetails.Size = new System.Drawing.Size(298, 137);
            this.gbEmailDetails.TabIndex = 7;
            this.gbEmailDetails.TabStop = false;
            this.gbEmailDetails.Text = "UPS Electronic Return Label Details";
            // 
            // tbEmailSubject
            // 
            this.tbEmailSubject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmailSubject.Enabled = false;
            this.tbEmailSubject.Location = new System.Drawing.Point(150, 99);
            this.tbEmailSubject.Name = "tbEmailSubject";
            this.tbEmailSubject.Size = new System.Drawing.Size(100, 20);
            this.tbEmailSubject.TabIndex = 7;
            // 
            // lblEmailSubject
            // 
            this.lblEmailSubject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEmailSubject.AutoSize = true;
            this.lblEmailSubject.Location = new System.Drawing.Point(17, 106);
            this.lblEmailSubject.Name = "lblEmailSubject";
            this.lblEmailSubject.Size = new System.Drawing.Size(96, 13);
            this.lblEmailSubject.TabIndex = 6;
            this.lblEmailSubject.Text = "UPS Email Subject";
            this.lblEmailSubject.Click += new System.EventHandler(this.lblEmailSubject_Click);
            // 
            // data
            // 
            this.data.DataSetName = "Data";
            this.data.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uPSReturnServiceTypeBindingSource
            // 
            this.uPSReturnServiceTypeBindingSource.DataMember = "UPSReturnServiceType";
            this.uPSReturnServiceTypeBindingSource.DataSource = this.data;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(130, 236);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(254, 236);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // UPSReturnServiceType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 271);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gbEmailDetails);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbUPSReturnService);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UPSReturnServiceType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "UPSReturnServiceType";
            this.gbEmailDetails.ResumeLayout(false);
            this.gbEmailDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.data)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uPSReturnServiceTypeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbUPSReturnService;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFromName;
        private System.Windows.Forms.Label lblFromName;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.GroupBox gbEmailDetails;
        private System.Windows.Forms.Label lblEmailSubject;
        private System.Windows.Forms.TextBox tbEmailSubject;
        private System.Windows.Forms.BindingSource uPSReturnServiceTypeBindingSource;
        private Data data;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}