﻿using System;
using System.Data;
using System.Configuration;
using System.Text;

namespace InteGr8
{
    // encryption / decryption with a key, implemented in a custom written alorythm
    public static class Encryption
    {
        private static readonly double factor = Int32.MaxValue / Double.MaxValue;

        public static string Encrypt(string key, string text)
        {
            return Stretch(DoXOR(key, text));
        }

        public static string Decrypt(string key, string text)
        {
            return DoXOR(key, Shrink(text));
        }

        private static int Seed(double value)
        {
            return Convert.ToInt32(value * factor);
        }

        private static string DoXOR(string key, string text)
        {
            Random rand = new Random(Seed(new Random(-1).NextDouble()));
            for (int nI = 0; nI < key.Length; nI++)
                rand = new Random(Seed(new Random(Seed(-rand.NextDouble() * Convert.ToByte(key[nI]))).NextDouble()));

            StringBuilder str = new StringBuilder(text);
            for (int k = 0; k < str.Length; k++)
                str[k] = Convert.ToChar(Convert.ToByte(str[k]) ^ Convert.ToInt32(rand.NextDouble() * 256));
            return str.ToString();
        }

        private static string Stretch(string text)
        {
            int nC;
            int nK = 0;
            int lA = text.Length;
            StringBuilder str = new StringBuilder(lA + (lA + 2) / 3);
            for (int lI = 0; lI < lA; lI++)
            {
                nC = Convert.ToByte(text[lI]);
                str.Append(Convert.ToChar((nC & 63) + 59));
                switch ((lI + 1) % 3)
                {
                    case 1: nK = nK | ((nC / 64) * 16); break;
                    case 2: nK = nK | ((nC / 64) * 4); break;
                    case 0:
                        nK = nK | (nC / 64);
                        str.Append(Convert.ToChar(nK + 59));
                        nK = 0;
                        break;
                }
            }
            if (lA % 3 > 0) str.Append(Convert.ToChar(nK + 59));
            return str.ToString();
        }

        private static string Shrink(string text)
        {
            int nC;
            int nD = 0;
            int nE = 0;
            int lJ = 0;
            int lK = 0;

            int lA = text.Length;
            int lB = lA - 1 - (lA - 1) / 4;
            if (lB < 0) lB = 0;
            StringBuilder str = new StringBuilder(lB);
            for (int lI = 0; lI < lB; lI++)
            {
                nC = Convert.ToByte(text[lJ++]) - 59;
                switch ((lI + 1) % 3)
                {
                    case 1:
                        lK += 4;
                        if (lK > lA) lK = lA;
                        nE = Convert.ToByte(text[lK - 1]) - 59;
                        nD = ((nE / 16) & 3) * 64;
                        break;
                    case 2: nD = ((nE / 4) & 3) * 64; break;
                    case 0:
                        nD = (nE & 3) * 64;
                        lJ++;
                        break;
                }
                str.Append(Convert.ToChar(nC | nD));
            }
            return str.ToString();
        }
    }
}