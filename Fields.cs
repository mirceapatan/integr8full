﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using InteGr8.WS2Ship;

namespace InteGr8
{
    // class to compare two Field instances
    //public class FieldsComparer : IComparer<Field>
    //{
    //    public int Compare(Field x, Field y)
    //    {
    //        if (x.Code == y.Code)
    //        {
    //            if (x.Index == y.Index) return 0;
    //            return x.Index < y.Index ? -1 : 1;
    //        }
    //        return x.Code < y.Code ? -1 : 1;
    //    }
    //}

    // customized fields list with sorted insert and unique constraint, plus a lot of indexers
    public class Fields : IEnumerable
    {
        private static readonly Data.FieldsTableDataTable field_definitions = new Data.FieldsTableDataTable();
        
        static Fields()
        {
            field_definitions.Merge(InteGr8_Main.data.FieldsTable);
            if (field_definitions.Count == 0) throw new Exception("Cannot read fields from xml file.");
        }

        private static int GetFieldId(string Name)
        {
            Data.FieldsTableRow field = field_definitions.FindByName(Name);
            return field == null ? -1 : field.Id;
        }

        private readonly List<Field> fields;

        public Fields()
        {
            fields = new List<Field>();
        }

        // the list is initialized with the instance references from the given array
        public Fields(Field[] fields)
        {
            if (fields == null) throw new ArgumentException("fields is null");
            this.fields = new List<Field>(fields);
            //this.fields.Sort(new FieldsComparer());
        }

        public int Count
        {
            get { return fields.Count; }
        }

        public System.Collections.IEnumerator GetEnumerator()
        {
            return fields.GetEnumerator();
        }

        //public void Add(Field field)
        //{
        //    int index = fields.BinarySearch(field, new FieldsComparer());
        //    if (index >= 0) throw new Exception("Cannot add field [" + field.Code + ", " + field.Index + ", '" + field.Value + "'] because this field already exists with value '" + fields[index].Value + "'");
        //    fields.Insert(~index, field);
        //}

        //// will not allow duplicate fields (based on code and index)
        //public Field Add(int Code, int Index, string Value)
        //{
        //    Field field = new Field();
        //    field.Code = Code;
        //    field.Index = Index;
        //    field.Value = Value;
        //    Add(field);
        //    return field;
        //}

        public bool Remove(Field field)
        {
            return fields.Remove(field);
        }

        //public bool Remove(int Code, int Index)
        //{
        //    Field f = this[Code, Index, "", false];
        //    if (f == null) return false; 
        //    return fields.Remove(f);
        //}

        public Field[] ToArray()
        {
            return fields.ToArray();
        }

        // returns all the Field references with the given code
        //public Fields GetFields(int Code)
        //{
        //    Fields ret = new Fields();
        //    foreach (Field field in fields)
        //        if (field.Code == Code) ret.fields.Add(field);
        //    return ret;
        //}

        //public Fields GetFields(string Name)
        //{
        //    Fields ret = new Fields();
        //    int Code = GetFieldId(Name);
        //    foreach (Field field in fields)
        //        if (field.Code == Code) ret.fields.Add(field);
        //    return ret;
        //}

        // returns all the Field references with the given field index
        //public Fields FieldsIndex(int Index)
        //{
        //    Fields ret = new Fields();
        //    foreach (Field field in fields)
        //        if (field.Index == Index) ret.fields.Add(field);
        //    return ret;
        //}

        #region indexers

        // indexers that return / set the field value

        //public string this[int Code]
        //{
        //    get { return this[Code, 0, null]; }
        //    set { this[Code, 0] = value; }
        //}

        //public string this[string Name]
        //{
        //    get { return this[Name, 0, null]; }
        //    set { this[Name, 0] = value; }
        //}

        //public string this[int Code, int Index]
        //{
        //    get { return this[Code, Index, null]; }
        //    set { Field field = this[Code, Index, value == null ? "" : value, true]; }
        //}

        //public string this[string Name, int Index]
        //{
        //    get { return this[Name, Index, null]; }
        //    set { Field field = this[Name, Index, value == null ? "" : value, true]; }
        //}

        //public string this[int Code, string Default]
        //{
        //    get { return this[Code, 0, Default]; }
        //}

        //public string this[string Name, string Default]
        //{
        //    get { return this[Name, 0, Default]; }
        //}

       // public string this[int Code, int Index, string Default]
        //{
        //    get
        //    {
        //        Field field = this[Code, Index, null, false];
        //        if (field == null) return Default;
        //        return field.Value;
        //    }
        //}

        //public string this[string Name, int Index, string Default]
        //{
        //    get
        //    {
        //        Field field = this[Name, Index, null, false];
        //        if (field == null) return Default;
        //        return field.Value;
        //    }
        //}

        // indexer that return the field instance

        //public Field this[int Code, int Index, string Default, bool Add]
        //{
        //    get
        //    {
        //        Field f = new Field();
        //        f.Code = Code;
        //        f.Index = Index;
        //        f.Value = Default;
        //        int index = fields.BinarySearch(f, new FieldsComparer());
        //        if (index >= 0)
        //        {
        //            f = fields[index];
        //            if (Add) f.Value = Default;
        //            return f;
        //        }
        //        if (Add)
        //        {
        //            fields.Insert(~index, f);
        //            return f;
        //        }
        //        return null;
        //    }
        //}

        //public Field this[string Name, int Index, string Default, bool Add]
        //{
        //    get
        //    {
        //        int id = GetFieldId(Name);
        //        if (id == -1) return null;
        //        Field f = new Field();
        //        f.Code = id;
        //        if (f.Code == -1) return null;
        //        f.Index = Index;
        //        f.Value = Default;
        //        int index = fields.BinarySearch(f, new FieldsComparer());
        //        if (index >= 0)
        //        {
        //            f = fields[index];
        //            if (Add) f.Value = Default;
        //            return f;
        //        }
        //        if (Add)
        //        {
        //            fields.Insert(~index, f);
        //            return f;
        //        }
        //        return null;
        //    }
        //}

        // indexers that return a boolean value: CheckBoolean - if set, will return true if the field's value == '1', 
        // otherwise will return true if the field exists in the list

        //public bool this[int Code, bool CheckBoolean]
        //{
        //    get { return this[Code, 0, CheckBoolean]; }
        //}

        //public bool this[string Name, bool CheckBoolean]
        //{
        //    get { return this[Name, 0, CheckBoolean]; }
        //}

        //public bool this[int Code, int Index, bool CheckBoolean]
        //{
        //    get
        //    {
        //        foreach (Field field in fields)
        //            if (field.Code == Code && field.Index == Index)
        //            {
        //                if (CheckBoolean) field.Value.Equals("1");
        //                return true;
        //            }
        //        return false;
        //    }
        //}

        //public bool this[string Name, int Index, bool CheckBoolean]
        //{
        //    get
        //    {
        //        int Code = GetFieldId(Name);
        //        if (Code == -1) return false;
        //        foreach (Field field in fields)
        //            if (field.Code == Code && field.Index == Index)
        //            {
        //                if (CheckBoolean) field.Value.Equals("1");
        //                return true;
        //            }
        //        return false;
        //    }
        //}

        //public decimal this[int Code, decimal Default]
        //{
        //    get { return this[Code, 0, Default]; }
        //}

        //public decimal this[string Name, decimal Default]
        //{
        //    get { return this[Name, 0, Default]; }
        //}

        //public decimal this[int Code, int Index, decimal Default]
        //{
        //    get
        //    {
        //        string val = this[Code, Index];
        //        if (val == null) return Default;
        //        return Convert.ToDecimal(val);
        //    }
        //}

        //public decimal this[string Name, int Index, decimal Default]
        //{
        //    get
        //    {
        //        string val = this[Name, Index];
        //        if (val == null) return Default;
        //        return Convert.ToDecimal(val);
        //    }
        //}

        #endregion

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            foreach (Field f in fields)
                str.Append(f).Append("\n");
            return str.ToString();
        }
    }
}
