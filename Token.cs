﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InteGr8.WS2Ship;
using InteGr8.WSTwoShip;
using InteGr8.Properties;

namespace InteGr8
{
    public class WSKey
    {
        private readonly int Client;
        private readonly string Login;
        private readonly string Password;
        private string WSKeyString;
        private List<LocalLocation> Locations = new List<LocalLocation>();

        public WSKey()
        {
            this.Client = 0;
            this.Login = "";
            this.Password = "";
            this.WSKeyString = "";
        }

        public WSKey(int Client, string Login, string Password)
        {
            this.Client = Client;
            this.Login = Login;
            this.Password = Password;

            try
            {
                InteGr8.WSTwoShip.I2ShipServiceClient ws = new I2ShipServiceClient(); //TwoShipService();
                //ws.Url = ConfigurationManager.AppSettings["WS2Ship"];
                WSKeyString = ws.Authenticate(Login, Password, Client);//, true);// new TwoShipService().Authenticate(Login, Password, Client, true);
                if (String.IsNullOrEmpty(WSKeyString)) throw new Exception("Emtpy result received from login webservice.");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to contact 2Ship webservice.", e);
            }
            if (WSKeyString.StartsWith("#1")) throw new Exception("For user " + Login + " web service returned #1: Incorrect username and password.");
            if (WSKeyString.StartsWith("#2")) throw new Exception("For user " + Login + " web service returned #2: Your 2Ship user has no default location defined. Please login to 2Ship.com and setup a default sender.");

            try
            {
                //string[] locations;
                LocationReply lr = new I2ShipServiceClient().GetAllLocations(WSKeyString);
                //public Location(int Id, string Name, string Country, string State, string City, string Zip, bool Default, string Token)
                foreach (Location l in lr.Locations)
                {
                    Locations.Add(new LocalLocation(l.Id, l.Name, l.Address.Country, l.Address.State, l.Address.City, l.Address.PostalCode, l.Address));
                }
            }
            catch (Exception e)
            {
                throw new Exception("Unable to read the 2Ship locations. Please contact 2Ship admin.", e);
            }

        }

        public string MyWSKey { get { return WSKeyString; } set { WSKeyString = value; } }
        //public List<LocalLocation> MyLocations { get { return Locations; } }

        public int getLocationID(string Country, string State, string City, string Zip)
        {
            int locationID = 0;
            foreach (LocalLocation loc in Locations)
            {
                if (loc.Address.Country.Equals(Country) && loc.Address.State.Equals(State) && loc.Address.City.ToUpper().Equals(City.ToUpper()) && loc.Address.PostalCode.Equals(Zip.Replace(" ", "")))
                {
                    locationID = loc.Id;
                    return locationID;
                }
            }
            return locationID;
        }
        private class LocalLocation
        {
            public readonly int Id;
            public readonly string Name;
            public readonly LocationAddress Address;

            public LocalLocation(int Id, string Name, string Country, string State, string City, string Zip, LocationAddress address)
            {
                this.Id = Id;
                this.Name = Name;
                //this.Address.Country = Country;
                //this.Address.State = State;
                //this.Address.City = City;
                //this.Address.PostalCode = Zip;
                this.Address = address;
            }
        }

    }
    //class Token
    //{
    //    public readonly int                 Client;
    //    public readonly string              Login;
    //    public readonly string              Password;

    //    private readonly List<Location>     Locations = new List<Location>();
    //    //new constructor here, based on token
    //    public Token(string MasterToken)
    //    {
    //        string[] decryptedToken = Encryption.Decrypt(Settings.Default.Key, MasterToken).Split('|');

    //        //new Token(Convert.ToInt32(decryptedToken[0]), decryptedToken[3], decryptedToken[4]);

    //        //decryptedToken[0] - clientId, decryptedToken[1] - ?, decryptedToken[2] - /, decryptedToken[3] - username, decryptedToken[4] - password
    //        int Client = Convert.ToInt32(decryptedToken[0]);
    //        string Login = decryptedToken[3];
    //        string password = decryptedToken[4];
    //        this.Client = Client;
    //        this.Login = Login;
    //        this.Password = password;

    //        string TokenString;
    //        try
    //        {
    //            TokenString = new ShipService().UserLogin(Client, Login, Password);
    //            if (String.IsNullOrEmpty(TokenString)) throw new Exception("Emtpy result received from login webservice.");
    //        }
    //        catch (Exception e)
    //        {
    //            Utils.SendError("Error at 2Ship login webservice.", e);
    //            throw new Exception("Unable to contact 2Ship webservice.");
    //        }
    //        if (TokenString.StartsWith("#1")) throw new Exception("For user " + Login + " web service returned #1: Incorrect username and password.");
    //        if (TokenString.StartsWith("#2")) throw new Exception("For user " + Login + " web service returned #2: Your 2Ship user has no default location defined. Please login to 2Ship.com and setup a default sender.");

    //        try
    //        {
    //            if (TokenString.Length % 3 != 0) throw new Exception("Incorrect length in decrypted token.");
    //            char[] ascii = new char[3];
    //            int pos = 0;
    //            StringBuilder token = new StringBuilder(TokenString.Length / 3);
    //            foreach (char c in TokenString)
    //            {
    //                if (!Char.IsDigit(c)) throw new Exception("Incorrect character in decrypted token.");
    //                ascii[pos++] = c;
    //                if (pos == 3)
    //                {
    //                    token.Append(Convert.ToChar(Convert.ToByte(new string(ascii))));
    //                    pos = 0;
    //                }
    //            }
    //            string[] fields = Encryption.Decrypt(Settings.Default.Key, token.ToString()).Split('|');
    //            if (fields.Length != 5) throw new Exception("Incorrect data length in decrypted token.");
    //        }
    //        catch (Exception e)
    //        {
    //            Utils.SendError("Error parsing token from 2Ship login webservice.", e);
    //            throw new Exception("Unable to parse 2Ship webservice token.");
    //        }

    //        try
    //        {
    //            string[] locations = new ShipService().GetUserLocations(Client, Login, Password);
    //            if (locations == null || locations.Length == 0) throw new Exception("No locations defined.");
    //            foreach (string line in locations)
    //            {
    //                string[] cols = line.Split('\n');
    //                if (cols.Length != 8) throw new Exception("Invalid locations data format received from 2Ship.");
    //                Locations.Add(new Location(Convert.ToInt32(cols[0]), cols[1], cols[2], cols[3], cols[4], cols[5], "1".Equals(cols[6]), cols[7]));
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            Utils.SendError("Error at 2Ship get user locations webservice for client " + Client + " with user login '" + Login + "'", e);
    //            throw new Exception("Unable to read the 2Ship locations. Please contact 2Ship admin.");
    //        }
    //    }


    //    public Token(int Client, string Login, string Password)
    //    {
    //        this.Client = Client;
    //        this.Login = Login;
    //        this.Password = Password;
    //        if (String.IsNullOrEmpty(Settings.Default.LoginToken))
    //        {
    //            string TokenString;
    //            try
    //            {
    //                TokenString = new ShipService().UserLogin(Client, Login, Password);
    //                if (String.IsNullOrEmpty(TokenString)) throw new Exception("Emtpy result received from login webservice.");
    //            }
    //            catch (Exception e)
    //            {
    //                Utils.SendError("Error at 2Ship login webservice.", e);
    //                throw new Exception("Unable to contact 2Ship webservice.");
    //            }
    //            if (TokenString.StartsWith("#1")) throw new Exception("For user " + Login + " web service returned #1: Incorrect username and password.");
    //            if (TokenString.StartsWith("#2")) throw new Exception("For user " + Login + " web service returned #2: Your 2Ship user has no default location defined. Please login to 2Ship.com and setup a default sender.");
    //            //		fields[4]	"1000:WydYIRbMLLF8KdKzHYo9fs49ndWytbmz:gzpmvWlAŌeMu"	string

    //            try
    //            {
    //                if (TokenString.Length % 3 != 0) throw new Exception("Incorrect length in decrypted token.");
    //                char[] ascii = new char[3];
    //                int pos = 0;
    //                StringBuilder token = new StringBuilder(TokenString.Length / 3);
    //                foreach (char c in TokenString)
    //                {
    //                    if (!Char.IsDigit(c)) throw new Exception("Incorrect character in decrypted token.");
    //                    ascii[pos++] = c;
    //                    if (pos == 3)
    //                    {
    //                        token.Append(Convert.ToChar(Convert.ToByte(new string(ascii))));
    //                        pos = 0;
    //                    }
    //                }
    //                string[] fields = Encryption.Decrypt(Settings.Default.Key, token.ToString()).Split('|');
    //                if (fields.Length != 5) throw new Exception("Incorrect data length in decrypted token.");
    //            }
    //            catch (Exception e)
    //            {
    //                Utils.SendError("Error parsing token from 2Ship login webservice.", e);
    //                throw new Exception("Unable to parse 2Ship webservice token.");
    //            }
    //        }
    //        try
    //        {
    //            string[] locations = new ShipService().GetUserLocations(Client, Login, Password);
    //            if (locations == null || locations.Length == 0) throw new Exception("No locations defined.");
    //            foreach (string line in locations)
    //            {
    //                string[] cols = line.Split('\n');
    //                if (cols.Length != 8) throw new Exception("Invalid locations data format received from 2Ship.");
    //                Locations.Add(new Location(Convert.ToInt32(cols[0]), cols[1], cols[2], cols[3], cols[4], cols[5], "1".Equals(cols[6]), cols[7]));
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            Utils.SendError("Error at 2Ship get user locations webservice for client " + Client + " with user login '" + Login + "'", e);
    //            throw new Exception("Unable to read the 2Ship locations. Please contact 2Ship admin.");
    //        }
    //    }

    //    public string GetToken()
    //    {
    //        return Locations[0].Token;
    //    }

    //    public string GetToken(string LocationName)
    //    {
    //        foreach (Location loc in Locations)
    //            if (loc.Name.Equals(LocationName)) return loc.Token;

    //        Utils.SendError("Invalid location name '" + LocationName + "' for client " + Client + " with login '" + Login + "'", null);
    //        throw new Exception("Internal error.");
    //    }

    //    public string GetToken(string Country, string State, string City, string Zip)
    //    {
    //        foreach (Location loc in Locations)
    //            if (loc.Country.Equals(Country, StringComparison.InvariantCultureIgnoreCase) &&
    //                loc.State.Equals(State, StringComparison.InvariantCultureIgnoreCase) &&
    //                loc.City.Equals(City, StringComparison.InvariantCultureIgnoreCase) &&
    //                loc.Zip.Equals(Zip, StringComparison.InvariantCultureIgnoreCase))
    //                return loc.Token; // exact sender match

    //        // no exact match, return the default loc for the sender country
    //        foreach (Location loc in Locations)
    //            if (loc.Default && loc.Country.Equals(Country, StringComparison.InvariantCultureIgnoreCase))
    //                return loc.Token;

    //        // no default for the given country
    //        if (Country.Equals("CA", StringComparison.InvariantCultureIgnoreCase))
    //        {
    //            // return the first CA location
    //            foreach (Location loc in Locations)
    //                if (loc.Country.Equals(Country, StringComparison.InvariantCultureIgnoreCase))
    //                    return loc.Token;
    //        }
    //        else
    //        {
    //            // return the default intl loc
    //            foreach (Location loc in Locations)
    //                if (loc.Default && !loc.Country.Equals("CA", StringComparison.InvariantCultureIgnoreCase))
    //                    return loc.Token;
    //        }

    //        // no default at all, return any location
    //        return Locations[0].Token;
    //    }

    //    public string[] GetLocations()
    //    {
    //        string[] names = new string[Locations.Count];
    //        for (int k = 0; k < Locations.Count; k++)
    //            names[k] = Locations[k].Name;
    //        return names;
    //    }

    //    private class Location
    //    {
    //        public readonly int     Id;
    //        public readonly string  Name;
    //        public readonly string  Country;
    //        public readonly string  State;
    //        public readonly string  City;
    //        public readonly string  Zip;
    //        public readonly bool    Default;
    //        public readonly string  Token;

    //        public Location(int Id, string Name, string Country, string State, string City, string Zip, bool Default, string Token)
    //        {
    //            this.Id = Id;
    //            this.Name = Name;
    //            this.Country = Country;
    //            this.State = State;
    //            this.City = City;
    //            this.Zip = Zip;
    //            this.Default = Default;
    //            this.Token = Token;
    //        }
    //    }

    //}
}
