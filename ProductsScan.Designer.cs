﻿namespace InteGr8
{
    partial class ProductsScan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductsScan));
            this.txtScanCode = new System.Windows.Forms.TextBox();
            this.labelScanCode = new System.Windows.Forms.Label();
            this.btnNewPackage = new System.Windows.Forms.Button();
            this.btn_finish = new System.Windows.Forms.Button();
            this.labelSerialNum = new System.Windows.Forms.Label();
            this.txtSerialNum = new System.Windows.Forms.TextBox();
            this.labelWarningMessage = new System.Windows.Forms.Label();
            this.dgw_serial = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_prev_pakage = new System.Windows.Forms.Button();
            this.btn_next_pakage = new System.Windows.Forms.Button();
            this.lb_carton_nr = new System.Windows.Forms.Label();
            this.dataGrid_products = new System.Windows.Forms.DataGridView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_bypass = new System.Windows.Forms.Button();
            this.groupBox_bypass = new System.Windows.Forms.GroupBox();
            this.radioButton_bypassone_item = new System.Windows.Forms.RadioButton();
            this.radioButton_bypassfull_row = new System.Windows.Forms.RadioButton();
            this.label_full_row = new System.Windows.Forms.Label();
            this.label_one_item = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgw_serial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_products)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox_bypass.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtScanCode
            // 
            this.txtScanCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtScanCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtScanCode.Location = new System.Drawing.Point(31, 36);
            this.txtScanCode.Name = "txtScanCode";
            this.txtScanCode.Size = new System.Drawing.Size(598, 20);
            this.txtScanCode.TabIndex = 0;
            this.txtScanCode.TextChanged += new System.EventHandler(this.txtScanCode_TextChanged);
            this.txtScanCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtScanCode_KeyPress);
            // 
            // labelScanCode
            // 
            this.labelScanCode.AutoSize = true;
            this.labelScanCode.Location = new System.Drawing.Point(28, 9);
            this.labelScanCode.Name = "labelScanCode";
            this.labelScanCode.Size = new System.Drawing.Size(53, 13);
            this.labelScanCode.TabIndex = 1;
            this.labelScanCode.Text = "Item scan";
            // 
            // btnNewPackage
            // 
            this.btnNewPackage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNewPackage.Location = new System.Drawing.Point(98, 575);
            this.btnNewPackage.Name = "btnNewPackage";
            this.btnNewPackage.Size = new System.Drawing.Size(114, 23);
            this.btnNewPackage.TabIndex = 2;
            this.btnNewPackage.Text = "Add Package";
            this.btnNewPackage.UseVisualStyleBackColor = true;
            this.btnNewPackage.Click += new System.EventHandler(this.btnNewPackage_Click);
            // 
            // btn_finish
            // 
            this.btn_finish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_finish.Location = new System.Drawing.Point(532, 575);
            this.btn_finish.Name = "btn_finish";
            this.btn_finish.Size = new System.Drawing.Size(96, 23);
            this.btn_finish.TabIndex = 3;
            this.btn_finish.Text = "Finish";
            this.btn_finish.UseVisualStyleBackColor = true;
            this.btn_finish.Click += new System.EventHandler(this.button2_Click);
            // 
            // labelSerialNum
            // 
            this.labelSerialNum.AutoSize = true;
            this.labelSerialNum.Enabled = false;
            this.labelSerialNum.Location = new System.Drawing.Point(28, 69);
            this.labelSerialNum.Name = "labelSerialNum";
            this.labelSerialNum.Size = new System.Drawing.Size(71, 13);
            this.labelSerialNum.TabIndex = 5;
            this.labelSerialNum.Text = "Serial number";
            // 
            // txtSerialNum
            // 
            this.txtSerialNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSerialNum.Location = new System.Drawing.Point(31, 89);
            this.txtSerialNum.Name = "txtSerialNum";
            this.txtSerialNum.Size = new System.Drawing.Size(598, 20);
            this.txtSerialNum.TabIndex = 6;
            this.txtSerialNum.TextChanged += new System.EventHandler(this.txtSerialNum_TextChanged);
            this.txtSerialNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSerialNum_KeyPress);
            // 
            // labelWarningMessage
            // 
            this.labelWarningMessage.AutoSize = true;
            this.labelWarningMessage.Location = new System.Drawing.Point(133, 9);
            this.labelWarningMessage.Name = "labelWarningMessage";
            this.labelWarningMessage.Size = new System.Drawing.Size(0, 13);
            this.labelWarningMessage.TabIndex = 7;
            // 
            // dgw_serial
            // 
            this.dgw_serial.AllowUserToAddRows = false;
            this.dgw_serial.AllowUserToDeleteRows = false;
            this.dgw_serial.AllowUserToOrderColumns = true;
            this.dgw_serial.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgw_serial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgw_serial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgw_serial.Location = new System.Drawing.Point(0, 0);
            this.dgw_serial.MultiSelect = false;
            this.dgw_serial.Name = "dgw_serial";
            this.dgw_serial.ReadOnly = true;
            this.dgw_serial.RowHeadersVisible = false;
            this.dgw_serial.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgw_serial.Size = new System.Drawing.Size(597, 195);
            this.dgw_serial.TabIndex = 8;
            this.dgw_serial.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgw_serial_MouseDoubleClick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 538);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Package";
            // 
            // btn_prev_pakage
            // 
            this.btn_prev_pakage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_prev_pakage.Location = new System.Drawing.Point(98, 517);
            this.btn_prev_pakage.Name = "btn_prev_pakage";
            this.btn_prev_pakage.Size = new System.Drawing.Size(114, 23);
            this.btn_prev_pakage.TabIndex = 11;
            this.btn_prev_pakage.Text = "< Previous Package";
            this.btn_prev_pakage.UseVisualStyleBackColor = true;
            this.btn_prev_pakage.Click += new System.EventHandler(this.btn_prev_pakage_Click);
            // 
            // btn_next_pakage
            // 
            this.btn_next_pakage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_next_pakage.Location = new System.Drawing.Point(98, 546);
            this.btn_next_pakage.Name = "btn_next_pakage";
            this.btn_next_pakage.Size = new System.Drawing.Size(114, 23);
            this.btn_next_pakage.TabIndex = 12;
            this.btn_next_pakage.Text = "Next Package >";
            this.btn_next_pakage.UseVisualStyleBackColor = true;
            this.btn_next_pakage.Click += new System.EventHandler(this.btn_next_pakage_Click);
            // 
            // lb_carton_nr
            // 
            this.lb_carton_nr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_carton_nr.AutoSize = true;
            this.lb_carton_nr.Location = new System.Drawing.Point(56, 561);
            this.lb_carton_nr.Name = "lb_carton_nr";
            this.lb_carton_nr.Size = new System.Drawing.Size(0, 13);
            this.lb_carton_nr.TabIndex = 13;
            // 
            // dataGrid_products
            // 
            this.dataGrid_products.AllowUserToAddRows = false;
            this.dataGrid_products.AllowUserToDeleteRows = false;
            this.dataGrid_products.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid_products.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid_products.Location = new System.Drawing.Point(0, 0);
            this.dataGrid_products.MultiSelect = false;
            this.dataGrid_products.Name = "dataGrid_products";
            this.dataGrid_products.ReadOnly = true;
            this.dataGrid_products.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGrid_products.Size = new System.Drawing.Size(597, 192);
            this.dataGrid_products.TabIndex = 14;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(31, 120);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgw_serial);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGrid_products);
            this.splitContainer1.Size = new System.Drawing.Size(597, 391);
            this.splitContainer1.SplitterDistance = 195;
            this.splitContainer1.TabIndex = 15;
            // 
            // btn_bypass
            // 
            this.btn_bypass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_bypass.Location = new System.Drawing.Point(532, 546);
            this.btn_bypass.Name = "btn_bypass";
            this.btn_bypass.Size = new System.Drawing.Size(96, 23);
            this.btn_bypass.TabIndex = 16;
            this.btn_bypass.Text = "Bypass";
            this.btn_bypass.UseVisualStyleBackColor = true;
            this.btn_bypass.Click += new System.EventHandler(this.btn_bypass_Click);
            // 
            // groupBox_bypass
            // 
            this.groupBox_bypass.Controls.Add(this.radioButton_bypassone_item);
            this.groupBox_bypass.Controls.Add(this.radioButton_bypassfull_row);
            this.groupBox_bypass.Location = new System.Drawing.Point(432, 533);
            this.groupBox_bypass.Name = "groupBox_bypass";
            this.groupBox_bypass.Size = new System.Drawing.Size(85, 65);
            this.groupBox_bypass.TabIndex = 17;
            this.groupBox_bypass.TabStop = false;
            this.groupBox_bypass.Text = "Bypass";
            // 
            // radioButton_bypassone_item
            // 
            this.radioButton_bypassone_item.AutoSize = true;
            this.radioButton_bypassone_item.Checked = true;
            this.radioButton_bypassone_item.Location = new System.Drawing.Point(6, 38);
            this.radioButton_bypassone_item.Name = "radioButton_bypassone_item";
            this.radioButton_bypassone_item.Size = new System.Drawing.Size(68, 17);
            this.radioButton_bypassone_item.TabIndex = 1;
            this.radioButton_bypassone_item.TabStop = true;
            this.radioButton_bypassone_item.Text = "One Item";
            this.radioButton_bypassone_item.UseVisualStyleBackColor = true;
            // 
            // radioButton_bypassfull_row
            // 
            this.radioButton_bypassfull_row.AutoSize = true;
            this.radioButton_bypassfull_row.Location = new System.Drawing.Point(6, 15);
            this.radioButton_bypassfull_row.Name = "radioButton_bypassfull_row";
            this.radioButton_bypassfull_row.Size = new System.Drawing.Size(66, 17);
            this.radioButton_bypassfull_row.TabIndex = 0;
            this.radioButton_bypassfull_row.Text = "Full Row";
            this.radioButton_bypassfull_row.UseVisualStyleBackColor = true;
            // 
            // label_full_row
            // 
            this.label_full_row.AutoSize = true;
            this.label_full_row.Location = new System.Drawing.Point(386, 550);
            this.label_full_row.Name = "label_full_row";
            this.label_full_row.Size = new System.Drawing.Size(42, 13);
            this.label_full_row.TabIndex = 18;
            this.label_full_row.Text = "Ctrl + G";
            // 
            // label_one_item
            // 
            this.label_one_item.AutoSize = true;
            this.label_one_item.Location = new System.Drawing.Point(388, 574);
            this.label_one_item.Name = "label_one_item";
            this.label_one_item.Size = new System.Drawing.Size(41, 13);
            this.label_one_item.TabIndex = 19;
            this.label_one_item.Text = "Ctrl + B";
            // 
            // ProductsScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 614);
            this.Controls.Add(this.label_one_item);
            this.Controls.Add(this.label_full_row);
            this.Controls.Add(this.groupBox_bypass);
            this.Controls.Add(this.btn_bypass);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.lb_carton_nr);
            this.Controls.Add(this.btn_next_pakage);
            this.Controls.Add(this.btn_prev_pakage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelWarningMessage);
            this.Controls.Add(this.txtSerialNum);
            this.Controls.Add(this.labelSerialNum);
            this.Controls.Add(this.btn_finish);
            this.Controls.Add(this.btnNewPackage);
            this.Controls.Add(this.labelScanCode);
            this.Controls.Add(this.txtScanCode);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(678, 648);
            this.Name = "ProductsScan";
            this.Text = "ProductsScan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProductsScan_FormClosing);
            this.Load += new System.EventHandler(this.ProductsScan_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ProductsScan_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgw_serial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_products)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox_bypass.ResumeLayout(false);
            this.groupBox_bypass.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtScanCode;
        private System.Windows.Forms.Label labelScanCode;
        private System.Windows.Forms.Button btnNewPackage;
        private System.Windows.Forms.Button btn_finish;
        private System.Windows.Forms.Label labelSerialNum;
        private System.Windows.Forms.TextBox txtSerialNum;
        private System.Windows.Forms.Label labelWarningMessage;
        private System.Windows.Forms.DataGridView dgw_serial;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_prev_pakage;
        private System.Windows.Forms.Button btn_next_pakage;
        private System.Windows.Forms.Label lb_carton_nr;
        private System.Windows.Forms.DataGridView dataGrid_products;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btn_bypass;
        private System.Windows.Forms.GroupBox groupBox_bypass;
        private System.Windows.Forms.RadioButton radioButton_bypassone_item;
        private System.Windows.Forms.RadioButton radioButton_bypassfull_row;
        private System.Windows.Forms.Label label_full_row;
        private System.Windows.Forms.Label label_one_item;
    }
}