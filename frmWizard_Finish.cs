﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard_Finish : Form
    {
        public frmWizard_Finish()
        {
            InitializeComponent();
            if (!Template.SaveShipments) tcSQL.TabPages.Remove(tpShipments);
        }

        private void frmWizardFinish_Load(object sender, EventArgs e)
        {
            tcSQL_SelectedIndexChanged(sender, e);
        }
        
        private void frmWizardFinish_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK) return;
            if (DialogResult == DialogResult.No)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            DialogResult = DialogResult.Abort;
        }

        private void frmWizardFinish_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_Finish");
            e.Cancel = true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Template.Save();
            DialogResult = DialogResult.OK;
        }

        private void tcSQL_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSQL.Parent = tcSQL.SelectedTab;
            switch (tcSQL.SelectedTab.Text)
            {
                case TableType.Orders:
                    txtSQL.Lines = (Orders_Select_Statement.Select + "\nWHERE \n" + Select_Filter.Filter_With_Status).Split('\n');
                    break;

                case TableType.Packages:
                    txtSQL.Lines = (Packages_Select_Statement.Select).Split('\n');
                    break;

                case TableType.Products:
                    txtSQL.Lines = (Products_Select_Statement.Select).Split('\n');
                    break;

                case TableType.Shipments:
                    txtSQL.Lines = Shipments_Insert_Statement.Insert.Split('\n');
                    break;
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                //Select_Filter.Filter_With_Status might be null
                string s = string.IsNullOrEmpty(Select_Filter.Filter_With_Status) ? "" : "\nWHERE\n" + Select_Filter.Filter_With_Status;
                switch (tcSQL.SelectedTab.Text)
                {
                    case TableType.Orders:
                        //new frmWizard_DataPreview(tcSQL.SelectedTab.Text, Template.ReadConn.ConnStr, Orders_Select_Statement.Select.Replace("SELECT", "SELECT TOP 10") + "\nWHERE\n" + Select_Filter.Filter_With_Status + " ORDER BY 1").ShowDialog(this);
                        new frmWizard_DataPreview(tcSQL.SelectedTab.Text, Template.ReadConn.ConnStr, Orders_Select_Statement.Select.Replace("SELECT","SELECT TOP 10") + s + "ORDER BY 1").ShowDialog(this);
                        break;

                    case TableType.Packages:
                        new frmWizard_DataPreview(tcSQL.SelectedTab.Text, Template.ReadConn.ConnStr, Packages_Select_Statement.Select.Replace("SELECT", "SELECT TOP 10") + s + " ORDER BY 1").ShowDialog(this);
                        //new frmWizard_DataPreview(tcSQL.SelectedTab.Text, Template.ReadConn.ConnStr, Packages_Select_Statement.Select.Replace("SELECT", "SELECT TOP 10") + "\nWHERE\n" + Select_Filter.Filter_With_Status + " ORDER BY 1").ShowDialog(this);
                        break;

                    case TableType.Products:
                        new frmWizard_DataPreview(tcSQL.SelectedTab.Text, Template.ReadConn.ConnStr, Products_Select_Statement.Select.Replace("SELECT", "SELECT TOP 10") + s + " ORDER BY 1").ShowDialog(this);
                        //new frmWizard_DataPreview(tcSQL.SelectedTab.Text, Template.ReadConn.ConnStr, Products_Select_Statement.Select.Replace("SELECT", "SELECT TOP 10") + "\nWHERE\n" + Select_Filter.Filter_With_Status + " ORDER BY 1").ShowDialog(this);
                        break;

                    case TableType.Shipments:
                        using (OdbcConnection Conn = new OdbcConnection(Template.ReadConn.ConnStr))
                        {
                            Conn.Open();
                            using (OdbcCommand Comm = new OdbcCommand())
                            {
                                try
                                {
                                    // replace the parameters with odbc style ? parameters
                                    string sql = Shipments_Insert_Statement.Insert;
                                    foreach (Binding Binding in Template.Bindings)
                                        if (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments)
                                            sql = sql.Replace('@' + Binding.Field.Name.Replace(" ", ""), "?");
                                    
                                    Comm.Connection = Conn;
                                    Comm.CommandText = sql;
                                    
                                    // ask the user the enter some test values for each insert parameter
                                    foreach (Binding Binding in Template.Bindings)
                                        if (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments)
                                            Comm.Parameters.AddWithValue("?", frmInputBox.InputBox(this, Binding.Field.Name));

                                    int rows = Comm.ExecuteNonQuery();

                                    MessageBox.Show(Owner, "Successfully inserted " + rows + " row(s).", "Shipment Insert Test OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(Owner, ex.Message, "Shipment Insert Test Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }           
        }
    }
}
