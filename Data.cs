﻿using System.Data;
using InteGr8.DataTableAdapters;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Collections.Generic;
using System;
using System.Text;
using System.Configuration;
using System.IO;
namespace InteGr8
{
    public partial class Data
    {
        #region partial dataset implementations

        partial class TemplatesRow
        {
            public string Connection { get { return "DSN=" + DSN + "; " + (Login.Equals("") ? "" : "Uid=" + Login + "; ") + (Password.Equals("") ? "" : "Pwd=" + Password + "; "); } }

            public IEnumerable<LinksRow> GetLinksRows()
            {
                List<LinksRow> links = new List<LinksRow>();
                foreach (TablesRow table in GetTablesRows())
                {
                    foreach (LinksRow link in table.GetLinksRowsByFK_Tables_Links_Parent())
                        if (!links.Contains(link)) links.Add(link);
                    foreach (LinksRow link in table.GetLinksRowsByFK_Tables_Links_Child())
                        if (!links.Contains(link)) links.Add(link);
                }
                return links;
            }

            public IEnumerable<BindingsRow> GetBindingsRows()
            {
                List<BindingsRow> bindings = new List<BindingsRow>();
                foreach (TablesRow table in GetTablesRows())
                    foreach (BindingsRow binding in table.GetBindingsRows())
                        if (!bindings.Contains(binding)) bindings.Add(binding);
                return bindings;
            }
        }

        partial class FieldsTableDataTable
        {
            public enum FieldType { Bool = TypeCode.Boolean, Date = TypeCode.DateTime, Decimal = TypeCode.Decimal, Int = TypeCode.Int32, String = TypeCode.String }
            public enum FieldCategory
            {
                Senders, Recipients, Orders, Packages, Products, Shipments, Skids, Items, COD, HoldAtLocation,
                ExpressCheque
            }
            public enum FieldDirection { In = 0, Out = 1, Both = 2 }

            // creates a user friendly name for the enum
            // enum.ToString() will return a non-user firendly name like "OrderPackages" instead of "Order Packages"
            public static string FieldCategoryName(FieldCategory category)
            {
                switch (category)
                {
                    case FieldCategory.Orders: return "Orders";
                    case FieldCategory.Senders: return "Senders";
                    case FieldCategory.Recipients: return "Recipients";
                    case FieldCategory.Packages: return "Packages";
                    case FieldCategory.Products: return "Products";
                    case FieldCategory.Shipments: return "Shipments";
                    case FieldCategory.Skids: return "Skids";
                    case FieldCategory.Items: return "Items";
                    case FieldCategory.COD: return "COD";
                    case FieldCategory.HoldAtLocation: return "HoldAtLocation";
                    case FieldCategory.ExpressCheque: return "ExpressCheque";
                    default: throw new Exception("Category not implemented: " + category.ToString());
                }
            }

            // parse a user friendly name (used from the above function) into an enum value
            // enum.ToString() will return a non-user firendly name like "OrderPackages" instead of "Order Packages"
            public static FieldCategory FieldCategoryParse(string category)
            {
                switch (category)
                {
                    case "Orders": return FieldCategory.Orders;
                    case "Senders": return FieldCategory.Senders;
                    case "Recipients": return FieldCategory.Recipients;
                    case "Packages": return FieldCategory.Packages;
                    case "Products": return FieldCategory.Products;
                    case "Shipments": return FieldCategory.Shipments;
                    case "Skids": return FieldCategory.Skids;
                    case "Items": return FieldCategory.Items;
                    default: throw new Exception("Category not found: " + category);
                }
            }

            public override void EndInit()
            {
                base.EndInit();
                this.ColumnChanging += new DataColumnChangeEventHandler(FieldsTableDataTable_ColumnChanging);
            }

            // force the Type column values to be only the ones defined in the enum
            // force the Category column values to be only the ones defined in the enum
            // force the Direction column values to be only the ones defined in the enum
            private void FieldsTableDataTable_ColumnChanging(object sender, DataColumnChangeEventArgs e)
            {
                if (e.ProposedValue == null) return;
                if (e.Column == this.columnType && !CheckEnum(typeof(FieldType), e.ProposedValue.ToString()))
                    throw new ArgumentException("Incorrect field type '" + e.ProposedValue + "'. Allowed types are: " + EnumList(typeof(FieldType)) + ".");
                if (e.Column == this.columnCategory && !CheckEnum(typeof(FieldCategory), e.ProposedValue.ToString()))
                    throw new ArgumentException("Incorrect field category '" + e.ProposedValue + "'. Allowed categories are: " + EnumList(typeof(FieldCategory)) + ".");
                if (e.Column == this.columnDirection && !CheckEnum(typeof(FieldDirection), e.ProposedValue.ToString()))
                    throw new ArgumentException("Incorrect field direction '" + e.ProposedValue + "'. Allowed direction types are: " + EnumList(typeof(FieldDirection)) + ".");
            }

            private bool CheckEnum(Type enum_type, string value)
            {
                string[] names = Enum.GetNames(enum_type);
                foreach (string type in names)
                    if (type.Equals(value)) return true;
                return false;
            }

            private string EnumList(Type enum_type)
            {
                StringBuilder str = new StringBuilder();
                foreach (string type in Enum.GetNames(enum_type))
                    str.Append(type).Append(", ");
                return str.Remove(str.Length - 2, 2).ToString();
            }

            public FieldsTableRow FindByName(string Name)
            {
                FieldsTableRow[] fields = Select("Name = '" + Name + "'") as FieldsTableRow[];
                if (fields == null || fields.Length != 1) return null;
                return fields[0];
            }

            public FieldsTableRow FindByCategoryName(string Category, string Name)
            {
                FieldsTableRow[] fields = Select("Category = '" + Category + "' And Name = '" + Name + "'") as FieldsTableRow[];
                if (fields == null || fields.Length != 1) return null;
                return fields[0];
            }
        }

        partial class FieldsTableRow
        {
            public FieldsTableDataTable.FieldType FieldType
            {
                get
                {
                    return (FieldsTableDataTable.FieldType)Enum.Parse(typeof(FieldsTableDataTable.FieldType), Type);
                }
                set
                {
                    Type = value.ToString();
                }
            }

            public FieldsTableDataTable.FieldCategory FieldCategory
            {
                get
                {
                    return (FieldsTableDataTable.FieldCategory)Enum.Parse(typeof(FieldsTableDataTable.FieldCategory), Category);
                }
                set
                {
                    Type = value.ToString();
                }
            }

            public FieldsTableDataTable.FieldDirection FieldDirection
            {
                get
                {
                    return (FieldsTableDataTable.FieldDirection)Enum.Parse(typeof(FieldsTableDataTable.FieldDirection), Direction);
                }
                set
                {
                    Type = value.ToString();
                }
            }
        }

        partial class RequestsTableDataTable
        {
            public override void EndInit()
            {
                base.EndInit();
                this.ColumnChanging += new DataColumnChangeEventHandler(RequestsTableDataTable_ColumnChanging);
            }

            // casts the original request field value to the correct field type
            private void RequestsTableDataTable_ColumnChanging(object sender, DataColumnChangeEventArgs e)
            {
                if (e.Column != this.columnValue || e.ProposedValue == null) return;
                FieldsTableDataTable.FieldType type = (e.Row as RequestsTableRow).FieldsTableRow.FieldType;
                try
                {
                    // if the target type is Int, try to convert to decimal first to be able to cut off zero decimals
                    if (type == FieldsTableDataTable.FieldType.Int) e.ProposedValue = Convert.ChangeType(e.ProposedValue, TypeCode.Decimal);
                    if (type == FieldsTableDataTable.FieldType.Date)
                        e.ProposedValue = Convert.ToDateTime(e.ProposedValue).ToString("MM/dd/yyyy");
                    else e.ProposedValue = Convert.ChangeType(e.ProposedValue, (TypeCode)type).ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to cast value '" + e.ProposedValue.ToString() + "' of type " + e.ProposedValue.GetType().ToString() + " to type " + type.ToString(), ex);
                }
            }
        }

        partial class TablesDataTable
        {
            public TablesRow FindByTemplateNameType(string Template, string Name, string Type)
            {
                TablesRow[] tables = Select("Template = '" + Template + "' And Name = '" + Name + "' And Type = '" + Type + "'") as TablesRow[];
                if (tables == null || tables.Length != 1) return null;
                return tables[0];
            }
        }

        partial class BillingAccountsDataTable
        {
            public BillingAccountsRow FindByAccount(string account)
            {
                BillingAccountsRow[] rows = Select("BillingAccount = '" + account + "'") as BillingAccountsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }
        }
        partial class Carriers_MappingDataTable
        {
            public Carriers_MappingRow FindByClient_Carrier_Code(string code)
            {
                Carriers_MappingRow[] rows = Select("Client_Carrier_Code = '" + code + "'") as Carriers_MappingRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }
        }
        partial class CarriersDataTable
        {
            public CarriersRow FindByCode(string code)
            {
                CarriersRow[] rows = Select("Carrier_Code = '" + code + "'") as CarriersRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }

            public CarriersRow FindByName(string name)
            {
                CarriersRow[] rows = Select("Name = '" + name + "'") as CarriersRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }
        }
        partial class AvailableCarriersDataTable
        {
            public AvailableCarriersRow FindByLocation(string id)
            {
                AvailableCarriersRow[] rows = Select("ClientID = '" + id + "'") as AvailableCarriersRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }
            public AvailableCarriersRow FindByLocation_Carrier(string id, string carrierCode)
            {
                AvailableCarriersRow[] rows = Select("ClientID = '" + id + "' And CarrierCode = '" + carrierCode + "'") as AvailableCarriersRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }
        }

        partial class ClientsDataTable
        {
            public ClientsRow FindByClientID(string id)
            {
                ClientsRow[] rows = Select("ClientID = '" + id + "'") as ClientsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }

            public ClientsRow FindByClientID_WSKey(string id, string wsKey)
            {
                ClientsRow[] rows = Select("ClientID = '" + id + "' And WSKey = '" + wsKey + "'") as ClientsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }

            public ClientsRow FindByLocation(string id)
            {
                ClientsRow[] rows = Select("Location = '" + id + "'") as ClientsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }

            public ClientsRow FindByLocation_WSKey(string id, string wsKey)
            {
                ClientsRow[] rows = Select("Location = '" + id + "' And WSKey = '" + wsKey + "'") as ClientsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }

        }

        partial class ShipmentOptionsDataTable
        {
            public ShipmentOptionsRow FindByOptionCode_CarrierCode(string code, string carrierCode)
            {
                ShipmentOptionsRow[] rows = Select("OptionCode = '" + code + "' And CarrierCode = '" + carrierCode + "'") as ShipmentOptionsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }
            public ShipmentOptionsRow FindByOptionCode(string optionCode)
            {
                ShipmentOptionsRow[] rows = Select("OptionCode = '" + optionCode + "'") as ShipmentOptionsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }

            public ShipmentOptionsRow FindByOptionCode_Index(string optionCode, string index)
            {
                ShipmentOptionsRow[] rows = Select("OptionCode = '" + optionCode + "' And Index = '" + index + "'") as ShipmentOptionsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }

            public ShipmentOptionsRow[] GetCarrierOptions(string carrierCode)
            {
                ShipmentOptionsRow[] rows = Select("CarrierCode = '" + carrierCode + "'") as ShipmentOptionsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows;
            }
        }

        partial class ServicesRow
        {
            public override string ToString()
            {
                return this.Name;
            }
        }

        partial class ServicesDataTable
        {
            public ServicesRow FindByCode(string carrier, string code)
            {
                ServicesRow[] rows = Select("Carrier_Code = '" + carrier + "' And Service_Code = '" + code + "'") as ServicesRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }

            public ServicesRow FindByName(string carrier, string name)
            {
                ServicesRow[] rows = Select("Carrier_Code = '" + carrier + "' And Name = '" + name + "'") as ServicesRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }

            public ServicesRow FindByUnique(string unique)
            {
                ServicesRow[] rows = Select("Unique_Code = '" + unique + "'") as ServicesRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }
        }

        partial class Carrier_PackagingsDataTable
        {
            public Carrier_PackagingsRow FindByUnique(string unique)
            {
                Carrier_PackagingsRow[] rows = Select("Unique_Code = '" + unique + "'") as Carrier_PackagingsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }
        }

        partial class PackageDimensionsDataTable
        {
            public PackageDimensionsRow FindByCode(string code)
            {
                PackageDimensionsRow[] rows = Select("PackageDimCode = '" + code + "'") as PackageDimensionsRow[];
                if (rows == null || rows.Length == 0) return null;
                return rows[0];
            }
        }

        #endregion

        // reloads the data in the dataset
        // TODO: synchronize access with a lock statement

        public static Dictionary<int, string> Special_Columns = new Dictionary<int, string>()
        {
            { 949, "PRO #"},
            { 161, "Delivery Date"},
            { 167, "ClientFreight" },
            { 1670, "ClientFreightForShipment" },
            { 1830, "ClientFinalChrgForShipment" },
            { 379, "ClientTaxes" },
            { 3790, "ClientTaxesForShipment" },
            { 6801, "ClientCarrier" },
            { 9401, "ClientService" },
            { 9301, "ShippingDate" },
            { -183, "ClientFinalChargePackage" },
            { -16, "Residential" },
            { -17, "Void" },
            { -18, "OversizeCode" },
            { -50, "OrderNumber"},
            { 945, "BOL URL"},
            { 611, "Return Label URL"},
            { 158159, "Carrier and service"},
            { 168, "Client Fuel Surcharge"},
            { 169, "Client Surcharges"},
            { 170, "Client GST"},
            { 171, "Client PST"},
            { 172, "Client QST"},
            { 173, "Client HST"},
            {1830183, "ShipmentTotalCharge"}
        };

        //* the shipment pickup days list
        public bool[] PickUpDays = new bool[10];

        public void InitPickUpDays()
        {
            //* init all weekdays as pickup days(default setting is to have all days as pick up days)
            PickUpDays[0] = Template.PickUpMonday;
            PickUpDays[1] = Template.PickUpTuesday;
            PickUpDays[2] = Template.PickUpWednesday;
            PickUpDays[3] = Template.PickUpThursday;
            PickUpDays[4] = Template.PickUpFriday;
            PickUpDays[5] = Template.PickUpSaturday;
            PickUpDays[6] = Template.PickUpSunday;

        }

        public void setOrdersShipDate()
        {
            String shipDate = getNextShipDate(DateTime.Today);
            for (int i = 0; i < OrdersTable.Rows.Count; i++)
            {
                string[] s = shipDate.Split('-');
                OrdersTable.Rows[i]["Ship Date"] = (new DateTime(Convert.ToInt32(s[2]), Convert.ToInt32(s[0]), Convert.ToInt32(s[1])));
            }

        }

        public bool isLTLCarrier(string carrier)
        {
            //CarriersDataTable
            bool isLTLCarrier = false;
            foreach (CarriersRow c in Carriers)
            {
                if (c.Carrier_Code.Equals(carrier))
                    isLTLCarrier = c.IsLTL;
            }
            return isLTLCarrier;
        }

        public bool CarrierHasItems(string carrier)
        {
            //CarriersDataTable
            bool hasItems = false;
            foreach (CarriersRow c in Carriers)
            {
                if (c.Carrier_Code.Equals(carrier))
                    hasItems = c.HasItems;
            }
            return hasItems;
        }

        public bool IsFreightService(string carrier, string service)
        {
            if (carrier.Equals("2") && (service.Equals("2 - 101") || (service.Equals("2 - 102"))))
                return true;
            return false;
        }

        public void AddUPSReturnServices()
        {
            UPSReturnServiceType.Clear();
            UPSReturnServiceType.AddUPSReturnServiceTypeRow("3", "Return Service 1-Attempt(RS1)");
            UPSReturnServiceType.AddUPSReturnServiceTypeRow("5", "Return Service 3-Attempt(RS3)");
            UPSReturnServiceType.AddUPSReturnServiceTypeRow("8", "Electronic Return Label(ERL)");
            UPSReturnServiceType.AddUPSReturnServiceTypeRow("9", "Print Return Label Now(PRL)");
        }

        public void Refresh()
        {
            Clear();

            // these two lines will load the fields definitions from the database and write them to an xml file
            // this file can be imported into resources when the exe is built, this way the app can run disconnected from the db
            // load field definitions from XML string (in exe resources)
            try
            {
                //FieldsTable.ReadXml(new System.IO.StringReader(Properties.Resources.Fields));
                FieldsTable.ReadXml(InteGr8.Properties.Settings.Default.Fields);

                //Carriers.ReadXml(InteGr8.Properties.Settings.Default.CarriersFile);
            }
            catch (Exception e)
            {
                //throw new Exception("Error reading Fields.xml from exe resources.", e);
                Utils.SendError("Error reading Fields.xml from exe resources.", e);
            }

            // manually add some internal fields required for InteGr8 to work
            try
            {
                FieldsTable.AddFieldsTableRow(-1, "Order #", "Packages", "String", null, null, null, "In", false);
                FieldsTable.AddFieldsTableRow(-2, "Package #", "Packages", "String", null, null, null, "In", false);
                FieldsTable.AddFieldsTableRow(-3, "Order #", "Products", "String", null, null, null, "In", false);
                FieldsTable.AddFieldsTableRow(-4, "Product #", "Products", "String", null, null, null, "In", false);
                FieldsTable.AddFieldsTableRow(-5, "Order #", "Shipments", "String", null, null, null, "Out", false);
                FieldsTable.AddFieldsTableRow(-6, "Status", "Shipments", "String", null, null, null, "Out", false);
                FieldsTable.AddFieldsTableRow(-7, "Total Weight", "Packages", "Int", null, null, null, "In", false);
                // Skids and Itemd IDs
                FieldsTable.AddFieldsTableRow(-8, "Order #", "Skids", "String", null, null, null, "In", false);
                FieldsTable.AddFieldsTableRow(-9, "Skid #", "Skids", "String", null, null, null, "In", false);
                FieldsTable.AddFieldsTableRow(-10, "Order #", "Items", "String", null, null, null, "In", false);
                FieldsTable.AddFieldsTableRow(-11, "LTL Item #", "Items", "String", null, null, null, "In", false);

            }
            catch (Exception e)
            {
                Utils.SendError("Error adding internal fields to the Fields table.", e);
                //throw new Exception("Error adding internal fields to the Fields table.", e);
            }

            // load static data from XML string (in exe resources)
            try
            {
                Carriers.ReadXml(InteGr8.Properties.Settings.Default.CarriersFile);

                Services.ReadXml(Properties.Settings.Default.ServicesFile);

                Packagings.ReadXml(Properties.Settings.Default.PackagingsFile);

                Carrier_Packagings.ReadXml(Properties.Settings.Default.Carrier_PackagingsFile);

                Billing_Types.ReadXml(Properties.Settings.Default.Billing_TypesFile);

                SCACS.ReadXml(Properties.Settings.Default.SCACSFile);
            }
            catch (Exception e)
            {
                Utils.SendError("Error reading xml data from exe resources.", e);
                //throw new Exception("Error reading xml data from exe resources.", e);
            }

            // load dynamic data from xml files
            try
            {
                Carriers_Mapping.ReadXml(Properties.Settings.Default.Carriers_MappingFile);
                bool ClientCarrierCodeIs2ShipCode = false;
                foreach (CarriersRow carrier in Carriers)
                    if (carrier.GetCarriers_MappingRows().Length == 0)
                        Carriers_Mapping.AddCarriers_MappingRow(carrier.Carrier_Code, carrier);
                    //if a carrier is already mapped in Carriers_Mapping, it will not be added, and  it will not be read in app.
                    else
                    {
                        int nCarrierRows = carrier.GetCarriers_MappingRows().Length;
                        for (int p = 0; p < nCarrierRows; p++)
                        {
                            ClientCarrierCodeIs2ShipCode = false;
                            int i = Carriers_Mapping.Rows.IndexOf(carrier.GetCarriers_MappingRows()[0]);
                            Carriers_MappingRow cM = (Carriers_MappingRow)Carriers_Mapping.Rows[i];
                            string sClientCarrierCode = cM.Client_Carrier_Code;
                            if (sClientCarrierCode.Equals(carrier.Carrier_Code))
                            {
                                ClientCarrierCodeIs2ShipCode = true;
                            }
                            Carriers_Mapping.RemoveCarriers_MappingRow(cM);
                            Carriers_Mapping.AddCarriers_MappingRow(sClientCarrierCode, carrier);
                        }
                        //if the client carrier code is the same as 2ship code, then this line is already added
                        if (!ClientCarrierCodeIs2ShipCode)
                        {
                            Carriers_Mapping.AddCarriers_MappingRow(carrier.Carrier_Code, carrier);
                        }
                    }

                Services_Mapping.ReadXml(Properties.Settings.Default.Services_MappingFile);
                ClientCarrierCodeIs2ShipCode = false;
                bool ClientServiceCodeIs2ShipService = false;
                foreach (ServicesRow service in Services)
                    if (service.GetServices_MappingRows().Length == 0)
                        Services_Mapping.AddServices_MappingRow(service.CarriersRow.GetCarriers_MappingRows()[0], service.Service_Code, service.Carrier_Code, service.Service_Code);
                    else
                    {
                        int nServiceRows = service.GetServices_MappingRows().Length;
                        int carrierIndex = 0;
                        ClientCarrierCodeIs2ShipCode = false;
                        ClientServiceCodeIs2ShipService = false;
                        for (int p = 0; p < nServiceRows; p++)
                        {
                            int i = Services_Mapping.Rows.IndexOf(service.GetServices_MappingRows()[0]);
                            Services_MappingRow sM = (Services_MappingRow)Services_Mapping.Rows[i];
                            carrierIndex = FindCarrierMappingIndex(sM);

                            string sClientServiceCode = sM.Client_Service_Code;
                            if (service.Carrier_Code.Equals(sM.Client_Carrier_Code))
                            {
                                ClientCarrierCodeIs2ShipCode = true;
                            }
                            if (sClientServiceCode.Equals(service.Service_Code))
                            {
                                ClientServiceCodeIs2ShipService = true;
                            }
                            Services_Mapping.RemoveServices_MappingRow(sM);
                            Services_Mapping.AddServices_MappingRow(service.CarriersRow.GetCarriers_MappingRows()[carrierIndex], sClientServiceCode, service.Carrier_Code, service.Service_Code);
                        }
                        //if the client carrier code is the same as 2ship code, then this line is already added
                        if (ClientCarrierCodeIs2ShipCode && !ClientServiceCodeIs2ShipService)
                        {
                            Services_Mapping.AddServices_MappingRow(service.CarriersRow.GetCarriers_MappingRows()[0], service.Service_Code, service.Carrier_Code, service.Service_Code);
                        }
                    }

                Packagings_Mapping.ReadXml(Properties.Settings.Default.Packagings_MappingFile);
                foreach (Carrier_PackagingsRow carrier_packaging in Carrier_Packagings)
                    if (carrier_packaging.GetPackagings_MappingRows().Length == 0)
                        Packagings_Mapping.AddPackagings_MappingRow(carrier_packaging.CarriersRow.GetCarriers_MappingRows()[0], carrier_packaging.Carrier_Packaging_Code, carrier_packaging.Carrier_Code, carrier_packaging.Carrier_Packaging_Code);
            }
            catch (Exception e)
            {
                Utils.SendError("Error reading xml data from external exe resources.", e);
                //throw new Exception("Error reading data from external xml resources.", e);
            }

            if (!Properties.Settings.Default.PackageDimensions.Equals(""))
                try
                {
                    PackageDimensions.ReadXml(Properties.Settings.Default.PackageDimensions);
                }
                catch (Exception e)
                {
                    Utils.SendError("Error reading data from PackageDimensions.xml.", e);
                    //throw new Exception("Error reading data from PackageDimensions.xml.", e);
                }
            if (!Properties.Settings.Default.BillingAccounts.Equals(""))
                try
                {
                    if (!String.IsNullOrEmpty(File.ReadAllText(Properties.Settings.Default.BillingAccounts)))
                    {
                        BillingAccounts.ReadXml(Properties.Settings.Default.BillingAccounts);
                    }
                }
                catch (Exception e)
                {
                    Utils.SendError("Error reading data from BillingAccounts.xml.", e);
                    //throw new Exception("Error reading data from BillingAccounts.xml.", e);
                }
            if (!Properties.Settings.Default.ShipmentOptions.Equals(""))
                try
                {
                    ShipmentOptions.ReadXml(Properties.Settings.Default.ShipmentOptions);
                }
                catch (Exception e)
                {
                    Utils.SendError("Error reading data from ShipmentOptions.xml.", e);
                    //throw new Exception("Error reading data from ShipmentOptions.xml.", e);
                }
            // load the templates from XML (the file must be in the same folder where the exe is)
            try
            {
                ReadXml(Properties.Settings.Default.TemplatesFile);
            }
            catch (Exception e)
            {
                Utils.SendError("Error reading data from Templates.xml.", e);
                //throw new Exception("Error reading data from Templates.xml.", e);
            }

        }

        private int FindCarrierMappingIndex(Services_MappingRow sM)
        {
            int index = 0;
            string sClientCarrier = sM.Client_Carrier_Code.ToString();
            CarriersRow carrier = Carriers.FindByCode(sM.Carrier_Code);
            int firstCarrierIndex = Carriers_Mapping.Rows.IndexOf(carrier.GetCarriers_MappingRows()[0]);
            foreach (Carriers_MappingRow cmr in carrier.GetCarriers_MappingRows())
            {
                if (cmr.Client_Carrier_Code == sClientCarrier)
                {
                    index = Carriers_Mapping.Rows.IndexOf(cmr);
                    index = index - firstCarrierIndex;
                    return index;
                }
            }
            return index;
        }

        // clears the import datatables
        public void ClearTemplate()
        {
            // clear existing data
            try
            {
                EnforceConstraints = false;
                RequestsTable.Clear();
                RepliesTable.Clear();
                 OrdersSkidsTable.Clear();
                OrderLTLItems.Clear();
                OrderProductsTable.Clear();
                OrderPackagesTable.Clear();
                ShipmentsTable.Clear();
                OrdersTable.Clear();
                //EnforceConstraints = true;
            }
            catch (Exception e)
            {
                Utils.SendError("Unable to clear dataset.", e);
                //throw new Exception("Unable to clear dataset.", e);
            }
        }

        // executes the import commands from the current template
        public void LoadTemplate(string currentFilter)
        {
            //* save the shipped orders which wew not saved to database yet(they are in shipments tab / page)
            Data.ShipmentsTableDataTable oldShipmentsTable = BackupUnsavedShipmentsForFilteredOrders();

            //if it is multi shipment, don't Clear the Template
            if (!Properties.Settings.Default.MultiOrderShipment)
            {
                ClearTemplate();
            }

            EnforceConstraints = false;

            using (OdbcConnection Conn = new OdbcConnection(Template.ReadConn.ConnStr))
            {
                try
                {
                    Conn.ConnectionTimeout = 50;
                    Conn.Open();
                }
                catch (Exception e)
                {
                    Utils.SendError("Unable to open the connection: '" + Template.ReadConn.ConnStr + "'.", e);
                    //throw new Exception("Unable to open the connection: '" + Template.ReadConn.ConnStr + "'.", e);
                }

                //* original code:
                //for some templates, ReadConn != WriteConn
                //the original condition, with automatic_filter as Select_Filter.Filter_With_Status
                //string  automatic_filter = Select_Filter.Filter_With_Status;
                //the new condition, with automatic_filter as the filter from parameter
                string automatic_filter = currentFilter;

                try
                {
                    Data tmp_data = new Data();

                    tmp_data.EnforceConstraints = false;
                    tmp_data.OrdersTable.Columns.Add("Shipment Reference Info", typeof(String));
                    ////get the sender address from database
                    //try
                    //{
                    //    OdbcDataReader reader = new OdbcCommand("SELECT CONTEXT_INFO()", Conn).ExecuteReader();
                    //    reader.Read();
                    //    string s = reader[0] != DBNull.Value ? reader.GetString(0) : "";

                    //    TimeSpan t = (DateTime.Now - new DateTime(1970, 1, 1));
                    //    decimal unic = (decimal)t.TotalSeconds;
                    //    new OdbcCommand("SET CONTEXT_INFO " + unic, Conn).ExecuteNonQuery();
                    //    reader = new OdbcCommand("SELECT CONTEXT_INFO()", Conn).ExecuteReader();
                    //    reader.Read();
                    //    s = reader.GetString(0);
                    //}
                    //catch (Exception e)
                    //{
                    //    //ignore
                    //}

                    if (!String.IsNullOrEmpty(automatic_filter))
                    {//!Template.Name.Equals("Chocolate Inn") &&
                        if (!(Template.Name.Equals("Surteco") || Template.Name.Contains("BodyPlus")))
                        {
                            automatic_filter = automatic_filter.Split('.')[1].Replace(")", "");
                        }

                        if (Template.Name.Contains("Surteco") || Template.Name.Contains("BodyPlus"))
                        {
                            automatic_filter = automatic_filter.Replace("\"", "").Replace("(", "").Replace(")", "");
                        }
                        if (Template.Name.Equals("Chocolate Inn"))
                        {
                            string test_InitalLoad = automatic_filter;//.Substring(0, automatic_filter.IndexOf(")") );
                            test_InitalLoad = test_InitalLoad.Trim();
                            if (test_InitalLoad.IndexOf("=") == test_InitalLoad.Length - 1)
                            {
                                automatic_filter = automatic_filter.Substring(0, automatic_filter.IndexOf("=") + 1) + "0 ";
                            }
                            else
                            {
                                test_InitalLoad = automatic_filter.Substring(automatic_filter.IndexOf("=") + 1);
                                test_InitalLoad = test_InitalLoad.Replace("\'", "");
                                automatic_filter = automatic_filter.Substring(0, automatic_filter.IndexOf("=") + 1) + test_InitalLoad;
                            }
                        }
                    }
                    //System.IO.File.AppendAllText(".\\Error.txt", Orders_Select_Statement.Select + " " + automatic_filter + "  -  " + DateTime.Now + "\r\n");
                    OdbcCommand com = new OdbcCommand(Orders_Select_Statement.Select + (!String.IsNullOrEmpty(automatic_filter) ? "\nWHERE\n" + automatic_filter : automatic_filter), Conn);

                    if (Template.Name.Contains("BodyPlus"))
                    {
                        com = new OdbcCommand(Orders_Select_Statement.Select + (!String.IsNullOrEmpty(automatic_filter) ? "\nAND\n" + automatic_filter : automatic_filter), Conn);

                    }
                    com.CommandTimeout = 500;
                    OdbcDataAdapter adapter = new OdbcDataAdapter(com);
                    adapter.Fill(tmp_data.OrdersTable);
                    OrdersTable.Merge(tmp_data.OrdersTable);
                    //System.IO.File.AppendAllText(".\\Error.txt", "Orders loaded" + "  -  " + DateTime.Now + "\r\n");

                    //rename the columns to 2ship names if they are not (Informix sql case)
                    RenameOrdersColumns();
                    //System.IO.File.AppendAllText(".\\Error.txt", "Orders columns renamed" + "  -  " + DateTime.Now + "\r\n");

                    // artifact: replace the original client service code and client packaging code to their unique codes (based on the carrier code and service code / packaging code combination)
                    string sCarrier = "";
                    string sService = "";
                    foreach (OrdersTableRow order in OrdersTable)
                    {
                        //client codes have white characters and that is stopping the carrier and services mappings to be found (code\r\n != code)
                        order.Carrier = order.Carrier.Replace("\r\n", "").Trim();
                        order.Service = order.Service.Replace("\r\n", "").Trim();
                        //the bellow commented code is just a test, not needed right now
                        //if we scan each order one at a time, and we have carrier * services 7 set up, the Carriers_MappingsRow could be null
                        //so we have to search for it and add it to the order
                        //if (!order.IsCarrierNull() && order.Carriers_MappingRow == null)
                        //{
                        //    Carriers_MappingRow carrierRow = Carriers_Mapping.FindByClient_Carrier_Code(order.Carrier);
                        //    if (carrierRow != null)
                        //    {
                        //        order.Carriers_MappingRow = carrierRow;
                        //    }
                        //}

                        ////if we scan each order one at a time, and we have carrier * services mappings set up, the Services_MappingsRow could be null
                        ////so we have to search for it and add it to the order

                        //if (!order.IsServiceNull() && order.Services_MappingRowParent == null)
                        //{
                        //    Services_MappingRow serviceRow = Services_Mapping.FindByClient_Carrier_CodeClient_Service_Code(order.Carrier, order.Service);
                        //    if (serviceRow != null)
                        //    {
                        //        order.Services_MappingRowParent = serviceRow;
                        //    }
                        //}

                        if (!order.IsServiceNull() && order.Services_MappingRowParent != null && order.Services_MappingRowParent.ServicesRowParent != null)
                        {
                            sService = order.Services_MappingRowParent.ServicesRowParent.Unique_Code;
                            order.Service = sService;
                        }
                        //to be tested this else for other cases - the scenario for which I added it is: MultiShipment the 2+ orders will reset the service for existing orders
                        //so, if the order has already a service, and no mapping row, leave the existing service in place
                        else if (order.IsServiceNull() && (!Properties.Settings.Default.MultiOrderShipment))
                        {
                            order.Service = sService;
                        }

                        //check the carrier now
                        if (!order.IsCarrierNull() && order.Carriers_MappingRow != null)
                        {
                            sCarrier = order.Carriers_MappingRow.Carrier_Code;
                            order.Carrier = sCarrier;
                        }
                        //order.Carrier = sCarrier;
                        if (order.IsPackagingNull() || Packagings.FindByPackaging_Code(order.Packaging) == null)
                            order.Packaging = "1";
                        sCarrier = "";
                        sService = "";
                    }
                }
                catch (Exception e)
                {
                    Utils.SendError("Error running Orders import: Select statement: \n" + Orders_Select_Statement.Select + " " + automatic_filter + "\n", e);
                }

                try
                {
                    // load the packages in a temporary table
                    //add skids columns back to packages data table so that they can be filled...
                    foreach (DataColumn col in OrdersSkidsTable.Columns)
                    {
                        if (OrderPackagesTable.Columns.IndexOf(col.ColumnName.ToString()) < 0)
                        {
                            OrderPackagesTable.Columns.Add(col.ColumnName);
                        }
                    }
                    Data tmp_data = new Data();
                    tmp_data.EnforceConstraints = false;
                    tmp_data.OrderPackagesTable.Columns.Add("Package Reference Info 1", typeof(String));
                    //System.IO.File.AppendAllText(".\\Error.txt", Packages_Select_Statement.Select + " " + automatic_filter + "  -  " + DateTime.Now + "\r\n");

                    OdbcCommand com = new OdbcCommand(Packages_Select_Statement.Select + (!String.IsNullOrEmpty(automatic_filter) ? "\nWHERE\n" + automatic_filter : automatic_filter), Conn);

                    if (Template.Name.Equals("Chocolate Inn"))
                    {
                        com = new OdbcCommand((Packages_Select_Statement.Select.Contains("ORDER BY") ? Packages_Select_Statement.Select.Substring(0, Packages_Select_Statement.Select.IndexOf("ORDER BY")) : Packages_Select_Statement.Select)
                        + ((!String.IsNullOrEmpty(automatic_filter) ? "\nWHERE\n" + automatic_filter : automatic_filter)
                         + (Packages_Select_Statement.Select.Contains("ORDER BY") ? Packages_Select_Statement.Select.Substring(Packages_Select_Statement.Select.IndexOf("ORDER BY")) : "")), Conn);

                    }

                    com.CommandTimeout = 500;
                    new OdbcDataAdapter(com).Fill(tmp_data.OrderPackagesTable);
                    OrderPackagesTable.Merge(tmp_data.OrderPackagesTable);
                    //System.IO.File.AppendAllText(".\\Error.txt", "Packages loaded" + "  -  " + DateTime.Now + "\r\n");

                    RenamePackagesColumns();
                    //System.IO.File.AppendAllText(".\\Error.txt", "Packages columns renamed" + "  -  " + DateTime.Now + "\r\n");

                    if (!Properties.Settings.Default.ClientSpecialRates.Equals(""))
                    {
                        foreach (Data.OrderPackagesTableRow package in OrderPackagesTable)
                            if (package.IsNull(0) || this.OrdersTable.FindBy_Order__(package._Order__) == null)
                                package.Delete();
                        OrderPackagesTable.AcceptChanges();
                    }
                }
                catch (Exception e)
                {
                    Utils.SendError("Error running Packages import: '" + e.Message + "'.", e);
                    //throw new Exception("Error running Packages import: '" + e.Message + "'.", e);
                }

                try
                {
                    // load the products in a temporary table
                    //* verifying the time for loading data
                    foreach (DataColumn col in OrderLTLItems.Columns)
                    {
                        if (OrderProductsTable.Columns.IndexOf(col.ColumnName.ToString()) < 0)
                        {
                            OrderProductsTable.Columns.Add(col.ColumnName);
                        }
                    }

                    Data tmp_data = new Data();
                    tmp_data.EnforceConstraints = false;
                    //System.IO.File.AppendAllText(".\\Error.txt", Products_Select_Statement.Select + " " + automatic_filter + "  -  " + DateTime.Now + "\r\n");

                    OdbcCommand com = new OdbcCommand(Products_Select_Statement.Select
                        + (!String.IsNullOrEmpty(automatic_filter) ? (Products_Select_Statement.Select.Contains("WHERE") ? "\nAND\n" : "\nWHERE\n") + automatic_filter : automatic_filter), Conn);
                    com.CommandTimeout = 500;
                    new OdbcDataAdapter(com).Fill(tmp_data.OrderProductsTable);
                    // copy the remaining products to the original products table
                    OrderProductsTable.Merge(tmp_data.OrderProductsTable);
                    // System.IO.File.AppendAllText(".\\Error.txt", "Products loaded" + "  -  " + DateTime.Now + "\r\n");

                    RenameProductsColumns();

                    //System.IO.File.AppendAllText(".\\Error.txt", "Products columns renamed" + "  -  " + DateTime.Now + "\r\n");


                    if (!Properties.Settings.Default.ClientSpecialRates.Equals(""))
                    {
                        // eliminate the products without an existing order
                        foreach (Data.OrderProductsTableRow product in OrderProductsTable)
                            if (product.IsNull(0) || this.OrdersTable.FindBy_Order__(product._Order__) == null)
                                product.Delete();
                        OrderProductsTable.AcceptChanges();
                    }
                }
                catch (Exception e)
                {
                    Utils.SendError("Error running Products import: '" + e.Message + "'.", e);
                    //throw new Exception("Error running Products import: '" + e.Message + "'.", e);
                }

                if (Template.Name == "")
                {
                    try
                    {
                        if (!InteGr8_Main.data.Tables.Contains("Serial_Nr"))
                        {
                            DataTable serial_nr = new DataTable();
                            new OdbcDataAdapter(new OdbcCommand("select * from ship_products_serial", Conn)).Fill(serial_nr);
                            InteGr8_Main.data.Tables.Add(serial_nr);
                            InteGr8_Main.data.Tables["Table1"].TableName = "Serial_Nr";
                        }
                        else
                        {
                            InteGr8_Main.data.Tables["Serial_Nr"].Clear();
                            new OdbcDataAdapter(new OdbcCommand("select * from ship_products_serial", Conn)).Fill(InteGr8_Main.data.Tables["Serial_Nr"]);
                        }

                    }
                    catch (Exception e)
                    {

                    }
                }

                //* set the orders ShipDate as the next available date
                setOrdersShipDate();
                //add Skids and Items tables(Skids-> default dims and info from packages, Items-> infos from packages and Products)
                try
                {
                    if (OrdersTable.Rows.Count > 0)
                    {
                        int packs = 1;
                        decimal totalWeight = 0;
                        OrderPackagesTableRow oldPackage;
                        foreach (Data.OrdersTableRow order in OrdersTable)
                        {
                            if (order.GetOrderPackagesTableRows().Length > 0)
                            {
                                oldPackage = order.GetOrderPackagesTableRows()[0];
                                getTotalWeight(order, out totalWeight, out packs);
                                OrdersSkidsTable.AddOrdersSkidsTableRow(order, "1",
                                    oldPackage["Use Skids"].ToString() == "" ? "1" : oldPackage["Use Skids"].ToString(),
                                    oldPackage["Skid Count"].ToString() == "" ? "1" : oldPackage["Skid Count"].ToString(),
                                    oldPackage["Skid Shipment Level"].ToString() == "" ? "S" : oldPackage["Skid Shipment Level"].ToString(),
                                    oldPackage["Skid Skid Level"].ToString() == "" ? "true" : oldPackage["Skid Skid Level"].ToString(),
                                    oldPackage["Skids Count"].ToString() == "" ? "1" : oldPackage["Skids Count"].ToString(),
                                    totalWeight.ToString(),
                                    oldPackage["Package Weight Type"].ToString(),
                                    oldPackage["Skid Insurance"].ToString(),
                                    oldPackage["Package Insurance Currency"].ToString(),
                                    oldPackage["Skid Length"].ToString(), oldPackage["Skid Width"].ToString(), oldPackage["Skid Height"].ToString(),
                                    oldPackage["Package Dimensions Type"].ToString(),
                                    oldPackage["Package Reference Info 1"].ToString(),
                                    oldPackage["Skid Reference2"].ToString(),
                                    oldPackage["Package PO Number"].ToString(),
                                    oldPackage["Skid Shipment ID"].ToString(),
                                    oldPackage["Package Invoice Number"].ToString(),
                                    "",//order["Shipment PO Number"].ToString(),
                                    order["Shipment Reference Info"].ToString(),
                                    oldPackage["Skid Freight Class"].ToString() == "" ? "125.0" : oldPackage["Skid Freight Class"].ToString(),
                                    packs.ToString(),
                                   "",//oldPackage["Skid Description"].ToString(),
                                   ""//oldPackage["BOL Comment"].ToString()
                                   );
                            }

                        }
                    }
                    else
                    {
                        Data.OrdersTableRow order = null;
                        OrdersSkidsTable.AddOrdersSkidsTableRow(order, "1", "1", "1", "false", "true",
                                    "1",
                                    "0",
                                     "LBS",
                                    "",
                                    "",
                                    "", "", "",
                                    "I",
                                    "",
                                    "",
                                    "",
                                    "", "",
                                    "", "",
                                    "125.0",
                                    "1",
                                    "",
                                    "");
                    }
                }
                catch (Exception e)
                {
                    Utils.SendError("Error running skids import: '" + e.Message + "'.", e);
                    //throw new Exception("Error running skids import: '" + e.Message + "'.", e);
                }

                try
                {
                    int i = 0;
                    decimal totalProductWeight = 0;
                    decimal productWeight = 0;
                    decimal productQuantity = 0;
                    foreach (Data.OrdersTableRow order in OrdersTable)
                    {
                        i = 0;
                        if (order.GetOrderProductsTableRows().Length > 0)
                        {
                            foreach (Data.OrderProductsTableRow r in order.GetOrderProductsTableRows())
                            {
                                Decimal.TryParse(r["Product Quantity"].ToString(), out productQuantity);
                                Decimal.TryParse(r["Product Unit Weight"].ToString(), out productWeight);
                                totalProductWeight = productQuantity * productWeight;
                                OrderLTLItems.AddOrderLTLItemsRow(r["LTL Items Count"].ToString(),
                                    r["LTL Item Description"].ToString(),
                                    totalProductWeight.ToString(),
                                    r["LTL Item Total Weight Type"].ToString(),
                                    r["LTL Item Total Length"].ToString(),
                                    r["LTL Item Total Width"].ToString(),
                                    r["LTL Item Total Height"].ToString(),
                                    r["LTL Item Total Dims Type"].ToString(),
                                    r["LTL Item Freight Class ID"].ToString(),
                                    r["Product Quantity"].ToString(),
                                    r["Product Quantity MU"].ToString(),
                                    order, (i++).ToString());
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Utils.SendError("Error running items import: '" + e.Message + "'.", e);
                    throw new Exception("Error running items import: '" + e.Message + "'.", e);
                }
                try
                {
                    foreach (DataColumn c in OrdersSkidsTable.Columns)
                        if ((OrderPackagesTable.Columns.IndexOf(c.ColumnName) >= 0) && (!c.ColumnName.ToString().Equals("Order #")))
                            OrderPackagesTable.Columns.Remove(c.ColumnName);

                    foreach (DataColumn c in OrderLTLItems.Columns)
                        if ((OrderProductsTable.Columns.IndexOf(c.ColumnName) >= 0) && (!c.ColumnName.ToString().Equals("Order #")))
                            OrderProductsTable.Columns.Remove(c.ColumnName);
                }
                catch (Exception e)
                {
                    Utils.SendError("Error removing skid and items info from packages: '" + e.Message + "'.", e);
                    throw new Exception("Error removing skid and items info from packages: '" + e.Message + "'.", e);
                }
                try
                {
                    OrderPackagesTable.Columns["Package #"].ReadOnly = false;
                }
                catch (Exception ex)
                {
                    Utils.SendError("Cannot remove ReadOnly property from Package #", ex);
                    //throw ex;
                }
                //add cost calculation columns to shipments data table
                try
                {
                    foreach (Binding b in Template.Bindings)
                    {
                        if (b.Field.FieldCategory == Data.FieldsTableDataTable.FieldCategory.Shipments &&
                            Special_Columns.ContainsKey(b.Field.Id) && !ShipmentsTable.Columns.Contains(Special_Columns[b.Field.Id]))
                            ShipmentsTable.Columns.Add(Special_Columns[b.Field.Id]);
                    }
                    try
                    {
                        AddUPSReturnServices();
                    }
                    catch (Exception ee)
                    {
                        Utils.SendError("Error adding UPS return service type keys", ee);
                        throw new Exception("Error adding UPS return service type keys", ee);
                    }

                    if (oldShipmentsTable.Rows.Count > 0)
                    {
                        if (OrdersTable.Columns.IndexOf("Order number") > -1)
                        {
                            if (!(ShipmentsTable.Columns.IndexOf("OrderNumber") > -1))
                                ShipmentsTable.Columns.Add("OrderNumber");
                        }
                        for (int i = 0; i < oldShipmentsTable.Rows.Count; i++)
                        {
                            ShipmentsTableRow row = (ShipmentsTableRow)oldShipmentsTable.Rows[i];
                            //for filtered orders, the backup info will be lost when loading a new order but
                            //for non-filtered orders, this will load again the shipments from backup, 
                            //causing duplicates in ShipmentsTable
                            if (ShipmentsTable.FindBy_Order__Package_Index(row["Order #"].ToString(), 0) == null)
                            {
                                ShipmentsTable.ImportRow(row);
                            }
                            Data.OrdersTableRow rr = OrdersTable.FindBy_Order__(row._Order__);
                            if (rr != null)
                            {
                                rr.Status = "Shipped";
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Error loading Shipments: '" + e.Message + "'.", e);
                }
            }

            // automatically unselect all orders
            foreach (Data.OrdersTableRow order in OrdersTable)
                order.Selected = false;
        }

        private void RenameProductsColumns()
        {
            try
            {
                if ((OrderProductsTable.Columns.IndexOf("order") >= 0) || (OrderProductsTable.Columns.IndexOf("orderid") >= 0))
                {
                    OrderProductsTable.Columns["Order #"].ReadOnly = false;
                    foreach (OrderProductsTableRow r in OrderProductsTable.Rows)
                    {
                        r["Order #"] = (OrderProductsTable.Columns.IndexOf("order") >= 0) ? r["order"] : r["orderid"];
                    }
                    OrderProductsTable.Columns["Order #"].ReadOnly = true;
                }

                if ((OrderProductsTable.Columns.IndexOf("product") >= 0) || (OrderProductsTable.Columns.IndexOf("productid") >= 0))
                {
                    OrderProductsTable.Columns["Product #"].ReadOnly = false;
                    foreach (OrderProductsTableRow r in OrderProductsTable.Rows)
                    {
                        r["Product #"] = (OrderProductsTable.Columns.IndexOf("product") >= 0) ? r["product"] : r["productid"];
                    }
                    OrderProductsTable.Columns["Product #"].ReadOnly = true;
                }

                string bindingName = "";
                foreach (Binding b in Template.Bindings)
                {
                    if (b.Field.FieldCategory == FieldsTableDataTable.FieldCategory.Products || b.Field.FieldCategory == FieldsTableDataTable.FieldCategory.Items)
                    {
                        bindingName = b.Field.Name;
                        if (OrderProductsTable.Columns.IndexOf(bindingName) < 0)
                        {
                            bindingName = bindingName.Replace("/", "");
                            foreach (DataColumn col in OrderProductsTable.Columns)
                            {
                                if ((col.ColumnName.ToLower() == bindingName.ToLower().Replace(" ", "")))
                                {
                                    if (OrderProductsTable.Columns.IndexOf(b.Field.Name) < 0)
                                        col.ColumnName = b.Field.Name;

                                }
                            }
                        }
                        else
                        {
                            bindingName = bindingName.Replace("/", "");
                            if (OrderProductsTable.Columns.IndexOf(bindingName.ToLower().Replace(" ", "")) >= 0)
                            {
                                foreach (OrderProductsTableRow r in OrderProductsTable)
                                {
                                    r[b.Field.Name] = r[bindingName.ToLower().Replace(" ", "")];
                                }
                            }
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error renaming Products columns for Informix databases", ex);
            }
        }

        private void RenamePackagesColumns()
        {
            try
            {
                if ((OrderPackagesTable.Columns.IndexOf("order") >= 0) || (OrderPackagesTable.Columns.IndexOf("orderid") >= 0))
                {
                    OrderPackagesTable.Columns["Order #"].ReadOnly = false;
                    foreach (OrderPackagesTableRow r in OrderPackagesTable.Rows)
                    {
                        r["Order #"] = (OrderPackagesTable.Columns.IndexOf("order") >= 0) ? r["order"] : r["orderid"];
                    }
                    OrderPackagesTable.Columns["Order #"].ReadOnly = true;
                }

                if ((OrderPackagesTable.Columns.IndexOf("package") >= 0) || (OrderPackagesTable.Columns.IndexOf("packageid") >= 0))
                {
                    OrderPackagesTable.Columns["Package #"].ReadOnly = false;
                    foreach (OrderPackagesTableRow r in OrderPackagesTable.Rows)
                    {
                        r["Package #"] = (OrderPackagesTable.Columns.IndexOf("package") >= 0) ? r["package"] : r["packageid"];
                    }
                    OrderPackagesTable.Columns["Package #"].ReadOnly = true;
                }
                string bindingName = "";
                foreach (Binding b in Template.Bindings)
                {
                    if (b.Field.FieldCategory == FieldsTableDataTable.FieldCategory.Packages || b.Field.FieldCategory == FieldsTableDataTable.FieldCategory.Skids)
                    {
                        bindingName = b.Field.Name;
                        if (OrderPackagesTable.Columns.IndexOf(bindingName) < 0)
                        {
                            bindingName = bindingName.Replace("/", "");
                            foreach (DataColumn col in OrderPackagesTable.Columns)
                            {
                                if ((col.ColumnName.ToLower() == bindingName.ToLower().Replace(" ", "")))
                                {
                                    if (OrderPackagesTable.Columns.IndexOf(b.Field.Name) < 0)
                                        col.ColumnName = b.Field.Name;

                                }
                            }
                        }
                        else
                        {
                            bindingName = bindingName.Replace("/", "");
                            if (OrderPackagesTable.Columns.IndexOf(bindingName.ToLower().Replace(" ", "")) >= 0)
                            {
                                foreach (OrderPackagesTableRow r in OrderPackagesTable)
                                {
                                    r[b.Field.Name] = r[bindingName.ToLower().Replace(" ", "")];
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error renaming Packages columns for Informix databases", ex);
            }
        }

        private void RenameOrdersColumns()
        {
            try
            {
                if ((OrdersTable.Columns.IndexOf("order") >= 0) || (OrdersTable.Columns.IndexOf("orderid") >= 0))
                {
                    OrdersTable.Columns["Order #"].ReadOnly = false;
                    foreach (OrdersTableRow r in OrdersTable.Rows)
                    {
                        r["Order #"] = (OrdersTable.Columns.IndexOf("order") >= 0) ? r["order"] : r["orderid"];
                    }
                    OrdersTable.Columns["Order #"].ReadOnly = true;
                }
                string log = ".\\Uploader\\Errors\\UploadLog.txt";

                string bindingName = "";
                foreach (Binding b in Template.Bindings)
                {
                    if (b.Field.FieldCategory == FieldsTableDataTable.FieldCategory.Orders || b.Field.FieldCategory == FieldsTableDataTable.FieldCategory.Senders || b.Field.FieldCategory == FieldsTableDataTable.FieldCategory.Recipients)
                    {
                        bindingName = b.Field.Name;
                        if (OrdersTable.Columns.IndexOf(bindingName) < 0)
                        {
                            bindingName = bindingName.Replace("/", "");
                            foreach (DataColumn col in OrdersTable.Columns)
                            {
                                if ((col.ColumnName.ToLower() == bindingName.ToLower().Replace(" ", "")))
                                {
                                    Utils.WriteLine(log, "Found: " + col.ColumnName);
                                    if (OrdersTable.Columns.IndexOf(b.Field.Name) < 0)
                                        col.ColumnName = b.Field.Name;
                                }
                            }
                        }
                        else
                        {

                            bindingName = bindingName.Replace("/", "");
                            if (OrdersTable.Columns.IndexOf(bindingName.ToLower().Replace(" ", "")) >= 0)
                            {

                                foreach (OrdersTableRow r in OrdersTable)
                                {
                                    Utils.WriteLine(log, "Replace column name to integr8 one: " + b.Field.Name);
                                    if (bindingName.Contains("Ship Date") && Template.Name.Contains("BodyPlus"))
                                    {
                                        string date = r[bindingName.ToLower().Replace(" ", "")].ToString();
                                        r[b.Field.Name] = date.Length >= 8 ? (new DateTime(Convert.ToInt32(date.Substring(0, 4)), Convert.ToInt32(date.Substring(4, 2)), Convert.ToInt32(date.Substring(6, 2)))) : DateTime.Today;
                                    }
                                    else
                                    {
                                        r[b.Field.Name] = r[bindingName.ToLower().Replace(" ", "")];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error renaming Orders columns for non sql databases", ex);
            }
        }

        public void getTotalWeight(OrdersTableRow oldOrder, out decimal totalWeight, out int packs)
        {
            decimal weight = 0;
            totalWeight = 0;
            packs = 0;
            foreach (Data.OrderPackagesTableRow pack in oldOrder.GetOrderPackagesTableRows())
            {
                Decimal.TryParse(pack["Package Weight"].ToString(), out weight);
                totalWeight += weight;
                packs++;
            }
        }

        private Data.ShipmentsTableDataTable BackupUnsavedShipmentsForFilteredOrders()
        {
            Data.ShipmentsTableDataTable oldShipmentsTable = (ShipmentsTableDataTable)ShipmentsTable.Copy();
            if (!String.IsNullOrEmpty(Select_Filter.Filter_With_Status))
            {   //if reload button is pressed, and there are unsaved shipments
                if (ShipmentsTable.Rows.Count > 0)
                {
                    if (OrdersTable.Columns.IndexOf("Order number") > -1)
                    {
                        if (!(oldShipmentsTable.Columns.IndexOf("OrderNumber") > -1))
                            oldShipmentsTable.Columns.Add("OrderNumber");
                        for (int i = 0; i < oldShipmentsTable.Rows.Count; i++)
                        {
                            ShipmentsTableRow shp = (ShipmentsTableRow)oldShipmentsTable.Rows[i];

                            OrdersTableRow r = (OrdersTableRow)OrdersTable.FindBy_Order__(shp["Order #"].ToString());
                            if (r != null)
                            {
                                if (OrdersTable.Columns.IndexOf("OrderNumber") < 0)
                                {
                                    shp["OrderNumber"] = r["Order number"];
                                    oldShipmentsTable.Rows[i]["OrderNumber"] = shp["OrderNumber"];
                                }
                                else
                                {
                                    shp["OrderNumber"] = r["OrderNumber"];
                                    oldShipmentsTable.Rows[i]["OrderNumber"] = shp["OrderNumber"];
                                }
                            }
                        }
                    }
                }

            }
            return oldShipmentsTable;
        }

        public void BuildRequest(string order_number)
        {
            // construct webservice requests
            try
            {
                FieldsTableRow field;
                OrderPackagesTableRow[] packages;
                OrderProductsTableRow[] products;
                OrdersSkidsTableRow[] skids;
                OrderLTLItemsRow[] LTLItems;
                string value;
                RequestsTableRow row;

                OrdersTableRow order = OrdersTable.FindBy_Order__(order_number);
                if (order == null)
                    throw new ArgumentException("order not found");
                string carrier = order["Carrier"].ToString();
                // add order
                foreach (DataColumn col in OrdersTable.Columns)
                {
                    field = FieldsTable.FindByName(col.ColumnName);

                    if (field == null || field.Id < 0)
                        continue;
                    if (order.IsNull(col))
                        continue;
                    //*column Carrier has in database the value DELIVERY and an int is expected
                    if (col.ColumnName == "Carrier")
                    {
                        continue;
                    }
                    if ((col.ColumnName == "Shipment Reference Info") && (order[col].ToString().Equals("")))
                    {
                        continue;
                    }
                    if ((col.ColumnName == "Shipment PO Number") && (order[col].ToString().Equals("")))
                    {
                        continue;
                    }
                    value = order[col].ToString();
                    if (value.Equals(""))
                        continue;
                    // set the ship date to the current date and not the create date existent in order row
                    if (col.ColumnName.Replace(" ", "") == "ShipDate")
                    {
                        DateTime day = DateTime.Parse(value);
                        value = getNextShipDate(day);
                        string[] s = value.Split('-');
                        DateTime t = new DateTime(Convert.ToInt32(s[2]), Convert.ToInt32(s[0]), Convert.ToInt32(s[1]));
                        value = t.ToShortDateString();
                    }
                    row = RequestsTable.FindBy_Order__FieldIndex(order_number, field.Id, 0);
                    if (row == null)
                        RequestsTable.AddRequestsTableRow(order, field, 0, value);
                    else
                        row.Value = value;
                }

                // add packages

                packages = order.GetOrderPackagesTableRows();

                for (int p = 0; p < packages.Length; p++)

                    foreach (DataColumn col in OrderPackagesTable.Columns)
                    {
                        field = FieldsTable.FindByName(col.ColumnName);
                        if (field == null || field.Id < 0) continue;
                        if (packages[p].IsNull(col)) continue;
                        if ((col.ColumnName == "Package Reference Info 1") && (packages[p][col].ToString().Equals("")))
                        {
                            continue;
                        }
                        if ((col.ColumnName == "Package Reference Info 2") && (packages[p][col].ToString().Equals("")))
                        {
                            continue;
                        }
                        if ((col.ColumnName == "Package PO Number") && (packages[p][col].ToString().Equals("")))
                        {
                            continue;
                        }
                        value = packages[p][col].ToString();
                        if (value.Equals("")) continue;

                        row = RequestsTable.FindBy_Order__FieldIndex(order_number, field.Id, p);

                        if (row == null) RequestsTable.AddRequestsTableRow(order, field, p, value);
                        else row.Value = value;
                    }

                //add Skids info to request (needed to rate both with packages and skids)
                // add Items info if current carrier needs it
                if (CarrierHasItems(carrier))
                {
                    LTLItems = order.GetOrderLTLItemsRows();
                    for (int p = 0; p < LTLItems.Length; p++)
                        foreach (DataColumn col in OrderLTLItems.Columns)
                        {
                            field = FieldsTable.FindByName(col.ColumnName);
                            if (field == null || field.Id < 0) continue;
                            if (LTLItems[p].IsNull(col)) continue;
                            value = LTLItems[p][col].ToString();
                            if (value.Equals("")) continue;
                            row = RequestsTable.FindBy_Order__FieldIndex(order_number, field.Id, p);

                            if (row == null) RequestsTable.AddRequestsTableRow(order, field, p, value);
                            else row.Value = value;
                        }
                }
                else
                {
                    skids = order.GetOrdersSkidsTableRows();
                    for (int p = 0; p < skids.Length; p++)
                        foreach (DataColumn col in OrdersSkidsTable.Columns)
                        {
                            field = FieldsTable.FindByName(col.ColumnName);
                            if (field == null || field.Id < 0) continue;
                            if (skids[p].IsNull(col)) continue;
                            if ((col.ColumnName == "Shipment Reference") && (skids[p][col].ToString().Equals("")))
                            {
                                continue;
                            }
                            if ((col.ColumnName == "ShipmentPONumber") && (skids[p][col].ToString().Equals("")))
                            {
                                continue;
                            }
                            value = skids[p][col].ToString();
                            if (value.Equals("")) continue;
                            row = RequestsTable.FindBy_Order__FieldIndex(order_number, field.Id, p);

                            if (row == null) RequestsTable.AddRequestsTableRow(order, field, p, value);
                            else row.Value = value;
                        }
                }
                // add products (documents / commodities)
                products = order.GetOrderProductsTableRows();
                for (int p = 0; p < products.Length; p++)
                    foreach (DataColumn col in OrderProductsTable.Columns)
                    {
                        field = FieldsTable.FindByName(col.ColumnName);
                        if (field == null || field.Id < 0) continue;
                        if (products[p].IsNull(col)) continue;
                        value = products[p][col].ToString();
                        if (col.ColumnName == "Product Quantity" && OrderProductsTable.Columns.Contains("Scanned Items"))
                        {
                            value = products[p]["Scanned Items"].ToString();
                        }
                        if (value.Equals("")) continue;
                        row = RequestsTable.FindBy_Order__FieldIndex(order_number, field.Id, p);
                        if (row == null) RequestsTable.AddRequestsTableRow(order, field, p, value);
                        else row.Value = value;
                    }

                row = RequestsTable.FindBy_Order__FieldIndex(order_number, 75, 0);
                if (row != null)
                    RequestsTable.RemoveRequestsTableRow(row);

                if (!Template.MPSPackageLevel)
                {
                    // there should be only one (package row, of course, not highlander)
                    if (packages.Length != 1)
                        throw new Exception("Internal error - there should be only one package row.");

                    row = RequestsTable.FindBy_Order__FieldIndex(order_number, 77, 0);
                    if (row != null)
                        RequestsTable.RemoveRequestsTableRow(row);
                    RequestsTable.AddRequestsTableRow(order, FieldsTable.FindById(77), 0, packages[0]["Total Weight"].ToString());

                    // Packages Level MPS Info
                    RequestsTable.AddRequestsTableRow(order, FieldsTable.FindById(75), 0, "false");
                }
                else
                {
                    // Packages Level MPS Info
                    RequestsTable.AddRequestsTableRow(order, FieldsTable.FindById(75), 0, "true");
                }
            }
            catch (Exception e)
            {
                Utils.SendError("Error in Data.LoadTemplate", "Unable to construct webservice requests for order '" + order_number + "'.", e);
                throw new Exception("Internal error. Unable to construct webservice requests.", e);
            }
        }

        public string getNextShipDate(DateTime shipDay)
        {
            DayOfWeek weekDay = (DayOfWeek)shipDay.DayOfWeek;
            int intDayOfWeek = 0;
            switch (weekDay)
            {
                case DayOfWeek.Monday:
                    {
                        intDayOfWeek = 0;
                        break;
                    }
                case DayOfWeek.Tuesday:
                    {
                        intDayOfWeek = 1;
                        break;
                    }
                case DayOfWeek.Wednesday:
                    {
                        intDayOfWeek = 2;
                        break;
                    }
                case DayOfWeek.Thursday:
                    {
                        intDayOfWeek = 3;
                        break;
                    }
                case DayOfWeek.Friday:
                    {
                        intDayOfWeek = 4;
                        break;
                    }
                case DayOfWeek.Saturday:
                    {
                        intDayOfWeek = 5;
                        break;
                    }
                case DayOfWeek.Sunday:
                    {
                        intDayOfWeek = 6;
                        break;
                    }
            }
            if (!PickUpDays[intDayOfWeek])
            {
                int nextPickUp = intDayOfWeek;
                while (!PickUpDays[nextPickUp] && nextPickUp < 7)
                    nextPickUp++;
                if (nextPickUp < 7)
                    shipDay = shipDay.AddDays(nextPickUp - intDayOfWeek);
                else
                {
                    nextPickUp = 0;
                    while (!PickUpDays[nextPickUp])
                        nextPickUp++;
                    shipDay = shipDay.AddDays(6 - intDayOfWeek + nextPickUp + 1);
                }
            }
            return shipDay.ToString("MM-dd-yyyy");
        }

        public void SaveSerial(string order)
        {
            using (OdbcConnection Conn = new OdbcConnection(Template.WriteConn.ConnStr))
            {
                Conn.Open();

                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;
                    Comm.CommandText = "insert into ship_l_serial values (?, seq_gen.NEXTVAL, ?, ?);";
                    Data.ProductsSerialNumbersRow[] rows = InteGr8_Main.data.ProductsSerialNumbers.Select("[Order #] = '" + order + "'") as Data.ProductsSerialNumbersRow[];
                    foreach (Data.ProductsSerialNumbersRow r in rows)
                    {
                        Comm.Parameters.Add(new OdbcParameter("", r.Serial_Id));
                        Comm.Parameters.Add(new OdbcParameter("", r.Serial_Number));
                        Comm.Parameters.Add(new OdbcParameter("", r.item_num));
                        Comm.ExecuteNonQuery();
                        Comm.Parameters.Clear();
                        BackupFile.RemoveShipment(InteGr8_Main.data.ProductsSerialNumbers.TableName, "deleted", order, InteGr8_Main.data);
                    }
                }
            }
        }

        // executes the export commands from the given template
        public Dictionary<Data.ShipmentsTableRow, string> SaveTemplate(Data.ShipmentsTableRow[] temp)
        {
            Dictionary<Data.ShipmentsTableRow, string> temp_error = new Dictionary<ShipmentsTableRow, string>();
            //we have to check if to insert a new row or update an existing one (check if row exists in db)
            if (Template.Check_Save)
            {
                List<Data.ShipmentsTableRow> temp_update = new List<ShipmentsTableRow>();
                List<Data.ShipmentsTableRow> temp_insert = new List<ShipmentsTableRow>();
                //checks in database, if it has to insert or update for each shipments table row
                CheckSave_Shipments(temp, temp_error, temp_update, temp_insert);
                File.AppendAllText("Error.txt", "In save template");

                if (!String.IsNullOrEmpty(Template.Check_Delete))
                {
                    ExecuteDelete(temp_update.ToArray(), ref temp_error);
                    ExecuteInsert(temp_update.ToArray(), ref temp_error);
                }
                else
                {
                    ExecuteUpdate(temp_update.ToArray(), ref temp_error);
                }

                ExecuteInsert(temp_insert.ToArray(), ref temp_error);
            }
            else
            {
                ExecuteInsert(temp, ref temp_error);
            }
            if (!Properties.Settings.Default.WriteTo.Equals("") && Template.Name.Equals("Chocolate Inn"))
            {
                StringBuilder s = new StringBuilder(File.ReadAllText(Properties.Settings.Default.WriteTo));
                s = s.Replace("\"", "");
                File.WriteAllText(Properties.Settings.Default.WriteTo, s.ToString());
                //string text = File.ReadAllText(Properties.Settings.Default.WriteTo);
                //text = text.Replace("\"", "");
                //File.WriteAllText(Properties.Settings.Default.WriteTo, text);
            }
            return temp_error;
        }

        private void CheckSave_Shipments(Data.ShipmentsTableRow[] temp, Dictionary<Data.ShipmentsTableRow, string> temp_error, List<Data.ShipmentsTableRow> temp_update, List<Data.ShipmentsTableRow> temp_insert)
        {
            ShipmentsTableDataTable newShipmentsTable = (ShipmentsTableDataTable)ShipmentsTable.Copy();
            newShipmentsTable.Rows.Clear();

            foreach (ShipmentsTableRow r in temp)
            {
                newShipmentsTable.Rows.Add(r.ItemArray);
            }

            using (OdbcConnection Conn = new OdbcConnection(Template.WriteConn.ConnStr))
            {
                Conn.Open();

                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.CommandText = Template.Save_Select;
                    Comm.Connection = Conn;
                    foreach (Binding Binding in Template.Bindings)
                    {
                        if ((Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments) || (Binding.Field.Name.Contains("Recipient")) || (Binding.Field.Name.Equals("Order #") && (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Orders)))
                        {
                            if (Comm.CommandText.Contains("@" + Binding.Field.Name.Replace(" ", "")))
                            {
                                Comm.CommandText = Comm.CommandText.Replace("@" + Binding.Field.Name.Replace(" ", ""), "?");
                                Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100);
                            }
                        }
                    }
                    foreach (ShipmentsTableRow r in newShipmentsTable.Rows)
                    {
                        foreach (OdbcParameter p in Comm.Parameters)
                        {
                            string name = "";

                            foreach (DataColumn c in newShipmentsTable.Columns)
                            {
                                if (c.ColumnName.Replace(" ", "") == (p.ParameterName[0] == '@' ? p.ParameterName.Substring(1) : p.ParameterName))
                                {
                                    name = c.ColumnName;
                                    break;
                                }
                            }
                            if (p.ParameterName == "@SCAC")
                            {
                                name = p.ParameterName.Replace("@", "");
                            }
                            if (name == "")
                            {
                                if (p.ParameterName[p.ParameterName.Length - 1] == '#')
                                {
                                    name = p.ParameterName.Substring(1, p.ParameterName.Length - 2) + " #";
                                }
                                else
                                {
                                    name = p.ParameterName.Substring(1);
                                }
                            }

                            if (p.ParameterName == "ShippingDate")
                            {
                                DateTime date = Convert.ToDateTime(r[name].ToString());
                                if (!Template.WriteConn.SupportsDelete)
                                {
                                    p.Value = date;
                                }
                                else
                                {
                                    p.Value = date.ToString("yyyy''MM''dd");
                                }
                            }
                            else
                            {
                                if (p.ParameterName == "@SCAC")
                                {
                                    Data.SCACSRow[] scac = SCACS.Select("Carrier_Code = '" + r.Carrier_Code + "' AND Service_Code = '" + r.Service_Code + "'") as Data.SCACSRow[];
                                    if (scac.Length == 0)
                                    {
                                        scac = SCACS.Select("Carrier_Code = '" + r.Carrier_Code + "' AND Service_Code = ''") as Data.SCACSRow[];
                                    }
                                    p.Value = scac[0].SCAC_Code;
                                }
                                else
                                {
                                    p.Value = r[name].ToString().TrimEnd();
                                }
                            }

                            if (Template.Name == "" && p.ParameterName == "ShipmentTrackingNumber")
                            {
                                if (p.Value.ToString().Length <= 15)
                                {
                                    while (p.Value.ToString().Length != 15)
                                    {
                                        p.Value += " ";
                                    }
                                }
                                else
                                {
                                    p.Value = p.Value.ToString().Substring(p.Value.ToString().Length - 15, 15);
                                }
                            }
                        }
                        try
                        {
                            OdbcDataReader reader = Comm.ExecuteReader();
                            reader.Read();
                            int count = reader.GetInt32(0);
                            if (count > 0)
                            {
                                temp_update.Add(r);

                            }
                            else
                            {
                                temp_insert.Add(r);
                            }
                            reader.Close();
                        }
                        catch (Exception e)
                        {
                            temp_error.Add(r, e.Message);
                            if (r.Package_Index == 0)
                            {
                                Data.ShipmentsTableRow row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index);
                                row.Status = "Error at Save";
                                ShipmentsTable.Columns["Error Message"].ReadOnly = false;
                                row.Error_Message = e.Message;
                                ShipmentsTable.Columns["Error Message"].ReadOnly = true;
                                Utils.SendClientEmail("Error saving order: " + r._Order__, e.Message);
                                Utils.SendError("Error saving order: " + r._Order__, e);

                            }
                            else
                            {
                                Data.ShipmentsTableRow row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, 0);
                                row.Status = "Error at Save";
                                ShipmentsTable.Columns["Error Message"].ReadOnly = false;
                                row.Error_Message = e.Message;
                                row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index);
                                row.Status = "Error at Save";
                                row.Error_Message = e.Message;
                                ShipmentsTable.Columns["Error Message"].ReadOnly = true;
                            }
                        }
                        foreach (OdbcParameter p in Comm.Parameters)
                        {
                            p.Value = "";
                        }

                    }
                }
            }
        }

        private void ExecuteUpdate(Data.ShipmentsTableRow[] temp, ref Dictionary<Data.ShipmentsTableRow, string> temp_error)
        {
            using (OdbcConnection Conn = new OdbcConnection(Template.WriteConn.ConnStr))
            {
                Conn.Open();

                using (OdbcCommand Comm = new OdbcCommand())
                {
                    {
                        List<Data.ShipmentsTableRow> remove = new List<Data.ShipmentsTableRow>();
                        remove.AddRange(temp);
                        if (!Template.SaveShipments && !Template.SaveShipmentPackages)
                            return;

                        if (!Template.SaveShipmentPackages)
                        {
                            for (int i = temp.Length - 1; i >= 0; i--)
                            {
                                if (temp[i]["Package_Index"].ToString() != "0")
                                {
                                    ShipmentsTable.FindBy_Order__Package_Index(remove[i]._Order__, remove[i].Package_Index)["Status"] = "Committed";
                                    temp[i].Delete();
                                }
                            }
                        }

                        if (!Template.SaveShipments)
                        {
                            for (int i = remove.Count - 1; i >= 0; i--)
                            {
                                if (remove[i]["Package_Index"].ToString() == "0")
                                {
                                    bool ok = false;
                                    foreach (Data.ShipmentsTableRow r in remove)
                                    {
                                        if (r["OrderNumber"] == remove[i]["OrderNumber"] && r.Package_Index != remove[i].Package_Index)
                                        {
                                            ok = true;
                                            break;
                                        }
                                    }
                                    if (ok)
                                    {
                                        ShipmentsTable.FindBy_Order__Package_Index(remove[i]._Order__, remove[i].Package_Index)["Status"] = "Committed";
                                        remove.RemoveAt(i);
                                    }
                                }
                            }
                        }

                        temp = remove.ToArray();
                        // replace the parameters with odbc style ? parameters
                        string sql = Shipments_Insert_Statement.Update;
                        string checkSql = sql.Substring(sql.IndexOf("set"), sql.Length - sql.IndexOf("set") - (sql.Length - sql.IndexOf("where")));
                        foreach (Binding Binding in Template.Bindings)
                        {
                            if ((Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments) || (Binding.Field.Name.Contains("Recipient")) || (Binding.Field.Name.Equals("Order #") && (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Orders)))
                            {
                                if (checkSql.Contains("@" + Binding.Field.Name.Replace(" ", "")))
                                {
                                    sql = sql.Replace("@" + Binding.Field.Name.Replace(" ", ""), "?");
                                }
                            }
                        }

                        if (sql.Contains("@SCAC"))
                        {
                            sql = sql.Replace("@SCAC", "?");
                            Comm.Parameters.Add("@SCAC", OdbcType.VarChar, 100);
                        }

                        Comm.Connection = Conn;
                        Comm.CommandText = sql;
                        // map the insert parameters to the table columns
                        foreach (Binding Binding in Template.Bindings)
                        {
                            //Order # is found multiple times in Bindings,(of type Orders, Packages and Products) and it will be added too many times in parameters list
                            if ((Binding.Field.Name.Equals("Order #") && (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Orders)) || (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments && (!String.IsNullOrEmpty(Binding.Column) || checkSql.IndexOf(Binding.Field.Name.Replace(" ", "")) > -1)))
                            {
                                //add only the parameters required by the 2Ship_update procedure
                                if (checkSql.IndexOf(Binding.Field.Name.Replace(" ", "")) > -1)
                                {
                                    if (!Properties.Settings.Default.ClientSpecialRates.Equals(""))
                                    {
                                        //informix db type, the parameters need no @ in front of the parametere name
                                        Comm.Parameters.Add(Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                    else
                                    {
                                        Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                }
                                if (ShipmentsTable.Columns[Binding.Field.Name] == null)
                                    // this column does not exist (bc this field is missing from the reply), add it now - otherwise the adapter will fail
                                    ShipmentsTable.Columns.Add(Binding.Field.Name, typeof(String)).MaxLength = 255;
                            }
                        }
                        //add recipient address to parameters if they are required
                        foreach (Binding Binding in Template.Bindings)
                        {
                            if (Binding.Field.Name.Contains("Recipient"))
                            {
                                //add only the parameters required by the 2Ship_update procedure
                                if (checkSql.IndexOf(Binding.Field.Name.Replace(" ", "")) > -1)
                                {
                                    if (!Properties.Settings.Default.ClientSpecialRates.Equals(""))
                                    {
                                        Comm.Parameters.Add(Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                    else
                                    {
                                        Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                }
                                if (ShipmentsTable.Columns[Binding.Field.Name] == null)
                                    // this column does not exist (bc this field is missing from the reply), add it now - otherwise the adapter will fail
                                    ShipmentsTable.Columns.Add(Binding.Field.Name, typeof(String)).MaxLength = 255;
                            }
                        }

                        //****************************************************************************************************
                        using (StreamWriter write = File.AppendText("ReplyTest.txt"))
                        {
                            write.WriteLine("ODBC Command text is: ");
                            write.WriteLine(Comm.CommandText.ToString());
                            write.WriteLine("Command parameters form:");
                            for (int i = 0; i < Comm.Parameters.Count; i++)
                            {
                                object oValue = Comm.Parameters[i].Value;
                                string value;
                                if (oValue == DBNull.Value)
                                    value = "DBNull.Value";
                                else
                                    if (oValue == null)
                                    value = "null";
                                else
                                    value = Comm.Parameters[i].Value.ToString();

                                string name = Comm.Parameters[i].ToString();
                                //System.Diagnostics.Debug.WriteLine(name + " : " + value);
                                write.WriteLine(name + " : " + value);
                            }
                        }
                        //****************************************************************************************************

                        foreach (Binding Binding in Template.Bindings)
                        {
                            if ((Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments) || (Binding.Field.Name.Contains("Recipient")) || (Binding.Field.Name.Equals("Order #") && (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Orders)))
                            {
                                if (Comm.CommandText.Contains("@" + Binding.Field.Name.Replace(" ", "")))
                                {
                                    Comm.CommandText = Comm.CommandText.Replace("@" + Binding.Field.Name.Replace(" ", ""), "?");
                                    Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100);
                                }
                            }
                            if (!Comm.CommandText.Contains("@"))
                            {
                                break;
                            }
                        }



                        ShipmentsTableDataTable newShipmentsTable = (ShipmentsTableDataTable)ShipmentsTable.Copy();
                        newShipmentsTable.Rows.Clear();

                        foreach (ShipmentsTableRow r in temp)
                        {
                            Data.ShipmentsTableRow row_test = newShipmentsTable.Rows.Add(r.ItemArray) as Data.ShipmentsTableRow;
                            if (Template.Name == "")
                            {
                                if (row_test.Shipment_Tracking_Number.Substring(1, 8) == "42153269")
                                {
                                    row_test.Table.Columns["Shipment Tracking Number"].ReadOnly = false;
                                    row_test.Shipment_Tracking_Number = row_test.Shipment_Tracking_Number.Substring(0, 1) + row_test.Shipment_Tracking_Number.Substring(9, row_test.Shipment_Tracking_Number.Length - 9);
                                    row_test.Table.Columns["Shipment Tracking Number"].ReadOnly = true;
                                }
                            }
                        }

                        for (int i = newShipmentsTable.Rows.Count - 1; i >= 0; i--)
                        {
                            if (newShipmentsTable.Rows[i]["Status"].ToString().Equals("Error"))
                            {
                                newShipmentsTable.Rows.Remove(newShipmentsTable.Rows[i]);
                            }
                        }



                        foreach (ShipmentsTableRow r in newShipmentsTable.Rows)
                        {
                            foreach (OdbcParameter p in Comm.Parameters)
                            {
                                string name = "";
                                foreach (DataColumn c in newShipmentsTable.Columns)
                                {
                                    if (c.ColumnName.Replace(" ", "") == (p.ParameterName[0] == '@' ? p.ParameterName.Substring(1) : p.ParameterName))
                                    {
                                        name = c.ColumnName;
                                        break;
                                    }
                                }
                                if (p.ParameterName == "@SCAC")
                                {
                                    name = p.ParameterName.Replace("@", "");
                                }
                                if (name == "")
                                {
                                    if (p.ParameterName[p.ParameterName.Length - 1] == '#')
                                    {
                                        name = p.ParameterName.Substring(1, p.ParameterName.Length - 2) + " #";
                                    }
                                    else
                                    {
                                        name = p.ParameterName.Substring(1);
                                    }
                                }

                                if (p.ParameterName == "@ShippingDate")
                                {
                                    DateTime date = Convert.ToDateTime(r[name].ToString());
                                    if (!Template.WriteConn.SupportsDelete)
                                    {
                                        p.Value = date.ToString("yyyy/MM/dd").Replace("/", "").Replace("-", "").Substring(0, 8);
                                        File.AppendAllText("Error.txt", "ShippingDate value before insert: " + p.Value.ToString());
                                    }
                                    else
                                    {
                                        p.Value = date.ToString("yyyy''MM''dd");
                                    }
                                }
                                else
                                {
                                    if (p.ParameterName == "@SCAC")
                                    {
                                        Data.SCACSRow[] scac = SCACS.Select("Carrier_Code = '" + r.Carrier_Code + "' AND Service_Code = '" + r.Service_Code + "'") as Data.SCACSRow[];
                                        if (scac.Length == 0)
                                        {
                                            scac = SCACS.Select("Carrier_Code = '" + r.Carrier_Code + "' AND Service_Code = ''") as Data.SCACSRow[];
                                        }
                                        p.Value = scac[0].SCAC_Code;
                                    }
                                    else
                                    {
                                        p.Value = r[name].ToString().TrimEnd();
                                    }
                                }

                                if (Template.Name == "" && p.ParameterName == "ShipmentTrackingNumber")
                                {
                                    if (p.Value.ToString().Length <= 15)
                                    {
                                        while (p.Value.ToString().Length != 15)
                                        {
                                            p.Value += " ";
                                        }
                                    }
                                    else
                                    {
                                        p.Value = p.Value.ToString().Substring(p.Value.ToString().Length - 15, 15);
                                    }
                                }
                            }
                            try
                            {
                                Comm.ExecuteNonQuery();
                            }
                            catch (Exception e)
                            {
                                temp_error.Add(r, e.Message);
                                if (r.Package_Index == 0)
                                {
                                    Data.ShipmentsTableRow row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index);
                                    row.Status = "Error at Save";
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = false;
                                    row.Error_Message = e.Message;
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = true;
                                    Utils.SendClientEmail("Error saving order: " + r._Order__, e.Message);
                                    Utils.SendError("Error saving order: " + r._Order__, e);
                                }
                                else
                                {
                                    Data.ShipmentsTableRow row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, 0);
                                    row.Status = "Error at Save";
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = false;
                                    row.Error_Message = e.Message;
                                    row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index);
                                    row.Status = "Error at Save";
                                    row.Error_Message = e.Message;
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = true;
                                }
                                continue;
                            }

                            ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index)["Status"] = "Committed";

                            foreach (OdbcParameter p in Comm.Parameters)
                            {
                                p.Value = null;
                            }
                        }

                        for (int i = 0; i < newShipmentsTable.Rows.Count; i++)
                        {
                            if (!ShipmentsTable.Rows[i]["Status"].ToString().Contains("Error"))
                            {
                                ShipmentsTable.Rows[i]["Status"] = "Committed";
                            }
                        }
                    }
                }
            }
        }

        private void ExecuteInsert(Data.ShipmentsTableRow[] temp, ref Dictionary<Data.ShipmentsTableRow, string> temp_error)
        {
            using (OdbcConnection Conn = new OdbcConnection(Template.WriteConn.ConnStr))
            {
                Conn.Open();

                using (OdbcCommand Comm = new OdbcCommand())
                {
                    {
                        List<ShipmentsTableRow> remove = new List<ShipmentsTableRow>();
                        remove.AddRange(temp);


                        if (!Template.SaveShipments && !Template.SaveShipmentPackages)
                            return;

                        if (!Template.SaveShipmentPackages)
                        {
                            for (int i = temp.Length - 1; i >= 0; i--)
                            {
                                if (temp[i]["Package_Index"].ToString() != "0")
                                {
                                    ShipmentsTable.FindBy_Order__Package_Index(remove[i]._Order__, remove[i].Package_Index)["Status"] = "Committed";
                                    remove.Remove(temp[i]); //temp[i].Delete();
                                }
                            }
                        }

                        if (!Template.SaveShipments)
                        {
                            for (int i = remove.Count - 1; i >= 0; i--)
                            {
                                if (remove[i]["Package_Index"].ToString() == "0")
                                {
                                    bool ok = false;
                                    foreach (Data.ShipmentsTableRow r in remove)
                                    {
                                        if (!r.Table.Columns.Contains("OrderNumber"))
                                        {
                                            if (r._Order__.ToString().Equals(remove[i]._Order__.ToString()) && r.Package_Index != remove[i].Package_Index)
                                            {
                                                ok = true;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (r["OrderNumber"].ToString().Equals(remove[i]["OrderNumber"].ToString()) && r.Package_Index != remove[i].Package_Index)
                                            {
                                                ok = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (ok)
                                    {
                                        ShipmentsTable.FindBy_Order__Package_Index(remove[i]._Order__, remove[i].Package_Index)["Status"] = "Committed";
                                        remove.RemoveAt(i);
                                    }
                                }
                            }
                        }
                        File.AppendAllText("Error.txt", "In execute insert");

                        temp = remove.ToArray();
                        // replace the parameters with odbc style ? parameters
                        string sql = Shipments_Insert_Statement.Insert;
                        string checkSql = sql;
                        foreach (Binding Binding in Template.Bindings)
                            if ((Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments) || (Binding.Field.Name.Contains("Recipient")) || (Binding.Field.Name.Equals("Order #") && (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Orders)))
                                sql = sql.Replace('@' + Binding.Field.Name.Replace(" ", ""), "?");

                        if (sql.Contains("@SCAC"))
                        {
                            sql = sql.Replace("@SCAC", "?");
                            Comm.Parameters.Add("@SCAC", OdbcType.VarChar, 100);
                        }

                        Comm.Connection = Conn;
                        Comm.CommandText = sql;
                        // map the insert parameters to the table columns
                        foreach (Binding Binding in Template.Bindings)
                        {
                            //Order # is found multiple times in Bindings,(of type Orders, Packages and Products) and it will be added too many times in parameters list
                            if ((Binding.Field.Name.Equals("Order #") && (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Orders)) || (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments && (!String.IsNullOrEmpty(Binding.Column) || checkSql.IndexOf(Binding.Field.Name.Replace(" ", "")) > -1)))
                            {
                                //add only the parameters required by the 2Ship_update procedure
                                if (checkSql.IndexOf(Binding.Field.Name.Replace(" ", "")) > -1)
                                {
                                    if (!Properties.Settings.Default.ClientSpecialRates.Equals(""))
                                    {
                                        //informix db type, the parameters need no @ in front of the parametere name
                                        Comm.Parameters.Add(Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 255, Binding.Field.Name).IsNullable = true;
                                    }
                                    else
                                    {
                                        Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 255, Binding.Field.Name).IsNullable = true;
                                        //    Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                }
                                if (ShipmentsTable.Columns[Binding.Field.Name] == null)
                                    // this column does not exist (bc this field is missing from the reply), add it now - otherwise the adapter will fail
                                    ShipmentsTable.Columns.Add(Binding.Field.Name, typeof(String)).MaxLength = 255;
                            }
                        }
                        //add recipient address to parameters if they are required
                        foreach (Binding Binding in Template.Bindings)
                        {
                            if (Binding.Field.Name.Contains("Recipient"))
                            {
                                //add only the parameters required by the 2Ship_update procedure
                                if (checkSql.IndexOf(Binding.Field.Name.Replace(" ", "")) > -1)
                                {
                                    if (!Properties.Settings.Default.ClientSpecialRates.Equals(""))
                                    {
                                        Comm.Parameters.Add(Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                    else
                                    {
                                        Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                }
                                if (ShipmentsTable.Columns[Binding.Field.Name] == null)
                                    // this column does not exist (bc this field is missing from the reply), add it now - otherwise the adapter will fail
                                    ShipmentsTable.Columns.Add(Binding.Field.Name, typeof(String)).MaxLength = 255;
                            }
                        }

                        //****************************************************************************************************
                        using (StreamWriter write = File.AppendText("ReplyTest.txt"))
                        {
                            write.WriteLine("ODBC Command text is: ");
                            write.WriteLine(Comm.CommandText.ToString());
                            write.WriteLine("Command parameters form:");
                            for (int i = 0; i < Comm.Parameters.Count; i++)
                            {
                                object oValue = Comm.Parameters[i].Value;
                                string value;
                                if (oValue == DBNull.Value)
                                    value = "DBNull.Value";
                                else
                                    if (oValue == null)
                                    value = "null";
                                else
                                    value = Comm.Parameters[i].Value.ToString();

                                string name = Comm.Parameters[i].ToString();
                                //System.Diagnostics.Debug.WriteLine(name + " : " + value);
                                write.WriteLine(name + " : " + value);
                            }
                        }
                        //****************************************************************************************************

                        ShipmentsTableDataTable newShipmentsTable = (ShipmentsTableDataTable)ShipmentsTable.Copy();
                        newShipmentsTable.Rows.Clear();

                        foreach (ShipmentsTableRow r in temp)
                        {
                            Data.ShipmentsTableRow row_test = newShipmentsTable.Rows.Add(r.ItemArray) as Data.ShipmentsTableRow;
                            if (Template.Name == "")
                            {
                                if (row_test.Shipment_Tracking_Number.Substring(1, 8) == "42153269")
                                {
                                    row_test.Table.Columns["Shipment Tracking Number"].ReadOnly = false;
                                    row_test.Shipment_Tracking_Number = row_test.Shipment_Tracking_Number.Substring(0, 1) + row_test.Shipment_Tracking_Number.Substring(9, row_test.Shipment_Tracking_Number.Length - 9);
                                    row_test.Table.Columns["Shipment Tracking Number"].ReadOnly = true;
                                }
                            }
                        }

                        for (int i = newShipmentsTable.Rows.Count - 1; i >= 0; i--)
                        {
                            if (newShipmentsTable.Rows[i]["Status"].ToString().Equals("Error"))
                            {
                                newShipmentsTable.Rows.Remove(newShipmentsTable.Rows[i]);
                            }
                        }

                        foreach (ShipmentsTableRow r in newShipmentsTable.Rows)
                        {
                            foreach (OdbcParameter p in Comm.Parameters)
                            {
                                string name = "";
                                foreach (DataColumn c in newShipmentsTable.Columns)
                                {
                                    if (c.ColumnName.Replace(" ", "") == (p.ParameterName[0] == '@' ? p.ParameterName.Substring(1) : p.ParameterName))
                                    {
                                        name = c.ColumnName;
                                        break;
                                    }
                                }
                                //File.AppendAllText("Error.txt","Parameter name: " + p.ParameterName + " before insert: " + p.Value.ToString());

                                if (p.ParameterName == "@SCAC")
                                {
                                    name = p.ParameterName.Replace("@", "");
                                }
                                if (name == "")
                                {
                                    if (p.ParameterName[p.ParameterName.Length - 1] == '#')
                                    {
                                        name = p.ParameterName.Substring(1, p.ParameterName.Length - 2) + " #";
                                    }
                                    else
                                    {
                                        name = p.ParameterName.Substring(1);
                                    }
                                }
                                //don't change the ShippingDate parameter for ChocolateInn, it was already formatted in SaveReplyToShipments method
                                if (p.ParameterName == "@ShippingDate")
                                {
                                    if ((Template.WriteConn.SupportsDelete))
                                    {
                                        DateTime date = String.IsNullOrEmpty(r[name].ToString()) ? DateTime.Today : Convert.ToDateTime(r[name].ToString());
                                        p.Value = date.ToString("yyyy''MM''dd");
                                    }
                                    else
                                    {
                                        string sDate = String.IsNullOrEmpty(r[name].ToString()) ? DateTime.Today.ToString() : r[name].ToString();
                                        if (sDate.Contains("-") || sDate.Contains("/"))
                                        {
                                            DateTime dDate = Convert.ToDateTime(sDate);
                                            sDate = dDate.ToString("yyyy-MM-dd");
                                            p.Value = sDate.Replace("-", "").Replace("/", "").Substring(0, 8);
                                        }
                                        else
                                        {
                                            p.Value = sDate;
                                        }
                                    }
                                }
                                else
                                {
                                    if (p.ParameterName == "@SCAC")
                                    {
                                        Data.SCACSRow[] scac = SCACS.Select("Carrier_Code = '" + r.Carrier_Code + "' AND Service_Code = '" + r.Service_Code + "'") as Data.SCACSRow[];
                                        if (scac.Length == 0)
                                        {
                                            scac = SCACS.Select("Carrier_Code = '" + r.Carrier_Code + "' AND Service_Code = ''") as Data.SCACSRow[];
                                        }
                                        p.Value = scac[0].SCAC_Code;
                                    }
                                    else
                                    {
                                        if (Template.WriteConn.SupportsDelete)
                                        {
                                            if (name.Equals("Order Number") && Template.Name.Equals("Choquette"))
                                            {
                                                p.Value = r["Order #"];
                                            }
                                            else
                                            {
                                                p.Value = r[name].ToString().TrimEnd();
                                            }
                                        }
                                        else
                                        {
                                            p.Value = r[name];

                                        }
                                    }
                                }

                                if (Template.Name == "" && p.ParameterName == "ShipmentTrackingNumber")
                                {
                                    if (p.Value.ToString().Length <= 15)
                                    {
                                        while (p.Value.ToString().Length != 15)
                                        {
                                            p.Value += " ";
                                        }
                                    }
                                    else
                                    {
                                        p.Value = p.Value.ToString().Substring(p.Value.ToString().Length - 15, 15);
                                    }
                                }
                            }

                            try
                            {
                                //throw new ArgumentException();
                                Comm.ExecuteNonQuery();
                            }
                            catch (Exception e)
                            {
                                temp_error.Add(r, e.Message);
                                if (r.Package_Index == 0)
                                {
                                    Data.ShipmentsTableRow row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index);
                                    row.Status = "Error at Save";
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = false;
                                    row.Error_Message = e.Message;
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = true;
                                    Utils.SendClientEmail("Error saving order: " + r._Order__, e.Message);
                                    Utils.SendError("Error saving order: " + r._Order__, e);
                                }
                                else
                                {
                                    Data.ShipmentsTableRow row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, 0);
                                    row.Status = "Error at Save";
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = false;
                                    row.Error_Message = e.Message;
                                    row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index);
                                    row.Status = "Error at Save";
                                    row.Error_Message = e.Message;
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = true;
                                }
                                continue;
                            }

                            ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index)["Status"] = "Committed";

                            foreach (OdbcParameter p in Comm.Parameters)
                            {
                                p.Value = null;
                            }
                        }


                        for (int i = 0; i < newShipmentsTable.Rows.Count; i++)
                        {
                            if (!ShipmentsTable.Rows[i]["Status"].ToString().Contains("Error"))
                            {
                                ShipmentsTable.Rows[i]["Status"] = "Committed";
                            }
                        }
                    }
                }
            }

            //try to remove "" from fields in csv file


        }

        private void ExecuteDelete(Data.ShipmentsTableRow[] temp, ref Dictionary<Data.ShipmentsTableRow, string> temp_error)
        {
            using (OdbcConnection Conn = new OdbcConnection(Template.WriteConn.ConnStr))
            {
                Conn.Open();

                using (OdbcCommand Comm = new OdbcCommand())
                {
                    {
                        List<Data.ShipmentsTableRow> remove = new List<Data.ShipmentsTableRow>();
                        remove.AddRange(temp);
                        if (!Template.SaveShipments && !Template.SaveShipmentPackages)
                            return;

                        if (!Template.SaveShipmentPackages)
                        {
                            for (int i = temp.Length - 1; i >= 0; i--)
                            {
                                if (temp[i]["Package_Index"].ToString() != "0")
                                {
                                    ShipmentsTable.FindBy_Order__Package_Index(remove[i]._Order__, remove[i].Package_Index)["Status"] = "Committed";
                                    remove.Remove(temp[i]); //temp[i].Delete();
                                }
                            }
                        }

                        if (!Template.SaveShipments)
                        {
                            for (int i = remove.Count - 1; i >= 0; i--)
                            {
                                if (remove[i]["Package_Index"].ToString() == "0")
                                {
                                    bool ok = false;
                                    foreach (Data.ShipmentsTableRow r in remove)
                                    {
                                        if (r["OrderNumber"].ToString().Equals(remove[i]["OrderNumber"].ToString()) && r.Package_Index != remove[i].Package_Index)
                                        {
                                            ok = true;
                                            break;
                                        }
                                    }
                                    if (ok)
                                    {
                                        ShipmentsTable.FindBy_Order__Package_Index(remove[i]._Order__, remove[i].Package_Index)["Status"] = "Committed";
                                        remove.RemoveAt(i);
                                    }
                                }
                            }
                        }

                        temp = remove.ToArray();
                        // replace the parameters with odbc style ? parameters
                        string sql = Shipments_Insert_Statement.Delete;
                        string checkSql = sql;
                        foreach (Binding Binding in Template.Bindings)
                            if ((Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments) || (Binding.Field.Name.Contains("Recipient")) || (Binding.Field.Name.Equals("Order #") && (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Orders)))
                                sql = sql.Replace('@' + Binding.Field.Name.Replace(" ", ""), "?");

                        if (sql.Contains("@SCAC"))
                        {
                            sql = sql.Replace("@SCAC", "?");
                            Comm.Parameters.Add("@SCAC", OdbcType.VarChar, 100);
                        }

                        Comm.Connection = Conn;
                        Comm.CommandText = sql;
                        // map the insert parameters to the table columns
                        foreach (Binding Binding in Template.Bindings)
                        {
                            //Order # is found multiple times in Bindings,(of type Orders, Packages and Products) and it will be added too many times in parameters list
                            if ((Binding.Field.Name.Equals("Order #") && (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Orders)) || (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments && (!String.IsNullOrEmpty(Binding.Column) || checkSql.IndexOf(Binding.Field.Name.Replace(" ", "")) > -1)))
                            {
                                //add only the parameters required by the 2Ship_update procedure
                                if (checkSql.IndexOf(Binding.Field.Name.Replace(" ", "")) > -1)
                                {
                                    if (!Properties.Settings.Default.ClientSpecialRates.Equals(""))
                                    {
                                        //informix db type, the parameters need no @ in front of the parametere name
                                        Comm.Parameters.Add(Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                    else
                                    {
                                        Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                }
                                if (ShipmentsTable.Columns[Binding.Field.Name] == null)
                                    // this column does not exist (bc this field is missing from the reply), add it now - otherwise the adapter will fail
                                    ShipmentsTable.Columns.Add(Binding.Field.Name, typeof(String)).MaxLength = 255;
                            }
                        }
                        //add recipient address to parameters if they are required
                        foreach (Binding Binding in Template.Bindings)
                        {
                            if (Binding.Field.Name.Contains("Recipient"))
                            {
                                //add only the parameters required by the 2Ship_update procedure
                                if (checkSql.IndexOf(Binding.Field.Name.Replace(" ", "")) > -1)
                                {
                                    if (!Properties.Settings.Default.ClientSpecialRates.Equals(""))
                                    {
                                        Comm.Parameters.Add(Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                    else
                                    {
                                        Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100, Binding.Field.Name).IsNullable = true;
                                    }
                                }
                                if (ShipmentsTable.Columns[Binding.Field.Name] == null)
                                    // this column does not exist (bc this field is missing from the reply), add it now - otherwise the adapter will fail
                                    ShipmentsTable.Columns.Add(Binding.Field.Name, typeof(String)).MaxLength = 255;
                            }
                        }

                        //****************************************************************************************************
                        using (StreamWriter write = File.AppendText("ReplyTest.txt"))
                        {
                            write.WriteLine("ODBC Command text is: ");
                            write.WriteLine(Comm.CommandText.ToString());
                            write.WriteLine("Command parameters form:");
                            for (int i = 0; i < Comm.Parameters.Count; i++)
                            {
                                object oValue = Comm.Parameters[i].Value;
                                string value;
                                if (oValue == DBNull.Value)
                                    value = "DBNull.Value";
                                else
                                    if (oValue == null)
                                    value = "null";
                                else
                                    value = Comm.Parameters[i].Value.ToString();

                                string name = Comm.Parameters[i].ToString();
                                //System.Diagnostics.Debug.WriteLine(name + " : " + value);
                                write.WriteLine(name + " : " + value);
                            }
                        }
                        //****************************************************************************************************

                        ShipmentsTableDataTable newShipmentsTable = (ShipmentsTableDataTable)ShipmentsTable.Copy();
                        newShipmentsTable.Rows.Clear();

                        foreach (ShipmentsTableRow r in temp)
                        {
                            Data.ShipmentsTableRow row_test = newShipmentsTable.Rows.Add(r.ItemArray) as Data.ShipmentsTableRow;
                            if (Template.Name == "")
                            {
                                if (row_test.Shipment_Tracking_Number.Substring(1, 8) == "42153269")
                                {
                                    row_test.Table.Columns["Shipment Tracking Number"].ReadOnly = false;
                                    row_test.Shipment_Tracking_Number = row_test.Shipment_Tracking_Number.Substring(0, 1) + row_test.Shipment_Tracking_Number.Substring(9, row_test.Shipment_Tracking_Number.Length - 9);
                                    row_test.Table.Columns["Shipment Tracking Number"].ReadOnly = true;
                                }
                            }
                        }

                        for (int i = newShipmentsTable.Rows.Count - 1; i >= 0; i--)
                        {
                            if (newShipmentsTable.Rows[i]["Status"].ToString().Equals("Error"))
                            {
                                newShipmentsTable.Rows.Remove(newShipmentsTable.Rows[i]);
                            }
                        }

                        foreach (ShipmentsTableRow r in newShipmentsTable.Rows)
                        {
                            foreach (OdbcParameter p in Comm.Parameters)
                            {
                                string name = "";
                                foreach (DataColumn c in newShipmentsTable.Columns)
                                {
                                    if (c.ColumnName.Replace(" ", "") == (p.ParameterName[0] == '@' ? p.ParameterName.Substring(1) : p.ParameterName))
                                    {
                                        name = c.ColumnName;
                                        break;
                                    }
                                }
                                if (p.ParameterName == "@SCAC")
                                {
                                    name = p.ParameterName.Replace("@", "");
                                }
                                if (name == "")
                                {
                                    if (p.ParameterName[p.ParameterName.Length - 1] == '#')
                                    {
                                        name = p.ParameterName.Substring(1, p.ParameterName.Length - 2) + " #";
                                    }
                                    else
                                    {
                                        name = p.ParameterName.Substring(1);
                                    }
                                }

                                if (p.ParameterName == "@ShippingDate")
                                {
                                    DateTime date = Convert.ToDateTime(r[name].ToString());
                                    if (!Template.WriteConn.SupportsDelete)
                                    {
                                        p.Value = date;
                                    }
                                    else
                                    {
                                        p.Value = date.ToString("yyyy''MM''dd");
                                    }

                                }
                                else
                                {
                                    if (p.ParameterName == "@SCAC")
                                    {
                                        Data.SCACSRow[] scac = SCACS.Select("Carrier_Code = '" + r.Carrier_Code + "' AND Service_Code = '" + r.Service_Code + "'") as Data.SCACSRow[];
                                        if (scac.Length == 0)
                                        {
                                            scac = SCACS.Select("Carrier_Code = '" + r.Carrier_Code + "' AND Service_Code = ''") as Data.SCACSRow[];
                                        }
                                        p.Value = scac[0].SCAC_Code;
                                    }
                                    else
                                    {
                                        p.Value = r[name].ToString().TrimEnd();
                                    }
                                }

                                if (Template.Name == "" && p.ParameterName == "ShipmentTrackingNumber")
                                {
                                    if (p.Value.ToString().Length <= 15)
                                    {
                                        while (p.Value.ToString().Length != 15)
                                        {
                                            p.Value += " ";
                                        }
                                    }
                                    else
                                    {
                                        p.Value = p.Value.ToString().Substring(p.Value.ToString().Length - 15, 15);
                                    }
                                }
                            }
                            try
                            {
                                //throw new ArgumentException();
                                Comm.ExecuteNonQuery();
                            }
                            catch (Exception e)
                            {
                                temp_error.Add(r, e.Message);
                                if (r.Package_Index == 0)
                                {
                                    Data.ShipmentsTableRow row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index);
                                    row.Status = "Error at Delete before Insert (at Save)";
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = false;
                                    row.Error_Message = e.Message;
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = true;
                                    Utils.SendClientEmail("Error at Delete before Insert (at Save) for order: " + r._Order__, e.Message);
                                    Utils.SendError("Error at Delete before Insert (at Save) for order: " + r._Order__, e);
                                }
                                else
                                {
                                    Data.ShipmentsTableRow row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, 0);
                                    row.Status = "Error at Delete before Insert (at Save)";
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = false;
                                    row.Error_Message = e.Message;
                                    row = ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index);
                                    row.Status = "Error at Delete before Insert (at Save)";
                                    row.Error_Message = e.Message;
                                    ShipmentsTable.Columns["Error Message"].ReadOnly = true;
                                }
                                continue;
                            }

                            //ShipmentsTable.FindBy_Order__Package_Index(r._Order__, r.Package_Index)["Status"] = "Committed";

                            foreach (OdbcParameter p in Comm.Parameters)
                            {
                                p.Value = null;
                            }
                        }
                        //for (int i = 0; i < newShipmentsTable.Rows.Count; i++)
                        //{
                        //    if (!ShipmentsTable.Rows[i]["Status"].ToString().Contains("Error"))
                        //    {
                        //        ShipmentsTable.Rows[i]["Status"] = "Committed";
                        //    }
                        //}
                    }
                }
            }
        }

        public void Complete_shipments()
        {
            if (OrdersTable.Columns.IndexOf("OrderNumber") > -1)
            {
                if (!(ShipmentsTable.Columns.IndexOf("OrderNumber") > -1))
                    ShipmentsTable.Columns.Add("OrderNumber");
            }
            else
            {
                if ((ShipmentsTable.Columns.IndexOf("OrderNumber") > -1))
                    ShipmentsTable.Columns.Remove("OrderNumber");
            }

            //add Sender, Recipient and Billing info to Shipments Table
            addRecipientAddressFieldsToShipments();
            for (int i = 0; i < ShipmentsTable.Rows.Count; i++)
            {
                ShipmentsTableRow shp = (ShipmentsTableRow)ShipmentsTable.Rows[i];
                //foreach (ShipmentsTableRow shp in ShipmentsTable)

                OrdersTableRow r = (OrdersTableRow)OrdersTable.FindBy_Order__(shp["Order #"].ToString());
                if (r != null)
                {
                    if (OrdersTable.Columns.IndexOf("OrderNumber") > -1)
                    {
                        if (String.IsNullOrEmpty(shp["OrderNumber"].ToString()))
                        {
                            shp["OrderNumber"] = r["OrderNumber"];
                        }
                    }
                    //set Sender, Recipient and Billing info in shipments table row
                    foreach (DataColumn col in OrdersTable.Columns)
                    {
                        if (!InteGr8_Main.data.ShipmentsTable.Columns.Contains(col.ColumnName))
                        {
                            InteGr8_Main.data.ShipmentsTable.Columns.Add(col.ColumnName, col.DataType);
                        }
                        //add all Sender, Rec., Billing, ShipDate & Shipment PO Number to Shipments table
                        if (col.ColumnName.Contains("Sender") || col.ColumnName.Contains("Recipient") || col.ColumnName.Contains("Bill") || col.ColumnName.Contains("Ship Date") || col.ColumnName.Contains("Shipment PO Number"))
                        {
                            if (String.IsNullOrEmpty(shp[col.ColumnName].ToString()))
                            {
                                if (String.IsNullOrEmpty(ShipmentsTable.Rows[i][col.ColumnName].ToString()))
                                {
                                    shp[col.ColumnName] = r[col.ColumnName];
                                }
                                else
                                {
                                    shp[col.ColumnName] = ShipmentsTable.Rows[i][col.ColumnName];
                                }
                            }
                        }

                    }

                }
            }

            //change the ship date from cuurent date to the next available shipping date
            bool allAreShipDays = true;
            String nextShipDay = System.DateTime.Today.ToString("MM-dd-yyyy");
            for (int i = 0; i < 7; i++)
                if (!PickUpDays[i])
                {
                    allAreShipDays = false;
                }
            //ClientFinalChargePackage is for ChocoInn, they want a special format for the date field, so ignore it here
            if (ShipmentsTable.Columns.IndexOf("ShippingDate") >= 0 && !(Template.Name.Contains("Chocolate Inn")))//(ShipmentsTable.Columns.IndexOf("ClientFinalChargePackage") > -1))
            {
                if (!allAreShipDays)
                {
                    nextShipDay = getNextShipDate(DateTime.Parse(nextShipDay));
                    for (int i = 0; i < ShipmentsTable.Rows.Count; i++)
                    {
                        ShipmentsTable.Rows[i]["ShippingDate"] = nextShipDay;
                    }
                }
                else
                {
                    for (int i = 0; i < ShipmentsTable.Rows.Count; i++)
                    {
                        if (!Template.Name.Contains("Chocolate Inn"))
                        {
                            ShipmentsTable.Rows[i]["ShippingDate"] = DateTime.Today.ToString("MM-dd-yyyy");
                        }
                    }
                }
            }
            //change 2Ship carrier and service to db carrier and service
            if ((ShipmentsTable.Columns.IndexOf("Carrier Code") > -1) && (ShipmentsTable.Columns.IndexOf("ClientCarrier") > -1))
            {
                string sCarrier = "1";
                string sService = "";
                foreach (ShipmentsTableRow shipRow in ShipmentsTable)
                {
                    sCarrier = getDBCarrier(shipRow["Carrier Code"].ToString());
                    sService = getDBService(shipRow["Carrier Code"].ToString(), shipRow["Service Code"].ToString());
                    shipRow["ClientCarrier"] = sCarrier;
                    shipRow["ClientService"] = sService;
                }
            }

            //******************************************************************************************************************
            using (StreamWriter writer = File.AppendText("ReplyTest.txt"))
            {
                writer.WriteLine("Shipments table fields:");
                for (int i = 0; i < ShipmentsTable.Rows.Count; i++)
                {
                    //System.Diagnostics.Debug.WriteLine("Shipment line no. " + i);
                    for (int k = 0; k < ShipmentsTable.Columns.Count; k++)
                    {
                        object oValue = ShipmentsTable.Rows[i][k];
                        string value;
                        if (oValue == DBNull.Value)
                            value = "DBNull.Value";
                        else
                            value = ShipmentsTable.Rows[i][k].ToString();

                        string name = ShipmentsTable.Columns[k].ColumnName;

                        writer.WriteLine(name + " : " + value);
                    }
                }
            }
            //******************************************************************************************************************
        }

        private void addRecipientAddressFieldsToShipments()
        {
            try
            {
                foreach (DataColumn col in OrdersTable.Columns)
                {
                    if (col.ColumnName.Contains("Sender") || col.ColumnName.Contains("Recipient") || col.ColumnName.Contains("Bill") || col.ColumnName.Contains("Ship Date"))
                    {
                        if (!(ShipmentsTable.Columns.IndexOf(col.ColumnName) > -1))
                        {
                            ShipmentsTable.Columns.Add(col.ColumnName);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.SendError("Exception adding recipient fields to Shipments", ex);
            }
        }
        //not used
        private void removeShipmentLevelPackage(out ShipmentsTableRow[] ShipmentsList)
        {
            int count = 0;
            ShipmentsList = new ShipmentsTableRow[ShipmentsTable.Rows.Count];
            foreach (ShipmentsTableRow shipment in ShipmentsTable)
            {
                if (shipment["Package index"].Equals("0"))
                {
                    ShipmentsList[count] = shipment;
                    count++;
                }
            }
            for (int i = 0; i < count; i++)
                ShipmentsTable.RemoveShipmentsTableRow(ShipmentsList[i]);

        }

        private string getDBCarrier(string shipCarrier)
        {
            string sClientCode = "";
            foreach (CarriersRow carrier in Carriers)
            {
                if (carrier.Carrier_Code == shipCarrier)
                {
                    int i = Carriers_Mapping.Rows.IndexOf(carrier.GetCarriers_MappingRows()[0]);
                    Carriers_MappingRow cMP = (Carriers_MappingRow)Carriers_Mapping.Rows[i];
                    string sCarrierCode = cMP.Carrier_Code;
                    string sClientCarrier = cMP.Client_Carrier_Code;
                    foreach (Carriers_MappingRow cm in Carriers_Mapping)
                    {
                        //get the first db carrier mapping
                        if (cm.Carrier_Code == sCarrierCode && !(cm.Client_Carrier_Code == cm.Carrier_Code))
                        {
                            sClientCode = cm.Client_Carrier_Code;
                            return sClientCode;
                        }

                    }
                }
            }
            return sClientCode;
        }

        private string getDBService(string shipCarrier, string shipService)
        {
            string sClientService = "";
            foreach (ServicesRow service in Services)
            {
                if (service.Carrier_Code == shipCarrier)
                {
                    int i = Services_Mapping.Rows.IndexOf(service.GetServices_MappingRows()[0]);
                    Services_MappingRow sMP = (Services_MappingRow)Services_Mapping.Rows[i];
                    string sCarrierCode = sMP.Carrier_Code;
                    string sClientCarrier = sMP.Client_Carrier_Code;
                    string sServiceCode = sMP.Service_Code;
                    string sClientServiceCode = sMP.Client_Service_Code;
                    foreach (Services_MappingRow sm in Services_Mapping)
                    {
                        //get the first db carrier mapping
                        if (sm.Carrier_Code == sCarrierCode && sm.Service_Code == sServiceCode && !(sm.Service_Code == sm.Client_Service_Code))
                        {
                            sClientService = sm.Client_Service_Code;
                            return sClientService;
                        }

                    }
                }
            }
            return sClientService;
        }

        // Shipments export command must have these parameters:  Order #, Tracking #, Status
        public void CheckTemplates()
        {
            foreach (TemplatesRow template in Templates)
                foreach (TablesRow table in template.GetTablesRows())
                    if (table.Type == TableType.Shipments)
                        if (Bindings.FindByField_Table(185, table.Id) == null)
                            throw new Exception("Missing mandatory column [Tracking #] for Shipments export table in [" + template.Name + "] template.");
        }
    }
}
