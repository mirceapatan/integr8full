﻿namespace InteGr8
{
    partial class frmWizard06
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard06));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lblHeaderLine = new System.Windows.Forms.Label();
            this.llAdvanced = new System.Windows.Forms.LinkLabel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.lblHeaderIcon = new System.Windows.Forms.Label();
            this.lblHeaderInfo = new System.Windows.Forms.Label();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.btnPreview = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.cbPackagesForeignKey = new System.Windows.Forms.ComboBox();
            this.lblPackagesForeignKey = new System.Windows.Forms.Label();
            this.cbPackagesTable = new System.Windows.Forms.ComboBox();
            this.lblPackagesTable = new System.Windows.Forms.Label();
            this.cbLength = new System.Windows.Forms.ComboBox();
            this.lblLength = new System.Windows.Forms.Label();
            this.cbInsuranceCurrency = new System.Windows.Forms.ComboBox();
            this.lblInsuranceCurrency = new System.Windows.Forms.Label();
            this.cbInsurance = new System.Windows.Forms.ComboBox();
            this.lblInsurance = new System.Windows.Forms.Label();
            this.lblWeightType = new System.Windows.Forms.Label();
            this.lblPackagesMappingLine = new System.Windows.Forms.Label();
            this.cbWeight = new System.Windows.Forms.ComboBox();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblPackagesMapping = new System.Windows.Forms.Label();
            this.cbWidth = new System.Windows.Forms.ComboBox();
            this.lblWidth = new System.Windows.Forms.Label();
            this.cbHeight = new System.Windows.Forms.ComboBox();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblDimensionsType = new System.Windows.Forms.Label();
            this.lblInsuranceCurrencyMessage = new System.Windows.Forms.Label();
            this.lblInsuranceMessage = new System.Windows.Forms.Label();
            this.lblWeightMessage = new System.Windows.Forms.Label();
            this.lblLengthMessage = new System.Windows.Forms.Label();
            this.lblHeightMessage = new System.Windows.Forms.Label();
            this.lblWidthMessage = new System.Windows.Forms.Label();
            this.lblDimensionsTypeMessage = new System.Windows.Forms.Label();
            this.lblWeightTypeMessage = new System.Windows.Forms.Label();
            this.lblReferenceMessage = new System.Windows.Forms.Label();
            this.cbReference = new System.Windows.Forms.ComboBox();
            this.lblReference = new System.Windows.Forms.Label();
            this.lblLink = new System.Windows.Forms.Label();
            this.cbWeightType = new System.Windows.Forms.ComboBox();
            this.cbDimensionsType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPackagesPrimaryKey = new System.Windows.Forms.ComboBox();
            this.lblPackagesPrimaryKey = new System.Windows.Forms.Label();
            this.lblPackagesPrimaryKeyMessage = new System.Windows.Forms.Label();
            this.lblOrderNumberIsPK = new System.Windows.Forms.Label();
            this.rbOrderNumberIsPKYes = new System.Windows.Forms.RadioButton();
            this.rbOrderNumberIsPKNo = new System.Windows.Forms.RadioButton();
            this.pnlHeader.SuspendLayout();
            this.pnlFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.lblHeaderLine);
            this.pnlHeader.Controls.Add(this.llAdvanced);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.lblHeaderIcon);
            this.pnlHeader.Controls.Add(this.lblHeaderInfo);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(629, 90);
            this.pnlHeader.TabIndex = 114;
            // 
            // lblHeaderLine
            // 
            this.lblHeaderLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderLine.BackColor = System.Drawing.SystemColors.Control;
            this.lblHeaderLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeaderLine.Location = new System.Drawing.Point(0, 88);
            this.lblHeaderLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderLine.Name = "lblHeaderLine";
            this.lblHeaderLine.Size = new System.Drawing.Size(629, 2);
            this.lblHeaderLine.TabIndex = 116;
            // 
            // llAdvanced
            // 
            this.llAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdvanced.Image = global::InteGr8.Properties.Resources.wrench;
            this.llAdvanced.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llAdvanced.LinkArea = new System.Windows.Forms.LinkArea(6, 13);
            this.llAdvanced.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdvanced.Location = new System.Drawing.Point(525, 0);
            this.llAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.llAdvanced.Name = "llAdvanced";
            this.llAdvanced.Size = new System.Drawing.Size(104, 24);
            this.llAdvanced.TabIndex = 119;
            this.llAdvanced.TabStop = true;
            this.llAdvanced.Text = "      advanced mode";
            this.llAdvanced.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llAdvanced.UseCompatibleTextRendering = true;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(525, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Package Level Information";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeaderIcon
            // 
            this.lblHeaderIcon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeaderIcon.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaderIcon.Image = global::InteGr8.Properties.Resources.data_next;
            this.lblHeaderIcon.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeaderIcon.Location = new System.Drawing.Point(0, 24);
            this.lblHeaderIcon.Name = "lblHeaderIcon";
            this.lblHeaderIcon.Padding = new System.Windows.Forms.Padding(5);
            this.lblHeaderIcon.Size = new System.Drawing.Size(64, 64);
            this.lblHeaderIcon.TabIndex = 115;
            // 
            // lblHeaderInfo
            // 
            this.lblHeaderInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderInfo.AutoEllipsis = true;
            this.lblHeaderInfo.Location = new System.Drawing.Point(68, 24);
            this.lblHeaderInfo.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderInfo.Name = "lblHeaderInfo";
            this.lblHeaderInfo.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.lblHeaderInfo.Size = new System.Drawing.Size(561, 64);
            this.lblHeaderInfo.TabIndex = 114;
            this.lblHeaderInfo.Text = resources.GetString("lblHeaderInfo.Text");
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.lblFooterLine);
            this.pnlFooter.Controls.Add(this.btnPreview);
            this.pnlFooter.Controls.Add(this.btnPrev);
            this.pnlFooter.Controls.Add(this.btnNext);
            this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFooter.Location = new System.Drawing.Point(0, 532);
            this.pnlFooter.Margin = new System.Windows.Forms.Padding(0);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Padding = new System.Windows.Forms.Padding(9);
            this.pnlFooter.Size = new System.Drawing.Size(629, 53);
            this.pnlFooter.TabIndex = 119;
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFooterLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(12, 5);
            this.lblFooterLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(605, 2);
            this.lblFooterLine.TabIndex = 122;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(11, 18);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(108, 23);
            this.btnPreview.TabIndex = 14;
            this.btnPreview.Text = "Preview Packages";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnPrev.Location = new System.Drawing.Point(461, 18);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 15;
            this.btnPrev.Text = "< Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(542, 18);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 16;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // cbPackagesForeignKey
            // 
            this.cbPackagesForeignKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPackagesForeignKey.FormattingEnabled = true;
            this.cbPackagesForeignKey.Location = new System.Drawing.Point(227, 129);
            this.cbPackagesForeignKey.Name = "cbPackagesForeignKey";
            this.cbPackagesForeignKey.Size = new System.Drawing.Size(174, 21);
            this.cbPackagesForeignKey.TabIndex = 1;
            // 
            // lblPackagesForeignKey
            // 
            this.lblPackagesForeignKey.AutoSize = true;
            this.lblPackagesForeignKey.Location = new System.Drawing.Point(9, 132);
            this.lblPackagesForeignKey.Name = "lblPackagesForeignKey";
            this.lblPackagesForeignKey.Size = new System.Drawing.Size(194, 13);
            this.lblPackagesForeignKey.TabIndex = 141;
            this.lblPackagesForeignKey.Text = "Packages Order # Foreign Key Column:";
            // 
            // cbPackagesTable
            // 
            this.cbPackagesTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPackagesTable.FormattingEnabled = true;
            this.cbPackagesTable.Location = new System.Drawing.Point(227, 102);
            this.cbPackagesTable.Name = "cbPackagesTable";
            this.cbPackagesTable.Size = new System.Drawing.Size(174, 21);
            this.cbPackagesTable.TabIndex = 0;
            this.cbPackagesTable.SelectedIndexChanged += new System.EventHandler(this.cbPackagesTable_SelectedIndexChanged);
            // 
            // lblPackagesTable
            // 
            this.lblPackagesTable.AutoSize = true;
            this.lblPackagesTable.Location = new System.Drawing.Point(9, 105);
            this.lblPackagesTable.Name = "lblPackagesTable";
            this.lblPackagesTable.Size = new System.Drawing.Size(88, 13);
            this.lblPackagesTable.TabIndex = 139;
            this.lblPackagesTable.Text = "Packages Table:";
            // 
            // cbLength
            // 
            this.cbLength.FormattingEnabled = true;
            this.cbLength.Location = new System.Drawing.Point(164, 376);
            this.cbLength.Name = "cbLength";
            this.cbLength.Size = new System.Drawing.Size(174, 21);
            this.cbLength.TabIndex = 9;
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(9, 379);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(85, 13);
            this.lblLength.TabIndex = 162;
            this.lblLength.Text = "Package length:";
            // 
            // cbInsuranceCurrency
            // 
            this.cbInsuranceCurrency.FormattingEnabled = true;
            this.cbInsuranceCurrency.Items.AddRange(new object[] {
            "Customer Packaging",
            "Envelope (Letter)",
            "Small Pack"});
            this.cbInsuranceCurrency.Location = new System.Drawing.Point(164, 349);
            this.cbInsuranceCurrency.Name = "cbInsuranceCurrency";
            this.cbInsuranceCurrency.Size = new System.Drawing.Size(174, 21);
            this.cbInsuranceCurrency.TabIndex = 8;
            // 
            // lblInsuranceCurrency
            // 
            this.lblInsuranceCurrency.AutoSize = true;
            this.lblInsuranceCurrency.Location = new System.Drawing.Point(9, 352);
            this.lblInsuranceCurrency.Name = "lblInsuranceCurrency";
            this.lblInsuranceCurrency.Size = new System.Drawing.Size(146, 13);
            this.lblInsuranceCurrency.TabIndex = 160;
            this.lblInsuranceCurrency.Text = "Package insurance currency:";
            // 
            // cbInsurance
            // 
            this.cbInsurance.FormattingEnabled = true;
            this.cbInsurance.Items.AddRange(new object[] {
            "(none)",
            "Cheapest",
            "Fastest"});
            this.cbInsurance.Location = new System.Drawing.Point(164, 322);
            this.cbInsurance.Name = "cbInsurance";
            this.cbInsurance.Size = new System.Drawing.Size(174, 21);
            this.cbInsurance.TabIndex = 7;
            // 
            // lblInsurance
            // 
            this.lblInsurance.AutoSize = true;
            this.lblInsurance.Location = new System.Drawing.Point(9, 325);
            this.lblInsurance.Name = "lblInsurance";
            this.lblInsurance.Size = new System.Drawing.Size(140, 13);
            this.lblInsurance.TabIndex = 158;
            this.lblInsurance.Text = "Package insurance amount:";
            // 
            // lblWeightType
            // 
            this.lblWeightType.AutoSize = true;
            this.lblWeightType.Location = new System.Drawing.Point(9, 298);
            this.lblWeightType.Name = "lblWeightType";
            this.lblWeightType.Size = new System.Drawing.Size(110, 13);
            this.lblWeightType.TabIndex = 156;
            this.lblWeightType.Text = "Package weight type:";
            // 
            // lblPackagesMappingLine
            // 
            this.lblPackagesMappingLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPackagesMappingLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblPackagesMappingLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPackagesMappingLine.Location = new System.Drawing.Point(12, 256);
            this.lblPackagesMappingLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblPackagesMappingLine.Name = "lblPackagesMappingLine";
            this.lblPackagesMappingLine.Size = new System.Drawing.Size(605, 2);
            this.lblPackagesMappingLine.TabIndex = 154;
            // 
            // cbWeight
            // 
            this.cbWeight.FormattingEnabled = true;
            this.cbWeight.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing"});
            this.cbWeight.Location = new System.Drawing.Point(164, 268);
            this.cbWeight.Name = "cbWeight";
            this.cbWeight.Size = new System.Drawing.Size(174, 21);
            this.cbWeight.TabIndex = 5;
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Location = new System.Drawing.Point(9, 271);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(87, 13);
            this.lblWeight.TabIndex = 153;
            this.lblWeight.Text = "Package weight:";
            // 
            // lblPackagesMapping
            // 
            this.lblPackagesMapping.AutoSize = true;
            this.lblPackagesMapping.Location = new System.Drawing.Point(9, 238);
            this.lblPackagesMapping.Name = "lblPackagesMapping";
            this.lblPackagesMapping.Size = new System.Drawing.Size(523, 13);
            this.lblPackagesMapping.TabIndex = 152;
            this.lblPackagesMapping.Text = "Please map the following 2Ship package variables to your Packages table columns, " +
                "or choose default values:";
            // 
            // cbWidth
            // 
            this.cbWidth.FormattingEnabled = true;
            this.cbWidth.Location = new System.Drawing.Point(164, 403);
            this.cbWidth.Name = "cbWidth";
            this.cbWidth.Size = new System.Drawing.Size(174, 21);
            this.cbWidth.TabIndex = 10;
            // 
            // lblWidth
            // 
            this.lblWidth.AutoSize = true;
            this.lblWidth.Location = new System.Drawing.Point(9, 406);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(81, 13);
            this.lblWidth.TabIndex = 164;
            this.lblWidth.Text = "Package width:";
            // 
            // cbHeight
            // 
            this.cbHeight.FormattingEnabled = true;
            this.cbHeight.Location = new System.Drawing.Point(164, 430);
            this.cbHeight.Name = "cbHeight";
            this.cbHeight.Size = new System.Drawing.Size(174, 21);
            this.cbHeight.TabIndex = 11;
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Location = new System.Drawing.Point(9, 433);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(85, 13);
            this.lblHeight.TabIndex = 166;
            this.lblHeight.Text = "Package height:";
            // 
            // lblDimensionsType
            // 
            this.lblDimensionsType.AutoSize = true;
            this.lblDimensionsType.Location = new System.Drawing.Point(9, 457);
            this.lblDimensionsType.Name = "lblDimensionsType";
            this.lblDimensionsType.Size = new System.Drawing.Size(131, 13);
            this.lblDimensionsType.TabIndex = 171;
            this.lblDimensionsType.Text = "Package dimensions type:";
            // 
            // lblInsuranceCurrencyMessage
            // 
            this.lblInsuranceCurrencyMessage.AutoSize = true;
            this.lblInsuranceCurrencyMessage.Location = new System.Drawing.Point(346, 352);
            this.lblInsuranceCurrencyMessage.Name = "lblInsuranceCurrencyMessage";
            this.lblInsuranceCurrencyMessage.Size = new System.Drawing.Size(151, 13);
            this.lblInsuranceCurrencyMessage.TabIndex = 172;
            this.lblInsuranceCurrencyMessage.Text = "three letters ISO curency code";
            // 
            // lblInsuranceMessage
            // 
            this.lblInsuranceMessage.AutoSize = true;
            this.lblInsuranceMessage.Location = new System.Drawing.Point(346, 325);
            this.lblInsuranceMessage.Name = "lblInsuranceMessage";
            this.lblInsuranceMessage.Size = new System.Drawing.Size(274, 13);
            this.lblInsuranceMessage.TabIndex = 173;
            this.lblInsuranceMessage.Text = "optional, two digits (carrier surcharges may apply, if used)";
            // 
            // lblWeightMessage
            // 
            this.lblWeightMessage.AutoSize = true;
            this.lblWeightMessage.Location = new System.Drawing.Point(346, 271);
            this.lblWeightMessage.Name = "lblWeightMessage";
            this.lblWeightMessage.Size = new System.Drawing.Size(95, 13);
            this.lblWeightMessage.TabIndex = 174;
            this.lblWeightMessage.Text = "required, two digits";
            // 
            // lblLengthMessage
            // 
            this.lblLengthMessage.AutoSize = true;
            this.lblLengthMessage.Location = new System.Drawing.Point(346, 379);
            this.lblLengthMessage.Name = "lblLengthMessage";
            this.lblLengthMessage.Size = new System.Drawing.Size(265, 13);
            this.lblLengthMessage.TabIndex = 175;
            this.lblLengthMessage.Text = "optional, one digit (use the largest dimension for length)";
            // 
            // lblHeightMessage
            // 
            this.lblHeightMessage.AutoSize = true;
            this.lblHeightMessage.Location = new System.Drawing.Point(346, 433);
            this.lblHeightMessage.Name = "lblHeightMessage";
            this.lblHeightMessage.Size = new System.Drawing.Size(271, 13);
            this.lblHeightMessage.TabIndex = 176;
            this.lblHeightMessage.Text = "optional, one digit (use the smallest dimension for height)";
            // 
            // lblWidthMessage
            // 
            this.lblWidthMessage.AutoSize = true;
            this.lblWidthMessage.Location = new System.Drawing.Point(346, 406);
            this.lblWidthMessage.Name = "lblWidthMessage";
            this.lblWidthMessage.Size = new System.Drawing.Size(90, 13);
            this.lblWidthMessage.TabIndex = 177;
            this.lblWidthMessage.Text = "optional, one digit";
            // 
            // lblDimensionsTypeMessage
            // 
            this.lblDimensionsTypeMessage.AutoSize = true;
            this.lblDimensionsTypeMessage.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblDimensionsTypeMessage.Location = new System.Drawing.Point(346, 459);
            this.lblDimensionsTypeMessage.Name = "lblDimensionsTypeMessage";
            this.lblDimensionsTypeMessage.Size = new System.Drawing.Size(246, 13);
            this.lblDimensionsTypeMessage.TabIndex = 178;
            this.lblDimensionsTypeMessage.Text = "(please specify dimensions to avoid incorrect rates)";
            // 
            // lblWeightTypeMessage
            // 
            this.lblWeightTypeMessage.AutoSize = true;
            this.lblWeightTypeMessage.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblWeightTypeMessage.Location = new System.Drawing.Point(346, 298);
            this.lblWeightTypeMessage.Name = "lblWeightTypeMessage";
            this.lblWeightTypeMessage.Size = new System.Drawing.Size(245, 13);
            this.lblWeightTypeMessage.TabIndex = 179;
            this.lblWeightTypeMessage.Text = "(please use correct weight to avoid incorrect rates)";
            // 
            // lblReferenceMessage
            // 
            this.lblReferenceMessage.AutoSize = true;
            this.lblReferenceMessage.Location = new System.Drawing.Point(346, 483);
            this.lblReferenceMessage.Name = "lblReferenceMessage";
            this.lblReferenceMessage.Size = new System.Drawing.Size(264, 13);
            this.lblReferenceMessage.TabIndex = 182;
            this.lblReferenceMessage.Text = "optional, not used by 2Ship, but can be used in reports";
            // 
            // cbReference
            // 
            this.cbReference.FormattingEnabled = true;
            this.cbReference.Location = new System.Drawing.Point(164, 480);
            this.cbReference.Name = "cbReference";
            this.cbReference.Size = new System.Drawing.Size(174, 21);
            this.cbReference.TabIndex = 13;
            // 
            // lblReference
            // 
            this.lblReference.AutoSize = true;
            this.lblReference.Location = new System.Drawing.Point(9, 483);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(121, 13);
            this.lblReference.TabIndex = 181;
            this.lblReference.Text = "Package reference info:";
            // 
            // lblLink
            // 
            this.lblLink.AutoSize = true;
            this.lblLink.Location = new System.Drawing.Point(411, 132);
            this.lblLink.Name = "lblLink";
            this.lblLink.Size = new System.Drawing.Size(121, 13);
            this.lblLink.TabIndex = 183;
            this.lblLink.Text = "one-to-many relationship";
            // 
            // cbWeightType
            // 
            this.cbWeightType.FormattingEnabled = true;
            this.cbWeightType.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing"});
            this.cbWeightType.Location = new System.Drawing.Point(164, 295);
            this.cbWeightType.Name = "cbWeightType";
            this.cbWeightType.Size = new System.Drawing.Size(174, 21);
            this.cbWeightType.TabIndex = 6;
            // 
            // cbDimensionsType
            // 
            this.cbDimensionsType.FormattingEnabled = true;
            this.cbDimensionsType.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing"});
            this.cbDimensionsType.Location = new System.Drawing.Point(164, 454);
            this.cbDimensionsType.Name = "cbDimensionsType";
            this.cbDimensionsType.Size = new System.Drawing.Size(174, 21);
            this.cbDimensionsType.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 516);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(491, 13);
            this.label1.TabIndex = 328;
            this.label1.Text = "Note: You will be able to add more variable mappings later, in the Summary Review" +
                " page of this wizard.";
            // 
            // cbPackagesPrimaryKey
            // 
            this.cbPackagesPrimaryKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPackagesPrimaryKey.Enabled = false;
            this.cbPackagesPrimaryKey.FormattingEnabled = true;
            this.cbPackagesPrimaryKey.Location = new System.Drawing.Point(227, 156);
            this.cbPackagesPrimaryKey.Name = "cbPackagesPrimaryKey";
            this.cbPackagesPrimaryKey.Size = new System.Drawing.Size(174, 21);
            this.cbPackagesPrimaryKey.TabIndex = 2;
            this.cbPackagesPrimaryKey.SelectedIndexChanged += new System.EventHandler(this.cbPackagesPrimaryKey_SelectedIndexChanged);
            // 
            // lblPackagesPrimaryKey
            // 
            this.lblPackagesPrimaryKey.AutoSize = true;
            this.lblPackagesPrimaryKey.Location = new System.Drawing.Point(9, 159);
            this.lblPackagesPrimaryKey.Name = "lblPackagesPrimaryKey";
            this.lblPackagesPrimaryKey.Size = new System.Drawing.Size(210, 13);
            this.lblPackagesPrimaryKey.TabIndex = 330;
            this.lblPackagesPrimaryKey.Text = "Packages Package # Primary Key Column:";
            // 
            // lblPackagesPrimaryKeyMessage
            // 
            this.lblPackagesPrimaryKeyMessage.AutoSize = true;
            this.lblPackagesPrimaryKeyMessage.Location = new System.Drawing.Point(411, 159);
            this.lblPackagesPrimaryKeyMessage.Name = "lblPackagesPrimaryKeyMessage";
            this.lblPackagesPrimaryKeyMessage.Size = new System.Drawing.Size(44, 13);
            this.lblPackagesPrimaryKeyMessage.TabIndex = 331;
            this.lblPackagesPrimaryKeyMessage.Text = "optional";
            // 
            // lblOrderNumberIsPK
            // 
            this.lblOrderNumberIsPK.AutoSize = true;
            this.lblOrderNumberIsPK.Enabled = false;
            this.lblOrderNumberIsPK.Location = new System.Drawing.Point(8, 185);
            this.lblOrderNumberIsPK.Name = "lblOrderNumberIsPK";
            this.lblOrderNumberIsPK.Size = new System.Drawing.Size(165, 13);
            this.lblOrderNumberIsPK.TabIndex = 332;
            this.lblOrderNumberIsPK.Text = "Order # is part of the Primary Key:";
            // 
            // rbOrderNumberIsPKYes
            // 
            this.rbOrderNumberIsPKYes.AutoSize = true;
            this.rbOrderNumberIsPKYes.Enabled = false;
            this.rbOrderNumberIsPKYes.Location = new System.Drawing.Point(227, 183);
            this.rbOrderNumberIsPKYes.Name = "rbOrderNumberIsPKYes";
            this.rbOrderNumberIsPKYes.Size = new System.Drawing.Size(302, 17);
            this.rbOrderNumberIsPKYes.TabIndex = 3;
            this.rbOrderNumberIsPKYes.Text = "Yes, the Package # is unique only within the same Order #";
            this.rbOrderNumberIsPKYes.UseVisualStyleBackColor = true;
            // 
            // rbOrderNumberIsPKNo
            // 
            this.rbOrderNumberIsPKNo.AutoSize = true;
            this.rbOrderNumberIsPKNo.Enabled = false;
            this.rbOrderNumberIsPKNo.Location = new System.Drawing.Point(227, 206);
            this.rbOrderNumberIsPKNo.Name = "rbOrderNumberIsPKNo";
            this.rbOrderNumberIsPKNo.Size = new System.Drawing.Size(322, 17);
            this.rbOrderNumberIsPKNo.TabIndex = 4;
            this.rbOrderNumberIsPKNo.Text = "No, the Package # is globally unique, regardless of the Order #";
            this.rbOrderNumberIsPKNo.UseVisualStyleBackColor = true;
            // 
            // frmWizard06
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(629, 585);
            this.Controls.Add(this.rbOrderNumberIsPKNo);
            this.Controls.Add(this.rbOrderNumberIsPKYes);
            this.Controls.Add(this.lblOrderNumberIsPK);
            this.Controls.Add(this.lblPackagesPrimaryKeyMessage);
            this.Controls.Add(this.cbPackagesPrimaryKey);
            this.Controls.Add(this.lblPackagesPrimaryKey);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbDimensionsType);
            this.Controls.Add(this.cbWeightType);
            this.Controls.Add(this.lblLink);
            this.Controls.Add(this.lblReferenceMessage);
            this.Controls.Add(this.cbReference);
            this.Controls.Add(this.lblReference);
            this.Controls.Add(this.lblWeightTypeMessage);
            this.Controls.Add(this.lblDimensionsTypeMessage);
            this.Controls.Add(this.lblWidthMessage);
            this.Controls.Add(this.lblHeightMessage);
            this.Controls.Add(this.lblLengthMessage);
            this.Controls.Add(this.lblWeightMessage);
            this.Controls.Add(this.lblInsuranceMessage);
            this.Controls.Add(this.lblInsuranceCurrencyMessage);
            this.Controls.Add(this.lblDimensionsType);
            this.Controls.Add(this.cbHeight);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.cbWidth);
            this.Controls.Add(this.lblWidth);
            this.Controls.Add(this.cbLength);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.cbInsuranceCurrency);
            this.Controls.Add(this.lblInsuranceCurrency);
            this.Controls.Add(this.cbInsurance);
            this.Controls.Add(this.lblInsurance);
            this.Controls.Add(this.lblWeightType);
            this.Controls.Add(this.lblPackagesMappingLine);
            this.Controls.Add(this.cbWeight);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.lblPackagesMapping);
            this.Controls.Add(this.cbPackagesForeignKey);
            this.Controls.Add(this.lblPackagesForeignKey);
            this.Controls.Add(this.cbPackagesTable);
            this.Controls.Add(this.lblPackagesTable);
            this.Controls.Add(this.pnlFooter);
            this.Controls.Add(this.pnlHeader);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(637, 542);
            this.Name = "frmWizard06";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 6 of 11";
            this.Load += new System.EventHandler(this.frmWizard06_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard06_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWizard06_FormClosing);
            this.pnlHeader.ResumeLayout(false);
            this.pnlFooter.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.LinkLabel llAdvanced;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label lblHeaderIcon;
        private System.Windows.Forms.Label lblHeaderInfo;
        private System.Windows.Forms.Label lblHeaderLine;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.ComboBox cbPackagesForeignKey;
        private System.Windows.Forms.Label lblPackagesForeignKey;
        private System.Windows.Forms.ComboBox cbPackagesTable;
        private System.Windows.Forms.Label lblPackagesTable;
        private System.Windows.Forms.ComboBox cbLength;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.ComboBox cbInsuranceCurrency;
        private System.Windows.Forms.Label lblInsuranceCurrency;
        private System.Windows.Forms.ComboBox cbInsurance;
        private System.Windows.Forms.Label lblInsurance;
        private System.Windows.Forms.Label lblWeightType;
        private System.Windows.Forms.Label lblPackagesMappingLine;
        private System.Windows.Forms.ComboBox cbWeight;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblPackagesMapping;
        private System.Windows.Forms.ComboBox cbWidth;
        private System.Windows.Forms.Label lblWidth;
        private System.Windows.Forms.ComboBox cbHeight;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.Label lblDimensionsType;
        private System.Windows.Forms.Label lblInsuranceCurrencyMessage;
        private System.Windows.Forms.Label lblInsuranceMessage;
        private System.Windows.Forms.Label lblWeightMessage;
        private System.Windows.Forms.Label lblLengthMessage;
        private System.Windows.Forms.Label lblHeightMessage;
        private System.Windows.Forms.Label lblWidthMessage;
        private System.Windows.Forms.Label lblDimensionsTypeMessage;
        private System.Windows.Forms.Label lblWeightTypeMessage;
        private System.Windows.Forms.Label lblReferenceMessage;
        private System.Windows.Forms.ComboBox cbReference;
        private System.Windows.Forms.Label lblReference;
        private System.Windows.Forms.Label lblLink;
        private System.Windows.Forms.ComboBox cbWeightType;
        private System.Windows.Forms.ComboBox cbDimensionsType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbPackagesPrimaryKey;
        private System.Windows.Forms.Label lblPackagesPrimaryKey;
        private System.Windows.Forms.Label lblPackagesPrimaryKeyMessage;
        private System.Windows.Forms.Label lblOrderNumberIsPK;
        private System.Windows.Forms.RadioButton rbOrderNumberIsPKYes;
        private System.Windows.Forms.RadioButton rbOrderNumberIsPKNo;

    }
}