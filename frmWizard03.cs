﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard03 : Form
    {
        private bool custom_binding = false;
        private FieldLink link = null;

        public frmWizard03()
        {
            InitializeComponent();
        }

        private void frmWizard03_Load(object sender, EventArgs e)
        {
            // populate the Senders table combobox with the ODBC catalog tables
            cbSendersTable.Items.Add("(none)");
            using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
            {
                Conn.Open();
                foreach (DataRow row in Conn.GetSchema("Tables").Rows)
                    cbSendersTable.Items.Add(row["TABLE_NAME"]);
            }

            // populate the Orders foreign key column combobox with the Orders table columns
            using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
            {
                Conn.Open();
                using (OdbcDataReader dr = new OdbcCommand("Select Top 0 * From " + InteGr8_Main.template.Orders, Conn).ExecuteReader(CommandBehavior.SingleRow))
                    for (int k = 0; k < dr.FieldCount; k++)
                        cbOrdersForeignKey.Items.Add("[" + dr.GetName(k) + "]");
            }
            
            // check that each field have the same link
            foreach (FieldBinding binding in InteGr8_Main.template.Bindings)
                if (binding.Field.FieldCategory == Data.FieldsTableDataTable.FieldCategory.OrderSender && binding.Link != null)
                    if (link == null) link = binding.Link;
                    else if (!binding.Link.Equals(link)) custom_binding = true;
            if (link != null && link.Type != FieldLink.LinkType.Orders && link.Type != FieldLink.LinkType.OrdersManyToOne) custom_binding = true;
            if (custom_binding)
            {
                foreach(Control control in this.Controls)
                    if (control is ComboBox) control.Enabled = false;
                MessageBox.Show(this, "Senders configuration was customized and cannot be edited in this page. You will be able to edit in the summary page. Please click Next to skip this page.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // load the existing senders configuration
            if (link == null) cbSendersTable.SelectedIndex = 0;
            else
            {
                int index = cbSendersTable.FindStringExact(link.FieldTable);
                if (index == -1)
                {
                    cbSendersTable.SelectedIndex = 0;
                    MessageBox.Show(this, "Senders table '" + link.FieldTable + "' not found in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else cbSendersTable.SelectedIndex = index;
                if (link.Type != FieldLink.LinkType.Orders && link.FieldTablePK != null)
                {
                    cbSendersPrimaryKey.SelectedIndex = cbSendersPrimaryKey.FindStringExact(link.FieldTablePK);
                    if (cbSendersPrimaryKey.SelectedIndex == -1)
                    {
                        MessageBox.Show(this, "Senders table primary key column '" + link.FieldTablePK + "' not found in the Senders table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                if (link.Type != FieldLink.LinkType.Orders && link.OrdersTableFieldFK != null)
                {
                    cbOrdersForeignKey.SelectedIndex = cbOrdersForeignKey.FindStringExact(link.OrdersTableFieldFK);
                    if (cbOrdersForeignKey.SelectedIndex == -1)
                    {
                        MessageBox.Show(this, "Orders table foreign key column '" + link.OrdersTableFieldFK + "' not found in the Orders table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
            if (InteGr8_Main.template.Find("Sender Contact") != null) cbContact.Text = InteGr8_Main.template.Find("Sender Contact").Value;
            if (InteGr8_Main.template.Find("Sender Company") != null) cbCompany.Text = InteGr8_Main.template.Find("Sender Company").Value;
            if (InteGr8_Main.template.Find("Sender Country") != null) cbCountry.Text = InteGr8_Main.template.Find("Sender Country").Value;
            if (InteGr8_Main.template.Find("Sender State / Province") != null) cbState.Text = InteGr8_Main.template.Find("Sender State / Province").Value;
            if (InteGr8_Main.template.Find("Sender City") != null) cbCity.Text = InteGr8_Main.template.Find("Sender City").Value;
            if (InteGr8_Main.template.Find("Sender ZIP / Postal Code") != null) cbPostal.Text = InteGr8_Main.template.Find("Sender ZIP / Postal Code").Value;
            if (InteGr8_Main.template.Find("Sender Address 1") != null) cbAddress1.Text = InteGr8_Main.template.Find("Sender Address 1").Value;
            if (InteGr8_Main.template.Find("Sender Address 2") != null) cbAddress2.Text = InteGr8_Main.template.Find("Sender Address 2").Value;
            if (InteGr8_Main.template.Find("Sender Tel") != null) cbPhone.Text = InteGr8_Main.template.Find("Sender Tel").Value;
            if (InteGr8_Main.template.Find("Sender Email") != null) cbEmail.Text = InteGr8_Main.template.Find("Sender Email").Value;
        }

        private void cbSendersTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // clear all column comboboxes
            cbSendersPrimaryKey.Items.Clear();
            cbContact.Items.Clear();
            cbCompany.Items.Clear();
            cbCountry.Items.Clear();
            cbState.Items.Clear();
            cbCity.Items.Clear();
            cbPostal.Items.Clear();
            cbAddress1.Items.Clear(); ;
            cbAddress2.Items.Clear();
            cbPhone.Items.Clear();
            cbEmail.Items.Clear();

            cbOrdersForeignKey.SelectedIndex = -1;

            // if link is needed or not (Orders left join Senders)
            bool link = cbSendersTable.SelectedIndex > 0 && !cbSendersTable.Text.Equals(InteGr8_Main.template.Orders);
            cbSendersPrimaryKey.Enabled = link;
            cbOrdersForeignKey.Enabled = link;
            lblLink.Visible = link;
            lblLinkTop.Visible = link;
            lblLinkRight.Visible = link;
            lblLinkBottom.Visible = link;

            // read the Senders table columns and populate all the sender comboboxes
            if (cbSendersTable.SelectedIndex > 0)
            {
                string[] columns = null;
                using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
                {
                    Conn.Open();
                    using (OdbcDataReader dr = new OdbcCommand("Select Top 0 * From " + cbSendersTable.Text, Conn).ExecuteReader(CommandBehavior.SingleRow))
                    {
                        columns = new string[dr.FieldCount];
                        for (int k = 0; k < dr.FieldCount; k++)
                            columns[k] = "[" + dr.GetName(k) + "]";
                    }
                }

                if (columns != null)
                {
                    cbSendersPrimaryKey.Items.AddRange(columns);
                    cbContact.Items.AddRange(columns);
                    cbCompany.Items.AddRange(columns);
                    cbCountry.Items.AddRange(columns);
                    cbState.Items.AddRange(columns);
                    cbCity.Items.AddRange(columns);
                    cbPostal.Items.AddRange(columns);
                    cbAddress1.Items.AddRange(columns);
                    cbAddress2.Items.AddRange(columns);
                    cbPhone.Items.AddRange(columns);
                    cbEmail.Items.AddRange(columns);
                }
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (cbSendersTable.SelectedIndex <= 0 || cbSendersTable.Text.Trim().Equals("")) return;
            new frmWizard_DataPreview("Senders", InteGr8_Main.template.Connection, "Select Top 100 * From " + cbSendersTable.Text).ShowDialog(this);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (cbSendersPrimaryKey.Enabled && cbSendersPrimaryKey.SelectedIndex == -1 || cbOrdersForeignKey.Enabled && cbOrdersForeignKey.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please map the link between the Senders table primary key column and the Orders table foreign key column.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // TODO: check the FK

            bool contact = !cbContact.Text.Trim().Equals("");
            bool company = !cbCompany.Text.Trim().Equals("");
            bool country = !cbCountry.Text.Trim().Equals("");
            bool state = !cbCountry.Text.Trim().Equals("US") && !cbCountry.Text.Trim().Equals("CA") || (cbCountry.Text.Trim().Equals("US") || cbCountry.Text.Trim().Equals("CA")) && !cbState.Text.Trim().Equals("");
            bool city = !cbCity.Text.Trim().Equals("");
            bool postal = !cbPostal.Text.Trim().Equals("");
            bool address1 = !cbAddress1.Text.Trim().Equals("");
            bool address2 = !cbAddress2.Text.Trim().Equals("");
            bool phone = !cbPhone.Text.Trim().Equals("");
            bool email = !cbEmail.Text.Trim().Equals("");

            if (!country || !state || !postal)
            {
                if (MessageBox.Show(this, "Warning, one or more sender fields required for rating are not mapped.\nYou will not be able to automatically rate or ship from InteGr8.\nEvery shipment will be individually transferred to the web browser, in the 2Ship shipment edit page.\nContinue with the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) 
                    return;
            }
            else if (!contact && !company || !country || !state || !postal || !phone)
            {
                if (MessageBox.Show(this, "Warning, one or more sender fields required for shipping are not mapped.\nYou will not be able to automatically ship from InteGr8.\nEvery shipment will be individually transferred to the web browser, in the 2Ship shipment edit page.\nContinue with the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) 
                    return;
            }
            else if (!contact || !company || !country || !state || !city || !postal || !address1 || !address2 || !phone || !email)
            {
                if (MessageBox.Show(this, "Warning, one or more optional sender fields are not mapped.\nHowever, you will be able to automatically ship from InteGr8.\nStill, it is recommended that you map every sender field.\nContinue with the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) 
                    return;
            }

            // save the results
            if (!custom_binding)
            {
                if (cbSendersTable.SelectedIndex == 0) link = null;
                else if (cbSendersTable.Text.Equals(InteGr8_Main.template.Orders))
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.Orders;
                    link.FieldTable = cbSendersTable.Text;
                }
                else
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.OrdersManyToOne;
                    link.FieldTable = cbSendersTable.Text;
                    link.FieldTablePK = cbSendersPrimaryKey.Text;
                    link.OrdersTableFieldFK = cbOrdersForeignKey.Text;
                }
                
                InteGr8_Main.template.UpdateBinding("Sender Contact", cbContact.FindStringExact(cbContact.Text) == -1, contact ? cbContact.Text : null, link);
                InteGr8_Main.template.UpdateBinding("Sender Company", cbCompany.FindStringExact(cbCompany.Text) == -1, company ? cbCompany.Text : null, link);
                InteGr8_Main.template.UpdateBinding("Sender Country", cbCountry.FindStringExact(cbCountry.Text) == -1, country ? cbCountry.Text : null, link);
                InteGr8_Main.template.UpdateBinding("Sender State / Province", cbState.FindStringExact(cbState.Text) == -1, state ? cbState.Text : null, link);
                InteGr8_Main.template.UpdateBinding("Sender City", cbCity.FindStringExact(cbCity.Text) == -1, city ? cbCity.Text : null, link);
                InteGr8_Main.template.UpdateBinding("Sender ZIP / Postal Code", cbPostal.FindStringExact(cbPostal.Text) == -1, postal ? cbPostal.Text : null, link);
                InteGr8_Main.template.UpdateBinding("Sender Address 1", cbAddress1.FindStringExact(cbAddress1.Text) == -1, address1 ? cbAddress1.Text : null, link);
                InteGr8_Main.template.UpdateBinding("Sender Address 2", cbAddress2.FindStringExact(cbAddress2.Text) == -1, address2 ? cbAddress2.Text : null, link);
                InteGr8_Main.template.UpdateBinding("Sender Tel", cbPhone.FindStringExact(cbPhone.Text) == -1, phone ? cbPhone.Text : null, link);
                InteGr8_Main.template.UpdateBinding("Sender Email", cbEmail.FindStringExact(cbEmail.Text) == -1, email ? cbEmail.Text : null, link);
            }
            DialogResult = DialogResult.OK;
        }

        private void frmWizard03_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK) return;
            if (DialogResult == DialogResult.No)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            DialogResult = DialogResult.Abort;
        }

        private void frmWizard03_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_03");
            e.Cancel = true;
        }
    }
}
