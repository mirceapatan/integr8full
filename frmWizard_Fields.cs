﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard_Fields : Form
    {
        private Table Table = null; // the table for the currently selected category

        public frmWizard_Fields()
        {
            InitializeComponent();

            FieldColumn.DataSource = InteGr8_Main.data.FieldsTable;
            FieldColumn.DataPropertyName = "Id";
            FieldColumn.DisplayMember = "Name";

            // add required fields
            int k = 0;
            foreach (string category in Template.Required_Fields.Keys)
            {
                foreach (string value in Template.Required_Fields[category])
                {
                    // skip the number of packages and total weight, for package level mps 
                    if (Template.MPSPackageLevel && category.Equals("Packages") && (value.Equals("Packages") || value.Equals("Total Weight"))) 
                        continue;

                    // skip the package weight, for shipment level mps 
                    if (!Template.MPSPackageLevel && category.Equals("Packages") && (value.Equals("Package Weight"))) 
                        continue;

                    // find the binding if exists already (editing an existing template)
                    Binding binding = Template.FindBinding(category, value);

                    Bindings.Rows.Add();

                    // hidden field for category
                    Bindings.Rows[k].Cells[0].Value = category;

                    // 2ship field
                    Bindings.Rows[k].Cells[1].Value = value;
                    Bindings.Rows[k].Cells[1].ReadOnly = true;

                    // table name
                    if (binding != null) Bindings.Rows[k].Cells[2].Value = binding.Table == null ? "(none)" : binding.Table.Name;
                    else Bindings.Rows[k].Cells[2].Value = Template.FindTable(TableType.Parse(category)).Name;

                    // link
                    if (binding != null && binding.Table != null && binding.Table != Template.FindTable(TableType.Parse(category.ToString())) && binding.Table.Type == TableType.OtherType)
                    {
                        Bindings.Rows[k].Cells[3].Value = binding.Link;
                    }

                    // column
                    if (binding != null)
                    {
                        Bindings.Rows[k].Cells[4].Value = binding.Column;
                        if (binding.Column != null) ColumnColumn.Items.Add(binding.Column);
                    }

                    // default value
                    if (binding != null) Bindings.Rows[k].Cells[5].Value = binding.Default;

                    k++;
                }
            }

            // load all other existing bindings
            foreach (Binding binding in Template.Bindings)
            {
                // skip required fields (these were populated above)
                if (Template.Required_Fields.Keys.Contains(binding.Field.Category) && Template.Required_Fields[binding.Field.Category].Contains(binding.Field.Name)) continue;
                Bindings.Rows.Add(
                    Data.FieldsTableDataTable.FieldCategoryName(binding.Field.FieldCategory), 
                    binding.Field.Name, 
                    binding.Table == null ? "(none)" : binding.Table.Name, 
                    binding.Table != Template.FindTable(TableType.Parse(Data.FieldsTableDataTable.FieldCategoryName(binding.Field.FieldCategory).ToString())) && binding.Table.Type == TableType.OtherType ? binding.Link : null, 
                    binding.Column, 
                    binding.Default);
                if (binding.Column != null) ColumnColumn.Items.Add(binding.Column);
            }

            Categories_SelectedIndexChanged(this, null);
        }

        private void Categories_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bindings.EndEdit();

            lblTable.Parent = Categories.SelectedTab;
            llTableEdit.Parent = Categories.SelectedTab;
            Bindings.Parent = Categories.SelectedTab;
            
            Data.FieldsTableDataTable.FieldCategory category = Data.FieldsTableDataTable.FieldCategoryParse(Categories.SelectedTab.Text);
            InteGr8_Main.data.FieldsTable.DefaultView.RowFilter = "Category = '" + category.ToString() + "'";
            bool selected = false;
            foreach (DataGridViewRow Row in Bindings.Rows)
            {
                Row.Visible = Row.IsNewRow || Categories.SelectedTab.Text.Equals(Row.Cells[0].Value);
                if (Row.Visible && !selected)
                {
                    // select the first row
                    Row.Selected = true;
                    selected = true;
                }
            }

            Table = Template.FindTable(TableType.Parse(category));

            // repopulate the table comboboxes with the ODBC catalog tables
            TableColumn.Items.Clear();
            TableColumn.Items.Add("(none)");
            if (Table != null)
                TableColumn.Items.AddRange((Table.Type == TableType.ShipmentsType ? Template.WriteConn : Template.ReadConn).Get_Catalog(true, true).ToArray());

            // display a summary about the category table setup
            lblTable.Text = Categories.SelectedTab.Text + " table: " + Table.Name + ", base table: " + Template.Orders;
            llTableEdit.Left = lblTable.Left + lblTable.Width + 20;

            Bindings.Rows[Bindings.NewRowIndex].Cells[2].Value = Table.Name;
        }

        #region Grid Events

        private void Bindings_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells[0].Value = Categories.SelectedTab.Text; // field category (hidden column)
            e.Row.Cells[2].Value = Table.Name; // field table
        }
        
        private void Bindings_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == TableColumn.DisplayIndex)
            {
                string LinkText = null;
                Link Link = null;
                Bindings.Rows[e.RowIndex].Cells[ColumnColumn.DisplayIndex].Value = "";
                object table = Bindings.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                if (table != null && !table.Equals("(none)"))
                {
                    List<Table> Tables = Template.FindTables(table.ToString());
                    if (Tables.Count == 0 || Tables.Count == 1 && Tables[0].Type == TableType.OtherType)
                    {
                        if (Tables.Count == 1) Link = Template.FindLink(Tables[0], Table);
                        else LinkText = "Create";
                    }
                }
                if (Link != null) LinkText = "Edit";
                Bindings.Rows[e.RowIndex].Cells[LinkColumn.DisplayIndex].Value = LinkText;
                Bindings.Rows[e.RowIndex].Cells[LinkColumn.DisplayIndex].Tag = Link;
            }
        }

        private void Bindings_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (Bindings.CurrentCellAddress.X == ColumnColumn.DisplayIndex && e.Control != null)
            {
                ComboBox cb = e.Control as ComboBox;
                string cell_value = cb.Text;
                cb.Items.Clear();
                cb.DropDownStyle = ComboBoxStyle.DropDown;
                object table = Bindings.Rows[Bindings.CurrentCellAddress.Y].Cells[TableColumn.DisplayIndex].Value;
                if (table != null && !table.ToString().Trim().Equals("") && !table.Equals("(none)"))
                    cb.Items.AddRange((Table.Type == TableType.ShipmentsType ? Template.WriteConn : Template.ReadConn).Get_Columns(table.ToString()).ToArray());
                cb.Items.Add(cell_value);
                cb.Text = cell_value;
            }
        }

        private void Bindings_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == ColumnColumn.DisplayIndex && !ColumnColumn.Items.Contains(e.FormattedValue))
                // add the new item to the gridview combo (the combo columns are never used, but will give errors otherwise)
                ColumnColumn.Items.Add(e.FormattedValue);
        }

        private void Bindings_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == LinkColumn.DisplayIndex)
            {
                // the user clicked on the link
                if (Bindings.SelectedRows == null || Bindings.SelectedRows.Count != 1) return;
                object table = Bindings.SelectedRows[0].Cells[TableColumn.DisplayIndex].Value;
                if (table == null || table.ToString().Trim().Equals("")) return;
                Link Link = Bindings.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag as Link;
                frmWizard_DirectLink editor = null;
                if (Link != null) editor = new frmWizard_DirectLink(Link);
                else
                {
                    List<Table> Tables = Template.FindTables(table.ToString());
                    if (Tables.Count == 1) editor = new frmWizard_DirectLink(Tables[0], Table);
                    else editor = new frmWizard_DirectLink(table.ToString(), Table);
                }
                if (editor.ShowDialog(this) == DialogResult.OK)
                {
                    Template.AddLink(editor.Link);
                    Bindings.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag = editor.Link;
                    Bindings.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "Edit";
                }
            }
        }

        #endregion

        private void llTableEdit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Bindings.EndEdit();
            if (!Save()) return;
            
            // go to the required wizard page
            int step = 0;
            switch (Table.Type.Type)
            {
                case TableType.Orders: step = 2; break;
                case TableType.Senders: step = 3; break;
                case TableType.Recipients: step = 4; break;
                case TableType.Packages: step = 5; break;
                case TableType.Products: step = 6; break;
                case TableType.Shipments: step = 7; break;
            }
            new frmWizard_Tables(Table.Type, step).ShowDialog(this.Owner);
        }

        private bool Save()
        {
            // validate
            try
            {
                int errors = 0;
                foreach (DataGridViewRow Row in Bindings.Rows)
                    if (!Row.IsNewRow && Row.Cells[1].Value != null)
                    {
                        object category = Row.Cells[0].Value;
                        object field = Row.Cells[1].Value;
                        object table = Row.Cells[2].Value;
                        object link = Row.Cells[3].Tag;
                        object column = Row.Cells[4].Value;
                        object def = Row.Cells[5].Value;

                        if (category == null || category.ToString().Trim().Equals(""))
                        {
                            MessageBox.Show("Internal error 1.");
                            return false;
                        }
                        if (table == null || table.ToString().Trim().Equals(""))
                        {
                            MessageBox.Show("Internal error 2.");
                            return false;
                        }
                        bool link_needed = false;
                        List<Table> tables = Template.FindTables(table.ToString());
                        if (tables == null || tables.Count == 0) link_needed = true;
                        else link_needed = !"(none)".Equals(table) && tables[0] != Template.FindTable(TableType.Parse(category.ToString())) && tables[0].Type == TableType.OtherType;

                        string error = null;
                        if (field == null || field.ToString().Trim().Equals("")) error = "Please select a 2Ship field.";
                        else if (table == null || table.ToString().Trim().Equals("")) error = "Please select a table.";
                        else if (link_needed && link == null) error = "Please create the table link.";
                        else if ((column == null || column.ToString().Trim().Equals("")) && (def == null || def.ToString().Trim().Equals(""))) error = "Please select a column or enter a default value.";

                        if (error != null)
                        {
                            errors++;
                            Row.ErrorText = error;
                        }
                        else
                        {
                            Row.ErrorText = null;
                        }
                    }
                if (errors > 0)
                {
                    MessageBox.Show(this, "There are " + errors + " validation errors.", "InteGr8 Wizard Field Mapping", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                // save all bindings
                Template.Bindings.Clear();
                foreach (DataGridViewRow Row in Bindings.Rows)
                    if (!Row.IsNewRow) Template.AddBinding(Template.CreateBinding(TableType.Parse(Row.Cells[0].Value.ToString()), Row.Cells[1].Value.ToString(), Row.Cells[2].Value.ToString() == "(none)" ? null : Row.Cells[2].Value.ToString(), Row.Cells[4].Value == null || String.IsNullOrEmpty(Row.Cells[4].Value.ToString()) ? null : Row.Cells[4].Value.ToString(), Row.Cells[5].Value));
            }
            catch (Exception e)
            {
                Utils.SendError(e.Message, e);
                MessageBox.Show(e.Message, "Error!", MessageBoxButtons.OK);
            }
            return true;
           
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Bindings.EndEdit();

            if (!Save()) return;
            DialogResult = DialogResult.OK;
        }

        private void Bindings_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // ignore ... hihihi
        }

        private void Bindings_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            // prevent removal of the required fields
            if (e.Row.Cells[1].ReadOnly)
            {
                e.Cancel = true;
            }
        }

        private void frmWizard_Fields_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_Fields");
            e.Cancel = true;
        }
    }
}
