﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Data.Odbc;

using InteGr8.WS2Ship;
using InteGr8.Properties;
using System.Collections.Specialized;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Drawing.Imaging;
using System.Printing;
using System.Configuration;
using PdfSharp;
using System.Threading;
using InteGr8.WSTwoShip;
using Renci.SshNet;
using System.Globalization;
using CefSharp;
using CefSharp.WinForms;
using CefSharp.MinimalExample.Wpf;
using CefSharp.MinimalExample.WinForms;

namespace InteGr8
{
    public partial class frmMain : Form
    {
        private frmProcessing progress;
        private readonly frmProcessing FillProgress;
        private BackgroundWorker bg_backup_server = new BackgroundWorker();

        private StringCollection OrdersGridColumnsOrder = null;
        private StringCollection PackagesGridColumnsOrder = null;
        public Data backupData = new Data();
        ProductsScan p;
        bool not_manual = false;
        bool manual_order = false;
        bool edit_mod = false;
        public bool refresh_click = false;
        public string template_text;
        bool scanned = true;
        bool errors_orders = false;
        string t = "";

        private Dictionary<int, string> PackageFields = new Dictionary<int, string>() { {77, "Weight"}, {83, "Length"}, {84, "Width"}, {85, "Height"} };

        public ChromiumWebBrowser chromeBrowser;
        public void InitializeChromium()
        {
            CefSettings settings = new CefSettings();
            settings.SetOffScreenRenderingBestPerformanceArgs();
            // Initialize cef with the provided settings
            Cef.Initialize(settings);
            // Create a browser component
            chromeBrowser = new ChromiumWebBrowser("https://www.ship-plus.com");
            chromeBrowser.RequestHandler = new RequestHandler();
            chromeBrowser.LifeSpanHandler = new LifeSpanHandler();
            // Add it to the form and fill it to the form window.
            this.TwoShipPage.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;
        }
        public void InitializeChromium(string url)
        {
            this.TwoShipPage.Controls.Remove(chromeBrowser);
            chromeBrowser = new ChromiumWebBrowser(url);
            chromeBrowser.RequestHandler = new RequestHandler();
            chromeBrowser.LifeSpanHandler = new LifeSpanHandler();
            this.TwoShipPage.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;
        }
        public frmMain()
        {
            InitializeComponent();
            //InitializeChromium();
            progress = new frmProcessing(this.TaskBackgroundWorker);
            FillProgress = new frmProcessing(this.FillBackgroundWorker);
            InitializeChromium(Template.Ship_Link);
            //chromeBrowser = new ChromiumWebBrowser();

            InteGr8_Main.data = this.data;
            //* init all weekdays as shipment pickup days
            data.InitPickUpDays();

            OrdersGrid.AutoGenerateColumns = true;
            OrderPackagesGrid.AutoGenerateColumns = true;
            ProductsGrid.AutoGenerateColumns = true;
            HistoryGrid.AutoGenerateColumns = true;

        }
        private static void downloadFileFromSFTP()
        {
            string host = "ftppickup.2ship.com";
            int port = 443;
            string username = "PrimoFTP1";
            string password = "Pb44555!!";
            string remoteFolder = "/PrimoUpload/";
            using (var client = new SftpClient(host, port, username, password))
            {
                try
                {
                    client.Connect();
                }
                catch (Exception ex)
                {
                    Utils.SendError("Exception at connect to : sftp://ftppickup.2ship.com" + ex.StackTrace);
                }
               
                Renci.SshNet.Sftp.SftpFile[] lines = new Renci.SshNet.Sftp.SftpFile[100];
                try
                {
                    IEnumerable<Renci.SshNet.Sftp.SftpFile> files = client.ListDirectory(remoteFolder);
                    lines = files.ToArray<Renci.SshNet.Sftp.SftpFile>();
                }
                catch (Exception ex)
                {
                    Utils.SendError("Exception at list directory : sftp://ftppickup.2ship.com/PrimoUpload" + ex.StackTrace);
                }

                foreach (Renci.SshNet.Sftp.SftpFile f in lines)
                {
                    if (f.Name.Contains(".txt"))
                    {
                        try
                        {
                            using (var file = File.Create(Path.Combine(".\\", f.Name)))
                            {
                                client.DownloadFile(f.FullName, file);
                            }
                           
                        }
                        catch (Exception ex)
                        {
                         Utils.SendError("Exception at download file from sftp://ftppickup.2ship.com/PrimoUpload/" + ex.StackTrace);
                        }
                    }
                }
                try
                {
                    client.Disconnect();
                }
                catch (Exception ex)
                {
                   Utils.SendError("Exception at disconnect from : sftp://ftppickup.2ship.com" + ex.StackTrace);
                }
            }
        }
      

        private void ChangeButtonsStatus(bool enable)
        {
            // templates toolbar
            TemplatesDropdown.Enabled = enable;
            Options.Enabled = enable;

            //disable wizard buttons
            TemplatesNew.Enabled = false;
            TemplatesEdit.Enabled = false;
            TemplatesDelete.Enabled = false;
            TemplatesImport.Enabled = false;
            TemplatesExport.Enabled = false;

            // orders toolbar
            OrdersProcessCurrentButton.Enabled = enable;
            OrdersRefreshButton.Enabled = enable;
            OrdersProcessAllButton.Enabled = enable;
            
            SelectAllOrdersButton.Enabled = enable;
            UnselectAllOrdersButton.Enabled = enable;
           
            cmdRateAllServices.Enabled = enable;
            cmdRateCheapestService.Enabled = enable;
            cmdRateFastestService.Enabled = enable;
            cmdRateCurrentService.Enabled = enable;
            btnManualOrders.Enabled = enable;
            labelCarrier.Enabled = enable;
            cmbMenuCarrier.Enabled = enable;
            labelService.Enabled = enable;
            cmbMenuService.Enabled = enable;
            btnApplyCarrierServiceToSelected.Enabled = enable;
            if (Properties.Settings.Default.TemplateSetting.Contains("E")) //if the 2ShipEdit page is not visible, disable the Edit and GetEdit buttons
            {
                OrdersEditCurrentButton.Enabled = false;
                GetEditResults.Enabled = false;
            }
            else
            {
                OrdersEditCurrentButton.Enabled = enable;
                GetEditResults.Enabled = enable;
            }
            // shipments toolbar
            ShipmentsLabelButton.Enabled = enable;
            ShipmentsReturnLabel.Enabled = enable;
            ShipmentsCIButton.Enabled = enable;
            ShipmentsBOLPrint.Enabled = enable;
            btnEditRates.Enabled = enable;
            ShipmentsSave.Enabled = enable;
            ShipmentsDelete.Enabled = enable;

            // history toolbar
            HistorySearchTextbox.Enabled = enable;
            HistorySearchButton.Enabled = enable;
            btnHistoryLabel.Enabled = enable;
            btnHistoryCI.Enabled = enable;
            btnHistoryBOLPrint.Enabled = enable;
            btnHistoryReturnLabel.Enabled = enable;

            if (Template.WriteConn.SupportsDelete)
                HistoryDeleteButton.Enabled = enable;
            else
                HistoryDeleteButton.Enabled = false;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                btn_shipments_contentslabel.Visible = false;

                t = Settings.Default.UserWSKey.Equals("") ? (String.IsNullOrEmpty(ConfigurationManager.AppSettings["ClientID"].ToString()) ? (((frmLogin)this.Owner).Controls["txtClientID"] as TextBox).Text : ConfigurationManager.AppSettings["ClientID"].ToString()) : Settings.Default.LoginClient;
                bg_backup_server.WorkerReportsProgress = true;
                bg_backup_server.WorkerSupportsCancellation = true;
                bg_backup_server.DoWork += new DoWorkEventHandler(bg_backup_server_DoWork);
                OrdersVSplitBottom.SplitterDistance = OrdersVSplitBottom.Width / 2 + 100;
                //if there are no special dimensions table, hide the package dimensions option
                if (Properties.Settings.Default.PackageDimensions.Equals(""))
                {
                    cmbPackageDims.Visible = false;
                    label2.Visible = false;
                    cmbPackageDims.DataSource = null;
                }
                ReloadTemplates();

                if (!Properties.Settings.Default.UserWSKey.Equals(""))
                {
                    getAvailableCarriersForClients();
                }
                if (!Properties.Settings.Default.ClientSpecialRates.Equals(""))
                {
                    getSpecialRatesForClients();
                }
                if (TemplatesDropdown.Items.Count > 0)
                {
                    TemplatesDropdown.SelectedIndex = 0;
                }

                //InputWeight - a bool which says if the client wants to set the same weight for all packages
                if (!Properties.Settings.Default.InputWeight)
                {
                    btnGetWeight.Enabled = false;
                }
                btn_aggregate.Visible = Template.Aggregate;
                btn_close.Visible = Template.Aggregate;
                //Edit rates it is not used for any client now, so we will leave it invisible for now
                btnEditRates.Visible = false;
                if (!String.IsNullOrEmpty(Properties.Settings.Default.TemplateSetting))
                {
                    if (Properties.Settings.Default.TemplateSetting.Contains("P"))
                    {
                        ProcessedOrdersPage.Hide();
                        MainPages.TabPages.Remove(ProcessedOrdersPage);
                    }
                    if (Properties.Settings.Default.TemplateSetting.Contains("S"))
                    {
                        ShipmentsPage.Hide();
                        MainPages.TabPages.Remove(ShipmentsPage);
                    }
                    if (Properties.Settings.Default.TemplateSetting.Contains("E"))
                    {
                        TwoShipPage.Hide();
                        MainPages.TabPages.Remove(TwoShipPage);

                        OrdersEditCurrentButton.Enabled = false;
                        GetEditResults.Enabled = false;
                    }
                    if (Properties.Settings.Default.TemplateSetting.Contains("C"))
                    {
                        OrdersPage.Hide();
                        MainPages.TabPages.Remove(OrdersPage);
                    }
                    
                    
                }
                 if (String.IsNullOrEmpty(Select_Filter.Filter_With_Status) && !Template.Name.Contains("Choquette")) 
                {
                    cbMultiOrder.Visible = false;
                    Properties.Settings.Default.MultiOrderShipment = false;
                }
                else
                {
                    Properties.Settings.Default.MultiOrderShipment = cbMultiOrder.Checked;
                }
                if (Template.Name.Contains("DirectDistribution"))
                {
                    ProcessedOrdersPage.Hide();
                    MainPages.TabPages.Remove(ProcessedOrdersPage);
                    ShipmentsPage.Hide();
                    MainPages.TabPages.Remove(ShipmentsPage);
                    TwoShipPage.Hide();
                    MainPages.TabPages.Remove(TwoShipPage);
                    ShipmentsPage.Show();
                    MainPages.TabPages.Add(ShipmentsPage);
                    ProcessedOrdersPage.Show();
                    MainPages.TabPages.Add(ProcessedOrdersPage);
                    TwoShipPage.Show();
                    MainPages.TabPages.Add(TwoShipPage);
                }
                if (Template.Name.Equals("Madcatz 02") )
                {
                    HistoryDeleteButton.Visible = false;
                }
                if (Template.Name.Equals("Surteco"))
                {
                    ShipmentsSave.Visible = false;
                    cbMultiOrder.Enabled = false;
                }
                dtpShipDate.Visible = true;
               

                    //* adding BOL #, BOL URL, PRO # to the shiments grid if they are in template
                    TableType field_type = TableType.OrdersType;
                foreach (Binding Binding in Template.Bindings)
                {
                    // skip the other field types
                    field_type = TableType.Parse(Binding.Field.FieldCategory);
                    //skip the fields that are not Shipment type or are not Enable Return or Print Return Label(fields for enabling and printing return labels)

                    if (Binding.Field.Name.ToString().Equals("Shipment PO Number"))
                    {
                        if (!data.ShipmentsTable.Columns.Contains("Shipment PO Number"))
                        {
                            data.ShipmentsTable.Columns.Add("Shipment PO Number");
                            ShipmentsGrid.Columns.Add(Binding.Field.Name.ToString(), Binding.Field.Name.ToString());
                            ShipmentsGrid.Columns[Binding.Field.Name.ToString()].DisplayIndex = 2;
                            ShipmentsGrid.Columns[Binding.Field.Name.ToString()].DataPropertyName = Binding.Field.Name.ToString();
                        }
                        
                    }

                    if ((field_type != TableType.ShipmentsType))
                    {
                        continue;
                    }
                    if (Binding.Field.Name.ToString() == "BOL #" || Binding.Field.Name.ToString() == "BOL URL" || Binding.Field.Name.ToString() == "PRO #" || Binding.Field.Name.ToString() == "Return Label URL")//|| Binding.Field.Name.ToString() == "Enable Return" || Binding.Field.Name.ToString() == "Print Return Label")
                    {
                        if (!data.ShipmentsTable.Columns.Contains(Binding.Field.Name.ToString()))
                        {
                            ShipmentsGrid.Columns.Add(Binding.Field.Name.ToString(), Binding.Field.Name.ToString());
                            ShipmentsGrid.Columns[Binding.Field.Name.ToString()].DataPropertyName = Binding.Field.Name.ToString();
                        }
                    }
                    if (Binding.Field.Name.ToString() == "Carrier and service")
                    {
                        if (!data.ShipmentsTable.Columns.Contains("Carrier and service"))
                        {
                            ShipmentsGrid.Columns.Add("Carrier and service", "Carrier and service");
                            ShipmentsGrid.Columns["Carrier and service"].DisplayIndex = 2;
                            ShipmentsGrid.Columns["Carrier and service"].DataPropertyName = Binding.Field.Name.ToString();
                        }
                    }

                }
           
                bg_backup_server.RunWorkerAsync();

                if (Template.Name == "")
                {
                    HistoryGrid.Columns["HistoryOrder"].HeaderText = "PPS Number";
                    OrdersGrid.Columns["txtOrderNumber"].HeaderText = "PPS Number";
                    ShipmentsGrid.Columns["orderDataGridViewTextBoxColumn"].HeaderText = "PPS Number";
                    data.OrdersTable.Columns.Add("Charge_Flag", typeof(string));
                    data.OrdersTable.Columns.Add("Order_Num", typeof(string));
                    data.OrdersTable.Columns.Add("Order_Instruction", typeof(string));
                    data.OrderProductsTable.Columns.Add("Scanned Items", typeof(Double));
                    data.OrderProductsTable.Columns.Add("BackOrder", typeof(Double));
                    data.OrderProductsTable.Columns.Add("Scan_Id", typeof(string));
                    data.OrderProductsTable.Columns.Add("Serial_Lot", typeof(string));
                    data.OrderProductsTable.Columns.Add("AllocatedQuantity", typeof(string));
                    data.OrderProductsTable.Columns.Add("ItemNumber", typeof(string));
                    data.OrderProductsTable.Columns.Add("Product_Instruction", typeof(string));
                    data.OrderProductsTable.Columns.Add("Serial_Id", typeof(string));
                    data.OrderProductsTable.Columns.Add("BackOrder_Flag", typeof(Double));
                    HistoryDeleteButton.Visible = false;
                    btnManualOrders.Visible = false;
                }
                else
                {
                    btnScanProducts.Visible = false;
                }
                template_text = TemplatesDropdown.Text;
                if (Template.Name == "FineLineGraphics")
                {
                    MainPages.TabPages.RemoveAt(3);
                    btnEditRates.Visible = false;
                    ShipmentsSave.Visible = false;
                }
                if (Template.Name == "MAAX")
                {
                    btnEditRates.Visible = false;
                    ShipmentsSave.Visible = false;
                    toolStripLabel2.Visible = false;
                    OrderSearchTextbox.Visible = false;
                    OrderSearchButton.Visible = false;
                    btnManualOrders.Visible = false;
                    cmbFilterColumns.Visible = false;
                    cmbFilterColumns.Enabled = false;
                    labelOperator.Visible = false;
                    btnAcceptFilter.Visible = false;
                    FilterLinkLabel.Visible = false;
                    label1.Visible = false;
                    FilterTableLayoutPanel.SetColumn(tbValue1, 0);
                    tbValue1.Dock = DockStyle.Fill;
                }
                Utils.IsAdobe(true);
                //clear Reply and Request files here, if necessary
                   ClearRequestReplyLog();


                   //using (StreamWriter write = File.AppendText("OrdersTest.txt"))
                   //{
                   //    write.WriteLine("Columns order at load finish");
                   //    write.WriteLine("Index  Column name");
                   //    foreach (DataGridViewColumn col in OrdersGrid.Columns)
                   //    {
                   //        write.WriteLine(col.DisplayIndex + "   -   " + col.Name);
                   //    }
                   //}

            }
            catch (Exception ex)
            {
                MessageBox.Show("An internal error has occured - an email will be sent to 2Ship support.", "InteGr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utils.SendError("InteGr8 error", ex);
                Application.Exit();
            }
        }

        private static void ClearRequestReplyLog()
        {
            if (File.Exists(Application.StartupPath + ".\\ReplyTest.txt"))
            {
                if ((new FileInfo(Application.StartupPath + ".\\ReplyTest.txt")).Length >= 5000000) //empty the ReplyTest when it reaches 5MB(~ one week)
                {
                    File.WriteAllText(Application.StartupPath + ".\\ReplyTest.txt", string.Empty);
                    //empty the RequestTest too - this has way less data than ReplyTest does
                    if (File.Exists(Application.StartupPath + "\\RequestTest.txt") )
                    {
                        File.WriteAllText(Application.StartupPath + "\\RequestTest.txt", string.Empty);
                    }
                }
            }
            
        }

        private void getSpecialRatesForClients()
        {
            try
            {
                if (!File.Exists(Properties.Settings.Default.ClientSpecialRates))
                {
                    MessageBox.Show("There is no file for clients special rates!");
                    return;
                }
                //the special rates are in a csv file in the form ClientID,FreightRate,Upcharge
                foreach (string line in File.ReadAllLines(Properties.Settings.Default.ClientSpecialRates))
                {
                    string[] cols = line.Split(',');
                    //skip the first line
                    if (cols[0].Equals("ClientID"))
                    {
                        continue;
                    }
                    data.ClientSpecialRates.AddClientSpecialRatesRow(cols[0], cols[1], cols[2]);
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error reading the clients special rates from file", ex);
            }
        }

        private void getAvailableCarriersForClients()
        {
            try
            {
                if (!Properties.Settings.Default.AvailableCarriers.Equals(""))
                {
                    if (File.Exists(Properties.Settings.Default.AvailableCarriers))
                    {
                        File.Delete(Properties.Settings.Default.AvailableCarriers);
                    }
                    if (!File.Exists(Properties.Settings.Default.Clients)){
                        return;
                    }
                    data.Clients.ReadXml(Properties.Settings.Default.Clients);
                    //string[] CarriersSplitCriteria = Properties.Settings.Default.CarrierCriteria.Split(';');
                    CarrierReply[] carrierCodes = null;
                    if (data.Clients.Rows.Count > 0)//(CarriersSplitCriteria.Length > 0)
                    {
                        foreach (Data.ClientsRow clients in data.Clients.Rows)//(string id in CarriersSplitCriteria)
                        {
                            //we have to replace LoginToken with the client's actual WSKey taken from database
                            try
                            {
                                //if (clients["ClientID"].ToString().Equals("2325"))
                                //{
                                //    bool found = true;
                                //}
                                carrierCodes = (new InteGr8Task(clients["ClientID"].ToString(), Convert.ToInt32(clients["Location"]), clients["WSKey"].ToString())).carriers;
                                foreach (CarrierReply carr in carrierCodes)
                                {
                                    if (data.AvailableCarriers.FindByLocation_Carrier(clients["Location"].ToString(), carr.Id.ToString()) == null)
                                    {
                                        data.AvailableCarriers.AddAvailableCarriersRow(clients["Location"].ToString(), carr.Id.ToString());//(clients["ClientID"].ToString(), carr.Id.ToString());
                                    }

                                }
                            }
                            catch(Exception eTest)
                            {
                                Utils.WriteLine("Error.txt","Exception at getAvailableCarriers for client " + clients["ClientID"].ToString() + " Location " + clients["Location"].ToString(),eTest.Message + eTest.StackTrace);
                            }
                        }
                         data.AvailableCarriers.WriteXml(Path.Combine(Path.GetDirectoryName(Properties.Settings.Default.CarriersFile), "AvailableCarriers.xml"));
                    }
                }
                else
                {
                    data.AvailableCarriers.ReadXml(Properties.Settings.Default.AvailableCarriers);
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error reading data from AvailableCarriers.xml file", ex);
            }
        }

        private void HideSender()
        {
            try
            {
                if (OrdersGrid.Columns.Contains("Sender Country"))
                {
                    OrdersGrid.Columns["Sender Country"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender Company"))
                {
                    OrdersGrid.Columns["Sender Company"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender Contact"))
                {
                    OrdersGrid.Columns["Sender Contact"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender State / Province"))
                {
                    OrdersGrid.Columns["Sender State / Province"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender ZIP / Postal Code"))
                {
                    OrdersGrid.Columns["Sender ZIP / Postal Code"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender City"))
                {
                    OrdersGrid.Columns["Sender City"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender Address 1"))
                {
                    OrdersGrid.Columns["Sender Address 1"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender Address 2"))
                {
                    OrdersGrid.Columns["Sender Address 2"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender Tel"))
                {
                    OrdersGrid.Columns["Sender Tel"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender Email"))
                {
                    OrdersGrid.Columns["Sender Email"].Visible = true;
                }
                if (OrdersGrid.Columns.Contains("Sender Tax ID"))
                {
                    OrdersGrid.Columns["Sender Tax ID"].Visible = true;
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error hiding sender address for login with token", ex);
            }
        }

        private void TemplatesDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            data.ClearTemplate();
            if (TemplatesDropdown.Text.Equals("")) return;
            Template.Load(TemplatesDropdown.Text);
            filterExpressionBindingSource.DataSource = Select_Filter.Expressions;
            filterLeftOperandNameBindingSource.DataSource = Select_Filter.Expressions;
            ReloadInfo();
            template_text = TemplatesDropdown.Text;

            
        }

        private void OrdersRefreshButton_Click(object sender, EventArgs e)
        {
            //save the packages columns order before reseting the datasource
            SaveColumns();
            OrdersGridColumnsOrder = null;
            PackagesGridColumnsOrder = null;
            refresh_click = true;
            ReloadInfo();
          
           
        }

        private void ReloadInfo()
        {
            try
            {
                ChangeButtonsStatus(false);
                manual_order = false;
                FilterTableLayoutPanel.Visible = !Select_Filter.Is_Empty;

                OrderFiltersLabel.ForeColor = Color.Black;
                OrderFiltersLabel.Text = "Orders filter:";
                ToolTip.SetToolTip(FilterLinkLabel, Select_Filter.Filter_No_Status);
                OrdersLabel.Text = "Please wait...";
                tbCurrentRate.Text = "";
                tbDimensionalWeight.Text = "";
                //don't reset the bindings if is MultiShipment - this is not needed here - the bindings need reseting, because a new order will be added to the list
                //if (!Properties.Settings.Default.MultiOrderShipment)
                //{
                    fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = false;
                    fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = false;
                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = false;
                    fKOrdersTableOrderProductsTableBindingSource.RaiseListChangedEvents = false;
                    fKOrdersTableRequestsTableBindingSource.RaiseListChangedEvents = false;
                    OrdersTableBindingSource.RaiseListChangedEvents = false;

                    fkOrdersTableOrderSkidsTableBindingSource.SuspendBinding();
                    fkOrdersTableLTLItemsBindingSource.SuspendBinding();
                    fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();
                    fKOrdersTableOrderProductsTableBindingSource.SuspendBinding();
                    fKOrdersTableRequestsTableBindingSource.SuspendBinding();
                    OrdersTableBindingSource.SuspendBinding();

                    this.OrderPackagesGrid.DataSource = null;
                    this.OrderSkidsGrid.DataSource = null;
                    this.ProductsGrid.DataSource = null;
                    this.LTLItemsGrid.DataSource = null;
                //}
                //else
                //{
                    //this is a multiple orders per shipment case, so don't reset the bindings
                //}
                //TODO schimba din show in shiow dialog mai jos, cu restul codului ce implica schimbarea
                //make the multi order shipment changes for the FillBackWorker too
                FillProgress.Show(this);

                FillBackgroundWorker.RunWorkerAsync(TemplatesDropdown.Text);


                //using (StreamWriter write = File.AppendText("OrdersTest.txt"))
                //{
                //    write.WriteLine("Columns order after Reload info");
                //    write.WriteLine("Index  Column name");
                //    foreach (DataGridViewColumn col in OrdersGrid.Columns)
                //    {
                //        write.WriteLine(col.DisplayIndex + "   -   " + col.Name);
                //    }
                //}
                if (!String.IsNullOrEmpty(Settings.Default.PackageDimensions))
                {
                    cmbPackageDims.SelectedIndex = 0;
                }
                tbCurrentRate.Text = "";
                tbDimensionalWeight.Text = "";
            }
            catch (Exception ex)
            {
                Utils.SendError("Error at loading data from database", ex);
                FillProgress.Close();
            }

            
        }

        private void OrdersEditCurrentButton_Click(object sender, EventArgs e)
        {
            edit_mod = true;
            not_manual = true;
            // open the shipment in browser for editing
            EndEdit();
            DataRowView order = OrdersTableBindingSource.Current as DataRowView;
            try
            {
                if (order == null)
                {
                    return;
                }
                //change the order's status to "Edit", so that we know to get the edit results for it
                if (data.OrdersTable.Columns.IndexOf("Status") >= 0)
                {
                    order["Status"] = "Edit";
                }
                //add the HandlingFee here, if it applies:
                if (data.OrdersTable.Columns.IndexOf("HandlingFee") >= 0)
                {
                    string rate = "";
                    if (!String.IsNullOrEmpty(tbCurrentRate.Text))
                    {
                        rate = tbCurrentRate.Text;
                    }
                    //else
                    //{
                    //    DialogResult result = MessageBox.Show("Current service was not rated yet, continue anyway?", "Order processing", MessageBoxButtons.OKCancel);
                    //    if (result == DialogResult.Cancel)
                    //    {
                    //        return;
                    //    }
                    //}
                    string HandlingFee = frmInputBox.InputBox(this, "Service Charge / Handling Fee. \n\n (The current rate is: " + rate + ")");
                    order["HandlingFee"] = String.IsNullOrEmpty(HandlingFee) ? "0" : HandlingFee;
                }

                InteGr8Task task = new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.Edit, order["Order #"].ToString());
                task.Process();
                MainPages.SelectedTab = TwoShipPage;
                //Browser.ScriptErrorsSuppressed = true;
                //Browser.Navigate(task.EditResult.TwoShipURL);//.Result);
                InitializeChromium(task.EditResult.TwoShipURL);
                //this.TwoShipPage.Controls.Remove(chromeBrowser);
                //chromeBrowser = new ChromiumWebBrowser(task.EditResult.TwoShipURL);
                //this.TwoShipPage.Controls.Add(chromeBrowser);
                //chromeBrowser.Dock = DockStyle.Fill;
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message.StartsWith("#"))
                {
                    MessageBox.Show(this, ex.InnerException.Message.Substring(1), "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (manual_order)
                    {
                        Data.OrdersTableRow[] rows = data.OrdersTable.Select("[Order #] = '" + order["Order #"].ToString() + "'") as Data.OrdersTableRow[];
                        foreach (Data.OrdersTableRow r in rows)
                        {
                            if (r.Status == "")
                            {
                                data.OrdersTable.Rows.Remove(r);
                            }
                        }
                    }
                }
                else
                {
                    Utils.SendError("Error in OrdersEditCurrentButton_Click", ex);
                    MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (manual_order)
                {
                    errors_orders = true;
                    manual_order = false;
                }
            }
        }

        private void OrdersProcessCurrentButton_Click(object sender, EventArgs e)
        {
            EndEdit();
            try
            {
                if (Properties.Settings.Default.MultiOrderShipment == true)
                {
                    if (OrdersTableBindingSource != null)
                    {
                        foreach (DataRowView r in OrdersTableBindingSource)
                        {
                            r["Selected"] = false;
                        }
                        OrdersTableBindingSource.Position = 0;
                    }
                }
                if (OrdersTableBindingSource == null)
                {
                    MessageBox.Show("Please select an order!");
                    return;
                }
                DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
                if (order_row == null)
                {
                    return;
                }
                Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_row.Row["Order #"].ToString());
                if (Template.Name == "")
                {
                    Data.OrderProductsTableRow[] productsList = order.GetOrderProductsTableRows();
                    foreach (Data.OrderProductsTableRow p in productsList)
                    {
                        if (Convert.ToDecimal(p["AllocatedQuantity"].ToString().Trim()) != Convert.ToDecimal(p["Scanned Items"].ToString().Trim()))
                        {
                            DialogResult r = MessageBox.Show("Short Ship PPS: " + p._Order__ + " ?", "", MessageBoxButtons.OKCancel);
                            if (r != DialogResult.OK)
                            {
                                return;
                            }
                            break;
                        }
                    }
                }

                

                object order_number = order_row.Row["Order #"];
                if (order_number == null || order_number.Equals(""))
                {
                    return;
                }

                //try to uncheck the Selected column from OrdersGrid, so that the shipped order will be removed from Orders list
                ((DataRowView)OrdersTableBindingSource.Current)["Selected"] = false;
                if (data.ShipmentsTable.FindBy_Order__Package_Index(order_number.ToString(), 0) != null)
                {
                    MessageBox.Show("Order " + order_number.ToString() + " has already been shipped and it is in the Shipments table! Save this shipment to database first or delete it from Shipments table and try again", "InteGr8 - Ship", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (order["Carrier"] == null && data.OrdersTable.FindBy_Order__(order_number.ToString()).Carriers_MappingRow == null)
                {
                    MessageBox.Show(this, "Please select a carrier", "Integr8 - Ship", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.IsNullOrEmpty(order["Carrier"].ToString()))
                {
                    MessageBox.Show(this, "Please select a carrier", "Integr8 - Ship", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (data.Carriers.FindByCode(order["Carrier"].ToString()).Carrier_Code == "999")
                {
                    MyCarrier c = new MyCarrier(data);
                    if (c.ShowDialog(this) != DialogResult.OK)
                    {
                        return;
                    }
                }

                if (data.OrdersTable.Columns.IndexOf("HandlingFee") >= 0)
                {
                    string rate = "";
                    if (!String.IsNullOrEmpty(tbCurrentRate.Text))
                    {
                        rate = tbCurrentRate.Text;
                    }
                    
                    string HandlingFee = frmInputBox.InputBox(this, "Service Charge / Handling Fee. \n\n (The current rate is: "+ rate + ")");
                    order["HandlingFee"] = String.IsNullOrEmpty(HandlingFee) ? "0" : HandlingFee;
                }

                if (data.Carriers.FindByCode(order["Carrier"].ToString()).Carrier_Code == "8" && data.OrdersTable.Columns.Contains("Enable Return") )
                {
                    if (order["Enable Return"].ToString().Equals("1"))
                    {
                        UPSReturnServiceType ups = new UPSReturnServiceType(order_number.ToString());
                        if (ups.ShowDialog(this) != DialogResult.OK)
                        {
                            return;
                        }
                        UPSReturnServiceFields(order, ups);
                    }
                }
                if (!order["Billing Type"].ToString().Equals("1"))
                {
                    if (Template.Name.Equals("FootPrint"))
                    {
                        frmBillingInfo billing = new frmBillingInfo();

                        if (billing.ShowDialog(this) != DialogResult.OK)
                        {
                            return;
                        }
                        else
                        {
                            AddBillingInfoFields(order, billing.getBillingInfo);
                        }
                    }
                    else
                    {
                        if (data.OrdersTable.Columns.IndexOf("Billing Account") < 0)
                        {
                            data.OrdersTable.Columns.Add("Billing Account");
                        }
                        if (String.IsNullOrWhiteSpace(order["Billing Account"].ToString()))
                        {
                            order["Billing Account"] = frmInputBox.InputBox(this, "Billing Account");
                        }

                    }

                }

                Data.ProductsInPackagesRow[] rows = data.ProductsInPackages.Select("[Order #] = '" + order_number + "'") as Data.ProductsInPackagesRow[];
                foreach (Data.ProductsInPackagesRow r in rows)
                {
                    BackupFile.AddShipment(data.ProductsInPackages.TableName, r as DataRow, data);
                }
                Data.ProductsSerialNumbersRow[] rows2 = data.ProductsSerialNumbers.Select("[Order #] = '" + order_number + "'") as Data.ProductsSerialNumbersRow[];
                foreach (Data.ProductsSerialNumbersRow r in rows2)
                {
                    BackupFile.AddShipment(data.ProductsSerialNumbers.TableName, r as DataRow, data);
                }



                OrdersTableBindingSource.RaiseListChangedEvents = false;
                OrdersTableBindingSource.SuspendBinding();

                StartTasks(new InteGr8Task[] { new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.Ship, order_number.ToString()) });

                OrdersTableBindingSource.RaiseListChangedEvents = true;
                OrdersTableBindingSource.ResetBindings(true);
                OrdersTableBindingSource.ResumeBinding();
                BackupFile.LoadSettings(t);

                //if just one order, and it is removed from OrdersBindingSource, then some actions in Shipments and History tab will not work
                //because they use the BindingSource.Current row 

                SetRowNumbers();
                RestoreOrdersGridColumnsOrder();
                //if (cbMultiOrder.Checked)
                //{
                //    cbMultiOrder.Checked = false;
                //}
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in OrdersProcessCurrentButton_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddBillingInfoFields(Data.OrdersTableRow order, List<string> billingInfo)
        {
            if (data.OrdersTable.Columns.IndexOf("Billing Account") < 0)
            {
                data.OrdersTable.Columns.Add("Billing Account");
            }
            order["Billing Account"] = billingInfo[0];
            if (data.OrdersTable.Columns.IndexOf("Billing Company") < 0)
            {
                data.OrdersTable.Columns.Add("Billing Company");
            }
            order["Billing Company"] = billingInfo[1];
            if (data.OrdersTable.Columns.IndexOf("Billing Contact") < 0)
            {
                data.OrdersTable.Columns.Add("Billing Contact");
            }
            order["Billing Contact"] = billingInfo[2];
            if (data.OrdersTable.Columns.IndexOf("Billing Country") < 0)
            {
                data.OrdersTable.Columns.Add("Billing Country");
            }
            order["Billing Country"] = billingInfo[3];
            if (data.OrdersTable.Columns.IndexOf("Billing State") < 0)
            {
                data.OrdersTable.Columns.Add("Billing State");
            }
            order["Billing State"] = billingInfo[4];
            if (data.OrdersTable.Columns.IndexOf("Billing City") < 0)
            {
                data.OrdersTable.Columns.Add("Billing City");
            }
            order["Billing City"] = billingInfo[5];
            if (data.OrdersTable.Columns.IndexOf("Billing Address 1") < 0)
            {
                data.OrdersTable.Columns.Add("Billing Address 1");
            }
            order["Billing Address 1"] = billingInfo[6];
            if (data.OrdersTable.Columns.IndexOf("Billing Address 2") < 0)
            {
                data.OrdersTable.Columns.Add("Billing Address 2");
            }
            order["Billing Address 2"] = billingInfo[7];
            if (data.OrdersTable.Columns.IndexOf("Billing ZIP") < 0)
            {
                data.OrdersTable.Columns.Add("Billing ZIP");
            }
            order["Billing ZIP"] = billingInfo[8];
            if (data.OrdersTable.Columns.IndexOf("Billing Tel") < 0)
            {
                data.OrdersTable.Columns.Add("Billing Tel");
            }
            order["Billing Tel"] = billingInfo[9];
            if (data.OrdersTable.Columns.IndexOf("Billing Email") < 0)
            {
                data.OrdersTable.Columns.Add("Billing Email");
            }
            order["Billing Email"] = billingInfo[10];
        }

        private void UPSReturnServiceFields(Data.OrdersTableRow order, UPSReturnServiceType ups)
        {
            if (!data.OrdersTable.Columns.Contains("Print Return Label"))
            {
                data.OrdersTable.Columns.Add("Print Return Label");
               
            }
            order["Print Return Label"] = "-1";
            if (!data.OrdersTable.Columns.Contains("UPS Return Type"))
            {
                data.OrdersTable.Columns.Add("UPS Return Type");
                
            }
            order["UPS Return Type"] = ups.returnService;
            if (ups.returnService.Equals("8"))
             {
                if (!data.OrdersTable.Columns.Contains("UPSReturnFromName"))
                {
                    data.OrdersTable.Columns.Add("UPSReturnFromName");
                    
                }
                order["UPSReturnFromName"] = ups.returnFrom;
                if (!data.OrdersTable.Columns.Contains("UPSReturnEmailAddress"))
                {
                    data.OrdersTable.Columns.Add("UPSReturnEmailAddress");
                    
                }
                order["UPSReturnEmailAddress"] = ups.returnEmail;
                if (!data.OrdersTable.Columns.Contains("UPSReturnEmailSubject"))
                {
                    data.OrdersTable.Columns.Add("UPSReturnEmailSubject");
                    
                }
                order["UPSReturnEmailSubject"] = ups.returnSubject;
            }
        }

        private void OrdersProcessAllButton_Click(object sender, EventArgs e)
        {
            EndEdit();
            if (MessageBox.Show(this, "This will start the automatic shipping for all the selected orders. Continue?", "Integr8 - Ship all confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    DataView orders = OrdersTableBindingSource.List as DataView;
                    if (Template.Name == "")
                    {
                        foreach (DataRowView order_row in orders)
                        {
                            bool order_selected = order_row["Selected"] != null && !order_row["Selected"].Equals(DBNull.Value) && Convert.ToBoolean(order_row["Selected"]);
                            if (!order_selected) continue;
                            Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_row.Row["Order #"].ToString());
                            Data.OrderProductsTableRow[] productsList = order.GetOrderProductsTableRows();
                            foreach (Data.OrderProductsTableRow p in productsList)
                            {
                                if (Convert.ToDecimal(p["BackOrder"].ToString()) != 0)
                                {
                                    DialogResult r = MessageBox.Show("Short Ship PPS: " + p._Order__ + " ?", "", MessageBoxButtons.OKCancel);
                                    if (r != DialogResult.OK)
                                    {
                                        return;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (orders == null)
                    {
                        return;
                    }
                    List<InteGr8Task> tasks = new List<InteGr8Task>(orders.Count);
                        foreach (DataRowView order_row in orders)
                        {
                            object order_number = order_row["Order #"];
                            bool order_selected = order_row["Selected"] != null && !order_row["Selected"].Equals(DBNull.Value) && Convert.ToBoolean(order_row["Selected"]);
                            if (order_number == null || !order_selected || order_number.Equals(""))
                            {
                                continue;
                            }
                            if (data.ShipmentsTable.FindBy_Order__Package_Index(order_number.ToString(), 0) != null)
                            {
                                MessageBox.Show("Order " + order_number.ToString() + " has already been shipped and it is in the Shipments table! Save this shipment to database first or delete the shipment before shipping it again", "InteGr8 - Ship", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            //*the orders selected for processing must have a carrier selected
                            if (order_selected && data.OrdersTable.FindBy_Order__(order_number.ToString()).Carriers_MappingRow == null)
                            {
                                MessageBox.Show(this, "Please select a carrier", "Integr8 - Ship", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            if (data.OrdersTable.Columns.IndexOf("HandlingFee") >= 0)
                            {
                                string HandlingFee = frmInputBox.InputBox(this, "Service Charge / Handling Fee");
                                order_row["HandlingFee"] = String.IsNullOrEmpty(HandlingFee) ? "0" : HandlingFee;
                            }

                            //if (!order_row["Billing Type"].ToString().Equals("1"))
                            //{
                            //   if (data.OrdersTable.Columns.IndexOf("Billing Account") < 0)
                            //    {
                            //        data.OrdersTable.Columns.Add("Billing Account");
                            //    }
                            //    if (String.IsNullOrWhiteSpace(order_row["Billing Account"].ToString()))
                            //    {
                            //        order_row["Billing Account"] = frmInputBox.InputBox(this, "Billing Account");
                            //    }
                            //}

                            if (!order_row["Billing Type"].ToString().Equals("1"))
                            {
                                if (Template.Name.Equals("FootPrint"))
                                {
                                    frmBillingInfo billing = new frmBillingInfo();

                                    if (billing.ShowDialog(this) != DialogResult.OK)
                                    {
                                        return;
                                    }
                                    else
                                    {
                                        AddBillingInfoFields((Data.OrdersTableRow)order_row.Row, billing.getBillingInfo);
                                    }
                                }
                                else
                                {
                                    if (data.OrdersTable.Columns.IndexOf("Billing Account") < 0)
                                    {
                                        data.OrdersTable.Columns.Add("Billing Account");
                                    }
                                    if (String.IsNullOrWhiteSpace(order_row["Billing Account"].ToString()))
                                    {
                                        order_row["Billing Account"] = frmInputBox.InputBox(this, "Billing Account");
                                    }

                                }

                            }


                            tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.Ship, order_number.ToString()));
                            //* remove the shipped orders from current Orders Grid
                            order_row["Selected"] = false;
                        }

                      if (tasks.Count == 0)
                    {
                        return;
                    }
                    MainPages.SelectedTab = ShipmentsPage;
                    
                    StartTasks(tasks.ToArray());
                    SetRowNumbers();
                }
                catch (Exception ex)
                {
                    Utils.SendError("Error in OrdersProcessAllButton_Click", ex);
                    MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        //not used, it is just for testing purposes
        private void DeleteShipments()
        {
            try
            {
                //* get the order # which must be deleted from the shipments header binding source
                //*DataRowView order = shipmentsHeaderTableBindingSource.Current as DataRowView;
                //*shipmentsTableBindingSource.IndexOf(orderDataGridViewTextBoxColumn as obj);?? - this is necesary to get the package index
                int rowCount = ShipmentsGrid.Rows.Count;

                for (int k = rowCount - 1; k >= 0; k--)
                {
                    DataGridViewRow r = ShipmentsGrid.Rows[k];
                    //delete all selected shipments, if is just one, delete it without needing to have the Selected checkbox checked
                    //if (r.Cells["ShipmentsSelectedColumn"].Value.Equals(true) || (rowCount == 1))
                    //{
                        shipmentsTableBindingSource.Position = r.Index;

                        DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
                        if (order == null)
                        {
                            return;
                        }
                        Data.ShipmentsTableRow shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), Convert.ToInt32(order.Row["Package_Index"]));
                        //in case of an error, delete the shipment only from shipments table and return
                        if (shipment == null || shipment.Shipment_Tracking_Number.Equals("N/A"))
                        {
                            int packs = data.OrdersTable.FindBy_Order__(order.Row["Order #"].ToString()).GetOrderPackagesTableRows().Count();
                            for (int p = 1; p < packs; p++)
                            {
                                shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), p);
                                if (shipment != null)
                                {
                                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                    data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                                }
                            }
                            shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), 0);
                            if (shipment != null)
                            {
                                BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                            }
                            return;
                        }
                        ChangeButtonsStatus(false);

                        // the ws will delete all packages
                        string shipmentTrackingNr = shipment.Shipment_Tracking_Number;
                        string orderNr = order.Row["Order #"].ToString();
                        string orderToken = "";
                        //if (data.ShipmentsTable.Columns.IndexOf("Token") >= 0)
                        //{
                        //    orderToken = shipment["Token"].ToString();
                        //}
                        //else
                        //{
                        //    orderToken = InteGr8_Main.token.GetToken();
                        //}

                        // remove all packages of this shipment from the shipments table
                        //in case of filtered orders, only the last saved order is still in orders table

                       

                        if (data.OrdersTable.FindBy_Order__(order.Row["Order #"].ToString()) != null)
                        {
                            int packages = data.OrdersTable.FindBy_Order__(order.Row["Order #"].ToString()).GetOrderPackagesTableRows().Count();
                            for (int p = 1; p <= packages; p++)
                            {
                                shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), p);
                                if (shipment != null)
                                {
                                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                    data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                                }
                            }
                            shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), 0);
                            if (shipment != null)
                            {
                                BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                            }
                        }
                        else
                        {
                            int shipments = data.ShipmentsTable.Rows.Count;
                            for (int i = shipments - 1; i >= 0; i--)
                            {
                                shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), i);
                                if (shipment != null)
                                {
                                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                    data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                                }
                            }
                        }

                        // the ws will delete all packages
                        //using (ShipService ws = new ShipService())
                        //{
                        //    ws.Delete(orderToken, shipmentTrackingNr);
                        //}
                        try
                        {
                            using (I2ShipServiceClient ws = new I2ShipServiceClient())
                            {
                                DeleteRequest del = new DeleteRequest();
                                del.WS_Key = InteGr8_Main.key.MyWSKey;
                                del.DeleteType = DeleteType.ByTrackingNumber;
                                del.TrackingNumber = shipmentTrackingNr;
                                ws.DeleteShipment(del);
                            }
                        }
                        catch (Exception ex)
                        {
                        if(! ex.Message.Contains("Tracking Number not found, or shipment closed off already"))
                                Utils.SendError("Exception at deleting shipment: " + orderNr + "\n" + ex.Message + ex.StackTrace);
                        }
                        //when an order is deleted from shipments table without being saved back to database, 
                        //it should be reloaded in orders grid again
                        if (data.OrdersTable.FindBy_Order__(orderNr) != null)
                        {
                            Data.OrdersTableRow row = data.OrdersTable.FindBy_Order__(orderNr);
                            row["Status"] = "";
                            row["Selected"] = true;
                        }
                    //}
                }
                shipmentsTableBindingSource.ResetBindings(true);
                shipmentsTableBindingSource.Filter = "Package_Index = 0";
                ChangeButtonsStatus(true);

                SetRowNumbers();
                
            }
            catch (Exception ex)
            {
                ChangeButtonsStatus(true);
                Utils.SendError("Error in ShipmentsDelete_Click", ex);
                MessageBox.Show(this, "Error at delete. \r\n (" + ex.Message + ")", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ShipmentsLabelButton_Click(object sender, EventArgs e)
        {
            DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
            if (order == null)
            {
                MessageBox.Show("There is no order selected");
                return;
            }
            try
            {
                //DO NOT autoprint the label when Label button is clicked and shipment selected wor anyone other than MAAX!!!
                if (Template.Name.Equals("MAAX")) // leave itfor MAAX, they may have been the ones who wanted it, add it for DD too, they want it also
                {
                    List<print_class> temp = new List<print_class>();
                bool ok = false;
                foreach (Data.ShipmentsTableRow rr in data.ShipmentsTable)
                {
                    if (rr.Selected == true)
                    {
                        ok = true;
                        break;
                    }
                }

                if (ok)
                {
                    if (!bg_print.IsBusy)
                    {
                        foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                        {
                            if (shipment.Selected == true && shipment.Package_Index.ToString().Equals("0"))
                            {
                                print_class print = new print_class();
                                print.order = shipment._Order__;
                                //new code
                                //Data.ShipmentsTableRow ship = data.ShipmentsTable.FindBy_Order__Package_Index(shipment._Order__, 0);
                                print.label = shipment["Label URL"].ToString();
                                print.commercial_invoice = "";
                                print.bol = "";
                                print.return_label = "";
                                //old code - this one adds the labels to the printing list for each package in shipment, so for a 9 pks shipment would add 10 lines * 9 labels = 90 labels(10 lines - 0 shipment level and the 9 levels for packages)
                                //foreach (DataRow r in data.ShipmentsTable.Rows)
                                //{
                                //    if (r["Order #"].ToString().Equals(print.order))
                                //    {
                                //        print.label = r["Label URL"].ToString();
                                //        print.commercial_invoice = "";
                                //        print.bol = "";
                                //        print.return_label = "";
                                //        break;
                                //    }
                                //}
                                if (print.label != "" && print.label != "N/A")
                                {
                                    temp.Add(print);
                                }
                            }
                        }
                        if (print_list.Count == 0 && temp.Count != 0)
                        {
                            
                            print_list.AddRange(temp);
                            bg_print.RunWorkerAsync();
                        }
                    }
                    else
                    {
                        MessageBox.Show("InteGr8 is still printing, please try again later.");
                    }
                }
                }
                else
                {
                    //* get the order # from the shipmentsHeader it must have Package_index too!!
                    Data.ShipmentsTableRow shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), Convert.ToInt32(order.Row["Package_Index"]));
                    if (shipment == null || shipment.Label_URL.Equals("") || shipment.Label_URL.Equals("N/A") || shipment.Label_URL == null)
                    {
                        MessageBox.Show("There is no url for label");
                        return;
                    }
                    Utils.openPDF(shipment.Label_URL, Template.UseAutoprint ? (Template.AutoprintLabelsFolder != "" ? Template.AutoprintLabelsFolder : Application.LocalUserAppDataPath) : Application.LocalUserAppDataPath);
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in ShipmentsLabelButton_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ShipmentsCIButton_Click(object sender, EventArgs e)
        {
            DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
            if (order == null)
            {
                MessageBox.Show("There is no order selected");
                return;
            }
            try
            {
                if (Template.Name.Equals("MAAX"))
                {
                    List<print_class> temp = new List<print_class>();
                    bool ok = false;
                    foreach (Data.ShipmentsTableRow rr in data.ShipmentsTable)
                    {
                        if (rr.Selected == true)
                        {
                            ok = true;
                            break;
                        }
                    }

                    if (ok)
                    {
                        if (!bg_print.IsBusy)
                        {
                            foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                            {
                                if (shipment.Selected == true && shipment.Package_Index.ToString().Equals("0"))
                                {
                                    print_class print = new print_class();
                                    print.order = shipment._Order__;
                                    //new code
                                    //Data.ShipmentsTableRow ship = data.ShipmentsTable.FindBy_Order__Package_Index(shipment._Order__, 0);
                                    print.label = "";
                                    print.commercial_invoice = shipment["Commercial Invoice URL"].ToString();
                                    print.bol = "";
                                    print.return_label = "";
                                    //old code - this one adds the labels to the printing list for each package in shipment, so for a 9 pks shipment would add 10 lines * 9 labels = 90 labels(10 lines - 0 shipment level and the 9 levels for packages)

                                    //foreach (DataRow r in data.ShipmentsTable.Rows)
                                    //{
                                    //    if (r["Order #"].ToString().Equals(print.order))
                                    //    {
                                    //        print.label = "";
                                    //        print.commercial_invoice = r["Commercial Invoice URL"].ToString();
                                    //        print.bol = "";
                                    //        print.return_label = "";
                                    //        break;
                                    //    }
                                    //}
                                    if (print.commercial_invoice != "" && print.commercial_invoice != "N/A")
                                    {
                                        print_list.Add(print);
                                    }
                                }
                            }

                            if (!bg_print.IsBusy && print_list.Count == 0)
                            {
                                print_list.AddRange(temp);
                                if (!bg_print.IsBusy)
                                {
                                    bg_print.RunWorkerAsync();
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("InteGr8 is still printing, please try again later.");
                        }
                    }
                }
                else
                {
                    //* get the order # from the shipmentsHeader it must have Package_index too!!
                    Data.ShipmentsTableRow shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), Convert.ToInt32(order.Row["Package_Index"]));
                    if (shipment == null || shipment.Commercial_Invoice_URL.Equals("") || shipment.Commercial_Invoice_URL.Equals("N/A") || shipment.Commercial_Invoice_URL == null)
                    {
                        MessageBox.Show("There is no url for Commercial Invoice");
                        return;
                    }
                    Utils.openPDF(shipment.Commercial_Invoice_URL, Template.UseAutoprint ? (Template.AutoprintCIFolder != "" ? Template.AutoprintCIFolder : Application.LocalUserAppDataPath) : Application.LocalUserAppDataPath);
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in ShipmentsCIButton_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ShipmentsSave_Click(object sender, EventArgs e)
        {
            if (ShipmentsGrid.RowCount > 1)
            {
                bool noCheckedShips = true;
                foreach (Data.ShipmentsTableRow r in data.ShipmentsTable)
                {
                    if ((bool)r["Selected"] == true)
                    {
                        noCheckedShips = false;
                    }
                }
                if (noCheckedShips)
                {
                    MessageBox.Show("Please select at least one shipment", "No shipment selected", MessageBoxButtons.OK);
                    return;
                }
            }

            //backup all shipments, before saving only the selected ones
            Data.ShipmentsTableDataTable backupShipments = (Data.ShipmentsTableDataTable)data.ShipmentsTable.Copy();
            int length = data.ShipmentsTable.Rows.Count;
            bool checkNoOfShipments = false;

            shipmentsTableBindingSource.RaiseListChangedEvents = false;
            shipmentsTableBindingSource.SuspendBinding();
            
            if (ShipmentsGrid.RowCount > 1)
            {
                for (int i = length - 1; i > -1; i--)
                {
                    Data.ShipmentsTableRow s = (Data.ShipmentsTableRow)data.ShipmentsTable.Rows[i];
                    //remove unselected shipments from the current list, so they will not be saved to database
                    if ((bool)s["Selected"] == false)
                    {
                        data.ShipmentsTable.Rows.RemoveAt(i);
                    }
                    //remove the selected shipments from the backup shipments list
                    else
                    {
                        backupShipments.Rows.RemoveAt(i);
                    }
                }
            }
            else
            {
                checkNoOfShipments = true;
            }

            foreach (Data.ShipmentsTableRow r in data.ShipmentsTable)
            {
                data.SaveSerial(r._Order__);
            }

            if (progress.Created)
            {
                progress.Close_Me();
            }
            progress = new frmProcessing(TaskBackgroundWorker);
            string[] arguments = { "Save" };
            
            bgworker_save.RunWorkerAsync(arguments);
            
            progress.ShowProgress2("Saving Shipments");
            progress.ShowDialog(this);
            
            HistoryBindingSource.ResetBindings(true);

            //reload the unsaved shipments to the current shipments list
            if (!checkNoOfShipments)
            {
                data.ShipmentsTable.Clear();
                data.ShipmentsTable.Merge(backupShipments);
            }
            shipmentsTableBindingSource.RaiseListChangedEvents = true;
            shipmentsTableBindingSource.ResetBindings(true);
            shipmentsTableBindingSource.ResumeBinding();

        }

        private void bgworker_save_DoWork(object sender, DoWorkEventArgs e)
        {
            string[] arguments = e.Argument as string[];
            if (arguments[0] == "Close")
            {
                for (int i = 1; i < arguments.Length; i++)
                {
                    CloseShipments(Convert.ToInt32(arguments[i]));
                }
            }
            else
            {
                if (arguments[0] == "Aggregate")
                {
                    AggregateShipments();
                    if (Template.Name == "")
                    {
                        applySpecialRatesToOrders();
                    }
                }
                else
                {
                    if (arguments[0] == "Save")
                    {
                        try
                        {
                            Data.ShipmentsTableRow[] temp;
                            temp = data.ShipmentsTable.Select() as Data.ShipmentsTableRow[];
                            Dictionary<Data.ShipmentsTableRow, string> temp1 = data.SaveTemplate(temp);
                            if (temp1.Count != 0)
                            {
                                string error = "";
                                foreach (KeyValuePair<Data.ShipmentsTableRow, string> s in temp1)
                                {
                                    error = "An error occurred at saving the shipment results to the database for order number: " + s.Key._Order__ + "\r\nThe error message is: " + s.Value;
                                    MessageBox.Show(error, "Error saving to database", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            e.Result = null;

                        }
                        catch (Exception ex)
                        {
                            if (manual_order)
                            {
                                manual_order = false;
                            }
                            Utils.SendError("Error in ShipmentsSave_Click", ex);
                            e.Result = ex.Message + "\r\n" + ex.StackTrace;
                        }
                    }
                }
            }
        }

        private void bgworker_save_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ChangeButtonsStatus(true);
            Data.ShipmentsTableRow[] temp;
            temp = data.ShipmentsTable.Select() as Data.ShipmentsTableRow[];
            File.AppendAllText("Error.txt", "In bgworker_save");
            //****************************************************************************************************************
            using (StreamWriter writer = File.AppendText("ReplyTest.txt"))
            {
                writer.WriteLine("****************************");
                writer.WriteLine("Shipments table rows status:");
                for (int i = 0; i < data.ShipmentsTable.Rows.Count; i++)
                {
                    writer.WriteLine("Status = " + data.ShipmentsTable.Rows[i]["Status"].ToString());
                }
                writer.WriteLine("*****************************");
            }
            //******************************************************************************************************************

            foreach (DataColumn column in data.OrdersTable.Columns)
            {
                if (column.ColumnName.Contains("Sender") || column.ColumnName.Contains("Bill") || column.ColumnName.Contains("Ship Date"))
                {
                    if (!data.HistoryTable.Columns.Contains(column.ColumnName))
                    {
                        data.HistoryTable.Columns.Add(column.ColumnName, column.DataType);
                        data.HistoryPackages.Columns.Add(column.ColumnName, column.DataType);
                    }
                }
            }
            File.AppendAllText("Error.txt", "Adding shipments to history");
        
            foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
            {
                if (!shipment.Status.Equals("Committed"))
                {
                    continue;
                }
                //if we use filter in integr8, then the below condition will not show all shipments in history
                Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(shipment._Order__);
              
                Data.HistoryTableRow row = data.HistoryTable.AddHistoryTableRow(false, shipment._Order__,
                     (data.ShipmentsTable.Columns.IndexOf("OrderNumber") > -1) ? shipment["OrderNumber"].ToString() : "",
                     (data.ShipmentsTable.Columns.IndexOf("Token") > -1) ? (shipment["Token"] != null ? shipment["Token"].ToString() : "") : "",
                     (data.ShipmentsTable.Columns.IndexOf("PRO #") > -1) ? shipment["PRO #"].ToString() : "",
                     data.ShipmentsTable.Columns.IndexOf("Label URL") >= 0 ? shipment["Label URL"].ToString() : "",
                     data.ShipmentsTable.Columns.IndexOf("Commercial Invoice URL") >= 0 ? shipment["Commercial Invoice URL"].ToString() : "",
                     data.ShipmentsTable.Columns.IndexOf("BOL URL") >= 0 ? shipment["BOL URL"].ToString() : "",
                     data.ShipmentsTable.Columns.IndexOf("Return Label URL") >= 0 ? shipment["Return Label URL"].ToString() : "",
                     data.ShipmentsTable.Columns.IndexOf("Recipient Company") >= 0 ? (shipment["Recipient Company"] == null ? "" : shipment["Recipient Company"].ToString()) : "",
                     data.ShipmentsTable.Columns.IndexOf("Recipient Address 1") >= 0 ? (shipment["Recipient Address 1"] == null ? "" : shipment["Recipient Address 1"].ToString()) : "",
                     data.ShipmentsTable.Columns.IndexOf("Recipient Address 2") >= 0 ? (shipment["Recipient Address 2"] == null ? "" : shipment["Recipient Address 2"].ToString()) : "",
                     data.ShipmentsTable.Columns.IndexOf("Recipient City") >= 0 ? (shipment["Recipient City"] == null ? "" : shipment["Recipient City"].ToString()) :"",
                     data.ShipmentsTable.Columns.IndexOf("Recipient ZIP / Postal Code") >= 0 ? (shipment["Recipient ZIP / Postal Code"] == null ? "" : shipment["Recipient ZIP / Postal Code"].ToString()) : "",
                     data.ShipmentsTable.Columns.IndexOf("Recipient State / Province") >= 0 ? (shipment["Recipient State / Province"] == null ? "" : shipment["Recipient State / Province"].ToString()) : "",
                     data.ShipmentsTable.Columns.IndexOf("Recipient Country") >= 0 ? (shipment["Recipient Country"] == null ? "" : shipment["Recipient Country"].ToString()) : "",
                     data.ShipmentsTable.Columns.IndexOf("Carrier Name") >= 0 ? shipment["Carrier Name"].ToString() : "",
                     data.ShipmentsTable.Columns.IndexOf("Service Name") >= 0 ? shipment["Service Name"].ToString() : "",
                     shipment.Shipment_Tracking_Number);

                   //data.ShipmentsTable.Columns.IndexOf("OrderNumber") >= 0 ? (shipment["Recipient Company"].ToString() == "" ? order["Recipient Company"].ToString() : shipment["Recipient Company"].ToString()) : order["Recipient Company"].ToString(),
                   //  data.ShipmentsTable.Columns.IndexOf("OrderNumber") >= 0 ? (shipment["Recipient Address 1"].ToString() == "" ? order["Recipient Address 1"].ToString() : shipment["Recipient Address 1"].ToString()) : order["Recipient Address 1"].ToString(),
                   //  data.ShipmentsTable.Columns.IndexOf("OrderNumber") >= 0 ? (shipment["Recipient Address 2"].ToString() == "" ? order["Recipient Address 2"].ToString() : shipment["Recipient Address 2"].ToString()) : order["Recipient Address 2"].ToString(),
                   //  data.ShipmentsTable.Columns.IndexOf("OrderNumber") >= 0 ? (shipment["Recipient City"].ToString() == "" ? order["Recipient City"].ToString() : shipment["Recipient City"].ToString()) : order["Recipient City"].ToString(),
                   //  data.ShipmentsTable.Columns.IndexOf("OrderNumber") >= 0 ? (shipment["Recipient ZIP / Postal Code"].ToString() == "" ? order["Recipient ZIP / Postal Code"].ToString() : shipment["Recipient ZIP / Postal Code"].ToString()) : order["Recipient ZIP / Postal Code"].ToString(),
                   //  data.ShipmentsTable.Columns.IndexOf("OrderNumber") >= 0 ? (shipment["Recipient State / Province"].ToString() == "" ? order["Recipient State / Province"].ToString() : shipment["Recipient State / Province"].ToString()) : order["Recipient State / Province"].ToString(),
                   //  data.ShipmentsTable.Columns.IndexOf("OrderNumber") >= 0 ? (shipment["Recipient Country"].ToString() == "" ? order["Recipient Country"].ToString() : shipment["Recipient Country"].ToString()) : order["Recipient Country"].ToString(),
                    

                AddSenderAndBillingColumns(shipment);
                File.AppendAllText("Error.txt", "Row added to history");
        
                if (!shipment["Package_Index"].ToString().Equals("0"))
                {
                    data.HistoryPackages.ImportRow(row);
                    data.HistoryTable.Rows.Remove(row);
                }
            }

            //* remove all committed shipments from shipments header too
            // remove all committed shipments
            int index = 0;
            while (index < data.ShipmentsTable.Count)
            {
                Data.ShipmentsTableRow shipment = data.ShipmentsTable[index];
                if (!shipment.Status.Equals("Committed"))
                {
                    index++;
                }
                else
                {
                    Data.ProductsInPackagesRow[] old = data.ProductsInPackages.Select("[Order #] = '" + shipment._Order__ + "'") as Data.ProductsInPackagesRow[];
                    foreach (Data.ProductsInPackagesRow p in old)
                    {
                        BackupFile.RemoveShipment(data.ProductsInPackages.TableName, "Processed", shipment._Order__, data);
                    }
                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "Processed", shipment._Order__, data);
                    data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                }
                shipmentsTableBindingSource.ResetBindings(true);
            }
            //close the frmProcessing
            if (progress.Created)
            {
                progress.Close_Me();
            }
            if (e.Result != null)
            {
                MessageBox.Show(e.Result as String);
            }
        }

        private void AggregateShipments()
        {
            using (I2ShipServiceClient ws = new I2ShipServiceClient())
            {
                GetShipmentsRequest getReq = new GetShipmentsRequest();
                getReq.WS_Key = InteGr8_Main.key.MyWSKey;
                getReq.Type = GetShipmentType.ByDateAndAggregation;
                getReq.DateStart = DateTime.SpecifyKind(DateTime.Today, DateTimeKind.Unspecified);
                getReq.DateEnd = DateTime.SpecifyKind(DateTime.Today, DateTimeKind.Unspecified);
                CarrierShipResponse[] aggregatedShipments = new CarrierShipResponse[1000];
                aggregatedShipments = ws.GetShipments(getReq);//.GetConsolidatedOrders(InteGr8_Main.token.GetToken(), DateTime.Now);
                foreach (CarrierShipResponse res in aggregatedShipments)
                {
                    //Fields tempShipments = new Fields(ws.ShipmentInfo(InteGr8_Main.token.GetToken(), orderID[i]));
                    foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable.Rows)
                    {
                        if (shipment["Order #"].ToString().Equals(res.OrderNumber) && (shipment.Status == "Closed" || shipment.Status == "No Aggregation"))
                        {
                            int index = Convert.ToInt32(shipment["Package index"]);
                            shipment["Client Final Charge"] = res.Service.ClientPrice.Total;//tempShipments[183, index, "0"];
                            shipment["Client Freight"] = res.Service.ClientPrice.Freight;//tempShipments[167, index, "0"];
                            shipment.Status = "Aggregated";
                        }
                    }
                }
                foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable.Rows)
                {
                    if (shipment.Status == "Closed")
                    {
                        shipment.Status = "No Aggregation";
                    }
                }
            }
        }

        private string[] CloseShipments(int carrier_code)
        {
            //try
            //{
            //    using (I2ShipServiceClient ws = new I2ShipServiceClient())//(ShipService ws = new WS2Ship.ShipService())
            //    {
            //        string[] orderID;

            //        orderID = ws.Close(InteGr8_Main.token.GetToken(), carrier_code);

            //        return orderID;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Utils.SendError("Error at consolidating shipments", ex);
            //    return null;
            //}
            return null;
        }

        private void applySpecialRatesToOrders()
        {
            try
            {
                if (data.OrdersTable.Columns.IndexOf("Client ID") < 0)
                {
                    return;
                }
                string carrier = "";
                string service = "";
                string price = "0";
                string upcharge = "0";
                decimal cost = 0;
                foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable.Rows)
                {
                    price = "0";
                    Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(shipment._Order__);

                    carrier = data.Carriers.FindByCode(order["Carrier"].ToString()).Name;

                    service = data.Services.FindByUnique(order["Service"].ToString()).Name;

                    shipment["Client Freight"] = shipment["Client Final Charge"].ToString();

                    if (data.Carriers.FindByCode(order["Carrier"].ToString()).Carrier_Code != "999")
                    {
                        if (!String.IsNullOrEmpty(order["Client ID"].ToString()))
                        {
                            Data.ClientSpecialRatesRow[] ratesConditions = data.ClientSpecialRates.Select("ClientID = '" + order["Client ID"].ToString().TrimEnd() + "'") as Data.ClientSpecialRatesRow[];

                            int condCount = ratesConditions.Length;
                            //backorders go 0
                            bool backorder = false;

                            Data.OrderProductsTableRow[] temp = order.GetOrderProductsTableRows();

                            foreach (Data.OrderProductsTableRow r in temp)
                            {
                                if (r["BackOrder_Flag"].ToString().TrimEnd() != "0")
                                {
                                    backorder = true;
                                    break;
                                }
                            }

                            if (backorder)
                            {
                                for (int i = 0; i < condCount; i++)
                                {
                                    if ((ratesConditions[i]["FreightCode"].Equals("FFBO") || ratesConditions[i]["FreightCode"].Equals("FFGB")))
                                    {
                                        shipment["Client Freight"] = 0;
                                        break;
                                    }
                                }
                            }

                            //ground services go 0
                            switch (carrier)
                            {
                                case "Purolator WS":
                                    for (int i = 0; i < condCount; i++)
                                    {
                                        if (order["Service"].ToString() == "11 - 260" && (ratesConditions[i]["FreightCode"].Equals("FFGR") || ratesConditions[i]["FreightCode"].Equals("FFGB")))
                                        {
                                            shipment["Client Freight"] = 0;
                                            break;
                                        }
                                    }
                                    break;
                                case "UPS":
                                    for (int i = 0; i < condCount; i++)
                                    {
                                        if (order["Service"].ToString() == "8 - 11" && (ratesConditions[i]["FreightCode"].Equals("FFGR") || ratesConditions[i]["FreightCode"].Equals("FFGB")))
                                        {
                                            shipment["Client Freight"] = 0;
                                        }
                                    }
                                    break;
                                case "Canpar":
                                    for (int i = 0; i < condCount; i++)
                                    {
                                        if (order["Service"].ToString() == "14 - 1" && (ratesConditions[i]["FreightCode"].Equals("FFGR") || ratesConditions[i]["FreightCode"].Equals("FFGB")))
                                        {
                                            shipment["Client Freight"] = 0;
                                        }
                                    }
                                    break;
                                case "Canada Post":
                                    for (int i = 0; i < condCount; i++)
                                    {
                                        if (order["Service"].ToString() == "20 - DOM.RP" && (ratesConditions[i]["FreightCode"].Equals("FFGR") || ratesConditions[i]["FreightCode"].Equals("FFGB")))
                                        {
                                            shipment["Client Freight"] = 0;
                                        }
                                    }
                                    break;
                                case "QuickX":
                                    for (int i = 0; i < condCount; i++)
                                    {
                                        if (order["Service"].ToString() == "" && (ratesConditions[i]["FreightCode"].Equals("FFGR") || ratesConditions[i]["FreightCode"].Equals("FFGB")))
                                        {
                                            shipment["Client Freight"] = 0;
                                        }
                                    }
                                    break;
                            }

                            if (service.Contains("2 Day Air") || service.Contains("2Day") || service.Contains("2Day Air") || service.Contains("Next Day Air"))
                            {
                                for (int i = 0; i < condCount; i++)
                                {
                                    if (ratesConditions[i]["FreightCode"].Equals("2DGR") || ratesConditions[i]["FreightCode"].Equals("NXGR"))
                                    {
                                        //rate with ground, and take the price
                                        InteGr8Task t = new InteGr8Task(template_text, data, InteGr8TaskType.Rate, order["Order #"].ToString());
                                        t.Process();
                                        t.Save();
                                        int lines = data.RepliesTable.Select("[Order #] = '" + order._Order__ + "' And Field = 158").Length;
                                        //foreach (Data.RepliesTableRow r in data.RepliesTable.Rows)
                                        for (int k = 0; k < lines; k++)
                                        {
                                            Data.RepliesTableRow reply_row = data.RepliesTable.FindBy_Order__FieldIndex(order._Order__, 371, k);
                                            string CarrName = reply_row.Value;
                                            //get only the services for the current carrier
                                            if (!CarrName.Contains(carrier))
                                            {
                                                continue;
                                            }
                                            Data.RepliesTableRow reply_service = data.RepliesTable.FindBy_Order__FieldIndex(order._Order__, 372, k);
                                            string SerName = reply_service.Value;
                                            switch (carrier)
                                            {
                                                case "Purolator WS":
                                                    if (SerName == "PurolatorGround")
                                                    {
                                                        Data.RepliesTableRow reply_cost = data.RepliesTable.FindBy_Order__FieldIndex(order._Order__, 183, k);
                                                        price = reply_cost.Value;
                                                    }
                                                    break;
                                                case "UPS":
                                                    if (SerName == "UPS Standard®")
                                                    {
                                                        Data.RepliesTableRow reply_cost = data.RepliesTable.FindBy_Order__FieldIndex(order._Order__, 183, k);
                                                        price = reply_cost.Value;
                                                    }
                                                    break;
                                                case "Canpar":
                                                    if (SerName == "Ground")
                                                    {
                                                        Data.RepliesTableRow reply_cost = data.RepliesTable.FindBy_Order__FieldIndex(order._Order__, 183, k);
                                                        price = reply_cost.Value;
                                                    }
                                                    break;
                                                case "Canada Post":
                                                    if (SerName == "Regular Parcel")
                                                    {
                                                        Data.RepliesTableRow reply_cost = data.RepliesTable.FindBy_Order__FieldIndex(order._Order__, 183, k);
                                                        price = reply_cost.Value;
                                                    }
                                                    break;
                                                case "QuickX":
                                                    if (SerName == "")
                                                    {
                                                        Data.RepliesTableRow reply_cost = data.RepliesTable.FindBy_Order__FieldIndex(order._Order__, 183, k);
                                                        price = reply_cost.Value;
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (price == "0")
                            {
                                price = shipment["Client Freight"].ToString().TrimEnd();
                            }
                            cost = Convert.ToDecimal(price);
                            if (ratesConditions.Count() > 0)
                            {
                                upcharge = ratesConditions[0]["Upcharge"].ToString();
                            }
                            else
                            {
                                upcharge = "38%";
                            }
                            decimal upc = 0;
                            if (data.OrdersTable.Columns.IndexOf("Charge_Flag") > 0 && order["Charge_Flag"].ToString().TrimEnd().ToUpper() == "H")
                            {
                                if (upcharge.IndexOf("%") > 0)
                                {
                                    upcharge = upcharge.Substring(0, upcharge.IndexOf("%"));
                                    upc = Convert.ToDecimal(upcharge);
                                    cost += (Convert.ToDecimal(price) * upc) / 100;
                                }
                                else
                                {
                                    upc = Convert.ToDecimal(upcharge);
                                    cost += (Convert.ToDecimal(price) * upc) / 100;
                                }
                            }
                            shipment["Client Freight"] = cost;
                        }
                        else
                        {
                            price = shipment["Client Freight"].ToString().TrimEnd();
                            cost = Convert.ToDecimal(price);
                            upcharge = "38%";
                            decimal upc1 = 0;
                            if (data.OrdersTable.Columns.IndexOf("Charge_Flag") > 0 && order["Charge_Flag"].ToString().TrimEnd().ToUpper() == "H")
                            {
                                if (upcharge.IndexOf("%") > 0)
                                {
                                    upcharge = upcharge.Substring(0, upcharge.IndexOf("%"));
                                    upc1 = Convert.ToDecimal(upcharge);
                                    cost += (Convert.ToDecimal(price) * upc1) / 100;
                                }
                                else
                                {
                                    upc1 = Convert.ToDecimal(upcharge);
                                    cost += (Convert.ToDecimal(price) * upc1) / 100;
                                }
                            }
                            shipment["Client Freight"] = cost;
                        }

                        if (data.OrdersTable.Columns.IndexOf("Charge_Flag") > 0 && order["Charge_Flag"].ToString().TrimEnd().ToUpper() == "P")
                        {
                            shipment["Client Freight"] = "0";
                        }
                        if (data.OrdersTable.Columns.IndexOf("Charge_Flag") > 0 && order["Charge_Flag"].ToString().TrimEnd().ToUpper() == "C")
                        {
                            shipment["Client Freight"] = "0";
                            shipment["Client Final Charge"] = "0";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in getting special rates for clients", ex);
            }
        }

        private void AddSenderAndBillingColumns(Data.ShipmentsTableRow shipment)
        {
            try
            {
                int index = data.HistoryTable.Rows.Count - 1;
                foreach (DataColumn col in data.HistoryTable.Columns)
                {
                    if ((col.ColumnName.Contains("Sender") || col.ColumnName.Contains("Bill") || col.ColumnName.Contains("Ship Date")) && (data.ShipmentsTable.Columns.IndexOf(col.ColumnName) >= 0))
                    {
                        data.HistoryTable.Rows[index][col.ColumnName] = shipment[col.ColumnName];
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.SendError("Exception at adding packslip info in HistoryGrid", ex);
            }
        }

        private void SelectAllOrdersButton_Click(object sender, EventArgs e)
        {
            EndEdit();
            foreach (Data.OrdersTableRow order in data.OrdersTable)
            {
                order.Selected = true;
            }
        }

        private void UnselectAllOrdersButton_Click(object sender, EventArgs e)
        {
            EndEdit();
            foreach (Data.OrdersTableRow order in data.OrdersTable)
            {
                order.Selected = false;
            }
        }

        private void SelectAllShipmentsButton_Click(object sender, EventArgs e)
        {
            foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
            {
                shipment.Selected = true;
            }
        }

        private void UnselectAllShipments_Click(object sender, EventArgs e)
        {
            foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
            {
                shipment.Selected = false;
            }
        }

        private void OrdersParametersGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ToolTip.SetToolTip(FilterLinkLabel, Select_Filter.Filter_No_Status);
                ReloadInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void ShipmentsDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete the selected shipment from 2Ship?", "Delete 2Ship Shipment", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                return;
            }
            try
            {
                //* get the order # which must be deleted from the shipments header binding source
                //*DataRowView order = shipmentsHeaderTableBindingSource.Current as DataRowView;
                //*shipmentsTableBindingSource.IndexOf(orderDataGridViewTextBoxColumn as obj);?? - this is necesary to get the package index
                int rowCount = ShipmentsGrid.Rows.Count;

                for (int k = rowCount - 1; k >= 0; k--)
                {
                    DataGridViewRow r = ShipmentsGrid.Rows[k];
                    //delete all selected shipments, if is just one, delete it without needing to have the Selected checkbox checked
                    if (r.Cells["ShipmentsSelectedColumn"].Value.Equals(true) || (rowCount == 1))
                    {
                        shipmentsTableBindingSource.Position = r.Index;

                        DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
                        if (order == null)
                        {
                            return;
                        }
                        Data.ShipmentsTableRow shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), Convert.ToInt32(order.Row["Package_Index"]));
                        //in case of an error, delete the shipment only from shipments table and return
                        if (shipment == null || shipment.Shipment_Tracking_Number.Equals("N/A"))
                        {
                            int packs = data.OrdersTable.FindBy_Order__(order.Row["Order #"].ToString()).GetOrderPackagesTableRows().Count();
                            for (int p = 1; p < packs; p++)
                            {
                                shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), p);
                                if (shipment != null)
                                {
                                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                    data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                                }
                            }
                            shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), 0);
                            if (shipment != null)
                            {
                                BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                            }
                            return;
                        }
                        ChangeButtonsStatus(false);

                        // the ws will delete all packages
                        string shipmentTrackingNr = shipment.Shipment_Tracking_Number;
                        string orderNr = order.Row["Order #"].ToString();
                        string wsKey = "";
                        if (data.ShipmentsTable.Columns.Contains("Token"))//(data.OrdersTable.Columns.Contains("Token"))
                        {
                            if (!String.IsNullOrEmpty(shipment["Token"].ToString()))
                            {
                                wsKey = shipment["Token"].ToString();
                            }
                        }
                        // remove all packages of this shipment from the shipments table
                        //in case of filtered orders, only the last saved order is still in orders table

                        Data.ProductsInPackagesRow[] packagesrows = data.ProductsInPackages.Select("[Order #] = '" + orderNr + "'") as Data.ProductsInPackagesRow[];
                        foreach (Data.ProductsInPackagesRow ro in packagesrows)
                        {
                            BackupFile.RemoveShipment(data.ProductsInPackages.TableName, "deleted", orderNr, data);
                            data.ProductsInPackages.Rows.Remove(ro);
                        }

                        Data.ProductsSerialNumbersRow[] serialrows = data.ProductsSerialNumbers.Select("[Order #] = '" + orderNr + "'") as Data.ProductsSerialNumbersRow[];
                        foreach (Data.ProductsSerialNumbersRow ro in serialrows)
                        {
                            BackupFile.RemoveShipment(data.ProductsSerialNumbers.TableName, "deleted", orderNr, data);
                            data.ProductsSerialNumbers.Rows.Remove(ro);
                        }

                        if (data.OrdersTable.FindBy_Order__(order.Row["Order #"].ToString()) != null)
                        {
                            int packages = data.OrdersTable.FindBy_Order__(order.Row["Order #"].ToString()).GetOrderPackagesTableRows().Count();
                            for (int p = 1; p <= packages; p++)
                            {
                                shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), p);
                                if (shipment != null)
                                {
                                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                    data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                                }
                            }
                            shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), 0);
                            if (shipment != null)
                            {
                                BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                            }
                        }
                        else
                        {
                            int shipments = data.ShipmentsTable.Rows.Count;
                            for (int i = shipments - 1; i >= 0; i--)
                            {
                                shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), i);
                                if (shipment != null)
                                {
                                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order.Row["Order #"].ToString(), data);
                                    data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                                }
                            }
                        }

                        using (I2ShipServiceClient ws = new I2ShipServiceClient())//TwoShipService())//(TwoShipService ws = new TwoShipService())
                        {
                            DeleteRequest delReq = new DeleteRequest();
                            delReq.DeleteType = DeleteType.ByTrackingNumber;
                            //delReq.DeleteTypeSpecified = true;
                            delReq.TrackingNumber = shipmentTrackingNr;
                            if (InteGr8_Main.key == null)
                            {
                                delReq.WS_Key = wsKey;
                            }
                            else
                            {
                                delReq.WS_Key = InteGr8_Main.key.MyWSKey;
                            }
                            ws.DeleteShipment(delReq);
                        }
                        //when an order is deleted from shipments table without being saved back to database, 
                        //it should be reloaded in orders grid again
                        if (data.OrdersTable.FindBy_Order__(orderNr) != null)
                        {
                            Data.OrdersTableRow row = data.OrdersTable.FindBy_Order__(orderNr);
                            row["Status"] = "";
                        }
                    }
                }
                shipmentsTableBindingSource.ResetBindings(true);
                shipmentsTableBindingSource.Filter = "Package_Index = 0";
                ChangeButtonsStatus(true);

                SetRowNumbers();
                if (Template.Name == "")
                {
                    refresh_products_rows();
                }
                if (data.ShipmentsTable.Rows.Count == 0)
                {
                    if (File.Exists(Template.Name + "_bkp.xml"))
                    {
                        File.Delete(Template.Name + "_bkp.xml");
                    }
                }
            }
            catch (Exception ex)
            {
                ChangeButtonsStatus(true);
                if (!ex.Message.Contains("Tracking Number not found, or shipment closed off already"))
                {
                    Utils.SendError("Error in ShipmentsDelete_Click", ex);
                    MessageBox.Show(this, "Error at delete. \r\n (" + ex.Message + ")", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Browser_NewWindow2(object sender, NewWindow2EventArgs e)
        {
            frmWebPopup popup = new frmWebPopup(this);
            e.PPDisp = popup.Browser.Application;
        }

        private void Options_Click(object sender, EventArgs e)
        {
            EndEdit();
            Data.OrdersTableRow order = null;
            if (OrdersTableBindingSource != null)
            {
                if (OrdersTableBindingSource.Current != null)
                {
                    DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
                    order = data.OrdersTable.FindBy_Order__(order_row.Row["Order #"].ToString());
                }
            }
            new Options(t, order).ShowDialog(this);
        }

        private void GetEditResults_Click(object sender, EventArgs e)
        {
            GetEditResultsFrom2Ship();
        }

        private void GetEditResultsFrom2Ship()
        {
            EndEdit();
            try
            {
                DataView orders = OrdersTableBindingSource.List as DataView;

                if (orders == null)
                {
                    return;
                }
                List<InteGr8Task> tasks = new List<InteGr8Task>(orders.Count);
                DataRowView order = OrdersTableBindingSource.Current as DataRowView;
                foreach (DataRowView order_row in orders)
                {
                    object order_number = order_row["Order #"];
                    bool order_selected = order_row["Selected"] == null || order_row["Selected"].Equals(DBNull.Value) ? false : Convert.ToBoolean(order_row["Selected"]);
                    if (order_number == null || !order_selected || order_number.Equals(""))
                    {
                        continue;
                    }
                    tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.GetBackEditResults, order_number.ToString()));
                }
                if (tasks.Count == 0 && order == null) return;
                else if (order != null)
                {
                    tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.GetBackEditResults, order["Order #"].ToString()));
                }

                OrdersTableBindingSource.RaiseListChangedEvents = false;
                OrdersTableBindingSource.SuspendBinding();

                StartTasks(tasks.ToArray());

                OrdersTableBindingSource.RaiseListChangedEvents = true;
                OrdersTableBindingSource.ResetBindings(true);
                OrdersTableBindingSource.ResumeBinding();

                BackupFile.LoadSettings(t);
                RestoreOrdersGridColumnsOrder();
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in GetEditResults", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistorySearchButton_Click(object sender, EventArgs e)
        {
            if (TemplatesDropdown.Text.Trim().Equals("") || HistorySearchTextbox.Text.Trim().Equals(""))
            {
                return;
            }
            if (!(HistoryGrid.Rows.Count > 0))
            {
                MessageBox.Show("There are no orders in the Processed ordes grid", " Processed orders grid empty", MessageBoxButtons.OK);
                return;
            }
            StringBuilder columns = new StringBuilder(1000);
            //informix database has no quota
            if (Template.WriteConn.Quote.Equals('?'))
            {
                foreach (Binding Binding in Template.Bindings)
                {
                    if (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments)
                    {
                        columns.Append("\t" + Binding.Column.Replace(" ", "") + " as " + Binding.Field.Name.Replace(" ", "") + ",\n");
                    }
                }

            }
            else
            {
                foreach (Binding Binding in Template.Bindings)
                {
                    if (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments)
                    {
                        columns.Append("\t" + Template.WriteConn.Quote + Binding.Column + Template.WriteConn.Quote + " as " + Template.WriteConn.Quote + Binding.Field.Name + Template.WriteConn.Quote + ",\n");
                    }
                }
            }
            if (columns.Length == 0)
            {
                return;
            }

            ChangeButtonsStatus(false);
            try
            {
                columns.Remove(columns.Length - 2, 2);
                if (Template.WriteConn.Quote.ToString().Equals("?"))
                {
                    using (OdbcConnection Conn = new OdbcConnection(Template.WriteConn.ConnStr))
                    {
                        Conn.Open();
                        using (OdbcDataReader Dr = new OdbcCommand("SELECT " + columns.ToString() + " FROM " + Template.Shipments.Name + " WHERE " + Template.WriteConn.Quote + Template.Shipments.PK[0] + " = '" + HistorySearchTextbox.Text.Replace("'", "''") + "'", Conn).ExecuteReader())
                            while (Dr.Read())
                            {
                                bool exists = false;
                                foreach (DataGridViewRow row in HistoryGrid.Rows)
                                    if (row.Cells[0].Value.ToString().Equals(Dr["Order #"]))
                                    {
                                        exists = true;
                                        break;
                                    }
                                if (!exists)
                                {
                                    HistoryGrid.Rows.Add(Dr["Order #"], Dr["Shipment Tracking Number"]);
                                }
                            }
                    }
                }
                else
                {
                    using (OdbcConnection Conn = new OdbcConnection(Template.WriteConn.ConnStr))
                    {
                        Conn.Open();
                        using (OdbcDataReader Dr = new OdbcCommand("SELECT " + columns.ToString() + " FROM " + Template.WriteConn.Quote + Template.Shipments.Name + Template.WriteConn.Quote + " WHERE " + Template.WriteConn.Quote + Template.Shipments.PK[0] + Template.WriteConn.Quote + " = '" + HistorySearchTextbox.Text.Replace("'", "''") + "'", Conn).ExecuteReader())
                            while (Dr.Read())
                            {
                                bool exists = false;
                                foreach (DataGridViewRow row in HistoryGrid.Rows)
                                    if (row.Cells[0].Value.ToString().Equals(Dr["Order #"]))
                                    {
                                        exists = true;
                                        break;
                                    }
                                if (!exists)
                                {
                                    HistoryGrid.Rows.Add(Dr["Order #"], Dr["Shipment Tracking Number"]);
                                }
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in HistorySearchButton_Click", ex);
                MessageBox.Show(this, "Error at searching.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                ChangeButtonsStatus(true);
            }
        }

        private void HistoryDeleteButton_Click(object sender, EventArgs e)
        {
            if (HistoryGrid.SelectedRows.Count == 0)
            {
                return;
            }

            //if shipments were definively commited to database - see Canada Cartage / Direct Distribution case - then don't allow deletion 
            //use a new seeting for this: CheckShipmentsCommited
            if (Properties.Settings.Default.CheckShipmentsDBCommited)
            {
                HistoryGrid.EndEdit();
                  
                for (int t = data.HistoryTable.Rows.Count - 1; t >= 0; t--)
                {
                    Data.HistoryTableRow Row;
                    try
                    {
                        Row = data.HistoryTable.Rows[t] as Data.HistoryTableRow;
                    }
                    catch
                    {
                        continue;
                    }
                    if ((bool)Row.Select)
                    {
                        string order_number = Row._Order__;

                       if( CheckOrderCommitted( order_number) == true)
                       {
                           MessageBox.Show("Order number: " + order_number + " has the status \"Closed\" and cannot be deleted from database!", "Warning");
                           Row.Select = false;
                           return;
                       }
                    }
                }
            }

            if (MessageBox.Show("Are you sure you want to delete the selected shipment? This will delete the shipment from both your database and 2ship.", "Delete Shipment", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (Template.WriteConn.SupportsDelete)
                {
                    HistoryGrid.EndEdit();
                    for (int t = data.HistoryTable.Rows.Count - 1; t >= 0; t--)
                    {
                        Data.HistoryTableRow Row;
                        try
                       {
                            Row = data.HistoryTable.Rows[t] as Data.HistoryTableRow;
                        }
                        catch
                        {
                            continue;
                        }
                        if ((bool)Row.Select)
                        {
                            ChangeButtonsStatus(false);
                            string order_number = Row._Order__;
                            string tracking_number = Row._Tracking__;

                            if (Row._Tracking__.Equals(""))
                            {
                                tracking_number = Row._PRO__;
                            }
                            // delete from 2Ship
                            try
                            {
                                using (I2ShipServiceClient ws = new I2ShipServiceClient())// TwoShipService())//(TwoShipService ws = new TwoShipService())
                                {
                                    DeleteRequest delReq = new DeleteRequest();
                                    delReq.DeleteType = DeleteType.ByTrackingNumber;
                                    //delReq.DeleteTypeSpecified = true;
                                    delReq.TrackingNumber = tracking_number;
                                    if (String.IsNullOrEmpty(Row.Token))
                                    {
                                        delReq.WS_Key = InteGr8_Main.key.MyWSKey;
                                    }
                                    else
                                    {
                                        delReq.WS_Key = Row.Token;
                                    }
                                    //if (!Row.Token.Equals("") /*&& (InteGr8_Main.key == null)*/)
                                    //{
                                    //    delReq.WS_Key = Row.Token;
                                    // }

                                    ws.DeleteShipment(delReq);
                                }
                                  }
                            catch (Exception ex)
                            {
                                if (!ex.Message.Contains("Tracking Number not found, or shipment closed off already"))
                                {
                                    Utils.SendError("Error in ShipmentsDelete_Click", ex);
                                    MessageBox.Show("Exception at deleting from 2ship: " + ex.Message, "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            try{
                                // delete from the database
                                if (Template.WriteConn.SupportsDelete)
                                {
                                    if (!String.IsNullOrEmpty(Shipments_Delete_Statement.Delete))
                                    {
                                        using (OdbcConnection Conn = new OdbcConnection(Template.WriteConn.ConnStr))
                                        {
                                            Conn.Open();
                                            int count = data.HistoryTable.Rows.Count - 1;
                                            for (int i = count; i >= 0; i--)
                                            {
                                                Data.HistoryTableRow r = data.HistoryTable.Rows[i] as Data.HistoryTableRow;
                                                if (r._Order__.ToString().Equals(order_number))
                                                {
                                                    tracking_number = r._Tracking__.ToString();
                                                    try
                                                    {
                                                        OdbcCommand Comm = new OdbcCommand();
                                                        string sql = Shipments_Delete_Statement.Delete;
                                                        string commandText = "";
                                                        //informix database sql sintax
                                                        if (String.IsNullOrEmpty(sql))
                                                        {
                                                            if (sql.IndexOf("@ShipmentTracking#") > 0)
                                                            {
                                                                commandText = "exec [2Ship_Delete] '" + order_number.Split('.')[0] + "'" + ", '" + tracking_number + "'";
                                                            }
                                                            else
                                                            {
                                                                commandText = "exec [2Ship_Delete] '" + order_number.Split('.')[0] + "'";
                                                            }
                                                            Comm.CommandText = commandText;
                                                        }
                                                        else
                                                        {
                                                            foreach (Binding Binding in Template.Bindings)
                                                            {
                                                                if ((Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments) || (Binding.Field.Name.Contains("Recipient")) || (Binding.Field.Name.Equals("Order #") && (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Orders)))
                                                                {
                                                                    if (sql.Contains("@" + Binding.Field.Name.Replace(" ", "")))
                                                                    {
                                                                        sql = sql.Replace("@" + Binding.Field.Name.Replace(" ", ""), "?");
                                                                        Comm.Parameters.Add("@" + Binding.Field.Name.Replace(" ", ""), OdbcType.VarChar, 100);
                                                                    }
                                                                }
                                                                if (!sql.Contains('@'))
                                                                {
                                                                    break;
                                                                }
                                                            }

                                                            if (sql.Contains("@SCAC"))
                                                            {
                                                                sql = sql.Replace("@SCAC", "?");
                                                                Comm.Parameters.Add("@SCAC", OdbcType.VarChar, 100);
                                                            }

                                                            Comm.CommandText = sql;

                                                            foreach (OdbcParameter p in Comm.Parameters)
                                                            {
                                                                string name = "";
                                                                foreach (DataColumn c in data.HistoryTable.Columns)
                                                                {
                                                                    if (c.ColumnName.Replace(" ", "") == (p.ParameterName[0] == '@' ? p.ParameterName.Substring(1) : p.ParameterName))
                                                                    {
                                                                        name = c.ColumnName;
                                                                        break;
                                                                    }
                                                                }
                                                                if (p.ParameterName == "@SCAC")
                                                                {
                                                                    name = p.ParameterName.Replace("@", "");
                                                                }
                                                                if (name == "")
                                                                {
                                                                    if (p.ParameterName[p.ParameterName.Length - 1] == '#')
                                                                    {
                                                                        name = p.ParameterName.Substring(1, p.ParameterName.Length - 2) + " #";
                                                                    }
                                                                    else
                                                                    {
                                                                        name = p.ParameterName.Substring(1);
                                                                    }
                                                                }

                                                                if (p.ParameterName == "ShippingDate")
                                                                {
                                                                    DateTime date =String.IsNullOrEmpty(r[name].ToString()) ? DateTime.Today : Convert.ToDateTime(r[name].ToString());
                                                                    
                                                                    p.Value = date.ToString("yyyy''MM''dd");
                                                                }
                                                                else
                                                                {
                                                                    if (p.ParameterName == "@SCAC")
                                                                    {
                                                                        Data.SCACSRow[] scac = data.SCACS.Select("Carrier_Code = '" + data.Carriers.FindByName(r.Carrier).Carrier_Code + "' AND Service_Code = '" + data.Services.FindByName(r.Carrier, r.Service).Service_Code + "'") as Data.SCACSRow[];
                                                                        if (scac.Length == 0)
                                                                        {
                                                                            scac = data.SCACS.Select("Carrier_Code = '" + data.Carriers.FindByName(r.Carrier).Carrier_Code + "' AND Service_Code = ''") as Data.SCACSRow[];
                                                                        }
                                                                        p.Value = scac[0].SCAC_Code;
                                                                    }
                                                                    else
                                                                    {
                                                                        p.Value = r[name].ToString().TrimEnd();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        Comm.Connection = Conn;
                                                        Comm.CommandType = CommandType.Text;
                                                        Comm.ExecuteNonQuery();
                                                        data.HistoryTable.Rows.RemoveAt(i);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Utils.SendError("Error in ShipmentsDelete_Click", ex);
                                                        MessageBox.Show(this, "Error at database delete.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                    }
                                                    Data.HistoryPackagesRow[] rows = data.HistoryPackages.Select("[Order Number] = '" + order_number + "'") as Data.HistoryPackagesRow[];
                                                    if (rows != null && rows.Length != 0)
                                                    {
                                                        foreach (Data.HistoryPackagesRow rr in rows)
                                                        {
                                                            tracking_number = rr._Tracking__.ToString();
                                                            try
                                                            {
                                                                OdbcCommand Comm = new OdbcCommand();
                                                                string sql = Shipments_Delete_Statement.Delete;
                                                                string commandText = "";
                                                                if (sql.IndexOf("@ShipmentTracking#") > 0)
                                                                {
                                                                    commandText = "exec [2Ship_Delete] '" + order_number.Split('.')[0] + "'" + ", '" + tracking_number + "'";
                                                                }
                                                                else
                                                                {
                                                                    commandText = "exec [2Ship_Delete] '" + order_number.Split('.')[0] + "'";
                                                                }
                                                                Comm.Connection = Conn;
                                                                Comm.CommandType = CommandType.Text;
                                                                Comm.CommandText = commandText;
                                                                Comm.ExecuteNonQuery();
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                Utils.SendError("Error in ShipmentsDelete_Click", ex);
                                                                MessageBox.Show(this, "Error at database delete.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                            }
                                                        }
                                                        for (int ii = rows.Length - 1; ii >= 0; ii--)
                                                        {
                                                            data.HistoryPackages.Rows.Remove(rows[ii]);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Data.HistoryPackagesRow[] rows = data.HistoryPackages.Select("[Order Number] = '" + order_number + "'") as Data.HistoryPackagesRow[];
                                        for (int ii = rows.Length - 1; ii >= 0; ii--)
                                        {
                                            data.HistoryPackages.Rows.Remove(rows[ii]);
                                        }
                                        int count = HistoryGrid.Rows.Count - 1;
                                        for (int i = count; i >= 0; i--)
                                        {
                                            DataGridViewRow r = HistoryGrid.Rows[i];
                                            if (r.Cells[1].Value.ToString().Equals(order_number) && (bool)r.Cells[0].Value)
                                            {
                                                HistoryGrid.Rows.Remove(HistoryGrid.Rows[i]);
                                            }
                                        }
                                    }
                                }
                                Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_number);
                                if (order != null)
                                {
                                    order["Status"] = "";
                                    OrdersTableBindingSource.EndEdit();
                                }
                                //remove from Shipments table
                                //int packages = 0;
                                //foreach (Data.ShipmentsTableRow r in data.ShipmentsTable)
                                //{
                                //    if (r["Order #"].ToString().Equals(order_number))
                                //    {
                                //        packages++;
                                //    }
                                //}
                                //for (int p = 0; p < packages; p++)
                                //{
                                    //Data.ShipmentsTableRow shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order_number, p);
                                   // if (shipment != null)
                                   // {
                                        BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order_number, data);
                                 //       data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                                  //  }
                               // }
                                shipmentsTableBindingSource.ResetBindings(true);
                                shipmentsTableBindingSource.Filter = "Package_Index = 0";
                            }
                            catch (Exception ex)
                            {
                                Utils.SendError("Error in ShipmentsDelete_Click", ex);
                                MessageBox.Show(this, "Error at delete from database", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            SetRowNumbers();
                        }
                    }
                    ChangeButtonsStatus(true);
                    SetRowNumbers();
                    if (Template.Name == "")
                    {
                        refresh_products_rows();
                    }
                }
                else
                {
                    MessageBox.Show("The shipments database does not support delete", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private bool CheckOrderCommitted(string order_number)
        {
            HistoryGrid.EndEdit();
            bool isCommitted = false;
            try
            {
                using (OdbcConnection Conn = new OdbcConnection(Template.WriteConn.ConnStr))
                {
                    Conn.Open();
                    //int count = data.HistoryTable.Rows.Count - 1;
                    try
                      {
                          OdbcCommand Comm = new OdbcCommand();
                          string commandText = "select dbo.[CheckCommited] (?)";//"select Status from WMSCarrier_TMS.dbo.V_2SHIP_Orders where Ord_Key = '" + order_number + "'"; //"select dbo.[CheckCommited] (?)";
                          Comm.CommandText = commandText;
                           Comm.Parameters.Add("@order#", OdbcType.VarChar, 50);
                           Comm.Parameters["@order#"].Value = order_number;
                           Comm.CommandType = CommandType.Text;
                           Comm.Connection = Conn;
                           string result = "";
                          result = (string) Comm.ExecuteScalar();
                          if (result.ToUpper().Contains("CLOSE"))
                          {
                              isCommitted = true;
                          }
                           }
                           catch (Exception ex)
                           {
                                Utils.SendError("Error in Check if shipments are commited", ex);
                                MessageBox.Show(this, "Error at database delete.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return isCommitted;
                           }
                      
                   
                 }
                 }
                 catch (Exception ex)
                    {
                        Utils.SendError("Error in ShipmentsDelete_Click", ex);
                        MessageBox.Show(this, "Error at database delete", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return isCommitted;
                    }
            
                return isCommitted;
            
            }
        
        private void Shipments_Delete_RowUpdating(object sender, OdbcRowUpdatingEventArgs e)
        {
            if (e.Row["Status"].ToString().Equals("error"))
            {
                e.Status = UpdateStatus.SkipCurrentRow;
            }
        }

        private void Shipments_Delete_RowUpdated(object sender, OdbcRowUpdatedEventArgs e)
        {
            if (e.Errors == null && e.RecordsAffected == e.RowCount)
            {
                MessageBox.Show("Shipment deleted from database", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void HistorySearchTextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // start searching when the user has pressed Enter
            if (e.KeyChar == '\r')
            {
                HistorySearchButton_Click(sender, e);
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {

            //close adobe if it is still opened

            try
            {
                Process[] procs = Process.GetProcessesByName("acrord32");
                if (procs == null || procs.Length <= 0)
                {
                    procs = Process.GetProcessesByName("Acrobat");
                }
                for (int i = procs.Length - 1; i >= 0; i--)
                {
                    //if (!Print.HasExited)
                    {
                        procs[i].CloseMainWindow();
                        Thread.Sleep(1000);
                        if (!procs[i].HasExited)
                        {
                            procs[i].Kill();
                            procs[i].WaitForExit();
                        }
                    }
                    //else
                    //{
                    //    Print.WaitForExit();
                    //}
                }
            }
                catch(Exception ex)
            {

                Utils.SendError("Error at closing Adobe", ex);
            }


            //SaveColumns();

            if (e.CloseReason == CloseReason.UserClosing)
            {
                SaveColumns();
            }
            // warn the user about unsaved shipments
            if (e.CloseReason == CloseReason.UserClosing)
            {
                foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                {
                    if (shipment.Status.Equals("shipped"))
                    {
                        MainPages.SelectedTab = ShipmentsPage;
                        string printing = "";
                        if (print_list.Count > 0)
                        {
                            printing = " and labels/reports waiting to be printed";
                        }
                        if (MessageBox.Show(this, "There are unsaved shipments" + printing + ", are you sure you want to close the application ?", "InteGr8 Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            return;
                        }
                        else
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }
            if (!Template.Backup_Orders)
            {
                return;
            }
            if (String.IsNullOrEmpty(Template.Backup_Month))
            {
                return;
            }
            if (Template.Backup_at_Scheadule)
            {
                using (OdbcConnection Conn = new OdbcConnection(Template.BackupConn.ConnStr))
                {
                    Conn.Open();

                    using (OdbcCommand Comm = new OdbcCommand())
                    {
                        Comm.Connection = Conn;
                        Comm.CommandText = Template.Backup_Schedule;
                        Comm.ExecuteNonQuery();
                    }
                }
            }
            Cef.Shutdown();
        }

        private void SaveColumns()
        {
            StringCollection ord_cols = new StringCollection();
            int i = 0;
            foreach (DataGridViewColumn column in OrdersGrid.Columns)
            {
                ord_cols.Add(string.Format(
                    "{0},{1},{2},{3}",
                    column.DisplayIndex.ToString("D2"),
                    column.Width,
                    column.Visible,
                    i++));
                //using (StreamWriter write = File.AppendText("OrdersTest.txt"))
                //{
                //    //write.WriteLine("Index  Column name");
                //        write.WriteLine(column.DisplayIndex + "   -   " + column.Name + "   -   " + i);
                    
                //}
            }
            Properties.Settings.Default.OrdersGridColumnsOrder = ord_cols;

            StringCollection pakages_cols = new StringCollection();
            i = 0;
            foreach (DataGridViewColumn column in OrderPackagesGrid.Columns)
            {
                pakages_cols.Add(string.Format(
                    "{0},{1},{2},{3}",
                    column.DisplayIndex.ToString("D2"),
                    column.Width,
                    column.Visible,
                    i++));
            }
            Properties.Settings.Default.PackagesGridColumnsOrder = pakages_cols;

            StringCollection products_cols = new StringCollection();
            i = 0;
            foreach (DataGridViewColumn column in ProductsGrid.Columns)
            {
                products_cols.Add(string.Format(
                    "{0},{1},{2},{3}",
                    column.DisplayIndex.ToString("D2"),
                    column.Width,
                    column.Visible,
                    i++));
            }
            Properties.Settings.Default.ProductsGridColumns = products_cols;
            Properties.Settings.Default.Save();

            //using (StreamWriter write = File.AppendText("SaveOrdersTest.txt"))
            //{
            //    write.WriteLine("Index  Column name");
            //    foreach (DataGridViewColumn col in OrdersGrid.Columns)
            //    {
            //        write.WriteLine(col.DisplayIndex + "   -   " + col.Name);
            //    }
            //}

            //save the orders columns order to a file too
            ColumnsSettings.Save(ord_cols, pakages_cols, products_cols, t);
        }

        private void OrderSearchTextbox_TextChanged(object sender, EventArgs e)
        {
            if (OrderSearchTextbox.Text.Length != 0)
            {
                if (OrderSearchTextbox.Text.Length > 1 && scanned)
                {
                    OrderSearchButton_Click(null, null);
                    OrderSearchTextbox.Text = "";
                }
                else
                {
                    scanned = false;
                }
            }
            else
            {
                scanned = true;
            }
        }

        private void OrderSearchTextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // start searching when the user has pressed Enter
            if (e.KeyChar == '\r')
            {
                OrderSearchButton_Click(sender, e);
                OrderSearchTextbox.Text = "";
            }
        }

        private void OrderSearchButton_Click(object sender, EventArgs e)
        {
            // search in the grid for the entered text 
            // start from the current line and stop at the first occurence and make it the current line
            scanned = true;
            EndEdit();
            bool selected = false;
            //start searching from the first order in grid, not from the current one(if the searched word is above the selected line, then it won't find it)
            for (int i = 0; i < OrdersGrid.Rows.Count; i++)
            {
                if (Template.Name == "")
                {
                    if (OrdersGrid.Rows[i].Cells["txtOrdernumber"].Value != null && OrdersGrid.Rows[i].Cells["txtOrdernumber"].Value.ToString().Trim().Equals(OrderSearchTextbox.Text))
                    {
                        if (OrdersGrid.SelectedRows.Count != 0)
                        {
                            OrdersGrid.SelectedRows[0].Selected = false;
                        }
                        OrdersGrid.Rows[i].Selected = true;
                        OrdersGrid.CurrentCell = OrdersGrid.Rows[i].Cells[0];
                        selected = true;
                        if (OrdersGrid.SelectedRows.Count > 0)
                        {
                            if (Template.Name == "")
                            {
                                refresh_products_rows();
                            }
                        }
                        break;
                    }
                }
                else
                {
                    foreach (DataGridViewCell cell in OrdersGrid.Rows[i].Cells)
                    {
                        if (cell.Value != null && cell.Value.ToString().Trim().Equals(OrderSearchTextbox.Text))
                        {
                            if (OrdersGrid.SelectedRows.Count != 0)
                            {
                                OrdersGrid.SelectedRows[0].Selected = false;
                            }
                            OrdersGrid.Rows[i].Selected = true;
                            OrdersGrid.CurrentCell = OrdersGrid.Rows[i].Cells[0];
                            selected = true;
                            break;
                        }
                    }
                }
            }
            if (!selected)
            {
                MessageBox.Show("No order found !");
                return;
            }
            OrdersGrid_CellMouseDoubleClick(null, null);
        }

        private void OrdersGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //there is no order in grid
            if (OrdersTableBindingSource.Position < 0)
            {
                return;
            }
            if (e.ColumnIndex == OrdersGrid.Columns["cmbOrderService"].Index)
            {
                (OrdersGrid[e.ColumnIndex, e.RowIndex] as DataGridViewComboBoxCell).DataSource = ServicesBSFiltered;
                ServicesBSFiltered.Filter = "Carrier_Code = '" + OrdersGrid["cmbOrderCarrier", e.RowIndex].Value.ToString() + "'";
            }
        }

        private void OrdersGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //there is no order in grid
            if (OrdersTableBindingSource.Position < 0)
            {
                return;
            }
            if (e.ColumnIndex == OrdersGrid.Columns["cmbOrderCarrier"].Index)
            {
                // clear the service and packaging if the carrier has changed
                object carrier_code = OrdersGrid[e.ColumnIndex, e.RowIndex].Value.ToString();
                if (carrier_code == null)
                {
                    OrdersGrid["cmbOrderService", e.RowIndex].Value = null;
                    //clear the current rate and dimensional weight when the carrier selection changes
                    tbCurrentRate.Text = "";
                    tbDimensionalWeight.Text = "";
                }
                else
                {
                    object service_code = OrdersGrid["cmbOrderService", e.RowIndex].Value;
                    if (service_code != null && data.Services.FindByCarrier_CodeService_Code(carrier_code.ToString(), service_code.ToString()) == null)
                    {
                        OrdersGrid["cmbOrderService", e.RowIndex].Value = null;
                        //clear the current rate and dimensional weight when the carrier selection changes
                        tbCurrentRate.Text = "";
                        tbDimensionalWeight.Text = "";
                    }
                }
            }
            else if (e.ColumnIndex == OrdersGrid.Columns["cmbOrderService"].Index)
            {
                (OrdersGrid[e.ColumnIndex, e.RowIndex] as DataGridViewComboBoxCell).DataSource = ServicesBS;
                ServicesBSFiltered.RemoveFilter();
                //clear the current rate and dimensional weight when the service selection changes
                tbCurrentRate.Text = "";
                tbDimensionalWeight.Text = "";
            }
            //else if (e.ColumnIndex == OrdersGrid.Columns["cmbBillTo"].Index)
            //{
            //    DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            //    Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_row.Row["Order #"].ToString());
                
            //    if (!order["Billing Type"].ToString().Equals("1"))
            //    {
            //        if (Template.Name.Equals("FootPrint"))
            //        {
            //            if (data.OrdersTable.Columns.IndexOf("Billing Account") < 0)
            //            {
            //                data.OrdersTable.Columns.Add("Billing Account");
            //            }
            //            if (String.IsNullOrWhiteSpace(order["Billing Account"].ToString()))
            //            {
            //                frmBillingInfo billing = new frmBillingInfo();

            //                if (billing.ShowDialog(this) != DialogResult.OK)
            //                {
            //                    return;
            //                }
            //                else
            //                {
            //                    AddBillingInfoFields(order, billing.getBillingInfo);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (data.OrdersTable.Columns.IndexOf("Billing Account") < 0)
            //            {
            //                data.OrdersTable.Columns.Add("Billing Account");
            //            }
            //            if (String.IsNullOrWhiteSpace(order["Billing Account"].ToString()))
            //            {
            //                order["Billing Account"] = frmInputBox.InputBox(this, "Billing Account");
            //            }

            //        }

            //    }
            //}
        }

        private void cmdRateCheapestService_Click(object sender, EventArgs e)
        {
            EndEdit();
            try
            {
                DataView orders = OrdersTableBindingSource.List as DataView;
                if (orders == null) return;
                List<InteGr8Task> tasks = new List<InteGr8Task>(orders.Count);
                bool bIsSelected = false;
                foreach (DataRowView order_row in orders)
                {
                    if (order_row["Selected"] != null && order_row["Selected"] != DBNull.Value)
                    {
                        object order_number = order_row["Order #"];
                        bool order_selected = Convert.ToBoolean(order_row["Selected"]);
                        if (order_number == null || !order_selected || order_number.Equals("")) continue;
                        bIsSelected = true;
                        tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.RateCheapestService, order_number.ToString()));
                    }
                }
                if (!bIsSelected)
                {
                    DataRowView order = OrdersTableBindingSource.Current as DataRowView;
                    if (order != null)
                    {
                        tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.RateCheapestService, order["Order #"].ToString()));
                    }
                    else
                    {
                        MessageBox.Show("Please select at least one order!", "Warning");
                    }
                }
                if (tasks == null || tasks.Count == 0)
                {
                    return;
                }
                StartTasks(tasks.ToArray());
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in cmdRateCheapestService_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmdRateFastestService_Click(object sender, EventArgs e)
        {
            EndEdit();
            try
            {
                DataView orders = OrdersTableBindingSource.List as DataView;
                if (orders == null)
                {
                    return;
                }
                List<InteGr8Task> tasks = new List<InteGr8Task>(orders.Count);
                bool bIsSelected = false;
                foreach (DataRowView order_row in orders)
                {

                    if (order_row["Selected"] != null && order_row["Selected"] != DBNull.Value)
                    {
                        object order_number = order_row["Order #"];
                        bool order_selected = Convert.ToBoolean(order_row["Selected"]);
                        if (order_number == null || !order_selected || order_number.Equals(""))
                        {
                            continue;
                        }
                        bIsSelected = true;
                        tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.RateFastestService, order_number.ToString()));
                    }
                }
                if (!bIsSelected)
                {
                    DataRowView order = OrdersTableBindingSource.Current as DataRowView;
                    if (order != null)
                    {
                        tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.RateFastestService, order["Order #"].ToString()));
                    }
                    else
                    {
                        MessageBox.Show("Please select at least one order!", "Warning");
                    }
                }
                if (tasks == null || tasks.Count == 0)
                {
                    return;
                }
                StartTasks(tasks.ToArray());
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in cmdRateFastestService_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmdRateAllServices_Click(object sender, EventArgs e)
        {
            EndEdit();
            try
            {
                DataRowView order = OrdersTableBindingSource.Current as DataRowView;
                if (order == null)
                {
                    return;
                }
                System.Diagnostics.Debug.WriteLine("Time before starting the rating task: " + DateTime.Now);
                
                StartTasks(new InteGr8Task[] { new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.Rate, order.Row["Order #"].ToString()) });
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in cmdRateAllServices_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmdCommodityDelete_Click(object sender, EventArgs e)
        {
            EndEdit();
            DataRowView product_row = fKOrdersTableOrderProductsTableBindingSource.Current as DataRowView;
            if (product_row != null)
            {
                Data.OrderProductsTableRow product = product_row.Row as Data.OrderProductsTableRow;
                if (product != null)
                {
                    data.OrderProductsTable.RemoveOrderProductsTableRow(product);
                }
            }
        }

        private void EndEdit()
        {
            OrdersGrid.EndEdit();
            OrderPackagesGrid.EndEdit();
            ProductsGrid.EndEdit();
        }

        #region FillBackgroundWorker

        private void FillBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            System.IO.File.AppendAllText("Error.txt", "Fill worker starts task: " + DateTime.Now + "\r\n");
            string current_Filter_Expression = Select_Filter.Filter_With_Status;
            if (filterExpressionBindingSource.Count > 0)
            {
                Filter_Expression cfex = (Filter_Expression)filterExpressionBindingSource.Current;
                if (cfex.Join_Type != Filter_Expression_Join_Type.NONE)
                {
                    cfex.Join_Type = Filter_Expression_Join_Type.NONE;
                }
                if (cfex.Is_Custom)
                {
                    if (!Template.Name.Equals("Chocolate Inn"))
                    {
                        current_Filter_Expression = filterExpressionBindingSource.Current.ToString().Replace("@", "");
                        int i = current_Filter_Expression.IndexOf("'");
                        if (i != -1)
                        {
                            int f = current_Filter_Expression.IndexOf("'", i + 1);
                            string s = current_Filter_Expression.Substring(i + 1, f - i - 1);
                            if (s.All(c => Char.IsDigit(c)))
                            {
                                current_Filter_Expression = current_Filter_Expression.Replace("'", "");
                            }
                        }
                    }
                }
                else
                {
                    current_Filter_Expression = filterExpressionBindingSource.Current.ToString();
                }
            }
            //if there are more than 1 filter expressions and no join type between them, use only the current row as a filter
            System.IO.File.AppendAllText("Error.txt", "Backup file load settings: " + DateTime.Now + "\r\n");
            BackupFile.LoadSettings(t);
            System.IO.File.AppendAllText("Error.txt", "Backup settings loaded, load emails: " + DateTime.Now + "\r\n");
           
            BackupFile.LoadSettingList("Emails", t);
            System.IO.File.AppendAllText("Error.txt", "Emails loaded, load carriers: " + DateTime.Now + "\r\n");
          
            BackupFile.LoadSettingList("Carriers", t);
            System.IO.File.AppendAllText("Error.txt", "Carriers loaded, load template now: " + DateTime.Now + "\r\n");
          
            FillBackgroundWorker.ReportProgress(1);
            //read the orders information from database
            data.LoadTemplate(current_Filter_Expression);


            System.IO.File.AppendAllText("Error.txt", "Template loaded, complete shipments: " + DateTime.Now + "\r\n");
          
            data.Complete_shipments();
         
            System.IO.File.AppendAllText("Error.txt", "Shipments completed, load backup: " + DateTime.Now + "\r\n");
          
            BackupFile.Load(data);
            System.IO.File.AppendAllText("Error.txt", "Backup loaded: " + DateTime.Now + "\r\n");
          
            foreach (Data.ShipmentsTableRow r in data.ShipmentsTable)
            {
                Data.OrdersTableRow rr = data.OrdersTable.FindBy_Order__(r._Order__);
                if (rr != null)
                {
                    rr.Status = "Shipped";
                }
            }
            //*show only the first row(shipment level) for unsaved shipments
            if (data.ShipmentsTable.Rows.Count > 0)
            {
                shipmentsTableBindingSource.Filter = "Package_Index = 0";
            }
            System.IO.File.AppendAllText("Error.txt", "Fill finishes work: " + DateTime.Now + "\r\n");
          
        }

        private void FillBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            FillProgress.ShowProgress("Loading data, please wait");
        }

        private void FillBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.IO.File.AppendAllText("Error.txt", "Fill completed starts work: " + DateTime.Now + "\r\n");
          

                OrdersTableBindingSource.RaiseListChangedEvents = true;
                fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = true;
                fKOrdersTableOrderProductsTableBindingSource.RaiseListChangedEvents = true;
                fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = true;
                fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = true;
                fKOrdersTableRequestsTableBindingSource.RaiseListChangedEvents = true;

                OrdersTableBindingSource.ResetBindings(true);
                fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(true);
                fKOrdersTableOrderProductsTableBindingSource.ResetBindings(true);
                fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(true);
                fkOrdersTableLTLItemsBindingSource.ResetBindings(true);
                fKOrdersTableRequestsTableBindingSource.ResetBindings(true);

                OrdersTableBindingSource.ResumeBinding();
                fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();
                fKOrdersTableOrderProductsTableBindingSource.ResumeBinding();
                fkOrdersTableOrderSkidsTableBindingSource.ResumeBinding();
                fkOrdersTableLTLItemsBindingSource.ResumeBinding();
                fKOrdersTableRequestsTableBindingSource.ResumeBinding();

                OrderPackagesGrid.DataSource = fKOrdersTableOrderPackagesTableBindingSource;
                OrderSkidsGrid.DataSource = fkOrdersTableOrderSkidsTableBindingSource;
                ProductsGrid.DataSource = fKOrdersTableOrderProductsTableBindingSource;
                LTLItemsGrid.DataSource = fkOrdersTableLTLItemsBindingSource;
                // set as visible the multiple shipment options
                if (Select_Filter.Is_Empty)
                {
                    labelCarrier.Visible = true;
                    cmbMenuCarrier.Visible = true;
                    labelService.Visible = true;
                    cmbMenuService.Visible = true;
                    btnApplyCarrierServiceToSelected.Visible = true;
                    tsCarrierAndServiceApply.Visible = true;
                    cmbMenuCarrier.ComboBox.DisplayMember = "Name";
                    cmbMenuCarrier.ComboBox.ValueMember = "Carrier_Code";
                    cmbMenuCarrier.ComboBox.DataSource = bsMenuCarriers;
                    cmbMenuService.ComboBox.DisplayMember = "Name";
                    cmbMenuService.ComboBox.ValueMember = "Unique_Code";
                    cmbMenuService.ComboBox.DataSource = bsMenuServices;
                    bsMenuCarriers.Position = -1;
                    bsMenuServices.Position = -1;
                }
                else
                {
                    labelCarrier.Visible = false;
                    cmbMenuCarrier.Visible = false;
                    labelService.Visible = false;
                    cmbMenuService.Visible = false;
                    btnApplyCarrierServiceToSelected.Visible = false;
                    tsCarrierAndServiceApply.Visible = false;
                }

                //set PackageFields combo box data source
                cbPackageField.DataSource = new BindingSource(PackageFields, null);
                cbPackageField.ValueMember = "Key";
                cbPackageField.DisplayMember = "Value";

                
                //filter grid 
                if (!Select_Filter.Is_Empty)
                {
                    filterExpressionBindingSource.RaiseListChangedEvents = true;
                    filterExpressionBindingSource.ResetBindings(true);
                    filterExpressionBindingSource.ResumeBinding();
                }

                // autosize columns
                OrdersGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                OrderPackagesGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

                OrdersLabel.Text = "Orders waiting to be shipped:";

                if (e.Error != null)
                {
                    MessageBox.Show(this, e.Error.Message, "Error loading template", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


                // if Shipment level, add the column Total weight to packages datatable
                if (!Template.MPSPackageLevel)
                {
                    if (data.OrderPackagesTable.Columns.IndexOf("Total Weight") < 0)
                    {
                        data.OrderPackagesTable.Columns.Add("Total Weight");
                    }
                    if (data.OrdersTable.Columns.IndexOf("Packages") < 0)
                    {
                        data.OrdersTable.Columns.Add("Packages");
                    }
                }
                // row numbers
                SetRowNumbers();
                ChangeButtonsStatus(true);

                if (data.OrdersTable.Columns.Contains("Packages"))
                {
                    foreach (Data.OrdersTableRow order in data.OrdersTable)
                    {
                        Data.OrderPackagesTableRow[] packs = order.GetOrderPackagesTableRows();
                        order["Packages"] = packs.Length;
                        foreach (Data.OrderPackagesTableRow p in packs)
                        {
                            p["Total Weight"] = p["Package Weight"];
                        }
                    }
                    OrdersGrid.Columns["Packages"].DisplayIndex = 2;
                }

                // restore the orders grid columns order
                try
                {
                    //if (OrdersGridColumnsOrder == null)
                    //{
                        try
                        {
                            if (Template.BackupConn.SupportsDelete)
                            {
                                //if there is a columns order file, load the order from there
                                OrdersGridColumnsOrder = ColumnsSettings.Load("Orders", t);

                            }
                            if (OrdersGridColumnsOrder == null)
                            {
                                // load the columns order from the user settings file
                                OrdersGridColumnsOrder = Properties.Settings.Default.OrdersGridColumnsOrder;
                            }

                        }
                        catch (Exception ee)
                        {
                            OrdersGridColumnsOrder = new StringCollection();
                        }
                    //}
                    //else
                    //{
                    //    // save the current columns order
                    //    OrdersGridColumnsOrder.Clear();
                    //    int i = 0;
                    //    foreach (DataGridViewColumn column in OrdersGrid.Columns)
                    //    {
                    //        OrdersGridColumnsOrder.Add(string.Format(
                    //            "{0},{1},{2},{3}",
                    //            column.DisplayIndex.ToString("D2"),
                    //            column.Width,
                    //            column.Visible,
                    //            i++));
                    //    }
                    //}

                    // restore the orders grid columns order
                    RestoreOrdersGridColumnsOrder();

                    if (OrdersGridColumnsOrder != null && OrdersGridColumnsOrder.Count != 0)
                    {
                        try
                        {
                            string[] colsArray0 = new string[OrdersGridColumnsOrder.Count];
                            OrdersGridColumnsOrder.CopyTo(colsArray0, 0);
                            Array.Sort(colsArray0);
                            for (int i = 0; i < colsArray0.Length; ++i)
                            {
                                string[] a = colsArray0[i].Split(',');
                                int index = Convert.ToInt32(a[3]);
                                if (index >= OrdersGrid.Columns.Count)
                                {
                                    continue;
                                }
                                OrdersGrid.Columns[index].DisplayIndex = Convert.ToInt32(a[0]);
                                OrdersGrid.Columns[index].Width = Convert.ToInt32(a[1]);
                                OrdersGrid.Columns[index].Visible = Convert.ToBoolean(a[2]);
                            }
                        }
                        catch (Exception)
                        {
                            //ignore   
                        }
                    }

                   
                if (data.OrdersTable.Columns.IndexOf("Selected") >= 0)
                {
                    foreach (Data.OrdersTableRow order in data.OrdersTable)
                    {
                        order["Selected"] = false;
                    }
                }
                //using (StreamWriter write = File.AppendText("OrdersTest.txt"))
                //{
                //    write.WriteLine("Index  Column name");
                //    foreach (DataGridViewColumn col in OrdersGrid.Columns)
                //    {
                //        write.WriteLine(col.DisplayIndex + "   -   " + col.Name);
                //    }
                //}

                //restore packages grid columns order from the user settings file
                RestorePackagesColumnsOrder();

                //restore the packages grid columns order
                StringCollection temp = null;
                try
                {
                    //if there is a columns order file, load the order from there
                    if (Template.BackupConn.SupportsDelete)
                    {
                        temp = ColumnsSettings.Load("Packages", t);
                    }
                    if (temp == null)
                    {
                        // load the columns order from the user settings file
                        temp = Properties.Settings.Default.PackagesGridColumnsOrder;
                    }
                }
                catch (Exception ee)
                {
                    temp = new StringCollection();
                }
                if (temp != null && temp.Count != 0)
                {
                    try
                    {
                        string[] colsArray1 = new string[temp.Count];
                        temp.CopyTo(colsArray1, 0);
                        Array.Sort(colsArray1);
                        for (int i = 0; i < colsArray1.Length; ++i)
                        {
                            string[] a = colsArray1[i].Split(',');
                            int index = Convert.ToInt32(a[3]);
                            if (index >= OrderPackagesGrid.Columns.Count)
                            {
                                continue;
                            }
                            OrderPackagesGrid.Columns[index].DisplayIndex = Convert.ToInt32(a[0]);
                            OrderPackagesGrid.Columns[index].Width = Convert.ToInt32(a[1]);
                            OrderPackagesGrid.Columns[index].Visible = Convert.ToBoolean(a[2]);
                        }
                    }
                    catch (Exception)
                    {
                        //ignore   
                    }
                }
                // if (Template.Name.ToLower().Contains("bodyplus"))
                     SetDTPShipDate();

                //restore the products grid columns order
                temp = new StringCollection();
                try
                {
                    //if there is a columns order file, load the order from there
                    if (Template.BackupConn.SupportsDelete)
                    {
                        temp = ColumnsSettings.Load("Products", t);
                    }
                    if (temp == null)
                    {
                        // load the columns order from the user settings file
                        temp = Properties.Settings.Default.ProductsGridColumns;
                    }
                }
                catch (Exception ee)
                {
                    temp = new StringCollection();
                }
                if (temp != null && temp.Count != 0)
                {
                    try
                    {
                        string[] colsArray1 = new string[temp.Count];
                        temp.CopyTo(colsArray1, 0);
                        Array.Sort(colsArray1);
                        for (int i = 0; i < colsArray1.Length; ++i)
                        {
                            string[] a = colsArray1[i].Split(',');
                            int index = Convert.ToInt32(a[3]);
                            if (index >= ProductsGrid.Columns.Count)
                            {
                                continue;
                            }
                            ProductsGrid.Columns[index].DisplayIndex = Convert.ToInt32(a[0]);
                            ProductsGrid.Columns[index].Width = Convert.ToInt32(a[1]);
                            ProductsGrid.Columns[index].Visible = Convert.ToBoolean(a[2]);
                        }
                    }
                    catch (Exception)
                    {
                        //ignore
                    }
                }

                HideDuplicateColumns();
                if (Template.Name == "")
                {
                    if (refresh_click)
                    {
                        foreach (DataGridViewRow r in OrdersGrid.Rows)
                        {
                            Data.ProductsInPackagesRow[] temp0 = InteGr8_Main.data.ProductsInPackages.Select("[Order #] = '" + r.Cells["txtOrderNumber"].Value.ToString().TrimEnd() + "'") as Data.ProductsInPackagesRow[];

                            foreach (Data.ProductsInPackagesRow rr in temp0)
                            {
                                InteGr8_Main.data.ProductsInPackages.Rows.Remove(rr);
                            }

                            Data.ProductsSerialNumbersRow[] temp1 = InteGr8_Main.data.ProductsSerialNumbers.Select("[Order #] = '" + r.Cells["txtOrderNumber"].Value.ToString().TrimEnd() + "'") as Data.ProductsSerialNumbersRow[];

                            foreach (Data.ProductsSerialNumbersRow rr in temp1)
                            {
                                InteGr8_Main.data.ProductsSerialNumbers.Rows.Remove(rr);
                            }
                        }
                        refresh_click = false;
                    }
                    refresh_products_rows();
                    data.Complete_shipments();
                }
                if (data.ShipmentsTable.Rows.Count == 0)
                {
                    if (File.Exists(Template.Name + "_bkp.xml"))
                    {
                        File.Delete(Template.Name + "_bkp.xml");
                    }
                }

                try
                {
                    //if a scale is connected, then the GetWeight button will be enabled and we can read the weight when adding a new package
                    if (Properties.Settings.Default.InputWeight)
                    {
                        ReadWeight();
                    }
                }
                catch (Exception ex2)
                {
                    Utils.SendError("Error at reading weight from scale, in ReloadInfo", ex2);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                FillProgress.Hide();
                if (Template.Name == "")
                {
                    refresh_orders_rows();
                    ProductsGrid.Columns["Order #"].HeaderText = "PPS Number";
                    ProductsGrid.Columns["Scan_Id"].HeaderText = "Scan #";
                    ProductsGrid.Columns["Serial_Lot"].HeaderText = "Serial or Lot";
                    OrderPackagesGrid.Columns["Order #"].HeaderText = "PPS Number";
                }
                if (MainPages.SelectedTab == OrdersPage)
                {
                    tbValue1.Focus();
                }
                tbValue1.Text = "";
                System.IO.File.AppendAllText("Error.txt", "Fill completed done: " + DateTime.Now + "\r\n");
          
            }

        }

        private void RestoreOrdersGridColumnsOrder()
        {
            try
            {
                string[] colsArray = new string[OrdersGridColumnsOrder.Count];
                OrdersGridColumnsOrder.CopyTo(colsArray, 0);
                Array.Sort(colsArray);
                for (int i = 0; i < colsArray.Length; ++i)
                {
                    string[] a = colsArray[i].Split(',');
                    int index = Convert.ToInt32(a[3]);
                    if (index >= OrdersGrid.Columns.Count)
                    {
                        continue;
                    }
                    OrdersGrid.Columns[index].DisplayIndex = Convert.ToInt32(a[0]);
                    OrdersGrid.Columns[index].Width = Convert.ToInt32(a[1]);
                    OrdersGrid.Columns[index].Visible = Convert.ToBoolean(a[2]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("OrdersGrid columns order exception: " + ex.Message + "\n" + ex.StackTrace);
                // ignore
            }
        }

        private void HideDuplicateColumns()
        {
            try
            {
                foreach (DataGridViewColumn col in OrdersGrid.Columns)
                {
                    if ((col.HeaderText.IndexOf(" ") < 0) && (col.HeaderText.Equals(col.HeaderText.ToLower().Replace("/", "").Replace(" ", "")) || col.HeaderText.Equals(col.HeaderText.ToUpper().Replace("/", "").Replace(" ", ""))) && (!col.Name.ToLower().Equals("ordernumber")))
                    {
                        col.Visible = false;
                    }
                    if (col.Name.ToLower().Equals("ordernumber"))
                    {
                        col.Visible = true;
                        col.Name = "OrderNumber";
                        col.HeaderText = "Order Number";
                    }
                }
                
                foreach (DataGridViewColumn col in OrderPackagesGrid.Columns)
                {
                    if ((col.HeaderText.IndexOf(" ") < 0) && (col.HeaderText.Equals(col.HeaderText.ToLower().Replace("/", "").Replace(" ", "")) || col.HeaderText.Equals(col.HeaderText.ToUpper().Replace("/", "").Replace(" ", ""))) && (!col.HeaderText.ToLower().Equals("ordernumber")))
                    {
                        col.Visible = false;
                    }
                }
                foreach (DataGridViewColumn col in ProductsGrid.Columns)
                {
                    if ((col.HeaderText.IndexOf(" ") < 0) && (col.HeaderText.Equals(col.HeaderText.ToLower().Replace("/", "").Replace(" ", "")) || col.HeaderText.Equals(col.HeaderText.ToUpper().Replace("/", "").Replace(" ", ""))) && (!col.HeaderText.ToLower().Equals("ordernumber")))
                    {
                        col.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error at hiding duplicate columns, for informix database", ex);
            }
        }

        private void SetRowNumbers()
        {
            try
            {
                if (OrdersGrid.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in OrdersGrid.Rows)
                    {
                        row.HeaderCell.Value = (row.Index + 1).ToString();
                    }
                }
            }
            catch (Exception e)
            {
                //
            }
        }

        private void RestorePackagesColumns()
        {
            try
            {
                string[] colsArray = new string[PackagesGridColumnsOrder.Count];
                PackagesGridColumnsOrder.CopyTo(colsArray, 0);
                Array.Sort(colsArray);
                for (int i = 0; i < colsArray.Length; ++i)
                {
                    string[] a = colsArray[i].Split(',');
                    int index = Convert.ToInt32(a[3]);
                    OrderPackagesGrid.Columns[index].DisplayIndex = Convert.ToInt32(a[0]);
                    OrderPackagesGrid.Columns[index].Width = Convert.ToInt32(a[1]);
                    OrderPackagesGrid.Columns[index].Visible = Convert.ToBoolean(a[2]);
                }
            }
            catch (Exception)
            {
                // ignore
            }
        }

        private void RestorePackagesColumnsOrder()
        {
            if (PackagesGridColumnsOrder == null)
            {
                try
                {
                    // load the columns order from the user settings file
                    PackagesGridColumnsOrder = Properties.Settings.Default.PackagesGridColumnsOrder;
                }
                catch (Exception)
                {
                    PackagesGridColumnsOrder = new StringCollection();
                }
            }
            else
            {
                // save the current columns order
                PackagesGridColumnsOrder.Clear();
                int i = 0;
                foreach (DataGridViewColumn column in OrderPackagesGrid.Columns)
                {
                    PackagesGridColumnsOrder.Add(string.Format(
                        "{0},{1},{2},{3}",
                        column.DisplayIndex.ToString("D2"),
                        column.Width,
                        column.Visible,
                        i++));
                }
            }
        }

        #endregion

        #region TaskBackgroundWorker

        List<print_class> print_list = new List<print_class>();

        private void StartTasks(InteGr8Task[] tasks)
        {
            try
            {
                ChangeButtonsStatus(false);
                StatusLabel.Text = "Processing, please wait...";
                progress = new frmProcessing(TaskBackgroundWorker);
                progress.Show(this);
                // start the webservice processing on a different non-ui thread
                if (!TaskBackgroundWorker.IsBusy)
                {
                    TaskBackgroundWorker.RunWorkerAsync(tasks);
                }
            }
            catch (Exception ex)
            {
                ChangeButtonsStatus(true);
                Utils.SendError("Error in StartTasks", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team. "+ ex.Message, "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                StatusLabel.Text = "Idle.";
                progress.Close();
            }
        }

        private void TaskBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument == null)
            {
                return;
            }
            System.IO.File.AppendAllText("Error.txt",  " Time when back task starts processing: " +DateTime.Now + "\r\n");
            InteGr8Task[] tasks = e.Argument as InteGr8Task[];
            string errors = "";
            string orders = "";
            List<InteGr8Task> new_tasks = new List<InteGr8Task>();
            //if (tasks.Length > 1)
            //{
                //new multiple threads code
                //declare the threads list
                List<System.Threading.Tasks.Task<InteGr8Task>> threadTasks = new List<System.Threading.Tasks.Task<InteGr8Task>>();
                //the count for processing 25 simultaneously
                int iCountTasks = 0;
                //current task to be added to taskThreads
                InteGr8Task currentTask = tasks[0];
                //general count used for reporting progress from TaskBackgroundWorker
                int threadNo = 0;
                while (iCountTasks < tasks.Length)
                {
                    //run up to 25 threads simultaneously
                    for (int i = iCountTasks; i < iCountTasks + 25; i++)
                    {
                        if (i < tasks.Length)
                        {
                            currentTask = tasks[i];
                            if (TaskBackgroundWorker.CancellationPending)
                            {
                                break;
                            }
                            TaskBackgroundWorker.ReportProgress(i, tasks);
                            System.IO.File.AppendAllText("Error.txt", " Start back task no. " + i + ": " + DateTime.Now + "\r\n");
                            threadTasks.Add(System.Threading.Tasks.Task<InteGr8Task>.Factory.StartNew((object obj) =>
                            {
                                (obj as InteGr8Task).Process();
                                return (obj as InteGr8Task);
                            }, currentTask));

                        }
                        else
                        {
                            break;
                        }
                    }
                    System.IO.File.AppendAllText("Error.txt", " Task no. : " + iCountTasks + " added: " + DateTime.Now + "\r\n");

                    System.Console.WriteLine("Time after adding " + (iCountTasks + 25) + " tasks: " + DateTime.Now);
                    //wait for all threads to finish execution
                    System.Threading.Tasks.Task.WaitAll(threadTasks.ToArray());

                    System.IO.File.AppendAllText("Error.txt", " All threads finished execution: " + DateTime.Now + "\r\n");

                    //System.Console.WriteLine("Time after " + (iCountTasks + 25) + " tasks finished processing: " + DateTime.Now);

                    // System.Console.WriteLine("Time before saving " + (iCountTasks + 25) + " tasks: " + DateTime.Now);
                    //read threads execution results

                    foreach (System.Threading.Tasks.Task<InteGr8Task> t in threadTasks)
                    {
                        currentTask = t.Result;
                        try
                        {
                            if (TaskBackgroundWorker.CancellationPending)
                            {
                                break;
                            }
                            TaskBackgroundWorker.ReportProgress(threadNo, tasks);
                            System.IO.File.AppendAllText("Error.txt", " Saving results for task " + t.Id + ": " + DateTime.Now + "\r\n");

                            currentTask.Save();
                            // System.Console.WriteLine("Time after saving task no." +threadNo + " is: " + DateTime.Now);
                            if (currentTask.ShipResult != null || currentTask.Rates != null || currentTask.EditResult != null || currentTask.delReq.WS_Key != null)//(currentTask.reply == null)
                            {
                                new_tasks.Add(currentTask);
                            }
                            else
                            {
                                //MessageBox.Show("WebService reply was null for order " + currentTask.order_number, "Reply null", MessageBoxButtons.OK);
                            }
                            threadNo++;
                        }
                        catch (Exception ex)
                        {
                            errors += "Error processing task for order " + currentTask.order_number + "\r\n" + ex.Message + "\r\n" + ex.StackTrace + "\r\n\r\n";
                            orders += currentTask.order_number + "\r\n";
                        }
                    }
                    iCountTasks += 25;
                    //remove all processed tasks
                    threadTasks.Clear();
                }

                if (errors != "")
                {
                    Utils.SendError(errors);
                    MessageBox.Show("Error processing task for order(s):\r\n " + orders + "\n An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                e.Result = new_tasks.ToArray();

                System.IO.File.AppendAllText("Error.txt", " Time when back task thread finishes execution: " + DateTime.Now + "\r\n");
            //}
            //else
            //{
            //    //original task work below
            //    for (int k = 0; k < tasks.Length; k++)
            //    {
            //        try
            //        {
            //            if (TaskBackgroundWorker.CancellationPending)
            //            {
            //                break;
            //            }
            //            System.IO.File.AppendAllText("Error.txt", " Start back task no. " + k + ": " + DateTime.Now + "\r\n");
                        
            //            TaskBackgroundWorker.ReportProgress(k, tasks);
            //            tasks[k].Process();
            //            System.IO.File.AppendAllText("Error.txt", " Back task no. " + k + " was processes, save results: " + DateTime.Now + "\r\n");
                        
            //            tasks[k].Save();

            //            if (tasks[k].reply == null)
            //            {
            //               // MessageBox.Show("WebService reply was null for task " + k, "Reply null", MessageBoxButtons.OK);
            //            }
            //            else
            //            {
            //                new_tasks.Add(tasks[k]);
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            errors += "Error processing task for order " + tasks[k].order_number + "\r\n" + ex.Message + "\r\n" + ex.StackTrace + "\r\n\r\n";
            //            orders += tasks[k].order_number + "\r\n";
            //        }
            //    }
            //    if (errors != "")
            //    {
            //        Utils.SendError(errors);
            //        MessageBox.Show("Error processing task for order(s):\r\n " + orders + "\n An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //    e.Result = new_tasks.ToArray();
            //    System.IO.File.AppendAllText("Error.txt", " Time when back task thread finishes execution: " + DateTime.Now + "\r\n");
            //}
        }

        private void TaskBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                if (e.UserState == null)
                {
                    return;
                }
                InteGr8Task[] tasks = e.UserState as InteGr8Task[];
                progress.ShowProgress("Task " + (e.ProgressPercentage + 1) + " of " + tasks.Length + "...", e.ProgressPercentage + 1, tasks.Length);
                //progress.ShowProgress2("");
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in TaskBackgroundWorker_ProgressChanged", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bg_print_DoWork(object sender, DoWorkEventArgs e)
        {
            if (Template.UseAutoprint)
            {
                //Labels section
                string error = "";
                //error = iTextSharpLabelsDoc(error);
                string message = "";
               try
                {
                   
                    if (print_list.Count > 0)
                    {
                        List<string> pdfLabels = new List<string>();
                        //PdfSharp.Pdf.PdfDocument outputDoc = new PdfSharp.Pdf.PdfDocument();
                        //string jobOrderNo = print_list.ElementAt(0).order.ToString().Trim() + "_" + print_list.ElementAt(print_list.Count - 1).order.ToString().Trim();
                        //if (print_list.Count > 1)
                        //{
                        //    foreach (print_class p in print_list)
                        //    {
                        //        if (!String.IsNullOrWhiteSpace(p.label))
                        //        {
                        //            try
                        //            {
                        //                string pdfLabel = Utils.download(p.label, Template.AutoprintLabelsFolder);
                        //                //pdfLabels.Add(pdfLabel);
                        //                try
                        //                {
                        //                    PdfSharp.Pdf.PdfDocument inputDoc = PdfSharp.Pdf.IO.PdfReader.Open(Path.Combine(Template.AutoprintLabelsFolder, pdfLabel), PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);

                        //                    int Count = inputDoc.PageCount;
                        //                    for (int i = 0; i < Count; i++)
                        //                    {
                        //                        PdfSharp.Pdf.PdfPage page = inputDoc.Pages[i];
                        //                        outputDoc.AddPage(page);
                        //                    }
                        //                }
                        //                catch (PdfSharpException pdfEx)
                        //                {
                        //                    using (StreamWriter write = File.AppendText(Path.Combine(Template.AutoprintLabelsFolder, "ErrorLabels.txt")))
                        //                    {
                        //                        write.WriteLine("Could not open file " + p.label + " for order # " + p.order + "\n" + pdfEx.Message + "\n" + pdfEx.StackTrace + "\n");
                        //                    }
                        //                }
                        //            }
                        //            catch (Exception ee1)
                        //            {
                        //                //write in an error text file the name of the labels that could not be opened(they were corrupted, etc.)
                        //                using (StreamWriter write = File.AppendText(Path.Combine(Template.AutoprintLabelsFolder, "ErrorLabels.txt")))
                        //                {
                        //                    write.WriteLine("Could not open file " + p.label + " for order # " + p.order + "\n" + ee1.Message + "\n" + ee1.StackTrace + "\n");
                        //                }
                        //                //continue;

                        //                //Utils.SendError("Error at adding labels to printing list", ee1);
                        //            }
                        //        }
                        //    }
                        //    if (outputDoc.CanSave(ref message) && (outputDoc.PageCount > 0))
                        //    {
                        //        string fileName = "Labels_" + jobOrderNo + ".pdf";
                        //        outputDoc.Save(Path.Combine(Template.AutoprintLabelsFolder, fileName));

                        //        Utils.printPDF(fileName, Template.AutoprintLabelsFolder, Template.AutoprintLabelsPrinter);
                        //    }
                        //}
                        //else
                       // {
                       // System.IO.File.AppendAllText("Error.txt", " Print thread downloading labels: " + DateTime.Now + "\r\n");
         
                            foreach (print_class p in print_list)
                            {
                                if (!String.IsNullOrWhiteSpace(p.label))
                                {
                                    string pdfLabel = Utils.download(p.label, Template.AutoprintLabelsFolder);
                                    pdfLabels.Add(pdfLabel);
                                }
                            }
                            //System.IO.File.AppendAllText("Error.txt", "Labels downloaded, send pdfs to printer: " + DateTime.Now + "\r\n");
         
                            foreach (string s in pdfLabels)
                            {
                                Utils.printPDF(s, Template.AutoprintLabelsFolder, Template.AutoprintLabelsPrinter);
                            }
                             //close Adobe only after all labels have been printed out
                            //Utils.closeAdobe();
                           
                            //if (!String.IsNullOrWhiteSpace(print_list[0].label)) { 
                            //string pdfLabel = Utils.download(print_list[0].label, Template.AutoprintLabelsFolder);
                            //pdfLabels.Add(pdfLabel);
                            //foreach (string s in pdfLabels)
                            //{
                            //    Utils.printPDF(s, Template.AutoprintLabelsFolder, Template.AutoprintLabelsPrinter);
                            //}

                            //}
                        //}
                    }
                }
                catch (Exception e1)
                {
                    if (!String.IsNullOrEmpty(message))
                    {
                        Utils.SendError("Error at printing labels. The PrintPDF inner message is: " + message, e1);   
                    }
                    else
                    {
                        Utils.SendError("Error at printing labels", e1);   
                    }
                    
                }

                //Commercial Invoice section
                //error = iTextSharpCI(error);
               
                try
                {
                    if (print_list.Count > 0)
                    {
                        List<string> pdfCIs = new List<string>();
                        message = "";
                        PdfSharp.Pdf.PdfDocument outputDoc = new PdfSharp.Pdf.PdfDocument();
                        string jobOrderNo = print_list.ElementAt(0).order.ToString().Trim() + "_" + print_list.ElementAt(print_list.Count - 1).order.ToString().Trim();
                        //if (print_list.Count > 1)
                        //{
                        //    foreach (print_class p in print_list)
                        //    {
                        //        if (!String.IsNullOrWhiteSpace(p.commercial_invoice))
                        //        {
                        //            try
                        //            {
                        //                string pdfCI = Utils.download(p.commercial_invoice, Template.AutoprintCIFolder);
                        //                PdfSharp.Pdf.PdfDocument inputDoc = PdfSharp.Pdf.IO.PdfReader.Open(Path.Combine(Template.AutoprintCIFolder, pdfCI), PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                        //                int Count = inputDoc.PageCount;
                        //                for (int i = 0; i < Count; i++)
                        //                {
                        //                    PdfSharp.Pdf.PdfPage page = inputDoc.Pages[i];
                        //                    outputDoc.AddPage(page);
                        //                }
                        //            }
                        //            catch (Exception ee2)
                        //            {
                        //                //write in an error text file the name of the labels that could not be opened(they were corrupted, etc.)
                        //                using (StreamWriter write = File.AppendText(Path.Combine(Template.AutoprintCIFolder, "ErrorCIs.txt")))
                        //                {
                        //                    write.WriteLine("Could not open file " + p.commercial_invoice + " for order # " + p.order + "\n" + ee2.Message + "\n" + ee2.StackTrace + "\n");
                        //                }
                        //                continue;
                        //            }
                        //        }
                        //    }

                        //    if (outputDoc.CanSave(ref message) && (outputDoc.PageCount > 0))
                        //    {
                        //        string fileName = "CIs_" + jobOrderNo + ".pdf";
                        //        outputDoc.Save(Path.Combine(Template.AutoprintCIFolder, fileName));
                        //        Utils.printPDF(fileName, Template.AutoprintCIFolder, Template.AutoprintCIPrinter);
                        //    }
                        //}
                        //else
                        //{
                            foreach (print_class p in print_list)
                            {
                                if ((!String.IsNullOrWhiteSpace(p.commercial_invoice)) && (!p.commercial_invoice.Contains("N/A")))
                                {
                                    string pdfCI = Utils.download(p.commercial_invoice, Template.AutoprintCIFolder);
                                    pdfCIs.Add(pdfCI);
                                    foreach (string s in pdfCIs)
                                    {
                                        Utils.printPDF(s, Template.AutoprintCIFolder, Template.AutoprintCIPrinter);
                                    }
                                }
                            }
                            //if (!String.IsNullOrWhiteSpace(print_list[0].commercial_invoice)) {
                            //string pdfCI = Utils.download(print_list[0].commercial_invoice, Template.AutoprintCIFolder);
                            //pdfCIs.Add(pdfCI);
                            //foreach (string s in pdfCIs)
                            //{
                            //    Utils.printPDF(s, Template.AutoprintCIFolder, Template.AutoprintCIPrinter);
                            //}
                          //}
                        //}
                    }
                }
                catch (Exception e2)
                {
                    if (!String.IsNullOrEmpty(message))
                    {
                        Utils.SendError("Error at printing commercial invoices. The PrintPDF inner message is: " + message, e2);
                    }
                    else
                    {
                        Utils.SendError("Error at printing commercial invoices", e2);
                    }
                }

                //BOL section
                //error = iTextSharpBOL(error);
                try
                {
                    if (print_list.Count > 0)
                    {
                        message = "";
                        List<string> pdfBOLs = new List<string>();
                        PdfSharp.Pdf.PdfDocument outputDoc = new PdfSharp.Pdf.PdfDocument();
                        string jobOrderNo = print_list.ElementAt(0).order.ToString().Trim() + "_" + print_list.ElementAt(print_list.Count - 1).order.ToString().Trim();
                        if (print_list.Count > 1)
                        {
                            foreach (print_class p in print_list)
                            {
                                if (!String.IsNullOrWhiteSpace(p.bol))
                                {
                                    try
                                    {
                                        string pdfBOL = Utils.download(p.bol, Template.AutoprintBOLFolder);

                                        PdfSharp.Pdf.PdfDocument inputDoc = PdfSharp.Pdf.IO.PdfReader.Open(Path.Combine(Template.AutoprintBOLFolder, pdfBOL), PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                                        int Count = inputDoc.PageCount;
                                        for (int i = 0; i < Count; i++)
                                        {
                                            PdfSharp.Pdf.PdfPage page = inputDoc.Pages[i];
                                            outputDoc.AddPage(page);
                                        }
                                    }
                                    catch (Exception ee3)
                                    {
                                        //write in an error text file the name of the labels that could not be opened(they were corrupted, etc.)
                                        using (StreamWriter write = File.AppendText(Path.Combine(Template.AutoprintBOLFolder, "ErrorBOLs.txt")))
                                        {
                                            write.WriteLine("Could not open file " + p.bol + " for order # " + p.order + "\n" + ee3.Message + "\n" + ee3.StackTrace);
                                        }
                                        continue;

                                    }
                                }
                            }
                            if (outputDoc.CanSave(ref message) && (outputDoc.PageCount > 0))
                            {
                                string fileName = "BOLs_" + jobOrderNo + ".pdf";
                                outputDoc.Save(Path.Combine(Template.AutoprintBOLFolder, fileName));

                                Utils.printPDF(fileName, Template.AutoprintBOLFolder, Template.AutoprintBOLPrinter);
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(print_list[0].bol))
                            {
                                string pdfBOL = Utils.download(print_list[0].bol, Template.AutoprintBOLFolder);
                                pdfBOLs.Add(pdfBOL);
                                foreach (string s in pdfBOLs)
                                {
                                    Utils.printPDF(s, Template.AutoprintBOLFolder, Template.AutoprintBOLPrinter);
                                }
                            }
                        }
                    }

                }
                catch (Exception e3)
                {
                    if (!String.IsNullOrEmpty(message))
                    {
                        Utils.SendError("Error at printing BOLs. The PrintPDF inner message is: " + message, e3);
                    }
                    else
                    {
                        Utils.SendError("Error at printing BOLs", e3);
                    }
                }


                //Return Label section
                //error = iTextSharpReturnLabel(error);
                try
                {
                    if (print_list.Count > 0)
                    {
                        message = "";
                        List<string> pdfReturnLabels = new List<string>();
                        PdfSharp.Pdf.PdfDocument outputDoc = new PdfSharp.Pdf.PdfDocument();
                        string jobOrderNo = print_list.ElementAt(0).order.ToString().Trim() + "_" + print_list.ElementAt(print_list.Count - 1).order.ToString().Trim();

                        if (print_list.Count > 1)
                        {
                            foreach (print_class p in print_list)
                            {
                                if (!String.IsNullOrWhiteSpace(p.return_label))
                                {
                                    try
                                    {
                                        string pdfReturnLabel = Utils.download(p.return_label, Template.AutoprintReturnLabelFolder);

                                        PdfSharp.Pdf.PdfDocument inputDoc = PdfSharp.Pdf.IO.PdfReader.Open(Path.Combine(Template.AutoprintReturnLabelFolder, pdfReturnLabel), PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                                        int Count = inputDoc.PageCount;
                                        for (int i = 0; i < Count; i++)
                                        {
                                            PdfSharp.Pdf.PdfPage page = inputDoc.Pages[i];
                                            outputDoc.AddPage(page);
                                        }
                                    }
                                    catch (Exception ee4)
                                    {
                                        //write in an error text file the name of the labels that could not be opened(they were corrupted, etc.)
                                        using (StreamWriter write = File.AppendText(Path.Combine(Template.AutoprintReturnLabelFolder, "ErrorReturnLabels.txt")))
                                        {
                                            write.WriteLine("Could not open file " + p.return_label + " for order # " + p.order + "\n" + ee4.Message + "\n" + ee4.StackTrace);
                                        }
                                        continue;
                                    }
                                }
                            }
                            if (outputDoc.CanSave(ref message) && (outputDoc.PageCount > 0))
                            {
                                string fileName = "ReturnLabels_" + jobOrderNo + ".pdf";
                                outputDoc.Save(Path.Combine(Template.AutoprintReturnLabelFolder, fileName));
                                Utils.printPDF(fileName, Template.AutoprintReturnLabelFolder, Template.AutoprintReturnLabelPrinter);
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(print_list[0].return_label))
                            {
                                string pdfRetLabel = Utils.download(print_list[0].return_label, Template.AutoprintReturnLabelFolder);
                                pdfReturnLabels.Add(pdfRetLabel);
                                foreach (string s in pdfReturnLabels)
                                {
                                    Utils.printPDF(s, Template.AutoprintReturnLabelFolder, Template.AutoprintReturnLabelPrinter);
                                }
                            }
                        }
                    }
                }
                catch (Exception e4)
                {
                    if (!String.IsNullOrEmpty(message))
                    {
                        Utils.SendError("Error at printing return labels. The PrintPDF inner messsage is: "+ message, e4);
                    }
                    else
                    {
                        Utils.SendError("Error at printing return labels", e4);
                    }
                }

                //Packslip section
                try
                {
                    if (print_list.Count > 0)
                    {
                        message = "";
                        List<string> pdfPackslips = new List<string>();
                        PdfSharp.Pdf.PdfDocument outputDoc = new PdfSharp.Pdf.PdfDocument();
                        string jobOrderNo = print_list.ElementAt(0).order.ToString().Trim() + "_" + print_list.ElementAt(print_list.Count - 1).order.ToString().Trim();
                        if (print_list.Count > 1)
                        {
                            foreach (print_class p in print_list)
                            {
                                if (!String.IsNullOrWhiteSpace(p.packslip))
                                {
                                    try
                                    {
                                        string pdfPackslip = Utils.download(p.packslip, Template.AutoprintPackSlipFolder);
                                        //pdfLabels.Add(pdfLabel);

                                        PdfSharp.Pdf.PdfDocument inputDoc = PdfSharp.Pdf.IO.PdfReader.Open(Path.Combine(Template.AutoprintPackSlipFolder, pdfPackslip), PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                                        int Count = inputDoc.PageCount;
                                        for (int i = 0; i < Count; i++)
                                        {
                                            PdfSharp.Pdf.PdfPage page = inputDoc.Pages[i];
                                            outputDoc.AddPage(page);
                                        }
                                    }
                                    catch (Exception ee5)
                                    {
                                        //write in an error text file the name of the labels that could not be opened(they were corrupted, etc.)
                                        using (StreamWriter write = File.AppendText(Path.Combine(Template.AutoprintPackSlipFolder, "ErrorPackslips.txt")))
                                        {
                                            write.WriteLine("Could not open file " + p.packslip + " for order # " + p.order + "\n" + ee5.Message + "\n" + ee5.StackTrace);
                                        }
                                        continue;
                                    }
                                }
                            }
                            if (outputDoc.CanSave(ref message) && (outputDoc.PageCount > 0))
                            {
                                string fileName = "Packslips_" + jobOrderNo + ".pdf";
                                outputDoc.Save(Path.Combine(Template.AutoprintPackSlipFolder, fileName));
                                Utils.printPDF(fileName, Template.AutoprintPackSlipFolder, Template.AutoprintPackSlipPrinter);
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(print_list[0].packslip))
                            {
                                string pdfPackslip = Utils.download(print_list[0].packslip, Template.AutoprintPackSlipFolder);
                                pdfPackslips.Add(pdfPackslip);
                                foreach (string s in pdfPackslips)
                                {
                                    Utils.printPDF(s, Template.AutoprintPackSlipFolder, Template.AutoprintPackSlipPrinter);
                                }
                            }
                        }
                    }
                }
                catch (Exception e5)
                {
                    if (!String.IsNullOrEmpty(message))
                    {
                        Utils.SendError("Error at printing packslips. The PrintPDF inner message is: " + message, e5);
                    }
                    else
                    {
                        Utils.SendError("Error at printing packslips", e5);
                    }
                    
                }


                //Contents Label section
                //error = iTextSharpContentsLabel(error);
                try
                {
                    if (print_list.Count > 0)
                    {
                        message = "";
                        List<string> pdfContentLabels = new List<string>();
                        PdfSharp.Pdf.PdfDocument outputDoc = new PdfSharp.Pdf.PdfDocument();
                        string jobOrderNo = print_list.ElementAt(0).order.ToString().Trim() + "_" + print_list.ElementAt(print_list.Count - 1).order.ToString().Trim();
                        if (print_list.Count > 1)
                        {
                            foreach (print_class p in print_list)
                            {
                                if (p.comntentslabel.Count > 0)
                                {
                                    if (!String.IsNullOrWhiteSpace(p.comntentslabel[0]))
                                    {
                                        try
                                        {
                                            for (int count = 0; count < p.comntentslabel.Count; count++)
                                            {
                                                if (!String.IsNullOrWhiteSpace(p.comntentslabel[count]))
                                                {
                                                    string pdfContentsLabel = Utils.download(p.comntentslabel[count], Template.AutoprintContentsLabelFolder);

                                                    PdfSharp.Pdf.PdfDocument inputDoc = PdfSharp.Pdf.IO.PdfReader.Open(Path.Combine(Template.AutoprintContentsLabelFolder, pdfContentsLabel), PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                                                    int Count = inputDoc.PageCount;
                                                    for (int i = 0; i < Count; i++)
                                                    {
                                                        PdfSharp.Pdf.PdfPage page = inputDoc.Pages[i];
                                                        outputDoc.AddPage(page);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ee6)
                                        {
                                            //write in an error text file the name of the labels that could not be opened(they were corrupted, etc.)
                                            using (StreamWriter write = File.AppendText(Path.Combine(Template.AutoprintContentsLabelFolder, "ErrorContentsLabels.txt")))
                                            {
                                                write.WriteLine("Could not open file " + p.comntentslabel + " for order # " + p.order + "\n " + ee6.Message + "\n" + ee6.StackTrace);
                                            }
                                            continue;
                                        }
                                    }
                                }
                            }
                            if (outputDoc.CanSave(ref message) && (outputDoc.PageCount > 0))
                            {
                                string fileName = "ContentsLabels_" + jobOrderNo + ".pdf";
                                outputDoc.Save(Path.Combine(Template.AutoprintContentsLabelFolder, fileName));
                                Utils.printPDF(fileName, Template.AutoprintContentsLabelFolder, Template.AutoprintContentsLabelPrinter);
                            }
                        }
                        else
                        {
                            for (int count = 0; count < print_list[0].comntentslabel.Count; count++)
                            {
                                if (!String.IsNullOrWhiteSpace(print_list[0].comntentslabel[count]))
                                {
                                    string pdfContentsLabel = Utils.download(print_list[0].comntentslabel[count], Template.AutoprintContentsLabelFolder);
                                    //string pdfContentLabel = Utils.download(print_list[0].label, Template.AutoprintLabelsFolder);
                                    pdfContentLabels.Add(pdfContentsLabel);
                                }
                            }
                            foreach (string s in pdfContentLabels)
                            {
                                Utils.printPDF(s, Template.AutoprintContentsLabelFolder, Template.AutoprintContentsLabelPrinter);
                            }
                        }
                    }
                }
                catch (Exception e6)
                {
                    if (!String.IsNullOrEmpty(message))
                    {
                        Utils.SendError("Error at printing contents labels. The PrintPDF inner message is: " + message, e6);
                    }
                    else
                    {
                        Utils.SendError("Error at printing contents labels", e6);
                    }
                    
                }



                print_list = new List<print_class>();
                ////test labels printing without iTextSharp
                //if (File.Exists(Path.Combine(Template.AutoprintLabelsFolder, System.Environment.MachineName.ToString() + "Labels.pdf")))
                //{
                //    Utils.printPDF(System.Environment.MachineName.ToString() + "Labels.pdf", Template.AutoprintLabelsFolder, Template.AutoprintLabelsPrinter);
                //    //File.Delete(Path.Combine(Template.AutoprintLabelsFolder, System.Environment.MachineName.ToString() + "Labels.pdf"));
                //}

                //if (File.Exists(Path.Combine(Template.AutoprintCIFolder, System.Environment.MachineName.ToString() + "Commercial_Invoice.pdf")))
                //{
                //    Utils.printPDF(System.Environment.MachineName.ToString() + "Commercial_Invoice.pdf", Template.AutoprintCIFolder, Template.AutoprintCIPrinter);
                //    //File.Delete(Path.Combine(Template.AutoprintCIFolder, System.Environment.MachineName.ToString() + "Commercial_Invoice.pdf"));
                //}

                //if (File.Exists(Path.Combine(Template.AutoprintBOLFolder, System.Environment.MachineName.ToString() + "BOL.pdf")))
                //{
                //    Utils.printPDF(System.Environment.MachineName.ToString() + "BOL.pdf", Template.AutoprintBOLFolder, Template.AutoprintBOLPrinter);
                //    //File.Delete(Path.Combine(Template.AutoprintBOLFolder, System.Environment.MachineName.ToString() + "BOL.pdf"));
                //}

                //if (File.Exists(Path.Combine(Template.AutoprintReturnLabelFolder, System.Environment.MachineName.ToString() + "Return_Label.pdf")))
                //{
                //    Utils.printPDF(System.Environment.MachineName.ToString() + "Return_Label.pdf", Template.AutoprintReturnLabelFolder, Template.AutoprintReturnLabelPrinter);
                //    //File.Delete(Path.Combine(Template.AutoprintReturnLabelFolder, System.Environment.MachineName.ToString() + "Return_Label.pdf"));
                //}

                //if (File.Exists(Path.Combine(Template.AutoprintPackSlipFolder, System.Environment.MachineName.ToString() + "PackSlip.pdf")))
                //{
                //    Utils.printPDF(System.Environment.MachineName.ToString() + "PackSlip.pdf", Template.AutoprintPackSlipFolder, Template.AutoprintPackSlipPrinter);
                //    //File.Delete(Path.Combine(Template.AutoprintPackSlipFolder, System.Environment.MachineName.ToString() + "PackSlip.pdf"));
                //}

                //if (File.Exists(Path.Combine(Template.AutoprintContentsLabelFolder, System.Environment.MachineName.ToString() + "ContentLabel.pdf")))
                //{
                //    Utils.printPDF(System.Environment.MachineName.ToString() + "ContentLabel.pdf", Template.AutoprintContentsLabelFolder, Template.AutoprintContentsLabelPrinter);
                //    //File.Delete(Path.Combine(Template.AutoprintContentsLabelFolder, System.Environment.MachineName.ToString() + "ContentLabel.pdf"));
                //}

                e.Result = error;
            }
            else
            {
                print_list = new List<print_class>();
                e.Result = "Set AutoPrint to on from the option panel and try again";
            }
        }

        private string iTextSharpContentsLabel(string error)
        {
            bool contentslabel_haspdfs = false;
            try
            {
                List<PdfReader> close = new List<PdfReader>();
                string pdfFilePath_size = print_list[0].comntentslabel[0];
                PdfReader reader_size = new PdfReader(pdfFilePath_size);
                close.Add(reader_size);
                iTextSharp.text.Rectangle page_size = reader_size.GetPageSize(1);
                iTextSharp.text.Document ContentLabel = new iTextSharp.text.Document(page_size, 0f, 0f, 0f, 0f);
                PdfWriter writer3 = PdfWriter.GetInstance(ContentLabel, new FileStream(Path.Combine(Template.AutoprintContentsLabelFolder, System.Environment.MachineName.ToString() + "ContentLabel.pdf"), FileMode.Create));

                ContentLabel.Open();
                PdfContentByte cb3 = writer3.DirectContent;

                foreach (print_class p in print_list)
                {
                    foreach (string s in p.comntentslabel)
                    {
                        if (s != "" && s != "N/A")
                        {
                            try
                            {
                                string pdfFilePath = s;
                                PdfReader reader3 = new PdfReader(pdfFilePath);
                                close.Add(reader3);
                                for (int i = 1; i <= reader3.NumberOfPages; i++)
                                {
                                    ContentLabel.NewPage();
                                    cb3.AddTemplate(writer3.GetImportedPage(reader3, 1), 1f, 0f, 0f, 1f, 0f, 0f);
                                }
                                contentslabel_haspdfs = true;
                            }
                            catch (Exception)
                            {
                                if (error == "")
                                {
                                    error = "Error printing the order(s): " + p.order;
                                }
                                else
                                {
                                    error += ", " + p.order;
                                }
                            }
                        }
                    }
                }
                if (writer3.PageNumber == 1)
                {
                    Paragraph p = new Paragraph(" ");
                    ContentLabel.Add(p);
                }
                ContentLabel.Close();
                writer3.Close();

                foreach (PdfReader r in close)
                {
                    r.Close();
                }

                if (!contentslabel_haspdfs)
                {
                    File.Delete(Path.Combine(Template.AutoprintPackSlipFolder, System.Environment.MachineName.ToString() + "ContentLabel.pdf"));
                }
                else
                {
                }
            }
            catch (Exception e5)
            {

            }
            return error;
        }

        private string iTextSharpPackslip(string error)
        {
            bool PackSlip_haspdfs = false;
            try
            {
                List<PdfReader> close = new List<PdfReader>();
                string pdfFilePath_size = print_list[0].packslip;
                PdfReader reader_size = new PdfReader(pdfFilePath_size);
                close.Add(reader_size);
                iTextSharp.text.Rectangle page_size = reader_size.GetPageSize(1);
                iTextSharp.text.Document PackSlip = new iTextSharp.text.Document(page_size, 0f, 0f, 0f, 0f);
                PdfWriter writer3 = PdfWriter.GetInstance(PackSlip, new FileStream(Path.Combine(Template.AutoprintPackSlipFolder, System.Environment.MachineName.ToString() + "PackSlip.pdf"), FileMode.Create));

                PackSlip.Open();
                PdfContentByte cb3 = writer3.DirectContent;

                foreach (print_class p in print_list)
                {
                    if (p.packslip != "" && p.packslip != "N/A")
                    {
                        try
                        {
                            string pdfFilePath = p.packslip;
                            PdfReader reader3 = new PdfReader(pdfFilePath);
                            close.Add(reader3);
                            for (int i = 1; i <= reader3.NumberOfPages; i++)
                            {
                                PackSlip.NewPage();
                                cb3.AddTemplate(writer3.GetImportedPage(reader3, i), 1f, 0f, 0f, 1f, 0f, 0f);
                            }
                            PackSlip_haspdfs = true;
                        }
                        catch (Exception)
                        {
                            if (error == "")
                            {
                                error = "Error printing the order(s): " + p.order;
                            }
                            else
                            {
                                error += ", " + p.order;
                            }
                        }
                    }
                }
                if (writer3.PageNumber == 1)
                {
                    Paragraph p = new Paragraph(" ");
                    PackSlip.Add(p);
                }
                PackSlip.Close();
                writer3.Close();

                foreach (PdfReader r in close)
                {
                    r.Close();
                }

                if (!PackSlip_haspdfs)
                {
                    File.Delete(Path.Combine(Template.AutoprintPackSlipFolder, System.Environment.MachineName.ToString() + "PackSlip.pdf"));
                }
                else
                {
                }
            }
            catch (Exception e4)
            {

            }
            return error;
        }

        private string iTextSharpReturnLabel(string error)
        {
            bool Return_Label_haspdfs = false;
            try
            {
                List<PdfReader> close = new List<PdfReader>();
                string pdfFilePath_size = Utils.download(print_list[0].return_label, Template.AutoprintReturnLabelFolder);
                PdfReader reader_size = new PdfReader(pdfFilePath_size);
                close.Add(reader_size);
                iTextSharp.text.Rectangle page_size = reader_size.GetPageSize(1);
                iTextSharp.text.Document Return_Label = new iTextSharp.text.Document(page_size, 0f, 0f, 0f, 0f);
                PdfWriter writer3 = PdfWriter.GetInstance(Return_Label, new FileStream(Path.Combine(Template.AutoprintReturnLabelFolder, System.Environment.MachineName.ToString() + "Return_Label.pdf"), FileMode.Create));

                Return_Label.Open();
                PdfContentByte cb3 = writer3.DirectContent;

                foreach (print_class p in print_list)
                {
                    if (p.return_label != "" && p.return_label != "N/A")
                    {
                        try
                        {
                            string pdfFilePath = Utils.download(p.bol, Template.AutoprintReturnLabelFolder);
                            PdfReader reader3 = new PdfReader(pdfFilePath);
                            close.Add(reader3);
                            for (int i = 1; i <= reader3.NumberOfPages; i++)
                            {
                                Return_Label.NewPage();
                                cb3.AddTemplate(writer3.GetImportedPage(reader3, i), 1f, 0f, 0f, 1f, 0f, 0f);
                            }
                            Return_Label_haspdfs = true;
                        }
                        catch (Exception)
                        {
                            if (error == "")
                            {
                                error = "Error printing the order(s): " + p.order;
                            }
                            else
                            {
                                error += ", " + p.order;
                            }
                        }
                    }
                }
                if (writer3.PageNumber == 1)
                {
                    Paragraph p = new Paragraph(" ");
                    Return_Label.Add(p);
                }
                Return_Label.Close();
                writer3.Close();

                foreach (PdfReader r in close)
                {
                    r.Close();
                }

                if (!Return_Label_haspdfs)
                {
                    File.Delete(Path.Combine(Template.AutoprintReturnLabelFolder, System.Environment.MachineName.ToString() + "Return_Label.pdf"));
                }
                else
                {
                }
            }
            catch (Exception e3)
            {

            }
            return error;
        }

        private string iTextSharpBOL(string error)
        {
            bool bol_haspdfs = false;
            try
            {
                List<PdfReader> close = new List<PdfReader>();
                string pdfFilePath_size = Utils.download(print_list[0].bol, Template.AutoprintBOLFolder);
                PdfReader reader_size = new PdfReader(pdfFilePath_size);
                close.Add(reader_size);
                iTextSharp.text.Rectangle page_size = reader_size.GetPageSize(1);
                iTextSharp.text.Document BOL = new iTextSharp.text.Document(page_size, 0f, 0f, 0f, 0f);
                PdfWriter writer2 = PdfWriter.GetInstance(BOL, new FileStream(Path.Combine(Template.AutoprintBOLFolder, System.Environment.MachineName.ToString() + "BOL.pdf"), FileMode.Create));

                BOL.Open();
                PdfContentByte cb2 = writer2.DirectContent;

                foreach (print_class p in print_list)
                {
                    if (p.bol != "" && p.bol != "N/A")
                    {
                        try
                        {
                            string pdfFilePath = Utils.download(p.bol, Template.AutoprintBOLFolder);
                            PdfReader reader2 = new PdfReader(pdfFilePath);
                            close.Add(reader2);
                            for (int i = 1; i <= reader2.NumberOfPages; i++)
                            {
                                BOL.NewPage();
                                cb2.AddTemplate(writer2.GetImportedPage(reader2, i), 1f, 0f, 0f, 1f, 0f, 0f);
                            }
                            bol_haspdfs = true;
                        }
                        catch (Exception)
                        {
                            if (error == "")
                            {
                                error = "Error printing the order(s): " + p.order;
                            }
                            else
                            {
                                error += ", " + p.order;
                            }
                        }
                    }
                }
                if (writer2.PageNumber == 1)
                {
                    Paragraph p = new Paragraph(" ");
                    BOL.Add(p);
                }
                BOL.Close();
                writer2.Close();

                foreach (PdfReader r in close)
                {
                    r.Close();
                }

                if (!bol_haspdfs)
                {
                    File.Delete(Path.Combine(Template.AutoprintBOLFolder, System.Environment.MachineName.ToString() + "BOL.pdf"));
                }
                else
                {
                }
            }
            catch (Exception e2)
            {

            }
            return error;
        }

        private string iTextSharpCI(string error)
        {
            bool ci_haspdfs = false;
            try
            {
                List<PdfReader> close = new List<PdfReader>();
                string pdfFilePath_size = Utils.download(print_list[0].commercial_invoice, Template.AutoprintCIFolder);
                PdfReader reader_size = new PdfReader(pdfFilePath_size);
                close.Add(reader_size);
                iTextSharp.text.Rectangle page_size = reader_size.GetPageSize(1);
                iTextSharp.text.Document Commercial_Invoice = new iTextSharp.text.Document(page_size, 0f, 0f, 0f, 0f);
                PdfWriter writer1 = PdfWriter.GetInstance(Commercial_Invoice, new FileStream(Path.Combine(Template.AutoprintCIFolder, System.Environment.MachineName.ToString() + "Commercial_Invoice.pdf"), FileMode.Create));

                Commercial_Invoice.Open();
                PdfContentByte cb1 = writer1.DirectContent;

                foreach (print_class p in print_list)
                {
                    if (p.commercial_invoice != "" && p.commercial_invoice != "N/A")
                    {
                        try
                        {
                            string pdfFilePath = Utils.download(p.commercial_invoice, Template.AutoprintCIFolder);
                            PdfReader reader1 = new PdfReader(pdfFilePath);
                            close.Add(reader1);
                            for (int i = 1; i <= reader1.NumberOfPages; i++)
                            {
                                Commercial_Invoice.NewPage();
                                cb1.AddTemplate(writer1.GetImportedPage(reader1, i), 1f, 0f, 0f, 1f, 0f, 0f);
                            }
                            ci_haspdfs = true;
                        }
                        catch (Exception)
                        {
                            if (error == "")
                            {
                                error = "Error printing the order(s): " + p.order;
                            }
                            else
                            {
                                error += ", " + p.order;
                            }
                        }
                    }
                }
                if (writer1.PageNumber == 1)
                {
                    Paragraph p = new Paragraph(" ");
                    Commercial_Invoice.Add(p);
                }
                Commercial_Invoice.Close();
                writer1.Close();

                foreach (PdfReader r in close)
                {
                    r.Close();
                }

                if (!ci_haspdfs)
                {
                    File.Delete(Path.Combine(Template.AutoprintCIFolder, System.Environment.MachineName.ToString() + "Commercial_Invoice.pdf"));
                }
                else
                {
                }
            }
            catch (Exception e1)
            {

            }
            return error;
        }

        private string iTextSharpLabelsDoc(string error)
        {
            try
            {
                List<PdfReader> close = new List<PdfReader>();
                bool labels_haspdfs = false;
                //get label for the first task in list -> print_list[0]
                string pdfFilePath_size = Utils.download(print_list[0].label, Template.AutoprintLabelsFolder);
                bool first_is_valid = false;
                //create a pdfReader for the downloaded file
                int count = 0;
                iTextSharp.text.pdf.PdfReader reader_size = null;
                while (!first_is_valid && count < print_list.Count - 1)
                {
                    try
                    {
                        reader_size = new PdfReader(pdfFilePath_size);
                        first_is_valid = true;
                    }
                    catch
                    {
                        first_is_valid = false;
                    }
                    if (!first_is_valid)
                    {
                        pdfFilePath_size = Utils.download(print_list[count++].label, Template.AutoprintLabelsFolder);
                    }
                }

                //add the file to a list
                close.Add(reader_size);
                //get file size(size of the first page from the file)
                iTextSharp.text.Rectangle page_size = reader_size.GetPageSize(1);

                //create a new document with page_size, the size of the first label of the first shipment in list
                iTextSharp.text.Document labels = new iTextSharp.text.Document(page_size, 0f, 0f, 0f, 0f);

                if (File.Exists(Path.Combine(Template.AutoprintLabelsFolder, System.Environment.MachineName.ToString() + "Labels.pdf")))
                {
                    //File.Copy(Path.Combine(Template.AutoprintLabelsFolder, System.Environment.MachineName.ToString() + "Labels.pdf"), Template.AutoprintLabelsFolder +@"\test\" + "LabelsTest.pdf", true);

                    File.Delete(Path.Combine(Template.AutoprintLabelsFolder, System.Environment.MachineName.ToString() + "Labels.pdf"));
                }
                //write the labels document to disk, on the application's Labels folder, file name being: MachineNameLabels.pdf
                PdfWriter writer = PdfWriter.GetInstance(labels, new FileStream(Path.Combine(Template.AutoprintLabelsFolder, System.Environment.MachineName.ToString() + "Labels.pdf"), FileMode.Create));
                //open the document
                labels.Open();
                //get bytes
                PdfContentByte cb = writer.DirectContent;
                //take the labels from each print task and add them to the labels document and MachineNameLabels.pdf file
                foreach (print_class p in print_list)
                {
                    if (p.label != "" && p.label != "N/A")
                    {
                        try
                        {
                            string pdfFilePath = Utils.download(p.label, Template.AutoprintLabelsFolder);
                            if (pdfFilePath.Length <= 0)
                            {
                                continue;
                            }
                            PdfReader reader = null;
                            bool isValidPdf = true;
                            //check if the Pdf file with the labes is corrupted, and if so, then skip it
                            try
                            {
                                reader = new PdfReader(pdfFilePath);
                            }
                            catch
                            {
                                isValidPdf = false;
                            }
                            if (isValidPdf)
                            {
                                close.Add(reader);
                                for (int i = 1; i <= reader.NumberOfPages; i++)
                                {
                                    labels.NewPage();
                                    cb.AddTemplate(writer.GetImportedPage(reader, i), 1f, 0f, 0f, 1f, 0f, 0f);
                                }
                                labels_haspdfs = true;
                            }

                        }
                        catch (Exception)
                        {
                            if (error == "")
                            {
                                error = "Error printing the order(s): " + p.order;
                            }
                            else
                            {
                                error += ", " + p.order;
                            }
                        }
                    }
                }
                if (writer.PageNumber == 1)
                {
                    Paragraph p = new Paragraph(" ");
                    labels.Add(p);
                }
                labels.Close();
                writer.Close();

                foreach (PdfReader r in close)
                {
                    r.Close();
                }

                if (!labels_haspdfs)
                {
                    File.Delete(Path.Combine(Template.AutoprintLabelsFolder, System.Environment.MachineName.ToString() + "Labels.pdf"));
                }
            }
            catch (Exception ee)
            {

            }
            return error;
        }

        private void bg_print_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string error = e.Result as string;
            if (error != "")
            {
                MessageBox.Show(error);
            }

            //if (File.Exists(Path.Combine(Template.AutoprintLabelsFolder, System.Environment.MachineName.ToString() + "Labels.pdf")))
            //{
            //    //Utils.printPDF(System.Environment.MachineName.ToString() + "Labels.pdf", Template.AutoprintLabelsFolder, Template.AutoprintLabelsPrinter);
            //    File.Delete(Path.Combine(Template.AutoprintLabelsFolder, System.Environment.MachineName.ToString() + "Labels.pdf"));
            //}

            //if (File.Exists(Path.Combine(Template.AutoprintCIFolder, System.Environment.MachineName.ToString() + "Commercial_Invoice.pdf")))
            //{
            //    //Utils.printPDF(System.Environment.MachineName.ToString() + "Commercial_Invoice.pdf", Template.AutoprintCIFolder, Template.AutoprintCIPrinter);
            //    File.Delete(Path.Combine(Template.AutoprintCIFolder, System.Environment.MachineName.ToString() + "Commercial_Invoice.pdf"));
            //}

            //if (File.Exists(Path.Combine(Template.AutoprintBOLFolder, System.Environment.MachineName.ToString() + "BOL.pdf")))
            //{
            //    //Utils.printPDF(System.Environment.MachineName.ToString() + "BOL.pdf", Template.AutoprintBOLFolder, Template.AutoprintBOLPrinter);
            //    File.Delete(Path.Combine(Template.AutoprintBOLFolder, System.Environment.MachineName.ToString() + "BOL.pdf"));
            //}

            //if (File.Exists(Path.Combine(Template.AutoprintReturnLabelFolder, System.Environment.MachineName.ToString() + "Return_Label.pdf")))
            //{
            //    //Utils.printPDF(System.Environment.MachineName.ToString() + "Return_Label.pdf", Template.AutoprintReturnLabelFolder, Template.AutoprintReturnLabelPrinter);
            //    File.Delete(Path.Combine(Template.AutoprintReturnLabelFolder, System.Environment.MachineName.ToString() + "Return_Label.pdf"));
            //}

            //if (File.Exists(Path.Combine(Template.AutoprintPackSlipFolder, System.Environment.MachineName.ToString() + "PackSlip.pdf")))
            //{
            //    //Utils.printPDF(System.Environment.MachineName.ToString() + "PackSlip.pdf", Template.AutoprintPackSlipFolder, Template.AutoprintPackSlipPrinter);
            //    File.Delete(Path.Combine(Template.AutoprintPackSlipFolder, System.Environment.MachineName.ToString() + "PackSlip.pdf"));
            //}

            //if (File.Exists(Path.Combine(Template.AutoprintContentsLabelFolder, System.Environment.MachineName.ToString() + "ContentLabel.pdf")))
            //{
            //    //Utils.printPDF(System.Environment.MachineName.ToString() + "ContentLabel.pdf", Template.AutoprintContentsLabelFolder, Template.AutoprintContentsLabelPrinter);
            //    File.Delete(Path.Combine(Template.AutoprintContentsLabelFolder, System.Environment.MachineName.ToString() + "ContentLabel.pdf"));
            //}
            if (print_list.Count > 0)
            {
                bg_print.RunWorkerAsync();
            }
        }

        private void TaskBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                System.IO.File.AppendAllText("Error.txt", " Back task completed runs: " + DateTime.Now + "\r\n");
         
                if ((!rbPackageLevel.Checked) && (backupData.OrderPackagesTable.Rows.Count > 1))
                {
                    CopyShipmentLevelResultsToPackages();
                }
                //System.IO.File.AppendAllText("Error.txt", "Check and remove from backup deleted shipments: " + DateTime.Now + "\r\n");
         
                //foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                //{
                //    if (shipment["Ship Date"].ToString() == "" || Convert.ToDateTime(shipment["Ship Date"].ToString()).ToShortDateString() == DateTime.Now.ToShortDateString())
                //    {
                //        BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", shipment["Order #"].ToString(), data);
                //    }
                //}
                //System.IO.File.AppendAllText("Error.txt", " Cleaned from backup, complete shipments with s., rec.: " + DateTime.Now + "\r\n");
         
                //data.Complete_shipments();
                //System.IO.File.AppendAllText("Error.txt", " Add completed shipments to backup: " + DateTime.Now + "\r\n");
         
                //foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                //{
                //    if (!shipment.Status.Equals("Error") && Convert.ToDateTime(shipment["Ship Date"].ToString()).ToShortDateString() == DateTime.Now.ToShortDateString())
                //    {
                //        BackupFile.AddShipment(data.ShipmentsTable.TableName, shipment as System.Data.DataRow, data);
                //    }
                //}
                //System.IO.File.AppendAllText("Error.txt", " Shipments backed-up: " + DateTime.Now + "\r\n");
         
                try
                {
                    if (Template.UseAutoprint && (e.Result as InteGr8Task[]).Length > 0 && (((e.Result as InteGr8Task[])[0].type == InteGr8TaskType.Ship) || ((e.Result as InteGr8Task[])[0].type == InteGr8TaskType.GetBackEditResults)))
                    {
                        //System.IO.File.AppendAllText("Error.txt", " Creating print tasks list: " + DateTime.Now + "\r\n");
         
                        InteGr8Task[] printtasks = e.Result as InteGr8Task[];
                        for (int i = 0; i < printtasks.Length; i++)
                        {
                            Data.ShipmentsTableRow row_test = data.ShipmentsTable.FindBy_Order__Package_Index(printtasks[i].order_number, 0);
                           if (row_test != null)
                           { 
                            if (!row_test.Status.ToString().Trim().Equals("Error"))
                            {
                                print_class print = new print_class();
                                print.order = printtasks[i].order_number.ToString();
                                foreach (DataRow r in data.ShipmentsTable.Rows)
                                {
                                    if (r["Order #"].ToString().Equals(printtasks[i].order_number.ToString()))
                                    {
                                        if (Template.AutoprintLabelsFolder != null && Template.AutoprintLabelsFolder != "")
                                        {
                                            //System.IO.File.AppendAllText("Error.txt", " Adding labels: " + DateTime.Now + "\r\n");
         
                                            print.label = r["Label URL"].ToString();
                                        }
                                        if (Template.AutoprintCIFolder != null && Template.AutoprintCIFolder != "")
                                        {
                                            print.commercial_invoice = r["Commercial Invoice URL"].ToString();
                                        }
                                        if (Template.AutoprintBOLFolder != null && Template.AutoprintBOLFolder != "")
                                        {
                                            print.bol = r["BOL URL"].ToString();
                                        }
                                        if (Template.AutoprintReturnLabelFolder != null && Template.AutoprintReturnLabelFolder != "")
                                        {
                                            print.return_label = r["Return Label URL"].ToString();
                                        }
                                        if (Template.AutoprintPackSlipFolder != null && Template.AutoprintPackSlipFolder != "")
                                        {
                                            Data.ShipmentsTableRow row = data.ShipmentsTable.FindBy_Order__Package_Index(r["Order #"].ToString(), Convert.ToInt32(r["Package_Index"].ToString()));
                                            print.packslip = PackSlip(row);
                                        }
                                        if (Template.AutoprintContentsLabelFolder != null && Template.AutoprintContentsLabelFolder != "")
                                        {
                                            print.comntentslabel = Contents_Label(r["Order #"].ToString());
                                        }
                                        break;
                                    }
                                }

                                //System.IO.File.AppendAllText("Error.txt", " Print list is created: " + DateTime.Now + "\r\n");
         
                                print_list.Add(print);
                            }
                        }
                        }
                        //if (!bg_print.IsBusy)
                        //{
                        //    bg_print.RunWorkerAsync();
                        //}

                        Thread.Sleep(100);

                    }
                }

                catch (Exception ex)
                {
                    Utils.SendError("Error at adding labels to printing list", ex);
                }
                finally
                {
                    //System.IO.File.AppendAllText("Error.txt", " Add completed shipments to backup: " + DateTime.Now + "\r\n");

                    //foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                    //{
                    //    if (!shipment.Status.Equals("Error") && Convert.ToDateTime(shipment["Ship Date"].ToString()).ToShortDateString() == DateTime.Now.ToShortDateString())
                    //    {
                    //        BackupFile.AddShipment(data.ShipmentsTable.TableName, shipment as System.Data.DataRow, data);
                    //    }
                    //}
                    //System.IO.File.AppendAllText("Error.txt", " Shipments backed-up: " + DateTime.Now + "\r\n");
         
                }
                if (Template.AutoSave_AtShipping && (e.Result as InteGr8Task[]).Length > 0 && (((e.Result as InteGr8Task[])[0].type == InteGr8TaskType.Ship) || ((e.Result as InteGr8Task[])[0].type == InteGr8TaskType.GetBackEditResults)))
                {
                    if (progress.Created)
                    {
                        progress.Close_Me();
                    }
                    string[] arguments = { "Save" };
                    bgworker_save.RunWorkerAsync(arguments);
                    progress = new frmProcessing(TaskBackgroundWorker);
                    progress.ShowProgress2("Saving Shipments");
                    progress.ShowDialog(this);
                    HistoryBindingSource.ResetBindings(true);
                 }
                    if (data.ShipmentsTable.Columns.IndexOf("OrderNumber") >= 0)
                    {
                        if (HistoryGrid.Columns["OrderNumber"] != null)
                        {
                            HistoryGrid.Columns["OrderNumber"].Visible = true;
                        }
                    }
                    //if autosave, then show shipment info in History tab, to identify the saved shipment and/or reprint the label, etc
                    foreach (DataGridViewColumn col in HistoryGrid.Columns)
                    {
                        if (!col.Name.Equals("Token") && !col.Name.Equals("OrderNumber") && !col.Name.Contains("Sender") && !col.Name.Contains("Bill"))
                        {
                            col.Visible = true;
                        }
                    }

                    OrdersTableBindingSource.ResetBindings(false);
                    fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(false);
                    fKOrdersTableOrderProductsTableBindingSource.ResetBindings(false);
                    fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(false);
                    fkOrdersTableLTLItemsBindingSource.ResetBindings(false);
                    shipmentsTableBindingSource.ResetBindings(false);
                    InteGr8_Main.data.AcceptChanges();

                    SetRowNumbers();
                    if ((e.Result as InteGr8Task[]).Length > 0 && (e.Result as InteGr8Task[])[0].type == InteGr8TaskType.Ship)
                    {

                        if (cbMultiOrder.Checked  && data.OrdersTable.Select("[Status] <> 'Shipped'").Length <= 1)
                        {
                            cbMultiOrder.Checked = false;
                        }
                    }
                if (!e.Cancelled && e.Error != null)
                {
                    // error from the working thread
                    Utils.SendError("Error processing task", e.Error);
                    MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //start printing after all documents have been added to the printing list
                try
                {
                    if (Template.UseAutoprint && (e.Result as InteGr8Task[]).Length > 0 && (((e.Result as InteGr8Task[])[0].type == InteGr8TaskType.Ship) || ((e.Result as InteGr8Task[])[0].type == InteGr8TaskType.GetBackEditResults)))
                    {

                        if (!bg_print.IsBusy)
                        {
                            //System.IO.File.AppendAllText("Error.txt", " Start the printing thread: " + DateTime.Now + "\r\n");
         
                            bg_print.RunWorkerAsync();
                        }
                        else
                        {
                            //System.IO.File.AppendAllText("Error.txt", " Print thread is busy, wait 100 ms: " + DateTime.Now + "\r\n");
         
                            Thread.Sleep(100);
                            if (!bg_print.IsBusy)
                            {
                                bg_print.RunWorkerAsync();
                            }
                        }
                    }
                }
                catch (Exception printEx)
                {
                    Utils.SendError("Error at printing labels", printEx);
                }

                if ((e.Result as InteGr8Task[]).Length > 0 && (e.Result as InteGr8Task[])[0].type == InteGr8TaskType.Rate2)
                {
                    System.IO.File.AppendAllText("Error.txt", " Rate task final processing - current rate, etc: " + DateTime.Now + "\r\n");
                     string order = (OrdersTableBindingSource.Current as DataRowView).Row["Order #"].ToString();
                     if ( String.IsNullOrEmpty(data.RepliesTable.FindBy_Order__FieldIndex(order, 322, 0).Value)) // 322 = Error message field
                     {
                         if (Template.Name.Equals("Choquette"))
                         {
                             if (data.RepliesTable.FindBy_Order__FieldIndex(order, 175, 0) != null)
                             {
                                 decimal valueRate = Convert.ToDecimal(data.RepliesTable.FindBy_Order__FieldIndex(order, 175, 0).Value);
                                 
                                 tbCurrentRate.Text = valueRate.ToString("N2");

                             }
                             else
                             {
                                 tbCurrentRate.Text = "N/A";
                             }
                         }
                         else
                         {
                             if (data.RepliesTable.FindBy_Order__FieldIndex(order, 183, 0) != null)
                             {
                                 decimal valueRate = Convert.ToDecimal(data.RepliesTable.FindBy_Order__FieldIndex(order, 183, 0).Value);
                                 tbCurrentRate.Text = valueRate.ToString("N2");
                             }
                             else
                             {
                                 tbCurrentRate.Text = "N/A";
                             }
                         }

                         //write the dimensional weight also in a textbox
                         System.IO.File.AppendAllText("Error.txt", " Rate task final writing dims. weight, etc: " + DateTime.Now + "\r\n");
                  
                         if (data.RepliesTable.FindBy_Order__FieldIndex(order, 365, 0) != null)
                         {
                             decimal dimsWeight = Convert.ToDecimal(data.RepliesTable.FindBy_Order__FieldIndex(order, 365, 0).Value);
                             if (data.RepliesTable.FindBy_Order__FieldIndex(order, 355, 0) != null)
                             {
                                 decimal billedWeight = Convert.ToDecimal(data.RepliesTable.FindBy_Order__FieldIndex(order, 355, 0).Value);
                                 if (billedWeight > dimsWeight)
                                 {
                                     dimsWeight = billedWeight;
                                 }
                             }
                             tbDimensionalWeight.Text = dimsWeight.ToString("N2");
                         }
                         else
                         {
                             if (data.RepliesTable.FindBy_Order__FieldIndex(order, 355, 0) != null)
                             {
                                 decimal packageWeight = 0;
                                 if (data.RepliesTable.FindBy_Order__FieldIndex(order, 77, 0) != null)
                                 {
                                     packageWeight = Convert.ToDecimal(data.RepliesTable.FindBy_Order__FieldIndex(order, 77, 0).Value);
                                 }
                                 decimal dimsWeight = Convert.ToDecimal(data.RepliesTable.FindBy_Order__FieldIndex(order, 355, 0).Value);
                                 if (packageWeight > dimsWeight)
                                 {
                                     dimsWeight = packageWeight;
                                 }
                                 tbDimensionalWeight.Text = dimsWeight.ToString("N2");
                             }
                             else
                             {
                                 tbDimensionalWeight.Text = "N/A";
                             }
                         }

                     }
                     else
                     {
                         MessageBox.Show(data.RepliesTable.FindBy_Order__FieldIndex(order, 322, 0).Value, "Error rating current order");
                     }
                }

                if ((e.Result as InteGr8Task[]).Length > 0 && (e.Result as InteGr8Task[])[0].type == InteGr8TaskType.Rate)
                {
                    System.IO.File.AppendAllText("Error.txt", " Rate task final processing , etc: " + DateTime.Now + "\r\n");
                  
                    StatusLabel.Text = "Idle.";
                    progress.Hide();
                    ChangeButtonsStatus(true);
                    string order = (OrdersTableBindingSource.Current as DataRowView).Row["Order #"].ToString();
                    frmRates dialog = new frmRates(order);
                    refresh_products_rows();
                   // System.Diagnostics.Debug.WriteLine("Time before opening the Rates window " + DateTime.Now);
                
                    if (dialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                    {
                        data.OrdersTable.FindBy_Order__(order).Carrier = dialog.selected_carrier;
                        data.OrdersTable.FindBy_Order__(order).Service = dialog.selected_service;
                        DataRowView ord = OrdersTableBindingSource.Current as DataRowView;
                        if ((bool)ord["Selected"] == true)
                        {
                            ord["Selected"] = false;
                            data.OrdersTable.FindBy_Order__(order).Carrier = dialog.selected_carrier;
                            data.OrdersTable.FindBy_Order__(order).Service = dialog.selected_service;
                            ord["Selected"] = true;
                        }
                        OrdersTableBindingSource.EndEdit();
                        return;
                    }
                    System.IO.File.AppendAllText("Error.txt", "Time after the rates window: " + DateTime.Now + "\r\n");
                    System.Diagnostics.Debug.WriteLine("Time after the rates window: " + DateTime.Now);
                
                }
                else
                {
                    shipmentsTableBindingSource.ResetBindings(true);
                    shipmentsTableBindingSource.Filter = "Package_Index = 0";
                    ShipmentsGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

                    // make all shipments columns readonly (except the first one with Selected)
                    foreach (DataGridViewColumn Column in ShipmentsGrid.Columns)
                    {
                        if (!Column.Name.Equals("ShipmentsSelectedColumn")) Column.ReadOnly = true;
                    }

                    foreach (Data.ShipmentsTableRow r in data.ShipmentsTable.Rows)
                    {
                        if (r.Status.ToString().Trim().Equals("Error"))
                        {
                            foreach (DataGridViewRow row in ShipmentsGrid.Rows)
                            {
                                DataRowView row_test = row.DataBoundItem as DataRowView;
                                if (row_test["Order #"].ToString() == r._Order__.ToString())
                                {
                                    row.DefaultCellStyle.BackColor = Color.Red;
                                    row.DefaultCellStyle.SelectionForeColor = Color.Red;
                                }
                            }
                            ShipmentsGrid.Refresh();
                            foreach (DataGridViewRow row in OrdersGrid.Rows)
                            {
                                DataRowView row_test = row.DataBoundItem as DataRowView;
                                if (row_test["Order #"].ToString() == r._Order__.ToString())
                                {
                                    row.DefaultCellStyle.BackColor = Color.Red;
                                    row.DefaultCellStyle.SelectionForeColor = Color.Red;
                                }
                            }
                            OrdersGrid.Refresh();
                            MessageBox.Show("Error at shipping order: " + r._Order__ + "\nError message: \n" + r.Error_Message);
                        }
                    }
                    Data.ShipmentsTableRow[] rows = data.ShipmentsTable.Select("Status = 'Error'") as Data.ShipmentsTableRow[];
                    foreach (Data.ShipmentsTableRow r in rows)
                    {
                        data.ShipmentsTable.Rows.Remove(r);
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.SendError("Error processing task results", ex);
                UseWaitCursor = false;
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                StatusLabel.Text = "Idle.";
                if (progress.Created)
                {
                    //progress.Hide();
                    progress.Close_Me();
                }
                ChangeButtonsStatus(true);

                
                if (MainPages.SelectedTab == OrdersPage)
                {
                    tbValue1.Focus();
                }
                refresh_products_rows();
            }
        }

        private void CopyShipmentLevelResultsToPackages()
        {
            decimal charge = 0;
            for (int i = 0; i < backupData.OrderPackagesTable.Rows.Count; i++)
            {
                Data.ShipmentsTableRow ship = (Data.ShipmentsTableRow)data.ShipmentsTable.Rows[0];
                Decimal.TryParse(data.ShipmentsTable.Rows[0]["Client Final Charge"].ToString(), out charge);
                ship["Client Final Charge"] = charge / backupData.OrderPackagesTable.Rows.Count;
                data.ShipmentsTable.Columns["Package #"].ReadOnly = false;
                ship["Package #"] = backupData.OrderPackagesTable.Rows[i]["Package #"];
                data.ShipmentsTable.Columns["Package #"].ReadOnly = true;
                data.ShipmentsTable.Rows.Add(ship);
            }
        }

        #endregion

        #region ignore_gridview_errors

        private void OrdersGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void OrderPackagesGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void ProductsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void OrderRequestGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void OrderSkidsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void LTLItemsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void ShipmentsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        #endregion

        #region Templates

        private void ReloadTemplates()
        {
            try
            {
                // remember the currently selected template
                string old_template = TemplatesDropdown.Text;

                data.Refresh();
                data.CheckTemplates();

                // manually load all template names into the dropdown (toolbar dropdown does not support databinding)
                TemplatesDropdown.Items.Clear();
                foreach (Data.TemplatesRow Template in data.Templates)
                    TemplatesDropdown.Items.Add(Template.Name);

                // try to select the template that was selected before the reload
                TemplatesDropdown.SelectedIndex = TemplatesDropdown.FindStringExact(old_template);
            }
            catch (Exception e)
            {
                Utils.SendError("Error in ReloadTemplates.", e);
            }
        }

        public bool ShowWizard(int step)
        {
            Form form = null;
            switch (step)
            {
                case 0: return false;
                case 1: form = new frmWizard_Connection(); break;
                case 2: form = new frmWizard_Options(); break;
                case 3: form = new frmWizard_Tables(TableType.OrdersType, step); break;
                case 4: form = new frmWizard_Tables(TableType.SendersType, step); break;
                case 5: form = new frmWizard_Tables(TableType.RecipientsType, step); break;
                case 6: form = new frmWizard_Tables(TableType.PackagesType, step); break;
                case 7: form = new frmWizard_Tables(TableType.ProductsType, step); break;
                case 8: form = new frmWizard_Tables(TableType.ShipmentsType, step); break;
                case 9: form = new frmWizard_Fields(); break;
                case 10: form = new frmWizard_Filter(); break;
                case 11: form = new frmWizard_Finish(); break;
                case 12:
                    {
                        //OrdersParametersGrid must refresh the data source
                        RefreshOrdersDataSource();
                        return true;
                    }

            }
            if (form == null)
            {
                return false;
            }
            DialogResult result = form.ShowDialog(this);
            if (result == DialogResult.Abort)
            {
                return false;
            }
            else
            {
                if (result == DialogResult.Cancel)
                {
                    return ShowWizard(--step);
                }
            }
            return ShowWizard(++step);
        }

        private void RefreshOrdersDataSource()
        {
            filterExpressionBindingSource.DataSource = null;
            filterExpressionBindingSource.DataSource = Select_Filter.Expressions;
            filterLeftOperandNameBindingSource.DataSource = null;
            filterLeftOperandNameBindingSource.DataSource = Select_Filter.Expressions;
        }

        private void TemplatesNew_Click(object sender, EventArgs e)
        {
            Template.Load(null);
            ShowWizard(1);
            TemplatesDropdown_SelectedIndexChanged(sender, e);
        }

        private void TemplatesEdit_Click(object sender, EventArgs e)
        {
            if (TemplatesDropdown.Text.Equals(""))
            {
                return;
            }
            ShowWizard(1);
            TemplatesDropdown_SelectedIndexChanged(sender, e);
        }

        private void TemplatesDelete_Click(object sender, EventArgs e)
        {
            if (TemplatesDropdown.Text.Equals(""))
            {
                return;
            }
            data.Templates.FindByName(TemplatesDropdown.Text).Delete();
            ReloadTemplates();
        }

        private void TemplatesImport_Click(object sender, EventArgs e)
        {
            ReloadTemplates();
        }

        private void TemplatesExport_Click(object sender, EventArgs e)
        {
            if (TemplatesDropdown.Text.Equals(""))
            {
                return;
            }
        }

        #endregion

        private void MainPages_Selected(object sender, TabControlEventArgs e)
        {
            EndEdit();
            if (MainPages.SelectedTab == TwoShipPage)
            {
                if (!edit_mod)
                {
                    //MainPages.SelectedIndex = 0;
                    not_manual = false;
                    //return;
                }
            }
            else
            {
                if (!not_manual)
                {
                    manual_order = false;
                    edit_mod = false;
                }
                not_manual = false;
            }
            if (MainPages.SelectedTab != TwoShipPage  || chromeBrowser == null)//|| Browser.Url != null)
            //if (MainPages.SelectedTab != TwoShipPage && Browser.Url != null)
            {
                return;
            }

            try
            {
                string orderKey = "";
                if (InteGr8_Main.key == null)
                {
                    DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
                    if (order_row == null)
                    {
                        return;
                    }
                    string order_number = order_row.Row["Order #"].ToString();
                    if (order_number == null || order_number.Equals(""))
                    {
                        return;
                    }
                    Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_number);
                    if (order != null && data.OrdersTable.Columns.IndexOf("Token") >= 0)
                    {
                        if (!String.IsNullOrEmpty(order["Token"].ToString()))
                        {
                            orderKey = order["Token"].ToString();
                        }
                    }
                }
                else
                {
                    orderKey = InteGr8_Main.key.MyWSKey;
                }
                //Browser.ScriptErrorsSuppressed = true;
                //Browser.Navigate(Template.Ship_Link + "/io2ship.aspx?ws_key=" + orderKey + "&id=0"); //InteGr8_Main.key.MyWSKey
                InitializeChromium(Template.Ship_Link + "/io2ship.aspx?ws_key=" + orderKey + "&id=0");
                //this.TwoShipPage.Controls.Remove(chromeBrowser);
                //chromeBrowser = new ChromiumWebBrowser(Template.Ship_Link + "/io2ship.aspx?ws_key=" + orderKey + "&id=0");
                //this.TwoShipPage.Controls.Add(chromeBrowser);
                //chromeBrowser.Dock = DockStyle.Fill;
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in MainPages_Selected", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BOL_URLPrint()
        {
            try
            {
                //* get the order # from the shipmentsHeader it must have Package_index too!!
                DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
                if (order == null)
                {
                    MessageBox.Show("There is no order selected");
                    return;
                }
                if (data.ShipmentsTable.Columns.IndexOf("BOL URL") < 0)
                {
                    MessageBox.Show("There is no Bill of lading for this shipment");
                    return;
                }
                Data.ShipmentsTableRow shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), Convert.ToInt32(order.Row["Package_Index"]));

                if (shipment == null || shipment["BOL URL"].ToString().Equals("") || shipment["BOL URL"].ToString().Equals("N/A") || shipment["BOL URL"] == null)
                {
                    MessageBox.Show("There is no url for the Bill of lading");
                    return;
                }
                Utils.openPDF(shipment["BOL URL"].ToString(), Template.UseAutoprint ? (Template.AutoprintBOLFolder != "" ? Template.AutoprintBOLFolder : Application.LocalUserAppDataPath) : Application.LocalUserAppDataPath);
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in ShipmentsBOL_URLButton_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ShipmentsBOLPrint_Click(object sender, EventArgs e)
        {
            DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
            if (order == null)
            {
                MessageBox.Show("There is no order selected");
                return;
            }
            if (Template.Name.Equals("MAAX"))
            {
                List<print_class> temp = new List<print_class>();
                bool ok = false;
                foreach (Data.ShipmentsTableRow rr in data.ShipmentsTable)
                {
                    if (rr.Selected == true)
                    {
                        ok = true;
                        break;
                    }
                }

                if (ok)
                {
                    if (!bg_print.IsBusy)
                    {
                        foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                        {
                            if (shipment.Selected == true && shipment.Package_Index.ToString().Equals("0"))
                            {
                                print_class print = new print_class();
                                print.order = shipment._Order__;
                                //new code
                                //Data.ShipmentsTableRow ship = data.ShipmentsTable.FindBy_Order__Package_Index(shipment._Order__, 0);
                                print.label = "";
                                print.commercial_invoice = "";
                                print.bol = shipment["BOL URL"].ToString();
                                print.return_label = "";
                                //old code - this one adds the labels to the printing list for each package in shipment, so for a 9 pks shipment would add 10 lines * 9 labels = 90 labels(10 lines - 0 shipment level and the 9 levels for packages)

                                //foreach (DataRow r in data.ShipmentsTable.Rows)
                                //{
                                //    if (r["Order #"].ToString().Equals(print.order))
                                //    {
                                //        print.label = "";
                                //        print.commercial_invoice = "";
                                //        print.bol = r["BOL URL"].ToString();
                                //        print.return_label = "";
                                //        break;
                                //    }
                                //}
                                if (print.bol != "" && print.bol != "N/A")
                                {
                                    temp.Add(print);
                                }
                            }
                        }

                        if (!bg_print.IsBusy && print_list.Count == 0)
                        {
                            print_list.AddRange(temp);
                            bg_print.RunWorkerAsync();
                        }
                    }
                    else
                    {
                        MessageBox.Show("InteGr8 is still printing, please try again later.");
                    }
                }
            }
            else
            {
                BOL_URLPrint();
            }
        }

        private void tabPackageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CurrentCarrierHasItems() && tabPackageType.SelectedTab.Equals(tabPageSkids))
            {
                tabPackageType.SelectedTab = tabPagePackages;
                MessageBox.Show("The selected carrier doesn't use skids, but items", "Information");
            }
            else
            {
                if (!CurrentCarrierHasItems() && tabPackageType.SelectedTab.Equals(tabPageItems))
                {
                    tabPackageType.SelectedTab = tabPagePackages;
                    MessageBox.Show("The selected carrier doesn't use items, but skids", "Information");
                }
            }
            if (OrdersTableBindingSource.Position < 0)
            {
                return;
            }
            DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            if (order == null)
            {
                return;
            }
            switch (tabPackageType.SelectedTab.Name)
            {
                case "tabPagePackages":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getPackagesWeight(order);
                        break;
                    }
                case "tabPageSkids":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getSkidsWeight(order);
                        break;
                    }
                case "tabPageItems":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getLTLItemsWeight(order);
                        break;
                    }
            }
        }

        private bool CurrentCarrierHasItems()
        {
            try
            {
                if (OrdersTableBindingSource.Position == -1)
                {
                    return false;
                }
                DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
                if (order_row == null)
                {
                    return false;
                }
                object order_number = order_row.Row["Order #"];
                if (order_number == null || order_number.Equals(""))
                {
                    return false;
                }
                if (!(data.OrdersTable.FindBy_Order__(order_number.ToString()).Carriers_MappingRow == null))
                {
                    Data.CarriersRow carrier = (Data.CarriersRow)data.Carriers.FindByCode(order_row.Row["Carrier"].ToString());
                    if (carrier.HasItems == true)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error at OrdersGrid_SelectionChanged", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        private void OrdersGrid_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (OrdersTableBindingSource.Position < 0)
                {
                    lblTotalWeight.Text = "Total Weight: 0";
                    return;
                }
                if (OrdersTableBindingSource.Current == null)
                {
                    lblTotalWeight.Text = "Total Weight: 0";
                    return;
                }
                DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;

                Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
                if (order == null)
                {
                    return;
                }
                tbCurrentRate.Text = "";
                tbDimensionalWeight.Text = "";
                switch (tabPackageType.SelectedTab.Name)
                {
                    case "tabPagePackages":
                        {
                            lblTotalWeight.Text = "Total Weight: " + getPackagesWeight(order);
                            break;
                        }
                    case "tabPageSkids":
                        {
                            lblTotalWeight.Text = "Total Weight: " + getSkidsWeight(order);
                            break;
                        }
                    case "tabPageItems":
                        {
                            lblTotalWeight.Text = "Total Weight: " + getLTLItemsWeight(order);
                            break;
                        }
                }
                //refresh the available carriers list based on Available carriers if they are defined
          if (!Properties.Settings.Default.AvailableCarriers.Equals("") && data.AvailableCarriers.Rows.Count > 0)
                {
                    if (data.OrdersTable.Columns.IndexOf("Client ID") >= 0)
                    {
                        CarriersBS.RaiseListChangedEvents = false;
                        CarriersBS.SuspendBinding();

                        ServicesBS.RaiseListChangedEvents = false;
                        ServicesBS.SuspendBinding();

                        data.Carriers.Clear();
                        data.Carriers.ReadXml(Settings.Default.CarriersFile);

                        data.Services.Clear();
                        data.Services.ReadXml(Settings.Default.ServicesFile);

                        CarriersBS.RaiseListChangedEvents = true;
                        CarriersBS.ResetBindings(true);

                        ServicesBS.RaiseListChangedEvents = true;
                        ServicesBS.ResetBindings(true);

                        string clientID = order["Client ID"].ToString();
                        string WSKey = order["Token"].ToString();

                        Data.ClientsRow clientLoc = data.Clients.FindByClientID_WSKey(clientID, WSKey);

                        string LocationID = clientLoc["Location"].ToString();
                        int i = 0;
                        while (i < data.Carriers.Rows.Count)
                        {
                            if (!isAvailable(data.Carriers.Rows[i]["Carrier_Code"].ToString(), LocationID))
                            {
                                data.Carriers.Rows.RemoveAt(i);
                            }
                            else
                            {
                                i++;
                            }
                        }
                        CarriersBS.ResumeBinding();
                        CarriersBS.EndEdit();
                        ServicesBS.ResumeBinding();
                        ServicesBS.EndEdit();

                       }
                }



                //if (OrdersGrid.SelectedRows.Count > 0 && Template.Name == "")
                //{
                //    refresh_products_rows();
                //}
            }
            catch (Exception ex)
            {
                Utils.SendError("Error updating weight on orders grid selection changed", ex);
            }
        }

        private bool isAvailable(string carrier_code, string clientID)
        {
            bool isAvailableCarrier = false;
            foreach (Data.AvailableCarriersRow isCarrier in data.AvailableCarriers)
            {
                if (isCarrier["ClientID"].ToString().Equals(clientID) && isCarrier["CarrierCode"].ToString().Equals(carrier_code))
                {
                    isAvailableCarrier = true;
                }
            }
            return isAvailableCarrier;
        }

        private void rbPackageLevel_CheckedChanged(object sender, EventArgs e)
        {
            Template.MPSPackageLevel = rbPackageLevel.Checked;
            if (rbPackageLevel.Checked == false)
            {
                if (data.OrderPackagesTable.Columns.IndexOf("Total Weight") < 0)
                {
                    data.OrderPackagesTable.Columns.Add("Total Weight");
                }
                if (data.OrderPackagesTable.Columns.IndexOf("Packages") < 0)
                {
                    data.OrderPackagesTable.Columns.Add("Packages");
                }
                calcTotalWeightAndPackagesNo();
                foreach (DataGridViewRow r in OrderPackagesGrid.Rows)
                {
                    if (r.Index > 0)
                    {
                        r.Visible = false;
                    }
                }
            }
            else
            {
                foreach (DataGridViewRow r in OrderPackagesGrid.Rows)
                {
                    r.Visible = true;
                }
            }
        }

        private void calcTotalWeightAndPackagesNo()
        {
            decimal totalWeight = 0;
            int packagesNo = 0;
            foreach (Data.OrdersTableRow order in data.OrdersTable)
            {
                Data.OrderPackagesTableRow[] packages = order.GetOrderPackagesTableRows();
                if (packages.Length > 0)
                {
                    data.getTotalWeight(order, out totalWeight, out packagesNo);
                }
                for (int i = 0; i < packages.Length; i++)
                {
                    packages[i]["Total Weight"] = totalWeight;
                    packages[i]["Packages"] = packagesNo;
                }
            }
        }

        private void ReturnLabelPrint()
        {
            try
            {
                //* get the order # from the shipmentsHeader it must have Package_index too!!
                DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
                if (order == null)
                {
                    MessageBox.Show("There is no order selected");
                    return;
                }
                if (data.ShipmentsTable.Columns.IndexOf("Return Label URL") < 0)
                {
                    MessageBox.Show("There is no Return label for this shipment");
                    return;
                }
                Data.ShipmentsTableRow shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), Convert.ToInt32(order.Row["Package_Index"]));

                if (shipment == null || shipment["Return Label URL"].ToString().Equals("") || shipment["Return Label URL"].ToString().Equals("N/A") || shipment["Return Label URL"] == null)
                {
                    MessageBox.Show("There is no url for the Return label");
                    return;
                }
                Utils.openPDF(shipment["Return Label URL"].ToString(), Template.UseAutoprint ? (Template.AutoprintReturnLabelFolder != "" ? Template.AutoprintReturnLabelFolder : Application.LocalUserAppDataPath) : Application.LocalUserAppDataPath);
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in ShipmentsReturnLabel_Button_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ShipmentsReturnLabel_Click(object sender, EventArgs e)
        {
            DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
            if (order == null)
            {
                MessageBox.Show("There is no order selected");
                return;
            }
            if (Template.Name.Equals("MAAX"))
            {
            bool ok = false;
            List<print_class> temp = new List<print_class>();
            foreach (Data.ShipmentsTableRow rr in data.ShipmentsTable)
            {
                if (rr.Selected == true)
                {
                    ok = true;
                    break;
                }
            }

            if (ok)
            {
                if (!bg_print.IsBusy)
                {
                    foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                    {
                        if (shipment.Selected == true && (!shipment["Return Label URL"].ToString().Equals("")) && (!shipment["Return Label URL"].ToString().Equals("N/A")))
                        {
                            print_class print = new print_class();
                            print.order = shipment._Order__;
                            //new code
                            //Data.ShipmentsTableRow ship = data.ShipmentsTable.FindBy_Order__Package_Index(shipment._Order__, 0);
                            print.label = "";
                            print.commercial_invoice = "";
                            print.bol = "";
                            print.return_label = shipment["Return Label URL"].ToString();
                            //old code - this one adds the labels to the printing list for each package in shipment, so for a 9 pks shipment would add 10 lines * 9 labels = 90 labels(10 lines - 0 shipment level and the 9 levels for packages)
                               
                            //foreach (DataRow r in data.ShipmentsTable.Rows)
                            //{
                            //    if (r["Order #"].ToString().Equals(print.order))
                            //    {
                            //        print.label = "";
                            //        print.commercial_invoice = "";
                            //        print.bol = "";
                            //        print.return_label = r["Return Label URL"].ToString();
                            //        break;
                            //    }
                            //}
                            if (print.return_label != "" && print.return_label != "N/A")
                            {
                                print_list.Add(print);
                            }
                        }
                    }
                    if (!bg_print.IsBusy && print_list.Count == 0)
                    {
                        print_list.AddRange(temp);
                        bg_print.RunWorkerAsync();
                    }
                }
                else
                {
                    MessageBox.Show("InteGr8 is still printing, please try again later.");
                }
            }
        }
            else
            {
                ReturnLabelPrint();
            }
        }

        private Double getPackagesWeight(Data.OrdersTableRow order)
        {
            Data.OrderPackagesTableRow[] packages = order.GetOrderPackagesTableRows();
            if (packages.Length == 0)
            {
                return 0;
            }
            System.Double weight = 0;
            for (int i = 0; i < packages.Length; i++)
            {
                if (!String.IsNullOrEmpty(packages[i]["Package Weight"].ToString()) && !packages[i]["Package Weight"].ToString().Equals(""))
                {
                    weight += System.Convert.ToDouble(packages[i]["Package Weight"]);
                }
            }
            return weight;
        }

        private decimal getSkidsWeight(Data.OrdersTableRow order)
        {
            Data.OrdersSkidsTableRow[] skids = order.GetOrdersSkidsTableRows();
            if (skids.Length == 0)
            {
                return 0;
            }
            System.Decimal weight = 0;
            for (int i = 0; i < skids.Length; i++)
            {
                if (!String.IsNullOrEmpty(skids[i]["Skid Weight"].ToString()) && !skids[i]["Skid Weight"].ToString().Equals(""))
                {
                    weight += System.Convert.ToDecimal(skids[i]["Skid Weight"]);
                }
            }
            return weight;
        }

        private decimal getLTLItemsWeight(Data.OrdersTableRow order)
        {
            Data.OrderLTLItemsRow[] LTLItems = order.GetOrderLTLItemsRows();
            if (LTLItems.Length == 0)
            {
                return 0;
            }
            System.Decimal weight = 0;
            for (int i = 0; i < LTLItems.Length; i++)
            {
                if (!String.IsNullOrEmpty(LTLItems[i]["LTL Item Total Weight"].ToString()) && !LTLItems[i]["LTL Item Total Weight"].ToString().Equals(""))
                {
                    weight += System.Convert.ToDecimal(LTLItems[i]["LTL Item Total Weight"]);
                }
            }
            return weight;
        }

        private void button_PackagesAdd_Click(object sender, EventArgs e)
        {
            AddNewPackage();
            //try to read the weight from scale, if one is connected - then the GetWeight button will be enabled too
            if (Properties.Settings.Default.InputWeight)
            {
                ReadWeight();
            }

            fKOrdersTableOrderPackagesTableBindingSource.Position = fKOrdersTableOrderPackagesTableBindingSource.Count - 1;
        }

        private void AddNewPackage()
        {
            if (OrdersTableBindingSource.Position < 0)
            {
                MessageBox.Show("There is no order selected!", "Attention", MessageBoxButtons.OK);
                return;
            }
            DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            if (order == null)
            {
                return;
            }
            if (!Template.MPSPackageLevel && (order.GetOrderPackagesTableRows().Length > 0))
            {
                MessageBox.Show("The current configuration does not allow adding more packages", "Atention", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            if (tabPackageType.SelectedTab == tabPagePackages)
            {
                object order_number = order["Order #"];
                if (order_number == null || order_number.Equals(""))
                {
                    return;
                }

                Data.OrderPackagesTableRow[] packages = order.GetOrderPackagesTableRows();
                if (packages.Length > 0)
                {
                    //if the user adds more than one row, than the package number would increase too much, when adding the
                    //previous line's Package #, then "- new" will aprear multiple times, so the is is taken from the first package
                    object package_number = packages[0]["Package #"];

                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = false;
                    fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();

                    string new_package_number = (order.GetOrderPackagesTableRows().Count() + 1).ToString();
                    Data.OrderPackagesTableRow addedRow = data.OrderPackagesTable.AddOrderPackagesTableRow(order, new_package_number,
                        0, packages[order.GetOrderPackagesTableRows().Length - 1]["Package Weight Type"].ToString(), 0, 0, 0,
                        packages[order.GetOrderPackagesTableRows().Length - 1]["Package Dimensions Type"].ToString(),
                        0, "", "");

                    if (data.OrderPackagesTable.Columns.IndexOf("Package Insurance Currency") > -1)
                    {
                        addedRow["Package Insurance Currency"] = packages[0]["Package Insurance Currency"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Insurance Amount") > -1)
                    {
                        addedRow["Package Insurance Amount"] = packages[0]["Package Insurance Amount"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Reference Info 1") > -1)
                    {
                        addedRow["Package Reference Info 1"] = packages[0]["Package Reference Info 1"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package PO Number") > -1)
                    {
                        addedRow["Package PO Number"] = packages[0]["Package PO Number"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Invoice Number") > -1)
                    {
                        addedRow["Package Invoice Number"] = packages[0]["Package Invoice Number"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Number") > -1)
                    {
                        addedRow["Package Number"] = package_number;
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Total Weight") > -1)
                    {
                        addedRow["Total Weight"] = packages[0]["Total Weight"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Packages") > -1)
                    {
                        addedRow["Packages"] = System.Convert.ToInt32(packages[0]["Packages"]) + 1;
                        foreach (Data.OrderPackagesTableRow d in data.OrderPackagesTable.Rows)
                        {
                            d["Packages"] = addedRow["Packages"];
                        }
                    }
                    foreach (DataColumn col in data.OrderPackagesTable.Columns)
                    {
                        if (addedRow[col.ColumnName].ToString().Equals(""))
                        {
                            addedRow[col.ColumnName] = packages[0][col.ColumnName];
                        }
                    }

                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = true;
                    fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(true);
                    fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();
                    updateNoOfPackagesInSkids(order);
                }
                else
                {
                    //if there is no package for this order, add a new one with default 0 and "" values(and Package # = "no. - new")
                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = false;
                    fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();

                    string new_package_number = (order.GetOrderPackagesTableRows().Count() + 1).ToString();
                    Data.OrderPackagesTableRow addedRow = data.OrderPackagesTable.AddOrderPackagesTableRow(order, new_package_number,
                        0, "", 0, 0, 0, "", 0, "", "");

                    if (!data.OrderPackagesTable.Columns.Contains("Package Weight Type"))
                    {
                        data.OrderPackagesTable.Columns.Add("Package Weight Type");
                    }

                    addedRow["Package Weight Type"] = "LBS";

                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = true;
                    fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(true);
                    fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();
   

                    fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = false;
                    fkOrdersTableOrderSkidsTableBindingSource.SuspendBinding();

                    string new_skid_number = "1 - new";
                    Data.OrdersSkidsTableRow addRow = data.OrdersSkidsTable.AddOrdersSkidsTableRow(order, new_skid_number,
                        "1", "1", "false", "true", "1", "0", "LBS", "", "", "", "", "", "I", "", "", "", "", "", "", "", "125.0", "1", "", "");

                    fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = true;
                    fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(true);
                    fkOrdersTableOrderSkidsTableBindingSource.ResumeBinding();
                }
                lblTotalWeight.Text = "Total Weight: " + getPackagesWeight(order).ToString();

                fKOrdersTableOrderPackagesTableBindingSource.Position = fKOrdersTableOrderPackagesTableBindingSource.Count - 1;
                   
            }

            if (tabPackageType.SelectedTab == tabPageSkids)
            {
                Data.OrdersTableRow skid_order = order_row.Row as Data.OrdersTableRow;
                if (skid_order == null)
                {
                    return;
                }
                object order_no = skid_order["Order #"];
                if (order_no == null || order_no.Equals(""))
                {
                    return;
                }

                Data.OrdersSkidsTableRow[] skids = skid_order.GetOrdersSkidsTableRows();
                if (skids.Length > 0)
                {
                    //if the user adds more than one row, than the package number would increase too much, when adding the
                    //previous line's Package #, then "- new" will aprear multiple times, so the is is taken from the first package
                    object skid_number = skids[0]["Skid #"];

                    fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = false;
                    fkOrdersTableOrderSkidsTableBindingSource.SuspendBinding();

                    string new_skid_number = (skids.Length + 1).ToString() + " - new";
                    Data.OrdersSkidsTableRow addRow = data.OrdersSkidsTable.AddOrdersSkidsTableRow(skid_order, new_skid_number,
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Use Skids"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Count"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Shipment Level"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Skid Level"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skids Count"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Weight"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Weight Type"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Insurance"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Insurance Currency"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Length"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Width"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Height"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Dimensions Type"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Reference1"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Reference2"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid PO Number"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Shipment ID"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Invoice Number"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["ShipmentPONumber"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Shipment Reference"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Freight Class"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Number of Packages in Skid"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["Skid Description"].ToString(),
                        skids[skid_order.GetOrdersSkidsTableRows().Length - 1]["BOL Comment"].ToString());

                    fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = true;
                    fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(true);
                    fkOrdersTableOrderSkidsTableBindingSource.ResumeBinding();
                }
                else
                {
                    //if there is no skid for this order, add a new one with default 0 and "" values(and Skid # = "no. - new")
                    fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = false;
                    fkOrdersTableOrderSkidsTableBindingSource.SuspendBinding();

                    string new_skid_number = skids.Length + " - new";
                    Data.OrdersSkidsTableRow addRow = data.OrdersSkidsTable.AddOrdersSkidsTableRow(skid_order, new_skid_number,
                        "1", "1", "false", "true", "1", "0", "LBS", "", "", "", "", "", "I", "", "", "", "", "", "", "", "125.0", "1", "", "");

                    fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = true;
                    fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(true);
                    fkOrdersTableOrderSkidsTableBindingSource.ResumeBinding();
                }
                lblTotalWeight.Text = "Total Weight: " + getSkidsWeight(skid_order).ToString();
            }

            if (tabPackageType.SelectedTab == tabPageItems)
            {
                Data.OrdersTableRow items_order = order_row.Row as Data.OrdersTableRow;
                if (items_order == null)
                {
                    return;
                }
                object order_no = items_order["Order #"];
                if (order_no == null || order_no.Equals(""))
                {
                    return;
                }

                Data.OrderLTLItemsRow[] items = items_order.GetOrderLTLItemsRows();
                if (items.Length > 0)
                {
                    //if the user adds more than one row, than the package number would increase too much, when adding the
                    //previous line's Package #, then "- new" will aprear multiple times, so the is is taken from the first package
                    object item_number = items[0]["LTL Item #"];

                    fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = false;
                    fkOrdersTableLTLItemsBindingSource.SuspendBinding();

                    string new_items_number = item_number.ToString() + " - " + (items_order.GetOrderPackagesTableRows().Length + 1) + " - new";
                    Data.OrderLTLItemsRow addItemaRow = data.OrderLTLItems.AddOrderLTLItemsRow(
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Items Count"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item Description"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item Total Weight"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item Total Weight Type"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item Total Length"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item Total Width"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item Total Height"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item Total Dims Type"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item Freight Class ID"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item Quantity"].ToString(),
                        items[items_order.GetOrderLTLItemsRows().Length - 1]["LTL Item UM"].ToString(),
                        items_order, new_items_number);

                    fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = true;
                    fkOrdersTableLTLItemsBindingSource.ResetBindings(true);
                    fkOrdersTableLTLItemsBindingSource.ResumeBinding();
                }
                else
                {
                    //if there is no skid for this order, add a new one with default 0 and "" values(and Skid # = "no. - new")
                    fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = false;
                    fkOrdersTableLTLItemsBindingSource.SuspendBinding();

                    string new_items_number = items.Length + " - new";
                    Data.OrderLTLItemsRow addItemsRow = data.OrderLTLItems.AddOrderLTLItemsRow(
                        "1", "", "1", "LBS", "1", "1", "1", "I", "", "1", "", items_order, new_items_number);

                    fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = true;
                    fkOrdersTableLTLItemsBindingSource.ResetBindings(true);
                    fkOrdersTableLTLItemsBindingSource.ResumeBinding();
                }
                lblTotalWeight.Text = "Total Weight: " + getLTLItemsWeight(order).ToString();
            }
        }

        private void updateNoOfPackagesInSkids(Data.OrdersTableRow order)
        {
            Data.OrdersSkidsTableRow[] skids = order.GetOrdersSkidsTableRows();
            if (skids.Length > 0)
            {
                string packsInSkid = skids[0]["Skid Number of Packages in Skid"].ToString();
                int p = System.Convert.ToInt32(packsInSkid) + 1;
                skids[0]["Skid Number of Packages in Skid"] = p.ToString();
            }
        }

        private void buttonRemovePackage_Click(object sender, EventArgs e)
        {
            if (OrdersTableBindingSource.Position < 0)
            {
                MessageBox.Show("There is no order selected!", "Attention", MessageBoxButtons.OK);
                return;
            }
            DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            if (order == null)
            {
                return;
            }
            object order_number = order["Order #"];
            if (order_number == null || order_number.Equals(""))
            {
                return;
            }

            Data.OrderPackagesTableRow[] packages = order.GetOrderPackagesTableRows();
            if (packages.Length == 0)
            {
                return;
            }
            if (tabPackageType.SelectedTab == tabPagePackages)
            {
                if (fKOrdersTableOrderPackagesTableBindingSource.Position < 0)
                {
                    return;
                }
                DataRowView package_row = fKOrdersTableOrderPackagesTableBindingSource.Current as DataRowView;
                Data.OrderPackagesTableRow package = package_row.Row as Data.OrderPackagesTableRow;
                if (package == null)
                {
                    return;
                }
                fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = false;
                fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();
                //if there are more packages, then the skid weight and number of packages must also be updated
                if (packages.Length > 1)
                {
                    //if it's shipment level, then the weight column is Total Weight(but Package Weight column could be there as well, and if so, use it instead of Total Weight(this column should be the same for every row, and it's updated on add and on remove package))
                    if (data.OrderPackagesTable.Columns.IndexOf("Total Weight") > 0)
                    {
                        if (data.OrderPackagesTable.Columns.IndexOf("Package Weight") > 0)
                        {
                            updateSkidWeightAndPacks(order, package["Package Weight"].ToString());
                            if (!package["Total Weight"].ToString().Equals("") && package["Total Weight"] != null)
                                for (int i = 0; i < packages.Length; i++)
                                {
                                    packages[i]["Total Weight"] = System.Convert.ToDecimal(packages[i]["Total Weight"]) - System.Convert.ToDecimal(package["Package Weight"]);
                                }
                        }
                        else
                        {
                            updateSkidWeightAndPacks(order, package["Total Weight"].ToString());
                        }
                    }
                    else
                    {
                        updateSkidWeightAndPacks(order, package["Package Weight"].ToString());
                    }
                }
                if (data.OrderPackagesTable.Columns.IndexOf("Packages") > 0)
                    for (int i = 0; i < packages.Length; i++)
                    {
                        packages[i]["Packages"] = System.Convert.ToInt32(packages[i]["Packages"]) - 1;
                    }
                data.OrderPackagesTable.RemoveOrderPackagesTableRow(package);

                fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = true;
                fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(true);
                fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();

                packages = order.GetOrderPackagesTableRows();
                //if all packages were removed, delete the skid also
                if (packages.Length == 0)
                {
                    Data.OrdersSkidsTableRow skid = order.GetOrdersSkidsTableRows()[0];
                    if (skid == null)
                        return;
                    fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = false;
                    fkOrdersTableOrderSkidsTableBindingSource.SuspendBinding();

                    data.OrdersSkidsTable.RemoveOrdersSkidsTableRow(skid);

                    fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = true;
                    fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(true);
                    fkOrdersTableOrderSkidsTableBindingSource.ResumeBinding();
                }
                lblTotalWeight.Text = "Total Weight: " + getPackagesWeight(order).ToString();
            }
            if (tabPackageType.SelectedTab == tabPageSkids)
            {
                if (fkOrdersTableOrderSkidsTableBindingSource.Position < 0)
                {
                    return;
                }
                DataRowView skids_row = fkOrdersTableOrderSkidsTableBindingSource.Current as DataRowView;
                Data.OrdersSkidsTableRow skid = skids_row.Row as Data.OrdersSkidsTableRow;
                if (skid == null)
                {
                    return;
                }
                fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = false;
                fkOrdersTableOrderSkidsTableBindingSource.SuspendBinding();

                data.OrdersSkidsTable.RemoveOrdersSkidsTableRow(skid);

                fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = true;
                fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(true);
                fkOrdersTableOrderSkidsTableBindingSource.ResumeBinding();
                lblTotalWeight.Text = "Total Weight: " + getSkidsWeight(order).ToString();
            }

            if (tabPackageType.SelectedTab == tabPageItems)
            {
                if (fkOrdersTableLTLItemsBindingSource.Position < 0)
                {
                    return;
                }
                DataRowView items_row = fkOrdersTableLTLItemsBindingSource.Current as DataRowView;
                Data.OrderLTLItemsRow item = items_row.Row as Data.OrderLTLItemsRow;
                if (item == null)
                {
                    return;
                }
                fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = false;
                fkOrdersTableLTLItemsBindingSource.SuspendBinding();

                data.OrderLTLItems.RemoveOrderLTLItemsRow(item);

                fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = true;
                fkOrdersTableLTLItemsBindingSource.ResetBindings(true);
                fkOrdersTableLTLItemsBindingSource.ResumeBinding();
                lblTotalWeight.Text = "Total Weight: " + getLTLItemsWeight(order).ToString();
            }
        }

        private void updateSkidWeightAndPacks(Data.OrdersTableRow order, string weight)
        {
            Data.OrdersSkidsTableRow[] skids = order.GetOrdersSkidsTableRows();
            if (skids.Length > 0)
            {
                string skidWeight = skids[0]["Skid Weight"].ToString();
                string packsInSkid = skids[0]["Skid Number of Packages in Skid"].ToString();
                System.Decimal w = System.Convert.ToDecimal(skidWeight);
                if (weight != null && !weight.Equals(""))
                {
                    w -= System.Convert.ToDecimal(weight);
                }
                int p = System.Convert.ToInt32(packsInSkid) - 1;
                skids[0]["Skid Weight"] = w.ToString();
                skids[0]["Skid Number of Packages in Skid"] = p.ToString();
            }
        }

        private void OrderPackagesGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (OrdersTableBindingSource.Position < 0)
            {
                lblTotalWeight.Text = "Total Weight: 0";
                return;
            }
            DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            if (order_row == null)
            {
                return;
            }
            Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            if (order == null)
            {
                return;
            }
            object order_number = order["Order #"];
            if (order_number == null || order_number.Equals(""))
            {
                return;
            }

            Data.OrderPackagesTableRow[] packages = order.GetOrderPackagesTableRows();
            if (packages.Length == 0)
            {
                return;
            }

            CheckDimensions(order, packages);

            RecalculateSkidWeight(order, packages);

            switch (tabPackageType.SelectedTab.Name)
            {
                case "tabPagePackages":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getPackagesWeight(order);
                        break;
                    }
                case "tabPageSkids":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getSkidsWeight(order);
                        break;
                    }
                case "tabPageItems":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getLTLItemsWeight(order);
                        break;
                    }
            }

        }

        private void CheckDimensions(Data.OrdersTableRow order, Data.OrderPackagesTableRow[] packages)
        {
            if (!Properties.Settings.Default.PackageDimensions.Equals(""))
            {
                if (packageDimsBindingSource.Position < 0)
                {
                    return;
                }
                if (packageDimsBindingSource.Current == null)
                {
                    return;
                }

                if (fKOrdersTableOrderPackagesTableBindingSource.Current == null)
                {
                    return;
                }
                if (fKOrdersTableOrderPackagesTableBindingSource.Position < 0)
                {
                    return;
                }
               
                string code = cmbPackageDims.SelectedValue as string;

                DataRowView p_row = fKOrdersTableOrderPackagesTableBindingSource.Current as DataRowView;
                Data.OrderPackagesTableRow pRow = p_row.Row as Data.OrderPackagesTableRow;
                if (String.IsNullOrEmpty(pRow["Package Length"].ToString()) || String.IsNullOrEmpty(pRow["Package Width"].ToString()) || String.IsNullOrEmpty(pRow["Package Height"].ToString()))
                {
                    return;
                }
                //DataRowView d_row = packageDimsBindingSource.Current as DataRowView;
                Data.PackageDimensionsRow dimRow = data.PackageDimensions.FindByCode(code);
                decimal length = Convert.ToDecimal(pRow["Package Length"]);
                decimal width = Convert.ToDecimal(pRow["Package Width"]);
                decimal height = Convert.ToDecimal(pRow["Package Height"]);

                if ((!pRow["Package Length"].ToString().Equals(dimRow["Length"])) 
                    || (!pRow["Package Width"].ToString().Equals(dimRow["Width"])) 
                    || (!pRow["Package Height"].ToString().Equals(dimRow["Height"])))
                {
                    cmbPackageDims.SelectedIndex = 0;
                }
                pRow["Package Length"] = length;
                pRow["Package Width"] = width;
                pRow["Package Height"] = height;
                if (data.OrderPackagesTable.Columns.IndexOf("Package Dimensions Code") >= 0)
                {
                    pRow["Package Dimensions Code"] = code;
                }
            }
        }

        private void RecalculateSkidWeight(Data.OrdersTableRow order, Data.OrderPackagesTableRow[] packages)
        {
            Data.OrdersSkidsTableRow[] skids = order.GetOrdersSkidsTableRows();
            string skidWeight = "0";
            if (skids.Length > 0)
            {
                skidWeight = skids[0]["Skid Weight"].ToString();
            }

            System.Decimal weight = 0;
            for (int i = 0; i < packages.Length; i++)
            {
                if (!String.IsNullOrEmpty(packages[i]["Package Weight"].ToString()) && !packages[i]["Package Weight"].ToString().Equals(""))
                {
                    weight += System.Convert.ToDecimal(packages[i]["Package Weight"]);
                }
            }
            skidWeight = weight.ToString();
            skids[0]["Skid Weight"] = skidWeight;

            if (data.OrderPackagesTable.Columns.IndexOf("Total Weight") > -1)
            {
                for (int i = 0; i < packages.Length; i++)
                {
                    packages[i]["Total Weight"] = weight;
                }
            }
        }

        private void OrderSkidsGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (OrdersTableBindingSource.Position < 0)
            {
                lblTotalWeight.Text = "Total Weight: 0";
                return;
            }
            DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            if (order_row == null)
            {
                return;
            }
            Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            if (order == null)
            {
                return;
            }
            switch (tabPackageType.SelectedTab.Name)
            {
                case "tabPagePackages":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getPackagesWeight(order);
                        break;
                    }
                case "tabPageSkids":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getSkidsWeight(order);
                        break;
                    }
                case "tabPageItems":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getLTLItemsWeight(order);
                        break;
                    }
            }
        }

        private void LTLItemsGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (OrdersTableBindingSource.Position < 0)
            {
                lblTotalWeight.Text = "Total Weight: 0";
                return;
            }
            DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            if (order_row == null)
            {
                return;
            }
            Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            if (order == null)
            {
                return;
            }
            switch (tabPackageType.SelectedTab.Name)
            {
                case "tabPagePackages":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getPackagesWeight(order);
                        break;
                    }
                case "tabPageSkids":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getSkidsWeight(order);
                        break;
                    }
                case "tabPageItems":
                    {
                        lblTotalWeight.Text = "Total Weight: " + getLTLItemsWeight(order);
                        break;
                    }
            }
        }

        private void ShipmentsGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //if there is only one row in grid make the selected column true for all its packages, because this is the only shipment to save to db
                if (data.ShipmentsTable.Rows.Count > 0)
                {
                    shipmentsTableBindingSource.EndEdit();
                    DataRowView ship_row = shipmentsTableBindingSource.Current as DataRowView;

                    Data.ShipmentsTableRow shipp = ship_row.Row as Data.ShipmentsTableRow;
                    if (shipp == null)
                    {
                        return;
                    }
                    if (ShipmentsGrid.RowCount > 1)
                    {
                        if ((bool)shipp["Selected"] == true)
                        {
                            foreach (Data.ShipmentsTableRow r in data.ShipmentsTable)
                            {
                                if (r["Order #"].ToString() == shipp["Order #"].ToString())
                                {
                                    r["Selected"] = true;
                                }
                            }
                        }
                        else
                        {
                            foreach (Data.ShipmentsTableRow r in data.ShipmentsTable)
                            {
                                if (r["Order #"].ToString() == shipp["Order #"].ToString())
                                {
                                    r["Selected"] = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        if ((bool)shipp["Selected"] == true)
                        {
                            foreach (Data.ShipmentsTableRow r in data.ShipmentsTable)
                            {
                                r["Selected"] = true;
                            }
                        }
                        else
                        {
                            foreach (Data.ShipmentsTableRow r in data.ShipmentsTable)
                            {
                                r["Selected"] = false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //
            }

        }

        private void ShipmentsGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    ShipmentsGrid.EndEdit();
                }
            }
            catch (Exception)
            {
                //
            }
        }

        private void btnEditRates_Click(object sender, EventArgs e)
        {
            if (data.ShipmentsTable.Rows.Count == 0)
            {
                MessageBox.Show("There is no shipment in Shipments table. Please ship an order and try again", "Attention", MessageBoxButtons.OK);
                return;
            }

            frmReplyOverride editRates = new frmReplyOverride();
            if (editRates.ShowDialog() == DialogResult.OK)
            {
                shipmentsTableBindingSource.ResetBindings(true);
            }
            Data.ShipmentsTableRow shipment;
            int packages = data.ShipmentsTable.Rows.Count;
            for (int p = 0; p < packages; p++)
            {
                shipment = (Data.ShipmentsTableRow)data.ShipmentsTable.Rows[p];
                if (shipment != null)
                {
                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", shipment._Order__, data);
                }
            }
            for (int p = 0; p < packages; p++)
            {
                shipment = (Data.ShipmentsTableRow)data.ShipmentsTable.Rows[p];
                if (shipment != null)
                {
                    BackupFile.AddShipment(data.ShipmentsTable.TableName, shipment as System.Data.DataRow, data);
                }
            }
        }

        private void OrderPackagesGrid_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (fKOrdersTableOrderPackagesTableBindingSource.DataSource == null)
                {
                    return;
                }
                if (fKOrdersTableOrderPackagesTableBindingSource.Position < 0)
                {
                    return;
                }
                if (OrdersTableBindingSource.DataSource == null)
                {
                    return;
                }
                if (OrdersTableBindingSource.Position < 0 || OrdersTableBindingSource.Current == null)
                {
                    lblTotalWeight.Text = "Total Weight: 0";
                    return;
                }
                DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;

                Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
                if (order == null)
                {
                    return;
                }
                switch (tabPackageType.SelectedTab.Name)
                {
                    case "tabPagePackages":
                        {
                            lblTotalWeight.Text = "Total Weight: " + getPackagesWeight(order);
                            break;
                        }
                    case "tabPageSkids":
                        {
                            lblTotalWeight.Text = "Total Weight: " + getSkidsWeight(order);
                            break;
                        }
                    case "tabPageItems":
                        {
                            lblTotalWeight.Text = "Total Weight: " + getLTLItemsWeight(order);
                            break;
                        }
                }
            }
            catch (Exception)
            {
                //
            }
        }
        private void OrderPackagesGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
       
        private void btnHistoryLabel_Click(object sender, EventArgs e)
        {
            DataGridViewRow historyRow = HistoryGrid.CurrentRow;
            if (historyRow == null)
            {
                MessageBox.Show("There is no row selected");
                return;
            }
            try
            {
                if (Template.Name.Equals("MAAX"))
                {
                HistoryGrid.EndEdit();

                List<print_class> temp = new List<print_class>();
                bool ok = false;
                for (int t = HistoryGrid.Rows.Count - 1; t >= 0; t--)
                {
                    DataGridViewRow Row = HistoryGrid.Rows[t] as DataGridViewRow;
                    if (Row.Cells[0].Value.ToString() == "true" || Row.Cells[0].Value.ToString() == "True")
                    {
                        ok = true;
                        break;
                    }
                }
                if (ok)
                {
                    if (!bg_print.IsBusy)
                    {
                        for (int t = HistoryGrid.Rows.Count - 1; t >= 0; t--)
                        {
                            DataGridViewRow Row = HistoryGrid.Rows[t] as DataGridViewRow;
                            if (Row.Cells[0].Value.ToString() == "true" || Row.Cells[0].Value.ToString() == "True")
                            {
                                print_class print = new print_class();
                                print.order = HistoryGrid.Rows[t].Cells[0].ToString();
                                if (HistoryGrid.Rows[t].Cells[0].ToString().Equals(print.order))
                                {
                                    print.label = HistoryGrid.Rows[t].Cells["Label URL"].Value == null ? "" : HistoryGrid.Rows[t].Cells["Label URL"].Value.ToString();
                                    print.commercial_invoice = "";
                                    print.bol = "";
                                    print.return_label = "";
                                }
                                if (print.label != "" && print.label != "N/A")
                                {
                                    temp.Add(print);
                                }
                            }
                        }
                        if (!bg_print.IsBusy && print_list.Count == 0 && temp.Count != 0)
                        {
                            print_list.AddRange(temp);
                            bg_print.RunWorkerAsync();
                        }
                    }
                    else
                    {
                        MessageBox.Show("InteGr8 is still printing, please try again later.");
                    }
                }
            }
                else
                {
                    if (historyRow.Cells["Label URL"].Value.ToString().Equals("") || historyRow.Cells["Label URL"].Value.ToString().Equals("N/A") || historyRow.Cells["Label URL"].Value == null)
                    {
                        MessageBox.Show("There is no url for label");
                        return;
                    }
                    Utils.openPDF(historyRow.Cells["Label URL"].Value.ToString(), Template.UseAutoprint ? (Template.AutoprintLabelsFolder != "" ? Template.AutoprintLabelsFolder : Application.LocalUserAppDataPath) : Application.LocalUserAppDataPath);
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in HistoryLabelButton_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnHistoryCI_Click(object sender, EventArgs e)
        {
            DataGridViewRow historyRow = HistoryGrid.CurrentRow;
            if (historyRow == null)
            {
                MessageBox.Show("There is no row selected");
                return;
            }
            try
            {
                if (Template.Name.Equals("MAAX"))
                {
                    HistoryGrid.EndEdit();
                    List<print_class> temp = new List<print_class>();
                    bool ok = false;
                    for (int t = HistoryGrid.Rows.Count - 1; t >= 0; t--)
                    {
                        DataGridViewRow Row = HistoryGrid.Rows[t] as DataGridViewRow;
                        if (Row.Cells[0].Value.ToString() == "true" || Row.Cells[0].Value.ToString() == "True")
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (ok)
                    {
                        if (!bg_print.IsBusy)
                        {
                            for (int t = HistoryGrid.Rows.Count - 1; t >= 0; t--)
                            {
                                DataGridViewRow Row = HistoryGrid.Rows[t] as DataGridViewRow;
                                if (Row.Cells[0].Value.ToString() == "true" || Row.Cells[0].Value.ToString() == "True")
                                {
                                    print_class print = new print_class();
                                    print.order = HistoryGrid.Rows[t].Cells[0].ToString();
                                    if (HistoryGrid.Rows[t].Cells[0].ToString().Equals(print.order))
                                    {
                                        print.label = "";
                                        print.commercial_invoice = HistoryGrid.Rows[t].Cells["CIURL"].Value == null ? "" : HistoryGrid.Rows[t].Cells["CIURL"].ToString();
                                        print.bol = "";
                                        print.return_label = "";
                                    }
                                    if (print.label != "" && print.label != "N/A")
                                    {
                                        temp.Add(print);
                                    }
                                }
                            }
                            if (!bg_print.IsBusy && print_list.Count == 0 && temp.Count != 0)
                            {
                                print_list.AddRange(temp);
                                bg_print.RunWorkerAsync();
                            }
                        }
                        else
                        {
                            MessageBox.Show("InteGr8 is still printing, please try again later.");
                        }
                    }
                }
                else
                {
                    //Data.ShipmentsTableRow shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order.Row["Order #"].ToString(), Convert.ToInt32(order.Row["Package_Index"]));
                    //if (shipment == null || shipment.Label_URL.Equals("") || shipment.Label_URL.Equals("N/A") || shipment.Label_URL == null)
                    if (historyRow.Cells["CIURL"].Value.ToString().Equals("") || historyRow.Cells["CIURL"].Value.ToString().Equals("N/A") || historyRow.Cells["CIURL"].Value == null)
                    {
                        MessageBox.Show("There is no url for commercial invoice");
                        return;
                    }
                    Utils.openPDF(historyRow.Cells["CIURL"].Value.ToString(), Template.UseAutoprint ? (Template.AutoprintCIFolder != "" ? Template.AutoprintCIFolder : Application.LocalUserAppDataPath) : Application.LocalUserAppDataPath);
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in HistoryCIButton_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnHistoryBOLPrint_Click(object sender, EventArgs e)
        {
            DataGridViewRow historyRow = HistoryGrid.CurrentRow;
            if (historyRow == null)
            {
                MessageBox.Show("There is no row selected");
                return;
            }
            try
            {
                if (Template.Name.Equals("MAAX"))
                {
                    HistoryGrid.EndEdit();
                    List<print_class> temp = new List<print_class>();
                    bool ok = false;
                    for (int t = HistoryGrid.Rows.Count - 1; t >= 0; t--)
                    {
                        DataGridViewRow Row = HistoryGrid.Rows[t] as DataGridViewRow;
                        if (Row.Cells[0].Value.ToString() == "true" || Row.Cells[0].Value.ToString() == "True")
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (ok)
                    {
                        if (!bg_print.IsBusy)
                        {
                            for (int t = HistoryGrid.Rows.Count - 1; t >= 0; t--)
                            {
                                DataGridViewRow Row = HistoryGrid.Rows[t] as DataGridViewRow;
                                if (Row.Cells[0].Value.ToString() == "true" || Row.Cells[0].Value.ToString() == "True")
                                {
                                    print_class print = new print_class();
                                    print.order = HistoryGrid.Rows[t].Cells[0].ToString();
                                    if (HistoryGrid.Rows[t].Cells[0].ToString().Equals(print.order))
                                    {
                                        print.label = "";
                                        print.commercial_invoice = "";
                                        print.bol = HistoryGrid.Rows[t].Cells["BOLURL"].Value == null ? "" : HistoryGrid.Rows[t].Cells["BOLURL"].ToString();
                                        print.return_label = "";
                                    }
                                    if (print.label != "" && print.label != "N/A")
                                    {
                                        temp.Add(print);
                                    }
                                }
                            }
                            if (!bg_print.IsBusy && print_list.Count == 0 && temp.Count != 0)
                            {
                                print_list.AddRange(temp);
                                bg_print.RunWorkerAsync();
                            }
                        }
                        else
                        {
                            MessageBox.Show("InteGr8 is still printing, please try again later.");
                        }
                    }
                }
                else
                {
                    if (historyRow.Cells["BOLURL"].Value.ToString().Equals("") || historyRow.Cells["BOLURL"].Value.ToString().Equals("N/A") || historyRow.Cells["BOLURL"].Value == null)
                    {
                        MessageBox.Show("There is no url for BOL");
                        return;
                    }
                    Utils.openPDF(historyRow.Cells["BOLURL"].Value.ToString(), Template.UseAutoprint ? (Template.AutoprintBOLFolder != "" ? Template.AutoprintBOLFolder : Application.LocalUserAppDataPath) : Application.LocalUserAppDataPath);
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in HistoryBOLButton_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnHistoryReturnLabel_Click(object sender, EventArgs e)
        {
            DataGridViewRow historyRow = HistoryGrid.CurrentRow;
            if (historyRow == null)
            {
                MessageBox.Show("There is no row selected");
                return;
            }
            try
            {
                if (Template.Name.Equals("MAAX"))
                {
                    HistoryGrid.EndEdit();
                    List<print_class> temp = new List<print_class>();
                    bool ok = false;
                    for (int t = HistoryGrid.Rows.Count - 1; t >= 0; t--)
                    {
                        DataGridViewRow Row = HistoryGrid.Rows[t] as DataGridViewRow;
                        if (Row.Cells[0].Value.ToString() == "true" || Row.Cells[0].Value.ToString() == "True")
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (ok)
                    {
                        if (!bg_print.IsBusy)
                        {
                            for (int t = HistoryGrid.Rows.Count - 1; t >= 0; t--)
                            {
                                DataGridViewRow Row = HistoryGrid.Rows[t] as DataGridViewRow;
                                if (Row.Cells[0].Value.ToString() == "true" || Row.Cells[0].Value.ToString() == "True")
                                {
                                    print_class print = new print_class();
                                    print.order = HistoryGrid.Rows[t].Cells[0].ToString();
                                    if (HistoryGrid.Rows[t].Cells[0].ToString().Equals(print.order))
                                    {
                                        print.label = "";
                                        print.commercial_invoice = "";
                                        print.bol = "";
                                        print.return_label = HistoryGrid.Rows[t].Cells["ReturnLabelURL"].Value == null ? "" : HistoryGrid.Rows[t].Cells["ReturnLabelURL"].ToString();
                                    }
                                    if (print.label != "" && print.label != "N/A")
                                    {
                                        temp.Add(print);
                                    }
                                }
                            }
                            if (!bg_print.IsBusy && print_list.Count == 0 && temp.Count != 0)
                            {
                                print_list.AddRange(temp);
                                bg_print.RunWorkerAsync();
                            }
                        }
                        else
                        {
                            MessageBox.Show("InteGr8 is still printing, please try again later.");
                        }
                    }
                }
                else
                {
                    if (historyRow.Cells["ReturnLabelURL"].Value.ToString().Equals("") || historyRow.Cells["ReturnLabelURL"].Value.ToString().Equals("N/A") || historyRow.Cells["ReturnLabelURL"].Value == null)
                    {
                        MessageBox.Show("There is no url for return label");
                        return;
                    }
                    Utils.openPDF(historyRow.Cells["ReturnLabelURL"].Value.ToString(), Template.UseAutoprint ? (Template.AutoprintReturnLabelFolder != "" ? Template.AutoprintReturnLabelFolder : Application.LocalUserAppDataPath) : Application.LocalUserAppDataPath);
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in HistoryReturnLabelButton_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAcceptFilter_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbFilterColumns.SelectedIndex < 0)
                {
                    cmbFilterColumns.SelectedIndex = 0;
                }
                string currentFilterText = cmbFilterColumns.SelectedValue.ToString();
                int position = -1;
                foreach (Filter_Expression fex in Select_Filter.Expressions)
                {
                    position++;
                    if (fex.Left_Operand.Equals(currentFilterText))
                    {
                        fex.Right_Operand_1_No_Quotes = tbValue1.Text.Trim();
                        ToolTip.SetToolTip(FilterLinkLabel, fex.ToString());
                        filterExpressionBindingSource.Position = position;
                    }
                }
                
                ReloadInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void btnGetWeight_Click(object sender, EventArgs e)
        {

            //DataRowView row = OrderPackagesGrid.SelectedRows[0].DataBoundItem as DataRowView;
            //*********************added code
            try
            {
                //MessageBox.Show("Before reading weight");
                DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
                if (order_row == null)
                {
                    return;
                }
                Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
                if (order == null)
                {
                    return;
                }
                //MessageBox.Show("Selected order is not null");
                object order_number = order["Order #"];
                if (order_number == null || order_number.Equals(""))
                {
                    return;
                }
                //MessageBox.Show("Order number is: " + order_number.ToString());
                System.Decimal weight = 0;
                try
                {
                    if (!String.IsNullOrEmpty(Settings.Default.ScaleVID) && !String.IsNullOrEmpty(Settings.Default.ScalePID))
                    {
                        ScaleIntegr8.MyScale scale = new ScaleIntegr8.MyScale();
                        weight = scale.GetWeight(Settings.Default.ScaleVID, Settings.Default.ScalePID);
                        if (weight == -1)
                        {
                            MessageBox.Show("Could not read the weight, no scale connected! Please connect a scale and try again!", "Scale reading error");
                            return;
                        }
                    }
                    else
                    {
                        //read from NCI scale here
                        ScaleInteg8WTComm.MyScale scale = new ScaleInteg8WTComm.MyScale();
                        weight = Convert.ToDecimal(scale.GetWeight());
                        if (weight == -1)
                        {
                            MessageBox.Show("Could not read the weight, no scale connected! Please connect a scale and try again!", "Scale reading error");
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Scale reading error: " + ex.Message + "/n Stack trace: " + ex.StackTrace);
                    return;
                }
                //MessageBox.Show("Red weight is: " + weight.ToString());
                if (tabPackageType.SelectedTab == tabPagePackages)
                {
                    //MessageBox.Show("Selected tab is Packages(Small)");
                    Data.OrderPackagesTableRow[] packages = order.GetOrderPackagesTableRows();
                    if (packages.Length == 0)
                    {
                        return;
                    }
                    //MessageBox.Show("No. of packages for current order is: " + packages.Length);
                    DataRowView package_row = fKOrdersTableOrderPackagesTableBindingSource.Current as DataRowView;
                    Data.OrderPackagesTableRow package = package_row.Row as Data.OrderPackagesTableRow ;
                    if (package == null)
                    {
                        MessageBox.Show("No package is selected. Please select one package to add the weight!", "Weight input");
                        return;
                    }
                    //MessageBox.Show("Current package is(Package #): " + package["Package #"].ToString());
                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = false;
                    fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();

                    if (data.OrderPackagesTable.Columns.IndexOf("Package Weight") > 0)
                    {
                        package["Package Weight"] = weight;
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Total Weight") > 0)
                    {
                        package["Total Weight"] = weight;
                    }
                    //MessageBox.Show("New weight for " + package["Package #"].ToString() + " package is: " + package["Package Weight"].ToString());
                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = true;
                    fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(true);
                    fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();

                    //******************************************************
                    //row["Package Weight"] = scale.GetWeight("0EB8", "F000");
                    OrderPackagesGrid.Refresh();

                    lblTotalWeight.Text = "Total Weight: " + getPackagesWeight(order).ToString();
                }
            
            if (tabPackageType.SelectedTab == tabPageSkids)
            {
                //MessageBox.Show("Selected tab is Skid(LTL)");
                   
                readSkidWeight(order, weight);
            }
            if (tabPackageType.SelectedTab == tabPageItems)
            {
                //MessageBox.Show("Selected tab is LTL Items");
                   
                readLTLTItemWeight(order, weight);
            }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An exception occured at reading Package weight: " + ex.Message, "Weight read error");
            }
            //**********************************************previous code
            //frmGetWeight newPackageWeight = new frmGetWeight();
            //newPackageWeight.ShowDialog();
            //if (newPackageWeight.DialogResult == DialogResult.OK)
            //{
                //change the way the weight is set, so that only the selected package for the selected order will get the weight set
                //foreach (Data.OrdersTableRow order in data.OrdersTable)
                //{
                    //Data.OrderPackagesTableRow[] packages = order.GetOrderPackagesTableRows();
                   // foreach (Data.OrderPackagesTableRow p in packages)
                   // {
                        //if (data.OrderPackagesTable.Columns.IndexOf("Package Weight") > 0)
                        //{
                        //    p["Package Weight"] = newPackageWeight.enteredPackageWeight;
                        //}
                   // }
                    //Data.OrdersSkidsTableRow[] skids = order.GetOrdersSkidsTableRows();
                    //foreach (Data.OrdersSkidsTableRow s in skids)
                    //{
                        //if (data.OrdersSkidsTable.Columns.IndexOf("Skid Weight") > 0)
                        //{
                        //    s["Skid Weight"] = getPackagesWeight(order).ToString();
                        //}
                    //}
                    //Data.OrderLTLItemsRow[] LTLItems = order.GetOrderLTLItemsRows();
                    //foreach (Data.OrderLTLItemsRow LTLi in LTLItems)
                   // {
                         //if (data.OrderLTLItems.Columns.IndexOf("LTL Item Total Weight") > 0)
                        //{
                        //    LTLi["LTL Item Total Weight"] = getPackagesWeight(order).ToString();
                        //}
                    //}
                //}
                //lblTotalWeight.Text = "Total Weight: " + newPackageWeight.enteredPackageWeight;
            //}
        }

       

        private void readSkidWeight(Data.OrdersTableRow order, System.Decimal weight)
        {
            try
            {
                Data.OrdersSkidsTableRow[] skids = order.GetOrdersSkidsTableRows();
                if (skids.Length == 0)
                {
                    return;
                }
                DataRowView skid_row = fkOrdersTableOrderSkidsTableBindingSource.Current as DataRowView;
                Data.OrdersSkidsTableRow skid = skid_row.Row as Data.OrdersSkidsTableRow;
                if (skid == null)
                {
                    MessageBox.Show("No skid is selected. Please select one skid to add the weight!", "Weight input");
                    return;
                }

                fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = false;
                fkOrdersTableOrderSkidsTableBindingSource.SuspendBinding();

                //System.Decimal weight = 0;

                //ScaleIntegr8.MyScale scale = new ScaleIntegr8.MyScale();
                //weight = scale.GetWeight("0EB8", "F000");

                if (data.OrdersSkidsTable.Columns.IndexOf("Skid Weight") > 0)
                {
                    skid["Skid Weight"] = weight;
                }

                fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = true;
                fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(true);
                fkOrdersTableOrderSkidsTableBindingSource.ResumeBinding();

                //******************************************************
                //row["Package Weight"] = scale.GetWeight("0EB8", "F000");
                OrderSkidsGrid.Refresh();

                lblTotalWeight.Text = "Total Weight: " + getSkidsWeight(order).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(" Skid weight error: " + ex.Message, "Weight read error");
            }
        }
        private void readLTLTItemWeight(Data.OrdersTableRow order, System.Decimal weight)
        {
            try
            {
                Data.OrderLTLItemsRow[] items = order.GetOrderLTLItemsRows();
                if (items.Length == 0)
                {
                    return;
                }
                DataRowView item_row = fkOrdersTableLTLItemsBindingSource.Current as DataRowView;
                Data.OrderLTLItemsRow item = item_row.Row as Data.OrderLTLItemsRow;
                if (item == null)
                {
                    MessageBox.Show("No LTL item is selected. Please select one LTL item to add the weight!", "Weight input");
                    return;
                }

                fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = false;
                fkOrdersTableLTLItemsBindingSource.SuspendBinding();

                //System.Decimal weight = 0;

                //ScaleIntegr8.MyScale scale = new ScaleIntegr8.MyScale();
                //weight = nascale.GetWeight("0EB8", "F000");

                if (data.OrderLTLItems.Columns.IndexOf("LTL Item Total Weight") > 0)
                {
                    item["LTL Item Total Weight"] = weight;
                }

                fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = true;
                fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(true);
                fkOrdersTableOrderSkidsTableBindingSource.ResumeBinding();

                //******************************************************
                //row["Package Weight"] = scale.GetWeight("0EB8", "F000");
                LTLItemsGrid.Refresh();

                lblTotalWeight.Text = "Total Weight: " + getLTLItemsWeight(order).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("LTLItem weight error: " + ex.Message, "Weight read error");
            }
        }

        private void btnManualOrders_Click(object sender, EventArgs e)
        {
            try
            {
                EndEdit();
                frmNewOrder newOrder = new frmNewOrder();
                DialogResult result = newOrder.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    foreach (DataGridViewRow row in OrdersGrid.Rows)
                    {
                        row.HeaderCell.Value = (row.Index + 1).ToString();
                    }
                    OrdersGrid.ClearSelection();
                    OrdersGrid.Rows[OrdersGrid.Rows.Count - 1].Selected = true;
                    OrdersTableBindingSource.Position = data.OrdersTable.Rows.Count - 1;
                    errors_orders = false;
                    manual_order = true;
                    not_manual = true;
                    OrdersEditCurrentButton_Click(sender, e);
                    if (errors_orders)
                    {
                        manual_order = false;
                        not_manual = false;
                    }
                }
                if (result == DialogResult.Cancel)
                {
                    manual_order = false;
                    not_manual = false;
                    edit_mod = false;
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error adding new order row", ex);
            }
        }

        private void NewOrderInGrid()
        {
            OrdersTableBindingSource.RaiseListChangedEvents = false;
            OrdersTableBindingSource.SuspendBinding();
            fKOrdersTableOrderProductsTableBindingSource.RaiseListChangedEvents = false;
            fKOrdersTableOrderProductsTableBindingSource.SuspendBinding();
            if (data.OrdersTable.Rows.Count > 0)
            {
                Data.OrdersTableRow order = (Data.OrdersTableRow)data.OrdersTable.Rows[data.OrdersTable.Rows.Count - 1];

                Data.Carriers_MappingRow carriersMapping = order.Carriers_MappingRow;
                Data.Billing_TypesRow billingTypes = order.Billing_TypesRow;
                Data.OrdersTableRow newOrder = data.OrdersTable.AddOrdersTableRow(order["Order #"].ToString() + " - new", order["Sender Country"].ToString(),
                                               order["Recipient Country"].ToString(), carriersMapping, order["Service"].ToString(), order["Packaging"].ToString(),
                                               order["Sender State / Province"].ToString(), order["Sender ZIP / Postal Code"].ToString(), "", "", DateTime.Today,
                                               "", false, billingTypes);
                if (data.OrdersTable.Columns.Contains("Sender Company"))
                {
                    newOrder["Sender Company"] = order["Sender Company"];
                }
                if (data.OrdersTable.Columns.Contains("Sender City"))
                {
                    newOrder["Sender City"] = order["Sender City"];
                }
                if (data.OrdersTable.Columns.Contains("Sender Address 1"))
                {
                    newOrder["Sender Address 1"] = order["Sender Address 1"];
                }
                if (data.OrdersTable.Columns.Contains("Sender Address 2"))
                {
                    newOrder["Sender Address 2"] = order["Sender Address 2"];
                }
                if (data.OrdersTable.Columns.Contains("Sender Tel"))
                {
                    newOrder["Sender Tel"] = order["Sender Tel"];
                }
                if (data.OrdersTable.Columns.Contains("Sender Contact"))
                {
                    newOrder["Sender Contact"] = order["Sender Contact"];
                }
                data.OrderProductsTable.AddOrderProductsTableRow(newOrder, "1", "", false, "", 1, "", 1, 1, 1, "", 1);
            }
            else
            {
                Data.Carriers_MappingRow carriersMapping = null;
                Data.Billing_TypesRow billingTypes = null;
                Data.OrdersTableRow newOrder = data.OrdersTable.AddOrdersTableRow(data.OrdersTable.Rows.Count + 1 + " - new", "",
                                              "", carriersMapping, "", "", "", "", "", "", DateTime.Today,
                                              "", false, billingTypes);

                data.OrderProductsTable.AddOrderProductsTableRow(newOrder, "1", "", false, "", 1, "", 1, 1, 1, "", 1);
            }
            OrdersTableBindingSource.RaiseListChangedEvents = true;
            OrdersTableBindingSource.ResetBindings(true);
            OrdersTableBindingSource.ResumeBinding();
            fKOrdersTableOrderProductsTableBindingSource.RaiseListChangedEvents = true;
            fKOrdersTableOrderProductsTableBindingSource.ResetBindings(true);
            fKOrdersTableOrderProductsTableBindingSource.ResumeBinding();
        }

        private void tbValue1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r') btnAcceptFilter_Click(sender, e);
        }

        public string Get_Packing_Slip(string filepath, string img_path, string OrderName, string OrderDate, string client, string po_id, string TrackingNo, string BillTo, string ShipTo, List<packing_list> list, string ShippingMethod, string SpecialInstructions, bool French)
        {
            iTextSharp.text.Document doc = null;
            PdfWriter writer = null;
            PdfPTable table = null;

            try
            {
                Decimal total = 0;
                doc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
                doc.SetMargins(50F, 70F, 60F, 40F);

                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);
                }
                else
                {
                    if (File.Exists(Path.Combine(filepath, OrderName + "_.pdf")))
                    {
                        try
                        {
                            Stream stream = new FileStream(Path.Combine(filepath, OrderName + "_.pdf"), FileMode.Open);
                            stream.Close();
                        }
                        catch (IOException)
                        {
                            //the file is unavailable because it is:
                            //still being written to
                            //or being processed by another thread
                            MessageBox.Show("PackSlip already open ! Please close it and try again.");
                            return "";
                        }
                        File.Delete(Path.Combine(filepath, OrderName + "_.pdf"));
                    }
                }
                writer = PdfWriter.GetInstance(doc, new FileStream(Path.Combine(filepath, OrderName + "_.pdf"), FileMode.Create));
                doc.Open();
                iTextSharp.text.Image img;

                if (img_path == null || img_path == "")
                {
                    MemoryStream ms = new MemoryStream();
                    if (Template.Name == "")
                    {
                        InteGr8.Properties.Resources.ortoped_logo.Save(ms, ImageFormat.Bmp);
                    }
                    else
                    {
                        InteGr8.Properties.Resources._2ship.Save(ms, ImageFormat.Bmp);
                    }
                    byte[] bitmapData = ms.ToArray();
                    img = iTextSharp.text.Image.GetInstance(bitmapData);
                }
                else
                {
                    img = iTextSharp.text.Image.GetInstance(img_path);
                }

                img.SetAbsolutePosition(60, doc.PageSize.Height - img.ScaledHeight - 68.29F);

                iTextSharp.text.Font font = iTextSharp.text.FontFactory.GetFont("Arial", 18, 1, new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#004269")));
                iTextSharp.text.Font font1 = iTextSharp.text.FontFactory.GetFont("Arial", 11, 0);
                iTextSharp.text.Font font2 = iTextSharp.text.FontFactory.GetFont("Arial", 10, 1, iTextSharp.text.BaseColor.WHITE);
                iTextSharp.text.Font font3 = iTextSharp.text.FontFactory.GetFont("Arial", 9);
                iTextSharp.text.Font font4 = iTextSharp.text.FontFactory.GetFont("Arial", 10, 1, iTextSharp.text.BaseColor.BLACK);

                PdfPCell cell = null;

                table = new PdfPTable(6);
                table.WidthPercentage = 100;

                if (Template.Name != "")
                {
                    cell = new PdfPCell(new Phrase(" "));
                    cell.Colspan = 6;
                    cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#004269"));
                    cell.BorderColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#004269"));
                    cell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                    table.AddCell(cell);
                }

                img.ScaleAbsolute(135F, 38F);
                cell = new PdfPCell(img);
                cell.FixedHeight = 40F;
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                cell.Colspan = 4;
                cell.HorizontalAlignment = 0;
                cell.VerticalAlignment = Element.ALIGN_BOTTOM;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(new Chunk(French ? "PACKING SLIP Bon d'emballage" : "PACKING SLIP", font)));
                cell.HorizontalAlignment = 1;
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(French ? " INC\r\n373 MCCAFFREY\r\nVILLE SAINT-LAURENT. QC H4T 1Z7" : "", font4));
                cell.Colspan = 3;
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Shipment ID: ", font4));
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.HorizontalAlignment = 1;
                cell.VerticalAlignment = Element.ALIGN_BOTTOM;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(OrderName, font1));
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.HorizontalAlignment = 1;
                cell.VerticalAlignment = Element.ALIGN_BOTTOM;
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(" "));
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                cell.Colspan = 6;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Date", font1));
                cell.BorderWidth = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(OrderDate, font1));
                cell.BorderWidth = 1;
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell();
                cell.Colspan = 3;
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Client", font1));
                cell.BorderWidth = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(client, font1));
                cell.BorderWidth = 1;
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell();
                cell.Colspan = 3;
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("PO #", font1));
                cell.BorderWidth = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(po_id, font1));
                cell.BorderWidth = 1;
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell();
                cell.Colspan = 3;
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Tracking #", font1));
                cell.BorderWidth = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(TrackingNo, font1));
                cell.BorderWidth = 1;
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell();
                cell.Colspan = 3;
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(" "));
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(" "));
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(new Chunk(French ? "Bill to/ Vendu à" : "Bill to", font4)));
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.BorderWidth = 1;
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(new Chunk(French ? "Ship to/ Expédier à" : "Ship to", font4)));
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.BorderWidth = 1;
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(BillTo, font3));
                cell.MinimumHeight = 60F;
                cell.Colspan = 3;
                cell.BorderWidth = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(ShipTo, font3));
                cell.MinimumHeight = 60F;
                cell.Colspan = 3;
                cell.BorderWidth = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(" "));
                cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
                cell.Border = iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                cell.Colspan = 6;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(new Chunk("Item #", font4)));
                cell.HorizontalAlignment = 1;
                cell.BorderWidth = 1;
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.Colspan = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(new Chunk("Description", font4)));
                cell.HorizontalAlignment = 1;
                cell.BorderWidth = 1;
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.Colspan = 3;
                table.AddCell(cell);

                if (French)
                {
                    cell = new PdfPCell(new Phrase(new Chunk("Expédié Shipped", font4)));
                    cell.HorizontalAlignment = 1;
                    cell.BorderWidth = 1;
                    cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(new Chunk("BO", font4)));
                    cell.HorizontalAlignment = 1;
                    cell.BorderWidth = 1;
                    cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    table.AddCell(cell);
                }
                else
                {
                    cell = new PdfPCell(new Phrase(new Chunk("Shipped", font4)));
                    cell.HorizontalAlignment = 1;
                    cell.BorderWidth = 1;
                    cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    cell.Colspan = 2;
                    table.AddCell(cell);
                }

                foreach (packing_list p in list)
                {
                    cell = new PdfPCell(new Phrase(p.styleID, font3));
                    cell.HorizontalAlignment = 1;
                    cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    cell.BorderWidth = 1;
                    cell.Colspan = 1;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(p.Title, font3));
                    cell.HorizontalAlignment = 1;
                    cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    cell.BorderWidth = 1;
                    cell.Colspan = 3;
                    table.AddCell(cell);

                    if (French)
                    {

                        cell = new PdfPCell(new Phrase(Math.Floor(Convert.ToDouble(p.Quantity.ToString())).ToString(), font3));
                        cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        cell.BorderWidth = 1;
                        cell.HorizontalAlignment = 1;
                        table.AddCell(cell);

                        cell = new PdfPCell(new Phrase(p.BO.ToString(), font3));
                        cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        cell.BorderWidth = 1;
                        cell.HorizontalAlignment = 1;
                        table.AddCell(cell);
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase(Convert.ToInt32(p.Quantity.ToString()).ToString(), font3));
                        cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        cell.BorderWidth = 1;
                        cell.HorizontalAlignment = 2;
                        cell.Colspan = 2;
                        table.AddCell(cell);
                    }

                    total += Convert.ToDecimal(p.Quantity == "" ? "0" : p.Quantity);
                }

                cell = new PdfPCell(new Phrase(" ", font3));
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.BorderWidth = 1;
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Total", iTextSharp.text.FontFactory.GetFont("Arial", 9, 1)));
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = 2;
                cell.BorderWidth = 1;
                cell.Colspan = 2;
                table.AddCell(cell);

                if (French)
                {
                    cell = new PdfPCell(new Phrase(total.ToString(), font3));
                    cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    cell.BorderWidth = 1;
                    cell.HorizontalAlignment = 1;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(" ", font3));
                    cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    cell.BorderWidth = 1;
                    table.AddCell(cell);
                }
                else
                {
                    cell = new PdfPCell(new Phrase(total.ToString(), font3));
                    cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    cell.BorderWidth = 1;
                    cell.HorizontalAlignment = 2;
                    cell.Colspan = 2;
                    table.AddCell(cell);
                }

                cell = new PdfPCell(new Phrase(" "));
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
                cell.BorderColorLeft = iTextSharp.text.BaseColor.WHITE;
                cell.Colspan = 6;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase((French ? "Expédier par/Ship via: " : "Ship via: ") + ShippingMethod, font3));
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = 1;
                cell.BorderWidth = 1;
                cell.Colspan = 6;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(" "));
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
                cell.BorderColorLeft = iTextSharp.text.BaseColor.WHITE;
                cell.Colspan = 6;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(new Chunk(French ? "Special Instructions/Des Instructions Spéciales" : "Special Instructions", font4)));
                cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                cell.BorderWidth = 1;
                cell.Colspan = 6;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(SpecialInstructions, font3));
                cell.MinimumHeight = 40F;
                cell.Colspan = 6;
                cell.BorderWidth = 1;
                table.AddCell(cell);

                doc.Add(table);
                doc.Close();
                writer.Close();
                return Path.Combine(filepath, OrderName + "_.pdf");
            }
            catch (Exception ex)
            {
                if (writer.PageNumber == 1)
                {
                    Paragraph p = new Paragraph("junk");
                    doc.Add(p);
                }
                doc.Close();
                writer.Close();
                Utils.SendError("Error generating the Packslip pdf", ex);
                MessageBox.Show("Error generating the Packslip pdf");
                return "";
            }
        }

        private void cmbFilterColumns_Format(object sender, ListControlConvertEventArgs e)
        {
            Data.TemplatesRow r = data.Templates.Rows[0] as Data.TemplatesRow;
            if (r.Custom_Filter)
            {
                Filter_Expression f = (Filter_Expression)filterExpressionBindingSource.Current;
                int i = r.Filter.IndexOf('@');
                int j = r.Filter.IndexOf(" ", i + 1);
                if (j == -1)
                {
                    j = r.Filter.IndexOf(")", i + 1);
                }
                e.Value = r.Filter.Substring(i, j - i);
            }
        }

        private void Browser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (Template.Name == "FineLineGraphics" || Template.Name == "MAAX")
            {
                //get edit results, after ship was pressed and the Ship edit page changed
                //if there is a null reply, do nothing
                //if the reply is not null, (the manual order was shipped) do GetEditResults
                //save to database the results
                //and go back to current orders screen and open the Add shipment pop -up so the next shipment id can be entered
                //and not if a client clicks edit and then getEditResults
                EndEdit();
                try
                {
                    List<InteGr8Task> tasks = new List<InteGr8Task>();
                    DataRowView order = OrdersTableBindingSource.Current as DataRowView;
                    if (order == null)
                    {
                        return;
                    }
                    tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.GetBackEditResults, order["Order #"].ToString()));
                    bool ok = false;

                    foreach (InteGr8Task t in tasks)
                    {
                        try
                        {
                            t.Process();
                            //the order was not shipped yet
                            if (t.reply == null)
                            {
                                continue;
                            }
                            ok = true;
                            t.Save();
                            foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                            {
                                if (shipment["Ship Date"].ToString() == "" || Convert.ToDateTime(shipment["Ship Date"].ToString()).ToShortDateString() == DateTime.Now.ToShortDateString())
                                {
                                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "Processed", shipment._Order__, data);
                                }
                            }
                            data.Complete_shipments();
                            foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
                            {
                                if (!shipment.Status.Equals("Error") && Convert.ToDateTime(shipment["Ship Date"].ToString()).ToShortDateString() == DateTime.Now.ToShortDateString())
                                {
                                    BackupFile.AddShipment(data.ShipmentsTable.TableName, shipment as System.Data.DataRow, data);
                                }
                            }
                            if (Template.UseAutoprint)
                            {
                                print_class print = new print_class();
                                print.order = t.order_number.ToString();
                                foreach (DataRow r in data.ShipmentsTable.Rows)
                                {
                                    if (r["Order #"].ToString().Equals(t.order_number.ToString()))
                                    {
                                        print.label = r["Label URL"].ToString();
                                        print.commercial_invoice = r["Commercial Invoice URL"].ToString();
                                        print.bol = r["BOL URL"].ToString();
                                        print.return_label = r["Return Label URL"].ToString();
                                        break;
                                    }
                                }
                                print_list.Add(print);
                                if (t == tasks[tasks.Count - 1])
                                {
                                    if (!bg_print.IsBusy)
                                    {
                                        bg_print.RunWorkerAsync();
                                    }
                                }
                                if (data.ShipmentsTable.Columns.IndexOf("OrderNumber") >= 0)
                                {
                                    if (HistoryGrid.Columns["OrderNumber"] != null)
                                    {
                                        HistoryGrid.Columns["OrderNumber"].Visible = true;
                                    }
                                }
                                //if autosave, then show shipment info in History tab, to identify the saved shipment and/or reprint the label, etc
                                foreach (DataGridViewColumn col in HistoryGrid.Columns)
                                {
                                    if (!col.Name.Equals("Token") && !col.Name.Equals("OrderNumber") && !col.Name.Contains("Sender") && !col.Name.Contains("Bill"))
                                    {
                                        col.Visible = true;
                                    }
                                }
                            }
                            if (Template.AutoSave_AtShipping)
                            {
                                if (!data.ShipmentsTable.Rows[0]["Status"].ToString().Equals("error"))
                                {
                                    if (progress.Created)
                                    {
                                        progress.Close_Me();
                                    }
                                    progress = new frmProcessing(TaskBackgroundWorker);
                                    string[] arguments = { "Save" };
                                    bgworker_save.RunWorkerAsync(arguments);
                                    progress.ShowProgress2("Saving Shipments");
                                    progress.ShowDialog(this);
                                    HistoryBindingSource.ResetBindings(true);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.SendError("Error processing task for order " + t.order_number, ex);
                        }
                    }
                    if (ok)
                    {
                        MainPages.SelectedTab = OrdersPage;
                        if (manual_order)
                        {
                            manual_order = false;
                            edit_mod = false;
                            not_manual = false;
                            btnManualOrders_Click(sender, e);
                        }
                        manual_order = false;
                        edit_mod = false;
                        not_manual = false;
                    }
                }
                catch (Exception ex)
                {
                    Utils.SendError("Error in GetEditResults from 2ship page", ex);
                    MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnScanProducts_Click(object sender, EventArgs e)
        {
            OrdersGrid_CellMouseDoubleClick(null, null);
        }

        private void OrdersGrid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (OrdersGrid.SelectedRows.Count > 0 && Template.Name == "" && !FillBackgroundWorker.IsBusy && FillProgress.Visible == false)
            {
                if (OrdersGrid.SelectedRows.Contains(OrdersGrid.Rows[0]))
                {
                    fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();
                    fKOrdersTableOrderProductsTableBindingSource.SuspendBinding();
                    foreach (DataGridViewRow r in OrderPackagesGrid.Rows)
                    {
                        if (r.Visible == false)
                        {
                            r.Visible = true;
                        }
                    }
                    foreach (DataGridViewRow r in ProductsGrid.Rows)
                    {
                        if (r.Visible == false)
                        {
                            r.Visible = true;
                        }
                    }
                    fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();
                    fKOrdersTableOrderProductsTableBindingSource.ResumeBinding();
                }
                DataRowView order_r = OrdersTableBindingSource.Current as DataRowView;
                if (order_r == null)
                {
                    return;
                }

                object order_number = order_r.Row["Order #"];
                if (order_number == null || order_number.Equals(""))
                {
                    return;
                }

                p = new ProductsScan(order_number.ToString(), fKOrdersTableOrderProductsTableBindingSource);
                p.refresh_row = new ProductsScan.row_state(refresh_products_rows);
                p.ShowDialog();
                SetRowNumbers();
            }
        }

        public void refresh_products_rows()
        {
            try
            {
                if (Template.Name == "")
                {
                    refresh_orders_rows();
                    foreach (DataGridViewRow row in ProductsGrid.Rows)
                    {
                        if (Convert.ToDecimal(row.Cells["AllocatedQuantity"].Value.ToString().Trim()) == Convert.ToDecimal(row.Cells["Scanned Items"].Value.ToString().Trim()))
                        {
                            row.DefaultCellStyle.BackColor = Color.LimeGreen;
                            row.DefaultCellStyle.SelectionForeColor = Color.Yellow;
                        }

                    }
                }
            }
            catch (Exception e)
            {
                //ignore
            }
        }

        public void refresh_orders_rows()
        {
            foreach (DataGridViewRow r in OrdersGrid.Rows)
            {
                Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(r.Cells["txtOrderNumber"].Value.ToString().TrimEnd());
                if (order == null)
                {
                    return;
                }
                Data.OrderProductsTableRow[] products = order.GetOrderProductsTableRows();
                foreach (Data.OrderProductsTableRow p in products)
                {
                    if (p["Scanned Items"] == null || p["Scanned Items"].ToString() == "")
                    {
                        p["Scanned Items"] = 0;
                        p["AllocatedQuantity"] = Math.Floor(Convert.ToDecimal(p["AllocatedQuantity"]));
                        p["Product Quantity"] = Math.Floor(Convert.ToDecimal(p["Product Quantity"]));
                    }
                }
            }
            foreach (DataGridViewRow r in OrdersGrid.Rows)
            {
                Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(r.Cells["txtOrderNumber"].Value.ToString().TrimEnd());
                Data.OrderProductsTableRow[] products = order.GetOrderProductsTableRows();
                bool valid = true;
                foreach (Data.OrderProductsTableRow p in products)
                {
                    if (Convert.ToDecimal(p["AllocatedQuantity"].ToString().Trim()) != Convert.ToDecimal(p["Scanned Items"].ToString().Trim()))
                    {
                        valid = false;
                        break;
                    }
                }
                if (valid)
                {
                    if (r.DefaultCellStyle.BackColor != Color.Red)
                    {
                        r.DefaultCellStyle.BackColor = Color.LimeGreen;
                        System.Diagnostics.Trace.WriteLine("so modificat un rand din order");
                    }
                    r.DefaultCellStyle.SelectionForeColor = Color.Yellow;
                }
                else
                {
                    if (r.DefaultCellStyle.BackColor != Color.Red)
                    {
                        r.DefaultCellStyle.BackColor = Color.White;
                    }
                    r.DefaultCellStyle.SelectionForeColor = Color.White;
                }
            }
        }

        private void btn_select_all_Click(object sender, EventArgs e)
        {
            foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
            {
                shipment.Selected = true;
            }
        }

        private void btn_unselect_all_Click(object sender, EventArgs e)
        {
            foreach (Data.ShipmentsTableRow shipment in data.ShipmentsTable)
            {
                shipment.Selected = false;
            }
        }

        private void btn_selectall_Click(object sender, EventArgs e)
        {
            HistoryGrid.EndEdit();
            foreach (DataGridViewRow r in HistoryGrid.Rows)
            {
                r.Cells[0].Value = true;
            }
        }

        private void btn_unselectall_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in HistoryGrid.Rows)
            {
                r.Cells[0].Value = false;
            }
        }

        private void btn_shipments_packslip_Click(object sender, EventArgs e)
        {
            DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
            if (order == null)
            {
                MessageBox.Show("There is no order selected");
                return;
            }
            DataRowView shipment_row = shipmentsTableBindingSource.Current as DataRowView;
            Data.ShipmentsTableRow shipment = shipment_row.Row as Data.ShipmentsTableRow;
            string path = PackSlip(shipment);
            if (path != "")
            {
                if (path == "Can't generate packslip !")
                {
                    MessageBox.Show(path);
                }
                else
                {
                    Process.Start(path);
                }
            }
        }

        private string PackSlip(Data.ShipmentsTableRow shipment)
        {
            //get the info from shipments table
            if (shipmentsTableBindingSource.Position < 0)
            {
                MessageBox.Show("There is no shipment selected!", "Attention", MessageBoxButtons.OK);
                return "";
            }

            if (shipment == null)
            {
                return "";
            }
            object order_number = shipment["Order #"];
            if (order_number == null || order_number.Equals(""))
            {
                return "";
            }

            Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_number.ToString().Split('.')[0]);
            if (order == null)
            {
                MessageBox.Show("The selected shipment is no longer in Orders list, so the Packslip cannot be generated", "Warning", MessageBoxButtons.OK);
                return "Can't generate packslip, there is no order selected!";
            }
           
            string client;
            if (!data.OrdersTable.Columns.Contains("Client ID"))
            {
                client = order["Recipient Contact"].ToString();
            }
            else
            {
                client = order["Client ID"].ToString();
            }
            string po_id = order["Shipment PO Number"].ToString();

           

            Data.OrderProductsTableRow[] products = order.GetOrderProductsTableRows();
            List<packing_list> products_desc = new List<packing_list>();
            packing_list current_prod = new packing_list();
            if (products.Length > 0)
            {
                foreach (Data.OrderProductsTableRow p in products)
                {
                    current_prod = new packing_list();
                    if (Template.Name == "")
                    {
                        int k = 0;
                        current_prod.styleID = p["ItemNumber"].ToString();
                        Data.ProductsSerialNumbersRow[] rows_serial = InteGr8_Main.data.ProductsSerialNumbers.Select("[Order #] = '" + order_number.ToString().TrimEnd() + "'") as Data.ProductsSerialNumbersRow[];
                        if (rows_serial.Length != 0)
                        {
                            current_prod.Title = p["Product Description"].ToString() + "\n";
                            if (data.OrderProductsTable.Columns.IndexOf("Product_Instruction") > 0)
                            {
                                current_prod.Title += p["Product_Instruction"].ToString().TrimEnd() + "\n";
                            }
                            DataRow[] rows = InteGr8_Main.data.ProductsSerialNumbers.Select("[Product #] = '" + p["Product #"].ToString().TrimEnd() + "'");
                            k = rows.Length;
                            foreach (DataRow r in rows)
                            {
                                if (r["Serial Number"].ToString() != "")
                                {
                                    current_prod.Title += r["Serial Number"] + "\n";
                                }
                            }
                        }
                        else
                        {
                            current_prod.Title = p["Product Description"].ToString();
                            if (data.OrderProductsTable.Columns.IndexOf("Product_Instruction") > 0)
                            {
                                current_prod.Title += "\n" + p["Product_Instruction"].ToString().TrimEnd();
                            }
                        }

                        if (k != 0)
                        {
                            int backorder = Convert.ToInt32(p["BackOrder"].ToString().TrimEnd());
                            current_prod.BO = ((backorder - k) > 0 ? backorder - k : 0).ToString();
                            current_prod.Quantity = (k).ToString();
                        }
                        else
                        {
                            current_prod.BO = p["BackOrder"].ToString();
                            current_prod.Quantity = Convert.ToDouble(p["Scanned Items"].ToString()).ToString();
                        }
                    }
                    else
                    {
                        current_prod.styleID = p["Product #"].ToString();
                        current_prod.Title = p["Product Description"].ToString();
                        current_prod.Quantity = Convert.ToDouble(p["Product Quantity"].ToString()).ToString();
                    }
                    products_desc.Add(current_prod);
                }
            }
            else
            {
                current_prod.styleID = "";
                current_prod.Title = "";
                current_prod.BO = "";
                current_prod.Quantity = "";
                products_desc.Add(current_prod);
            }

            //logging****************************************************************************************************************
            using (StreamWriter writer = File.AppendText("RequestTest.txt"))
            {
                writer.WriteLine("*******************************************");
                foreach (packing_list p in products_desc)
                {
                    writer.WriteLine("Products, from Shipments tab:");
                    writer.WriteLine("Product # = " + p.styleID + " ; Description = " + p.Title + " ; BO = " + p.BO + " ; Quantity = " + p.Quantity);
                }
                writer.WriteLine("*******************************************");
            }
            //*************************************************************************************************************************

            string BillAddress = "";
            string RecAddress = "";
            RecAddress = ((data.ShipmentsTable.Columns.IndexOf("Recipient Company") >= 0) ? shipment["Recipient Company"].ToString().TrimEnd() : (data.OrdersTable.Columns.IndexOf("Recipient Company") >= 0 ? order["Recipient Company"].ToString().TrimEnd() : "")) + "\n" +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient Address 1") >= 0) ? shipment["Recipient Address 1"].ToString().TrimEnd() : order["Recipient Address 1"].ToString().TrimEnd()) + " " +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient Address 2") >= 0) ? shipment["Recipient Address 2"].ToString().TrimEnd() : order["Recipient Address 2"].ToString().TrimEnd()) + "\n" +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient City") >= 0) ? shipment["Recipient City"].ToString().TrimEnd() : order["Recipient City"].ToString().TrimEnd()) + ", " +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient State / Province") >= 0) ? shipment["Recipient State / Province"].ToString().TrimEnd() : order["Recipient State / Province"].ToString().TrimEnd()) + ", " +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient ZIP / Postal Code") >= 0) ? shipment["Recipient ZIP / Postal Code"].ToString().TrimEnd() : order["Recipient ZIP / Postal Code"].ToString().TrimEnd()) + ", " +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient Company") >= 0) ? shipment["Recipient Country"].ToString() : order["Recipient Country"].ToString()) + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Recipient Tel") >= 0) ? (String.IsNullOrEmpty(shipment["Recipient Tel"].ToString()) ? "" : "Phone: " + shipment["Recipient Tel"].ToString().TrimEnd()) + "\n" : "") +
                        ((data.ShipmentsTable.Columns.IndexOf("Recipient Email") >= 0) ? (String.IsNullOrEmpty(shipment["Recipient Email"].ToString()) ? "" : "Email: " + shipment["Recipient Email"].ToString().TrimEnd()) + " " : "");

            string billType = (data.ShipmentsTable.Columns.IndexOf("Billing Type") >= 0) ? shipment["Billing Type"].ToString().TrimEnd() : order["Billing Type"].ToString().TrimEnd();

            if (Template.Name == "")
            {
                BillAddress = ((data.ShipmentsTable.Columns.IndexOf("Billing Company") >= 0) ? shipment["Billing Company"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing Company") >= 0 ? order["Billing Company"].ToString().TrimEnd() : "") + " ") + "\n" +
                    ((data.ShipmentsTable.Columns.IndexOf("Billing Address 1") >= 0) ? shipment["Billing Address 1"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing Address 1") >= 0 ? order["Billing Address 1"].ToString().TrimEnd() : "") + " ") +
                    ((data.ShipmentsTable.Columns.IndexOf("Billing Address 2") >= 0) ? shipment["Billing Address 2"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing Address 2") >= 0 ? order["Billing Address 2"].ToString().TrimEnd() : "") + " ") + "\n" +
                    ((data.ShipmentsTable.Columns.IndexOf("Billing City") >= 0) ? shipment["Billing City"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing City") >= 0 ? order["Billing City"].ToString().TrimEnd() : "")) + ", " +
                    ((data.ShipmentsTable.Columns.IndexOf("Billing State") >= 0) ? shipment["Billing State"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing State") >= 0 ? order["Billing State"].ToString().TrimEnd() : "")) + ", " +
                    ((data.ShipmentsTable.Columns.IndexOf("Billing ZIP") >= 0) ? shipment["Billing ZIP"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing ZIP") >= 0 ? order["Billing ZIP"].ToString().TrimEnd() : "")) + ", " +
                    ((data.ShipmentsTable.Columns.IndexOf("Billing Country") >= 0) ? shipment["Billing Country"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing Country") >= 0 ? order["Billing Country"].ToString().TrimEnd() : "")) + "\n" +
                    ((data.ShipmentsTable.Columns.IndexOf("Billing Tel") >= 0) ? (String.IsNullOrEmpty(shipment["Billing Tel"].ToString()) ? "" : "Phone: " + shipment["Billing Tel"].ToString().TrimEnd()) + "\n" : "") +
                    ((data.ShipmentsTable.Columns.IndexOf("Billing Email") >= 0) ? (String.IsNullOrEmpty(shipment["Billing Email"].ToString()) ? "" : "Email: " + shipment["Billing Email"].ToString().TrimEnd() + " ") : "");
            }
            else
            {
                if (billType == "3")
                {
                    BillAddress = ((data.ShipmentsTable.Columns.IndexOf("Billing Company") >= 0) ? shipment["Billing Company"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing Company") >= 0 ? order["Billing Company"].ToString().TrimEnd() : "") + " ") + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Address 1") >= 0) ? shipment["Billing Address 1"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing Address 1") >= 0 ? order["Billing Address 1"].ToString().TrimEnd() : "") + " ") +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Address 2") >= 0) ? shipment["Billing Address 2"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing Address 2") >= 0 ? order["Billing Address 2"].ToString().TrimEnd() : "") + " ") + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing City") >= 0) ? shipment["Billing City"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing City") >= 0 ? order["Billing City"].ToString().TrimEnd() : "")) + ", " +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing State") >= 0) ? shipment["Billing State"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing State") >= 0 ? order["Billing State"].ToString().TrimEnd() : "")) + ", " +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing ZIP") >= 0) ? shipment["Billing ZIP"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing ZIP") >= 0 ? order["Billing ZIP"].ToString().TrimEnd() : "")) + ", " +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Country") >= 0) ? shipment["Billing Country"].ToString().TrimEnd() + " " : (data.OrdersTable.Columns.IndexOf("Billing Country") >= 0 ? order["Billing Country"].ToString().TrimEnd() : "")) + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Tel") >= 0) ? (String.IsNullOrEmpty(shipment["Billing Tel"].ToString()) ? "" : "Phone: " + shipment["Billing Tel"].ToString().TrimEnd()) + "\n" : "") +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Email") >= 0) ? (String.IsNullOrEmpty(shipment["Billing Email"].ToString()) ? "" : "Email: " + shipment["Billing Email"].ToString().TrimEnd() + " ") : "");
                }
                else
                {
                    if (billType == "2")
                    {
                        BillAddress = RecAddress;
                    }
                    else
                    {
                        BillAddress = ((data.ShipmentsTable.Columns.IndexOf("Sender Company") >= 0) ? shipment["Sender Company"].ToString().TrimEnd() : (data.OrdersTable.Columns.IndexOf("Sender Company") >= 0 ? order["Sender Company"].ToString().TrimEnd() : "")) + "\n" +
                            ((data.ShipmentsTable.Columns.IndexOf("Sender Address 1") >= 0) ? shipment["Sender Address 1"].ToString().TrimEnd() : (data.OrdersTable.Columns.IndexOf("Sender Address 1") >= 0 ? order["Sender Address 1"].ToString().TrimEnd() : "")) + " " +
                            ((data.ShipmentsTable.Columns.IndexOf("Sender Address 2") >= 0) ? shipment["Sender Address 2"].ToString().TrimEnd() : (data.OrdersTable.Columns.IndexOf("Sender Address 2") >= 0 ? order["Sender Address 2"].ToString().TrimEnd() : "")) + "\n" +
                            ((data.ShipmentsTable.Columns.IndexOf("Sender City") >= 0) ? shipment["Sender City"].ToString().TrimEnd() : (data.OrdersTable.Columns.IndexOf("Sender City") >= 0 ? order["Sender City"].ToString().TrimEnd() : "")) + ", " +
                            ((data.ShipmentsTable.Columns.IndexOf("Sender State / Province") >= 0) ? shipment["Sender State / Province"].ToString().TrimEnd() : (data.OrdersTable.Columns.IndexOf("Sender State / Province") >= 0 ? order["Sender State / Province"].ToString().TrimEnd() : "")) + ", " +
                            ((data.ShipmentsTable.Columns.IndexOf("Sender ZIP / Postal Code") >= 0) ? shipment["Sender ZIP / Postal Code"].ToString().TrimEnd() : (data.OrdersTable.Columns.IndexOf("Sender ZIP / Postal Code") >= 0 ? order["Sender ZIP / Postal Code"].ToString().TrimEnd() : "")) + ", " +
                            ((data.ShipmentsTable.Columns.IndexOf("Sender Country") >= 0) ? shipment["Sender Country"].ToString().TrimEnd() : (data.OrdersTable.Columns.IndexOf("Sender Country") >= 0 ? order["Sender Country"].ToString().TrimEnd() : "")) + "\n" +
                            ((data.ShipmentsTable.Columns.IndexOf("Sender Tel") >= 0) ? (String.IsNullOrEmpty(shipment["Sender Tel"].ToString().TrimEnd()) ? "" : "Phone: " + shipment["Sender Tel"].ToString()) + "\n" : (data.OrdersTable.Columns.IndexOf("Sender Email") >= 0 ? ((String.IsNullOrEmpty(order["Sender Tel"].ToString()) ? "" : "Phone: " + order["Sender Tel"].ToString().TrimEnd()) + "\n") : "")) +
                            ((data.ShipmentsTable.Columns.IndexOf("Sender Email") >= 0) ? (String.IsNullOrEmpty(shipment["Sender Email"].ToString().TrimEnd()) ? "" : "Email: " + shipment["Sender Email"].ToString()) + " " : (data.OrdersTable.Columns.IndexOf("Sender Email") >= 0 ? ((String.IsNullOrEmpty(order["Sender Email"].ToString()) ? "" : "Email: " + order["Sender Email"].ToString().TrimEnd()) + " ") : ""));
                    }
                }
            }

            string date = "";
            try
            {
                date = (data.ShipmentsTable.Columns.IndexOf("Ship Date") >= 0 ? Convert.ToDateTime(shipment["Ship Date"].ToString().TrimEnd()).ToShortDateString() : Convert.ToDateTime(order["Ship Date"].ToString().TrimEnd()).ToShortDateString());
            }
            catch
            {
                date = DateTime.Now.ToShortDateString();
            }

            string path = Get_Packing_Slip(String.IsNullOrEmpty(Template.AutoprintPackSlipFolder) ? Settings.Default.PackingSlipPath : Template.AutoprintPackSlipFolder, null, shipment["Order #"].ToString().TrimEnd(),
                date, client, po_id,
                shipment["Shipment Tracking Number"].ToString(), BillAddress, RecAddress, products_desc,
                (data.ShipmentsTable.Columns.IndexOf("Carrier Name") >= 0 ? shipment["Carrier Name"].ToString().TrimEnd() : String.IsNullOrEmpty(order["Carrier"].ToString().TrimEnd()) ? "" : order["Carrier"].ToString().TrimEnd()) + " - " +
                (data.ShipmentsTable.Columns.IndexOf("Service Name") >= 0 ? shipment["Service Name"].ToString().TrimEnd() : String.IsNullOrEmpty(order["Service"].ToString().TrimEnd()) ? "" : order["Service"].ToString().TrimEnd()), (data.OrdersTable.Columns.IndexOf("Order_Instruction") > 0) ? order["Order_Instruction"].ToString().TrimEnd() : "", Template.Name == "");

            return path;
        }

        private void btn_history_packslip_Click(object sender, EventArgs e)
        {
            DataGridViewRow historyRow = HistoryGrid.CurrentRow;
            if (historyRow == null)
            {
                MessageBox.Show("There is no row selected");
                return;
            }
            //get the info from shipments table
            DataGridViewRow Row = HistoryGrid.SelectedRows[0];
            string order_number = Row.Cells[1].Value.ToString();
            if (order_number == null || order_number.Equals(""))
            {
                return;
            }
            string TN = String.IsNullOrEmpty(Row.Cells["HistoryTN"].Value.ToString()) ? Row.Cells["ProNumber"].Value.ToString() : Row.Cells["HistoryTN"].Value.ToString();
            string ShipDate = "";
            try
            {
                ShipDate = Convert.ToDateTime(Row.Cells["Ship Date"].Value.ToString()).ToShortDateString();
            }
            catch
            {
                ShipDate = DateTime.Now.ToShortDateString();
            }
            string CarrierAndService = Row.Cells["Carrier"].Value.ToString() + " - " + Row.Cells["Service"].Value.ToString();
            Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_number.ToString());
            if (order == null)
            {
                MessageBox.Show("The selected shipment is no longer in Orders list, so the Packslip cannot be generated", "Warning", MessageBoxButtons.OK);
                return;
            }

            string client;
            if (!data.OrdersTable.Columns.Contains("Client ID"))
            {
                client = order["Recipient Contact"].ToString();
            }
            else
            {
                client = order["Client ID"].ToString();
            }
            string po_id = order["Shipment PO Number"].ToString();

            Data.OrderProductsTableRow[] products = order.GetOrderProductsTableRows();
            List<packing_list> products_desc = new List<packing_list>();
            packing_list current_prod = new packing_list();
            if (products.Length > 0)
            {
                foreach (Data.OrderProductsTableRow p in products)
                {
                    current_prod = new packing_list();
                    if (Template.Name == "")
                    {
                        current_prod.styleID = p["ItemNumber"].ToString();
                        if (p["Scanned Items"].ToString() != null && p["Scanned Items"].ToString() != "" && Convert.ToDouble(p["Scanned Items"].ToString()) == Convert.ToDouble(p["Product Quantity"].ToString()))
                        {
                            current_prod.Title = p["Product Description"].ToString() + "\n";
                            if (data.OrderProductsTable.Columns.IndexOf("Product_Instruction") > 0)
                            {
                                current_prod.Title += p["Product_Instruction"].ToString().TrimEnd() + "\n";
                            }
                            DataRow[] rows = InteGr8_Main.data.ProductsSerialNumbers.Select("[Product #] = " + p["Product #"]);
                            foreach (DataRow r in rows)
                            {
                                if (r["Serial Number"].ToString() != "")
                                {
                                    current_prod.Title += r["Serial Number"] + "\n";
                                }
                            }
                        }
                        else
                        {
                            current_prod.Title = p["Product Description"].ToString();
                            if (data.OrderProductsTable.Columns.IndexOf("Product_Instruction") > 0)
                            {
                                current_prod.Title += "\n" + p["Product_Instruction"].ToString().TrimEnd();
                            }
                        }
                        current_prod.BO = p["BackOrder"].ToString();
                        current_prod.Quantity = Convert.ToDouble(p["Scanned Items"].ToString()).ToString();
                    }
                    else
                    {
                        current_prod.styleID = p["Product #"].ToString();
                        current_prod.Title = p["Product Description"].ToString();
                        current_prod.Quantity = Convert.ToDouble(p["Product Quantity"].ToString()).ToString();
                    }
                    products_desc.Add(current_prod);
                }
            }
            else
            {
                current_prod.styleID = "";
                current_prod.Title = "";
                current_prod.BO = "";
                current_prod.Quantity = "";
                products_desc.Add(current_prod);
            }

            using (StreamWriter writer = File.AppendText("RequestTest.txt"))
            {
                writer.WriteLine("******************************************");
                foreach (packing_list p in products_desc)
                {
                    writer.WriteLine("Products:");
                    writer.WriteLine("Product # = " + p.styleID + " ; Description = " + p.Title + " ; BO = " + p.BO + " ; Quantity = " + p.Quantity);
                }
                writer.WriteLine("******************************************");
            }

            string BillAddress = "";
            string RecAddress = "";
            RecAddress = Row.Cells["RecipientCompany"].Value.ToString().TrimEnd() + "\n" +
                        Row.Cells["RecipientAddress1"].Value.ToString().TrimEnd() + " " +
                        Row.Cells["RecipientAddress2"].Value.ToString().TrimEnd() + "\n" +
                        Row.Cells["RecipientCity"].Value.ToString().TrimEnd() + ", " +
                        Row.Cells["RecipientState"].Value.ToString().TrimEnd() + ", " +
                        Row.Cells["RecipientZIP"].Value.ToString().TrimEnd() + ", " +
                        Row.Cells["RecipientCountry"].Value.ToString().TrimEnd() + "\n" +
                        (((data.ShipmentsTable.Columns.IndexOf("Recipient Tel") >= 0) && (HistoryGrid.Columns.Contains("RecipientTel"))) ? (String.IsNullOrEmpty(Row.Cells["Recipient Tel"].Value.ToString()) ? "" : "Phone: " + Row.Cells["Recipient Tel"].Value.ToString().TrimEnd()) + "\n" : "") +
                        (((data.ShipmentsTable.Columns.IndexOf("Recipient Email") >= 0) && (HistoryGrid.Columns.Contains("RecipientEmail"))) ? (String.IsNullOrEmpty(Row.Cells["Recipient Email"].Value.ToString()) ? "" : "Email: " + Row.Cells["Recipient Email"].Value.ToString().TrimEnd()) + " " : "");

            if (Template.Name == "")
            {
                BillAddress = ((data.ShipmentsTable.Columns.IndexOf("Billing Company") >= 0) ? Row.Cells["Billing Company"].Value.ToString().TrimEnd() + " " : "") + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Address 1") >= 0) ? Row.Cells["Billing Address 1"].Value.ToString().TrimEnd() + " " : "") +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Address 2") >= 0) ? Row.Cells["Billing Address 2"].Value.ToString().TrimEnd() + " " : "") + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing City") >= 0) ? Row.Cells["Billing City"].Value.ToString().TrimEnd() + " " : "") + ", " +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing State") >= 0) ? Row.Cells["Billing State"].Value.ToString().TrimEnd() + " " : "") + ", " +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing ZIP") >= 0) ? Row.Cells["Billing ZIP"].Value.ToString().TrimEnd() + " " : "") + ", " +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Country") >= 0) ? Row.Cells["Billing Country"].Value.ToString().TrimEnd() + " " : "") + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Tel") >= 0) ? (String.IsNullOrEmpty(Row.Cells["Billing Tel"].Value.ToString()) ? "" : "Phone: " + Row.Cells["Billing Tel"].Value.ToString().TrimEnd()) + "\n" : "") +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Email") >= 0) ? (String.IsNullOrEmpty(Row.Cells["Billing Email"].Value.ToString()) ? "" : "Email: " + Row.Cells["Billing Email"].Value.ToString().TrimEnd() + " ") : "");
            }
            else
            {
                if (Row.Cells["Billing type"].Value.ToString() == "3")
                {
                    BillAddress = ((data.ShipmentsTable.Columns.IndexOf("Billing Company") >= 0) ? Row.Cells["Billing Company"].Value.ToString().TrimEnd() + " " : "") + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Address 1") >= 0) ? Row.Cells["Billing Address 1"].Value.ToString().TrimEnd() + " " : "") +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Address 2") >= 0) ? Row.Cells["Billing Address 2"].Value.ToString().TrimEnd() + " " : "") + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing City") >= 0) ? Row.Cells["Billing City"].Value.ToString().TrimEnd() + " " : "") + ", " +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing State") >= 0) ? Row.Cells["Billing State"].Value.ToString().TrimEnd() + " " : "") + ", " +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing ZIP") >= 0) ? Row.Cells["Billing ZIP"].Value.ToString().TrimEnd() + " " : "") + ", " +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Country") >= 0) ? Row.Cells["Billing Country"].Value.ToString().TrimEnd() + " " : "") + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Tel") >= 0) ? (String.IsNullOrEmpty(Row.Cells["Billing Tel"].Value.ToString()) ? "" : "Phone: " + Row.Cells["Billing Tel"].Value.ToString().TrimEnd()) + "\n" : "") +
                        ((data.ShipmentsTable.Columns.IndexOf("Billing Email") >= 0) ? (String.IsNullOrEmpty(Row.Cells["Billing Email"].Value.ToString()) ? "" : "Email: " + Row.Cells["Billing Email"].Value.ToString().TrimEnd() + " ") : "");
                }
                else
                {
                    if (Row.Cells["Billing type"].Value.ToString() == "2")
                    {
                        BillAddress = RecAddress;
                    }
                    else
                    {
                        BillAddress = Row.Cells["Sender Company"].Value.ToString().TrimEnd() + "\n" +
                            Row.Cells["Sender Address 1"].Value.ToString().TrimEnd() + " " +
                            Row.Cells["Sender Address 2"].Value.ToString().TrimEnd() + "\n" +
                            Row.Cells["Sender City"].Value.ToString().TrimEnd() + ", " +
                            Row.Cells["Sender State / Province"].Value.ToString().TrimEnd() + ", " +
                            Row.Cells["Sender ZIP / Postal Code"].Value.ToString().TrimEnd() + ", " +
                            Row.Cells["Sender Country"].Value.ToString().TrimEnd() + "\n" +
                            ((HistoryGrid.Columns.Contains("Sender Tel")) ? (String.IsNullOrEmpty(Row.Cells["Sender Tel"].Value.ToString()) ? "" : "Phone: " + Row.Cells["Sender Tel"].Value.ToString().TrimEnd()) + "\n" : "") +
                            ((HistoryGrid.Columns.Contains("Sender Email")) ? (String.IsNullOrEmpty(Row.Cells["Sender Email"].Value.ToString()) ? "" : "Email: " + Row.Cells["Sender Email"].Value.ToString().TrimEnd()) + " " : "");

                    }
                }
            }
            string path = Get_Packing_Slip(String.IsNullOrEmpty(Template.AutoprintPackSlipFolder) ? Settings.Default.PackingSlipPath : Template.AutoprintPackSlipFolder, null, order_number.Split('.')[0], ShipDate, client, po_id, TN, BillAddress, RecAddress, products_desc, CarrierAndService, "", Template.Name == "");
            Process.Start(path);
        }

        public List<string> Contents_Label(string order)
        {
            Data.OrdersTableRow order_row = data.OrdersTable.FindBy_Order__(order);
            List<string> contents_labels = new List<string>();
            Data.ShipmentsTableRow shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order, 0);

            string RecAddress = ((data.ShipmentsTable.Columns.IndexOf("Recipient Company") >= 0) ? shipment["Recipient Company"].ToString().TrimEnd() : (data.OrdersTable.Columns.IndexOf("Recipient Company") >= 0 ? order_row["Recipient Company"].ToString().TrimEnd() : "")) + "\n" +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient Address 1") >= 0) ? shipment["Recipient Address 1"].ToString().TrimEnd() : order_row["Recipient Address 1"].ToString().TrimEnd()) + " " +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient Address 2") >= 0) ? shipment["Recipient Address 2"].ToString().TrimEnd() : order_row["Recipient Address 2"].ToString().TrimEnd()) + "\n" +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient City") >= 0) ? shipment["Recipient City"].ToString().TrimEnd() : order_row["Recipient City"].ToString().TrimEnd()) + ", " +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient State / Province") >= 0) ? shipment["Recipient State / Province"].ToString().TrimEnd() : order_row["Recipient State / Province"].ToString().TrimEnd()) + ", " +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient ZIP / Postal Code") >= 0) ? shipment["Recipient ZIP / Postal Code"].ToString().TrimEnd() : order_row["Recipient ZIP / Postal Code"].ToString().TrimEnd()) + ", " +
                    ((data.ShipmentsTable.Columns.IndexOf("Recipient Company") >= 0) ? shipment["Recipient Country"].ToString().TrimEnd() : order_row["Recipient Country"].ToString().TrimEnd()) + "\n" +
                        ((data.ShipmentsTable.Columns.IndexOf("Recipient Tel") >= 0) ? (String.IsNullOrEmpty(shipment["Recipient Tel"].ToString().TrimEnd()) ? "" : "Phone: " + shipment["Recipient Tel"].ToString().TrimEnd()) + "\n" : "") +
                        ((data.ShipmentsTable.Columns.IndexOf("Recipient Email") >= 0) ? (String.IsNullOrEmpty(shipment["Recipient Email"].ToString().TrimEnd()) ? "" : "Email: " + shipment["Recipient Email"].ToString().TrimEnd()) + " " : "");

            Data.OrderPackagesTableRow[] pakages_row = data.OrderPackagesTable.Select("[Order #] = '" + order + "'") as Data.OrderPackagesTableRow[];
            DataView view = new DataView(data.ProductsInPackages);
            DataTable distinctValues = view.ToTable(true, "Package #", "Order #");
            int prodinpackages = 0;
            prodinpackages = distinctValues.Select("[Order #] = '" + order + "'").Count();
            while (prodinpackages > pakages_row.Length)
            {
                data.OrderPackagesTable.AddOrderPackagesTableRow(order_row, (order_row.GetOrderPackagesTableRows().Count() + 1).ToString(), 0, "LBS", 0, 0, 0, "I", 0, "CAD", "");
                pakages_row = data.OrderPackagesTable.Select("[Order #] = '" + order + "'") as Data.OrderPackagesTableRow[];
            }
            foreach (Data.OrderPackagesTableRow p in pakages_row)
            {
                contents_labels.Add(Get_Contents_Label(String.IsNullOrEmpty(Template.AutoprintContentsLabelFolder) ? Settings.Default.PackingSlipPath : Template.AutoprintContentsLabelFolder, p._Package__, RecAddress, order_row["Shipment PO Number"].ToString(), order));
            }

            return contents_labels;
        }

        public string Get_Contents_Label(string filepath, string carton, string recaddress, string po, string order)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Path.Combine(filepath, order + "_Carton_" + carton + ".pdf"), FileMode.Create));
            doc.Open();

            iTextSharp.text.Font font = iTextSharp.text.FontFactory.GetFont("Arial", 18, 1, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font font1 = iTextSharp.text.FontFactory.GetFont("Arial", 14, 1, iTextSharp.text.BaseColor.BLACK);

            PdfPTable table = null;
            PdfPCell cell = null;

            table = new PdfPTable(8);
            table.WidthPercentage = 100;

            cell = new PdfPCell(new Phrase("From/De", font));
            cell.Colspan = 8;
            cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("\n373 McCaffrey\nST-LAURENT, QC\nH4T 1Z7", font1));
            cell.Colspan = 4;
            cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Carton " + carton, font));
            cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Colspan = 4;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("To/A", font));
            cell.Colspan = 8;
            cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(recaddress, font1));
            cell.Colspan = 8;
            cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.Colspan = 8;
            cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Customer PO #", font1));
            cell.Colspan = 4;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Our Order #", font1));
            cell.Colspan = 4;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(po, font));
            cell.Colspan = 4;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(order, font));
            cell.Colspan = 4;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.Colspan = 8;
            cell.BorderColor = iTextSharp.text.BaseColor.WHITE;
            cell.Border = iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Sku #", font1));
            cell.Colspan = 3;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Description", font1));
            cell.Colspan = 4;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Qty", font1));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            Data.ProductsInPackagesRow[] productsinpackges = data.ProductsInPackages.Select("[Order #] = " + order + "AND [Package #] = " + carton) as Data.ProductsInPackagesRow[];

            foreach (Data.ProductsInPackagesRow p in productsinpackges)
            {
                cell = new PdfPCell(new Phrase(p._Product__, font));
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(p.Product_Description, font));
                cell.Colspan = 4;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(p.Product_Quantity, font));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);
            }

            doc.Add(table);

            var content = writer.DirectContent;
            var pageBorderRect = new iTextSharp.text.Rectangle((doc.PageSize.GetLeft(doc.LeftMargin) - 25F), (doc.PageSize.GetBottom(doc.BottomMargin) + 20F), (doc.PageSize.GetRight(doc.RightMargin) + 35F), (doc.PageSize.GetTop(doc.TopMargin)) + 30F);

            content.SetColorStroke(BaseColor.BLACK);
            content.SetLineWidth(2);
            content.Rectangle(pageBorderRect.Left, pageBorderRect.Bottom, pageBorderRect.Width, pageBorderRect.Height);
            content.Stroke();
            content.ClosePathStroke();

            doc.Close();
            writer.Close();
            return Path.Combine(filepath, order + "_Carton_" + carton + ".pdf");
        }

        private void btn_shipments_contentslabel_Click(object sender, EventArgs e)
        {
            DataRowView order = shipmentsTableBindingSource.Current as DataRowView;
            if (order == null)
            {
                MessageBox.Show("There is no order selected");
                return;
            }
            DataRowView shipment_row = shipmentsTableBindingSource.Current as DataRowView;
            Data.ShipmentsTableRow shipment = shipment_row.Row as Data.ShipmentsTableRow;
            List<string> contents_labels = Contents_Label(shipment._Order__);
            foreach (string s in contents_labels)
            {
                if (s != "")
                {
                    Process.Start(s);
                }
            }
            if (contents_labels.Count == 0)
            {
                MessageBox.Show("No Content Labels were generated");
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Data.HistoryPackagesDataTable temp = data.HistoryPackages.Copy() as Data.HistoryPackagesDataTable;
            temp.Clear();
            if (HistoryGrid.Rows.Count > 0)
            {
                string ordernumber = HistoryGrid.SelectedRows[0].Cells[1].Value.ToString();
                Data.HistoryPackagesRow[] rows = data.HistoryPackages.Select("[Order #] = '" + ordernumber + "'") as Data.HistoryPackagesRow[];
                foreach (Data.HistoryPackagesRow r in rows)
                {
                    temp.ImportRow(r);
                }
                temp.Columns.Remove("Select");
                if (temp.Rows.Count != 0)
                {
                    frmWizard_DataPreview prev = new frmWizard_DataPreview("Packages", temp);
                    prev.ShowDialog();
                }
                else
                {
                    MessageBox.Show("This order does not have aditional packages !");
                }
            }
        }

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S && e.Control)
            {
                e.SuppressKeyPress = true;
                OrdersProcessCurrentButton_Click(null, null);
            }
            //if (e.KeyCode == Keys.Enter)
            //{
            //    //if we are on OrdersPage, use Enter key to ship
            //    if (MainPages.SelectedTab == OrdersPage)
            //    {
            //        if (!OrdersPage.Focused)
            //        {
            //            return;
            //        }
            //        DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            //        if (order_row == null)
            //        {
            //            //MessageBox.Show("Please select an order to ship!");
            //            return;
            //        }
            //        Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            //        if (order == null)
            //        {
            //            MessageBox.Show("Selected order is null. Please make a valid selection");
            //            return;
            //        }
            //        // MessageBox.Show("Selected order is not null");
            //        object order_number = order["Order #"];
            //        if (order_number == null || order_number.Equals(""))
            //        {
            //            MessageBox.Show("Order number selected is null. Please select a valid order");
            //            return;
            //        }
            //        e.SuppressKeyPress = true;
            //        OrdersProcessCurrentButton_Click(null, null);
            //    }
            //}
            //add a new package using the "+" hotkey
            if (e.KeyCode == Keys.A && e.Control)
            {
                AddNewPackage();
                //if a scale is connected, then the GetWeight button will be enabled and we can read the weight when adding a new package
                if (Properties.Settings.Default.InputWeight)
                {
                    ReadWeight();
                }
                fKOrdersTableOrderPackagesTableBindingSource.Position = fKOrdersTableOrderPackagesTableBindingSource.Count - 1;
                e.SuppressKeyPress = true;
            }
            if (e.KeyCode == Keys.Add)
            {
                AddNewPackage();
                //if a scale is connected, then the GetWeight button will be enabled and we can read the weight when adding a new package
                if (Properties.Settings.Default.InputWeight)
                {
                    ReadWeight();
                }
                fKOrdersTableOrderPackagesTableBindingSource.Position = fKOrdersTableOrderPackagesTableBindingSource.Count - 1;
                e.SuppressKeyPress = true;
            }
            //if (e.KeyCode == Keys.Add && e.Alt)
            //{
            //    AddNewPackage();
            //    //if a scale is connected, then the GetWeight button will be enabled and we can read the weight when adding a new package
            //    if (Properties.Settings.Default.InputWeight)
            //    {
            //        ReadWeight();
            //    }
            //    fKOrdersTableOrderPackagesTableBindingSource.Position = fKOrdersTableOrderPackagesTableBindingSource.Count - 1;
            //    e.SuppressKeyPress = true;
            //}
            if (e.KeyCode == Keys.W && e.Control)
            {
                //DataRowView row = OrderPackagesGrid.SelectedRows[0].DataBoundItem as DataRowView;
                //*********************added code
                try
                {
                   // MessageBox.Show("In Ctrl W event");
                    DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
                    if (order_row == null)
                    {
                        return;
                    }
                    Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
                    if (order == null)
                    {
                        return;
                    }
                   // MessageBox.Show("Selected order is not null");
                    object order_number = order["Order #"];
                    if (order_number == null || order_number.Equals(""))
                    {
                        return;
                    }
                   // MessageBox.Show("Order # is: " + order_number.ToString());
                    //****read weight from scale
                    System.Decimal weight = 0;
                    try
                    {
                        if (!String.IsNullOrEmpty(Settings.Default.ScaleVID) && !String.IsNullOrEmpty(Settings.Default.ScalePID))
                        {
                            ScaleIntegr8.MyScale scale = new ScaleIntegr8.MyScale();
                            weight = scale.GetWeight(Settings.Default.ScaleVID, Settings.Default.ScalePID);
                            if (weight == -1)
                            {
                                MessageBox.Show("Could not read the weight, no scale connected! Please connect a scale and try again!", "Scale reading error");
                                return;
                            }
                        }
                        else
                        {
                            //read from NCI scale here
                            ScaleInteg8WTComm.MyScale scale = new ScaleInteg8WTComm.MyScale();
                            weight = Convert.ToDecimal(scale.GetWeight());
                            if (weight == -1)
                            {
                                MessageBox.Show("Could not read the weight, no scale connected! Please connect a scale and try again!", "Scale reading error");
                                return;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Scale reading error: " + ex.Message + " Stack trace: " + ex.StackTrace);
                        return;
                    }
                   // MessageBox.Show("Red weight is: " + weight.ToString());
                    //****set weight to corresponding package / skid or LTL Item
                    if (tabPackageType.SelectedTab == tabPagePackages)
                    {
                        //MessageBox.Show("Selected tab is Packages(Small)");
                    Data.OrderPackagesTableRow[] packages = order.GetOrderPackagesTableRows();
                    if (packages.Length == 0)
                    {
                        return;
                    }

                    DataRowView package_row = fKOrdersTableOrderPackagesTableBindingSource.Current as DataRowView;
                    Data.OrderPackagesTableRow package = package_row.Row as Data.OrderPackagesTableRow;
                    if (package == null)
                    {
                        MessageBox.Show("No package is selected. Please select one package to add the weight!", "Weight input");
                        return;
                    }
                    //MessageBox.Show("Current package is: " + package["Package #"].ToString());
                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = false;
                    fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();

                    //System.Decimal weight = 0;

                    //ScaleIntegr8.MyScale scale = new ScaleIntegr8.MyScale();
                    //weight = scale.GetWeight("0EB8", "F000");

                    if (data.OrderPackagesTable.Columns.IndexOf("Package Weight") > 0)
                    {
                        package["Package Weight"] = weight;
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Total Weight") > 0)
                    {
                        package["Total Weight"] = weight;
                    }
                    //MessageBox.Show("New weight for selected package is: " + package["Package Weight"].ToString());
                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = true;
                    fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(true);
                    fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();

                    //******************************************************
                    //row["Package Weight"] = scale.GetWeight("0EB8", "F000");
                    OrderPackagesGrid.Refresh();
                    }

                    if (tabPackageType.SelectedTab == tabPageSkids)
                    {
                        readSkidWeight(order, weight);
                    }
                    if (tabPackageType.SelectedTab == tabPageItems)
                    {
                        readLTLTItemWeight(order, weight);
                    }
                    e.SuppressKeyPress = true;
                }
                catch (Exception ex)
                {

                }
            }
            if (e.KeyCode == Keys.S && e.Alt)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void ReadWeight()
        {
             try
            {
                if (OrdersTableBindingSource.Position < 0)
                {
                    if (OrdersTableBindingSource.Count > 0)
                    {
                        OrdersTableBindingSource.Position = OrdersTableBindingSource.Count - 1; //OrdersTableBindingSource.Position = 0;
                    }
                }
                DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
                if (order_row == null)
                {
                    return;
                }
                Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
                if (order == null)
                {
                    return;
                }
                // MessageBox.Show("Selected order is not null");
                object order_number = order["Order #"];
                if (order_number == null || order_number.Equals(""))
                {
                    return;
                }
                // MessageBox.Show("Order # is: " + order_number.ToString());
                //****read weight from scale
                System.Decimal weight = 0;
                try
                {
                    if (!String.IsNullOrEmpty(Settings.Default.ScaleVID) && !String.IsNullOrEmpty(Settings.Default.ScalePID))
                    {
                        ScaleIntegr8.MyScale scale = new ScaleIntegr8.MyScale();
                        weight = scale.GetWeight(Settings.Default.ScaleVID, Settings.Default.ScalePID);
                        if (weight == -1)
                        {
                            MessageBox.Show("Could not read the weight, no scale connected! Please connect a scale and try again!", "Scale reading error");
                            return;
                        }
                    }
                    else
                    {
                        //read from NCI scale here
                        ScaleInteg8WTComm.MyScale scale = new ScaleInteg8WTComm.MyScale();
                        weight = Convert.ToDecimal(scale.GetWeight());
                        if (weight == -1)
                        {
                            MessageBox.Show("Could not read the weight, no scale connected! Please connect a scale and try again!", "Scale reading error");
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Scale reading error: " + ex.Message + " Stack trace: " + ex.StackTrace);
                    return;
                }
                // MessageBox.Show("Red weight is: " + weight.ToString());
                //****set weight to corresponding package / skid or LTL Item
                if (tabPackageType.SelectedTab == tabPagePackages)
                {
                    //MessageBox.Show("Selected tab is Packages(Small)");
                    Data.OrderPackagesTableRow[] packages = order.GetOrderPackagesTableRows();
                    if (packages.Length == 0)
                    {
                        return;
                    }

                    DataRowView package_row = fKOrdersTableOrderPackagesTableBindingSource.Current as DataRowView;
                    Data.OrderPackagesTableRow package = package_row.Row as Data.OrderPackagesTableRow;
                    if (package == null)
                    {
                        MessageBox.Show("No package is selected. Please select one package to add the weight!", "Weight input");
                        return;
                    }
                    //MessageBox.Show("Current package is: " + package["Package #"].ToString());
                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = false;
                    fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();

                    //System.Decimal weight = 0;

                    //ScaleIntegr8.MyScale scale = new ScaleIntegr8.MyScale();
                    //weight = scale.GetWeight("0EB8", "F000");

                    if (data.OrderPackagesTable.Columns.IndexOf("Package Weight") > 0)
                    {
                        package["Package Weight"] = weight;
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Total Weight") > 0)
                    {
                        package["Total Weight"] = weight;
                    }
                    //MessageBox.Show("New weight for selected package is: " + package["Package Weight"].ToString());
                    fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = true;
                    fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(true);
                    fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();

                    //******************************************************
                    //row["Package Weight"] = scale.GetWeight("0EB8", "F000");
                    OrderPackagesGrid.Refresh();
                }

                if (tabPackageType.SelectedTab == tabPageSkids)
                {
                    readSkidWeight(order, weight);
                }
                if (tabPackageType.SelectedTab == tabPageItems)
                {
                    readLTLTItemWeight(order, weight);
                }
                
            }
            catch (Exception ex)
            {

            }
        }

        #region Backup

        bool delete_backup = false;
        DateTime backup_detete_time = new DateTime();

        private void bg_backup_server_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!Template.Backup_at_Scheadule || !Template.Backup_Orders || !String.IsNullOrEmpty(Template.Backup_Schedule))
            {
                return;
            }
            while (true)
            {
                if (DateTime.Now.TimeOfDay >= Settings.Default.Backup_delete_houre.TimeOfDay && DateTime.Now.TimeOfDay <= (Settings.Default.Backup_delete_houre + new TimeSpan(0, 5, 0)).TimeOfDay && !delete_backup)
                {
                    backup_detete_time = (Settings.Default.Backup_delete_houre + new TimeSpan(0, 5, 0));
                    delete_backup = true;
                    using (OdbcConnection Conn = new OdbcConnection(Template.BackupConn.ConnStr))
                    {
                        Conn.Open();

                        using (OdbcCommand Comm = new OdbcCommand())
                        {
                            Comm.Connection = Conn;
                            Comm.CommandText = Template.Backup_Schedule;
                            Comm.ExecuteNonQuery();
                        }
                    }
                }
                if (DateTime.Now.TimeOfDay >= backup_detete_time.TimeOfDay && delete_backup)
                {
                    delete_backup = false;
                }
                System.Threading.Thread.Sleep(100000);
            }
        }

        #endregion

        private void ShipmentsGrid_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            if (ShipmentsGrid.Rows[e.RowIndex] != null)
            {
                if (ShipmentsGrid.Rows[e.RowIndex].Selected)
                {
                    using (Pen pen = new Pen(Color.DarkBlue))
                    {
                        int penWidth = 4;

                        pen.Width = penWidth;

                        int x = e.RowBounds.Left + (penWidth / 2);
                        int y = e.RowBounds.Top + (penWidth / 2);
                        int width = e.RowBounds.Width - penWidth;
                        int height = e.RowBounds.Height - penWidth;

                        e.Graphics.DrawRectangle(pen, x, y, width, height);
                    }
                }
                if (ShipmentsGrid.Rows[e.RowIndex].Cells["Shipmentsstatus"].Value != null)
                {
                    if (ShipmentsGrid.Rows[e.RowIndex].Cells["Shipmentsstatus"].Value.ToString() == "Closed")
                    {
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Yellow;
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Black;
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.Yellow;
                    }
                    if (ShipmentsGrid.Rows[e.RowIndex].Cells["Shipmentsstatus"].Value.ToString() == "Aggregated" || ShipmentsGrid.Rows[e.RowIndex].Cells["Shipmentsstatus"].Value.ToString() == "No Aggregation")
                    {
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LimeGreen;
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Black;
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.LimeGreen;
                    }
                    if (ShipmentsGrid.Rows[e.RowIndex].Cells["Shipmentsstatus"].Value.ToString().Contains("Error"))
                    {
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.BackColor = ControlPaint.Light(Color.Red);
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Black;
                        ShipmentsGrid.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = ControlPaint.Light(Color.Red);
                    }
                }
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            if (data.ShipmentsTable.Rows.Count > 0)
            {
                if (progress.Created)
                {
                    progress.Close_Me();
                }
                List<string> carriers = new List<string>();
                foreach (Data.ShipmentsTableRow r in data.ShipmentsTable)
                {
                    if (r.Status == "Shipped" && !carriers.Contains(r.Carrier_Code))
                    {
                        carriers.Add(r.Carrier_Code);
                    }
                }
                if (carriers.Count == 0)
                {
                    MessageBox.Show("There are no orders to close !");
                    return;
                }
                CloseCarriers close = new CloseCarriers(carriers);
                if (close.ShowDialog() != DialogResult.Cancel)
                {
                    carriers.Clear();
                    carriers.AddRange(close.carriers);
                }
                else
                {
                    return;
                }
                progress = new frmProcessing(TaskBackgroundWorker);
                string[] arguments = new string[carriers.Count + 1];
                arguments[0] = "Close";
                int i = 1;
                foreach (string s in carriers)
                {
                    arguments[i++] = s;
                }
                bgworker_save.RunWorkerAsync(arguments);
                progress.ShowProgress2("Saving Shipments");
                progress.ShowDialog(this);
                foreach (string carrier in carriers)
                {
                    foreach (Data.ShipmentsTableRow ship in data.ShipmentsTable)
                    {
                        if (carrier == ship.Carrier_Code)
                        {
                            ship.Status = "Closed";
                        }
                    }
                }
                shipmentsTableBindingSource.ResetBindings(true);
                HistoryBindingSource.ResetBindings(true);
            }
        }

        private void btn_aggregate_Click(object sender, EventArgs e)
        {
            if (data.ShipmentsTable.Rows.Count > 0)
            {
                if (progress.Created)
                {
                    progress.Close_Me();
                }
                progress = new frmProcessing(TaskBackgroundWorker);
                string[] arguments = { "Aggregate" };
                bgworker_save.RunWorkerAsync(arguments);
                progress.ShowProgress2("Saving Shipments");
                progress.ShowDialog(this);
                shipmentsTableBindingSource.ResetBindings(true);
                HistoryBindingSource.ResetBindings(true);
            }
        }

        private void MainPages_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MainPages.SelectedTab == OrdersPage)
            {
                tbValue1.Focus();
                //if the previous tab was 2ShipEdit and the order was shipped in that page, get the edit results
                try
                {
                    DataView orders = OrdersTableBindingSource.List as DataView;
                   if (orders == null)
                    {
                        return;
                    }
                   if (data.OrdersTable.Columns.IndexOf("Status") >= 0)
                   {
                       foreach (DataRowView order_row in orders)
                       {
                           if (order_row["Status"].ToString() == "Edit")
                           {
                               GetEditResultsFrom2Ship();
                               order_row["Status"] = "";
                           }
                       }
                   }
                }
                catch (Exception ex)
                {
                    Utils.SendError("Error in GetEditResults", ex);
                    MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
       
        private void btn_clear_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Template.Backup_Schedule))
            {
                return;
            }
            using (OdbcConnection Conn = new OdbcConnection(Template.BackupConn.ConnStr))
            {
                Conn.Open();

                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;
                    Comm.CommandText = Template.Backup_Schedule;
                    Comm.ExecuteNonQueryAsync(); //- not XP compatible(not in .Net Framework 4.0)
                    //Comm.ExecuteNonQuery();
                }
            }
            BackupFile.Load(data);
        }

        private void OrdersGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void ShipmentsGrid_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void cmbMenuCarrier_Click(object sender, EventArgs e)
        {
            if (cmbMenuCarrier.ComboBox.SelectedValue != null)
            {
                cmbMenuService.ComboBox.DataSource = bsMenuServicesFiltered;
                bsMenuServicesFiltered.Filter = "Carrier_Code = '" + cmbMenuCarrier.ComboBox.SelectedValue.ToString() + "'";
            }
        }

        private void cmbMenuCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMenuCarrier.ComboBox.SelectedValue != null)
            {
                cmbMenuService.ComboBox.DataSource = bsMenuServicesFiltered;
                bsMenuServicesFiltered.Filter = "Carrier_Code = '" + cmbMenuCarrier.ComboBox.SelectedValue.ToString() + "'";
            }
        }

        private void btnApplyCarrierServiceToSelected_Click(object sender, EventArgs e)
        {
            if (OrdersTableBindingSource.Position < 0)
            {
                MessageBox.Show("There is no order selected!", "Attention", MessageBoxButtons.OK);
                return;
            }
            DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            if (order == null)
            {
                return;
            }
            DataView ord_rows = OrdersTableBindingSource.List as DataView;
            foreach (DataRowView ord in ord_rows)
            {
                object order_number = ord["Order #"];
                bool order_selected = ord["Selected"] != null && !ord["Selected"].Equals(DBNull.Value) && Convert.ToBoolean(ord["Selected"]);
                if (order_number == null || !order_selected || order_number.Equals(""))
                {
                    continue;
                }
                if (order_selected)
                {
                    //Data.OrdersTableRow orderSelected = ord.Row as Data.OrdersTableRow;
                        ord["Selected"] = false;
                        data.OrdersTable.FindBy_Order__(order_number.ToString()).Carrier = cmbMenuCarrier.ComboBox.SelectedValue.ToString();
                        data.OrdersTable.FindBy_Order__(order_number.ToString()).Service = cmbMenuService.ComboBox.SelectedValue.ToString();
                        ord["Selected"] = true;
                }
            }
            OrdersTableBindingSource.EndEdit();
        }
        
        private void btnSetFieldValue_Click(object sender, EventArgs e)
        {
            if (cbPackageField.SelectedValue == null)
            {
                MessageBox.Show("Please select one field from the list!", "No field selected");
                return;
            }
            if (String.IsNullOrWhiteSpace(txtPackageFieldValue.Text))
            {
                MessageBox.Show("The Value field cannot be empty!", "No value entered");
                return;
            
            }
            double valueEntered = 0;
            if (!Double.TryParse(txtPackageFieldValue.Text, out valueEntered))
            {
                MessageBox.Show("The value field has to be numeric!", "Invalid value");
                return;
            
            }
            if (OrdersTableBindingSource.Position < 0)
            {
                MessageBox.Show("There is no order selected!", "Attention", MessageBoxButtons.OK);
                return;
            }
            DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            if (order == null)
            {
                return;
            }
            if (tabPackageType.SelectedTab == tabPagePackages)
            {
                object order_number = order["Order #"];
                if (order_number == null || order_number.Equals(""))
                {
                    return;
                }
                if (fKOrdersTableOrderPackagesTableBindingSource.Position < 0)
                {
                    MessageBox.Show("There is no package selected! Please select a package first", "Attention", MessageBoxButtons.OK);
                    return;
                }
                DataRowView package_row = fKOrdersTableOrderPackagesTableBindingSource.Current as DataRowView;
                Data.OrderPackagesTableRow package = package_row.Row as Data.OrderPackagesTableRow;
                if (package == null)
                {
                    return;
                }
                string fieldName = data.FieldsTable.FindById(Int32.Parse(cbPackageField.SelectedValue.ToString())).Name;
                
                fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = false;
                fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();

                if (rbSetForMissing.Checked)
                {
                    foreach (Data.OrderPackagesTableRow p in order.GetOrderPackagesTableRows())
                    {
                        if (p[fieldName] == null || (System.Convert.ToDouble(p[fieldName]) <= 0) )
                        {
                            p[fieldName] = valueEntered;
                        }
                    }
                }
                else if (rbSetFieldAll.Checked)
                {
                    foreach (Data.OrderPackagesTableRow p in order.GetOrderPackagesTableRows())
                    {
                        p[fieldName] = valueEntered;
                    }
                }
                else
                {
                    package[fieldName] = valueEntered;
                }

                fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = true;
                fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(true);
                fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();
                
                OrderPackagesGrid.EndEdit();
                RecalculateSkidWeight(order, order.GetOrderPackagesTableRows());
               
            }
            if (tabPackageType.SelectedTab == tabPageSkids)
            {
                object order_number = order["Order #"];
                if (order_number == null || order_number.Equals(""))
                {
                    return;
                }
                if (fkOrdersTableOrderSkidsTableBindingSource.Position < 0)
                {
                    MessageBox.Show("There is no order selected!", "Attention", MessageBoxButtons.OK);
                    return;
                }
                DataRowView skid_row = fkOrdersTableOrderSkidsTableBindingSource.Current as DataRowView;
                Data.OrdersSkidsTableRow skid = skid_row.Row as Data.OrdersSkidsTableRow;
                if (skid == null)
                {
                    return;
                }
                string skidFieldName = data.FieldsTable.FindById(Int32.Parse("30" + cbPackageField.SelectedValue.ToString())).Name;

                fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = false;
                fkOrdersTableOrderSkidsTableBindingSource.SuspendBinding();
                 if (rbSetForMissing.Checked)
                {
                    foreach (Data.OrdersSkidsTableRow s in order.GetOrdersSkidsTableRows())
                    {
                        if (String.IsNullOrWhiteSpace(s[skidFieldName].ToString()) || (System.Convert.ToDouble(s[skidFieldName]) <= 0))
                        {
                            s[skidFieldName] = valueEntered;
                        }
                    }
                }
                 else if (rbSetFieldAll.Checked)
                 {
                     foreach (Data.OrdersSkidsTableRow s in order.GetOrdersSkidsTableRows())
                     {
                         s[skidFieldName] = valueEntered;
                     }
                 }
                 else
                 {
                     skid[skidFieldName] = valueEntered;
                 }

                fkOrdersTableOrderSkidsTableBindingSource.RaiseListChangedEvents = true;
                fkOrdersTableOrderSkidsTableBindingSource.ResetBindings(true);
                fkOrdersTableOrderSkidsTableBindingSource.ResumeBinding();
                OrderSkidsGrid.EndEdit();
            }
            if (tabPackageType.SelectedTab == tabPageItems)
            {
                object order_number = order["Order #"];
                if (order_number == null || order_number.Equals(""))
                {
                    return;
                }
                if (fkOrdersTableLTLItemsBindingSource.Position < 0)
                {
                    MessageBox.Show("There is no order selected!", "Attention", MessageBoxButtons.OK);
                    return;
                }
                DataRowView LTLItem_row = fkOrdersTableLTLItemsBindingSource.Current as DataRowView;
                Data.OrderLTLItemsRow LTLItem = LTLItem_row.Row as Data.OrderLTLItemsRow;
                if (LTLItem == null)
                {
                    return;
                }
                int fieldCode = 0;
                switch (cbPackageField.SelectedValue.ToString())
                {
                    case "77":
                        {
                            fieldCode = 3202;
                            break;
                        }
                    case "83":
                        {
                            fieldCode = 3204;
                            break;
                        }
                    case "84":
                        {
                            fieldCode = 3205;
                            break;
                        }
                    case "85":
                        {
                            fieldCode = 3206;
                            break;
                        }
                }
                string itemFieldName = data.FieldsTable.FindById(fieldCode).Name;

                fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = false;
                fkOrdersTableLTLItemsBindingSource.SuspendBinding();
                 if (rbSetForMissing.Checked)
                {
                    foreach (Data.OrderLTLItemsRow LTL in order.GetOrderLTLItemsRows())
                    {
                        if (String.IsNullOrWhiteSpace(LTL[itemFieldName].ToString()) || (System.Convert.ToDouble(LTL[itemFieldName]) <= 0))
                        {
                            LTL[itemFieldName] = valueEntered;
                        }
                    }
                }
                 else if (rbSetFieldAll.Checked)
                 {
                     foreach (Data.OrderLTLItemsRow LTL in order.GetOrderLTLItemsRows())
                     {
                         LTL[itemFieldName] = valueEntered;
                     }
                 }
                 else
                 {
                     LTLItem[itemFieldName] = valueEntered;
                 }

                fkOrdersTableLTLItemsBindingSource.RaiseListChangedEvents = true;
                fkOrdersTableLTLItemsBindingSource.ResetBindings(true);
                fkOrdersTableLTLItemsBindingSource.ResumeBinding();
                LTLItemsGrid.EndEdit();
            }
            rbFieldCurrent.Checked = false;
            rbSetForMissing.Checked = false;
            rbSetFieldAll.Checked = false;
            txtPackageFieldValue.Text = "";

            OrdersPage.Focus();
        }

        private void tsButtonPackslip_Click(object sender, EventArgs e)
        {
            DataRowView order = OrdersTableBindingSource.Current as DataRowView;
            if (order == null)
            {
                MessageBox.Show("There is no order selected");
                return;
            }
            DataRowView orders_row = OrdersTableBindingSource.Current as DataRowView;
            Data.OrdersTableRow ord = orders_row.Row as Data.OrdersTableRow;
            string order_number = ord["Order #"].ToString();
            string ShipDate = ord["Ship Date"].ToString();
            string client = data.OrdersTable.Columns.Contains("Client ID") ? ord["Client ID"].ToString() : (data.OrdersTable.Columns.Contains("Recipient Contact") ? ord["Recipient Contact"].ToString() : ord["Recipient Name"].ToString());
            string po_id = data.OrdersTable.Columns.Contains("Shipment PO Number") ? ord["Shipment PO Number"].ToString() : "";
            string TN = "";
            string RecAddress = "";
            RecAddress = (data.OrdersTable.Columns.IndexOf("Recipient Company") >= 0 ? order["Recipient Company"].ToString().TrimEnd() : "") + "\n" +
                   order["Recipient Address 1"].ToString().TrimEnd() + " " +
                   order["Recipient Address 2"].ToString().TrimEnd() + "\n" +
                   order["Recipient City"].ToString().TrimEnd() + ", " +
                   order["Recipient State / Province"].ToString().TrimEnd() + ", " +
                   order["Recipient ZIP / Postal Code"].ToString().TrimEnd() + ", " +
                   order["Recipient Country"].ToString() + "\n" +
                       ((data.OrdersTable.Columns.IndexOf("Recipient Tel") >= 0) ? (String.IsNullOrEmpty(ord["Recipient Tel"].ToString()) ? "" : "Phone: " + ord["Recipient Tel"].ToString().TrimEnd()) + "\n" : "") +
                       ((data.OrdersTable.Columns.IndexOf("Recipient Email") >= 0) ? (String.IsNullOrEmpty(ord["Recipient Email"].ToString()) ? "" : "Email: " + ord["Recipient Email"].ToString().TrimEnd()) + " " : "");

            string BillAddress = "";
            string billType = ord["Billing Type"].ToString().TrimEnd();
            if (billType == "3")
            {
                BillAddress = (data.OrdersTable.Columns.IndexOf("Billing Company") >= 0 ? ord["Billing Company"].ToString().TrimEnd() : "") + " " + "\n" +
                    (data.OrdersTable.Columns.IndexOf("Billing Address 1") >= 0 ? ord["Billing Address 1"].ToString().TrimEnd() : "") + " " +
                    (data.OrdersTable.Columns.IndexOf("Billing Address 2") >= 0 ? ord["Billing Address 2"].ToString().TrimEnd() : "") + " " + "\n" +
                    (data.OrdersTable.Columns.IndexOf("Billing City") >= 0 ? ord["Billing City"].ToString().TrimEnd() : "") + ", " +
                    (data.OrdersTable.Columns.IndexOf("Billing State") >= 0 ? ord["Billing State"].ToString().TrimEnd() : "") + ", " +
                    (data.OrdersTable.Columns.IndexOf("Billing ZIP") >= 0 ? ord["Billing ZIP"].ToString().TrimEnd() : "") + ", " +
                    (data.OrdersTable.Columns.IndexOf("Billing Country") >= 0 ? ord["Billing Country"].ToString().TrimEnd() : "") + "\n" +
                    ((data.OrdersTable.Columns.IndexOf("Billing Tel") >= 0) ? (String.IsNullOrEmpty(ord["Billing Tel"].ToString()) ? "" : "Phone: " + ord["Billing Tel"].ToString().TrimEnd()) + "\n" : "") +
                    ((data.OrdersTable.Columns.IndexOf("Billing Email") >= 0) ? (String.IsNullOrEmpty(ord["Billing Email"].ToString()) ? "" : "Email: " + ord["Billing Email"].ToString().TrimEnd() + " ") : "");
            }
            else
            {
                if (billType == "2")
                {
                    BillAddress = RecAddress;
                }
                else
                {
                    BillAddress = (data.OrdersTable.Columns.IndexOf("Sender Company") >= 0 ? ord["Sender Company"].ToString().TrimEnd() : "") + "\n" +
                        (data.OrdersTable.Columns.IndexOf("Sender Address 1") >= 0 ? ord["Sender Address 1"].ToString().TrimEnd() : "") + " " +
                        (data.OrdersTable.Columns.IndexOf("Sender Address 2") >= 0 ? ord["Sender Address 2"].ToString().TrimEnd() : "") + "\n" +
                        (data.OrdersTable.Columns.IndexOf("Sender City") >= 0 ? ord["Sender City"].ToString().TrimEnd() : "") + ", " +
                        (data.OrdersTable.Columns.IndexOf("Sender State / Province") >= 0 ? ord["Sender State / Province"].ToString().TrimEnd() : "") + ", " +
                        (data.OrdersTable.Columns.IndexOf("Sender ZIP / Postal Code") >= 0 ? ord["Sender ZIP / Postal Code"].ToString().TrimEnd() : "") + ", " +
                        (data.OrdersTable.Columns.IndexOf("Sender Country") >= 0 ? ord["Sender Country"].ToString().TrimEnd() : "") + "\n" +
                        (data.OrdersTable.Columns.IndexOf("Sender Email") >= 0 ? ((String.IsNullOrEmpty(ord["Sender Tel"].ToString()) ? "" : "Phone: " + ord["Sender Tel"].ToString().TrimEnd()) + "\n") : "") +
                        (data.OrdersTable.Columns.IndexOf("Sender Email") >= 0 ? ((String.IsNullOrEmpty(ord["Sender Email"].ToString()) ? "" : "Email: " + ord["Sender Email"].ToString().TrimEnd()) + " ") : "");
                }
            }

            Data.OrderProductsTableRow[] products = ord.GetOrderProductsTableRows();
            List<packing_list> products_desc = new List<packing_list>();
            packing_list current_prod = new packing_list();
            if (products.Length > 0)
            {
                foreach (Data.OrderProductsTableRow p in products)
                {
                    current_prod = new packing_list();
                    current_prod.styleID = p["Product #"].ToString();
                    current_prod.Title = p["Product Description"].ToString();
                    current_prod.Quantity = Convert.ToDouble(p["Product Quantity"].ToString()).ToString();
                    products_desc.Add(current_prod);
                }
            }
            else
            {
                current_prod.styleID = "";
                current_prod.Title = "";
                current_prod.BO = "";
                current_prod.Quantity = "";
                products_desc.Add(current_prod);
            }

            string CarrierAndService = (String.IsNullOrEmpty(ord["Carrier"].ToString()) ? "" : (String.IsNullOrEmpty(data.Carriers.FindByCode(ord["Carrier"].ToString()).Name) )? "" : data.Carriers.FindByCode(ord["Carrier"].ToString()).Name) + " - " +
                (String.IsNullOrEmpty(ord["Service"].ToString()) ? "" : (String.IsNullOrEmpty(data.Services.FindByUnique(ord["Service"].ToString()).Name) ? "" : data.Services.FindByUnique(ord["Service"].ToString()).Name));
            string path = Get_Packing_Slip(String.IsNullOrEmpty(Template.AutoprintPackSlipFolder) ? Settings.Default.PackingSlipPath : Template.AutoprintPackSlipFolder, null, order_number.Split('.')[0], ShipDate, client, po_id, TN, BillAddress, RecAddress, products_desc, CarrierAndService, "", Template.Name == "");
            // PackSlip(ord);
            if (path != "")
            {
                if (path == "Can't generate packslip !")
                {
                    MessageBox.Show(path);
                }
                else
                {
                    Process.Start(path);
                }
            }
        }

        private void BillingTypesBS_PositionChanged(object sender, EventArgs e)
        {
            //if (OrdersTableBindingSource.Position < 0)
            //{
            //    return;
            //}
            //DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            //Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_row.Row["Order #"].ToString());

            //if (!order["Billing Type"].ToString().Equals("1"))
            //{
            //    if (Template.Name.Equals("FootPrint"))
            //    {
            //        if (data.OrdersTable.Columns.IndexOf("Billing Account") < 0)
            //        {
            //            data.OrdersTable.Columns.Add("Billing Account");
            //        }
            //        if (String.IsNullOrWhiteSpace(order["Billing Account"].ToString()))
            //        {
            //            frmBillingInfo billing = new frmBillingInfo();

            //            if (billing.ShowDialog(this) != DialogResult.OK)
            //            {
            //                return;
            //            }
            //            else
            //            {
            //                AddBillingInfoFields(order, billing.getBillingInfo);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        if (data.OrdersTable.Columns.IndexOf("Billing Account") < 0)
            //        {
            //            data.OrdersTable.Columns.Add("Billing Account");
            //        }
            //        if (String.IsNullOrWhiteSpace(order["Billing Account"].ToString()))
            //        {
            //            order["Billing Account"] = frmInputBox.InputBox(this, "Billing Account");
            //        }

            //    }

            //}
        }

        private void cmbPackageDims_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                
                if (packageDimsBindingSource.Position < 0)
                {
                    return;
                }
                if (packageDimsBindingSource.Current == null)
                {
                    return;
                }
               
                if (fKOrdersTableOrderPackagesTableBindingSource.Current == null)
                {
                    return;
                }
                if (fKOrdersTableOrderPackagesTableBindingSource.Position < 0)
                {
                    return;
                }
                //packageDimsBindingSource.EndEdit();
                //packageDimsBindingSource.RaiseListChangedEvents = false;
                //packageDimsBindingSource.SuspendBinding();
                
                //packageDimsBindingSource.RaiseListChangedEvents = true;
                
                //packageDimsBindingSource.ResetCurrentItem();
              
                ////packageDimsBindingSource.ResetBindings(true);
                //packageDimsBindingSource.ResumeBinding();

                string code = cmbPackageDims.SelectedValue as string;
                
                DataRowView p_row = fKOrdersTableOrderPackagesTableBindingSource.Current as DataRowView;
                Data.OrderPackagesTableRow pRow = p_row.Row as Data.OrderPackagesTableRow;
                //DataRowView d_row = packageDimsBindingSource.Current as DataRowView;
                Data.PackageDimensionsRow dimRow = data.PackageDimensions.FindByCode(code); //d_row.Row as Data.PackageDimensionsRow;
                pRow["Package Length"] = dimRow["Length"];
                pRow["Package Width"] = dimRow["Width"];
                pRow["Package Height"] = dimRow["Height"];
                if (data.OrderPackagesTable.Columns.IndexOf("Package Dimensions Code") >= 0)
                {
                    pRow["Package Dimensions Code"] = code;
                }
            }
            catch (Exception ex)
            {
                //
            }
        }

        private void cmdRateCurrentService_Click(object sender, EventArgs e)
        {

            EndEdit();
            try
            {


                DataView orders = OrdersTableBindingSource.List as DataView;
                if (orders == null) return;
                List<InteGr8Task> tasks = new List<InteGr8Task>(orders.Count);
                bool bIsSelected = false;
                foreach (DataRowView order_row in orders)
                {
                    if (order_row["Selected"] != null && order_row["Selected"] != DBNull.Value)
                    {
                        object order_number = order_row["Order #"];
                        bool order_selected = Convert.ToBoolean(order_row["Selected"]);
                        if (order_number == null || !order_selected || order_number.Equals("")) continue;
                        bIsSelected = true;
                        tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.Rate2, order_number.ToString()));
                    }
                }
                if (!bIsSelected)
                {
                    DataRowView order = OrdersTableBindingSource.Current as DataRowView;
                    if (order != null)
                    {
                        tasks.Add(new InteGr8Task(TemplatesDropdown.Text, data, InteGr8TaskType.Rate2, order["Order #"].ToString()));
                    }
                    else
                    {
                        MessageBox.Show("Please select at least one order!", "Warning");
                    }
                }
                if (tasks == null || tasks.Count == 0)
                {
                    return;
                }
                StartTasks(tasks.ToArray());
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in cmdRateCurrentService_Click", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbMultiOrder_CheckedChanged(object sender, EventArgs e)
        {
            //create a new property in settings which will hold the value of this checkbox checked / unchecked
            //make here all the changes needed - just set the value in settings, so that orders will be processed - allow scan of multiple
            //orders without adding packages and weight to the 2+ orders
            //at ship, if this button is checked, ship only the first order - only one label needed, one shipment
            //at Shipments save - if this checkbox is checked, add the 2+ orders to shipments table, copying the values for ShipmentTracking#
            //Cost and the rest of columns in Shipments table, from the first line - the real Shipment processed
            //if there is no OrdersFilter in integr8 - the full version with multiple orders loading from database, and without order scan
            //hide this checkbox as it will not be necessary
            Properties.Settings.Default.MultiOrderShipment = cbMultiOrder.Checked;
        }

        private void btnAddMultiPacks_Click(object sender, EventArgs e)
        {
            //open pop-up for multiple package add
            if (OrdersTableBindingSource.Position < 0)
            {
                MessageBox.Show("There is no order selected!", "Attention", MessageBoxButtons.OK);
                return;
            }
            DataRowView order_row = OrdersTableBindingSource.Current as DataRowView;
            Data.OrdersTableRow order = order_row.Row as Data.OrdersTableRow;
            if (order == null)
            {
                return;
            }
            EndEdit();
            fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = false;
            fKOrdersTableOrderPackagesTableBindingSource.SuspendBinding();

            new MultiPack(order).ShowDialog();


            fKOrdersTableOrderPackagesTableBindingSource.RaiseListChangedEvents = true;
            fKOrdersTableOrderPackagesTableBindingSource.ResetBindings(true);
            fKOrdersTableOrderPackagesTableBindingSource.ResumeBinding();
            updateNoOfPackagesInSkids(order);
        }
        //dateTimePicker custom selection of ShipDate
        private void dtpShipDate_ValueChanged(object sender, EventArgs e)
        {
            
            if (dtpShipDate.Value.Date > DateTime.Today.Date)//(dtpShipDate.BackColor == Color.Red)
            {
                dtpShipDate.BackColor = Color.Red;
            }
            else
            {
                dtpShipDate.BackColor = Color.FromKnownColor(KnownColor.Window);
            }
            //if (Template.Name.ToLower().Contains("bodyplus"))
                SetDTPShipDate();
        }

        private void SetDTPShipDate()
        {
            DateTime newShipDate = dtpShipDate.Value;
            EndEdit();
            OrdersTableBindingSource.RaiseListChangedEvents = false;
            OrdersTableBindingSource.SuspendBinding();
            foreach (Data.OrdersTableRow ord in data.OrdersTable.Rows)
            {
                ord["Ship Date"] = newShipDate;
            }

            OrdersTableBindingSource.RaiseListChangedEvents = true;
            OrdersTableBindingSource.ResetBindings(true);
            OrdersTableBindingSource.ResumeBinding();
        }

        private void tbValue1_TextChanged(object sender, EventArgs e)
        {

        }
    }

    #region packing_list

    public class packing_list
    {
        public string styleID = "";
        public string Title = "";
        public string BO = "";
        public string Quantity = "";
    }

    #endregion

    #region InteGr8Task

    enum InteGr8TaskType { RateCheapestService = 20, RateFastestService = 21, Rate = 7, Rate2 = -7, Ship = 2, Edit = 9, Delete = 4, GetBackEditResults = -10 }

    class InteGr8Task
    {
        private readonly string template;
        private readonly Data data;
        public readonly InteGr8TaskType type;
        public readonly string order_number;
        public readonly Fields request;
        private string _result;
        private Fields _reply;
        public readonly CarrierReply[] carriers;
        public Exception exception;
        public CarrierShipRequest shipReq = new CarrierShipRequest();
        public RateRequest rateReq = new RateRequest();
        public EditRequest editReq = new EditRequest();
        public GetShipmentsRequest getReq = new GetShipmentsRequest();
        public DeleteRequest delReq = new DeleteRequest();
        private CarrierRate[] rates;
        private EditResponse _editResult;
        CarrierShipResponse[] _shipResult;

        public InteGr8Task(string ClientID, int LocationID, string ClientWSKey)
        {
            try
            {
                using (WSTwoShip.I2ShipServiceClient ws = new I2ShipServiceClient())//(ShipService ws = new WS2Ship.ShipService())
                {
          
                    GetCarriersRequest carr = new GetCarriersRequest();
                    carr.WS_Key = ClientWSKey;
                    carr.Type = RequestType.Location;
                    carr.LocationId = LocationID;
                   
                    carriers = ws.GetCarriers(carr);
                }
            }
            catch (Exception e)
            {
                Utils.SendError("Error retrieving available carriers for client " + ClientID, e);
            }
        }

        public InteGr8Task(string template, Data data, InteGr8TaskType type, string order_number)
        {
            try
            {
                if (template == null || String.IsNullOrEmpty(template))
                {
                    throw new ArgumentException("template");
                }
                if (data == null)
                {
                    throw new ArgumentException("data is null");
                }
                if (String.IsNullOrEmpty(order_number))
                {
                    throw new ArgumentException("order_number is null");
                }

                this.template = template;
                this.data = data;
                this.type = type;
                this.order_number = order_number;
                request = new Fields();
              
                Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_number);
                if (order == null)
                    throw new ArgumentException("order not found");
                if (type == InteGr8TaskType.Ship)
                {
                    shipReq = new CarrierShipRequest();
                    string carrier = order["Carrier"].ToString();

                    // add order fields(sender, recipient, billing, carrier & service)
                    Contact_Ship sender = new Contact_Ship();
                    sender.Address1 = order["Sender Address 1"].ToString();
                    sender.Address2 = order["Sender Address 2"].ToString();
                    sender.City = order["Sender City"].ToString();
                    sender.CompanyName = order["Sender Company"].ToString();
                    sender.Country = order["Sender Country"].ToString();
                    sender.State = order["Sender State / Province"].ToString();
                    sender.PostalCode = order["Sender ZIP / Postal Code"].ToString();
                    if (data.OrdersTable.Columns.Contains("Sender Contact"))
                    {
                        sender.PersonName = order["Sender Contact"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Sender Tel"))
                    {
                        sender.Telephone = order["Sender Tel"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Sender Email"))
                    {
                        sender.Email = order["Sender Email"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Sender Tax Id"))
                    {
                        sender.TaxID = order["Sender Tax Id"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Sender Address 3"))
                    {
                        sender.Address3 = order["Sender Address 3"].ToString();
                    }

                    shipReq.Sender = sender;
                    //MessageBox.Show("Ship sender: " + sender.ToString());
                    Contact_Ship recipient = new Contact_Ship();

                    recipient.Address1 = order["Recipient Address 1"].ToString();
                    recipient.Address2 = order["Recipient Address 2"].ToString();
                    recipient.City = order["Recipient City"].ToString();
                    recipient.CompanyName = order["Recipient Company"].ToString();
                    recipient.Country = order["Recipient Country"].ToString();
                    recipient.State = order["Recipient State / Province"].ToString();
                    recipient.PostalCode = order["Recipient ZIP / Postal Code"].ToString();
                    if (data.OrdersTable.Columns.Contains("Recipient Contact"))
                    {
                        recipient.PersonName = order["Recipient Contact"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Recipient Tel"))
                    {
                        recipient.Telephone = order["Recipient Tel"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Recipient Email"))
                    {
                        recipient.Email = order["Recipient Email"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Recipient Tax Id"))
                    {
                        recipient.TaxID = order["Recipient Tax Id"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Recipient Address 3"))
                    {
                        recipient.Address3 = order["Recipient Address 3"].ToString();
                    }
                    //MessageBox.Show("Ship rec.: " + recipient.ToString());

                    shipReq.Recipient = recipient;
                    if (!String.IsNullOrEmpty(order["Billing Type"].ToString()) && !(order["Billing Type"].ToString().Equals("1")))
                    {
                        BillingOptions1 billing = new BillingOptions1();
                        switch (order["Billing Type"].ToString())
                        {
                            case "1":
                                {
                                    billing.BillingType = BillingType1.Prepaid;
                                    break;
                                }
                            case "2":
                                {
                                    billing.BillingType = BillingType1.Recipient;
                                    break;
                                }
                            case "3":
                                {
                                    billing.BillingType = BillingType1.ThirdParty;
                                    break;
                                }
                            case "4":
                                {
                                    billing.BillingType = BillingType1.ThirdParty;
                                    break;
                                }
                        }
                        if (data.OrdersTable.Columns.Contains("Billing Account"))
                        {
                            billing.BillingAccount = order["Billing Account"].ToString();
                        }

                        Contact_Ship billingAddress = new Contact_Ship();

                        if (data.OrdersTable.Columns.Contains("Billing Address 1"))
                        {
                            billingAddress.Address1 = order["Billing Address 1"].ToString();
                        }
                        if (data.OrdersTable.Columns.Contains("Billing Address 2"))
                        {
                            billingAddress.Address2 = order["Billing Address 2"].ToString();
                        }
                        if (data.OrdersTable.Columns.Contains("Billing City"))
                        {
                            billingAddress.City = order["Billing City"].ToString();
                        }
                        if (data.OrdersTable.Columns.Contains("Billing Company"))
                        {
                            billingAddress.CompanyName = order["Billing Company"].ToString();
                        }
                        if (data.OrdersTable.Columns.Contains("Billing Country"))
                        {
                            billingAddress.Country = order["Billing Country"].ToString();
                        }
                        if (data.OrdersTable.Columns.Contains("Billing State"))
                        {
                            billingAddress.State = order["Billing State"].ToString();
                        }
                        if (data.OrdersTable.Columns.Contains("Billing ZIP"))
                        {
                            billingAddress.PostalCode = order["Billing ZIP"].ToString();
                        }
                        if (data.OrdersTable.Columns.Contains("Billing Contact"))
                        {
                            billingAddress.PersonName = order["Billing Contact"].ToString();
                        }
                        if (data.OrdersTable.Columns.Contains("Billing Tel"))
                        {
                            billingAddress.Telephone = order["Billing Tel"].ToString();
                        }
                        if (data.OrdersTable.Columns.Contains("Billing Email"))
                        {
                            billingAddress.Email = order["Billing Email"].ToString();
                        }
                        //MessageBox.Show("Billing client: " + billingAddress.ToString());

                        billing.BillingAddress = billingAddress;
                        shipReq.Billing = billing;
                    }

                    if (!String.IsNullOrEmpty(carrier))
                    {
                        shipReq.CarrierId = Convert.ToInt32(carrier);
                    }
                    shipReq.OrderNumber = order_number;
                    if (data.OrdersTable.Columns.Contains("Shipment Reference Info"))
                    {
                        shipReq.ShipmentReference = order["Shipment Reference Info"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Shipment Reference Info 2"))
                    {
                        shipReq.ShipmentReference2 = order["Shipment Reference Info 2"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Shipment PO Number"))
                    {
                        shipReq.ShipmentPONumber = order["Shipment PO Number"].ToString();
                    }
                    ////check for DeconsolidationAddress, if it exists, then add it to request
                    //if (data.OrdersTable.Columns.Contains("DeconsolidationAddressIsSet"))
                    //{
                    //    //only if DeconsolidationAddressIsSet is set to true, add the Deconsol. Addr. to request otherwise it is a simple order / shipment
                    //    if (order["DeconsolidationAddressIsSet"].ToString().Equals("True") || order["DeconsolidationAddressIsSet"].ToString().Equals(true) || order["DeconsolidationAddressIsSet"].ToString().Equals("1"))
                    //    {
                    //        DeconsolidationAddress deconsAddr = new DeconsolidationAddress();
                    //        ConsolidationUnit_Request cu = new ConsolidationUnit_Request();
                    //        deconsAddr.Address1 = order["DeconsolidationAddressAddress1"].ToString();
                    //        deconsAddr.Address2 = order["DeconsolidationAddressAddress2"].ToString();
                    //        deconsAddr.City = order["DeconsolidationAddressCity"].ToString();
                    //        deconsAddr.CompanyName = order["DeconsolidationAddressCompanyName"].ToString();
                    //        deconsAddr.Country = order["DeconsolidationAddressCountry"].ToString();
                    //        deconsAddr.Email = order["DeconsolidationAddressEmail"].ToString();
                    //        deconsAddr.PersonName = order["DeconsolidationAddressContactName"].ToString();
                    //        deconsAddr.PostalCode = order["DeconsolidationAddressPostal"].ToString();
                    //        deconsAddr.State = order["DeconsolidationAddressProv"].ToString();
                    //        deconsAddr.Telephone = order["DeconsolidationAddressTel"].ToString();

                    //        cu.DeconsolidationAddress = deconsAddr;
                    //        cu.DistributionDate = data.OrdersTable.Columns.Contains("DistributionDate") ? Convert.ToDateTime(order["DistributionDate"] != null ? order["DistributionDate"] : DateTime.Today.AddDays(5)) : DateTime.Today.AddDays(5);
                    //        cu.CUCarrierID = Convert.ToInt32(order["CUCarrier"]);
                    //        cu.CUServiceCode = order["CUServiceCode"].ToString();
                    //       // req.ConsolidationUnit = cu;
                    //       // req.ConsolidationUnit.ApplyType = ApplyType.ByDeconsolidationAddress;
                    //    }
                    //}

                    //add package fields    
                    bool carrierIsLTLcarrier = false;
                    Package_Ship[] packagesLTL = new Package_Ship[order.GetOrdersSkidsTableRows().Length];

                    if (order.Carrier != null)
                    {
                        Data.CarriersRow carrierIsLTL = data.Carriers.FindByCode(order.Carrier);
                        if (carrierIsLTL != null)
                        {
                            if (carrierIsLTL.IsLTL)
                            {
                                carrierIsLTLcarrier = true;
                                Data.OrdersSkidsTableRow[] s = order.GetOrdersSkidsTableRows();
                                for (int i = 0; i < s.Length; i++)
                                {
                                    packagesLTL[i] = new Package_Ship();
                                    switch (s[i]["Skid Dimensions Type"].ToString())
                                    {
                                        case "C":
                                            {
                                                packagesLTL[i].DimensionType = DimensionType1.Centimeters;
                                                packagesLTL[i].WeightType = WeightType2.Kilograms;
                                                break;
                                            }
                                        case "I":
                                            {
                                                packagesLTL[i].DimensionType = DimensionType1.Inches;
                                                packagesLTL[i].WeightType = WeightType2.Pounds;
                                                break;
                                            }
                                    }
                                    packagesLTL[i].Weight = String.IsNullOrEmpty(s[i]["Skid Weight"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Weight"]);//if no weight specified, default to 0
                                     packagesLTL[i].Length = String.IsNullOrEmpty(s[i]["Skid Length"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Length"]);
                                     packagesLTL[i].Width = String.IsNullOrEmpty(s[i]["Skid Width"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Width"]);
                                    packagesLTL[i].Height = String.IsNullOrEmpty(s[i]["Skid Height"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Height"]);
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Insurance Amount"))
                                    {
                                        packagesLTL[i].InsuranceAmount = String.IsNullOrEmpty(s[i]["Skid Insurance Amount"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Insurance Amount"]);
                                    }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Insurance Currency"))
                                    {
                                        packagesLTL[i].InsuranceCurrency = s[i]["Skid Insurance Currency"] != null ? s[i]["Skid Insurance Currency"].ToString() : "";
                                    }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid PO Number"))
                                    {
                                        packagesLTL[i].PONumber = s[i]["Skid PO Number"].ToString();
                                    }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Reference1"))
                                    {
                                        packagesLTL[i].Reference1 = s[i]["Skid Reference1"].ToString();
                                    }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Reference2"))
                                    {
                                        packagesLTL[i].Reference2 = s[i]["Skid Reference2"].ToString();
                                    }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Freight Class"))
                                    {
                                        packagesLTL[i].FreightClassId = Convert.ToDecimal(s[i]["Skid Freight Class"]);
                                    }
                                }
                            }

                        }
                    }

                    //previous code
                    int packageNo = (Properties.Settings.Default.MultiOrderShipment == true) ? data.OrderPackagesTable.Select("[Package Weight] > '0'").Length : order.GetOrderPackagesTableRows().Length;
                    Package_Ship[] packages = new Package_Ship[packageNo];//[order.GetOrderPackagesTableRows().Length];
                    Data.OrderPackagesTableRow[] p = (Properties.Settings.Default.MultiOrderShipment == true) ? (data.OrderPackagesTable.Select("[Package Weight] > '0'") as Data.OrderPackagesTableRow[]): order.GetOrderPackagesTableRows();
                    for (int i = 0; i < p.Length; i++)
                    {
                        if (!p[i]["Package Weight"].ToString().Equals("0"))
                        {
                            packages[i] = new Package_Ship();
                            switch (p[i]["Package Dimensions Type"].ToString())
                            {
                                case "C":
                                    {
                                        packages[i].DimensionType = DimensionType1.Centimeters;
                                        packages[i].WeightType = WeightType2.Kilograms;
                                        break;
                                    }
                                case "I":
                                    {
                                        packages[i].DimensionType = DimensionType1.Inches;
                                        packages[i].WeightType = WeightType2.Pounds;
                                        break;
                                    }
                            }
                            packages[i].Weight = !String.IsNullOrEmpty(p[i]["Package Weight"].ToString()) ? Convert.ToDecimal(p[i]["Package Weight"]) : 0;//if no weight specified, default to 0
                            packages[i].Length = !String.IsNullOrEmpty(p[i]["Package Length"].ToString()) ? Convert.ToDecimal(p[i]["Package Length"]) : 0;
                            packages[i].Width = !String.IsNullOrEmpty(p[i]["Package Width"].ToString()) ? Convert.ToDecimal(p[i]["Package Width"]) : 0;
                            packages[i].Height = !String.IsNullOrEmpty(p[i]["Package Height"].ToString()) ? Convert.ToDecimal(p[i]["Package Height"]) : 0;
                            packages[i].InsuranceAmount = !String.IsNullOrEmpty(p[i]["Package Insurance Amount"].ToString()) ? Convert.ToDecimal(p[i]["Package Insurance Amount"]) : 0;
                            packages[i].InsuranceCurrency = !String.IsNullOrEmpty(p[i]["Package Insurance Currency"].ToString()) ? p[i]["Package Insurance Currency"].ToString() : "";

                            if (data.OrderPackagesTable.Columns.Contains("Package PO Number"))
                            {
                                packages[i].PONumber = p[i]["Package PO Number"].ToString();
                            }
                            if (data.OrderPackagesTable.Columns.Contains("Package Reference Info 1"))
                            {
                                packages[i].Reference1 = p[i]["Package Reference Info 1"].ToString();
                            }
                            if (data.OrderPackagesTable.Columns.Contains("Package Reference Info 2"))
                            {
                                packages[i].Reference2 = p[i]["Package Reference Info 2"].ToString();
                            }
                        }
                    }
                    //if (Properties.Settings.Default.MultiOrderShipment == true)
                    //{
                    //    Package_Ship[] packagesMulti = new Package_Ship[data.OrderPackagesTable.Rows.Count];
                    //    foreach(Data.OrderPackagesTableRow pack in data.OrderPackagesTable.Rows)
                    //    {
                    //        if (!pack["Order #"].ToString().Equals(order._Order__))
                    //        {

                    //        }
                    //    }
                    //}

                    if (carrierIsLTLcarrier)
                    {
                        shipReq.Packages = packagesLTL;
                    }
                    else
                    {
                        shipReq.Packages = packages;
                    }
                    //products / commodities fields -> req.InternationalOptions.Commodities
                    carrier = order["Carrier"].ToString();
                    string sService = order["Service"].ToString();
                    string serviceCode = "";
                    if (!String.IsNullOrEmpty(sService) && !String.IsNullOrEmpty(carrier))
                    {
                        serviceCode = data.Services.FindByUnique(sService).Service_Code;
                    }
                    if (!String.IsNullOrEmpty(serviceCode))
                    {
                        shipReq.ServiceCode = serviceCode;
                    }

                    ////upload the service code only, not the unique code -> it needs just the service code to rate in OnHold
                    // MessageBox.Show("Adding commodities");

                    Data.OrderProductsTableRow[] products = order.GetOrderProductsTableRows();
                    Commodity[] commodities = new Commodity[products.Length];
                    for (int k = 0; k < products.Length; k++)
                    {
                        commodities[k] = new Commodity();
                        commodities[k].Description = products[k]["Product Description"].ToString();
                        commodities[k].HarmonizedCode = products[k]["Product Harmonized Code"].ToString();
                        commodities[k].MadeInCountryCode = products[k]["Product Manufacture Country"].ToString();
                        commodities[k].Quantity = Convert.ToDecimal(products[k]["Product Quantity"]);
                         commodities[k].QuantityUnitOfMeasure = products[k]["Product Quantity MU"].ToString();
                        commodities[k].SKUOrItemOrUPS = (data.OrderProductsTable.Columns.Contains("Product SKU") ? products[k]["Product SKU"].ToString() : "");
                        commodities[k].UnitValue = (data.OrderProductsTable.Columns.Contains("Product Unit Value") ? Convert.ToDecimal(products[k]["Product Unit Value"]) : 0);
                        commodities[k].TotalWeight = (data.OrderProductsTable.Columns.Contains("Product Unit Weight") ? (products[k]["Product Unit Weight"] != null ? 1 : Convert.ToDecimal(products[k]["Product Unit Weight"]) * Convert.ToDecimal(products[k]["Product Quantity"])) : 0);
                    }
                    if (commodities != null)
                    {
                        InternationalOptions intOptions = new InternationalOptions();
                        shipReq.InternationalOptions = intOptions;
                    }
                    //MessageBox.Show("Adding special options");

                    string ship_date = order["Ship Date"].ToString();

                    shipReq.PickupDate = String.IsNullOrEmpty(ship_date) ? DateTime.Today : Convert.ToDateTime(ship_date);
                    if (carrier.Contains("17"))
                    {
                        if (shipReq.Billing != null)
                        {
                            if (shipReq.Billing.BillingAccount != null)
                            {
                                if (shipReq.Billing.BillingAccount.Length > 8)
                                {
                                    string splitBillAccount = shipReq.Billing.BillingAccount.Substring(8);
                                    shipReq.Billing.TransitNumber = splitBillAccount;
                                    splitBillAccount = shipReq.Billing.BillingAccount.Substring(0, 8);
                                    shipReq.Billing.BillingAccount = splitBillAccount;
                                }
                            }
                        }
                    }

                    //Shipment options
                    if (!String.IsNullOrEmpty(Properties.Settings.Default.ShipmentOptions))
                    {
                        if (carrier != null)
                        {
                            data.ShipmentOptions.Clear();
                            data.ShipmentOptions.ReadXml(Properties.Settings.Default.ShipmentOptions); //refresh the options values
                            Data.ShipmentOptionsRow[] rows = data.ShipmentOptions.GetCarrierOptions(carrier);
                            if (rows != null)
                            {
                                ShipmentOption1[] shipOptions = new ShipmentOption1[rows.Length];

                                for (int i = 0; i < rows.Length; i++)
                                {
                                    int optCode = Convert.ToInt32(rows[i]["OptionCode"]);
                                    if (optCode != 1140) //1140 is not a 2ship field id, is just a field we use in integr8, to enable FedEx Sig. Req.
                                    {
                                        if (!String.IsNullOrEmpty(rows[i]["Value"].ToString()) && !rows[i]["Value"].ToString().Equals("0"))
                                        {
                                            shipOptions[i] = new ShipmentOption1();
                                            shipOptions[i].code = Convert.ToInt32(rows[i]["OptionCode"]);
                                            shipOptions[i].value = rows[i]["Value"].ToString();
                                        }
                                    }
                                    if (data.OrdersTable.Columns.Contains("Signature Required"))
                                    {
                                        if (order["Signature Required"].ToString().Equals("1"))
                                        {
                                            switch (optCode)
                                            {
                                                    //FedEx
                                                case 1141:
                                                    {
                                                        shipOptions[i] = new ShipmentOption1();
                                                        shipOptions[i].code = 1141;
                                                        shipOptions[i].value = "1";
                                                        shipOptions[i+1] = new ShipmentOption1();
                                                        shipOptions[i+1].code = 1142;
                                                        shipOptions[i + 1].value = "SERVICE_DEFAULT";
                                                        i = i + 1;
                                                        break;
                                                    }
                                                    //CAPost
                                                case 1190:
                                                    //Dicom
                                                case 1447:
                                                    {
                                                        shipOptions[i] = new ShipmentOption1();
                                                        shipOptions[i].code = Convert.ToInt32(rows[i]["OptionCode"]);
                                                        shipOptions[i].value = "1";
                                                        break;
                                                    }
                                                    //DHL
                                                case 1375:
                                                    {
                                                        shipOptions[i] = new ShipmentOption1();
                                                        shipOptions[i].code = 1375;
                                                        shipOptions[i].value = "1";
                                                        shipOptions[i+1] = new ShipmentOption1();
                                                        shipOptions[i+1].code = 1374;
                                                        shipOptions[i+1].value = "SA";
                                                        i = i + 1;
                                                        break;
                                                    }
                                                    //Puro - default has OSNR now
                                                case 1904:
                                                    {
                                                        shipOptions[i] = new ShipmentOption1();
                                                        shipOptions[i].code = 1904;
                                                        shipOptions[i].value = "1";
                                                        break;
                                                    }
                                                    //UPS Shipment Level
                                                case 1218:
                                                    {
                                                        shipOptions[i] = new ShipmentOption1();
                                                        shipOptions[i].code = 1218;
                                                        shipOptions[i].value = "True";
                                                        shipOptions[i + 1] = new ShipmentOption1();
                                                        shipOptions[i + 1].code = 1219;
                                                        shipOptions[i + 1].value = "2";
                                                        i = i + 1;
                                                        break;
                                                    }
                                            }
                                        }
                                       
                                    }
                                }
                                shipReq.ShipOptions = shipOptions;
                            }
                        }
                    }
                    //MyCarrier
                    if (carrier.Equals("999"))
                    {
                        Data.MyCarrierRow[] carrier_row = data.MyCarrier.Select() as Data.MyCarrierRow[];
                        MyCarrierDetails myCarrier = new MyCarrierDetails();
                        myCarrier.ListPrice = Convert.ToDecimal(carrier_row[0].List_Tarrif.ToString().All(c => Char.IsDigit(c)) ? carrier_row[0].List_Tarrif.ToString() : "0");
                         myCarrier.DiscountValue = Convert.ToDecimal(carrier_row[0].Discount.ToString().All(c => Char.IsDigit(c)) ? carrier_row[0].Discount.ToString() : "0");
                        if (carrier_row[0].Discount_Type.ToString().Contains("#"))
                        {
                            myCarrier.DiscountType = DiscountType.FixedValue;
                        }
                        if (carrier_row[0].Discount_Type.ToString().Contains("%"))
                        {
                            myCarrier.DiscountType = DiscountType.Percent;
                        }
                        myCarrier.FeulValue = Convert.ToDecimal(carrier_row[0].Fuel.ToString().All(c => Char.IsDigit(c)) ? carrier_row[0].Fuel.ToString() : "0");
                        if (carrier_row[0].Fuel_Type.ToString().Contains("#"))
                        {
                            myCarrier.FuelType = FuelType.FixedValue;
                        }
                        if (carrier_row[0].Fuel_Type.ToString().Contains("%"))
                        {
                            myCarrier.FuelType = FuelType.Percent;
                        }
                        if (carrier_row[0].Small_Package)
                        {
                            myCarrier.ProNo_TrackingNo = carrier_row[0].Tracking_Number.Trim() == "" ? order._Order__ : carrier_row[0].Tracking_Number;
                            myCarrier.MyCarrierType = MyCarrierType.SmallPack;
                        }
                        if (carrier_row[0].Same_Day)
                        {
                            myCarrier.ProNo_TrackingNo = carrier_row[0].Tracking_Number.Trim() == "" ? order._Order__ : carrier_row[0].Tracking_Number;
                            myCarrier.MyCarrierType = MyCarrierType.SameDay;
                        }
                        if (carrier_row[0].LTL)
                        {
                            myCarrier.ProNo_TrackingNo = carrier_row[0].Pro_Number.Trim() == "" ? order._Order__ : carrier_row[0].Pro_Number;
                            myCarrier.MyCarrierType = MyCarrierType.LTL;
                        }
                        myCarrier.CarrierName = carrier_row[0].Carrier_Name;
                        myCarrier.ServiceName = carrier_row[0].Carrier_Service;
                   
                        shipReq.MycarrierDetails = myCarrier;

                        if (!carrier_row[0].Reuse)
                        {
                            data.MyCarrier.Clear();
                        }
                    }

                    LabelPrintPreferencesType l = new LabelPrintPreferencesType();
                    l.Encoding = EncodingType.PDF;
                    l.OutputFormat = OutputFormatType.Format_4x6;
                    shipReq.LabelPrintPreferences = l;
                 
                }
                if (type == InteGr8TaskType.Edit)
                {
                    // MessageBox.Show("Starting an Edit task");
                    editReq = new EditRequest();
                    //File.AppendAllText("Error.txt", "Carrier in orders table is: " + order["Carrier"].ToString());

                    string carrier = "";
                    File.AppendAllText("Error.txt", "Getting carrier now");
               
                    object carrierValue = order["Carrier"];
                    if (carrierValue == DBNull.Value)
                    {
                        File.AppendAllText("Error.txt", "Carrier is DBNull");
                        carrier = "";
                    }
                    else
                    {
                        carrier = order["Carrier"].ToString();
                    }
                    // add order fields(sender, recipient, billing, carrier & service)
                    //string name = col.ColumnName;
                    Contact_Hold1 sender = new Contact_Hold1();
                    sender.Address1 = order["Sender Address 1"].ToString();
                    sender.Address2 = order["Sender Address 2"].ToString();
                    sender.City = order["Sender City"].ToString();
                    sender.CompanyName = order["Sender Company"].ToString();
                    sender.Country = order["Sender Country"].ToString();
                    sender.State = order["Sender State / Province"].ToString();
                    sender.PostalCode = order["Sender ZIP / Postal Code"].ToString();
                    sender.PersonName = order["Sender Contact"].ToString();
                    sender.Telephone = order["Sender Tel"].ToString();
                    sender.Email = order["Sender Email"].ToString();
                    if (data.OrdersTable.Columns.Contains("Sender Tax Id"))
                    {
                        sender.TaxID = order["Sender Tax Id"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Sender Address 3"))
                    {
                        sender.Address3 = order["Sender Address 3"].ToString();
                    }
                    // MessageBox.Show("Sender is: " + sender.ToString());
                    editReq.Sender = sender;

                    Contact_Hold1 recipient = new Contact_Hold1();

                    recipient.Address1 = order["Recipient Address 1"].ToString();
                    recipient.Address2 = order["Recipient Address 2"].ToString();
                    recipient.City = order["Recipient City"].ToString();
                    recipient.CompanyName = order["Recipient Company"].ToString();
                    recipient.Country = order["Recipient Country"].ToString();
                    recipient.State = order["Recipient State / Province"].ToString();
                    recipient.PostalCode = order["Recipient ZIP / Postal Code"].ToString();
                    recipient.PersonName = order["Recipient Contact"].ToString();
                    recipient.Telephone = order["Recipient Tel"].ToString();
                    recipient.Email = order["Recipient Email"].ToString();
                    if (data.OrdersTable.Columns.Contains("Recipient Tax Id"))
                    {
                        recipient.TaxID = order["Recipient Tax Id"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Recipient Address 3"))
                    {
                        recipient.Address3 = order["Recipient Address 3"].ToString();
                    }
                    //MessageBox.Show("Recipient is: " + recipient.ToString());
                    editReq.Recipient = recipient;
                    BillingOptions3 billing = new BillingOptions3();
                    switch (order["Billing Type"].ToString())
                    {
                        case "1":
                            {
                                billing.BillingType = BillingType3.Prepaid;
                                break;
                            }
                        case "2":
                            {
                                billing.BillingType = BillingType3.Recipient;
                               break;
                            }
                        case "3":
                            {
                                billing.BillingType = BillingType3.ThirdParty;
                               break;
                            }
                        case "4":
                            {
                                billing.BillingType = BillingType3.ThirdParty;
                                break;
                            }
                    }
                    if (data.OrdersTable.Columns.Contains("Billing Account"))
                    {
                        billing.BillingAccount = order["Billing Account"].ToString();
                    }

                    Contact_Hold1 billingAddress = new Contact_Hold1();

                    if (data.OrdersTable.Columns.Contains("Billing Address 1"))
                    {
                        billingAddress.Address1 = order["Billing Address 1"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing Address 2"))
                    {
                        billingAddress.Address2 = order["Billing Address 2"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing City"))
                    {
                        billingAddress.City = order["Billing City"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing Company"))
                    {
                        billingAddress.CompanyName = order["Billing Company"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing Country"))
                    {
                        billingAddress.Country = order["Billing Country"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing State"))
                    {
                        billingAddress.State = order["Billing State"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing ZIP"))
                    {
                        billingAddress.PostalCode = order["Billing ZIP"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing Contact"))
                    {
                        billingAddress.PersonName = order["Billing Contact"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing Tel"))
                    {
                        billingAddress.Telephone = order["Billing Tel"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing Email"))
                    {
                        billingAddress.Email = order["Billing Email"].ToString();
                    }
                    if (billingAddress != null)
                    {
                        billing.BillingAddress = billingAddress;
                    }
                    if (billing != null)
                    {
                        editReq.Billing = billing;
                    }
                    // MessageBox.Show("Billinc client is: " + billing);
                    
                    if (!String.IsNullOrEmpty(carrier))
                    {
                        File.AppendAllText("Error.txt", "Carrier value is: " + carrier);
                        if (data.Carriers.FindByCode(carrier) != null)
                        editReq.CarrierId = Convert.ToInt32(carrier);
                    }
                    //MessageBox.Show("Order number is: " + order_number + " and has carrier: " + carrier);
                    editReq.OrderNumber = order_number;
                    if (data.OrdersTable.Columns.Contains("Shipment Reference Info"))
                    {
                        editReq.ShipmentReference = order["Shipment Reference Info"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Shipment Reference Info 2"))
                    {
                        editReq.ShipmentReference2 = order["Shipment Reference Info 2"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Shipment PO Number"))
                    {
                        editReq.ShipmentPONumber = order["Shipment PO Number"].ToString();
                    }
                    //add package fields    
                    bool carrierIsLTLcarrier = false;
                    Package_Hold1[] packagesLTL = new Package_Hold1[order.GetOrdersSkidsTableRows().Length];

                    if (order.Carrier != null)
                    {
                        Data.CarriersRow carrierIsLTL = data.Carriers.FindByCode(order.Carrier);
                        if (carrierIsLTL != null)
                        {
                            if (carrierIsLTL.IsLTL)
                            {
                                carrierIsLTLcarrier = true;
                                Data.OrdersSkidsTableRow[] s = order.GetOrdersSkidsTableRows();
                                for (int i = 0; i < s.Length; i++)
                                {
                                    packagesLTL[i] = new Package_Hold1();
                                    switch (s[i]["Skid Dimensions Type"].ToString())
                                    {
                                        case "C":
                                            {
                                                packagesLTL[i].DimensionType = DimensionType3.Centimeters;
                                                packagesLTL[i].WeightType = WeightType4.Kilograms;
                                                 break;
                                            }
                                        case "I":
                                            {
                                                packagesLTL[i].DimensionType = DimensionType3.Inches;
                                                packagesLTL[i].WeightType = WeightType4.Pounds;
                                                break;
                                            }
                                    }
                                    packagesLTL[i].Weight = String.IsNullOrEmpty(s[i]["Skid Weight"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Weight"]);//if no weight specified, default to 0
                                    packagesLTL[i].Length = String.IsNullOrEmpty(s[i]["Skid Length"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Length"]);
                                    packagesLTL[i].Width = String.IsNullOrEmpty(s[i]["Skid Width"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Width"]);
                                    packagesLTL[i].Height = String.IsNullOrEmpty(s[i]["Skid Height"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Height"]);
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Insurance Amount"))
                                    {
                                        packagesLTL[i].InsuranceAmount = String.IsNullOrEmpty(s[i]["Skid Insurance Amount"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Insurance Amount"]);
                                     }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Insurance Currency"))
                                    {
                                        packagesLTL[i].InsuranceCurrency = s[i]["Skid Insurance Currency"] != null ? s[i]["Skid Insurance Currency"].ToString() : "";
                                    } if (data.OrderPackagesTable.Columns.Contains("Skid PO Number"))
                                    {
                                        packagesLTL[i].PONumber = s[i]["Skid PO Number"].ToString();
                                    }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Reference1"))
                                    {
                                        packagesLTL[i].Reference1 = s[i]["Skid Reference1"].ToString();
                                    }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Reference2"))
                                    {
                                        packagesLTL[i].Reference2 = s[i]["Skid Reference2"].ToString();
                                    }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Freight Class"))
                                    {
                                        packagesLTL[i].FreightClassId = Convert.ToDecimal(s[i]["Skid Freight Class"]);
                                    }
                                }
                            }

                        }
                    }

                    Package_Hold1[] packages = new Package_Hold1[order.GetOrderPackagesTableRows().Length];
                    Data.OrderPackagesTableRow[] p = order.GetOrderPackagesTableRows();
                    //MessageBox.Show("Adding the packages:" + packages.Length);
                    for (int i = 0; i < p.Length; i++)
                    {
                        packages[i] = new Package_Hold1();
                        switch (p[i]["Package Dimensions Type"].ToString())
                        {
                            case "C":
                                {
                                    packages[i].DimensionType = DimensionType3.Centimeters;
                                     packages[i].WeightType = WeightType4.Kilograms;
                                    break;
                                }
                            case "I":
                                {
                                    packages[i].DimensionType = DimensionType3.Inches;
                                    packages[i].WeightType = WeightType4.Pounds;
                                    break;
                                }
                        }
                        packages[i].Weight = !String.IsNullOrEmpty(p[i]["Package Weight"].ToString()) ? Convert.ToDecimal(p[i]["Package Weight"]) : 0;//if no weight specified, default to 0
                        packages[i].Length = !String.IsNullOrEmpty(p[i]["Package Length"].ToString()) ? Convert.ToDecimal(p[i]["Package Length"]) : 0;
                         packages[i].Width = !String.IsNullOrEmpty(p[i]["Package Width"].ToString()) ? Convert.ToDecimal(p[i]["Package Width"]) : 0;
                        packages[i].Height = !String.IsNullOrEmpty(p[i]["Package Height"].ToString()) ? Convert.ToDecimal(p[i]["Package Height"]) : 0;
                        packages[i].InsuranceAmount = !String.IsNullOrEmpty(p[i]["Package Insurance Amount"].ToString()) ? Convert.ToDecimal(p[i]["Package Insurance Amount"]) : 0;
                        packages[i].InsuranceCurrency = !String.IsNullOrEmpty(p[i]["Package Insurance Currency"].ToString()) ? p[i]["Package Insurance Currency"].ToString() : "";
                        packages[i].DimensionCode = packages[i].Length + "X" + packages[i].Width + "X" + packages[i].Height;
                        if (data.OrderPackagesTable.Columns.Contains("Package PO Number"))
                        {
                            packages[i].PONumber = p[i]["Package PO Number"].ToString();
                        }
                        if (data.OrderPackagesTable.Columns.Contains("Package Reference Info 1"))
                        {
                            packages[i].Reference1 = p[i]["Package Reference Info 1"].ToString();
                        }
                        if (data.OrderPackagesTable.Columns.Contains("Package Reference Info 2"))
                        {
                            packages[i].Reference2 = p[i]["Package Reference Info 2"].ToString();
                        }
                    }

                    if (carrierIsLTLcarrier)
                    {
                        editReq.Packages = packagesLTL;
                    }
                    else
                    {
                        editReq.Packages = packages;
                    }
                    //products / commodities fields -> req.InternationalOptions.Commodities


                    //string
                    carrier = order["Carrier"].ToString();
                    string sService = order["Service"].ToString();
                    string serviceCode = "";
                    if (!String.IsNullOrEmpty(sService) && !String.IsNullOrEmpty(carrier))
                    {
                        serviceCode = data.Services.FindByUnique(sService) == null ? "" : data.Services.FindByUnique(sService).Service_Code;
                    }
                    if (!String.IsNullOrEmpty(serviceCode))
                    {
                        editReq.ServiceCode = serviceCode;
                    }
                    ////upload the service code only, not the unique code -> it needs just the service code to rate in OnHold

                    // remove all the commodities from the editRequest with zero quantity, and redo the count and customs sum
                    Data.OrderProductsTableRow[] products = order.GetOrderProductsTableRows();
                  
                    // MessageBox.Show("Adding commodities");
                    Commodity2[] commodities = new Commodity2[products.Length];
                    for (int k = 0; k < products.Length; k++)
                    {
                        commodities[k] = new Commodity2();
                        commodities[k].Description = products[k]["Product Description"].ToString();
                        commodities[k].HarmonizedCode = products[k]["Product Harmonized Code"].ToString();
                        commodities[k].MadeInCountryCode = products[k]["Product Manufacture Country"].ToString();
                        commodities[k].Quantity = Convert.ToDecimal(products[k]["Product Quantity"] != null ? products[k]["Product Quantity"] : 0);
                        commodities[k].QuantityUnitOfMeasure = products[k]["Product Quantity MU"].ToString();
                        commodities[k].SKUOrItemOrUPS = (data.OrderProductsTable.Columns.Contains("Product SKU") ? products[k]["Product SKU"].ToString() : "");
                        commodities[k].UnitValue = (data.OrderProductsTable.Columns.Contains("Product Unit Value") ? Convert.ToDecimal(products[k]["Product Unit Value"]) : 0);
                        commodities[k].TotalWeight = (data.OrderProductsTable.Columns.Contains("Product Unit Weight") ? (products[k]["Product Unit Weight"] != null ? 1 : Convert.ToDecimal(products[k]["Product Unit Weight"]) * Convert.ToDecimal(products[k]["Product Quantity"])) : 0);
                    }
                    //MessageBox.Show("Commodities added");

                    if (commodities != null)
                    {
                        InternationalOptions2 intOptions = new InternationalOptions2();
                        editReq.InternationalOptions = intOptions;
                    }
                    // MessageBox.Show("Adding ship date: " + order["Ship Date"].ToString());

                    string ship_date = order["Ship Date"].ToString();

                    editReq.PickupDate = String.IsNullOrEmpty(ship_date) ? DateTime.Today : Convert.ToDateTime(ship_date);

                    if (carrier.Contains("17"))
                    {
                        if (editReq.Billing != null)
                        {
                            if (editReq.Billing.BillingAccount != null)
                            {
                                if (editReq.Billing.BillingAccount.Length > 8)
                                {
                                    string splitBillAccount = editReq.Billing.BillingAccount.Substring(8);
                                    editReq.Billing.TransitNumber = splitBillAccount;
                                    splitBillAccount = editReq.Billing.BillingAccount.Substring(0, 8);
                                    editReq.Billing.BillingAccount = splitBillAccount;
                                }
                            }
                        }
                     }

                    //Shipment options
                    // MessageBox.Show("Shipment Options: " + String.IsNullOrEmpty(Properties.Settings.Default.ShipmentOptions));

                    if (!String.IsNullOrEmpty(Properties.Settings.Default.ShipmentOptions))
                    {
                        if (carrier != null)
                        {
                            data.ShipmentOptions.Clear();
                            data.ShipmentOptions.ReadXml(Properties.Settings.Default.ShipmentOptions); //refresh the options values
                            Data.ShipmentOptionsRow[] rows = data.ShipmentOptions.GetCarrierOptions(carrier);
                            if (rows != null)
                            {
                                ShipmentOption3[] shipOptions = new ShipmentOption3[rows.Length];
                                for (int i = 0; i < rows.Length; i++)
                                {
                                    int optCode = Convert.ToInt32(rows[i]["OptionCode"]);
                                    if (optCode != 1140)
                                    {
                                        if (!String.IsNullOrEmpty(rows[i]["Value"].ToString()) && !rows[i]["Value"].ToString().Equals("0"))
                                        {
                                            shipOptions[i] = new ShipmentOption3();
                                            shipOptions[i].code = Convert.ToInt32(rows[i]["OptionCode"]);
                                            shipOptions[i].value = rows[i]["Value"].ToString();
                                        }
                                    }
                                    if (data.OrdersTable.Columns.Contains("Signature Required"))
                                    {
                                        if (order["Signature Required"].ToString().Equals("1"))
                                        {
                                           switch (optCode)
                                            {
                                                   //FedEx
                                                case 1141:
                                                    {
                                                        shipOptions[i] = new ShipmentOption3();
                                                        shipOptions[i].code = 1141;
                                                        shipOptions[i].value = "1";
                                                        shipOptions[i + 1] = new ShipmentOption3();
                                                        shipOptions[i + 1].code = 1142;
                                                        shipOptions[i + 1].value = "SERVICE_DEFAULT";
                                                        i = i + 1;
                                                        break;
                                                    }
                                                  //CAPost
                                                case 1190:
                                                   //Dicom
                                                case 1447:
                                                    {
                                                        shipOptions[i] = new ShipmentOption3();
                                                        shipOptions[i].code = Convert.ToInt32(rows[i]["OptionCode"]);
                                                        shipOptions[i].value = "1";
                                                        break;
                                                    }
                                                   //DHL
                                                case 1375:
                                                    {
                                                        shipOptions[i] = new ShipmentOption3();
                                                        shipOptions[i].code = 1375;
                                                        shipOptions[i].value = "1";
                                                        shipOptions[i + 1] = new ShipmentOption3();
                                                        shipOptions[i + 1].code = 1374;
                                                        shipOptions[i + 1].value = "SA";
                                                        i = i + 1;
                                                        break;
                                                    }
                                                   //Puro has OSNR default now
                                                case 1904:
                                                    {
                                                        shipOptions[i] = new ShipmentOption3();
                                                        shipOptions[i].code = 1904;
                                                        shipOptions[i].value = "1";
                                                        break;
                                                    }
                                                //UPS Shipment Level
                                                case 1218:
                                                    {
                                                        shipOptions[i] = new ShipmentOption3();
                                                        shipOptions[i].code = 1218;
                                                        shipOptions[i].value = "True";
                                                        shipOptions[i + 1] = new ShipmentOption3();
                                                        shipOptions[i + 1].code = 1219;
                                                        shipOptions[i + 1].value = "2";
                                                        i = i + 1;
                                                        break;
                                                    }
                                            }
                                        }
                                    }
                                }
                                editReq.ShipOptions = shipOptions;
                            }
                        }
                    }
           
                }
                if (type == InteGr8TaskType.Rate || type == InteGr8TaskType.Rate2 || type == InteGr8TaskType.RateCheapestService || type == InteGr8TaskType.RateFastestService)
                {
                    rateReq = new RateRequest();
                    string carrier = order["Carrier"] == null ? "" : order["Carrier"].ToString();

                    // add order fields(sender, recipient, billing, carrier & service)
                    Contact_Rate sender = new Contact_Rate();
                    sender.Address1 = order["Sender Address 1"].ToString();
                    sender.City = order["Sender City"].ToString();
                    sender.Country = order["Sender Country"].ToString();
                    sender.State = order["Sender State / Province"].ToString();
                    sender.PostalCode = order["Sender ZIP / Postal Code"].ToString();
                    sender.CompanyName = order["Sender Company"].ToString();
                    
                    rateReq.Sender = sender;

                    Contact_Rate recipient = new Contact_Rate();

                    recipient.Address1 = order["Recipient Address 1"].ToString();
                    recipient.City = order["Recipient City"].ToString();
                    recipient.Country = order["Recipient Country"].ToString();
                    recipient.State = order["Recipient State / Province"].ToString();
                    recipient.PostalCode = order["Recipient ZIP / Postal Code"].ToString();
                    recipient.CompanyName = order["Recipient Company"].ToString();
                    recipient.IsResidential = data.OrdersTable.Columns.Contains("Recipient Residential") ? Convert.ToBoolean(order["Recipient Residential"].ToString()) : false;
                    rateReq.Recipient = recipient;
                    BillingOptions billing = new BillingOptions();
                    switch (order["Billing Type"].ToString())
                    {
                        case "1":
                            {
                                billing.BillingType = BillingType.Prepaid;
                               break;
                            }
                        case "2":
                            {
                                billing.BillingType = BillingType.Recipient;
                                break;
                            }
                        case "3":
                            {
                                billing.BillingType = BillingType.ThirdParty;
                                break;
                            }
                        case "4":
                            {
                                billing.BillingType = BillingType.ThirdParty;
                                break;
                            }
                    }
                    if (data.OrdersTable.Columns.Contains("Billing Account"))
                    {
                        billing.BillingAccount = order["Billing Account"].ToString();
                    }

                    Contact_Rate billingAddress = new Contact_Rate();

                    if (data.OrdersTable.Columns.Contains("Billing Address 1"))
                    {
                        billingAddress.Address1 = order["Billing Address 1"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing City"))
                    {
                        billingAddress.City = order["Billing City"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing Country"))
                    {
                        billingAddress.Country = order["Billing Country"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing State"))
                    {
                        billingAddress.State = order["Billing State"].ToString();
                    }
                    if (data.OrdersTable.Columns.Contains("Billing ZIP"))
                    {
                        billingAddress.PostalCode = order["Billing ZIP"].ToString();
                    }

                    billing.BillingAddress = billingAddress;
                    rateReq.Billing = billing;
                    if (!String.IsNullOrEmpty(carrier) && type == InteGr8TaskType.Rate2)
                    {
                        rateReq.CarrierId = Convert.ToInt32(carrier);
                    }
                    //add package fields    
                    bool carrierIsLTLcarrier = false;
                    Package_Rate[] packagesLTL = new Package_Rate[order.GetOrdersSkidsTableRows().Length];

                    if (order.Carrier != null)
                    {
                        Data.CarriersRow carrierIsLTL = data.Carriers.FindByCode(order.Carrier);
                        if (carrierIsLTL != null)
                        {
                            if (carrierIsLTL.IsLTL)
                            {
                                carrierIsLTLcarrier = true;
                                Data.OrdersSkidsTableRow[] s = order.GetOrdersSkidsTableRows();
                                for (int i = 0; i < s.Length; i++)
                                {
                                    packagesLTL[i] = new Package_Rate();
                                    switch (s[i]["Skid Dimensions Type"].ToString())
                                    {
                                        case "C":
                                            {
                                                packagesLTL[i].DimensionType = DimensionType.Centimeters;
                                                packagesLTL[i].WeightType = WeightType.Kilograms;
                                                break;
                                            }
                                        case "I":
                                            {
                                                packagesLTL[i].DimensionType = DimensionType.Inches;
                                                packagesLTL[i].WeightType = WeightType.Pounds;
                                                break;
                                            }
                                    }
                                    packagesLTL[i].Weight = String.IsNullOrEmpty(s[i]["Skid Weight"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Weight"]);//if no weight specified, default to 0
                                   packagesLTL[i].Length = String.IsNullOrEmpty(s[i]["Skid Length"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Length"]);
                                    packagesLTL[i].Width = String.IsNullOrEmpty(s[i]["Skid Width"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Width"]);
                                    packagesLTL[i].Height = String.IsNullOrEmpty(s[i]["Skid Height"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Height"]);
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Insurance Amount"))
                                    {
                                        packagesLTL[i].InsuranceAmount = String.IsNullOrEmpty(s[i]["Skid Insurance Amount"].ToString()) ? 0 : Convert.ToDecimal(s[i]["Skid Insurance Amount"]);
                                     }
                                    if (data.OrderPackagesTable.Columns.Contains("Skid Insurance Currency"))
                                    {
                                        packagesLTL[i].InsuranceCurrency = s[i]["Skid Insurance Currency"] != null ? s[i]["Skid Insurance Currency"].ToString() : "";
                                    } if (data.OrderPackagesTable.Columns.Contains("Skid Freight Class"))
                                    {
                                        packagesLTL[i].FreightClassId = Convert.ToDecimal(s[i]["Skid Freight Class"]);
                                    }
                                 }
                            }

                        }
                    }

                    Package_Rate[] packages = new Package_Rate[order.GetOrderPackagesTableRows().Length];
                    Data.OrderPackagesTableRow[] p = order.GetOrderPackagesTableRows();
                    for (int i = 0; i < p.Length; i++)
                    {
                        packages[i] = new Package_Rate();
                        switch (p[i]["Package Dimensions Type"].ToString())
                        {
                            case "C":
                                {
                                    packages[i].DimensionType = DimensionType.Centimeters;
                                    packages[i].WeightType = WeightType.Kilograms;
                                    break;
                                }
                            case "I":
                                {
                                    packages[i].DimensionType = DimensionType.Inches;
                                    packages[i].WeightType = WeightType.Pounds;
                                    break;
                                }
                        }
                        packages[i].Weight = !String.IsNullOrEmpty(p[i]["Package Weight"].ToString()) ? Convert.ToDecimal(p[i]["Package Weight"]) : 0;//if no weight specified, default to 0
                        packages[i].Length = !String.IsNullOrEmpty(p[i]["Package Length"].ToString()) ? Convert.ToDecimal(p[i]["Package Length"]) : 0;
                        packages[i].Width = !String.IsNullOrEmpty(p[i]["Package Width"].ToString()) ? Convert.ToDecimal(p[i]["Package Width"]) : 0;
                        packages[i].Height = !String.IsNullOrEmpty(p[i]["Package Height"].ToString()) ? Convert.ToDecimal(p[i]["Package Height"]) : 0;
                        packages[i].InsuranceAmount = !String.IsNullOrEmpty(p[i]["Package Insurance Amount"].ToString()) ? Convert.ToDecimal(p[i]["Package Insurance Amount"]) : 0;
                        packages[i].InsuranceCurrency = !String.IsNullOrEmpty(p[i]["Package Insurance Currency"].ToString()) ? p[i]["Package Insurance Currency"].ToString() : "";
                      }

                    if (carrierIsLTLcarrier)
                    {
                        rateReq.Packages = packagesLTL;
                    }
                    else
                    {
                        rateReq.Packages = packages;
                    }

                    carrier = order["Carrier"] == null ? "" : order["Carrier"].ToString();
                    string sService = order["Service"] == null ? "" : order["Service"].ToString();
                    string serviceCode = "";
                    if (!String.IsNullOrEmpty(sService) && !String.IsNullOrEmpty(carrier))
                    {
                        serviceCode = data.Services.FindByUnique(sService) == null ? "" : data.Services.FindByUnique(sService).Service_Code;
                    }
                    if (!String.IsNullOrEmpty(serviceCode) && type == InteGr8TaskType.Rate2)
                    {
                        rateReq.ServiceCode = serviceCode;
                    }
                    string ship_date = order["Ship Date"].ToString();

                    rateReq.PickupDate = String.IsNullOrEmpty(ship_date) ? DateTime.Today : Convert.ToDateTime(ship_date);
                    //if (carrier.Contains("17"))
                    //{
                    //    if (rateReq.Billing != null)
                    //    {
                    //        if (rateReq.Billing.BillingAccount != null)
                    //        {
                    //            if (rateReq.Billing.BillingAccount.Length > 8)
                    //            {

                    //            }
                    //        }
                    //    }

                    //}

                    //Shipment options
                    RateOptions rateOptions = new RateOptions();
                    if (!String.IsNullOrEmpty(Properties.Settings.Default.ShipmentOptions))
                    {
                        if (carrier != null)
                        {
                            data.ShipmentOptions.Clear();
                            data.ShipmentOptions.ReadXml(Properties.Settings.Default.ShipmentOptions); //refresh the options values
                            Data.ShipmentOptionsRow[] rows = data.ShipmentOptions.GetCarrierOptions(carrier);
                            if (rows != null)
                            {
                               
                                for (int i = 0; i < rows.Length; i++)
                                {
                                    if (!String.IsNullOrEmpty(rows[i]["Value"].ToString()) && !rows[i]["Value"].ToString().Equals("0"))
                                    {
                                        rateOptions = new RateOptions();

                                        //CollectOnDelivery
                                        if (rows[i]["CarrierCode"].ToString().Equals(carrier))
                                        {
                                            switch (rows[i]["OptionCode"].ToString())
                                            {
                                                case "1074":
                                                case "1221":
                                                case "1198":
                                                case "1419":
                                                    {
                                                        COD cod = new COD();
                                                        cod.Ammount = Convert.ToDecimal(rows[i]["Value"]);
                                                        rateOptions.CollectOnDelivery = cod;
                                                        break;
                                                    }
                                                case "1125":
                                                case "1917":
                                                case "1938":
                                                case "1216":
                                                case "1370":
                                                case "1417":
                                                    {
                                                        rateOptions.SaturdayDelivery = true;
                                                        break;
                                                    }
                                                case "1126":
                                                case "1918":
                                                case "1217":
                                                case "1371":
                                                    {
                                                        rateOptions.SaturdayPickUp = true;
                                                         break;
                                                    }
                                                case "1141":
                                                case "1904":
                                                case "1190":
                                                case "1447":
                                                case "1375":
                                                    {
                                                        rateOptions.SignatureRequired = true;
                                                         break;
                                                    }
                                                case "1942":
                                                    {
                                                        rateOptions.SignatureRequired = false;
                                                        break;
                                                    }

                                            }
                                        }
                                       }
                                }
                                rateReq.ShipmentOptions = rateOptions;

                            }
                        }
                    }
                    if (data.OrdersTable.Columns.Contains("Signature Required"))
                    {
                        if (order["Signature Required"].ToString().Equals("1"))
                        {
                            if (rateOptions == null)
                            {
                                rateOptions = new RateOptions();
                            }
                           rateOptions.SignatureRequired = true;
                            
                        }
                    }
                    rateReq.ShipmentOptions = rateOptions;
                }

                if (type == InteGr8TaskType.GetBackEditResults)
                {
                    getReq = new GetShipmentsRequest();
                    //if (Template.Name.Contains("Chocolate Inn"))
                    //{
                    //    string trim_order_number = order_number.Trim('0');
                    //    getReq.OrderNumber = trim_order_number;
                    //}
                    //else
                    //{
                    getReq.Type = GetShipmentType.ByOrderNumber;
                    getReq.OrderNumber = order_number;
                    //}

                }

                //if (type == InteGr8TaskType.Delete)
                //{
                //    delReq = new DeleteRequest();
                //    delReq.DeleteType = DeleteType.ByTrackingNumber;

                //}
            }

            catch (Exception e)
            {
                this.exception = new Exception("Error preparing " + type.ToString() + " task for order # '" + order_number + "' " + "Error message is: " + e.Message, e);
                Utils.SendError("Exception preparing task", this.exception);
                throw this.exception;
            }
        }

        public void Process()
        {
            try
            {
                
                WSKey key = null;
                WSKey wsKey_backup = InteGr8_Main.key;
                if (InteGr8_Main.key == null)
                {
                    if (data.OrdersTable.Columns.IndexOf("Token") > -1)
                    {
                        Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_number);
                        string currentKey = order["Token"].ToString();
                        Data.ClientsRow client = data.Clients.FindByClientID(order["Client ID"].ToString());
                        InteGr8_Main.key = new WSKey();//(Convert.ToInt32(client.ClientID), client.Login, client.Pwd);
                        InteGr8_Main.key.MyWSKey = currentKey;
                    }
                }

                key = InteGr8_Main.key;

                //http://ship.2ship.com/SOAP.svc/ws  https://neopost.2ship.com/SOAP.svc/ws
                using (I2ShipServiceClient ws = new I2ShipServiceClient())// TwoShipService())//(TwoShipService ws = new TwoShipService())
                {
                    //ws.Timeout = 300000;
                    //GetCarrierRequest carrierReq = new GetCarrierRequest();

                    //carrierReq.CarrierId = 30;
                    //carrierReq.WS_Key = key.MyWSKey;
                    
                    //CarrierReply _carrierReply = ws.GetCarrierInfo(carrierReq);
                    //foreach (ShipmentOption so in _carrierReply.Options)
                    //{
                    //    data.ShipmentOptions.AddShipmentOptionsRow(so.code.ToString(), "30", "0", "0");
                    //}

                    switch (type)
                    {
                        case InteGr8TaskType.Edit:
                            {
                                try
                                {
                                    editReq.WS_Key = key.MyWSKey;
                                    editReq.LocationId = key.getLocationID(editReq.Sender.Country, editReq.Sender.State, editReq.Sender.City, editReq.Sender.PostalCode);
                                    //editReq.LocationIdSpecified = true;
                                    var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(editReq);
                                    Utils.Write(Utils.orders_ok, json);
                              
                                    _editResult = ws.GetEditURL(editReq);

                                    if (String.IsNullOrEmpty(_editResult.ToString()))
                                    {
                                        throw new Exception("Empty result received from the import webservice.");
                                    }
                                    if (_editResult.ToString().StartsWith("#"))
                                    {
                                        throw new Exception("#Error received from the import webservice: " + _editResult.ToString().Substring(1));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Exception at order import for editing in 2ship:\n" + ex.Message);
                                }
                                break;
                            }
                        //case InteGr8TaskType.Delete:
                        //    {
                        //        try
                        //        {
                        //            delReq.WS_Key = key.MyWSKey;
                        //            delReq.DeleteType = DeleteType.ByTrackingNumber;

                        //            ws.DeleteShipment(delReq);
                        //        }
                        //        catch (Exception ex)
                        //        {
                        //            MessageBox.Show("Exception at shipment delete:\n" + ex.Message);
                        //        }
                        //        break;
                        //    }
                        case InteGr8TaskType.GetBackEditResults:
                            try
                            {
                                getReq.WS_Key = key.MyWSKey;
                                //getReq.Type = GetShipmentType.ByTrackingNumber;
                                //getReq.TrackingNumber = "LSHP00917711";//"LSHP00911978";
                                _shipResult = new CarrierShipResponse[1];
                                _shipResult = ws.GetShipments(getReq);
                            }
                            catch (Exception ex)
                            {
                                if (!ex.Message.Contains("Deleted") && ! ex.Message.Contains("No shipments found"))
                                {
                                    MessageBox.Show("Exception at get shipment info:\n" + ex.Message);
                                }

                            }
                            break;

                        case InteGr8TaskType.Ship:
                            //request.Add(522, 0, "1"); 
                            try
                            {
                                //MessageBox.Show("Sending Ship request");
                                shipReq.WS_Key = key.MyWSKey;
                                shipReq.LocationId = key.getLocationID(shipReq.Sender.Country, shipReq.Sender.State, shipReq.Sender.City, shipReq.Sender.PostalCode);
                            
                                _shipResult = new CarrierShipResponse[1];

                                var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(shipReq);

                                var log = Path.Combine(ConfigurationManager.AppSettings["UploaderErrorPath"], shipReq.OrderNumber + "_log.txt");
                                Utils.Write(log, json);
                                _shipResult[0] = ws.Ship(shipReq);
                            }
                            catch (Exception ex)
                            {
                                Utils.Write(Utils.orders_error, ex.Message, ex.StackTrace);
                                MessageBox.Show("Exception at ship:\n" + ex.Message);
                            }
                            break;

                        case InteGr8TaskType.Rate:
                            {
                                try
                                {
                                    rateReq.WS_Key = key.MyWSKey;
                                    rateReq.LocationId = key.getLocationID(rateReq.Sender.Country, rateReq.Sender.State, rateReq.Sender.City, rateReq.Sender.PostalCode);
                                     rateReq.RateFilter = RateFilterType.GetAllServices;

                                     var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(rateReq);
                                     var log = Path.Combine(ConfigurationManager.AppSettings["UploaderErrorPath"],
                                         DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_rate.txt");
                                     Utils.WriteLine(log, json);
                               
                                    rates = ws.RateAllCarriers(rateReq);
                                }
                                catch (Exception rateException)
                                {
                                    Utils.Write(Utils.orders_error, rateException.Message, rateException.StackTrace);
                                    MessageBox.Show("Exception at rating with all carriers:\n" + rateException.Message);
                                }
                                break;

                            }
                        case InteGr8TaskType.Rate2:
                            {
                                try
                                {
                                    rateReq.WS_Key = key.MyWSKey;
                                    rateReq.LocationId = key.getLocationID(rateReq.Sender.Country, rateReq.Sender.State, rateReq.Sender.City, rateReq.Sender.PostalCode);
                                    //rateReq.LocationIdSpecified = true;
                                    //rateReq.RateFilter = RateFilterType.GetAllServices;
                                    rates = ws.RateAllCarriers(rateReq);
                                }
                                catch (Exception rateException)
                                {
                                    Utils.Write(Utils.orders_error, rateException.Message, rateException.StackTrace);
                                    MessageBox.Show("Exception at get current rate:\n" + rateException.Message);
                                }
                                break;

                            }
                        case InteGr8TaskType.RateCheapestService:
                            {
                                try
                                {
                                    rateReq.WS_Key = key.MyWSKey;
                                    rateReq.LocationId = key.getLocationID(rateReq.Sender.Country, rateReq.Sender.State, rateReq.Sender.City, rateReq.Sender.PostalCode);
                                     rateReq.RateFilter = RateFilterType.GetJustCheapestService;

                                     var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(rateReq);
                         
                                    rates = ws.RateAllCarriers(rateReq);
                                }
                                catch (Exception rateException)
                                {
                                    Utils.Write(Utils.orders_error, rateException.Message, rateException.StackTrace);
                                    if (rateException.Message.Contains("Sequence contains no elements"))
                                    {
                                        MessageBox.Show("All rates for this order have 0 cost, so there is no cheapest rate available", "Exception at rate cheapest", MessageBoxButtons.OK);
                                       // MessageBox.Show("Exception at get cheapest rate:\n" + rateException.Message);
                                    }
                                }
                                break;

                            }
                        case InteGr8TaskType.RateFastestService:
                            {
                                try
                                {
                                    rateReq.WS_Key = key.MyWSKey;
                                    rateReq.LocationId = key.getLocationID(rateReq.Sender.Country, rateReq.Sender.State, rateReq.Sender.City, rateReq.Sender.PostalCode);
                                    //rateReq.LocationIdSpecified = true;
                                    //remove carrier & service from request, to get fastest from all rates
                                    //rateReq.CarrierId = 0;
                                    //rateReq.CarrierIdSpecified = false;
                                    //rateReq.ServiceCode = "";
                                    rateReq.RateFilter = RateFilterType.GetJustFastestService;

                                    var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(rateReq);
                         
                                    rates = ws.RateAllCarriers(rateReq);
                                }
                                catch (Exception rateException)
                                {
                                    Utils.Write(Utils.orders_error, rateException.Message, rateException.StackTrace);
                                    if (rateException.Message.Contains("Sequence contains no elements"))
                                    {
                                        MessageBox.Show("All rates for this order have no delivery date attached, so there is no fastest rate available", "Exception at rate fastest", MessageBoxButtons.OK);
                                        // MessageBox.Show("Exception at get cheapest rate:\n" + rateException.Message);
                                    }
                                    //MessageBox.Show("Exception at get fastest rate:\n" + rateException.Message);
                                }
                                break;

                            }

                    }
                    InteGr8_Main.key= wsKey_backup;
                    //if (_reply != null && _reply.Count > 0)
                    //{
                    //    bool hasToken = false;
                    //    foreach (Field f in _reply)
                    //    {
                    //        if (f.Code.Equals(5000))
                    //        {
                    //            hasToken = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!hasToken)
                    //    {
                    //        _reply.Add(5000, 0, token);
                    //    }
                    //}
                }

                //using (ShipService ws = new ShipService())
                //{
                //    ws.Timeout = 600000;
                //    switch (type)
                //    {
                //        case InteGr8TaskType.Edit:

                //            _result = ws.Edit(token, request.ToArray());
                //            if (String.IsNullOrEmpty(_result))
                //            {
                //                throw new Exception("Empty result received from the import webservice.");
                //            }
                //            if (_result.StartsWith("#"))
                //            {
                //                throw new Exception("#Error received from the import webservice: " + _result.Substring(1));
                //            }
                //            break;

                //        case InteGr8TaskType.Delete:
                //            _result = ws.Delete(token, order_number);
                //            break;

                //        case InteGr8TaskType.GetBackEditResults:
                //            _reply = new Fields(ws.ShipmentInfo(token, order_number));

                //            if (_reply[185, ""].Equals("")) _reply = null;

                //            break;

                //        case InteGr8TaskType.Ship:
                //            //request.Add(522, 0, "1"); 
                //            _reply = new Fields(ws.Process(token, Convert.ToInt32(InteGr8TaskType.Ship), request.ToArray()));
                //            break;

                //        case InteGr8TaskType.Rate:
                //        case InteGr8TaskType.RateCheapestService:
                //        case InteGr8TaskType.RateFastestService:
                //            // remove the selected carrier and service to request a rate with all services
                //            System.Diagnostics.Debug.WriteLine("Time before requesting the rate: " + DateTime.Now);

                //            request.Remove(68, 0);
                //            request.Remove(94, 0);
                //            _reply = new Fields(ws.Process(token, Convert.ToInt32(InteGr8TaskType.Rate), request.ToArray()));
                //            System.Diagnostics.Debug.WriteLine("Time after the rating response: " + DateTime.Now);

                //            break;

                //        case InteGr8TaskType.Rate2:
                //            // remove the selected carrier and service to request a rate with all services
                //            System.Diagnostics.Debug.WriteLine("Time before requesting the rate: " + DateTime.Now);

                //            //request.Remove(68, 0);
                //            //request.Remove(94, 0);
                //            _reply = new Fields(ws.Process(token, Convert.ToInt32(InteGr8TaskType.Rate), request.ToArray()));
                //            System.Diagnostics.Debug.WriteLine("Time after the rating response: " + DateTime.Now);

                //            break;

                //    }
                //    if (_reply != null && _reply.Count > 0)
                //    {
                //        bool hasToken = false;
                //        foreach (Field f in _reply)
                //        {
                //            if (f.Code.Equals(5000))
                //            {
                //                hasToken = true;
                //                break;
                //            }
                //        }
                //        if (!hasToken)
                //        {
                //            _reply.Add(5000, 0, token);
                //        }
                //    }
                //}
                //restore the token to the value from settings, not the current order's one(if tokens are defined in settings)
                //InteGr8_Main.token = backupToken;
            }
            catch (Exception e)
            {
                this.exception = new Exception("Error executing " + type.ToString() + " task for order # '" + order_number + "' " + " Error message is: " + e.Message, e);
                Utils.SendError("Exception processing task", this.exception);
            }
        }

        private bool GetCheapestService(out string Carrier, out string Service)
        {
            Carrier = null;
            Service = null;
            decimal cheapest = -1;
            try
            {
                if (rates != null)
                {

                    foreach (CarrierRate c in rates)
                    {
                        foreach (RateService s in c.Services)
                        {
                            if (String.IsNullOrEmpty(s.ERROR))
                            {
                                if (s.ClientPrice != null)
                                {
                                    if ((s.ClientPrice.Total < cheapest && s.ClientPrice.Total > 0) || cheapest == -1)
                                    {
                                        cheapest = s.ClientPrice.Total;
                                        Carrier = s.CarrierId.ToString();
                                        Service = s.Service.Code;
                                    }
                                }
                            }
                        }
                    }
                }
                //if (_reply == null) return false;
                //Fields costs = _reply.GetFields(183); // client final charge
                //Decimal currentCost = 0m;
                //Decimal cheapestCost = -1;
                //if (costs == null || costs.Count == 0) return false;
                //Field cheapest = null;

                //foreach (Field cost in costs)
                //{
                //    //remove Puro Freight from rating if is small pack or Puro if is LTL shipment
                //    if (_reply[158, cost.Index].ToString().Equals("Purolator Freight") && isSmallPackage(order_number))
                //    {
                //        continue;
                //    }
                //    if (_reply[158, cost.Index].ToString().Equals("Purolator") && !isSmallPackage(order_number))
                //    {
                //        continue;
                //    }
                //    if (Convert.ToInt32(_reply[544, cost.Index, "0"]) != 0 || cost.Value == null || !Decimal.TryParse(cost.Value, out currentCost))
                //        //Convert.ToDecimal(cost.Value) <= 0m)
                //    {
                //        continue; // only shipment level costs are considered (package index zero)
                //    }
                //    if (cheapest == null)
                //    {
                //        cheapest = cost;
                //    }

                //    if (!Decimal.TryParse(cheapest.Value, out cheapestCost))
                //    {
                //        continue;
                //    }

                //    //if (Convert.ToDecimal(cost.Value) < Convert.ToDecimal(cheapest.Value))
                //    if ((cheapestCost > -1) && (currentCost < cheapestCost))
                //    {
                //        cheapest = cost;
                //    }
                //}
                //if (cheapest == null)
                //{
                //    return false;
                //}
                //Carrier = _reply[158, cheapest.Index];
                //Service = _reply[159, cheapest.Index];
                if (Carrier == null || Service == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.SendError("Error in GetCheapestService for order '" + order_number + "'", e);
                return false;
            }
        }

        private bool GetFastestService(out string Carrier, out string Service)
        {
            Carrier = null;
            Service = null;
            DateTime fastest = DateTime.MaxValue;
            DateTime currentDate = DateTime.Today;
            try
            {
                if (rates != null)
                {
                    foreach (CarrierRate c in rates)
                    {
                        foreach (RateService s in c.Services)
                        {
                            if (String.IsNullOrEmpty(s.ERROR))
                            {
                                if (s.DeliveryDate != null)
                                {
                                    currentDate = s.DeliveryDate;
                                    if (currentDate.ToString().Contains("01/01/0001") && (s.TransitDays != null))
                                    {
                                        if (s.TransitDays > 0) currentDate = DateTime.Today.AddDays(s.TransitDays);
                                    }
                                    if (currentDate < fastest && !(s.DeliveryDate.ToString().Contains("01/01/0001")))
                                    {
                                        fastest = currentDate;//s.DeliveryDate;
                                        Carrier = s.CarrierId.ToString();
                                        Service = s.Service.Code;
                                    }
                                }
                            }
                        }
                    }
                }
                //if (_reply == null)
                //{
                //    return false;
                //}
                //Fields times = _reply.GetFields(161);
                //if (times == null || times.Count == 0)
                //{
                //    return false;
                //}
                //Field fastest = null;
                //DateTime date = DateTime.MaxValue;
                //DateTime fastestDate = DateTime.MaxValue;
                //foreach (Field delivery in times)
                //{
                //    //remove Puro Freight from rating if is small pack or Puro if is LTL shipment
                //    if (_reply[158, delivery.Index].ToString().Equals("Purolator Freight") && isSmallPackage(order_number))
                //    {
                //        continue;
                //    }
                //    if (_reply[158, delivery.Index].ToString().Equals("Purolator") && !isSmallPackage(order_number))
                //    {
                //        continue;
                //    }
                //    if (Convert.ToInt32(_reply[544, delivery.Index, "0"]) != 0 || delivery.Value == null || !DateTime.TryParse(delivery.Value, out date))
                //    {
                //        continue; // only shipment level costs are considered (package index zero)
                //    }
                //    if (!DateTime.TryParse(delivery.Value, out date))
                //    {
                //        continue;
                //    }
                //    if (fastest == null)
                //    {
                //        fastest = delivery;
                //    }

                //    if (!DateTime.TryParse(fastest.Value, out fastestDate))
                //    {
                //        continue;
                //    }
                //    if( date < fastestDate)
                //    //if (Convert.ToDateTime(delivery.Value) < Convert.ToDateTime(fastest.Value))
                //    {
                //        fastest = delivery;
                //    }
                //}
                ////here fastest remains null...altough address is good ant there are rates for it - only N/A values for DeliveryDate field
                //if (fastest == null)
                //{
                //    return false;
                //}
                //Carrier = _reply[158, fastest.Index];
                //Service = _reply[159, fastest.Index];
                if (Carrier == null || Service == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.SendError("Error in GetCheapestService for order '" + order_number + "'", e);
                return false;
            }
        }

        private bool isSmallPackage(string order_number)
        {
            bool isSmallPack = true;
            decimal weight = 0;
            Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_number);
            Data.OrderPackagesTableRow[] packs = order.GetOrderPackagesTableRows();
            try
            {
                foreach (Data.OrderPackagesTableRow r in packs)
                {
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Weight") >= 0)
                    {
                        weight += System.Convert.ToDecimal(r["Package Weight"].ToString());
                    }
                    else
                    {
                        if ((data.OrderPackagesTable.Columns.IndexOf("Total Weight") > 0))
                        {
                            weight += System.Convert.ToDecimal(r["Total Weight"].ToString());
                        }
                    }
                }
                if (weight > 150.0m)
                {
                    isSmallPack = false;
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error calculating total packages weight, cheapest / fastest", ex);
            }
            return isSmallPack;
        }

        public void Save()
        {
            try
            {
                Data.OrdersTableRow order = data.OrdersTable.FindBy_Order__(order_number);

                if (type == InteGr8TaskType.Ship || type == InteGr8TaskType.GetBackEditResults)
                {
                    //MessageBox.Show("Saving results");
                    Utils.WriteLine(Utils.download_log, "Shipments got back");
                       
                    SaveReplyToShipments(order);
                }
                else if (type == InteGr8TaskType.RateCheapestService)
                {
                    string carrier = null, service = null;
                    if (GetCheapestService(out carrier, out service))
                    {
                        order.Carrier = carrier;
                        order.Service = carrier + " - " + service;
                    }
                }
                else if (type == InteGr8TaskType.RateFastestService)
                {
                    string carrier = null, service = null;
                    if (GetFastestService(out carrier, out service))
                    {
                        order.Carrier = carrier;
                        order.Service = carrier + " - " + service;
                    }
                }
                else if (type == InteGr8TaskType.Rate2)
                {
                    // save all reply fields to the RepliesTable
                    data.RepliesTable.Clear();
                    Data.FieldsTableRow field_row = data.FieldsTable.FindById(183); // we need the final charge for rates, it is the only field we need
                    System.Diagnostics.Debug.WriteLine("Time before saving the rate results: " + DateTime.Now);
                    int index = 0;
                    if (rates != null)
                    {

                        for (int c = 0; c < rates.Length; c++)
                        {
                            if (rates[c].Carrier.Id.ToString().Equals(order["Carrier"].ToString()))
                            {
                                for (int s = 0; s < rates[c].Services.Length; s++)
                                {
                                    if (String.IsNullOrEmpty(rates[c].Services[s].ERROR))
                                    {
                                        string service = order["Service"].ToString();
                                        service = data.Services.FindByUnique(service).Service_Code;

                                        if (service.Equals(rates[c].Services[s].Service.Code))
                                        {
                                            field_row = data.FieldsTable.FindById(183);//Client Final Charge
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].ClientPrice != null ? rates[c].Services[s].ClientPrice.Total.ToString() : null);
                                            field_row = data.FieldsTable.FindById(175);//Client Charge before taxes
                                            decimal taxes = 0;
                                            if (rates[c].Services[s].ClientPrice != null)
                                            {
                                                Tax[] tax = rates[c].Services[s].ClientPrice.Taxes;
                                                foreach (Tax t in tax)
                                                {
                                                    taxes += t.Amount;
                                                }
                                            }
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].ClientPrice != null ? (rates[c].Services[s].ClientPrice.Total - taxes).ToString() : null);
                                            field_row = data.FieldsTable.FindById(371);//CarrierName
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, String.IsNullOrEmpty(rates[c].Services[s].CarrierName) ? "" : rates[c].Services[s].CarrierName);
                                            field_row = data.FieldsTable.FindById(158);//CarrierCode
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].CarrierId != null ? rates[c].Services[s].CarrierId.ToString() : null);
                                            field_row = data.FieldsTable.FindById(372);//ServiceName
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].Service != null ? rates[c].Services[s].Service.Name.ToString() : null);
                                            field_row = data.FieldsTable.FindById(159);//ServiceCode
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].Service != null ? rates[c].Services[s].Service.Code.ToString() : null);
                                            field_row = data.FieldsTable.FindById(355);//Billed Weight
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].ClientPrice != null ? (rates[c].Services[s].ClientPrice.BilledWeight != null ? rates[c].Services[s].ClientPrice.BilledWeight.ToString() : null) : null);
                                            field_row = data.FieldsTable.FindById(365);//Dimensional Weight
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].ClientPrice != null ? (rates[c].Services[s].ClientPrice.DimensionalWeight != null ? rates[c].Services[s].ClientPrice.DimensionalWeight.ToString() : null) : null);
                                            field_row = data.FieldsTable.FindById(77);//Package Weight
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].ClientPrice != null ? (rates[c].Services[s].ClientPrice.BilledWeight != null ? rates[c].Services[s].ClientPrice.BilledWeight.ToString() : null) : null);
                                            field_row = data.FieldsTable.FindById(322);//ErrorMessage
                                            data.RepliesTable.AddRepliesTableRow(order, field_row, index, "");
                                            index++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {

                    }
                }
                else if (type == InteGr8TaskType.Rate)
                {
                    // save all reply fields to the RepliesTable
                    data.RepliesTable.Clear();
                    Data.FieldsTableRow field_row; // we need the final charge for rates, it is the only field we need
                    System.Diagnostics.Debug.WriteLine("Time before saving the rate results: " + DateTime.Now);
                    int index = 0;
                    if(rates != null)
                    {
                        for (int c = 0; c < rates.Length; c++)
                    {
                        for (int s = 0; s < rates[c].Services.Length; s++)
                        {
                            if (String.IsNullOrEmpty(rates[c].Services[s].ERROR))
                            {
                                field_row = data.FieldsTable.FindById(183);//Client Final Charge
                                data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].ClientPrice != null ? rates[c].Services[s].ClientPrice.Total.ToString() : null);
                                field_row = data.FieldsTable.FindById(371);//CarrierName
                                data.RepliesTable.AddRepliesTableRow(order, field_row, index, String.IsNullOrEmpty(rates[c].Services[s].CarrierName) ? "" : rates[c].Services[s].CarrierName);
                                field_row = data.FieldsTable.FindById(158);//CarrierCode
                                data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].CarrierId != null ? rates[c].Services[s].CarrierId.ToString() : null);
                                field_row = data.FieldsTable.FindById(372);//ServiceName
                                data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].Service != null ? rates[c].Services[s].Service.Name.ToString() : null);
                                field_row = data.FieldsTable.FindById(159);//ServiceCode
                                data.RepliesTable.AddRepliesTableRow(order, field_row, index, rates[c].Services[s].Service != null ? rates[c].Services[s].Service.Code.ToString() : null);
                                field_row = data.FieldsTable.FindById(161);//DeliveryDate 1
                                string deliveryDate = rates[c].Services[s].DeliveryDate != null ? rates[c].Services[s].DeliveryDate.ToString() : null;
                                if (deliveryDate.Contains("01/01/0001") && (rates[c].Services[s].TransitDays != null))
                                {
                                    deliveryDate = DateTime.Today.AddDays(rates[c].Services[s].TransitDays).ToString();
                                }
                                data.RepliesTable.AddRepliesTableRow(order, field_row, index, deliveryDate);//rates[c].Services[s].DeliveryDate != null ? rates[c].Services[s].DeliveryDate.ToString() : null);
                                field_row = data.FieldsTable.FindById(363);//DeliveryDate 2
                                data.RepliesTable.AddRepliesTableRow(order, field_row, index, deliveryDate);//rates[c].Services[s].DeliveryDate != null ? rates[c].Services[s].DeliveryDate.ToString() : null);
                                field_row = data.FieldsTable.FindById(322);//ErrorMessage
                                data.RepliesTable.AddRepliesTableRow(order, field_row, index, "");
                                index++;
                            }
                        }
                    }
                }
                else{

                }
              }
            }
            catch (Exception e)
            {
                // try to clean up the mess...
                // ...
                Utils.SendError("Error at save task ", e);
                this.exception = new Exception("Error processing result in " + type.ToString() + " task for order # '" + order_number + "' " + "Error message is: " + e.Message, e);
                Utils.SendError("The exception is", this.exception);
            }
        }

        private void AddCostAndCarrierColumnsToShipments()
        {
            if (!data.ShipmentsTable.Columns.Contains("Package Count in Result"))
            {
                data.ShipmentsTable.Columns.Add("Package Count in Result");
            }
            //NumberOfPiecesInSkid
            if (!data.ShipmentsTable.Columns.Contains("Number Of Pieces In Skid"))
            {
                data.ShipmentsTable.Columns.Add("Number Of Pieces In Skid");
            }
            if (!data.ShipmentsTable.Columns.Contains("Shipment PO Number"))
            {
                data.ShipmentsTable.Columns.Add("Shipment PO Number");
            }
            if (!data.ShipmentsTable.Columns.Contains("Shipment Reference Info"))
            {
                data.ShipmentsTable.Columns.Add("Shipment Reference Info");
            }
            if (!data.ShipmentsTable.Columns.Contains("Packaging Count"))
            {
                data.ShipmentsTable.Columns.Add("Packaging Count");
            }
            if (!data.ShipmentsTable.Columns.Contains("Services Count"))
            {
                data.ShipmentsTable.Columns.Add("Services Count");
            }
            if (!data.ShipmentsTable.Columns.Contains("Packaging Code"))
            {
                data.ShipmentsTable.Columns.Add("Packaging Code");
            }
            if (!data.ShipmentsTable.Columns.Contains("Meter Number"))
            {
                data.ShipmentsTable.Columns.Add("Meter Number");
            }
            if (!data.ShipmentsTable.Columns.Contains("Account"))
            {
                data.ShipmentsTable.Columns.Add("Account");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Calculation Type"))
            {
                data.ShipmentsTable.Columns.Add("Client Calculation Type");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Calculation Value"))
            {
                data.ShipmentsTable.Columns.Add("Client Calculation Value");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client GST"))
            {
                data.ShipmentsTable.Columns.Add("Client GST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client PST"))
            {
                data.ShipmentsTable.Columns.Add("Client PST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client QST"))
            {
                data.ShipmentsTable.Columns.Add("Client QST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client HST"))
            {
                data.ShipmentsTable.Columns.Add("Client HST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client TransactionFeeType"))
            {
                data.ShipmentsTable.Columns.Add("Client TransactionFeeType");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client TransactionFeeValue"))
            {
                data.ShipmentsTable.Columns.Add("Client TransactionFeeValue");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client TransactionFee"))
            {
                data.ShipmentsTable.Columns.Add("Client TransactionFee");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Transaction GST"))
            {
                data.ShipmentsTable.Columns.Add("Client Transaction GST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Transaction PST"))
            {
                data.ShipmentsTable.Columns.Add("Client Transaction PST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Transaction HST"))
            {
                data.ShipmentsTable.Columns.Add("Client Transaction HST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Transaction QST"))
            {
                data.ShipmentsTable.Columns.Add("Client Transaction QST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Service Level Message"))
            {
                data.ShipmentsTable.Columns.Add("Service Level Message");
            }
            if (!data.ShipmentsTable.Columns.Contains("Packaging Name"))
            {
                data.ShipmentsTable.Columns.Add("Packaging Name");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier List Surcharges"))
            {
                data.ShipmentsTable.Columns.Add("Carrier List Surcharges");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier List Fuel Surcharges"))
            {
                data.ShipmentsTable.Columns.Add("Carrier List Fuel Surcharges");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier List GST"))
            {
                data.ShipmentsTable.Columns.Add("Carrier List GST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier List PST"))
            {
                data.ShipmentsTable.Columns.Add("Carrier List PST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier List QST"))
            {
                data.ShipmentsTable.Columns.Add("Carrier List QST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier List HST"))
            {
                data.ShipmentsTable.Columns.Add("Carrier List HST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Rating Error Message"))
            {
                data.ShipmentsTable.Columns.Add("Rating Error Message");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier List Freight"))
            {
                data.ShipmentsTable.Columns.Add("Carrier List Freight");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier List Charge"))
            {
                data.ShipmentsTable.Columns.Add("Carrier List Charge");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier List Canadian Taxes"))
            {
                data.ShipmentsTable.Columns.Add("Carrier List Canadian Taxes");
            }
            if (!data.ShipmentsTable.Columns.Contains("Dimensional Weight Used"))
            {
                data.ShipmentsTable.Columns.Add("Dimensional Weight Used");
            }
            if (!data.ShipmentsTable.Columns.Contains("Delivery Weekday"))
            {
                data.ShipmentsTable.Columns.Add("Delivery Weekday");
            }
            if (!data.ShipmentsTable.Columns.Contains("Delivery Days"))
            {
                data.ShipmentsTable.Columns.Add("Delivery Days");
            }
            if (!data.ShipmentsTable.Columns.Contains("Rate Zone"))
            {
                data.ShipmentsTable.Columns.Add("Rate Zone");
            }
            if (!data.ShipmentsTable.Columns.Contains("Dimensional Weight"))
            {
                data.ShipmentsTable.Columns.Add("Dimensional Weight");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Final GST"))
            {
                data.ShipmentsTable.Columns.Add("Client Final GST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Final PST"))
            {
                data.ShipmentsTable.Columns.Add("Client Final PST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Final QST"))
            {
                data.ShipmentsTable.Columns.Add("Client Final QST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Final HST"))
            {
                data.ShipmentsTable.Columns.Add("Client Final HST");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Final Canadian Taxes"))
            {
                data.ShipmentsTable.Columns.Add("Client Final Canadian Taxes");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client List Canadian Taxes"))
            {
                data.ShipmentsTable.Columns.Add("Client List Canadian Taxes");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Billing Account"))
            {
                data.ShipmentsTable.Columns.Add("Client Billing Account");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Account"))
            {
                data.ShipmentsTable.Columns.Add("Client Account");
            }
            if (!data.ShipmentsTable.Columns.Contains("Validation Code"))
            {
                data.ShipmentsTable.Columns.Add("Validation Code");
            }
            if (!data.ShipmentsTable.Columns.Contains("Validation Message"))
            {
                data.ShipmentsTable.Columns.Add("Validation Message");
            }
            if (!data.ShipmentsTable.Columns.Contains("Import Id"))
            {
                data.ShipmentsTable.Columns.Add("Import Id");
            }
            if (!data.ShipmentsTable.Columns.Contains("MPS Type"))
            {
                data.ShipmentsTable.Columns.Add("MPS Type");
            }
            if (!data.ShipmentsTable.Columns.Contains("MPS Value"))
            {
                data.ShipmentsTable.Columns.Add("MPS Value");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carriers Count"))
            {
                data.ShipmentsTable.Columns.Add("Carriers Count");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier Index"))
            {
                data.ShipmentsTable.Columns.Add("Carrier Index");
            }
            if (!data.ShipmentsTable.Columns.Contains("Service Index"))
            {
                data.ShipmentsTable.Columns.Add("Service Index");
            }
            if (!data.ShipmentsTable.Columns.Contains("Packages Count"))
            {
                data.ShipmentsTable.Columns.Add("Packages Count");
            }
            if (!data.ShipmentsTable.Columns.Contains("ServiceFreightType"))
            {
                data.ShipmentsTable.Columns.Add("ServiceFreightType");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier Level Error Code"))
            {
                data.ShipmentsTable.Columns.Add("Carrier Level Error Code");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier Level Error Message"))
            {
                data.ShipmentsTable.Columns.Add("Carrier Level Error Message");
            }
            if (!data.ShipmentsTable.Columns.Contains("Charges Currency"))
            {
                data.ShipmentsTable.Columns.Add("Charges Currency");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Discount"))
            {
                data.ShipmentsTable.Columns.Add("Client Discount");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Final Charge"))
            {
                data.ShipmentsTable.Columns.Add("Client Final Charge");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Freight"))
            {
                data.ShipmentsTable.Columns.Add("Client Freight");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Charge Before Taxes"))
            {
                data.ShipmentsTable.Columns.Add("Client Charge Before Taxes");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Surcharges"))
            {
                data.ShipmentsTable.Columns.Add("Client Surcharges");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Fuel Surcharge"))
            {
                data.ShipmentsTable.Columns.Add("Client Fuel Surcharge");
            }
            if (!data.ShipmentsTable.Columns.Contains("Client Canadian Taxes"))
            {
                data.ShipmentsTable.Columns.Add("Client Canadian Taxes");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier Code"))
            {
                data.ShipmentsTable.Columns.Add("Carrier Code");
            }
            if (!data.ShipmentsTable.Columns.Contains("Service Code"))
            {
                data.ShipmentsTable.Columns.Add("Service Code");
            }
            if (!data.ShipmentsTable.Columns.Contains("Carrier Name"))
            {
                data.ShipmentsTable.Columns.Add("Carrier Name");
            }
            if (!data.ShipmentsTable.Columns.Contains("Service Name"))
            {
                data.ShipmentsTable.Columns.Add("Service Name");
            }
            if (!data.ShipmentsTable.Columns.Contains("Billed Weight"))
            {
                data.ShipmentsTable.Columns.Add("Billed Weight");
            }
            if (!data.ShipmentsTable.Columns.Contains("Package index"))
            {
                data.ShipmentsTable.Columns.Add("Package index");
            }
            if (!data.ShipmentsTable.Columns.Contains("Customer Freight"))
            {
                data.ShipmentsTable.Columns.Add("Customer Freight");
            }
            if (!data.ShipmentsTable.Columns.Contains("Customer Fuel"))
            {
                data.ShipmentsTable.Columns.Add("Customer Fuel");
            }
            if (!data.ShipmentsTable.Columns.Contains("Customer Taxes"))
            {
                data.ShipmentsTable.Columns.Add("Customer Taxes");
            }
            if (!data.ShipmentsTable.Columns.Contains("Customer Charge Before Taxes"))
            {
                data.ShipmentsTable.Columns.Add("Customer Charge Before Taxes");
            }
            if (!data.ShipmentsTable.Columns.Contains("Customer Total"))
            {
                data.ShipmentsTable.Columns.Add("Customer Total");
            }
            if (!data.ShipmentsTable.Columns.Contains("ConsolidationUnitID"))
            {
                data.ShipmentsTable.Columns.Add("ConsolidationUnitID");
            }
            if (!data.ShipmentsTable.Columns.Contains("CODCharge"))
            {
                data.ShipmentsTable.Columns.Add("CODCharge");
            }
        }

        private void AddAllFieldsToShipments(CarrierShipResponse _shipRes, Data.ShipmentsTableRow[] shipments, int p)
        {
            try
            {
                string delDate = "N/A";
                if (_shipRes.Service.DeliveryDate != null)
                {
                    if (_shipRes.Service.DeliveryDate.ToString().Contains("01/01/0001"))
                    {
                        if (!String.IsNullOrWhiteSpace(_shipRes.Service.TransitDays.ToString()))
                        {
                            delDate = DateTime.Today.AddDays(_shipRes.Service.TransitDays).ToString("MM-dd-yyyy");
                        }
                    }
                    else
                    {
                        delDate = _shipRes.Service.DeliveryDate.ToString("MM-dd-yyyy");
                    }
                }
                //calculate total number of pieces in all skids
                Utils.WriteLine(".\\ReplyTest.txt", "In AddAllFieldsToShipments");

                int totalPieces = 0;
                if (data.Carriers.FindByCode(_shipRes.Service.CarrierId.ToString()) != null)
                {
                    if (data.Carriers.FindByCode(_shipRes.Service.CarrierId.ToString()).IsLTL)
                    {
                        for (int l = 0; l < _shipRes.ShipmentDetails.Packages.Length; l++)
                        {
                            totalPieces += _shipRes.ShipmentDetails.Packages[l].NumberOfPiecesInSkid;
                        }
                    }
                    else //if it is MyCarrier
                        if (_shipRes.Service.CarrierId.ToString().Contains("999"))
                    {
                        if (_shipRes.MycarrierDetails.MyCarrierType.Equals(SavedMyCarrierType.LTL))
                        {
                            for (int l = 0; l < _shipRes.ShipmentDetails.Packages.Length; l++)
                            {
                                totalPieces += _shipRes.ShipmentDetails.Packages[l].NumberOfPiecesInSkid;
                            }
                        }
                    }
                }
                Utils.WriteLine(".\\ReplyTest.txt", "Add all fields on level: " + p);

                int i = p > 0 ? p - 1 : p;
                int noOfPackages = p > 0 ? _shipRes.PackageTrackingNumbers.Length : 1;
                shipments[p]["Package Count in Result"] = _shipRes.PackageTrackingNumbers.Length;
                shipments[p]["Number Of Pieces In Skid"] = p > 0 ? _shipRes.ShipmentDetails.Packages[i].NumberOfPiecesInSkid : totalPieces;
                shipments[p]["Shipment PO Number"] = _shipRes.ShipmentDetails.ShipmentPONumber;
                shipments[p]["Shipment Reference Info"] = _shipRes.ShipmentDetails.ShipmentReference;
                shipments[p]["Packaging Count"] = data.Packagings.Count;
                shipments[p]["Services Count"] = 1;
                shipments[p]["Carrier Code"] = _shipRes.Service.CarrierId;
                shipments[p]["Service Code"] = _shipRes.Service.Service.Code;
                shipments[p]["Delivery Date"] = delDate;//_shipRes.Service.DeliveryDate != null ? (_shipRes.Service.DeliveryDate.ToString().Contains("01/01/0001") ? "" : _shipRes.Service.DeliveryDate.ToString()) : (_shipRes.Service.TransitDays != null ? DateTime.Today.AddDays(_shipRes.Service.TransitDays).ToString() : "N/A");
                shipments[p]["Packaging Code"] = _shipRes.ShipmentDetails.Packages[i] != null ? _shipRes.ShipmentDetails.Packages[i].Packaging.ToString() : "";
                shipments[p]["Meter Number"] = "";
                shipments[p]["Account"] = "";
                shipments[p]["Client Calculation Type"] = "";
                shipments[p]["Client Calculation Value"] = 0m;
                shipments[p]["Client Freight"] = _shipRes.Service.ClientPrice != null ? (p == 0 ? _shipRes.Service.ClientPrice.Freight : _shipRes.Service.ClientPrice.PackagePrices[i].Freight) : 0m;
                shipments[p]["Client Fuel Surcharge"] = _shipRes.Service.ClientPrice != null ? (_shipRes.Service.ClientPrice.Fuel.Amount > 0 ? (p == 0 ? _shipRes.Service.ClientPrice.Fuel.Amount : _shipRes.Service.ClientPrice.PackagePrices[i].Fuel.Amount) : 0m) : 0m;

                //set the surcharges to 0 first, so they won't be null in case that on reply we won't get back a value
                shipments[p]["Client Surcharges"] = 0m;
                shipments[p]["Client GST"] = 0m;
                shipments[p]["Client PST"] = 0m;
                shipments[p]["Client QST"] = 0m;
                shipments[p]["Client HST"] = 0m;

                if (_shipRes.Service.ClientPrice != null)
                {
                    decimal surcharges = 0m;
                    Surcharge[] indexSurcharges = p == 0 ? _shipRes.Service.ClientPrice.Surcharges : _shipRes.Service.ClientPrice.PackagePrices[i].Surcharges;
                    foreach (Surcharge s in indexSurcharges)//_shipRes.Service.ClientPrice.Surcharges)
                    {
                        if (s.Name.Contains("GST"))
                        {
                            shipments[p]["Client GST"] = s.Amount != null ? s.Amount : 0m;
                            surcharges += s.Amount != null ? s.Amount : 0m;
                        }
                        if (s.Name.Contains("PST"))
                        {
                            shipments[p]["Client PST"] = s.Amount != null ? s.Amount : 0m;
                            surcharges += s.Amount != null ? s.Amount : 0m;
                        }
                        if (s.Name.Contains("QST"))
                        {
                            shipments[p]["Client QST"] = s.Amount != null ? s.Amount : 0m;
                            surcharges += s.Amount != null ? s.Amount : 0m;
                        }
                        if (s.Name.Contains("HST"))
                        {
                            shipments[p]["Client HST"] = s.Amount != null ? s.Amount : 0m;
                            surcharges += s.Amount != null ? s.Amount : 0m;
                        }
                        if (s.Name.Contains("COD"))
                        {
                            shipments[p]["CODCharge"] = s.Amount != null ? s.Amount : 0m;
                            surcharges += s.Amount != null ? s.Amount : 0m;
                        }
                    }
                    shipments[p]["Client Surcharges"] = surcharges;// / noOfPackages;
                }

                if (_shipRes.Service.ClientPrice != null)
                {
                    decimal taxes = 0m;
                    Tax[] indexTaxes = p == 0 ? _shipRes.Service.ClientPrice.Taxes : _shipRes.Service.ClientPrice.PackagePrices[i].Taxes;
                    foreach (Tax t in indexTaxes)//_shipRes.Service.ClientPrice.Taxes)
                    {
                        taxes += t.Amount != null ? t.Amount : 0m;
                    }
                    shipments[p]["Client Canadian Taxes"] = taxes;
                    shipments[p]["Client Charge Before Taxes"] = (p == 0 ? _shipRes.Service.ClientPrice.Total : _shipRes.Service.ClientPrice.PackagePrices[i].Total) - taxes;
                }
                else
                {
                    shipments[p]["Client Canadian Taxes"] = 0m;
                    shipments[p]["Client Charge Before Taxes"] = 0m;
                }
                shipments[p]["Client TransactionFeeType"] = 0m;
                shipments[p]["Client TransactionFeeValue"] = 0m;
                shipments[p]["Client TransactionFee"] = 0m;
                shipments[p]["Client Transaction GST"] = 0m;
                shipments[p]["Client Transaction PST"] = 0m;
                shipments[p]["Client Transaction QST"] = 0m;
                shipments[p]["Client Transaction HST"] = 0m;

                shipments[p]["Service Level Message"] = "";
                //shipments[p]["Error Code"] = _shipRes.Service.ERROR;
                //shipments[p]["Error Message"] = _shipRes.Service.ERROR;
                shipments[p]["Packaging Name"] = _shipRes.ShipmentDetails.Packages[i].Packaging != null ? _shipRes.ShipmentDetails.Packages[i].Packaging.ToString() : "";

                //set the list prices to 0 first, so they won't be null and if they have a value on reply, update that value
                shipments[p]["Carrier List Surcharges"] = 0m;
                shipments[p]["Carrier List GST"] = 0m;
                shipments[p]["Carrier List PST"] = 0m;
                shipments[p]["Carrier List QST"] = 0m;
                shipments[p]["Carrier List HST"] = 0m;
                shipments[p]["Carrier List Fuel Surcharges"] = 0m;
                shipments[p]["Carrier List Freight"] = 0m;
                shipments[p]["Carrier List Charge"] = 0m;

                if (_shipRes.Service.ListPrice != null)
                {
                    decimal listSurcharges = 0m;
                    decimal listTaxes = 0m;
                    Surcharge[] indexSurcharges2 = p == 0 ? _shipRes.Service.ListPrice.Surcharges : _shipRes.Service.ListPrice.PackagePrices[i].Surcharges;
                    foreach (Surcharge s in indexSurcharges2)//_shipRes.Service.ListPrice.Surcharges)
                    {
                        if (s.Name.Contains("GST"))
                        {
                            shipments[p]["Carrier List GST"] = s.Amount != null ? s.Amount : 0m;
                            listSurcharges += s.Amount != null ? s.Amount : 0m;
                        }
                        if (s.Name.Contains("PST"))
                        {
                            shipments[p]["Carrier List PST"] = s.Amount != null ? s.Amount : 0m;
                            listSurcharges += s.Amount != null ? s.Amount : 0m;
                        }
                        if (s.Name.Contains("QST"))
                        {
                            shipments[p]["Carrier List QST"] = s.Amount != null ? s.Amount : 0m;
                            listSurcharges += s.Amount != null ? s.Amount : 0m;
                        }
                        if (s.Name.Contains("HST"))
                        {
                            shipments[p]["Carrier List HST"] = s.Amount != null ? s.Amount : 0m;
                            listSurcharges += s.Amount != null ? s.Amount : 0m;
                        }

                    }
                    shipments[p]["Carrier List Surcharges"] = listSurcharges;// / noOfPackages;
                    shipments[p]["Carrier List Fuel Surcharges"] = _shipRes.Service.ListPrice.Fuel.Amount != null ? (p == 0 ? _shipRes.Service.ListPrice.Fuel.Amount : _shipRes.Service.ListPrice.PackagePrices[i].Fuel.Amount) : 0m;
                    shipments[p]["Carrier List Freight"] = _shipRes.Service.ListPrice.Freight != null ? (p == 0 ? _shipRes.Service.ListPrice.Freight : _shipRes.Service.ListPrice.PackagePrices[i].Freight) : 0m;
                    shipments[p]["Carrier List Charge"] = _shipRes.Service.ListPrice.Total != null ? (p == 0 ? _shipRes.Service.ListPrice.Total : _shipRes.Service.ListPrice.PackagePrices[i].Total) : 0m;
                    Tax[] indexTaxes2 = p == 0 ? _shipRes.Service.ListPrice.Taxes : _shipRes.Service.ListPrice.PackagePrices[i].Taxes;
                    foreach (Tax t in indexTaxes2)//_shipRes.Service.ListPrice.Taxes)
                    {
                        listTaxes += t.Amount != null ? t.Amount : 0m;
                    }
                    shipments[p]["Carrier List Canadian Taxes"] = listTaxes;

                }

                shipments[p]["Rating Error Message"] = "";
                shipments[p]["Dimensional Weight Used"] = _shipRes.Service.ClientPrice.DimensionalWeight != null ? 1 : 0;
                shipments[p]["Delivery Weekday"] = _shipRes.Service.DeliveryDate != null ? _shipRes.Service.DeliveryDate.DayOfWeek.ToString() : (_shipRes.Service.TransitDays != null ? DateTime.Today.AddDays(_shipRes.Service.TransitDays).ToString() : "N/A");
                shipments[p]["Delivery Days"] = _shipRes.Service.TransitDays != null ? _shipRes.Service.TransitDays : (_shipRes.Service.DeliveryDate != null ? _shipRes.Service.DeliveryDate.AddDays(-DateTime.Today.Day).Day : 0);
                shipments[p]["Rate Zone"] = _shipRes.Service.DestinationRateZone;
                shipments[p]["Dimensional Weight"] = _shipRes.Service.ClientPrice.DimensionalWeight != null ? (p == 0 ? _shipRes.Service.ClientPrice.DimensionalWeight : _shipRes.Service.ClientPrice.PackagePrices[i].DimensionalWeight) : 0m;
                shipments[p]["Client Final GST"] = shipments[p]["Client GST"];
                shipments[p]["Client Final PST"] = shipments[p]["Client PST"];
                shipments[p]["Client Final QST"] = shipments[p]["Client QST"];
                shipments[p]["Client Final HST"] = shipments[p]["Client HST"];
                shipments[p]["Client Final Canadian Taxes"] = shipments[p]["Client Canadian Taxes"];
                shipments[p]["Client Billing Account"] = "";
                shipments[p]["Client Account"] = "";
                shipments[p]["Validation Code"] = "";
                shipments[p]["Validation Message"] = "";
                shipments[p]["Import ID"] = _shipRes.ShipId != null ? _shipRes.ShipId : 0;
                shipments[p]["MPS Type"] = Template.MPS ? (Template.MPSPackageLevel ? 2 : 1) : 0;
                shipments[p]["MPS Value"] = Template.MPS ? (Template.MPSPackageLevel ? 2 : 1) : 0;
                shipments[p]["Carriers Count"] = data.Carriers.Count;
                shipments[p]["Carrier Index"] = 0;
                shipments[p]["Service Index"] = 0;
                shipments[p]["Packages Count"] = _shipRes.ShipmentDetails.Packages.Length;
                shipments[p]["ServiceFreightType"] = _shipRes.ShipmentDetails.Packages[i].FreightClassId != null ? _shipRes.ShipmentDetails.Packages[i].FreightClassId.ToString() : "N/A";
                shipments[p]["Carrier Level Error Code"] = "";
                shipments[p]["Carrier Level Error Message"] = "";
                shipments[p]["Charges Currency"] = _shipRes.Service.ClientPrice.Currency;
                shipments[p]["Client Discount"] = 0;
                shipments[p]["ConsolidationUnitID"] = _shipRes.ShipmentDetails.ConsolidationUnit.ConsolidationUnitId != null ? _shipRes.ShipmentDetails.ConsolidationUnit.ConsolidationUnitId : 0;
            }
            catch (Exception ex)
            {
                Utils.WriteLine("Exception adding extra fields to shipments on level: " + p, ex.Message);
            }

        }


        private void SaveReplyToShipments(Data.OrdersTableRow order)
        {
            try
            {
                if (type == InteGr8TaskType.Ship || type == InteGr8TaskType.GetBackEditResults)
                {
                    Data.ShipmentsTableRow shipment = null;
                
                    if (!String.IsNullOrEmpty(ShipResult[0].Service.ERROR))
                    {
                        MessageBox.Show("Shipment cannot be made : \'" + ShipResult[0].Service.ERROR + "\'", "2Ship Message");
                        return;
                    }

                    int packages = ShipResult[0].PackageTrackingNumbers.Length;

                    for (int i = 0; i < ShipResult[0].PackageTrackingNumbers.Length; i++)
                    {
                        if (ShipResult[0].PackageTrackingNumbers[i] == null)
                        {
                            packages = i > 1 ? i - 1 : 1;
                            break;
                        }
                        if (String.IsNullOrEmpty(ShipResult[0].PackageTrackingNumbers[i]))
                        {
                            packages = i > 1 ? i - 1 : 1;
                            break;
                        }
                    }

                    //MessageBox.Show("No of packages in shipment: " + packages);

                    //* add special columns to ShipmentsTable
                    foreach (Binding b in Template.Bindings)
                    {
                        if (b.Field.FieldCategory == Data.FieldsTableDataTable.FieldCategory.Shipments &&
                            Data.Special_Columns.ContainsKey(b.Field.Id) && !data.ShipmentsTable.Columns.Contains(Data.Special_Columns[b.Field.Id]))
                            data.ShipmentsTable.Columns.Add(Data.Special_Columns[b.Field.Id]);
                    }

                    if (!Settings.Default.UserWSKey.Equals("") && !Settings.Default.LoginClient.Equals(""))
                    {
                        if (data.ShipmentsTable.Columns.IndexOf("Token") < 0)
                        {
                            data.ShipmentsTable.Columns.Add("Token", typeof(String));
                        }
                    }
                    if (data.ShipmentsTable.Columns.IndexOf("Shipment PO Number") < 0)
                    {
                        data.ShipmentsTable.Columns.Add("Shipment PO Number", typeof(String));
                    }
                    //*remove old shipments of the current order
                    for (int p = 0; p <= packages; p++)
                    {
                        shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order_number, p);
                        if (shipment != null)
                        {
                            BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", shipment._Order__, data);
                            data.ShipmentsTable.RemoveShipmentsTableRow(shipment);
                        }
                    }

                    Data.ShipmentsTableRow[] shipments = new Data.ShipmentsTableRow[packages + 1];//packages is the actual no. of packages, we need one more for shipment level
                    string label = "";
                    string CI = "";
                    string returnLabel = "";
                    string BOL = "";
                    string packingSlip = "";
                    string returnCI = "";
                    string returnBOL = "";
                    for (int d = 0; d < ShipResult[0].ShipDocuments.Length; d++)
                    {
                        if (ShipResult[0].ShipDocuments[d].Type == ShipDocumentType.Label)
                        {
                            label = ShipResult[0].ShipDocuments[d].Href;
                        }
                        if (ShipResult[0].ShipDocuments[d].Type == ShipDocumentType.CommercialInvoice)
                        {
                            CI = ShipResult[0].ShipDocuments[d].Href;
                        }
                        if (ShipResult[0].ShipDocuments[d].Type == ShipDocumentType.BOL)
                        {
                            BOL = ShipResult[0].ShipDocuments[d].Href;
                        }
                        if (ShipResult[0].ShipDocuments[d].Type == ShipDocumentType.ReturnLabel)
                        {
                            returnLabel = ShipResult[0].ShipDocuments[d].Href;
                        }
                        if (ShipResult[0].ShipDocuments[d].Type == ShipDocumentType.ReturnCommercialInvoice)
                        {
                            returnCI = ShipResult[0].ShipDocuments[d].Href;
                        }
                        if (ShipResult[0].ShipDocuments[d].Type == ShipDocumentType.ReturnBOL)
                        {
                            returnBOL = ShipResult[0].ShipDocuments[d].Href;
                        }

                    }
                    string delDate = "N/A";
                    if (ShipResult[0].Service.DeliveryDate != null)
                    {
                        if (ShipResult[0].Service.DeliveryDate.ToString().Contains("01/01/0001"))
                        {
                            if (ShipResult[0].Service != null)
                            {
                                delDate = DateTime.Today.AddDays(ShipResult[0].Service.TransitDays).ToString("MM-dd-yyyy");
                            }
                        }
                        else
                        {
                            delDate = ShipResult[0].Service.DeliveryDate.ToString("MM-dd-yyyy");
                        }
                    }

                    if (!data.ShipmentsTable.Columns.Contains("Client Final Charge"))
                    {
                        data.ShipmentsTable.Columns.Add("Client Final Charge");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Client Charge Before Taxes"))
                    {
                        data.ShipmentsTable.Columns.Add("Client Charge Before Taxes");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Carrier Code"))
                    {
                        data.ShipmentsTable.Columns.Add("Carrier Code");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Service Code"))
                    {
                        data.ShipmentsTable.Columns.Add("Service Code");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Carrier Name"))
                    {
                        data.ShipmentsTable.Columns.Add("Carrier Name");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Service Name"))
                    {
                        data.ShipmentsTable.Columns.Add("Service Name");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Billed Weight"))
                    {
                        data.ShipmentsTable.Columns.Add("Billed Weight");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Dim Weight"))
                    {
                        data.ShipmentsTable.Columns.Add("Dim Weight");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Package index"))
                    {
                        data.ShipmentsTable.Columns.Add("Package index");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Customer Freight"))
                    {
                        data.ShipmentsTable.Columns.Add("Customer Freight");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Customer Fuel"))
                    {
                        data.ShipmentsTable.Columns.Add("Customer Fuel");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Customer Taxes"))
                    {
                        data.ShipmentsTable.Columns.Add("Customer Taxes");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Customer Charge Before Taxes"))
                    {
                        data.ShipmentsTable.Columns.Add("Customer Charge Before Taxes");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Customer Total"))
                    {
                        data.ShipmentsTable.Columns.Add("Customer Total");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("ShipmentTotalCharge"))
                    {
                        data.ShipmentsTable.Columns.Add("ShipmentTotalCharge");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("List Freight"))
                    {
                        data.ShipmentsTable.Columns.Add("List Freight");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("List Fuel"))
                    {
                        data.ShipmentsTable.Columns.Add("List Fuel");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("List Surcharges"))
                    {
                        data.ShipmentsTable.Columns.Add("List Surcharges");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("List Charge Before Taxes"))
                    {
                        data.ShipmentsTable.Columns.Add("List Charge Before Taxes");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("List Taxes"))
                    {
                        data.ShipmentsTable.Columns.Add("List Taxes");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("List Charge"))
                    {
                        data.ShipmentsTable.Columns.Add("List Charge");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("Token"))
                    {
                        data.ShipmentsTable.Columns.Add("Token");
                   
                    }
                    if (!data.ShipmentsTable.Columns.Contains("FormattedShipDate"))
                    {
                        data.ShipmentsTable.Columns.Add("FormattedShipDate");
                    }
                    if (!data.ShipmentsTable.Columns.Contains("WeightUM"))
                    {
                        data.ShipmentsTable.Columns.Add("WeightUM");
                    }
                    string allTrackingNumbers = "";
                    string formatShipDate = "";
                    foreach (string num in _shipResult[0].PackageTrackingNumbers)
                    {
                        allTrackingNumbers += num + "|";
                    }
                    allTrackingNumbers = allTrackingNumbers.Remove(allTrackingNumbers.LastIndexOf('|'));
                    formatShipDate = _shipResult[0].ShipmentDetails.ShipDate.ToString("MMM, dd, YYYY");
                        //DateTime.ParseExact(_shipResult[0].ShipmentDetails.ShipDate,"MMMM, dd, YYYY", CultureInfo.InvariantCulture).ToShortDateString();

                    ///new costs calculation
                    AddCostAndCarrierColumnsToShipments(); 
                    int noOfPackages = _shipResult[0].Service.ClientPrice.PackagePrices.Length;
                    decimal ChargeWithoutTaxes = _shipResult[0].Service.ClientPrice.Freight;
                    decimal clientFreight = _shipResult[0].Service.ClientPrice.Freight;
                    decimal[] surchargesNoFuel = new decimal[noOfPackages + 1];
                    decimal customerChargeNoTaxes = _shipResult[0].Service.CustomerPrice != null ? _shipResult[0].Service.CustomerPrice.Freight : 0m;
                    decimal[] customerSurcharges = new decimal[noOfPackages + 1];
                    decimal[] clientSurcharges = new decimal[noOfPackages + 1];
                    decimal[] clientFuel = new decimal[noOfPackages + 1];
                    decimal[] clientTaxes = new decimal[noOfPackages + 1];
                    decimal customerFreight = (_shipResult[0].Service.CustomerPrice != null ? _shipResult[0].Service.CustomerPrice.Freight : 0m);
                    decimal customerTotal = (_shipResult[0].Service.CustomerPrice != null ? _shipResult[0].Service.CustomerPrice.Total : 0m);
                    decimal[] customerTaxes = new decimal[noOfPackages + 1];
                    decimal[] customerFuel = new decimal[noOfPackages + 1];
                    decimal[] listSurcharges = new decimal[noOfPackages + 1];
                    decimal[] listTaxes = new decimal[noOfPackages + 1];

                    foreach (Surcharge t in _shipResult[0].Service.ListPrice.Surcharges)
                    {
                        listSurcharges[0] += t.Amount;
                    }
                    foreach (Tax t in _shipResult[0].Service.ListPrice.Taxes)
                    {
                        listTaxes[0] += t.Amount > 0 ? t.Amount : (t.Percentage > 0 ? t.Percentage * _shipResult[0].Service.ListPrice.Freight / 100 : 0);
                    }

                    //calculate clientSurcharges and add them to ClientFreight - if a surcharge affects fuel and fuel is percent of Freight + Surcharge
                    //then add it directly to Freight, and if it doesn't affect fuel then add it to Freight only after fuel was calculated and added
                    if (_shipResult[0].Service != null)
                    {
                        foreach (Surcharge t in _shipResult[0].Service.ClientPrice.Surcharges)
                        {
                            clientSurcharges[0] += (t.Amount != null ? t.Amount : 0m);
                            if (t.AffectsFuel)
                                ChargeWithoutTaxes += t.Amount;
                            else
                                surchargesNoFuel[0] += t.Amount;
                        }
                        if (_shipResult[0].Service.ClientPrice != null)
                        {
                            if (_shipResult[0].Service.ClientPrice.Fuel.Amount > 0)
                            {
                                ChargeWithoutTaxes += _shipResult[0].Service.ClientPrice.Fuel.Amount;
                                clientFuel[0] = _shipResult[0].Service.ClientPrice.Fuel.Amount;
                            }
                            else if (_shipResult[0].Service.ClientPrice.Fuel.Percentage > 0)
                            {
                                ChargeWithoutTaxes += ChargeWithoutTaxes * _shipResult[0].Service.ClientPrice.Fuel.Percentage;
                                clientFuel[0] = ChargeWithoutTaxes * _shipResult[0].Service.ClientPrice.Fuel.Percentage;
                            }
                        }
                        ChargeWithoutTaxes += surchargesNoFuel[0];
                        //calculate ClientTaxes
                        foreach (Tax tax in _shipResult[0].Service.ClientPrice.Taxes)
                        {
                            clientTaxes[0] += tax.Amount;
                        }
                        //same logic here for customer freight with surcharges and fuel
                        if (_shipResult[0].Service.CustomerPrice != null)
                        {
                            foreach (Surcharge t in _shipResult[0].Service.CustomerPrice.Surcharges)
                            {
                                customerSurcharges[0] += t.Amount;
                                if (t.AffectsFuel)
                                    customerChargeNoTaxes += t.Amount;
                                else
                                    customerSurcharges[0] += t.Amount;
                            }
                            if (_shipResult[0].Service.CustomerPrice != null)
                            {
                                if (_shipResult[0].Service.CustomerPrice.Fuel.Amount > 0)
                                {
                                    customerChargeNoTaxes += _shipResult[0].Service.CustomerPrice.Fuel.Amount;
                                    customerFuel[0] += _shipResult[0].Service.CustomerPrice.Fuel.Amount;
                                }
                                else if (_shipResult[0].Service.CustomerPrice.Fuel.Percentage > 0)
                                {
                                    customerChargeNoTaxes += customerChargeNoTaxes * _shipResult[0].Service.CustomerPrice.Fuel.Percentage;
                                    customerFuel[0] = customerChargeNoTaxes * _shipResult[0].Service.CustomerPrice.Fuel.Percentage;
                                }
                            }
                            foreach (Tax tax in _shipResult[0].Service.CustomerPrice.Taxes)
                            {
                                customerTaxes[0] += tax.Amount;
                            }
                            customerChargeNoTaxes += customerSurcharges[0];
                        }
                        else
                        {
                            customerChargeNoTaxes = ChargeWithoutTaxes;
                            customerTaxes = clientTaxes;
                        }
                        //add package level costs
                        for (int pkg = 0; pkg < noOfPackages; pkg++)
                        {
                            decimal ChargeWithoutTaxesPackage = 0m;
                            decimal customerChargeNoTaxesPackage = 0m;
                            clientSurcharges[pkg + 1] = 0m;
                            clientTaxes[pkg + 1] = 0m;
                            clientFuel[pkg + 1] = 0m;
                            customerFuel[pkg + 1] = 0m;
                            customerTaxes[pkg + 1] = 0m;
                            surchargesNoFuel[pkg + 1] = 0m;
                            listSurcharges[pkg + 1] = 0m;
                            listTaxes[pkg + 1] = 0m;
                            foreach (Surcharge t in _shipResult[0].Service.ClientPrice.PackagePrices[pkg].Surcharges)
                            {
                                clientSurcharges[pkg + 1] += t.Amount;
                                if (t.AffectsFuel)
                                    ChargeWithoutTaxesPackage += t.Amount;
                                else
                                    surchargesNoFuel[pkg + 1] += t.Amount;
                            }
                            if (_shipResult[0].Service.ClientPrice.PackagePrices[pkg] != null)
                            {
                                if (_shipResult[0].Service.ClientPrice.PackagePrices[pkg].Fuel.Amount > 0)
                                {
                                    ChargeWithoutTaxesPackage += _shipResult[0].Service.ClientPrice.PackagePrices[pkg].Fuel.Amount;
                                    clientFuel[pkg + 1] = _shipResult[0].Service.ClientPrice.PackagePrices[pkg].Fuel.Amount;
                                }
                                else if (_shipResult[0].Service.ClientPrice.PackagePrices[pkg].Fuel.Percentage > 0)
                                {
                                    ChargeWithoutTaxesPackage += ChargeWithoutTaxesPackage * _shipResult[0].Service.ClientPrice.PackagePrices[pkg].Fuel.Percentage;
                                    clientFuel[pkg + 1] = ChargeWithoutTaxesPackage * _shipResult[0].Service.ClientPrice.PackagePrices[pkg].Fuel.Percentage;
                                }
                            }
                            ChargeWithoutTaxesPackage += surchargesNoFuel[pkg + 1];
                            //calculate ClientTaxes
                            foreach (Surcharge s in _shipResult[0].Service.ListPrice.PackagePrices[pkg].Surcharges)
                            {
                                clientTaxes[pkg + 1] += s.Amount ;
                            }
                            //List surcharges and taxes
                            foreach (Tax tax in _shipResult[0].Service.ClientPrice.PackagePrices[pkg].Taxes)
                            {
                                listSurcharges[pkg + 1] += tax.Amount ;
                            }

                            foreach (Tax tax in _shipResult[0].Service.ListPrice.PackagePrices[pkg].Taxes)
                            {
                                listTaxes[pkg + 1] += tax.Amount;
                            }

                            //same logic here for customer freight with surcharges and fuel
                            if (_shipResult[0].Service.CustomerPrice != null)
                            {
                                foreach (Surcharge t in _shipResult[0].Service.CustomerPrice.PackagePrices[pkg].Surcharges)
                                {
                                    customerSurcharges[pkg + 1] += t.Amount;
                                    if (t.AffectsFuel)
                                        customerChargeNoTaxesPackage += t.Amount;
                                    else
                                        customerSurcharges[pkg + 1] += t.Amount;
                                }
                                if (_shipResult[0].Service.CustomerPrice.PackagePrices[pkg] != null)
                                {
                                    if (_shipResult[0].Service.CustomerPrice.PackagePrices[pkg].Fuel.Amount > 0)
                                    {
                                        customerFuel[pkg + 1] += _shipResult[0].Service.CustomerPrice.PackagePrices[pkg].Fuel.Amount;
                                    }
                                    else if (_shipResult[0].Service.CustomerPrice.PackagePrices[pkg].Fuel.Percentage > 0)
                                    {
                                        customerFuel[pkg + 1] = customerChargeNoTaxesPackage * _shipResult[0].Service.CustomerPrice.PackagePrices[pkg].Fuel.Percentage;
                                    }
                                }
                                foreach (Tax tax in _shipResult[0].Service.CustomerPrice.PackagePrices[pkg].Taxes)
                                {
                                    customerTaxes[pkg + 1] += tax.Amount;
                                }
                            }
                            else
                            {
                                customerTaxes = clientTaxes;
                                customerFuel = clientFuel;
                            }

                        }

                    }


                    // MessageBox.Show("Costs calculated, adding shipment info");


                    for (int p = 0; p <= packages; p++)
                    {
                        if (p == 0)
                        {
                            shipments[p] = data.ShipmentsTable.AddShipmentsTableRow(order,
                            (ShipResult[0].TrackingNumber == null ? "Error" : "Shipped"),
                            label == "" ? "N/A" : label,
                            CI == "" ? "N/A" : CI,
                            ShipResult[0].Service.ClientPrice.Total,
                            delDate,
                            ShipResult[0].Service.ERROR,
                            ShipResult[0].Service.ERROR,
                            ShipResult[0].TrackingNumber, false, p, packages.ToString(),
                            ShipResult[0].Service.CarrierName, ShipResult[0].Service.Service.Name,
                            ShipResult[0].Service.CarrierId.ToString(), ShipResult[0].Service.Service.Code,
                            order.GetOrderPackagesTableRows()[p]["Package #"].ToString());

                            shipments[p]["Client Final Charge"] = ShipResult[0].Service.ClientPrice.Total;

                            shipments[p]["Client Charge Before Taxes"] = ChargeWithoutTaxes;
                            shipments[p]["Carrier Code"] = ShipResult[0].Service.CarrierId;
                            shipments[p]["Service Code"] = ShipResult[0].Service.Service.Code;
                            shipments[p]["Billed Weight"] = ShipResult[0].Service.ClientPrice.BilledWeight;
                            shipments[p]["Package index"] = p;
                            shipments[p]["Customer Freight"] = ShipResult[0].Service.CustomerPrice != null ? ShipResult[0].Service.CustomerPrice.Freight : ShipResult[0].Service.ClientPrice.Freight;
                            shipments[p]["Customer Fuel"] = ShipResult[0].Service.CustomerPrice != null ? ShipResult[0].Service.CustomerPrice.Fuel.Amount : ShipResult[0].Service.ClientPrice.Fuel.Amount;
                            shipments[p]["Customer Total"] = ShipResult[0].Service.CustomerPrice != null ? ShipResult[0].Service.CustomerPrice.Total : ShipResult[0].Service.ClientPrice.Total;
                            shipments[p]["Customer Charge Before Taxes"] = customerTotal - customerTaxes[0];
                            shipments[p]["Dim Weight"] = ShipResult[0].Service.ClientPrice.DimensionalWeight > 0 ? ShipResult[0].Service.ClientPrice.DimensionalWeight : 0;
                            shipments[p]["Carrier Name"] = ShipResult[0].Service.CarrierName;
                            shipments[p]["Service Name"] = ShipResult[0].Service.Service.Name;
                            shipments[p]["List Freight"] = ShipResult[0].Service.ListPrice != null ? ShipResult[0].Service.ListPrice.Freight : 0;
                            shipments[p]["List Fuel"] = ShipResult[0].Service.ListPrice != null ? (ShipResult[0].Service.ListPrice.Fuel.Amount > 0 ? ShipResult[0].Service.ListPrice.Fuel.Amount : ShipResult[0].Service.ListPrice.Fuel.Percentage) : 0;
                            shipments[p]["List Charge"] = ShipResult[0].Service.ListPrice != null ? ShipResult[0].Service.ListPrice.Total : 0;
                            shipments[p]["List Taxes"] = ShipResult[0].Service.ListPrice != null ? listTaxes[p] : 0;
                            shipments[p]["List Surcharges"] = ShipResult[0].Service.ListPrice != null ? listSurcharges[p] : 0;
                            shipments[p]["FormattedShipDate"] = formatShipDate;
                            shipments[p]["WeightUM"] = "LBS";
                            shipments[p]["Package Count in Result"] = ShipResult[0].PackageTrackingNumbers.Length;

                            shipments[p]["Packages Count"] = ShipResult[0].PackageTrackingNumbers.Length;
                        }
                        else
                        {
                            shipments[p] = data.ShipmentsTable.AddShipmentsTableRow(order,
                                (ShipResult[0].PackageTrackingNumbers[p - 1] == null ? "Error" : "Shipped"),
                                label == "" ? "N/A" : label,
                                CI == "" ? "N/A" : CI,
                                ShipResult[0].Service.ClientPrice.Total / (packages),
                                delDate,
                                ShipResult[0].Service.ERROR,
                                ShipResult[0].Service.ERROR,
                                ShipResult[0].PackageTrackingNumbers[p - 1], false, p, packages.ToString(),
                                ShipResult[0].Service.CarrierName, ShipResult[0].Service.Service.Name,
                            ShipResult[0].Service.CarrierId.ToString(), ShipResult[0].Service.Service.Code,
                            (p <= order.GetOrderPackagesTableRows().Length ? order.GetOrderPackagesTableRows()[p - 1]["Package #"].ToString() : p.ToString()));

                            shipments[p]["Client Final Charge"] = ShipResult[0].Service.ClientPrice.PackagePrices[p - 1].Total;

                            shipments[p]["Client Charge Before Taxes"] = ShipResult[0].Service.ClientPrice.PackagePrices[p - 1].Total - clientTaxes[p];
                            shipments[p]["Carrier Code"] = ShipResult[0].Service.CarrierId;
                            shipments[p]["Service Code"] = ShipResult[0].Service.Service.Code;
                            shipments[p]["Billed Weight"] = ShipResult[0].Service.ClientPrice.PackagePrices[p - 1].BilledWeight;
                            shipments[p]["Package index"] = p;
                            shipments[p]["Customer Freight"] = ShipResult[0].Service.CustomerPrice != null ? (ShipResult[0].Service.CustomerPrice.PackagePrices[p - 1].Freight) : (ShipResult[0].Service.ClientPrice.PackagePrices[p - 1].Freight);
                            shipments[p]["Customer Fuel"] = ShipResult[0].Service.CustomerPrice != null ? (ShipResult[0].Service.CustomerPrice.PackagePrices[p - 1].Fuel.Amount) : (ShipResult[0].Service.ClientPrice.PackagePrices[p - 1].Fuel.Amount);
                            shipments[p]["Customer Total"] = ShipResult[0].Service.CustomerPrice != null ? (ShipResult[0].Service.CustomerPrice.PackagePrices[p - 1].Total) : (ShipResult[0].Service.ClientPrice.PackagePrices[p - 1].Total);
                            shipments[p]["Customer Charge Before Taxes"] = (ShipResult[0].Service.CustomerPrice != null ? (ShipResult[0].Service.CustomerPrice.PackagePrices[p - 1].Total - customerTaxes[p]) : (ShipResult[0].Service.ClientPrice.PackagePrices[p - 1].Total - clientTaxes[p]));
                            shipments[p]["Dim Weight"] = ShipResult[0].Service.ClientPrice.DimensionalWeight > 0 ? ShipResult[0].Service.ClientPrice.PackagePrices[p - 1].DimensionalWeight : 0;
                            shipments[p]["Carrier Name"] = ShipResult[0].Service.CarrierName;
                            shipments[p]["Service Name"] = ShipResult[0].Service.Service.Name;
                            shipments[p]["List Freight"] = ShipResult[0].Service.ListPrice != null ? ShipResult[0].Service.ListPrice.PackagePrices[p - 1].Freight : 0;
                            shipments[p]["List Fuel"] = ShipResult[0].Service.ListPrice != null ? (ShipResult[0].Service.ListPrice.PackagePrices[p - 1].Fuel.Amount > 0 ? ShipResult[0].Service.ListPrice.PackagePrices[p - 1].Fuel.Amount : ShipResult[0].Service.ListPrice.PackagePrices[p - 1].Fuel.Percentage) : 0;
                            shipments[p]["List Charge"] = ShipResult[0].Service.ListPrice != null ? ShipResult[0].Service.ListPrice.PackagePrices[p - 1].Total : 0;
                            shipments[p]["List Taxes"] = ShipResult[0].Service.ListPrice != null ? listTaxes[p] : 0;
                            shipments[p]["List Surcharges"] = ShipResult[0].Service.ListPrice != null ? listSurcharges[p] : 0;
                            shipments[p]["FormattedShipDate"] = formatShipDate;
                            shipments[p]["WeightUM"] = "LBS";
                            shipments[p]["Package Count in Result"] = ShipResult[0].PackageTrackingNumbers.Length;
                            shipments[p]["Packages Count"] = ShipResult[0].PackageTrackingNumbers.Length;

                        }

                        try
                        {
                            if (data.ShipmentsTable.Columns.IndexOf("Token") >= 0)
                            {
                                shipments[p]["Token"] = order["Token"]; 
                            }
                            if (data.ShipmentsTable.Columns.IndexOf("Carrier and service") >= 0)
                            {
                                shipments[p]["Carrier and service"] = ShipResult[0].Service.CarrierName + " - " + ShipResult[0].Service.Service.Name;
                            }
                            //Choquette - ShipmentTotalCharge has the shipment level ClientFinalCharge, but addedd for each package 

                            shipments[p]["ShipmentTotalCharge"] = ChargeWithoutTaxes; //_reply[175, 0, 0m];

                            Utils.WriteLine("Error.txt", "Call AddAllFieldsToShipments");

                            //add all costs & rest to shipments
                            AddAllFieldsToShipments(ShipResult[0], shipments, p);


                            if (data.OrdersTable.Columns.IndexOf("HandlingFee") >= 0)
                            {
                                decimal fee = Convert.ToDecimal(((Data.OrdersTableRow)data.OrdersTable.FindBy_Order__(order_number))["HandlingFee"]);
                                if (p == 0)
                                {
                                    shipments[p]["Client Charge Before Taxes"] = ChargeWithoutTaxes + fee;
                                    shipments[p]["ShipmentTotalCharge"] = shipments[0]["Client Charge Before Taxes"];
                                }
                                else
                                {

                                    shipments[p]["Client Charge Before Taxes"] = ChargeWithoutTaxes / (packages) + (fee / (packages));
                                    shipments[p]["ShipmentTotalCharge"] = shipments[0]["Client Charge Before Taxes"];
                                }

                            }
                        }
                        catch (Exception e)
                        {
                        }
                        try
                        {
                            //Paradigm
                            if (data.ShipmentsTable.Columns.IndexOf("Return Label URL") > -1)
                            {
                                shipments[p]["Return Label URL"] = returnLabel;//_reply[611, p, ""];
                                shipments[p]["PRO #"] = (p > 0) ? ShipResult[0].PackageTrackingNumbers[p - 1] : ShipResult[0].TrackingNumber;//_reply[949, p, ""];
                                shipments[p]["BOL URL"] = BOL;//_reply[945, p, ""];
                            }
                            //Chocolate Inn
                            if (data.ShipmentsTable.Columns.IndexOf("ClientFinalChargePackage") > -1)
                            {
                                shipments[p]["ClientFinalChargePackage"] = (p > 0) ? (ShipResult[0].Service.ClientPrice.PackagePrices[p - 1].Total) : ShipResult[0].Service.ClientPrice.Total;//_reply[183, p, 0m].ToString();
                                shipments[p]["ShippingDate"] = data.getNextShipDate(DateTime.Today);
                            }
                        }
                        catch (Exception ex1)
                        {
                        }
                        ///
                    }
                    //}

                    if (data.isLTLCarrier(order["Carrier"].ToString()))
                    {
                        data.ShipmentsTable.Columns["Package #"].ReadOnly = false;

                        shipments[0]["Package #"] = ((order.GetOrdersSkidsTableRows()[0]["Skid #"].ToString()).Equals("1") || String.IsNullOrEmpty(order.GetOrdersSkidsTableRows()[0]["Skid #"].ToString())) ? order.GetOrderPackagesTableRows()[0]["Package #"] : order.GetOrdersSkidsTableRows()[0]["Skid #"];
                        if (order.GetOrdersSkidsTableRows().Length > 1)
                        {
                            for (int i = 0; i < order.GetOrdersSkidsTableRows().Length; i++)
                            {
                                if (shipments[i + 1]["Package #"].ToString().Length <= 0)
                                {
                                    shipments[i + 1]["Package #"] = ((order.GetOrdersSkidsTableRows()[i]["Skid #"].ToString()).Equals("1")) || String.IsNullOrEmpty(order.GetOrdersSkidsTableRows()[0]["Skid #"].ToString()) ? order.GetOrderPackagesTableRows()[0]["Package #"] : order.GetOrdersSkidsTableRows()[0]["Skid #"];
                                }
                            }
                        }
                        data.ShipmentsTable.Columns["Package #"].ReadOnly = true;
                    }
                    else
                    {
                        data.ShipmentsTable.Columns["Package #"].ReadOnly = false;
                        shipments[0]["Package #"] = order.GetOrderPackagesTableRows()[0]["Package #"];
                        if (order.GetOrderPackagesTableRows().Length > 1)
                        {
                            for (int i = 0; i < order.GetOrderPackagesTableRows().Length; i++)
                            {
                                if (shipments[i + 1]["Package #"].ToString().Length <= 0)
                                {
                                    shipments[i + 1]["Package #"] = order.GetOrderPackagesTableRows()[i]["Package #"];
                                }
                            }
                        }
                        data.ShipmentsTable.Columns["Package #"].ReadOnly = true;

                    }


                    if ((Template.MPSPackageLevel && packages > order.GetOrderPackagesTableRows().Count() + 1) || ((Template.MPSPackageLevel && packages > order.GetOrdersSkidsTableRows().Count() + 1) && data.isLTLCarrier(order["Carrier"].ToString())))
                    {
                        data.ShipmentsTable.Columns["Package #"].ReadOnly = false;
                        //if more packages are added in 2ship, and then the results are get back, the new added packages will have no #
                        foreach (Data.ShipmentsTableRow shipRow in shipments) // data.ShipmentsTable)
                        {
                            if (shipRow["Package #"].ToString().Equals(""))
                            {
                                shipRow["Package #"] = shipRow["Order #"].ToString().Split('.')[0];
                            }

                        }
                        data.ShipmentsTable.Columns["Package #"].ReadOnly = true;
                    }

                    // copy the values for shipment level mps or if result is Shipment level, and database writing is package level
                    if (packages > 1 && Template.MPSPackageLevel)
                    {
                        bool isColReadOnly = false;
                        for (int p = 1; p <= packages; p++)
                        {
                            foreach (DataColumn col in data.ShipmentsTable.Columns)
                            {
                                isColReadOnly = col.ReadOnly;
                                col.ReadOnly = false;
                                if (shipments[p][col] == null || String.IsNullOrEmpty(shipments[p][col].ToString()))
                                {
                                    shipments[p][col] = shipments[0][col];
                                }
                                col.ReadOnly = isColReadOnly;
                            }
                        }
                    }

                    data.Complete_shipments();
                    //add shipments to backup
                    for (int p = 0; p < packages; p++)
                    {
                        shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order_number, p);
                        if (shipment != null)
                        {
                            if (!shipment.Status.Equals("Error") && Convert.ToDateTime(shipment["Ship Date"].ToString()).ToShortDateString() == DateTime.Now.ToShortDateString())
                            {
                                BackupFile.AddShipment(data.ShipmentsTable.TableName, shipment as System.Data.DataRow, data);
                            }
                        }
                    }
                    if (Template.Name == "")
                    {
                        for (int p = 0; p < packages; p++)
                        {
                            shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order_number, p);
                            if (shipment != null)
                            {
                                if (shipment.Status.ToString().Equals("Error"))
                                {
                                    Data.ProductsInPackagesRow[] rows = data.ProductsInPackages.Select("[Order #] = '" + order_number + "'") as Data.ProductsInPackagesRow[];
                                    foreach (Data.ProductsInPackagesRow r in rows)
                                    {
                                        BackupFile.RemoveShipment(data.ProductsInPackages.TableName, "deleted", order_number, data);
                                    }
                                    Data.ProductsSerialNumbersRow[] rows2 = data.ProductsSerialNumbers.Select("[Order #] = '" + order_number + "'") as Data.ProductsSerialNumbersRow[];
                                    foreach (Data.ProductsSerialNumbersRow r in rows2)
                                    {
                                        BackupFile.RemoveShipment(data.ProductsSerialNumbers.TableName, "deleted", order_number, data);
                                    }
                                    BackupFile.RemoveShipment(data.ShipmentsTable.TableName, "deleted", order_number, data);
                                }
                                //else
                                //{
                                //    BackupFile.AddShipment(data.ShipmentsTable.TableName, shipment as System.Data.DataRow, data);
                                //}
                            }
                        }
                    }
                    //don't change the status if there is only one order in OrdersGrid(filtered orers case) because the OrdersTableBindingSource
                    // has a filter of showing only the orders that don't have the status = shipped, and in case of just one order, it will try
                    //to remove from the grid the only line availble, having nothing to show, and so the actions on the grid's events will
                    //generate exceptions

                    if (!String.IsNullOrEmpty(ShipResult[0].TrackingNumber)) //((reply["Shipment Tracking Number"] != null || reply["PRO #"] != null))
                    {
                        order.Status = "Shipped";
                        int index = 0;
                        if (Properties.Settings.Default.MultiOrderShipment == true)
                        {
                            Data.ShipmentsTableRow currentRow = data.ShipmentsTable.FindBy_Order__Package_Index(order._Order__, 0);
                            index = data.ShipmentsTable.Rows.IndexOf(currentRow);
                            foreach (Data.OrdersTableRow row in data.OrdersTable.Rows)
                            {
                                row.Status = "Shipped";
                                if (row["Order #"] != order["Order #"])
                                {
                                    Data.ShipmentsTableRow[] ship = new Data.ShipmentsTableRow[1];
                                    ship[0] = data.ShipmentsTable.AddShipmentsTableRow(row, data.ShipmentsTable.Rows[index]["Status"].ToString(),
                                        data.ShipmentsTable.Rows[index]["Label URL"].ToString(), data.ShipmentsTable.Rows[index]["Commercial Invoice URL"].ToString(),
                                        0,//Convert.ToDecimal(data.ShipmentsTable.Rows[0]["Price"]), - only the first row will have the cost, the rest 0
                                        delDate,//data.ShipmentsTable.Rows[index]["Delivery Date"].ToString(),
                                        data.ShipmentsTable.Rows[index]["Error Code"].ToString(), data.ShipmentsTable.Rows[index]["Error Message"].ToString(),
                                        data.ShipmentsTable.Rows[index]["Shipment Tracking Number"].ToString(), (bool)data.ShipmentsTable.Rows[index]["Selected"],
                                        0, //data.ShipmentsTable.Rows.Count,
                                        data.ShipmentsTable.Rows[index]["Packages"].ToString(),
                                        data.ShipmentsTable.Rows[index]["Carrier"].ToString(), data.ShipmentsTable.Rows[index]["Service"].ToString(),
                                        data.ShipmentsTable.Rows[index]["Carrier Code"].ToString(), data.ShipmentsTable.Rows[index]["Service Code"].ToString(),
                                        "1");
                                    foreach (DataColumn d in data.ShipmentsTable.Columns)
                                    {
                                        if (d.ColumnName.Equals("ShipmentTotalCharge") || d.ColumnName.Equals("Client Charge Before Taxes") || d.ColumnName.Equals("Billed Weight"))
                                        {
                                            bool isReadOnly = data.ShipmentsTable.Columns[d.ColumnName].ReadOnly;
                                            data.ShipmentsTable.Columns[d.ColumnName].ReadOnly = false;
                                            ship[0][d.ColumnName] = 0;//data.ShipmentsTable.Rows[0][d.ColumnName];
                                            data.ShipmentsTable.Columns[d.ColumnName].ReadOnly = isReadOnly;
                                        }
                                        if (d.ColumnName.Equals("Carrier Name") || d.ColumnName.Equals("Service Name"))
                                        {
                                            bool isReadOnly = data.ShipmentsTable.Columns[d.ColumnName].ReadOnly;
                                            data.ShipmentsTable.Columns[d.ColumnName].ReadOnly = false;
                                            ship[0][d.ColumnName] = data.ShipmentsTable.Rows[index][d.ColumnName];
                                            data.ShipmentsTable.Columns[d.ColumnName].ReadOnly = isReadOnly;
                                        }
                                        if (ship[0][d.ColumnName] == null)
                                        {
                                            bool isReadOnly = data.ShipmentsTable.Columns[d.ColumnName].ReadOnly;
                                            data.ShipmentsTable.Columns[d.ColumnName].ReadOnly = false;
                                            //ship["Order #"] = row["Order #"];
                                            //data.ShipmentsTable.Rows.Add(ship);
                                            ship[0][d.ColumnName] = data.ShipmentsTable.Rows[index][d.ColumnName];
                                            data.ShipmentsTable.Columns[d.ColumnName].ReadOnly = isReadOnly;
                                        }
                                    }
                                    //shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order_number, p);

                                    //if (!shipment.Status.Equals("Error") && Convert.ToDateTime(shipment["Ship Date"].ToString()).ToShortDateString() == DateTime.Now.ToShortDateString())
                                    //{
                                        BackupFile.AddShipment(data.ShipmentsTable.TableName, ship[0] as System.Data.DataRow, data);
                                    //}
                                }
                            }
                        }
                    }
                   
                    //add shipment to log
                    for (int p = 0; p < packages; p++)
                    {
                        shipment = data.ShipmentsTable.FindBy_Order__Package_Index(order_number, p);
                        if (shipment != null)
                        {
                            
                            foreach (DataColumn col in data.ShipmentsTable.Columns)
                            {
                                Utils.WriteLine(Utils.download_log, col.ColumnName + " " + shipment[col.ColumnName].ToString());
                            }
                        }
                    }
                    
                }

            }
            catch (Exception saveExp)
            {
            }
        }

        //private void AutoprintDocs()
        //{
        //    if (Template.UseAutoprint && type == InteGr8TaskType.Ship && !reply[370, ""].Equals(""))
        //        try
        //        {
        //            Utils.printPDF(reply[370], Template.AutoprintLabelsFolder, Template.AutoprintLabelsPrinter);
        //            if (!reply[373, ""].Equals(""))
        //            {
        //                Utils.printPDF(reply[373], Template.AutoprintCIFolder, Template.AutoprintCIPrinter);
        //            }
        //            if (!reply[948, ""].Equals(""))
        //            {
        //                Utils.printPDF(reply[948], Template.AutoprintCIFolder, Template.AutoprintCIPrinter);
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            this.exception = new Exception("Error printing label '" + reply[370] + "' for order # '" + order_number + "' " + "Error message is: " + e.Message, e);
        //            throw this.exception;
        //        }
        //}

        //public bool AddCostsToShipment()
        //{
        //    try
        //    {
        //        if (_reply == null)
        //        {
        //            return false;
        //        }
        //        Fields freight = _reply.GetFields(167); // client final charge
        //        if (freight == null || freight.Count == 0)
        //        {
        //            return false;
        //        }
        //        Field cheapest = null;

        //        string carrier_index = _reply[542, cheapest.Index]; // the carrier index in the carriers array field, for the given rate line index
        //        string service_index = _reply[543, cheapest.Index]; // the service index in the services array field, for the given rate line index
        //        int carrier = -1;
        //        int service = -1;
        //        if (String.IsNullOrEmpty(carrier_index) || String.IsNullOrEmpty(service_index) || !Int32.TryParse(carrier_index, out carrier) || !Int32.TryParse(service_index, out service))
        //        {
        //            return false;
        //        }
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        Utils.SendError("Error in AddCostsToShipment for order '" + order_number + "'", e);
        //        return false;
        //    }
        //}

        public CarrierShipResponse[] ShipResult
        {
            get
            {
                return _shipResult;
            }
        }
        public CarrierRate[] Rates
        {
            get
            {
                return rates;
            }
        }
        public EditResponse EditResult
        {
            get
            {
                return _editResult;
            }
        }

        public Fields reply
        {
            get
            {
                return _reply;
            }
        }

        public string Result
        {
            get
            {
                return _result;
            }
        }
    }

    #endregion

    public class print_class
    {
        public string order = "";
        public string label = "";
        public string commercial_invoice = "";
        public string bol = "";
        public string return_label = "";
        public string packslip = "";
        public List<string> comntentslabel = new List<string>();
    }
}
