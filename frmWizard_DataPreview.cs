﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard_DataPreview : Form
    {
        public frmWizard_DataPreview()
        {
            InitializeComponent();
            Grid.AutoGenerateColumns = true;
        }

        public frmWizard_DataPreview(string Name, string ConnStr, string SqlSelect): this()
        {
            this.Text = "Data Preview - " + Name;
            using (OdbcConnection Conn = new OdbcConnection(ConnStr))
            {
                Conn.Open();
                Binding.DataSource = new OdbcCommand(SqlSelect, Conn).ExecuteReader();
            }
        }

        public frmWizard_DataPreview(string Name, DataTable table): this()
        {
            this.Text = "Data Preview - " + Name;
            Binding.DataSource = table;
            bindingNavigatorAddNewItem.Visible = false;
            bindingNavigatorDeleteItem.Visible = false;
        }

        private void Grid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(this, "Error displaying data: " + e.Exception.Message, "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.ThrowException = false;
        }
    }
}
