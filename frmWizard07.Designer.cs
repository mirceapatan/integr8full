﻿namespace InteGr8
{
    partial class frmWizard07
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard07));
            this.cbUnitValue = new System.Windows.Forms.ComboBox();
            this.cbIsDocument = new System.Windows.Forms.ComboBox();
            this.lblIsDocumentMessage = new System.Windows.Forms.Label();
            this.lblUnitValueMessage = new System.Windows.Forms.Label();
            this.lblUnitWeightMessage = new System.Windows.Forms.Label();
            this.lblCountryManufactureMessage = new System.Windows.Forms.Label();
            this.lbQuantityMessage = new System.Windows.Forms.Label();
            this.lblUnitValue = new System.Windows.Forms.Label();
            this.cbUnitWeight = new System.Windows.Forms.ComboBox();
            this.lblUnitWeight = new System.Windows.Forms.Label();
            this.cbQuantityMU = new System.Windows.Forms.ComboBox();
            this.lblQuantityMU = new System.Windows.Forms.Label();
            this.cbQuantity = new System.Windows.Forms.ComboBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.cbManufactureCountry = new System.Windows.Forms.ComboBox();
            this.lblCountryManufacture = new System.Windows.Forms.Label();
            this.lblHeaderIcon = new System.Windows.Forms.Label();
            this.llAdvanced = new System.Windows.Forms.LinkLabel();
            this.lblIsDocument = new System.Windows.Forms.Label();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.lblPackagesMappingLine = new System.Windows.Forms.Label();
            this.cbDescription = new System.Windows.Forms.ComboBox();
            this.lblProductsMapping = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblHeaderLine = new System.Windows.Forms.Label();
            this.lblHeaderInfo = new System.Windows.Forms.Label();
            this.cbProductsForeignKey = new System.Windows.Forms.ComboBox();
            this.lblProductsForeignKey = new System.Windows.Forms.Label();
            this.cbProductsTable = new System.Windows.Forms.ComboBox();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.btnPreview = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblProductsTable = new System.Windows.Forms.Label();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.cbHarmonizedCode = new System.Windows.Forms.ComboBox();
            this.lblHarmonizedCode = new System.Windows.Forms.Label();
            this.lblDescriptionMessage = new System.Windows.Forms.LinkLabel();
            this.lblQuantityMUMessage = new System.Windows.Forms.LinkLabel();
            this.lblHarmonizedCodeMessage = new System.Windows.Forms.LinkLabel();
            this.lblLinkType = new System.Windows.Forms.Label();
            this.rbDirectLink = new System.Windows.Forms.RadioButton();
            this.rbIndirectLink = new System.Windows.Forms.RadioButton();
            this.lblDirectLink = new System.Windows.Forms.Label();
            this.lblIndirectLink = new System.Windows.Forms.Label();
            this.cbProductsPrimaryKey = new System.Windows.Forms.ComboBox();
            this.lblProductsPrimaryKey = new System.Windows.Forms.Label();
            this.cbOrderProductsProductNumberFK = new System.Windows.Forms.ComboBox();
            this.lblOrderProductsProductNumberFK = new System.Windows.Forms.Label();
            this.lblLine0 = new System.Windows.Forms.Label();
            this.lblProductsPrimaryKeyMessage = new System.Windows.Forms.Label();
            this.pnlFooter.SuspendLayout();
            this.pnlHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbUnitValue
            // 
            this.cbUnitValue.FormattingEnabled = true;
            this.cbUnitValue.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing"});
            this.cbUnitValue.Location = new System.Drawing.Point(163, 550);
            this.cbUnitValue.Name = "cbUnitValue";
            this.cbUnitValue.Size = new System.Drawing.Size(174, 21);
            this.cbUnitValue.TabIndex = 12;
            // 
            // cbIsDocument
            // 
            this.cbIsDocument.FormattingEnabled = true;
            this.cbIsDocument.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing"});
            this.cbIsDocument.Location = new System.Drawing.Point(163, 415);
            this.cbIsDocument.Name = "cbIsDocument";
            this.cbIsDocument.Size = new System.Drawing.Size(174, 21);
            this.cbIsDocument.TabIndex = 7;
            // 
            // lblIsDocumentMessage
            // 
            this.lblIsDocumentMessage.AutoSize = true;
            this.lblIsDocumentMessage.Location = new System.Drawing.Point(345, 418);
            this.lblIsDocumentMessage.Name = "lblIsDocumentMessage";
            this.lblIsDocumentMessage.Size = new System.Drawing.Size(250, 13);
            this.lblIsDocumentMessage.TabIndex = 221;
            this.lblIsDocumentMessage.Text = "Use \'1\' if the product is a document (not commodity)";
            // 
            // lblUnitValueMessage
            // 
            this.lblUnitValueMessage.AutoSize = true;
            this.lblUnitValueMessage.Location = new System.Drawing.Point(345, 555);
            this.lblUnitValueMessage.Name = "lblUnitValueMessage";
            this.lblUnitValueMessage.Size = new System.Drawing.Size(46, 13);
            this.lblUnitValueMessage.TabIndex = 220;
            this.lblUnitValueMessage.Text = "six digits";
            // 
            // lblUnitWeightMessage
            // 
            this.lblUnitWeightMessage.AutoSize = true;
            this.lblUnitWeightMessage.Location = new System.Drawing.Point(345, 526);
            this.lblUnitWeightMessage.Name = "lblUnitWeightMessage";
            this.lblUnitWeightMessage.Size = new System.Drawing.Size(51, 13);
            this.lblUnitWeightMessage.TabIndex = 219;
            this.lblUnitWeightMessage.Text = "two digits";
            // 
            // lblCountryManufactureMessage
            // 
            this.lblCountryManufactureMessage.AutoSize = true;
            this.lblCountryManufactureMessage.Location = new System.Drawing.Point(345, 445);
            this.lblCountryManufactureMessage.Name = "lblCountryManufactureMessage";
            this.lblCountryManufactureMessage.Size = new System.Drawing.Size(136, 13);
            this.lblCountryManufactureMessage.TabIndex = 215;
            this.lblCountryManufactureMessage.Text = "two letter ISO country code";
            // 
            // lbQuantityMessage
            // 
            this.lbQuantityMessage.AutoSize = true;
            this.lbQuantityMessage.Location = new System.Drawing.Point(345, 472);
            this.lbQuantityMessage.Name = "lbQuantityMessage";
            this.lbQuantityMessage.Size = new System.Drawing.Size(51, 13);
            this.lbQuantityMessage.TabIndex = 214;
            this.lbQuantityMessage.Text = "two digits";
            // 
            // lblUnitValue
            // 
            this.lblUnitValue.AutoSize = true;
            this.lblUnitValue.Location = new System.Drawing.Point(8, 553);
            this.lblUnitValue.Name = "lblUnitValue";
            this.lblUnitValue.Size = new System.Drawing.Size(96, 13);
            this.lblUnitValue.TabIndex = 213;
            this.lblUnitValue.Text = "Product unit value:";
            // 
            // cbUnitWeight
            // 
            this.cbUnitWeight.FormattingEnabled = true;
            this.cbUnitWeight.Location = new System.Drawing.Point(163, 523);
            this.cbUnitWeight.Name = "cbUnitWeight";
            this.cbUnitWeight.Size = new System.Drawing.Size(174, 21);
            this.cbUnitWeight.TabIndex = 11;
            // 
            // lblUnitWeight
            // 
            this.lblUnitWeight.AutoSize = true;
            this.lblUnitWeight.Location = new System.Drawing.Point(8, 526);
            this.lblUnitWeight.Name = "lblUnitWeight";
            this.lblUnitWeight.Size = new System.Drawing.Size(101, 13);
            this.lblUnitWeight.TabIndex = 211;
            this.lblUnitWeight.Text = "Product unit weight:";
            // 
            // cbQuantityMU
            // 
            this.cbQuantityMU.FormattingEnabled = true;
            this.cbQuantityMU.Location = new System.Drawing.Point(163, 496);
            this.cbQuantityMU.Name = "cbQuantityMU";
            this.cbQuantityMU.Size = new System.Drawing.Size(174, 21);
            this.cbQuantityMU.TabIndex = 10;
            // 
            // lblQuantityMU
            // 
            this.lblQuantityMU.AutoSize = true;
            this.lblQuantityMU.Location = new System.Drawing.Point(8, 499);
            this.lblQuantityMU.Name = "lblQuantityMU";
            this.lblQuantityMU.Size = new System.Drawing.Size(150, 13);
            this.lblQuantityMU.TabIndex = 210;
            this.lblQuantityMU.Text = "Product quantity measure unit:";
            // 
            // cbQuantity
            // 
            this.cbQuantity.FormattingEnabled = true;
            this.cbQuantity.Items.AddRange(new object[] {
            "Customer Packaging",
            "Envelope (Letter)",
            "Small Pack"});
            this.cbQuantity.Location = new System.Drawing.Point(163, 469);
            this.cbQuantity.Name = "cbQuantity";
            this.cbQuantity.Size = new System.Drawing.Size(174, 21);
            this.cbQuantity.TabIndex = 9;
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(8, 472);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(87, 13);
            this.lblQuantity.TabIndex = 209;
            this.lblQuantity.Text = "Product quantity:";
            // 
            // cbManufactureCountry
            // 
            this.cbManufactureCountry.FormattingEnabled = true;
            this.cbManufactureCountry.Items.AddRange(new object[] {
            "(none)",
            "Cheapest",
            "Fastest"});
            this.cbManufactureCountry.Location = new System.Drawing.Point(163, 442);
            this.cbManufactureCountry.Name = "cbManufactureCountry";
            this.cbManufactureCountry.Size = new System.Drawing.Size(174, 21);
            this.cbManufactureCountry.TabIndex = 8;
            // 
            // lblCountryManufacture
            // 
            this.lblCountryManufacture.AutoSize = true;
            this.lblCountryManufacture.Location = new System.Drawing.Point(8, 445);
            this.lblCountryManufacture.Name = "lblCountryManufacture";
            this.lblCountryManufacture.Size = new System.Drawing.Size(147, 13);
            this.lblCountryManufacture.TabIndex = 208;
            this.lblCountryManufacture.Text = "Product manufacture country:";
            // 
            // lblHeaderIcon
            // 
            this.lblHeaderIcon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeaderIcon.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaderIcon.Image = global::InteGr8.Properties.Resources.data_next;
            this.lblHeaderIcon.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeaderIcon.Location = new System.Drawing.Point(0, 24);
            this.lblHeaderIcon.Name = "lblHeaderIcon";
            this.lblHeaderIcon.Padding = new System.Windows.Forms.Padding(5);
            this.lblHeaderIcon.Size = new System.Drawing.Size(64, 64);
            this.lblHeaderIcon.TabIndex = 115;
            // 
            // llAdvanced
            // 
            this.llAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdvanced.Image = global::InteGr8.Properties.Resources.wrench;
            this.llAdvanced.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llAdvanced.LinkArea = new System.Windows.Forms.LinkArea(6, 13);
            this.llAdvanced.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdvanced.Location = new System.Drawing.Point(544, 0);
            this.llAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.llAdvanced.Name = "llAdvanced";
            this.llAdvanced.Size = new System.Drawing.Size(104, 24);
            this.llAdvanced.TabIndex = 119;
            this.llAdvanced.TabStop = true;
            this.llAdvanced.Text = "      advanced mode";
            this.llAdvanced.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llAdvanced.UseCompatibleTextRendering = true;
            // 
            // lblIsDocument
            // 
            this.lblIsDocument.AutoSize = true;
            this.lblIsDocument.Location = new System.Drawing.Point(8, 418);
            this.lblIsDocument.Name = "lblIsDocument";
            this.lblIsDocument.Size = new System.Drawing.Size(107, 13);
            this.lblIsDocument.TabIndex = 207;
            this.lblIsDocument.Text = "Product is document:";
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(543, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Products Level Information";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPackagesMappingLine
            // 
            this.lblPackagesMappingLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPackagesMappingLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblPackagesMappingLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPackagesMappingLine.Location = new System.Drawing.Point(11, 376);
            this.lblPackagesMappingLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblPackagesMappingLine.Name = "lblPackagesMappingLine";
            this.lblPackagesMappingLine.Size = new System.Drawing.Size(624, 2);
            this.lblPackagesMappingLine.TabIndex = 206;
            // 
            // cbDescription
            // 
            this.cbDescription.FormattingEnabled = true;
            this.cbDescription.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing"});
            this.cbDescription.Location = new System.Drawing.Point(163, 388);
            this.cbDescription.Name = "cbDescription";
            this.cbDescription.Size = new System.Drawing.Size(174, 21);
            this.cbDescription.TabIndex = 6;
            // 
            // lblProductsMapping
            // 
            this.lblProductsMapping.AutoSize = true;
            this.lblProductsMapping.Location = new System.Drawing.Point(8, 358);
            this.lblProductsMapping.Name = "lblProductsMapping";
            this.lblProductsMapping.Size = new System.Drawing.Size(511, 13);
            this.lblProductsMapping.TabIndex = 204;
            this.lblProductsMapping.Text = "Please map the following 2Ship product variables to your Products table columns, " +
                "or choose default values:";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(8, 391);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(101, 13);
            this.lblDescription.TabIndex = 205;
            this.lblDescription.Text = "Product description:";
            // 
            // lblHeaderLine
            // 
            this.lblHeaderLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderLine.BackColor = System.Drawing.SystemColors.Control;
            this.lblHeaderLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeaderLine.Location = new System.Drawing.Point(0, 88);
            this.lblHeaderLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderLine.Name = "lblHeaderLine";
            this.lblHeaderLine.Size = new System.Drawing.Size(648, 2);
            this.lblHeaderLine.TabIndex = 116;
            // 
            // lblHeaderInfo
            // 
            this.lblHeaderInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderInfo.AutoEllipsis = true;
            this.lblHeaderInfo.Location = new System.Drawing.Point(68, 24);
            this.lblHeaderInfo.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderInfo.Name = "lblHeaderInfo";
            this.lblHeaderInfo.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.lblHeaderInfo.Size = new System.Drawing.Size(580, 64);
            this.lblHeaderInfo.TabIndex = 114;
            this.lblHeaderInfo.Text = resources.GetString("lblHeaderInfo.Text");
            // 
            // cbProductsForeignKey
            // 
            this.cbProductsForeignKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProductsForeignKey.FormattingEnabled = true;
            this.cbProductsForeignKey.Location = new System.Drawing.Point(242, 293);
            this.cbProductsForeignKey.Name = "cbProductsForeignKey";
            this.cbProductsForeignKey.Size = new System.Drawing.Size(174, 21);
            this.cbProductsForeignKey.TabIndex = 4;
            // 
            // lblProductsForeignKey
            // 
            this.lblProductsForeignKey.AutoSize = true;
            this.lblProductsForeignKey.Location = new System.Drawing.Point(9, 296);
            this.lblProductsForeignKey.Name = "lblProductsForeignKey";
            this.lblProductsForeignKey.Size = new System.Drawing.Size(216, 13);
            this.lblProductsForeignKey.TabIndex = 200;
            this.lblProductsForeignKey.Text = "Order Products Order # Foreign Key column:";
            // 
            // cbProductsTable
            // 
            this.cbProductsTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProductsTable.FormattingEnabled = true;
            this.cbProductsTable.Location = new System.Drawing.Point(242, 239);
            this.cbProductsTable.Name = "cbProductsTable";
            this.cbProductsTable.Size = new System.Drawing.Size(174, 21);
            this.cbProductsTable.TabIndex = 2;
            this.cbProductsTable.SelectedIndexChanged += new System.EventHandler(this.cbProductsTable_SelectedIndexChanged);
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.lblFooterLine);
            this.pnlFooter.Controls.Add(this.btnPreview);
            this.pnlFooter.Controls.Add(this.btnPrev);
            this.pnlFooter.Controls.Add(this.btnNext);
            this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFooter.Location = new System.Drawing.Point(0, 603);
            this.pnlFooter.Margin = new System.Windows.Forms.Padding(0);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Padding = new System.Windows.Forms.Padding(9);
            this.pnlFooter.Size = new System.Drawing.Size(648, 53);
            this.pnlFooter.TabIndex = 197;
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFooterLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(12, 5);
            this.lblFooterLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(624, 2);
            this.lblFooterLine.TabIndex = 122;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(11, 18);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(108, 23);
            this.btnPreview.TabIndex = 14;
            this.btnPreview.Text = "Preview Products";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnPrev.Location = new System.Drawing.Point(480, 18);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 15;
            this.btnPrev.Text = "< Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(561, 18);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 16;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblProductsTable
            // 
            this.lblProductsTable.AutoSize = true;
            this.lblProductsTable.Location = new System.Drawing.Point(9, 242);
            this.lblProductsTable.Name = "lblProductsTable";
            this.lblProductsTable.Size = new System.Drawing.Size(82, 13);
            this.lblProductsTable.TabIndex = 198;
            this.lblProductsTable.Text = "Products Table:";
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.lblHeaderLine);
            this.pnlHeader.Controls.Add(this.llAdvanced);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.lblHeaderIcon);
            this.pnlHeader.Controls.Add(this.lblHeaderInfo);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(648, 90);
            this.pnlHeader.TabIndex = 196;
            // 
            // cbHarmonizedCode
            // 
            this.cbHarmonizedCode.FormattingEnabled = true;
            this.cbHarmonizedCode.Location = new System.Drawing.Point(163, 577);
            this.cbHarmonizedCode.Name = "cbHarmonizedCode";
            this.cbHarmonizedCode.Size = new System.Drawing.Size(174, 21);
            this.cbHarmonizedCode.TabIndex = 13;
            // 
            // lblHarmonizedCode
            // 
            this.lblHarmonizedCode.AutoSize = true;
            this.lblHarmonizedCode.Location = new System.Drawing.Point(8, 580);
            this.lblHarmonizedCode.Name = "lblHarmonizedCode";
            this.lblHarmonizedCode.Size = new System.Drawing.Size(131, 13);
            this.lblHarmonizedCode.TabIndex = 226;
            this.lblHarmonizedCode.Text = "Product harmonized code:";
            // 
            // lblDescriptionMessage
            // 
            this.lblDescriptionMessage.AutoSize = true;
            this.lblDescriptionMessage.LinkArea = new System.Windows.Forms.LinkArea(38, 4);
            this.lblDescriptionMessage.Location = new System.Drawing.Point(345, 391);
            this.lblDescriptionMessage.Name = "lblDescriptionMessage";
            this.lblDescriptionMessage.Size = new System.Drawing.Size(231, 17);
            this.lblDescriptionMessage.TabIndex = 228;
            this.lblDescriptionMessage.TabStop = true;
            this.lblDescriptionMessage.Text = "see unaccepted commodity descriptions here";
            this.lblDescriptionMessage.UseCompatibleTextRendering = true;
            // 
            // lblQuantityMUMessage
            // 
            this.lblQuantityMUMessage.AutoSize = true;
            this.lblQuantityMUMessage.LinkArea = new System.Windows.Forms.LinkArea(6, 4);
            this.lblQuantityMUMessage.Location = new System.Drawing.Point(348, 499);
            this.lblQuantityMUMessage.Name = "lblQuantityMUMessage";
            this.lblQuantityMUMessage.Size = new System.Drawing.Size(121, 17);
            this.lblQuantityMUMessage.TabIndex = 229;
            this.lblQuantityMUMessage.TabStop = true;
            this.lblQuantityMUMessage.Text = "click here to see the list";
            this.lblQuantityMUMessage.UseCompatibleTextRendering = true;
            // 
            // lblHarmonizedCodeMessage
            // 
            this.lblHarmonizedCodeMessage.AutoSize = true;
            this.lblHarmonizedCodeMessage.LinkArea = new System.Windows.Forms.LinkArea(6, 4);
            this.lblHarmonizedCodeMessage.Location = new System.Drawing.Point(348, 580);
            this.lblHarmonizedCodeMessage.Name = "lblHarmonizedCodeMessage";
            this.lblHarmonizedCodeMessage.Size = new System.Drawing.Size(121, 17);
            this.lblHarmonizedCodeMessage.TabIndex = 230;
            this.lblHarmonizedCodeMessage.TabStop = true;
            this.lblHarmonizedCodeMessage.Text = "click here to see the list";
            this.lblHarmonizedCodeMessage.UseCompatibleTextRendering = true;
            // 
            // lblLinkType
            // 
            this.lblLinkType.AutoSize = true;
            this.lblLinkType.Location = new System.Drawing.Point(8, 100);
            this.lblLinkType.Name = "lblLinkType";
            this.lblLinkType.Size = new System.Drawing.Size(352, 13);
            this.lblLinkType.TabIndex = 231;
            this.lblLinkType.Text = "Choose how the products list for an order is organized in your datasource:";
            // 
            // rbDirectLink
            // 
            this.rbDirectLink.AutoSize = true;
            this.rbDirectLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDirectLink.Location = new System.Drawing.Point(14, 118);
            this.rbDirectLink.Name = "rbDirectLink";
            this.rbDirectLink.Size = new System.Drawing.Size(218, 17);
            this.rbDirectLink.TabIndex = 0;
            this.rbDirectLink.TabStop = true;
            this.rbDirectLink.Text = "Direct link with the Products table";
            this.rbDirectLink.UseVisualStyleBackColor = true;
            // 
            // rbIndirectLink
            // 
            this.rbIndirectLink.AutoSize = true;
            this.rbIndirectLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbIndirectLink.Location = new System.Drawing.Point(14, 158);
            this.rbIndirectLink.Name = "rbIndirectLink";
            this.rbIndirectLink.Size = new System.Drawing.Size(303, 17);
            this.rbIndirectLink.TabIndex = 1;
            this.rbIndirectLink.TabStop = true;
            this.rbIndirectLink.Text = "Indirect link through another OrderProducts table";
            this.rbIndirectLink.UseVisualStyleBackColor = true;
            // 
            // lblDirectLink
            // 
            this.lblDirectLink.AutoSize = true;
            this.lblDirectLink.Location = new System.Drawing.Point(30, 138);
            this.lblDirectLink.Name = "lblDirectLink";
            this.lblDirectLink.Size = new System.Drawing.Size(403, 13);
            this.lblDirectLink.TabIndex = 234;
            this.lblDirectLink.Text = "Choose this option if the Order # Foreign Key column is located in the Products t" +
                "able";
            // 
            // lblIndirectLink
            // 
            this.lblIndirectLink.Location = new System.Drawing.Point(30, 178);
            this.lblIndirectLink.Name = "lblIndirectLink";
            this.lblIndirectLink.Size = new System.Drawing.Size(606, 40);
            this.lblIndirectLink.TabIndex = 235;
            this.lblIndirectLink.Text = resources.GetString("lblIndirectLink.Text");
            // 
            // cbProductsPrimaryKey
            // 
            this.cbProductsPrimaryKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProductsPrimaryKey.FormattingEnabled = true;
            this.cbProductsPrimaryKey.Location = new System.Drawing.Point(242, 266);
            this.cbProductsPrimaryKey.Name = "cbProductsPrimaryKey";
            this.cbProductsPrimaryKey.Size = new System.Drawing.Size(174, 21);
            this.cbProductsPrimaryKey.TabIndex = 3;
            // 
            // lblProductsPrimaryKey
            // 
            this.lblProductsPrimaryKey.AutoSize = true;
            this.lblProductsPrimaryKey.Location = new System.Drawing.Point(9, 269);
            this.lblProductsPrimaryKey.Name = "lblProductsPrimaryKey";
            this.lblProductsPrimaryKey.Size = new System.Drawing.Size(197, 13);
            this.lblProductsPrimaryKey.TabIndex = 237;
            this.lblProductsPrimaryKey.Text = "Products Product # Primary Key column:";
            // 
            // cbOrderProductsProductNumberFK
            // 
            this.cbOrderProductsProductNumberFK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrderProductsProductNumberFK.FormattingEnabled = true;
            this.cbOrderProductsProductNumberFK.Location = new System.Drawing.Point(242, 320);
            this.cbOrderProductsProductNumberFK.Name = "cbOrderProductsProductNumberFK";
            this.cbOrderProductsProductNumberFK.Size = new System.Drawing.Size(174, 21);
            this.cbOrderProductsProductNumberFK.TabIndex = 5;
            // 
            // lblOrderProductsProductNumberFK
            // 
            this.lblOrderProductsProductNumberFK.AutoSize = true;
            this.lblOrderProductsProductNumberFK.Location = new System.Drawing.Point(9, 323);
            this.lblOrderProductsProductNumberFK.Name = "lblOrderProductsProductNumberFK";
            this.lblOrderProductsProductNumberFK.Size = new System.Drawing.Size(227, 13);
            this.lblOrderProductsProductNumberFK.TabIndex = 239;
            this.lblOrderProductsProductNumberFK.Text = "Order Products Product # Foreign Key column:";
            // 
            // lblLine0
            // 
            this.lblLine0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLine0.BackColor = System.Drawing.SystemColors.Window;
            this.lblLine0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLine0.Location = new System.Drawing.Point(12, 225);
            this.lblLine0.Margin = new System.Windows.Forms.Padding(0);
            this.lblLine0.Name = "lblLine0";
            this.lblLine0.Size = new System.Drawing.Size(624, 2);
            this.lblLine0.TabIndex = 240;
            // 
            // lblProductsPrimaryKeyMessage
            // 
            this.lblProductsPrimaryKeyMessage.AutoSize = true;
            this.lblProductsPrimaryKeyMessage.Location = new System.Drawing.Point(422, 269);
            this.lblProductsPrimaryKeyMessage.Name = "lblProductsPrimaryKeyMessage";
            this.lblProductsPrimaryKeyMessage.Size = new System.Drawing.Size(44, 13);
            this.lblProductsPrimaryKeyMessage.TabIndex = 241;
            this.lblProductsPrimaryKeyMessage.Text = "optional";
            // 
            // frmWizard07
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(648, 656);
            this.Controls.Add(this.lblProductsPrimaryKeyMessage);
            this.Controls.Add(this.lblLine0);
            this.Controls.Add(this.cbOrderProductsProductNumberFK);
            this.Controls.Add(this.lblOrderProductsProductNumberFK);
            this.Controls.Add(this.cbProductsPrimaryKey);
            this.Controls.Add(this.lblProductsPrimaryKey);
            this.Controls.Add(this.lblIndirectLink);
            this.Controls.Add(this.lblDirectLink);
            this.Controls.Add(this.rbIndirectLink);
            this.Controls.Add(this.rbDirectLink);
            this.Controls.Add(this.lblLinkType);
            this.Controls.Add(this.lblHarmonizedCodeMessage);
            this.Controls.Add(this.lblQuantityMUMessage);
            this.Controls.Add(this.lblDescriptionMessage);
            this.Controls.Add(this.cbHarmonizedCode);
            this.Controls.Add(this.lblHarmonizedCode);
            this.Controls.Add(this.cbUnitValue);
            this.Controls.Add(this.cbIsDocument);
            this.Controls.Add(this.lblIsDocumentMessage);
            this.Controls.Add(this.lblUnitValueMessage);
            this.Controls.Add(this.lblUnitWeightMessage);
            this.Controls.Add(this.lblCountryManufactureMessage);
            this.Controls.Add(this.lbQuantityMessage);
            this.Controls.Add(this.lblUnitValue);
            this.Controls.Add(this.cbUnitWeight);
            this.Controls.Add(this.lblUnitWeight);
            this.Controls.Add(this.cbQuantityMU);
            this.Controls.Add(this.lblQuantityMU);
            this.Controls.Add(this.cbQuantity);
            this.Controls.Add(this.lblQuantity);
            this.Controls.Add(this.cbManufactureCountry);
            this.Controls.Add(this.lblCountryManufacture);
            this.Controls.Add(this.lblIsDocument);
            this.Controls.Add(this.lblPackagesMappingLine);
            this.Controls.Add(this.cbDescription);
            this.Controls.Add(this.lblProductsMapping);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.cbProductsForeignKey);
            this.Controls.Add(this.lblProductsForeignKey);
            this.Controls.Add(this.cbProductsTable);
            this.Controls.Add(this.pnlFooter);
            this.Controls.Add(this.lblProductsTable);
            this.Controls.Add(this.pnlHeader);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(634, 491);
            this.Name = "frmWizard07";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 7 of 11";
            this.Load += new System.EventHandler(this.frmWizard07_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard07_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWizard07_FormClosing);
            this.pnlFooter.ResumeLayout(false);
            this.pnlHeader.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbUnitValue;
        private System.Windows.Forms.ComboBox cbIsDocument;
        private System.Windows.Forms.Label lblIsDocumentMessage;
        private System.Windows.Forms.Label lblUnitValueMessage;
        private System.Windows.Forms.Label lblUnitWeightMessage;
        private System.Windows.Forms.Label lblCountryManufactureMessage;
        private System.Windows.Forms.Label lbQuantityMessage;
        private System.Windows.Forms.Label lblUnitValue;
        private System.Windows.Forms.ComboBox cbUnitWeight;
        private System.Windows.Forms.Label lblUnitWeight;
        private System.Windows.Forms.ComboBox cbQuantityMU;
        private System.Windows.Forms.Label lblQuantityMU;
        private System.Windows.Forms.ComboBox cbQuantity;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.ComboBox cbManufactureCountry;
        private System.Windows.Forms.Label lblCountryManufacture;
        private System.Windows.Forms.Label lblHeaderIcon;
        private System.Windows.Forms.LinkLabel llAdvanced;
        private System.Windows.Forms.Label lblIsDocument;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label lblPackagesMappingLine;
        private System.Windows.Forms.ComboBox cbDescription;
        private System.Windows.Forms.Label lblProductsMapping;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblHeaderLine;
        private System.Windows.Forms.Label lblHeaderInfo;
        private System.Windows.Forms.ComboBox cbProductsForeignKey;
        private System.Windows.Forms.Label lblProductsForeignKey;
        private System.Windows.Forms.ComboBox cbProductsTable;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblProductsTable;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.ComboBox cbHarmonizedCode;
        private System.Windows.Forms.Label lblHarmonizedCode;
        private System.Windows.Forms.LinkLabel lblDescriptionMessage;
        private System.Windows.Forms.LinkLabel lblQuantityMUMessage;
        private System.Windows.Forms.LinkLabel lblHarmonizedCodeMessage;
        private System.Windows.Forms.Label lblLinkType;
        private System.Windows.Forms.RadioButton rbDirectLink;
        private System.Windows.Forms.RadioButton rbIndirectLink;
        private System.Windows.Forms.Label lblDirectLink;
        private System.Windows.Forms.Label lblIndirectLink;
        private System.Windows.Forms.ComboBox cbProductsPrimaryKey;
        private System.Windows.Forms.Label lblProductsPrimaryKey;
        private System.Windows.Forms.ComboBox cbOrderProductsProductNumberFK;
        private System.Windows.Forms.Label lblOrderProductsProductNumberFK;
        private System.Windows.Forms.Label lblLine0;
        private System.Windows.Forms.Label lblProductsPrimaryKeyMessage;
    }
}