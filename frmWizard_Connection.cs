﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Data.OleDb;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard_Connection : Form
    {
        public frmWizard_Connection()
        {
            InitializeComponent();

            txtName.Text = Template.Name;
            txtDescription.Text = Template.Description;
            cbConnection.Text = (Template.ReadConn == null ? "" : Template.ReadConn.DSN);
            txtLogin.Text = (Template.ReadConn == null ? "" : Template.ReadConn.Login);
            txtPassword.Text = (Template.ReadConn == null ? "" : Template.ReadConn.Password);
        }

        private void frmWizard01_Load(object sender, EventArgs e)
        {
            btnRefresh_Click(sender, e);
        }

        private void btnRefresh_MouseEnter(object sender, EventArgs e)
        {
            btnRefresh.FlatAppearance.BorderSize = 1;
        }

        private void btnRefresh_MouseLeave(object sender, EventArgs e)
        {
            btnRefresh.FlatAppearance.BorderSize = 0;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            string selected = cbConnection.Text;
            cbConnection.Items.Clear();
            // see http://msdn.microsoft.com/en-us/library/system.data.oledb.oledbenumerator.getenumerator(VS.80).aspx
            OleDbDataReader reader = OleDbEnumerator.GetEnumerator(Type.GetTypeFromProgID("MSDASQL Enumerator"));
            while (reader.Read()) 
                cbConnection.Items.Add(reader["SOURCES_NAME"]);
            cbConnection.Text = selected;
        }

        private void lnkODBC_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start(Environment.SystemDirectory + @"\odbcad32.exe");
            }
            catch
            {
                // ignore
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!TryConnection()) return;

            // Save the info
            Template.Name = txtName.Text;
            Template.Description = txtDescription.Text;
            Template.ReadConn = new Connection(cbConnection.Text, txtLogin.Text, txtPassword.Text);

            DialogResult = DialogResult.OK;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            if (TryConnection()) 
                MessageBox.Show(this, "Connection successfully established.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private bool TryConnection()
        {
            // validate
            if (txtName.Text.Trim().Equals(""))
            {
                MessageBox.Show(this, "Please enter a template name.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (cbConnection.Text.Trim().Equals(""))
            {
                MessageBox.Show(this, "Please select an ODBC DSN connection.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            // show modal dialog with connection waiting message
            frmWait wait = new frmWait("Please wait, connecting to DSN " + cbConnection.Text + "...", new DoWorkEventHandler(TestConnection), false, false, "DSN=" + cbConnection.Text + "; Uid=" + txtLogin.Text + "; Pwd=" + txtPassword.Text);
            wait.ShowDialog(this);
            if (wait.error != null)
            {
                MessageBox.Show(this, "Connection failed: " + wait.error.Message, "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void TestConnection(object sender, DoWorkEventArgs e)
        {
            new OdbcConnection(e.Argument.ToString()).Open();
        }

        private void frmWizard01_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.Cancel && MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) 
                e.Cancel = true;
        }

        private void frmWizard01_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_Connection");
            e.Cancel = true;
        }
    }
}
