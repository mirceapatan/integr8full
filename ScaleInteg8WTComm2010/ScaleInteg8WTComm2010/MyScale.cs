﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using AxWTComm_OCX;
using WTComm_OCX;

namespace ScaleInteg8WTComm
{
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("52F27AF3-CA3A-4DAC-A857-7DA68860A86F")]
    [ComVisible(true)]
    public class MyScale
    {
        public static string BHOKEYNAME = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects";
        
        private bool OpenDevice(AxWTComm_OCX.AxWTComm axWTComm)
        {

            //lstPort       // 3
            //lstBaudRate   // 9600
            //lstDataBits   // 7
            //lstParityBit  // E
            //lstStopBits   // 1

            // Basic control defaults:
            // For this demo program we will only use the RS-232 fastest synchronous mode.
            axWTComm.CommType = WT_COMMTYPE_ENUM.wtRS232;
            axWTComm.OpMode = WT_OPMODE_ENUM.wtSyncStdReso;
            axWTComm.SyncRate = WT_SYNCRATE_ENUM.wtSyncFastest;
            axWTComm.WtNotification = WT_NOTIFY_ENUM.wtEveryWeight;

            // Notes: 
            // The port to C# 2008 from VB6 converted Integer parameter to ref to short!?

            // Set the serial comm port

            axWTComm.CommPort = 3;
            axWTComm.Settings = 9600 + ","
                              + "E" + ","
                              + 7 + ","
                              + 1;

            short tf = -1;                              // in old VB6, False = 0, True = -1
            short x = axWTComm.ScaleOpen(ref tf);       // returns 1:PASS, 0:FAIL
            
            if (x == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public float GetWeight()
        {
            AxWTComm_OCX.AxWTComm axWTComm = new AxWTComm_OCX.AxWTComm();
            axWTComm.CreateControl();
            if (OpenDevice(axWTComm))
            {
                float weight = 0f;
                if (axWTComm.GetStdWt(ref weight))
                {
                    CloseDevice(axWTComm);
                    return weight;
                }
                else
                {
                    CloseDevice(axWTComm);
                    throw new Exception("The connection with the scale is not opened!");
                }
            }
            else
            {
                throw new Exception("The connection with the scale canot be oppened!");
            }
        }

        private void CloseDevice(AxWTComm_OCX.AxWTComm axWTComm)
        {
            short tf = 0;                                   // in old VB6, False = 0, True = -1
            axWTComm.ScaleOpen(ref tf);
        }

        [ComRegisterFunction]
        public static void RegisterBHO(Type t)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(BHOKEYNAME, true);
            if (key == null) key = Registry.LocalMachine.CreateSubKey(BHOKEYNAME);

            string guidString = t.GUID.ToString("B");
            RegistryKey bhoKey = key.OpenSubKey(guidString);

            if (bhoKey == null)
            {
                bhoKey = key.CreateSubKey(guidString);
                bhoKey.SetValue("", "2Ship.com AutoWeightScaleWTComm", RegistryValueKind.String);
                bhoKey.SetValue("NoExplorer", 1, RegistryValueKind.DWord);
            }

            key.Close();
            bhoKey.Close();
        }

        [ComUnregisterFunction]
        public static void UnregisterBHO(Type t)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(BHOKEYNAME, true);
            string guidString = t.GUID.ToString("B");

            if (key != null) key.DeleteSubKey(guidString, false);
        }
    }

}
