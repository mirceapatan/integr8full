﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InteGr8.Properties;

namespace InteGr8
{
    public partial class MultiPack : Form
    {
        public Data data;
       Data.OrdersTableRow currentOrder = null;
        public MultiPack(Data.OrdersTableRow order)
        {
            InitializeComponent();
            currentOrder = order;
        }
        private void MultiPack_Load(object sender, EventArgs e)
        {
            this.data = InteGr8_Main.data;
        }
        private void ntnAddMulti_Click(object sender, EventArgs e)
        {
            //check the values in the textboxes, and add that no of packages to OrderPackages table
            int noPacks = 0;
               if (! int.TryParse(tbPacks.Text, out noPacks))
               {
                     MessageBox.Show("Please enter a valid number of packages!");
                     return;
               }
               if(noPacks == 0)
            {
                MessageBox.Show("The number of packages has to be over 0!");
                return;
            }
            decimal totalWeight = 0m;
            if(! Decimal.TryParse(tbTotalWeight.Text, out totalWeight))
            {
                MessageBox.Show("Please enter a valid weight!");
                return;
            }
            decimal packWeight = totalWeight / noPacks;
            int height = 0, width = 0, length = 0;
            if (cbDims.Checked)
            {
                if (! int.TryParse(tbLength.Text, out length))
                {
                    MessageBox.Show("Please enter a valid integer length!");
                    return;
                }
                if (!int.TryParse(tbWidth.Text, out width))
                {
                    MessageBox.Show("Please enter a valid integer width!");
                    return;
                }
                if (!int.TryParse(tbHeight.Text, out height))
                {
                    MessageBox.Show("Please enter a valid integer height!");
                    return;
                }
            }
            Data.OrderPackagesTableRow[] packages = currentOrder.GetOrderPackagesTableRows();

            if (packages.Length > 0)
            {
                if (packages.Length == 1 && packages[0]["Package Weight"].ToString().Equals("0"))
                {//if there is only one default order package with weight 0, then add the pack weight to it, and add only noPacks-1 extra packages
                    packages[0]["Package Weight"] = packWeight;
                    noPacks -= 1;
                }
                int new_package_number = packages.Length;//(currentOrder.GetOrderPackagesTableRows().Count() + 1).ToString();
                string weightType = packages[currentOrder.GetOrderPackagesTableRows().Length - 1]["Package Weight Type"].ToString();
                string dimsType = packages[currentOrder.GetOrderPackagesTableRows().Length - 1]["Package Dimensions Type"].ToString();

                for (int i = 0; i < noPacks; i++)
                {
                    new_package_number += 1;
                    Data.OrderPackagesTableRow addedRow = data.OrderPackagesTable.AddOrderPackagesTableRow(currentOrder, new_package_number.ToString(),
                        packWeight, weightType , length, width, height, dimsType, 0, "", "");

                    if (data.OrderPackagesTable.Columns.IndexOf("Package Insurance Currency") > -1)
                    {
                        addedRow["Package Insurance Currency"] = packages[0]["Package Insurance Currency"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Insurance Amount") > -1)
                    {
                        addedRow["Package Insurance Amount"] = packages[0]["Package Insurance Amount"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Reference Info 1") > -1)
                    {
                        addedRow["Package Reference Info 1"] = packages[0]["Package Reference Info 1"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package PO Number") > -1)
                    {
                        addedRow["Package PO Number"] = packages[0]["Package PO Number"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Invoice Number") > -1)
                    {
                        addedRow["Package Invoice Number"] = packages[0]["Package Invoice Number"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Number") > -1)
                    {
                        addedRow["Package Number"] = new_package_number;
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Total Weight") > -1)
                    {
                        addedRow["Total Weight"] = packages[0]["Total Weight"];
                    }
                    if (data.OrderPackagesTable.Columns.IndexOf("Packages") > -1)
                    {
                        addedRow["Packages"] = System.Convert.ToInt32(packages[0]["Packages"]) + 1;
                        foreach (Data.OrderPackagesTableRow d in data.OrderPackagesTable.Rows)
                        {
                            d["Packages"] = addedRow["Packages"];
                        }
                    }
                    foreach (DataColumn col in data.OrderPackagesTable.Columns)
                    {
                        if (addedRow[col.ColumnName].ToString().Equals(""))
                        {
                            addedRow[col.ColumnName] = packages[0][col.ColumnName];
                        }
                    }

                }
            }
            else
            {
                for (int i = 0; i < noPacks; i++)
                {
                    Data.OrderPackagesTableRow addedRow = data.OrderPackagesTable.AddOrderPackagesTableRow(currentOrder, i.ToString(),
                        packWeight, "", length, width, height, "", 0, "", "");

                    if (!data.OrderPackagesTable.Columns.Contains("Package Weight Type"))
                    {
                        data.OrderPackagesTable.Columns.Add("Package Weight Type");
                    }

                    addedRow["Package Weight Type"] = "LBS";
                }
                string new_skid_number = "1 - new";
                Data.OrdersSkidsTableRow addRow = data.OrdersSkidsTable.AddOrdersSkidsTableRow(currentOrder, new_skid_number,
                    "1", "1", "false", "true", "1", "0", "LBS", "", "", "", "", "", "I", "", "", "", "", "", "", "", "125.0", "1", "", "");

            }

            data.OrderPackagesTable.AcceptChanges();
            Close();
        }

        private void cbDims_CheckedChanged(object sender, EventArgs e)
        {
            if (cbDims.Checked)
            {
                tbLength.Enabled = true;
                tbWidth.Enabled = true;
                tbHeight.Enabled = true;
            }
            else
            {
                tbLength.Enabled = false;
                tbWidth.Enabled = false;
                tbHeight.Enabled = false;
            }
        }

        
    }
}
