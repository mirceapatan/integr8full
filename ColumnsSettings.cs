﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.Text;

namespace InteGr8
{
    public static class ColumnsSettings
    {
        public static StringCollection Load(string grid, string user)
        {
            StringCollection ColumnsOrder = new StringCollection();

            using (OdbcConnection Conn = new OdbcConnection(Template.BackupConn.ConnStr))
            {
                Conn.Open();
                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;

                    Comm.CommandText = "select * from columns_backup where userid = '" + user + "' and grid = '" + grid + "'";
                    OdbcDataReader temp_data = Comm.ExecuteReader();
                    DataTable table = new DataTable();
                    table.Load(temp_data);
                    DataRow[] temp = table.Select("grid = '" + grid + "'") as DataRow[];
                    if (temp.Length > 0)
                    {
                        string[] colums = temp[0]["columns"].ToString().Split('\n');
                        foreach (string row in colums)
                        {
                            if (row.Count(c => c.Equals(',')) == 3)
                            {
                                string[] a = row.Split(',');
                                ColumnsOrder.Add(string.Format("{0},{1},{2},{3}",a[0], a[1], a[2], a[3]));
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return ColumnsOrder;
        }

        public static void Save(StringCollection ordersColumns, StringCollection packagesColumns, StringCollection productsColumns, string user)
        {
            string orders = "";
            string packages = "";
            string products = "";   
            
            string[] colsArray = new string[ordersColumns.Count];
            ordersColumns.CopyTo(colsArray, 0);
            Array.Sort(colsArray);
            for (int i = 0; i < colsArray.Length; ++i)
            {
                orders += colsArray[i] + "\n";
            }

            colsArray = new string[packagesColumns.Count];
            packagesColumns.CopyTo(colsArray, 0);
            Array.Sort(colsArray);
            for (int i = 0; i < colsArray.Length; ++i)
            {
                packages += colsArray[i] + "\n";
            }

            colsArray = new string[productsColumns.Count];
            productsColumns.CopyTo(colsArray, 0);
            Array.Sort(colsArray);
            for (int i = 0; i < colsArray.Length; ++i)
            {
                products += colsArray[i] + "\n";
            }
            
            using (OdbcConnection Conn = new OdbcConnection(Template.BackupConn.ConnStr))
            {
                Conn.Open();
                using (OdbcCommand Comm = new OdbcCommand())
                {
                    Comm.Connection = Conn;

                    Comm.CommandText = "select * from columns_backup where userid = '" + user + "'";
                    OdbcDataReader temp_data = Comm.ExecuteReader();
                    DataTable table = new DataTable();
                    table.Load(temp_data);
                    DataRow[] temp = table.Select("grid = 'Orders'") as DataRow[];
                    if (temp.Length > 0)
                    {
                        if (temp[0]["columns"].ToString() != orders)
                        {
                            Comm.CommandText = "update columns_backup set columns = '" + orders + "' where userid = '" + user + "' and grid = 'Orders'";
                            Comm.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        Comm.CommandText = "insert into columns_backup (columns, grid, userid) values ('" + orders + "', 'Orders', '" + user + "')";
                        Comm.ExecuteNonQuery();
                    }

                    temp = table.Select("grid = 'Packages'") as DataRow[];
                    if (temp.Length > 0)
                    {
                        if (temp[0]["columns"].ToString() != packages)
                        {
                            Comm.CommandText = "update columns_backup set columns = '" + packages + "' where userid = '" + user + "' and grid = 'Packages'";
                            Comm.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        Comm.CommandText = "insert into columns_backup (columns, grid, userid) values ('" + packages + "', 'Packages', '" + user + "')";
                        Comm.ExecuteNonQuery();
                    }

                    temp = table.Select("grid = 'Products'") as DataRow[];
                    if (temp.Length > 0)
                    {
                        if (temp[0]["columns"].ToString() != products)
                        {
                            Comm.CommandText = "update columns_backup set columns = '" + products + "' where userid = '" + user + "' and grid = 'Products'";
                            Comm.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        Comm.CommandText = "insert into columns_backup (columns, grid, userid) values ('" + products + "', 'Products', '" + user + "')";
                        Comm.ExecuteNonQuery();
                    }
                }
            }
        }
    }
}
