﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using GenericHID;
using Microsoft.VisualBasic;
using Microsoft.Win32.SafeHandles;
using Microsoft.Win32;

namespace ScaleIntegr8
{
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("52F27AF3-CA3A-4DAC-A857-7DA68860A86E")]
    [ComVisible(true)]
    public class MyScale
    {
        private Scale myscale;
        public static string BHOKEYNAME = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects";


        public MyScale()
        {
            //VendorId = "0EB8";
            //ProductId = "F000";
            // myscale = new Scale(VendorId, ProductId);
        }

        //public MyScale(string vendorid, string productid)
        //{
        //    VendorId = vendorid;
        //    ProductId = productid;
        //    //myscale = new Scale(VendorId, ProductId);
        //}

        public string getVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(2);
        }

        public bool FindHid(string vendorid, string productid)
        {
            //if (myscale != null)
            myscale = new Scale(vendorid, productid);
            return myscale.FindTheHid();
        }

        public decimal GetWeight(string vendorid, string productid)
        {
            //if (myscale != null)
            myscale = new Scale(vendorid, productid);
            return myscale.GetWeight();
        }

        [ComRegisterFunction]
        public static void RegisterBHO(Type t)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(BHOKEYNAME, true);
            if (key == null) key = Registry.LocalMachine.CreateSubKey(BHOKEYNAME);

            string guidString = t.GUID.ToString("B");
            RegistryKey bhoKey = key.OpenSubKey(guidString);

            if (bhoKey == null)
            {
                bhoKey = key.CreateSubKey(guidString);
                bhoKey.SetValue("", "2Ship.com AutoWeightScale", RegistryValueKind.String);
                bhoKey.SetValue("NoExplorer", 1, RegistryValueKind.DWord);
            }

            key.Close();
            bhoKey.Close();
        }

        [ComUnregisterFunction]
        public static void UnregisterBHO(Type t)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(BHOKEYNAME, true);
            string guidString = t.GUID.ToString("B");

            if (key != null) key.DeleteSubKey(guidString, false);
        }
    }

    class Scale
    {
        private decimal WeightData = -1m;
        private Hid MyHid = new Hid();
        private string VendorId;
        private string ProductId;

        private IntPtr deviceNotificationHandle;
        private Boolean exclusiveAccess;
        private FileStream fileStreamDeviceData;
        private SafeFileHandle hidHandle;
        private String hidUsage;
        private Boolean myDeviceDetected;
        private String myDevicePathName;
        private Boolean transferInProgress = false;

        private Debugging MyDebugging = new Debugging(); //  For viewing results of API calls via Debug.Write.
        private DeviceManagement MyDeviceManagement = new DeviceManagement();

        //private delegate void MarshalToForm(String action, String textToAdd);

        //internal Form1 FrmMy; 

        public Scale()
        {
            Startup("0EB8", "F000");
        }

        public Scale(string vendorId, string productId)
        {
            Startup(vendorId, productId);
        }

        private void Startup(string vendorId, string productId)
        {
            try
            {
                MyHid = new Hid();
                //FrmMy = this;
                //  Default USB Vendor ID and Product ID:

                VendorId = vendorId;
                ProductId = productId;
            }
            catch (Exception ex)
            {
                DisplayException("Startup", ex);
                throw;
            }
        }

        public decimal GetWeight()
        {
            decimal Weight = decimal.Zero;
            try
            {
                //  Don't allow another transfer request until this one completes.
                //  Move the focus away from cmdOnce to prevent the focus from 
                //  switching to the next control in the tab order on disabling the button.

                // fraSendAndReceive.Focus();


                ReadAndWriteToDevice();
                Weight = WeightData;
            }
            catch (Exception)
            {
                throw;
            }

            return Weight;
        }

        public Boolean FindTheHid()
        {
            Boolean deviceFound = false;
            String[] devicePathName = new String[128];
            String functionName = "";
            Guid hidGuid = Guid.Empty;
            Int32 memberIndex = 0;
            Int32 myProductID = 0;
            Int32 myVendorID = 0;
            Boolean success = false;

            try
            {
                myDeviceDetected = false;
                CloseCommunications();

                //  Get the device's Vendor ID and Product ID from the form's text boxes.

                GetVendorAndProductIDsFromTextBoxes(ref myVendorID, ref myProductID);

                //  ***
                //  API function: 'HidD_GetHidGuid

                //  Purpose: Retrieves the interface class GUID for the HID class.

                //  Accepts: 'A System.Guid object for storing the GUID.
                //  ***

                Hid.HidD_GetHidGuid(ref hidGuid);

                functionName = "GetHidGuid";

                //  Fill an array with the device path names of all attached HIDs.

                deviceFound = MyDeviceManagement.FindDeviceFromGuid(hidGuid, ref devicePathName);

                //  If there is at least one HID, attempt to read the Vendor ID and Product ID
                //  of each device until there is a match or all devices have been examined.

                if (deviceFound)
                {
                    memberIndex = 0;

                    do
                    {
                        //  ***
                        //  API function:
                        //  CreateFile

                        //  Purpose:
                        //  Retrieves a handle to a device.

                        //  Accepts:
                        //  A device path name returned by SetupDiGetDeviceInterfaceDetail
                        //  The type of access requested (read/write).
                        //  FILE_SHARE attributes to allow other processes to access the device while this handle is open.
                        //  A Security structure or IntPtr.Zero. 
                        //  A creation disposition value. Use OPEN_EXISTING for devices.
                        //  Flags and attributes for files. Not used for devices.
                        //  Handle to a template file. Not used.

                        //  Returns: a handle without read or write access.
                        //  This enables obtaining information about all HIDs, even system
                        //  keyboards and mice. 
                        //  Separate handles are used for reading and writing.
                        //  ***

                        // Open the handle without read/write access to enable getting information about any HID, even system keyboards and mice.

                        hidHandle = FileIO.CreateFile(devicePathName[memberIndex], 0, FileIO.FILE_SHARE_READ | FileIO.FILE_SHARE_WRITE, IntPtr.Zero, FileIO.OPEN_EXISTING, 0, 0);

                        functionName = "CreateFile";

                        if (!hidHandle.IsInvalid)
                        {
                            //  The returned handle is valid, 
                            //  so find out if this is the device we're looking for.

                            //  Set the Size property of DeviceAttributes to the number of bytes in the structure.

                            MyHid.DeviceAttributes.Size = Marshal.SizeOf(MyHid.DeviceAttributes);

                            //  ***
                            //  API function:
                            //  HidD_GetAttributes

                            //  Purpose:
                            //  Retrieves a HIDD_ATTRIBUTES structure containing the Vendor ID, 
                            //  Product ID, and Product Version Number for a device.

                            //  Accepts:
                            //  A handle returned by CreateFile.
                            //  A pointer to receive a HIDD_ATTRIBUTES structure.

                            //  Returns:
                            //  True on success, False on failure.
                            //  ***                            

                            success = Hid.HidD_GetAttributes(hidHandle, ref MyHid.DeviceAttributes);

                            if (success)
                            {
                                //  Find out if the device matches the one we're looking for.

                                if ((MyHid.DeviceAttributes.VendorID == myVendorID) && (MyHid.DeviceAttributes.ProductID == myProductID))
                                {

                                    //  Display the information in form's list box.

                                    //lstMessages.Items.Add("Device detected:");
                                    //lstMessages.Items.Add("  Vendor ID= " + Convert.ToString(MyHid.DeviceAttributes.VendorID, 16));
                                    //lstMessages.Items.Add("  Product ID = " + Convert.ToString(MyHid.DeviceAttributes.ProductID, 16));

                                    //ScrollToBottomOfListBox();

                                    myDeviceDetected = true;

                                    //  Save the DevicePathName for OnDeviceChange().

                                    myDevicePathName = devicePathName[memberIndex];
                                }
                                else
                                {
                                    //  It's not a match, so close the handle.

                                    myDeviceDetected = false;
                                    hidHandle.Close();
                                }
                            }
                            else
                            {
                                //  There was a problem in retrieving the information.


                                myDeviceDetected = false;
                                hidHandle.Close();
                            }
                        }

                        //  Keep looking until we find the device or there are no devices left to examine.

                        memberIndex = memberIndex + 1;
                    }
                    while (!((myDeviceDetected || (memberIndex == devicePathName.Length))));
                }

                if (myDeviceDetected)
                {
                    //  The device was detected.
                    //  Register to receive notifications if the device is removed or attached.

                    success = MyDeviceManagement.RegisterForDeviceNotifications(myDevicePathName, new System.Windows.Forms.Form().Handle, hidGuid, ref deviceNotificationHandle);



                    //  Learn the capabilities of the device.

                    MyHid.Capabilities = MyHid.GetDeviceCapabilities(hidHandle);

                    if (true)
                    {
                        //  Find out if the device is a system mouse or keyboard.

                        hidUsage = MyHid.GetHidUsage(MyHid.Capabilities);

                        //  Get the Input report buffer size.

                        GetInputReportBufferSize();
                        //cmdInputReportBufferSize.Enabled = true;

                        //Close the handle and reopen it with read/write access.

                        hidHandle.Close();
                        hidHandle = FileIO.CreateFile(myDevicePathName, FileIO.GENERIC_READ | FileIO.GENERIC_WRITE, FileIO.FILE_SHARE_READ | FileIO.FILE_SHARE_WRITE, IntPtr.Zero, FileIO.OPEN_EXISTING, 0, 0);

                        if (hidHandle.IsInvalid)
                        {
                            exclusiveAccess = true;
                            //lstMessages.Items.Add("The device is a system " + hidUsage + ".");
                            //lstMessages.Items.Add("Windows 2000 and Windows XP obtain exclusive access to Input and Output reports for this devices.");
                            //lstMessages.Items.Add("Applications can access Feature reports only.");
                            ////ScrollToBottomOfListBox();
                        }

                        else
                        {
                            if (MyHid.Capabilities.InputReportByteLength > 0)
                            {
                                //  Set the size of the Input report buffer. 

                                Byte[] inputReportBuffer = null;

                                inputReportBuffer = new Byte[MyHid.Capabilities.InputReportByteLength];

                                fileStreamDeviceData = new FileStream(hidHandle, FileAccess.Read | FileAccess.Write, inputReportBuffer.Length, false);
                            }

                            if (MyHid.Capabilities.OutputReportByteLength > 0)
                            {
                                Byte[] outputReportBuffer = null;
                                outputReportBuffer = new Byte[MyHid.Capabilities.OutputReportByteLength];
                            }

                            //  Flush any waiting reports in the input buffer. (optional)

                            MyHid.FlushQueue(hidHandle);
                        }
                    }
                }
                else
                {
                    //  The device wasn't detected.

                    //lstMessages.Items.Add("Device not found.");
                    //cmdInputReportBufferSize.Enabled = false;
                    //cmdOnce.Enabled = true;

                    //Debug.WriteLine(" Device not found.");

                    //ScrollToBottomOfListBox();
                }
                return myDeviceDetected;
            }
            catch (Exception ex)
            {
                // DisplayException("", ex);
                throw;
            }
        }

        private void GetInputReportBufferSize()
        {
            Int32 numberOfInputBuffers = 0;
            Boolean success;

            try
            {
                //  Get the number of input buffers.

                success = MyHid.GetNumberOfInputBuffers(hidHandle, ref numberOfInputBuffers);

                //  Display the result in the text box.

                DisplayMessage("GetNumberOfInputBuffers=" + Convert.ToString(numberOfInputBuffers));
            }
            catch (Exception ex)
            {
                DisplayException("", ex);
                throw;
            }
        }

        private void CloseCommunications()
        {
            if (fileStreamDeviceData != null)
            {
                fileStreamDeviceData.Close();
            }

            if ((hidHandle != null) && (!(hidHandle.IsInvalid)))
            {
                hidHandle.Close();
            }

            // The next attempt to communicate will get new handles and FileStreams.

            myDeviceDetected = false;
        }

        private void GetVendorAndProductIDsFromTextBoxes(ref Int32 myVendorID, ref Int32 myProductID)
        {
            try
            {
                myVendorID = Int32.Parse(VendorId, NumberStyles.AllowHexSpecifier);
                myProductID = Int32.Parse(ProductId, NumberStyles.AllowHexSpecifier);
            }
            catch (Exception ex)
            {
                //DisplayException("GetVendorAndProductIDsFromTextBoxes", ex);
                throw;
            }
        }

        private void DisplayException(String moduleName, Exception e)
        {
            String message = null;
            String caption = null;

            //  Create an error message.

            message = "Exception: " + e.Message + "Module: " + moduleName + "Method: " + e.TargetSite.Name;

            caption = "Unexpected Exception: ";

            //MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //Console.WriteLine(caption);
            //Console.WriteLine(message);

        }

        private void DisplayMessage(string message)
        {
            //String message = null;
            String caption = null;

            //  Create an error message.

            //message = "Exception: " + e.Message + "Module: " + moduleName + "Method: " + e.TargetSite.Name;

            caption = "Message: ";

            //lstMessages.Items.Add(caption + message);
            //MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //Console.WriteLine(caption);
            //Console.WriteLine(message);

        }

        private void ReadAndWriteToDevice()
        {
            // Report header for the debug display:

            //DisplayMessage("");
            // DisplayMessage("***** HID Test Report *****");
            // DisplayMessage(DateAndTime.Today + ": " + DateAndTime.TimeOfDay);

            try
            {
                //  If the device hasn't been detected, was removed, or timed out on a previous attempt
                //  to access it, look for the device.

                if ((myDeviceDetected == false))
                {
                    myDeviceDetected = FindTheHid();
                }

                if ((myDeviceDetected == true))
                {
                    //  Get the bytes to send in a report from the combo boxes.
                    //  Increment the values if the autoincrement check box is selected.

                    ExchangeInputAndOutputReports();

                }
            }
            catch (Exception)
            {
                //DisplayException("ReadAndWriteToDevice", ex);
                throw;
            }
        }

        private void ExchangeInputAndOutputReports()
        {
            String byteValue = null;
            Int32 count = 0;
            Byte[] inputReportBuffer = null;
            Byte[] outputReportBuffer = null;
            Boolean success = false;

            try
            {
                success = false;

                //  Don't attempt to exchange reports if valid handles aren't available
                //  (as for a mouse or keyboard under Windows 2000/XP.)

                if (!hidHandle.IsInvalid)
                {
                    //  Don't attempt to send an Output report if the HID has no Output report.                                    

                    //throw new Exception("Capabilities" + MyHid.Capabilities.OutputReportByteLength.ToString());

                    if (MyHid.Capabilities.OutputReportByteLength > 0)
                    {
                        //  Set the size of the Output report buffer.   

                        outputReportBuffer = new Byte[MyHid.Capabilities.OutputReportByteLength];

                        //  Store the report ID in the first byte of the buffer:

                        outputReportBuffer[0] = 0;

                        //  Store the report data following the report ID.
                        //  Use the data in the combo boxes on the form.

                        outputReportBuffer[1] = Convert.ToByte(0);

                        if (Information.UBound(outputReportBuffer, 1) > 1)
                        {
                            outputReportBuffer[2] = Convert.ToByte(0);
                        }

                        //  Write a report.


                        // If the HID has an interrupt OUT endpoint, the host uses an 
                        // interrupt transfer to send the report. 
                        // If not, the host uses a control transfer.

                        if (fileStreamDeviceData.CanWrite)
                        {
                            fileStreamDeviceData.Write(outputReportBuffer, 0, outputReportBuffer.Length);
                            success = true;
                        }

                        if (success)
                        {
                            //lstMessages.Items.Add("An Output report has been written.");

                            //  Display the report data in the form's list box.

                            //lstMessages.Items.Add(" Output Report ID: " + String.Format("{0:X2} ", outputReportBuffer[0]));
                            //lstMessages.Items.Add(" Output Report Data:");

                            //txtBytesReceived.Text = "";
                            for (count = 0; count <= outputReportBuffer.Length - 1; count++)
                            {
                                //  Display bytes as 2-character hex strings.

                                byteValue = String.Format("{0:X2}", outputReportBuffer[count]);
                                //lstMessages.Items.Add(" " + byteValue);
                            }
                        }
                        else
                        {
                            CloseCommunications();
                            //lstMessages.Items.Add("The attempt to write an Output report failed.");
                        }
                    }
                    else
                    {
                        //lstMessages.Items.Add("The HID doesn't have an Output report.");
                    }

                    //  Read an Input report.

                    success = false;

                    //  Don't attempt to send an Input report if the HID has no Input report.
                    //  (The HID spec requires all HIDs to have an interrupt IN endpoint,
                    //  which suggests that all HIDs must support Input reports.)

                    if (MyHid.Capabilities.InputReportByteLength > 0)
                    {
                        //  Set the size of the Input report buffer. 

                        inputReportBuffer = new Byte[MyHid.Capabilities.InputReportByteLength];


                        //  Read a report using interrupt transfers.                
                        //  To enable reading a report without blocking the main thread, this
                        //  application uses an asynchronous delegate.

                        IAsyncResult ar = null;
                        transferInProgress = true;

                        // Timeout if no report is available.

                        //tmrReadTimeout.Start();

                        if (fileStreamDeviceData.CanRead)
                        {
                            fileStreamDeviceData.Read(inputReportBuffer, 0, inputReportBuffer.Length);
                            int buflen = inputReportBuffer.Length - 1;
                            string Weight = String.Format("{0:X2}", inputReportBuffer[buflen]) +
                                String.Format("{0:X2}", inputReportBuffer[buflen - 1]);
                            //MessageBox.Show(WeightData);
                            decimal weightd = Convert.ToInt32(Weight, 16);
                            weightd = weightd / 100;
                            //throw new Exception("Weight:" + weightd.ToString());
                            this.WeightData = weightd;
                        }
                        else
                        {
                            CloseCommunications();
                            //lstMessages.Items.Add("The attempt to read an Input report has failed.");
                        }

                    }
                    else
                    {
                        //lstMessages.Items.Add("No attempt to read an Input report was made.");
                        //lstMessages.Items.Add("The HID doesn't have an Input report.");
                        //AccessForm("EnableCmdOnce", "");
                    }
                }
                else
                {
                    //lstMessages.Items.Add("Invalid handle. The device is probably a system mouse or keyboard.");
                    //lstMessages.Items.Add("No attempt to write an Output report or read an Input report was made.");
                    //AccessForm("EnableCmdOnce", "");
                }
                //ScrollToBottomOfListBox();
            }
            catch (Exception)
            {
                // DisplayException("", ex);
                throw;// new Exception("i dont knw");
            }
        }

        //private void MyMarshalToForm(String action, String textToDisplay)
        //{
        //    object[] args = { action, textToDisplay };
        //    MarshalToForm MarshalToFormDelegate = null;

        //    //  The AccessForm routine contains the code that accesses the form.

        //    MarshalToFormDelegate = new MarshalToForm(AccessForm);

        //    //  Execute AccessForm, passing the parameters in args.

        //    base.Invoke(MarshalToFormDelegate, args);
        //}

        private void GetInputReportData(IAsyncResult ar)
        {
            Byte[] inputReportBuffer = null;

            try
            {
                inputReportBuffer = (byte[])ar.AsyncState;

                fileStreamDeviceData.EndRead(ar);

                //tmrReadTimeout.Stop();

                if ((ar.IsCompleted))
                {
                    //MyMarshalToForm("AddItemToListBox", "An Input report has been read.");
                    //MyMarshalToForm("AddItemToListBox", " Input Report ID: " + String.Format("{0:X2} ", inputReportBuffer[0]));
                    //MyMarshalToForm("AddItemToListBox", " Input Report Data:");                    

                    int buflen = inputReportBuffer.Length - 1;
                    string Weight = String.Format("{0:X2}", inputReportBuffer[buflen]) +
                        String.Format("{0:X2}", inputReportBuffer[buflen - 1]);
                    //MessageBox.Show(WeightData);
                    decimal weightd = Convert.ToInt32(Weight, 16);
                    weightd = weightd / 100;
                    throw new Exception("Weight:" + weightd.ToString());
                    this.WeightData = weightd;
                    //MyMarshalToForm("SetWeight", weightd.ToString());
                }

                transferInProgress = false;
            }
            catch (Exception)
            {
                //DisplayException("", ex);
                throw;
            }
        }

        //private void AccessForm(String action, String formText)
        //{
        //    try
        //    {
        //        //  Select an action to perform on the form:

        //        switch (action)
        //        {
        //            case "AddItemToListBox":

        //                lstMessages.Items.Add(formText);

        //                break;
        //            case "AddItemToTextBox":

        //                //txtBytesReceived.SelectedText = formText + "\r\n";

        //                break;

        //            case "ScrollToBottomOfListBox":

        //                lstMessages.SelectedIndex = lstMessages.Items.Count - 1;

        //                break;
        //            case "TextBoxSelectionStart":
        //                //txtBytesReceived.SelectionStart = formText.Length;

        //                break;
        //            case "SetWeight":
        //                txtWeight.Text = formText;
        //                break;
        //            default:

        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        DisplayException(this.Name, ex);
        //        throw;
        //    }
        //}   
    }
}