﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmProcessing : Form
    {
        private readonly BackgroundWorker worker;
        public bool close_win = false;

        public frmProcessing(BackgroundWorker worker)
        {
            InitializeComponent();
            this.worker = worker;
        }

        public frmProcessing()
        {
            InitializeComponent();
            this.worker = null;
        }

        private void frmProcessing_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!close_win)
            {
                e.Cancel = e.CloseReason == CloseReason.UserClosing;
            }
        }

        public void ShowProgress(string message, int step, int total)
        {
            MessageLabel.Text = message;
            ProgressBar.Style = ProgressBarStyle.Blocks;
            ProgressBar.Maximum = total;
            ProgressBar.Value = step;
            Visible = true;
        }

        public void ShowProgress(string message)
        {
            MessageLabel.Text = message;
            ProgressBar.Style = ProgressBarStyle.Marquee;
            Visible = true;
        }

        public void ShowProgress2(string message)
        {
            MessageLabel.Text = message;
            ProgressBar.Style = ProgressBarStyle.Marquee;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            if (worker != null)
            {
                worker.CancelAsync();
            }
            
        }

        public void Close_Me()
        {
            close_win = true;
            this.Close();
            close_win = false;
        }
    }
}
