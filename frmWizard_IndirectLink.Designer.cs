﻿namespace InteGr8
{
    partial class frmWizard_IndirectLink
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard_IndirectLink));
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnHelp = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbRightParentType = new System.Windows.Forms.ComboBox();
            this.lblRightParentType = new System.Windows.Forms.Label();
            this.lblRightParentTable = new System.Windows.Forms.Label();
            this.lbRightParentPK = new System.Windows.Forms.ListBox();
            this.lblRightParentPK = new System.Windows.Forms.Label();
            this.cbRightParentTable = new System.Windows.Forms.ComboBox();
            this.llRightParentPreview = new System.Windows.Forms.LinkLabel();
            this.cbLeftParentType = new System.Windows.Forms.ComboBox();
            this.lblLeftParentType = new System.Windows.Forms.Label();
            this.lblLeftParentTable = new System.Windows.Forms.Label();
            this.lbLeftParentPK = new System.Windows.Forms.ListBox();
            this.lblLeftParentPK = new System.Windows.Forms.Label();
            this.cbLeftParentTable = new System.Windows.Forms.ComboBox();
            this.llLeftParentPreview = new System.Windows.Forms.LinkLabel();
            this.lblChildTable = new System.Windows.Forms.Label();
            this.lbLeftChildFK = new System.Windows.Forms.ListBox();
            this.lbChildPK = new System.Windows.Forms.ListBox();
            this.lblLeftChildFK = new System.Windows.Forms.Label();
            this.lblChildPK = new System.Windows.Forms.Label();
            this.cbChildType = new System.Windows.Forms.ComboBox();
            this.lblChildType = new System.Windows.Forms.Label();
            this.cbChildTable = new System.Windows.Forms.ComboBox();
            this.llChildPreview = new System.Windows.Forms.LinkLabel();
            this.lbRightChildFK = new System.Windows.Forms.ListBox();
            this.lblRightChildFK = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.lblLeft = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(12, 482);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(801, 2);
            this.lblFooterLine.TabIndex = 133;
            // 
            // btnRemove
            // 
            this.btnRemove.Enabled = false;
            this.btnRemove.Location = new System.Drawing.Point(93, 494);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 15;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(737, 494);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnHelp
            // 
            this.btnHelp.Location = new System.Drawing.Point(12, 494);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 23);
            this.btnHelp.TabIndex = 14;
            this.btnHelp.Text = "Help";
            this.btnHelp.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(656, 494);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // cbRightParentType
            // 
            this.cbRightParentType.FormattingEnabled = true;
            this.cbRightParentType.Location = new System.Drawing.Point(629, 39);
            this.cbRightParentType.Name = "cbRightParentType";
            this.cbRightParentType.Size = new System.Drawing.Size(186, 21);
            this.cbRightParentType.TabIndex = 11;
            // 
            // lblRightParentType
            // 
            this.lblRightParentType.AutoSize = true;
            this.lblRightParentType.Location = new System.Drawing.Point(552, 42);
            this.lblRightParentType.Name = "lblRightParentType";
            this.lblRightParentType.Size = new System.Drawing.Size(64, 13);
            this.lblRightParentType.TabIndex = 140;
            this.lblRightParentType.Text = "Table Type:";
            // 
            // lblRightParentTable
            // 
            this.lblRightParentTable.AutoSize = true;
            this.lblRightParentTable.Location = new System.Drawing.Point(552, 15);
            this.lblRightParentTable.Name = "lblRightParentTable";
            this.lblRightParentTable.Size = new System.Drawing.Size(71, 13);
            this.lblRightParentTable.TabIndex = 139;
            this.lblRightParentTable.Text = "Parent Table:";
            // 
            // lbRightParentPK
            // 
            this.lbRightParentPK.FormattingEnabled = true;
            this.lbRightParentPK.IntegralHeight = false;
            this.lbRightParentPK.Location = new System.Drawing.Point(555, 102);
            this.lbRightParentPK.Name = "lbRightParentPK";
            this.lbRightParentPK.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbRightParentPK.Size = new System.Drawing.Size(260, 352);
            this.lbRightParentPK.TabIndex = 12;
            // 
            // lblRightParentPK
            // 
            this.lblRightParentPK.Location = new System.Drawing.Point(552, 70);
            this.lblRightParentPK.Name = "lblRightParentPK";
            this.lblRightParentPK.Size = new System.Drawing.Size(263, 26);
            this.lblRightParentPK.TabIndex = 138;
            this.lblRightParentPK.Text = "Please select the Primary Key in the Parent table that uniquely identifies a reco" +
                "rd:";
            // 
            // cbRightParentTable
            // 
            this.cbRightParentTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRightParentTable.FormattingEnabled = true;
            this.cbRightParentTable.Location = new System.Drawing.Point(629, 12);
            this.cbRightParentTable.Name = "cbRightParentTable";
            this.cbRightParentTable.Size = new System.Drawing.Size(186, 21);
            this.cbRightParentTable.TabIndex = 10;
            // 
            // llRightParentPreview
            // 
            this.llRightParentPreview.AutoSize = true;
            this.llRightParentPreview.Location = new System.Drawing.Point(552, 458);
            this.llRightParentPreview.Name = "llRightParentPreview";
            this.llRightParentPreview.Size = new System.Drawing.Size(128, 13);
            this.llRightParentPreview.TabIndex = 13;
            this.llRightParentPreview.TabStop = true;
            this.llRightParentPreview.Text = "Preview parent table data";
            // 
            // cbLeftParentType
            // 
            this.cbLeftParentType.FormattingEnabled = true;
            this.cbLeftParentType.Location = new System.Drawing.Point(89, 39);
            this.cbLeftParentType.Name = "cbLeftParentType";
            this.cbLeftParentType.Size = new System.Drawing.Size(186, 21);
            this.cbLeftParentType.TabIndex = 1;
            // 
            // lblLeftParentType
            // 
            this.lblLeftParentType.AutoSize = true;
            this.lblLeftParentType.Location = new System.Drawing.Point(12, 42);
            this.lblLeftParentType.Name = "lblLeftParentType";
            this.lblLeftParentType.Size = new System.Drawing.Size(64, 13);
            this.lblLeftParentType.TabIndex = 147;
            this.lblLeftParentType.Text = "Table Type:";
            // 
            // lblLeftParentTable
            // 
            this.lblLeftParentTable.AutoSize = true;
            this.lblLeftParentTable.Location = new System.Drawing.Point(12, 15);
            this.lblLeftParentTable.Name = "lblLeftParentTable";
            this.lblLeftParentTable.Size = new System.Drawing.Size(71, 13);
            this.lblLeftParentTable.TabIndex = 146;
            this.lblLeftParentTable.Text = "Parent Table:";
            // 
            // lbLeftParentPK
            // 
            this.lbLeftParentPK.FormattingEnabled = true;
            this.lbLeftParentPK.IntegralHeight = false;
            this.lbLeftParentPK.Location = new System.Drawing.Point(15, 102);
            this.lbLeftParentPK.Name = "lbLeftParentPK";
            this.lbLeftParentPK.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbLeftParentPK.Size = new System.Drawing.Size(260, 352);
            this.lbLeftParentPK.TabIndex = 2;
            // 
            // lblLeftParentPK
            // 
            this.lblLeftParentPK.Location = new System.Drawing.Point(12, 70);
            this.lblLeftParentPK.Name = "lblLeftParentPK";
            this.lblLeftParentPK.Size = new System.Drawing.Size(263, 26);
            this.lblLeftParentPK.TabIndex = 145;
            this.lblLeftParentPK.Text = "Please select the Primary Key in the Parent table that uniquely identifies a reco" +
                "rd:";
            // 
            // cbLeftParentTable
            // 
            this.cbLeftParentTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLeftParentTable.FormattingEnabled = true;
            this.cbLeftParentTable.Location = new System.Drawing.Point(89, 12);
            this.cbLeftParentTable.Name = "cbLeftParentTable";
            this.cbLeftParentTable.Size = new System.Drawing.Size(186, 21);
            this.cbLeftParentTable.TabIndex = 0;
            // 
            // llLeftParentPreview
            // 
            this.llLeftParentPreview.AutoSize = true;
            this.llLeftParentPreview.Location = new System.Drawing.Point(12, 461);
            this.llLeftParentPreview.Name = "llLeftParentPreview";
            this.llLeftParentPreview.Size = new System.Drawing.Size(128, 13);
            this.llLeftParentPreview.TabIndex = 3;
            this.llLeftParentPreview.TabStop = true;
            this.llLeftParentPreview.Text = "Preview parent table data";
            // 
            // lblChildTable
            // 
            this.lblChildTable.AutoSize = true;
            this.lblChildTable.Location = new System.Drawing.Point(289, 15);
            this.lblChildTable.Name = "lblChildTable";
            this.lblChildTable.Size = new System.Drawing.Size(63, 13);
            this.lblChildTable.TabIndex = 154;
            this.lblChildTable.Text = "Child Table:";
            // 
            // lbLeftChildFK
            // 
            this.lbLeftChildFK.FormattingEnabled = true;
            this.lbLeftChildFK.IntegralHeight = false;
            this.lbLeftChildFK.Location = new System.Drawing.Point(292, 238);
            this.lbLeftChildFK.Name = "lbLeftChildFK";
            this.lbLeftChildFK.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbLeftChildFK.Size = new System.Drawing.Size(246, 82);
            this.lbLeftChildFK.TabIndex = 7;
            // 
            // lbChildPK
            // 
            this.lbChildPK.FormattingEnabled = true;
            this.lbChildPK.IntegralHeight = false;
            this.lbChildPK.Location = new System.Drawing.Point(292, 102);
            this.lbChildPK.Name = "lbChildPK";
            this.lbChildPK.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbChildPK.Size = new System.Drawing.Size(246, 87);
            this.lbChildPK.TabIndex = 6;
            // 
            // lblLeftChildFK
            // 
            this.lblLeftChildFK.Location = new System.Drawing.Point(289, 196);
            this.lblLeftChildFK.Name = "lblLeftChildFK";
            this.lblLeftChildFK.Size = new System.Drawing.Size(249, 39);
            this.lblLeftChildFK.TabIndex = 153;
            this.lblLeftChildFK.Text = "Please select the Foreign Key column in the Child table that contains a value poi" +
                "nting to a unique Left Parent entry:";
            // 
            // lblChildPK
            // 
            this.lblChildPK.Location = new System.Drawing.Point(289, 70);
            this.lblChildPK.Name = "lblChildPK";
            this.lblChildPK.Size = new System.Drawing.Size(249, 26);
            this.lblChildPK.TabIndex = 156;
            this.lblChildPK.Text = "Please select the Primary Key column in the Child table that uniquely identifies " +
                "a record:";
            // 
            // cbChildType
            // 
            this.cbChildType.FormattingEnabled = true;
            this.cbChildType.Location = new System.Drawing.Point(372, 39);
            this.cbChildType.Name = "cbChildType";
            this.cbChildType.Size = new System.Drawing.Size(166, 21);
            this.cbChildType.TabIndex = 5;
            this.cbChildType.SelectedIndexChanged += new System.EventHandler(this.cbChildType_SelectedIndexChanged);
            // 
            // lblChildType
            // 
            this.lblChildType.AutoSize = true;
            this.lblChildType.Location = new System.Drawing.Point(289, 42);
            this.lblChildType.Name = "lblChildType";
            this.lblChildType.Size = new System.Drawing.Size(64, 13);
            this.lblChildType.TabIndex = 155;
            this.lblChildType.Text = "Table Type:";
            // 
            // cbChildTable
            // 
            this.cbChildTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChildTable.FormattingEnabled = true;
            this.cbChildTable.Location = new System.Drawing.Point(372, 12);
            this.cbChildTable.Name = "cbChildTable";
            this.cbChildTable.Size = new System.Drawing.Size(166, 21);
            this.cbChildTable.TabIndex = 4;
            // 
            // llChildPreview
            // 
            this.llChildPreview.AutoSize = true;
            this.llChildPreview.Location = new System.Drawing.Point(289, 461);
            this.llChildPreview.Name = "llChildPreview";
            this.llChildPreview.Size = new System.Drawing.Size(120, 13);
            this.llChildPreview.TabIndex = 9;
            this.llChildPreview.TabStop = true;
            this.llChildPreview.Text = "Preview child table data";
            // 
            // lbRightChildFK
            // 
            this.lbRightChildFK.FormattingEnabled = true;
            this.lbRightChildFK.IntegralHeight = false;
            this.lbRightChildFK.Location = new System.Drawing.Point(292, 372);
            this.lbRightChildFK.Name = "lbRightChildFK";
            this.lbRightChildFK.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbRightChildFK.Size = new System.Drawing.Size(246, 82);
            this.lbRightChildFK.TabIndex = 8;
            // 
            // lblRightChildFK
            // 
            this.lblRightChildFK.Location = new System.Drawing.Point(289, 328);
            this.lblRightChildFK.Name = "lblRightChildFK";
            this.lblRightChildFK.Size = new System.Drawing.Size(249, 39);
            this.lblRightChildFK.TabIndex = 158;
            this.lblRightChildFK.Text = "Please select the Foreign Key column in the Child table that contains a value poi" +
                "nting to a unique Right Parent entry:";
            // 
            // lblRight
            // 
            this.lblRight.AutoSize = true;
            this.lblRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRight.Location = new System.Drawing.Point(541, 405);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(14, 13);
            this.lblRight.TabIndex = 159;
            this.lblRight.Text = ">";
            // 
            // lblLeft
            // 
            this.lblLeft.AutoSize = true;
            this.lblLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeft.Location = new System.Drawing.Point(276, 271);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(14, 13);
            this.lblLeft.TabIndex = 160;
            this.lblLeft.Text = "<";
            // 
            // frmWizard_IndirectLink
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 529);
            this.Controls.Add(this.lblLeft);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.lbRightChildFK);
            this.Controls.Add(this.lblRightChildFK);
            this.Controls.Add(this.lblChildTable);
            this.Controls.Add(this.lbLeftChildFK);
            this.Controls.Add(this.lbChildPK);
            this.Controls.Add(this.lblLeftChildFK);
            this.Controls.Add(this.lblChildPK);
            this.Controls.Add(this.cbChildType);
            this.Controls.Add(this.lblChildType);
            this.Controls.Add(this.cbChildTable);
            this.Controls.Add(this.llChildPreview);
            this.Controls.Add(this.cbLeftParentType);
            this.Controls.Add(this.lblLeftParentType);
            this.Controls.Add(this.lblLeftParentTable);
            this.Controls.Add(this.lbLeftParentPK);
            this.Controls.Add(this.lblLeftParentPK);
            this.Controls.Add(this.cbLeftParentTable);
            this.Controls.Add(this.llLeftParentPreview);
            this.Controls.Add(this.cbRightParentType);
            this.Controls.Add(this.lblRightParentType);
            this.Controls.Add(this.lblRightParentTable);
            this.Controls.Add(this.lbRightParentPK);
            this.Controls.Add(this.lblRightParentPK);
            this.Controls.Add(this.cbRightParentTable);
            this.Controls.Add(this.llRightParentPreview);
            this.Controls.Add(this.lblFooterLine);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWizard_IndirectLink";
            this.Text = "Indirect Link Editor - InteGr8 Template Wizard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cbRightParentType;
        private System.Windows.Forms.Label lblRightParentType;
        private System.Windows.Forms.Label lblRightParentTable;
        private System.Windows.Forms.ListBox lbRightParentPK;
        private System.Windows.Forms.Label lblRightParentPK;
        private System.Windows.Forms.ComboBox cbRightParentTable;
        private System.Windows.Forms.LinkLabel llRightParentPreview;
        private System.Windows.Forms.ComboBox cbLeftParentType;
        private System.Windows.Forms.Label lblLeftParentType;
        private System.Windows.Forms.Label lblLeftParentTable;
        private System.Windows.Forms.ListBox lbLeftParentPK;
        private System.Windows.Forms.Label lblLeftParentPK;
        private System.Windows.Forms.ComboBox cbLeftParentTable;
        private System.Windows.Forms.LinkLabel llLeftParentPreview;
        private System.Windows.Forms.Label lblChildTable;
        private System.Windows.Forms.ListBox lbLeftChildFK;
        private System.Windows.Forms.ListBox lbChildPK;
        private System.Windows.Forms.Label lblLeftChildFK;
        private System.Windows.Forms.Label lblChildPK;
        private System.Windows.Forms.ComboBox cbChildType;
        private System.Windows.Forms.Label lblChildType;
        private System.Windows.Forms.ComboBox cbChildTable;
        private System.Windows.Forms.LinkLabel llChildPreview;
        private System.Windows.Forms.ListBox lbRightChildFK;
        private System.Windows.Forms.Label lblRightChildFK;
        private System.Windows.Forms.Label lblRight;
        private System.Windows.Forms.Label lblLeft;
    }
}