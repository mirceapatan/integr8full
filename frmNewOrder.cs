﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmNewOrder : Form
    {
        Data data = new Data();
        public frmNewOrder()
        {
            InitializeComponent();
            data = InteGr8_Main.data;
        }

        private void btnNewOrderOK_Click(object sender, EventArgs e)
        {
            try
            {
                string orderNum = tbShipmentNumber.Text;
                if (String.IsNullOrEmpty(orderNum) || String.IsNullOrWhiteSpace(orderNum))
                {
                    MessageBox.Show("Please enter a value for the shipment ID", "Shipment ID empty", MessageBoxButtons.OK);
                    return;
                }

                Data.OrdersTableRow row = data.OrdersTable.FindBy_Order__(orderNum);
                if (row != null)
                {
                    data.OrdersTable.Rows.Remove(row);
                }
                string order_id = "";

                string guid = DateTime.Now.Ticks.ToString();
                guid = guid.Substring(guid.Length - 15, 15);

                for (int i = 1; i < 10000; i++)
                {
                    string s = string.Format("{0:0000}", i);
                    if (data.OrdersTable.Select("[Order #] = '" + orderNum + "." + guid + "." + s + "'").Count() == 0)
                    {
                        order_id = orderNum + "." + guid + "." + s;
                        break;
                    }
                }
                Data.OrdersTableRow newOrder = data.OrdersTable.AddOrdersTableRow(order_id, "", "", null, "", "", "", "", "", "", DateTime.Today, "", false, null);

                if (data.OrdersTable.Columns.Contains("IID"))
                {
                    newOrder["IID"] = orderNum;
                }

                if (data.OrdersTable.Columns.Contains("OrderNumber"))
                {
                    newOrder["OrderNumber"] = orderNum;
                }
                if (data.OrdersTable.Columns.Contains("Shipment Reference Info"))
                {
                    newOrder["Shipment Reference Info"] = orderNum;
                }
                Data.OrderPackagesTableRow newPackage = data.OrderPackagesTable.AddOrderPackagesTableRow(newOrder, orderNum, 0, "LBS", 0, 0, 0, "I", 0, "USD", "");
                if (data.OrderPackagesTable.Columns.Contains("IID"))
                {
                    newPackage["IID"] = orderNum;
                }
                if (data.OrderPackagesTable.Columns.Contains("Package Reference Info 1"))
                {
                    newPackage["Package Reference Info 1"] = orderNum;
                }
                Data.OrdersSkidsTableRow newSkid = data.OrdersSkidsTable.AddOrdersSkidsTableRow(newOrder, orderNum, "1", "1", "false", "true", "1", "0", "LBS", "0",
                                                    "USD", "0", "0", "0", "I", "", "", "", "", "", "", "", "125.0", "1", "", "");
                if (data.OrdersSkidsTable.Columns.Contains("IID"))
                {
                    newSkid["IID"] = orderNum;
                }
                Data.OrderLTLItemsRow newLTLItem = data.OrderLTLItems.AddOrderLTLItemsRow("1", "", "0", "LBS", "0", "0", "0", "I", "125.0", "0", "PCS", newOrder, "1");
                if (data.OrderLTLItems.Columns.Contains("IID"))
                {
                    newLTLItem["IID"] = orderNum;
                }
                Data.OrderProductsTableRow newProduct = data.OrderProductsTable.AddOrderProductsTableRow(newOrder, "1", "", false, "", 1, "PCS", 1, 1, 1, "", 1);
                if (data.OrderProductsTable.Columns.Contains("IID"))
                {
                    newProduct["IID"] = orderNum;
                }

                }
            catch (Exception ex)
            {
                Utils.SendError("Error creating new empty order", ex);
            }
            finally
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
        }

        private void btnNewOrderCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void tbShipmentNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r') btnNewOrderOK_Click(sender, e);
        }
    }
}
