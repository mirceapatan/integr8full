﻿namespace InteGr8
{
    partial class frmWizard_Fields
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard_Fields));
            this.Categories = new System.Windows.Forms.TabControl();
            this.Orders = new System.Windows.Forms.TabPage();
            this.llTableEdit = new System.Windows.Forms.LinkLabel();
            this.lblTable = new System.Windows.Forms.Label();
            this.Bindings = new System.Windows.Forms.DataGridView();
            this.CategoryColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.FieldColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TableColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LinkColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ColumnColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DefaultColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Senders = new System.Windows.Forms.TabPage();
            this.Recipients = new System.Windows.Forms.TabPage();
            this.OrderPackages = new System.Windows.Forms.TabPage();
            this.OrderProducts = new System.Windows.Forms.TabPage();
            this.Shipments = new System.Windows.Forms.TabPage();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Categories.SuspendLayout();
            this.Orders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Bindings)).BeginInit();
            this.pnlHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // Categories
            // 
            this.Categories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Categories.Controls.Add(this.Orders);
            this.Categories.Controls.Add(this.Recipients);
            this.Categories.Controls.Add(this.Senders);
            this.Categories.Controls.Add(this.OrderPackages);
            this.Categories.Controls.Add(this.OrderProducts);
            this.Categories.Controls.Add(this.Shipments);
            this.Categories.Location = new System.Drawing.Point(12, 96);
            this.Categories.Multiline = true;
            this.Categories.Name = "Categories";
            this.Categories.SelectedIndex = 0;
            this.Categories.Size = new System.Drawing.Size(700, 334);
            this.Categories.TabIndex = 117;
            this.Categories.SelectedIndexChanged += new System.EventHandler(this.Categories_SelectedIndexChanged);
            // 
            // Orders
            // 
            this.Orders.Controls.Add(this.llTableEdit);
            this.Orders.Controls.Add(this.lblTable);
            this.Orders.Controls.Add(this.Bindings);
            this.Orders.Location = new System.Drawing.Point(4, 22);
            this.Orders.Name = "Orders";
            this.Orders.Padding = new System.Windows.Forms.Padding(3);
            this.Orders.Size = new System.Drawing.Size(692, 308);
            this.Orders.TabIndex = 0;
            this.Orders.Text = "Orders";
            this.Orders.UseVisualStyleBackColor = true;
            // 
            // llTableEdit
            // 
            this.llTableEdit.AutoSize = true;
            this.llTableEdit.Location = new System.Drawing.Point(116, 12);
            this.llTableEdit.Name = "llTableEdit";
            this.llTableEdit.Size = new System.Drawing.Size(25, 13);
            this.llTableEdit.TabIndex = 123;
            this.llTableEdit.TabStop = true;
            this.llTableEdit.Text = "Edit";
            this.llTableEdit.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llTableEdit_LinkClicked);
            // 
            // lblTable
            // 
            this.lblTable.AutoSize = true;
            this.lblTable.Location = new System.Drawing.Point(3, 12);
            this.lblTable.Name = "lblTable";
            this.lblTable.Size = new System.Drawing.Size(107, 13);
            this.lblTable.TabIndex = 122;
            this.lblTable.Text = "<CATEGORY> table:";
            // 
            // Bindings
            // 
            this.Bindings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Bindings.BackgroundColor = System.Drawing.SystemColors.Window;
            this.Bindings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Bindings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CategoryColumn,
            this.FieldColumn,
            this.TableColumn,
            this.LinkColumn,
            this.ColumnColumn,
            this.DefaultColumn});
            this.Bindings.Location = new System.Drawing.Point(6, 38);
            this.Bindings.MultiSelect = false;
            this.Bindings.Name = "Bindings";
            this.Bindings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Bindings.Size = new System.Drawing.Size(680, 264);
            this.Bindings.TabIndex = 120;
            this.Bindings.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Bindings_CellContentClick);
            this.Bindings.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.Bindings_CellEndEdit);
            this.Bindings.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.Bindings_CellValidating);
            this.Bindings.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.Bindings_DataError);
            this.Bindings.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.Bindings_DefaultValuesNeeded);
            this.Bindings.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.Bindings_EditingControlShowing);
            this.Bindings.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.Bindings_UserDeletingRow);
            // 
            // CategoryColumn
            // 
            this.CategoryColumn.HeaderText = "Category";
            this.CategoryColumn.Name = "CategoryColumn";
            this.CategoryColumn.Visible = false;
            // 
            // FieldColumn
            // 
            this.FieldColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FieldColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.FieldColumn.DisplayStyleForCurrentCellOnly = true;
            this.FieldColumn.FillWeight = 20F;
            this.FieldColumn.HeaderText = "2Ship Field";
            this.FieldColumn.Name = "FieldColumn";
            // 
            // TableColumn
            // 
            this.TableColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TableColumn.DisplayStyleForCurrentCellOnly = true;
            this.TableColumn.FillWeight = 20F;
            this.TableColumn.HeaderText = "Field Table";
            this.TableColumn.Name = "TableColumn";
            // 
            // LinkColumn
            // 
            this.LinkColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LinkColumn.FillWeight = 20F;
            this.LinkColumn.HeaderText = "Table Link";
            this.LinkColumn.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.LinkColumn.Name = "LinkColumn";
            this.LinkColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LinkColumn.Text = "";
            this.LinkColumn.TrackVisitedState = false;
            // 
            // ColumnColumn
            // 
            this.ColumnColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnColumn.DisplayStyleForCurrentCellOnly = true;
            this.ColumnColumn.FillWeight = 20F;
            this.ColumnColumn.HeaderText = "Field Column";
            this.ColumnColumn.Name = "ColumnColumn";
            this.ColumnColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DefaultColumn
            // 
            this.DefaultColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DefaultColumn.FillWeight = 20F;
            this.DefaultColumn.HeaderText = "Default Value";
            this.DefaultColumn.MinimumWidth = 50;
            this.DefaultColumn.Name = "DefaultColumn";
            // 
            // Senders
            // 
            this.Senders.Location = new System.Drawing.Point(4, 22);
            this.Senders.Name = "Senders";
            this.Senders.Padding = new System.Windows.Forms.Padding(3);
            this.Senders.Size = new System.Drawing.Size(692, 308);
            this.Senders.TabIndex = 5;
            this.Senders.Text = "Senders";
            this.Senders.UseVisualStyleBackColor = true;
            // 
            // Recipients
            // 
            this.Recipients.Location = new System.Drawing.Point(4, 22);
            this.Recipients.Name = "Recipients";
            this.Recipients.Padding = new System.Windows.Forms.Padding(3);
            this.Recipients.Size = new System.Drawing.Size(692, 308);
            this.Recipients.TabIndex = 6;
            this.Recipients.Text = "Recipients";
            this.Recipients.UseVisualStyleBackColor = true;
            // 
            // OrderPackages
            // 
            this.OrderPackages.Location = new System.Drawing.Point(4, 22);
            this.OrderPackages.Name = "OrderPackages";
            this.OrderPackages.Padding = new System.Windows.Forms.Padding(3);
            this.OrderPackages.Size = new System.Drawing.Size(692, 308);
            this.OrderPackages.TabIndex = 1;
            this.OrderPackages.Text = "Packages";
            this.OrderPackages.UseVisualStyleBackColor = true;
            // 
            // OrderProducts
            // 
            this.OrderProducts.Location = new System.Drawing.Point(4, 22);
            this.OrderProducts.Name = "OrderProducts";
            this.OrderProducts.Padding = new System.Windows.Forms.Padding(3);
            this.OrderProducts.Size = new System.Drawing.Size(692, 308);
            this.OrderProducts.TabIndex = 2;
            this.OrderProducts.Text = "Products";
            this.OrderProducts.UseVisualStyleBackColor = true;
            // 
            // Shipments
            // 
            this.Shipments.Location = new System.Drawing.Point(4, 22);
            this.Shipments.Name = "Shipments";
            this.Shipments.Padding = new System.Windows.Forms.Padding(3);
            this.Shipments.Size = new System.Drawing.Size(692, 308);
            this.Shipments.TabIndex = 3;
            this.Shipments.Text = "Shipments";
            this.Shipments.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(637, 438);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 118;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPrev.Location = new System.Drawing.Point(556, 438);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 120;
            this.btnPrev.Text = "Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(12, 438);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(100, 23);
            this.btnPreview.TabIndex = 121;
            this.btnPreview.Text = "Preview Data";
            this.btnPreview.UseVisualStyleBackColor = true;
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.label1);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.label2);
            this.pnlHeader.Controls.Add(this.label3);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(724, 90);
            this.pnlHeader.TabIndex = 122;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(0, 88);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(724, 2);
            this.label1.TabIndex = 116;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(724, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Fields Setup";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.BackColor = System.Drawing.SystemColors.Window;
            this.label2.Image = global::InteGr8.Properties.Resources.data_next;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label2.Location = new System.Drawing.Point(0, 24);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(64, 64);
            this.label2.TabIndex = 115;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoEllipsis = true;
            this.label3.Location = new System.Drawing.Point(68, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.label3.Size = new System.Drawing.Size(656, 64);
            this.label3.TabIndex = 114;
            this.label3.Text = "Please map the 2Ship fields to your database table columns. If you don\'t have a c" +
                "orresponding column, or your column data might be missing, you can enter a defau" +
                "lt value to be used instead.";
            // 
            // frmWizard_Fields
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(724, 473);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.Categories);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWizard_Fields";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 9";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard_Fields_HelpButtonClicked);
            this.Categories.ResumeLayout(false);
            this.Orders.ResumeLayout(false);
            this.Orders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Bindings)).EndInit();
            this.pnlHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Categories;
        private System.Windows.Forms.TabPage Orders;
        private System.Windows.Forms.TabPage OrderPackages;
        private System.Windows.Forms.TabPage OrderProducts;
        private System.Windows.Forms.TabPage Shipments;
        private System.Windows.Forms.TabPage Senders;
        private System.Windows.Forms.TabPage Recipients;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.DataGridView Bindings;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.DataGridViewComboBoxColumn CategoryColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn FieldColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn TableColumn;
        private System.Windows.Forms.DataGridViewLinkColumn LinkColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefaultColumn;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Label lblTable;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel llTableEdit;
    }
}