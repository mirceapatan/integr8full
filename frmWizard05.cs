﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard05 : Form
    {
        private bool custom_binding = false;
        private FieldLink link = null;

        public frmWizard05()
        {
            InitializeComponent();
        }

        private void frmWizard05_Load(object sender, EventArgs e)
        {
            // populate the source table combobox with the ODBC catalog tables
            cbGeneralShipmentsTable.Items.Add("(none)");
            using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
            {
                Conn.Open();
                foreach (DataRow row in Conn.GetSchema("Tables").Rows)
                    cbGeneralShipmentsTable.Items.Add(row["TABLE_NAME"]);
            }

            // check that the link is OrdersOneToOne type and each field have the same link
            foreach (FieldBinding binding in InteGr8_Main.template.Bindings)
                if (binding.Field.FieldCategory == Data.FieldsTableDataTable.FieldCategory.OrderOther && binding.Link != null)
                    if (link == null) link = binding.Link;
                    else if (!binding.Link.Equals(link)) custom_binding = true;
            if (link != null && link.Type != FieldLink.LinkType.Orders && link.Type != FieldLink.LinkType.OrdersOneToOne) custom_binding = true;
            if (custom_binding)
            {
                foreach (Control control in this.Controls)
                    if (control is ComboBox) control.Enabled = false;
                MessageBox.Show(this, "Order Shipment configuration was customized and cannot be edited in this page. You will be able to edit in the summary page. Please click Next to skip this page.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (link == null) cbGeneralShipmentsTable.SelectedIndex = 0;
            else
            {
                int index = cbGeneralShipmentsTable.FindStringExact(link.FieldTable);
                if (index == -1)
                {
                    cbGeneralShipmentsTable.SelectedIndex = 0;
                    MessageBox.Show(this, "Order Shipments table '" + link.FieldTable + "' not found in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else cbGeneralShipmentsTable.SelectedIndex = index;
                if (link.Type != FieldLink.LinkType.Orders && link.FieldTablePK != null)
                {
                    cbGeneralShipmentsPrimaryKey.SelectedIndex = cbGeneralShipmentsPrimaryKey.FindStringExact(link.FieldTablePK);
                    if (cbGeneralShipmentsPrimaryKey.SelectedIndex == -1)
                    {
                        MessageBox.Show(this, "Order Shipments table primary key column '" + link.FieldTablePK + "' not found in the Order Shipments table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
            if (InteGr8_Main.template.Find("Ship Date") != null) cbShipDate.Text = InteGr8_Main.template.Find("Ship Date").Value;
            if (InteGr8_Main.template.Find("Carrier") != null) cbCarrier.Text = InteGr8_Main.template.Find("Carrier").Value;
            if (InteGr8_Main.template.Find("Service") != null) cbService.Text = InteGr8_Main.template.Find("Service").Value;
            if (InteGr8_Main.template.Find("Packaging") != null) cbPackaging.Text = InteGr8_Main.template.Find("Packaging").Value;
            if (InteGr8_Main.template.Find("Shipment Reference Info") != null) cbReference.Text = InteGr8_Main.template.Find("Shipment Reference Info").Value;
        }

        private void cbGeneralShipmentsTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // clear all column comboboxes
            cbGeneralShipmentsPrimaryKey.Items.Clear();
            cbShipDate.Items.Clear();
            cbCarrier.Items.Clear();
            cbService.Items.Clear();
            cbPackaging.Items.Clear();
            cbReference.Items.Clear();

            // if link is needed or not (Orders left join General Shipments)
            bool link = cbGeneralShipmentsTable.SelectedIndex > 0 && !cbGeneralShipmentsTable.Text.Equals(InteGr8_Main.template.Orders);
            cbGeneralShipmentsPrimaryKey.Enabled = link;

            // read the Recipients table columns and populate all the recipient comboboxes
            if (cbGeneralShipmentsTable.SelectedIndex > 0)
            {
                string[] columns = null;
                using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
                {
                    Conn.Open();
                    using (OdbcDataReader dr = new OdbcCommand("Select Top 0 * From " + cbGeneralShipmentsTable.Text, Conn).ExecuteReader(CommandBehavior.SingleRow))
                    {
                        columns = new string[dr.FieldCount];
                        for (int k = 0; k < dr.FieldCount; k++)
                            columns[k] = "[" + dr.GetName(k) + "]";
                    }
                }

                if (columns != null)
                {
                    cbGeneralShipmentsPrimaryKey.Items.AddRange(columns);
                    cbShipDate.Items.AddRange(columns);
                    cbShipDate.Items.AddRange(new string[] { "-- Current date of processing --", "-- Next day of processing --", "-- 2nd day of processing --", "-- 3rd day of processing --" });
                    cbCarrier.Items.AddRange(columns);
                    cbCarrier.Items.AddRange(new string[] { /*"-- Cheapest --", "-- Fastest --",*/ "-- FedEx --", "-- Purolator  -", "-- DHL --", "-- CA Post --" });
                    cbService.Items.AddRange(columns);
                    cbService.Items.AddRange(new string[] { /*"-- Cheapest --", "-- Fastest --" */ });
                    cbPackaging.Items.AddRange(columns);
                    cbPackaging.Items.AddRange(new string[] { "-- Customer Packaging --", "-- Envelope (Letter) --", "-- Small Pack --" });
                    cbReference.Items.AddRange(columns);
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (cbGeneralShipmentsPrimaryKey.Enabled && cbGeneralShipmentsPrimaryKey.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please choose the primary key column that also corresponds to the Order #.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // validate
            // ...

            if (!custom_binding)
            {
                if (cbGeneralShipmentsTable.SelectedIndex == 0) link = null;
                else if (cbGeneralShipmentsTable.Text.Equals(InteGr8_Main.template.Orders))
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.Orders;
                    link.FieldTable = cbGeneralShipmentsTable.Text;
                }
                else
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.OrdersOneToOne;
                    link.FieldTable = cbGeneralShipmentsTable.Text;
                    link.FieldTablePK = cbGeneralShipmentsPrimaryKey.Text;
                }

                InteGr8_Main.template.UpdateBinding("Ship Date", cbShipDate.FindStringExact(cbShipDate.Text) == -1, cbShipDate.Text.Trim().Equals("") ? null : cbShipDate.Text, link);
                InteGr8_Main.template.UpdateBinding("Carrier", cbCarrier.FindStringExact(cbCarrier.Text) == -1, cbCarrier.Text.Trim().Equals("") ? null : cbCarrier.Text, link);
                InteGr8_Main.template.UpdateBinding("Service", cbService.FindStringExact(cbService.Text) == -1, cbService.Text.Trim().Equals("") ? null : cbService.Text, link);
                InteGr8_Main.template.UpdateBinding("Packaging", cbPackaging.FindStringExact(cbPackaging.Text) == -1, cbPackaging.Text.Trim().Equals("") ? null : cbPackaging.Text, link);
                InteGr8_Main.template.UpdateBinding("Shipment Reference Info", cbReference.FindStringExact(cbReference.Text) == -1, cbReference.Text.Trim().Equals("") ? null : cbReference.Text, link);
            }
            DialogResult = DialogResult.OK;
        }

        private void frmWizard05_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK) return;
            if (DialogResult == DialogResult.No)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            DialogResult = DialogResult.Abort;
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (cbGeneralShipmentsTable.SelectedIndex <= 0 || cbGeneralShipmentsTable.Text.Trim().Equals("")) return;
            new frmWizard_DataPreview("General Shipments", InteGr8_Main.template.Connection, "Select Top 100 * From " + cbGeneralShipmentsTable.Text).ShowDialog(this);
        }

        private void frmWizard05_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_05");
            e.Cancel = true;
        }
    }
}
