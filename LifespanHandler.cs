﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace CefSharp.MinimalExample.WinForms
{
    public class LifeSpanHandler : ILifeSpanHandler
    {
        List<string> availableExtensionFiles = new List<string> { ".pdf", ".png" };

        public bool OnBeforePopup(IWebBrowser browserControl, IBrowser browser, IFrame frame, string targetUrl, string targetFrameName, WindowOpenDisposition targetDisposition, bool userGesture, IPopupFeatures popupFeatures, IWindowInfo windowInfo, IBrowserSettings browserSettings, ref bool noJavascriptAccess, out IWebBrowser newBrowser)
        {
            newBrowser = null;
            bool isProcessed = false;
            ProcessFileUrl(targetUrl, out isProcessed);

            return isProcessed;
        }

        private void ProcessFileUrl(string url, out bool isProcessed)
        {
            isProcessed = !string.IsNullOrEmpty(url) && 
                availableExtensionFiles.Contains(Path.GetExtension(url).ToLower());
            if (isProcessed)
            {
                string fileNamePath = Path.Combine( Path.GetTempPath(), Path.GetFileName(url));
                if (!File.Exists(fileNamePath))
                {
                    DownloadFile(url, fileNamePath);
                }
                OpenFile(fileNamePath);
            }
        }

        private void DownloadFile(string url, string fileName)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile(url, fileName);
            }
        }

        private void OpenFile(string fileName)
        {
            Process.Start(fileName);
        }

        public void OnAfterCreated(IWebBrowser browserControl, IBrowser browser){}

        public void OnBeforeClose(IWebBrowser browserControl, IBrowser browser) { }

        public bool DoClose(IWebBrowser browserControl, IBrowser browser) { return false; }
    }
}
