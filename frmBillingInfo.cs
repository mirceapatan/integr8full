﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmBillingInfo : Form
    {
        private List<string> billingInfo;
        InteGr8.Data data = InteGr8_Main.data;
        public frmBillingInfo()
        {
            InitializeComponent();
            billingInfo = new List<string>(11);
            cbBillingAccountsList.DataSource = data.BillingAccounts;
            cbBillingAccountsList.DisplayMember = "BillingAccount";
            cbBillingAccountsList.ValueMember = "BillingAccount";
        }

        private void btnAddBillingInfo_Click(object sender, EventArgs e)
        {
            //if (cbBillingAccountsList.SelectedIndex >= 0 && String.IsNullOrEmpty(tbBillingAccount.Text))
            //{
            //        Data.BillingAccountsRow row = data.BillingAccounts.Rows[cbBillingAccountsList.SelectedIndex] as Data.BillingAccountsRow; //.FindByAccount(cbBillingAccountsList.SelectedText);
            //        if (row != null)
            //        {
            //            tbBillingAccount.Text = row["BillingAccount"].ToString();
            //            tbBillingAddress1.Text = row["BillingAddress1"].ToString();
            //            tbBillingAddress2.Text = row["BillingAddress2"].ToString();
            //            tbBillingCity.Text = row["BillingCity"].ToString();
            //            tbBillingCompany.Text = row["BillingCompany"].ToString();
            //            tbBillingContact.Text = row["BillingContact"].ToString();
            //            tbBillingCountry.Text = row["BillingCountry"].ToString();
            //            tbBillingEmail.Text = row["BillingEmail"].ToString();
            //            tbBillingState.Text = row["BillingState"].ToString();
            //            tbBillingTel.Text = row["BillingTel"].ToString();
            //            tbBillingZIP.Text = row["BillingZIP"].ToString();
            //        }
            //}
            //else
            // {
            //    if (!String.IsNullOrEmpty(tbBillingAccount.Text))
            //      {
            //          if (data.BillingAccounts.FindByAccount(tbBillingAccount.Text) == null)
            //          {
            //              cbBillingAccountsList.DataSource = null;

            //              data.BillingAccounts.AddBillingAccountsRow(tbBillingAccount.Text, tbBillingCompany.Text, tbBillingContact.Text, tbBillingAddress1.Text,
            //                  tbBillingAddress2.Text, tbBillingCity.Text, tbBillingState.Text, tbBillingZIP.Text, tbBillingCountry.Text, tbBillingTel.Text, tbBillingEmail.Text);

            //              cbBillingAccountsList.DataSource = data.BillingAccounts;
            //              cbBillingAccountsList.DisplayMember = "BillingAccount";
            //              cbBillingAccountsList.ValueMember = "BillingAccount";
            //          }
            //          else
            //          {
            //              Data.BillingAccountsRow row = data.BillingAccounts.FindByAccount(tbBillingAccount.Text);
            //              tbBillingAccount.Text = row["BillingAccount"].ToString();
            //              tbBillingAddress1.Text = row["BillingAddress1"].ToString();
            //              tbBillingAddress2.Text = row["BillingAddress2"].ToString();
            //              tbBillingCity.Text = row["BillingCity"].ToString();
            //              tbBillingCompany.Text = row["BillingCompany"].ToString();
            //              tbBillingContact.Text = row["BillingContact"].ToString();
            //              tbBillingCountry.Text = row["BillingCountry"].ToString();
            //              tbBillingEmail.Text = row["BillingEmail"].ToString();
            //              tbBillingState.Text = row["BillingState"].ToString();
            //              tbBillingTel.Text = row["BillingTel"].ToString();
            //              tbBillingZIP.Text = row["BillingZIP"].ToString();
            //          }

            //      }
            // }
               
 
            billingInfo.Add(tbBillingAccount.Text);
            billingInfo.Add(tbBillingCompany.Text);
            billingInfo.Add(tbBillingContact.Text);
            billingInfo.Add(tbBillingCountry.Text);
            billingInfo.Add(tbBillingState.Text);
            billingInfo.Add(tbBillingCity.Text);
            billingInfo.Add(tbBillingAddress1.Text);
            billingInfo.Add(tbBillingAddress2.Text);
            billingInfo.Add(tbBillingZIP.Text);
            billingInfo.Add(tbBillingTel.Text);
            billingInfo.Add(tbBillingEmail.Text);
            //add the billing fields to the BillingAccounts table - which we will store in a xml file, and load at start
            //the billing account field will be changed to a dropdown, and will have as data source, the BillingAccounts table
            //selected index will be 0, in case there are billing accounts added
            //at selected index changed, the other textboxes will populate with the rest of the fields for the selected account

            //update the billing accounts in the file
            //data.BillingAccounts.WriteXml(Properties.Settings.Default.BillingAccounts);
        }
        public List<string> getBillingInfo
        {
            get
            {
                return billingInfo;
            }
        }

        private void btnCancelBilling_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddBillingAccount_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tbBillingAccount.Text))
            {
                MessageBox.Show("Please enter a billing account first!");
                return;
                //Data.BillingAccountsRow row = data.BillingAccounts.Rows[cbBillingAccountsList.SelectedIndex] as Data.BillingAccountsRow; //.FindByAccount(cbBillingAccountsList.SelectedText);
                //if (row != null)
                //{
                //    tbBillingAccount.Text = row["BillingAccount"].ToString();
                //    tbBillingAddress1.Text = row["BillingAddress1"].ToString();
                //    tbBillingAddress2.Text = row["BillingAddress2"].ToString();
                //    tbBillingCity.Text = row["BillingCity"].ToString();
                //    tbBillingCompany.Text = row["BillingCompany"].ToString();
                //    tbBillingContact.Text = row["BillingContact"].ToString();
                //    tbBillingCountry.Text = row["BillingCountry"].ToString();
                //    tbBillingEmail.Text = row["BillingEmail"].ToString();
                //    tbBillingState.Text = row["BillingState"].ToString();
                //    tbBillingTel.Text = row["BillingTel"].ToString();
                //    tbBillingZIP.Text = row["BillingZIP"].ToString();
                //}
            }
            else
            {
                //if (!String.IsNullOrEmpty(tbBillingAccount.Text))
                //{
                    if (data.BillingAccounts.FindByAccount(tbBillingAccount.Text) == null)
                    {
                        cbBillingAccountsList.DataSource = null;

                        data.BillingAccounts.AddBillingAccountsRow(tbBillingAccount.Text, tbBillingCompany.Text, tbBillingContact.Text, tbBillingAddress1.Text,
                            tbBillingAddress2.Text, tbBillingCity.Text, tbBillingState.Text, tbBillingZIP.Text, tbBillingCountry.Text, tbBillingTel.Text, tbBillingEmail.Text);

                        cbBillingAccountsList.DataSource = data.BillingAccounts;
                        cbBillingAccountsList.DisplayMember = "BillingAccount";
                        cbBillingAccountsList.ValueMember = "BillingAccount";
                        cbBillingAccountsList.SelectedIndex = data.BillingAccounts.Rows.Count - 1;
                    }
                    else
                    {
                        MessageBox.Show("This account number was already added!");
                        return;
                        //Data.BillingAccountsRow row = data.BillingAccounts.FindByAccount(tbBillingAccount.Text);
                        //tbBillingAccount.Text = row["BillingAccount"].ToString();
                        //tbBillingAddress1.Text = row["BillingAddress1"].ToString();
                        //tbBillingAddress2.Text = row["BillingAddress2"].ToString();
                        //tbBillingCity.Text = row["BillingCity"].ToString();
                        //tbBillingCompany.Text = row["BillingCompany"].ToString();
                        //tbBillingContact.Text = row["BillingContact"].ToString();
                        //tbBillingCountry.Text = row["BillingCountry"].ToString();
                        //tbBillingEmail.Text = row["BillingEmail"].ToString();
                        //tbBillingState.Text = row["BillingState"].ToString();
                        //tbBillingTel.Text = row["BillingTel"].ToString();
                        //tbBillingZIP.Text = row["BillingZIP"].ToString();
                    }

                //}
            }
            data.BillingAccounts.WriteXml(Properties.Settings.Default.BillingAccounts);
        }

        private void cbBillingAccountsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbBillingAccountsList.SelectedIndex >= 0)
            {
                Data.BillingAccountsRow row = data.BillingAccounts.Rows[cbBillingAccountsList.SelectedIndex] as Data.BillingAccountsRow;//.FindByAccount(cbBillingAccountsList.SelectedValue.ToString());
                if (row != null)
                {
                    tbBillingAccount.Text = row["BillingAccount"].ToString();
                    tbBillingAddress1.Text = row["BillingAddress1"].ToString();
                    tbBillingAddress2.Text = row["BillingAddress2"].ToString();
                    tbBillingCity.Text = row["BillingCity"].ToString();
                    tbBillingCompany.Text = row["BillingCompany"].ToString();
                    tbBillingContact.Text = row["BillingContact"].ToString();
                    tbBillingCountry.Text = row["BillingCountry"].ToString();
                    tbBillingEmail.Text = row["BillingEmail"].ToString();
                    tbBillingState.Text = row["BillingState"].ToString();
                    tbBillingTel.Text = row["BillingTel"].ToString();
                    tbBillingZIP.Text = row["BillingZIP"].ToString();
                }

            }
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }
    }
}
