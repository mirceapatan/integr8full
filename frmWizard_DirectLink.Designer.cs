﻿namespace InteGr8
{
    partial class frmWizard_DirectLink
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard_DirectLink));
            this.lblParentPK = new System.Windows.Forms.Label();
            this.cbParentTable = new System.Windows.Forms.ComboBox();
            this.cbChildTable = new System.Windows.Forms.ComboBox();
            this.btnHelp = new System.Windows.Forms.Button();
            this.llParentPreview = new System.Windows.Forms.LinkLabel();
            this.llChildPreview = new System.Windows.Forms.LinkLabel();
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.cbParentType = new System.Windows.Forms.ComboBox();
            this.lblParentType = new System.Windows.Forms.Label();
            this.lblParentTable = new System.Windows.Forms.Label();
            this.lbParentPK = new System.Windows.Forms.ListBox();
            this.ChildSplitContainer = new System.Windows.Forms.SplitContainer();
            this.lbChildPK = new System.Windows.Forms.ListBox();
            this.lblChildPK = new System.Windows.Forms.Label();
            this.lbChildFK = new System.Windows.Forms.ListBox();
            this.lblChildFK = new System.Windows.Forms.Label();
            this.cbChildType = new System.Windows.Forms.ComboBox();
            this.lblChildType = new System.Windows.Forms.Label();
            this.lblChildTable = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            this.ChildSplitContainer.Panel1.SuspendLayout();
            this.ChildSplitContainer.Panel2.SuspendLayout();
            this.ChildSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblParentPK
            // 
            this.lblParentPK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParentPK.Location = new System.Drawing.Point(-3, 61);
            this.lblParentPK.Name = "lblParentPK";
            this.lblParentPK.Size = new System.Drawing.Size(265, 26);
            this.lblParentPK.TabIndex = 31;
            this.lblParentPK.Text = "Please select the Primary Key in the Parent table that uniquely identifies a reco" +
                "rd:";
            // 
            // cbParentTable
            // 
            this.cbParentTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbParentTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbParentTable.FormattingEnabled = true;
            this.cbParentTable.Location = new System.Drawing.Point(74, 3);
            this.cbParentTable.Name = "cbParentTable";
            this.cbParentTable.Size = new System.Drawing.Size(188, 21);
            this.cbParentTable.TabIndex = 0;
            this.cbParentTable.SelectedIndexChanged += new System.EventHandler(this.cbParent_SelectedIndexChanged);
            // 
            // cbChildTable
            // 
            this.cbChildTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbChildTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChildTable.FormattingEnabled = true;
            this.cbChildTable.Location = new System.Drawing.Point(81, 3);
            this.cbChildTable.Name = "cbChildTable";
            this.cbChildTable.Size = new System.Drawing.Size(183, 21);
            this.cbChildTable.TabIndex = 4;
            this.cbChildTable.SelectedIndexChanged += new System.EventHandler(this.cbChild_SelectedIndexChanged);
            // 
            // btnHelp
            // 
            this.btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnHelp.Location = new System.Drawing.Point(11, 434);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 23);
            this.btnHelp.TabIndex = 9;
            this.btnHelp.Text = "Help";
            this.btnHelp.UseVisualStyleBackColor = true;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // llParentPreview
            // 
            this.llParentPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llParentPreview.AutoSize = true;
            this.llParentPreview.Location = new System.Drawing.Point(-3, 389);
            this.llParentPreview.Name = "llParentPreview";
            this.llParentPreview.Size = new System.Drawing.Size(128, 13);
            this.llParentPreview.TabIndex = 3;
            this.llParentPreview.TabStop = true;
            this.llParentPreview.Text = "Preview parent table data";
            this.llParentPreview.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llParentPreview_LinkClicked);
            // 
            // llChildPreview
            // 
            this.llChildPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llChildPreview.AutoSize = true;
            this.llChildPreview.Location = new System.Drawing.Point(-3, 389);
            this.llChildPreview.Name = "llChildPreview";
            this.llChildPreview.Size = new System.Drawing.Size(120, 13);
            this.llChildPreview.TabIndex = 8;
            this.llChildPreview.TabStop = true;
            this.llChildPreview.Text = "Preview child table data";
            this.llChildPreview.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llChildPreview_LinkClicked);
            // 
            // SplitContainer
            // 
            this.SplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SplitContainer.Location = new System.Drawing.Point(11, 12);
            this.SplitContainer.MinimumSize = new System.Drawing.Size(535, 150);
            this.SplitContainer.Name = "SplitContainer";
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.Controls.Add(this.cbParentType);
            this.SplitContainer.Panel1.Controls.Add(this.lblParentType);
            this.SplitContainer.Panel1.Controls.Add(this.lblParentTable);
            this.SplitContainer.Panel1.Controls.Add(this.lbParentPK);
            this.SplitContainer.Panel1.Controls.Add(this.lblParentPK);
            this.SplitContainer.Panel1.Controls.Add(this.cbParentTable);
            this.SplitContainer.Panel1.Controls.Add(this.llParentPreview);
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.Controls.Add(this.ChildSplitContainer);
            this.SplitContainer.Panel2.Controls.Add(this.cbChildType);
            this.SplitContainer.Panel2.Controls.Add(this.lblChildType);
            this.SplitContainer.Panel2.Controls.Add(this.lblChildTable);
            this.SplitContainer.Panel2.Controls.Add(this.cbChildTable);
            this.SplitContainer.Panel2.Controls.Add(this.llChildPreview);
            this.SplitContainer.Size = new System.Drawing.Size(538, 407);
            this.SplitContainer.SplitterDistance = 264;
            this.SplitContainer.SplitterWidth = 10;
            this.SplitContainer.TabIndex = 58;
            // 
            // cbParentType
            // 
            this.cbParentType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbParentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbParentType.FormattingEnabled = true;
            this.cbParentType.Location = new System.Drawing.Point(74, 30);
            this.cbParentType.Name = "cbParentType";
            this.cbParentType.Size = new System.Drawing.Size(188, 21);
            this.cbParentType.TabIndex = 1;
            this.cbParentType.SelectedIndexChanged += new System.EventHandler(this.cbParentType_SelectedIndexChanged);
            // 
            // lblParentType
            // 
            this.lblParentType.AutoSize = true;
            this.lblParentType.Location = new System.Drawing.Point(-3, 33);
            this.lblParentType.Name = "lblParentType";
            this.lblParentType.Size = new System.Drawing.Size(64, 13);
            this.lblParentType.TabIndex = 38;
            this.lblParentType.Text = "Table Type:";
            // 
            // lblParentTable
            // 
            this.lblParentTable.AutoSize = true;
            this.lblParentTable.Location = new System.Drawing.Point(-3, 6);
            this.lblParentTable.Name = "lblParentTable";
            this.lblParentTable.Size = new System.Drawing.Size(71, 13);
            this.lblParentTable.TabIndex = 37;
            this.lblParentTable.Text = "Parent Table:";
            // 
            // lbParentPK
            // 
            this.lbParentPK.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbParentPK.FormattingEnabled = true;
            this.lbParentPK.IntegralHeight = false;
            this.lbParentPK.Location = new System.Drawing.Point(0, 93);
            this.lbParentPK.Name = "lbParentPK";
            this.lbParentPK.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbParentPK.Size = new System.Drawing.Size(262, 294);
            this.lbParentPK.TabIndex = 2;
            // 
            // ChildSplitContainer
            // 
            this.ChildSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ChildSplitContainer.Location = new System.Drawing.Point(0, 61);
            this.ChildSplitContainer.Name = "ChildSplitContainer";
            this.ChildSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ChildSplitContainer.Panel1
            // 
            this.ChildSplitContainer.Panel1.Controls.Add(this.lbChildPK);
            this.ChildSplitContainer.Panel1.Controls.Add(this.lblChildPK);
            // 
            // ChildSplitContainer.Panel2
            // 
            this.ChildSplitContainer.Panel2.Controls.Add(this.lbChildFK);
            this.ChildSplitContainer.Panel2.Controls.Add(this.lblChildFK);
            this.ChildSplitContainer.Size = new System.Drawing.Size(264, 323);
            this.ChildSplitContainer.SplitterDistance = 165;
            this.ChildSplitContainer.TabIndex = 59;
            // 
            // lbChildPK
            // 
            this.lbChildPK.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbChildPK.FormattingEnabled = true;
            this.lbChildPK.IntegralHeight = false;
            this.lbChildPK.Location = new System.Drawing.Point(0, 32);
            this.lbChildPK.Name = "lbChildPK";
            this.lbChildPK.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbChildPK.Size = new System.Drawing.Size(264, 122);
            this.lbChildPK.TabIndex = 6;
            // 
            // lblChildPK
            // 
            this.lblChildPK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChildPK.Location = new System.Drawing.Point(-3, 0);
            this.lblChildPK.Name = "lblChildPK";
            this.lblChildPK.Size = new System.Drawing.Size(267, 26);
            this.lblChildPK.TabIndex = 44;
            this.lblChildPK.Text = "Please select the Primary Key column in the Child table that uniquely identifies " +
                "a record:";
            // 
            // lbChildFK
            // 
            this.lbChildFK.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbChildFK.FormattingEnabled = true;
            this.lbChildFK.IntegralHeight = false;
            this.lbChildFK.Location = new System.Drawing.Point(0, 35);
            this.lbChildFK.Name = "lbChildFK";
            this.lbChildFK.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbChildFK.Size = new System.Drawing.Size(264, 117);
            this.lbChildFK.TabIndex = 7;
            // 
            // lblChildFK
            // 
            this.lblChildFK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChildFK.Location = new System.Drawing.Point(-3, 3);
            this.lblChildFK.Name = "lblChildFK";
            this.lblChildFK.Size = new System.Drawing.Size(267, 26);
            this.lblChildFK.TabIndex = 36;
            this.lblChildFK.Text = "Please select the Foreign Key column in the Child table that contains a value poi" +
                "nting to a unique Parent entry:";
            // 
            // cbChildType
            // 
            this.cbChildType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbChildType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChildType.FormattingEnabled = true;
            this.cbChildType.Location = new System.Drawing.Point(81, 30);
            this.cbChildType.Name = "cbChildType";
            this.cbChildType.Size = new System.Drawing.Size(183, 21);
            this.cbChildType.TabIndex = 5;
            this.cbChildType.SelectedIndexChanged += new System.EventHandler(this.cbChildType_SelectedIndexChanged);
            // 
            // lblChildType
            // 
            this.lblChildType.AutoSize = true;
            this.lblChildType.Location = new System.Drawing.Point(-3, 33);
            this.lblChildType.Name = "lblChildType";
            this.lblChildType.Size = new System.Drawing.Size(64, 13);
            this.lblChildType.TabIndex = 40;
            this.lblChildType.Text = "Table Type:";
            // 
            // lblChildTable
            // 
            this.lblChildTable.AutoSize = true;
            this.lblChildTable.Location = new System.Drawing.Point(-3, 6);
            this.lblChildTable.Name = "lblChildTable";
            this.lblChildTable.Size = new System.Drawing.Size(63, 13);
            this.lblChildTable.TabIndex = 39;
            this.lblChildTable.Text = "Child Table:";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(474, 434);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemove.Enabled = false;
            this.btnRemove.Location = new System.Drawing.Point(92, 434);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 10;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Visible = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(11, 422);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(540, 2);
            this.lblFooterLine.TabIndex = 126;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(393, 434);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmWizard_DirectLink
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 469);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblFooterLine);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.SplitContainer);
            this.Controls.Add(this.btnHelp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(569, 285);
            this.Name = "frmWizard_DirectLink";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tables Link Editor - InteGr8 Template Wizard";
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel1.PerformLayout();
            this.SplitContainer.Panel2.ResumeLayout(false);
            this.SplitContainer.Panel2.PerformLayout();
            this.SplitContainer.ResumeLayout(false);
            this.ChildSplitContainer.Panel1.ResumeLayout(false);
            this.ChildSplitContainer.Panel2.ResumeLayout(false);
            this.ChildSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblParentPK;
        private System.Windows.Forms.ComboBox cbParentTable;
        private System.Windows.Forms.ComboBox cbChildTable;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.LinkLabel llParentPreview;
        private System.Windows.Forms.LinkLabel llChildPreview;
        private System.Windows.Forms.SplitContainer SplitContainer;
        private System.Windows.Forms.ListBox lbParentPK;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.ComboBox cbParentType;
        private System.Windows.Forms.Label lblParentType;
        private System.Windows.Forms.Label lblParentTable;
        private System.Windows.Forms.ComboBox cbChildType;
        private System.Windows.Forms.Label lblChildType;
        private System.Windows.Forms.Label lblChildTable;
        private System.Windows.Forms.SplitContainer ChildSplitContainer;
        private System.Windows.Forms.ListBox lbChildPK;
        private System.Windows.Forms.Label lblChildPK;
        private System.Windows.Forms.ListBox lbChildFK;
        private System.Windows.Forms.Label lblChildFK;
        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.Button btnCancel;
    }
}