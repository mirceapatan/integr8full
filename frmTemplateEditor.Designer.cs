﻿namespace InteGr8
{
    partial class frmTemplateEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTemplateEditor));
            this.lblDSN = new System.Windows.Forms.Label();
            this.txtDSN = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblCommandInfo = new System.Windows.Forms.Label();
            this.lblFilters = new System.Windows.Forms.Label();
            this.btnSQL = new System.Windows.Forms.Button();
            this.lblSQL = new System.Windows.Forms.Label();
            this.dgvFilters = new System.Windows.Forms.DataGridView();
            this.txtSQL = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnConnection = new System.Windows.Forms.Button();
            this.rbOrders = new System.Windows.Forms.RadioButton();
            this.lblCommand = new System.Windows.Forms.Label();
            this.rbOrderPackages = new System.Windows.Forms.RadioButton();
            this.rbOrderProducts = new System.Windows.Forms.RadioButton();
            this.rbShipments = new System.Windows.Forms.RadioButton();
            this.rbShipmentPackages = new System.Windows.Forms.RadioButton();
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.lblTemplate = new System.Windows.Forms.Label();
            this.txtTemplate = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.ParameterTemplate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParameterTable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParameterPos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParameterName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParameterType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParameterLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParameterDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilters)).BeginInit();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDSN
            // 
            this.lblDSN.AutoSize = true;
            this.lblDSN.Location = new System.Drawing.Point(12, 41);
            this.lblDSN.Name = "lblDSN";
            this.lblDSN.Size = new System.Drawing.Size(100, 13);
            this.lblDSN.TabIndex = 0;
            this.lblDSN.Text = "ODBC System DSN";
            // 
            // txtDSN
            // 
            this.txtDSN.Location = new System.Drawing.Point(118, 38);
            this.txtDSN.Name = "txtDSN";
            this.txtDSN.ReadOnly = true;
            this.txtDSN.Size = new System.Drawing.Size(175, 20);
            this.txtDSN.TabIndex = 2;
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(314, 41);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(33, 13);
            this.lblLogin.TabIndex = 2;
            this.lblLogin.Text = "Login";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(503, 41);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password";
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(383, 38);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.ReadOnly = true;
            this.txtLogin.Size = new System.Drawing.Size(114, 20);
            this.txtLogin.TabIndex = 3;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(562, 38);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.ReadOnly = true;
            this.txtPassword.Size = new System.Drawing.Size(114, 20);
            this.txtPassword.TabIndex = 4;
            // 
            // lblCommandInfo
            // 
            this.lblCommandInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCommandInfo.AutoEllipsis = true;
            this.lblCommandInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCommandInfo.Location = new System.Drawing.Point(12, 506);
            this.lblCommandInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 0);
            this.lblCommandInfo.Name = "lblCommandInfo";
            this.lblCommandInfo.Size = new System.Drawing.Size(928, 59);
            this.lblCommandInfo.TabIndex = 8;
            this.lblCommandInfo.Text = resources.GetString("lblCommandInfo.Text");
            // 
            // lblFilters
            // 
            this.lblFilters.AutoSize = true;
            this.lblFilters.Location = new System.Drawing.Point(3, 0);
            this.lblFilters.Name = "lblFilters";
            this.lblFilters.Size = new System.Drawing.Size(85, 13);
            this.lblFilters.TabIndex = 3;
            this.lblFilters.Text = "Filter Parameters";
            // 
            // btnSQL
            // 
            this.btnSQL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSQL.Enabled = false;
            this.btnSQL.Location = new System.Drawing.Point(12, 573);
            this.btnSQL.Name = "btnSQL";
            this.btnSQL.Size = new System.Drawing.Size(118, 23);
            this.btnSQL.TabIndex = 13;
            this.btnSQL.Text = "Edit SQL Statement";
            this.btnSQL.UseVisualStyleBackColor = true;
            this.btnSQL.Click += new System.EventHandler(this.btnSQL_Click);
            // 
            // lblSQL
            // 
            this.lblSQL.AutoSize = true;
            this.lblSQL.Location = new System.Drawing.Point(0, 0);
            this.lblSQL.Name = "lblSQL";
            this.lblSQL.Size = new System.Drawing.Size(79, 13);
            this.lblSQL.TabIndex = 2;
            this.lblSQL.Text = "SQL Statement";
            // 
            // dgvFilters
            // 
            this.dgvFilters.AllowUserToOrderColumns = true;
            this.dgvFilters.AllowUserToResizeRows = false;
            this.dgvFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFilters.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvFilters.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvFilters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFilters.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterTemplate,
            this.ParameterTable,
            this.ParameterPos,
            this.ParameterName,
            this.ParameterType,
            this.ParameterLength,
            this.ParameterDefault});
            this.dgvFilters.Location = new System.Drawing.Point(6, 16);
            this.dgvFilters.Margin = new System.Windows.Forms.Padding(0);
            this.dgvFilters.Name = "dgvFilters";
            this.dgvFilters.Size = new System.Drawing.Size(452, 333);
            this.dgvFilters.TabIndex = 12;
            this.dgvFilters.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilters_CellEndEdit);
            this.dgvFilters.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvFilters_DefaultValuesNeeded);
            this.dgvFilters.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvFilters_DataError);
            // 
            // txtSQL
            // 
            this.txtSQL.AcceptsReturn = true;
            this.txtSQL.AcceptsTab = true;
            this.txtSQL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSQL.Location = new System.Drawing.Point(0, 16);
            this.txtSQL.Margin = new System.Windows.Forms.Padding(0);
            this.txtSQL.Multiline = true;
            this.txtSQL.Name = "txtSQL";
            this.txtSQL.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSQL.Size = new System.Drawing.Size(466, 333);
            this.txtSQL.TabIndex = 11;
            this.txtSQL.WordWrap = false;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(858, 573);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(82, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(770, 573);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(82, 23);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnConnection
            // 
            this.btnConnection.Location = new System.Drawing.Point(695, 36);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(108, 23);
            this.btnConnection.TabIndex = 5;
            this.btnConnection.Text = "Connect";
            this.btnConnection.UseVisualStyleBackColor = true;
            this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // rbOrders
            // 
            this.rbOrders.AutoSize = true;
            this.rbOrders.Checked = true;
            this.rbOrders.Location = new System.Drawing.Point(101, 124);
            this.rbOrders.Name = "rbOrders";
            this.rbOrders.Size = new System.Drawing.Size(88, 17);
            this.rbOrders.TabIndex = 6;
            this.rbOrders.TabStop = true;
            this.rbOrders.Text = "Orders Import";
            this.rbOrders.UseVisualStyleBackColor = true;
            this.rbOrders.CheckedChanged += new System.EventHandler(this.rbCommand_CheckedChanged);
            // 
            // lblCommand
            // 
            this.lblCommand.AutoSize = true;
            this.lblCommand.Location = new System.Drawing.Point(9, 126);
            this.lblCommand.Name = "lblCommand";
            this.lblCommand.Size = new System.Drawing.Size(83, 13);
            this.lblCommand.TabIndex = 12;
            this.lblCommand.Text = "View Command:";
            // 
            // rbOrderPackages
            // 
            this.rbOrderPackages.AutoSize = true;
            this.rbOrderPackages.Location = new System.Drawing.Point(195, 124);
            this.rbOrderPackages.Name = "rbOrderPackages";
            this.rbOrderPackages.Size = new System.Drawing.Size(134, 17);
            this.rbOrderPackages.TabIndex = 7;
            this.rbOrderPackages.Text = "Order Packages Import";
            this.rbOrderPackages.UseVisualStyleBackColor = true;
            this.rbOrderPackages.CheckedChanged += new System.EventHandler(this.rbCommand_CheckedChanged);
            // 
            // rbOrderProducts
            // 
            this.rbOrderProducts.AutoSize = true;
            this.rbOrderProducts.Location = new System.Drawing.Point(335, 124);
            this.rbOrderProducts.Name = "rbOrderProducts";
            this.rbOrderProducts.Size = new System.Drawing.Size(128, 17);
            this.rbOrderProducts.TabIndex = 8;
            this.rbOrderProducts.Text = "Order Products Import";
            this.rbOrderProducts.UseVisualStyleBackColor = true;
            this.rbOrderProducts.CheckedChanged += new System.EventHandler(this.rbCommand_CheckedChanged);
            // 
            // rbShipments
            // 
            this.rbShipments.AutoSize = true;
            this.rbShipments.Location = new System.Drawing.Point(469, 124);
            this.rbShipments.Name = "rbShipments";
            this.rbShipments.Size = new System.Drawing.Size(107, 17);
            this.rbShipments.TabIndex = 9;
            this.rbShipments.Text = "Shipments Export";
            this.rbShipments.UseVisualStyleBackColor = true;
            this.rbShipments.CheckedChanged += new System.EventHandler(this.rbCommand_CheckedChanged);
            // 
            // rbShipmentPackages
            // 
            this.rbShipmentPackages.AutoSize = true;
            this.rbShipmentPackages.Location = new System.Drawing.Point(582, 124);
            this.rbShipmentPackages.Name = "rbShipmentPackages";
            this.rbShipmentPackages.Size = new System.Drawing.Size(153, 17);
            this.rbShipmentPackages.TabIndex = 10;
            this.rbShipmentPackages.Text = "Shipment Packages Export";
            this.rbShipmentPackages.UseVisualStyleBackColor = true;
            this.rbShipmentPackages.CheckedChanged += new System.EventHandler(this.rbCommand_CheckedChanged);
            // 
            // SplitContainer
            // 
            this.SplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SplitContainer.Enabled = false;
            this.SplitContainer.Location = new System.Drawing.Point(12, 155);
            this.SplitContainer.Margin = new System.Windows.Forms.Padding(0);
            this.SplitContainer.Name = "SplitContainer";
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.Controls.Add(this.lblSQL);
            this.SplitContainer.Panel1.Controls.Add(this.txtSQL);
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.Controls.Add(this.lblFilters);
            this.SplitContainer.Panel2.Controls.Add(this.dgvFilters);
            this.SplitContainer.Size = new System.Drawing.Size(928, 349);
            this.SplitContainer.SplitterDistance = 466;
            this.SplitContainer.TabIndex = 17;
            // 
            // lblTemplate
            // 
            this.lblTemplate.AutoSize = true;
            this.lblTemplate.Location = new System.Drawing.Point(12, 15);
            this.lblTemplate.Name = "lblTemplate";
            this.lblTemplate.Size = new System.Drawing.Size(82, 13);
            this.lblTemplate.TabIndex = 18;
            this.lblTemplate.Text = "Template Name";
            // 
            // txtTemplate
            // 
            this.txtTemplate.Location = new System.Drawing.Point(118, 12);
            this.txtTemplate.Name = "txtTemplate";
            this.txtTemplate.Size = new System.Drawing.Size(175, 20);
            this.txtTemplate.TabIndex = 0;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(314, 13);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 20;
            this.lblDescription.Text = "Description";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(383, 10);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(293, 20);
            this.txtDescription.TabIndex = 1;
            // 
            // lblInfo
            // 
            this.lblInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInfo.AutoEllipsis = true;
            this.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInfo.Location = new System.Drawing.Point(12, 67);
            this.lblInfo.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(928, 49);
            this.lblInfo.TabIndex = 21;
            this.lblInfo.Text = resources.GetString("lblInfo.Text");
            // 
            // ParameterTemplate
            // 
            this.ParameterTemplate.DataPropertyName = "Template";
            this.ParameterTemplate.HeaderText = "Template";
            this.ParameterTemplate.Name = "ParameterTemplate";
            this.ParameterTemplate.Visible = false;
            // 
            // ParameterTable
            // 
            this.ParameterTable.DataPropertyName = "Table";
            this.ParameterTable.HeaderText = "Command";
            this.ParameterTable.Name = "ParameterTable";
            this.ParameterTable.Visible = false;
            // 
            // ParameterPos
            // 
            this.ParameterPos.DataPropertyName = "Pos";
            this.ParameterPos.HeaderText = "Position";
            this.ParameterPos.Name = "ParameterPos";
            this.ParameterPos.Visible = false;
            // 
            // ParameterName
            // 
            this.ParameterName.DataPropertyName = "Name";
            this.ParameterName.HeaderText = "Name";
            this.ParameterName.MinimumWidth = 25;
            this.ParameterName.Name = "ParameterName";
            // 
            // ParameterType
            // 
            this.ParameterType.DataPropertyName = "Type";
            this.ParameterType.HeaderText = "Type";
            this.ParameterType.MinimumWidth = 25;
            this.ParameterType.Name = "ParameterType";
            this.ParameterType.Width = 75;
            // 
            // ParameterLength
            // 
            this.ParameterLength.DataPropertyName = "Length";
            this.ParameterLength.HeaderText = "Length";
            this.ParameterLength.MinimumWidth = 25;
            this.ParameterLength.Name = "ParameterLength";
            this.ParameterLength.Width = 50;
            // 
            // ParameterDefault
            // 
            this.ParameterDefault.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ParameterDefault.DataPropertyName = "Default";
            this.ParameterDefault.HeaderText = "Default";
            this.ParameterDefault.MinimumWidth = 50;
            this.ParameterDefault.Name = "ParameterDefault";
            // 
            // frmTemplateEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 608);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.txtTemplate);
            this.Controls.Add(this.lblTemplate);
            this.Controls.Add(this.SplitContainer);
            this.Controls.Add(this.lblCommandInfo);
            this.Controls.Add(this.rbShipmentPackages);
            this.Controls.Add(this.rbShipments);
            this.Controls.Add(this.btnSQL);
            this.Controls.Add(this.rbOrderProducts);
            this.Controls.Add(this.rbOrderPackages);
            this.Controls.Add(this.lblCommand);
            this.Controls.Add(this.rbOrders);
            this.Controls.Add(this.btnConnection);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblLogin);
            this.Controls.Add(this.txtDSN);
            this.Controls.Add(this.lblDSN);
            this.MinimumSize = new System.Drawing.Size(823, 400);
            this.Name = "frmTemplateEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Editor - New Template";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTemplateEditor_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilters)).EndInit();
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel1.PerformLayout();
            this.SplitContainer.Panel2.ResumeLayout(false);
            this.SplitContainer.Panel2.PerformLayout();
            this.SplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDSN;
        private System.Windows.Forms.TextBox txtDSN;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnSQL;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtSQL;
        private System.Windows.Forms.Label lblFilters;
        private System.Windows.Forms.Label lblSQL;
        private System.Windows.Forms.DataGridView dgvFilters;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Label lblCommandInfo;
        private System.Windows.Forms.RadioButton rbOrders;
        private System.Windows.Forms.Label lblCommand;
        private System.Windows.Forms.RadioButton rbOrderPackages;
        private System.Windows.Forms.RadioButton rbOrderProducts;
        private System.Windows.Forms.RadioButton rbShipments;
        private System.Windows.Forms.RadioButton rbShipmentPackages;
        private System.Windows.Forms.SplitContainer SplitContainer;
        private System.Windows.Forms.Label lblTemplate;
        private System.Windows.Forms.TextBox txtTemplate;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParameterTemplate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParameterTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParameterPos;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParameterName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParameterType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParameterLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParameterDefault;
    }
}