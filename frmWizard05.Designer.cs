﻿namespace InteGr8
{
    partial class frmWizard05
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard05));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lblHeaderLine = new System.Windows.Forms.Label();
            this.llAdvanced = new System.Windows.Forms.LinkLabel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.lblHeaderIcon = new System.Windows.Forms.Label();
            this.lblHeaderInfo = new System.Windows.Forms.Label();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.btnPreview = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblMappingLine = new System.Windows.Forms.Label();
            this.cbShipDate = new System.Windows.Forms.ComboBox();
            this.lblShipDate = new System.Windows.Forms.Label();
            this.lblMapping = new System.Windows.Forms.Label();
            this.cbGeneralShipmentsPrimaryKey = new System.Windows.Forms.ComboBox();
            this.lblGeneralShipmentsPrimaryKey = new System.Windows.Forms.Label();
            this.cbGeneralShipmentsTable = new System.Windows.Forms.ComboBox();
            this.lblGeneralShipmentsTable = new System.Windows.Forms.Label();
            this.cbCarrier = new System.Windows.Forms.ComboBox();
            this.lblCarrier = new System.Windows.Forms.Label();
            this.cbService = new System.Windows.Forms.ComboBox();
            this.lblService = new System.Windows.Forms.Label();
            this.cbPackaging = new System.Windows.Forms.ComboBox();
            this.lblPackaging = new System.Windows.Forms.Label();
            this.cbReference = new System.Windows.Forms.ComboBox();
            this.lblReference = new System.Windows.Forms.Label();
            this.lblShipDateMessage = new System.Windows.Forms.Label();
            this.lblCarrierMessage = new System.Windows.Forms.LinkLabel();
            this.lblServiceMessage = new System.Windows.Forms.LinkLabel();
            this.lblPackagingMessage = new System.Windows.Forms.LinkLabel();
            this.lblReferenceMessage = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPKMessage = new System.Windows.Forms.Label();
            this.pnlHeader.SuspendLayout();
            this.pnlFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.lblHeaderLine);
            this.pnlHeader.Controls.Add(this.llAdvanced);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.lblHeaderIcon);
            this.pnlHeader.Controls.Add(this.lblHeaderInfo);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(640, 94);
            this.pnlHeader.TabIndex = 115;
            // 
            // lblHeaderLine
            // 
            this.lblHeaderLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderLine.BackColor = System.Drawing.SystemColors.Control;
            this.lblHeaderLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeaderLine.Location = new System.Drawing.Point(0, 92);
            this.lblHeaderLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderLine.Name = "lblHeaderLine";
            this.lblHeaderLine.Size = new System.Drawing.Size(640, 2);
            this.lblHeaderLine.TabIndex = 116;
            // 
            // llAdvanced
            // 
            this.llAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdvanced.Image = global::InteGr8.Properties.Resources.wrench;
            this.llAdvanced.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llAdvanced.LinkArea = new System.Windows.Forms.LinkArea(6, 13);
            this.llAdvanced.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdvanced.Location = new System.Drawing.Point(536, 0);
            this.llAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.llAdvanced.Name = "llAdvanced";
            this.llAdvanced.Size = new System.Drawing.Size(104, 24);
            this.llAdvanced.TabIndex = 119;
            this.llAdvanced.TabStop = true;
            this.llAdvanced.Text = "      advanced mode";
            this.llAdvanced.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llAdvanced.UseCompatibleTextRendering = true;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(535, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Order - Shipment Level Information";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeaderIcon
            // 
            this.lblHeaderIcon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeaderIcon.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaderIcon.Image = global::InteGr8.Properties.Resources.data_next;
            this.lblHeaderIcon.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeaderIcon.Location = new System.Drawing.Point(0, 24);
            this.lblHeaderIcon.Name = "lblHeaderIcon";
            this.lblHeaderIcon.Padding = new System.Windows.Forms.Padding(5);
            this.lblHeaderIcon.Size = new System.Drawing.Size(64, 68);
            this.lblHeaderIcon.TabIndex = 115;
            // 
            // lblHeaderInfo
            // 
            this.lblHeaderInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderInfo.AutoEllipsis = true;
            this.lblHeaderInfo.Location = new System.Drawing.Point(68, 24);
            this.lblHeaderInfo.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderInfo.Name = "lblHeaderInfo";
            this.lblHeaderInfo.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.lblHeaderInfo.Size = new System.Drawing.Size(572, 68);
            this.lblHeaderInfo.TabIndex = 114;
            this.lblHeaderInfo.Text = resources.GetString("lblHeaderInfo.Text");
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.lblFooterLine);
            this.pnlFooter.Controls.Add(this.btnPreview);
            this.pnlFooter.Controls.Add(this.btnPrev);
            this.pnlFooter.Controls.Add(this.btnNext);
            this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFooter.Location = new System.Drawing.Point(0, 355);
            this.pnlFooter.Margin = new System.Windows.Forms.Padding(0);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Padding = new System.Windows.Forms.Padding(9);
            this.pnlFooter.Size = new System.Drawing.Size(640, 53);
            this.pnlFooter.TabIndex = 120;
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFooterLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(12, 5);
            this.lblFooterLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(616, 2);
            this.lblFooterLine.TabIndex = 122;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(12, 18);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(90, 23);
            this.btnPreview.TabIndex = 9;
            this.btnPreview.Text = "Preview Data";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnPrev.Location = new System.Drawing.Point(472, 18);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 10;
            this.btnPrev.Text = "< Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(553, 18);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 11;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblMappingLine
            // 
            this.lblMappingLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMappingLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblMappingLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMappingLine.Location = new System.Drawing.Point(12, 186);
            this.lblMappingLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblMappingLine.Name = "lblMappingLine";
            this.lblMappingLine.Size = new System.Drawing.Size(616, 2);
            this.lblMappingLine.TabIndex = 131;
            // 
            // cbShipDate
            // 
            this.cbShipDate.FormattingEnabled = true;
            this.cbShipDate.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing",
            "2nd day of processing",
            "3rd day of processing",
            "4th day of processing",
            "5th day of processing",
            "6th day of processing",
            "7th day of processing",
            "8th day of processing",
            "9th day of processing"});
            this.cbShipDate.Location = new System.Drawing.Point(111, 198);
            this.cbShipDate.Name = "cbShipDate";
            this.cbShipDate.Size = new System.Drawing.Size(174, 21);
            this.cbShipDate.TabIndex = 4;
            // 
            // lblShipDate
            // 
            this.lblShipDate.AutoSize = true;
            this.lblShipDate.Location = new System.Drawing.Point(12, 201);
            this.lblShipDate.Name = "lblShipDate";
            this.lblShipDate.Size = new System.Drawing.Size(55, 13);
            this.lblShipDate.TabIndex = 129;
            this.lblShipDate.Text = "Ship date:";
            // 
            // lblMapping
            // 
            this.lblMapping.AutoSize = true;
            this.lblMapping.Location = new System.Drawing.Point(12, 168);
            this.lblMapping.Name = "lblMapping";
            this.lblMapping.Size = new System.Drawing.Size(559, 13);
            this.lblMapping.TabIndex = 128;
            this.lblMapping.Text = "Please map the following 2Ship order shipment variables to your Orders Shipments " +
                "columns, or choose default values:";
            // 
            // cbGeneralShipmentsPrimaryKey
            // 
            this.cbGeneralShipmentsPrimaryKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGeneralShipmentsPrimaryKey.FormattingEnabled = true;
            this.cbGeneralShipmentsPrimaryKey.Location = new System.Drawing.Point(211, 132);
            this.cbGeneralShipmentsPrimaryKey.Name = "cbGeneralShipmentsPrimaryKey";
            this.cbGeneralShipmentsPrimaryKey.Size = new System.Drawing.Size(174, 21);
            this.cbGeneralShipmentsPrimaryKey.TabIndex = 2;
            // 
            // lblGeneralShipmentsPrimaryKey
            // 
            this.lblGeneralShipmentsPrimaryKey.AutoSize = true;
            this.lblGeneralShipmentsPrimaryKey.Location = new System.Drawing.Point(12, 135);
            this.lblGeneralShipmentsPrimaryKey.Name = "lblGeneralShipmentsPrimaryKey";
            this.lblGeneralShipmentsPrimaryKey.Size = new System.Drawing.Size(164, 13);
            this.lblGeneralShipmentsPrimaryKey.TabIndex = 126;
            this.lblGeneralShipmentsPrimaryKey.Text = "Order Shipments Order # column:";
            // 
            // cbGeneralShipmentsTable
            // 
            this.cbGeneralShipmentsTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGeneralShipmentsTable.FormattingEnabled = true;
            this.cbGeneralShipmentsTable.Location = new System.Drawing.Point(211, 105);
            this.cbGeneralShipmentsTable.Name = "cbGeneralShipmentsTable";
            this.cbGeneralShipmentsTable.Size = new System.Drawing.Size(174, 21);
            this.cbGeneralShipmentsTable.TabIndex = 1;
            this.cbGeneralShipmentsTable.SelectedIndexChanged += new System.EventHandler(this.cbGeneralShipmentsTable_SelectedIndexChanged);
            // 
            // lblGeneralShipmentsTable
            // 
            this.lblGeneralShipmentsTable.AutoSize = true;
            this.lblGeneralShipmentsTable.Location = new System.Drawing.Point(12, 108);
            this.lblGeneralShipmentsTable.Name = "lblGeneralShipmentsTable";
            this.lblGeneralShipmentsTable.Size = new System.Drawing.Size(114, 13);
            this.lblGeneralShipmentsTable.TabIndex = 125;
            this.lblGeneralShipmentsTable.Text = "Order Shipments table:";
            // 
            // cbCarrier
            // 
            this.cbCarrier.FormattingEnabled = true;
            this.cbCarrier.Items.AddRange(new object[] {
            "(none)",
            "Cheapest",
            "Fastest",
            "FedEx",
            "Purolator",
            "DHL",
            "CA Post"});
            this.cbCarrier.Location = new System.Drawing.Point(111, 225);
            this.cbCarrier.Name = "cbCarrier";
            this.cbCarrier.Size = new System.Drawing.Size(174, 21);
            this.cbCarrier.TabIndex = 5;
            // 
            // lblCarrier
            // 
            this.lblCarrier.AutoSize = true;
            this.lblCarrier.Location = new System.Drawing.Point(12, 228);
            this.lblCarrier.Name = "lblCarrier";
            this.lblCarrier.Size = new System.Drawing.Size(67, 13);
            this.lblCarrier.TabIndex = 137;
            this.lblCarrier.Text = "Carrier code:";
            // 
            // cbService
            // 
            this.cbService.FormattingEnabled = true;
            this.cbService.Items.AddRange(new object[] {
            "(none)",
            "Cheapest",
            "Fastest"});
            this.cbService.Location = new System.Drawing.Point(111, 252);
            this.cbService.Name = "cbService";
            this.cbService.Size = new System.Drawing.Size(174, 21);
            this.cbService.TabIndex = 6;
            // 
            // lblService
            // 
            this.lblService.AutoSize = true;
            this.lblService.Location = new System.Drawing.Point(12, 255);
            this.lblService.Name = "lblService";
            this.lblService.Size = new System.Drawing.Size(73, 13);
            this.lblService.TabIndex = 139;
            this.lblService.Text = "Service code:";
            // 
            // cbPackaging
            // 
            this.cbPackaging.FormattingEnabled = true;
            this.cbPackaging.Items.AddRange(new object[] {
            "Customer Packaging",
            "Envelope (Letter)",
            "Small Pack"});
            this.cbPackaging.Location = new System.Drawing.Point(111, 279);
            this.cbPackaging.Name = "cbPackaging";
            this.cbPackaging.Size = new System.Drawing.Size(174, 21);
            this.cbPackaging.TabIndex = 7;
            // 
            // lblPackaging
            // 
            this.lblPackaging.AutoSize = true;
            this.lblPackaging.Location = new System.Drawing.Point(12, 282);
            this.lblPackaging.Name = "lblPackaging";
            this.lblPackaging.Size = new System.Drawing.Size(88, 13);
            this.lblPackaging.TabIndex = 141;
            this.lblPackaging.Text = "Packaging code:";
            // 
            // cbReference
            // 
            this.cbReference.FormattingEnabled = true;
            this.cbReference.Location = new System.Drawing.Point(111, 306);
            this.cbReference.Name = "cbReference";
            this.cbReference.Size = new System.Drawing.Size(174, 21);
            this.cbReference.TabIndex = 8;
            // 
            // lblReference
            // 
            this.lblReference.AutoSize = true;
            this.lblReference.Location = new System.Drawing.Point(12, 309);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(80, 13);
            this.lblReference.TabIndex = 143;
            this.lblReference.Text = "Reference info:";
            // 
            // lblShipDateMessage
            // 
            this.lblShipDateMessage.AutoSize = true;
            this.lblShipDateMessage.Location = new System.Drawing.Point(291, 201);
            this.lblShipDateMessage.Name = "lblShipDateMessage";
            this.lblShipDateMessage.Size = new System.Drawing.Size(294, 13);
            this.lblShipDateMessage.TabIndex = 145;
            this.lblShipDateMessage.Text = "format: dd/mm/yyyy, from current date up to the next 10 days";
            // 
            // lblCarrierMessage
            // 
            this.lblCarrierMessage.AutoSize = true;
            this.lblCarrierMessage.LinkArea = new System.Windows.Forms.LinkArea(26, 4);
            this.lblCarrierMessage.Location = new System.Drawing.Point(291, 228);
            this.lblCarrierMessage.Name = "lblCarrierMessage";
            this.lblCarrierMessage.Size = new System.Drawing.Size(220, 17);
            this.lblCarrierMessage.TabIndex = 148;
            this.lblCarrierMessage.TabStop = true;
            this.lblCarrierMessage.Text = "two digit numbers - click here to see the list";
            this.lblCarrierMessage.UseCompatibleTextRendering = true;
            // 
            // lblServiceMessage
            // 
            this.lblServiceMessage.AutoSize = true;
            this.lblServiceMessage.LinkArea = new System.Windows.Forms.LinkArea(26, 4);
            this.lblServiceMessage.Location = new System.Drawing.Point(291, 255);
            this.lblServiceMessage.Name = "lblServiceMessage";
            this.lblServiceMessage.Size = new System.Drawing.Size(299, 17);
            this.lblServiceMessage.TabIndex = 149;
            this.lblServiceMessage.TabStop = true;
            this.lblServiceMessage.Text = "two digit numbers - click here to see the list for each carrier";
            this.lblServiceMessage.UseCompatibleTextRendering = true;
            // 
            // lblPackagingMessage
            // 
            this.lblPackagingMessage.AutoSize = true;
            this.lblPackagingMessage.LinkArea = new System.Windows.Forms.LinkArea(6, 4);
            this.lblPackagingMessage.Location = new System.Drawing.Point(291, 282);
            this.lblPackagingMessage.Name = "lblPackagingMessage";
            this.lblPackagingMessage.Size = new System.Drawing.Size(121, 17);
            this.lblPackagingMessage.TabIndex = 150;
            this.lblPackagingMessage.TabStop = true;
            this.lblPackagingMessage.Text = "click here to see the list";
            this.lblPackagingMessage.UseCompatibleTextRendering = true;
            // 
            // lblReferenceMessage
            // 
            this.lblReferenceMessage.AutoSize = true;
            this.lblReferenceMessage.Location = new System.Drawing.Point(291, 309);
            this.lblReferenceMessage.Name = "lblReferenceMessage";
            this.lblReferenceMessage.Size = new System.Drawing.Size(264, 13);
            this.lblReferenceMessage.TabIndex = 183;
            this.lblReferenceMessage.Text = "optional, not used by 2Ship, but can be used in reports";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 339);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(491, 13);
            this.label1.TabIndex = 328;
            this.label1.Text = "Note: You will be able to add more variable mappings later, in the Summary Review" +
                " page of this wizard.";
            // 
            // lblPKMessage
            // 
            this.lblPKMessage.AutoSize = true;
            this.lblPKMessage.Location = new System.Drawing.Point(391, 135);
            this.lblPKMessage.Name = "lblPKMessage";
            this.lblPKMessage.Size = new System.Drawing.Size(223, 13);
            this.lblPKMessage.TabIndex = 329;
            this.lblPKMessage.Text = "this should be also the primary key in the table";
            // 
            // frmWizard05
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(640, 408);
            this.Controls.Add(this.lblPKMessage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblReferenceMessage);
            this.Controls.Add(this.lblPackagingMessage);
            this.Controls.Add(this.lblServiceMessage);
            this.Controls.Add(this.lblCarrierMessage);
            this.Controls.Add(this.lblShipDateMessage);
            this.Controls.Add(this.cbReference);
            this.Controls.Add(this.lblReference);
            this.Controls.Add(this.cbPackaging);
            this.Controls.Add(this.lblPackaging);
            this.Controls.Add(this.cbService);
            this.Controls.Add(this.lblService);
            this.Controls.Add(this.cbCarrier);
            this.Controls.Add(this.lblCarrier);
            this.Controls.Add(this.lblMappingLine);
            this.Controls.Add(this.cbShipDate);
            this.Controls.Add(this.lblShipDate);
            this.Controls.Add(this.lblMapping);
            this.Controls.Add(this.cbGeneralShipmentsPrimaryKey);
            this.Controls.Add(this.lblGeneralShipmentsPrimaryKey);
            this.Controls.Add(this.cbGeneralShipmentsTable);
            this.Controls.Add(this.lblGeneralShipmentsTable);
            this.Controls.Add(this.pnlFooter);
            this.Controls.Add(this.pnlHeader);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(627, 435);
            this.Name = "frmWizard05";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 5 of 11";
            this.Load += new System.EventHandler(this.frmWizard05_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard05_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWizard05_FormClosing);
            this.pnlHeader.ResumeLayout(false);
            this.pnlFooter.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lblHeaderLine;
        private System.Windows.Forms.LinkLabel llAdvanced;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label lblHeaderIcon;
        private System.Windows.Forms.Label lblHeaderInfo;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblMappingLine;
        private System.Windows.Forms.ComboBox cbShipDate;
        private System.Windows.Forms.Label lblShipDate;
        private System.Windows.Forms.Label lblMapping;
        private System.Windows.Forms.ComboBox cbGeneralShipmentsPrimaryKey;
        private System.Windows.Forms.Label lblGeneralShipmentsPrimaryKey;
        private System.Windows.Forms.ComboBox cbGeneralShipmentsTable;
        private System.Windows.Forms.Label lblGeneralShipmentsTable;
        private System.Windows.Forms.ComboBox cbCarrier;
        private System.Windows.Forms.Label lblCarrier;
        private System.Windows.Forms.ComboBox cbService;
        private System.Windows.Forms.Label lblService;
        private System.Windows.Forms.ComboBox cbPackaging;
        private System.Windows.Forms.Label lblPackaging;
        private System.Windows.Forms.ComboBox cbReference;
        private System.Windows.Forms.Label lblReference;
        private System.Windows.Forms.Label lblShipDateMessage;
        private System.Windows.Forms.LinkLabel lblCarrierMessage;
        private System.Windows.Forms.LinkLabel lblServiceMessage;
        private System.Windows.Forms.LinkLabel lblPackagingMessage;
        private System.Windows.Forms.Label lblReferenceMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPKMessage;
    }
}