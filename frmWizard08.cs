﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard08 : Form
    {
        private bool custom_binding = false;
        private FieldLink link = null;

        public frmWizard08()
        {
            InitializeComponent();
        }

        private void frmWizard08_Load(object sender, EventArgs e)
        {
            // populate the Products table combobox with the ODBC catalog tables
            cbShipmentsTable.Items.Add("(none)");
            using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
            {
                Conn.Open();
                foreach (DataRow row in Conn.GetSchema("Tables").Rows)
                    cbShipmentsTable.Items.Add(row["TABLE_NAME"]);
            }

            // check that the link is OrdersOneToOne type and each field have the same link
            foreach (FieldBinding binding in InteGr8_Main.template.Bindings)
                if (binding.Field.FieldCategory == Data.FieldsTableDataTable.FieldCategory.Shipments && binding.Link != null)
                    if (link == null) link = binding.Link;
                    else if (!binding.Link.Equals(link)) custom_binding = true;
            if (link != null && link.Type != FieldLink.LinkType.Orders && link.Type != FieldLink.LinkType.OrdersOneToOne) custom_binding = true;
            if (custom_binding)
            {
                foreach (Control control in this.Controls)
                    if (control is ComboBox) control.Enabled = false;
                MessageBox.Show(this, "Shipments configuration was customized and cannot be edited in this page. You will be able to edit in the summary page. Please click Next to skip this page.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (link == null) cbShipmentsTable.SelectedIndex = 0;
            else
            {
                int index = cbShipmentsTable.FindStringExact(link.FieldTable);
                if (index == -1)
                {
                    cbShipmentsTable.SelectedIndex = 0;
                    MessageBox.Show(this, "Shipments table '" + link.FieldTable + "' not found in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else cbShipmentsTable.SelectedIndex = index;
                if (link.Type != FieldLink.LinkType.Orders && link.FieldTablePK != null)
                {
                    cbShipmentsOrderNumberColumn.SelectedIndex = cbShipmentsOrderNumberColumn.FindStringExact(link.FieldTablePK);
                    if (cbShipmentsOrderNumberColumn.SelectedIndex == -1)
                    {
                        MessageBox.Show(this, "Shipments table primary key column '" + link.FieldTablePK + "' not found in the Shipments table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                if (InteGr8_Main.template.Status != null)
                {
                    cbShipmentsStatusColumn.SelectedIndex = cbShipmentsStatusColumn.FindStringExact(InteGr8_Main.template.Status);
                    if (cbShipmentsStatusColumn.SelectedIndex == -1)
                    {
                        MessageBox.Show(this, "Shipments table Status column '" + InteGr8_Main.template.Status + "' not found in the Shipments table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
            if (InteGr8_Main.template.Find("Shipment Tracking Number") != null) cbShipmentNumber.Text = InteGr8_Main.template.Find("Shipment Tracking Number").Value;
            if (InteGr8_Main.template.Find("Label URL") != null) cbLabelURL.Text = InteGr8_Main.template.Find("Label URL").Value;
            if (InteGr8_Main.template.Find("Commercial Invoice URL") != null) cbCommercialInvoiceURL.Text = InteGr8_Main.template.Find("Commercial Invoice URL").Value;
            if (InteGr8_Main.template.Find("Client Final Charge") != null) cbPrice.Text = InteGr8_Main.template.Find("Client Final Charge").Value;
            if (InteGr8_Main.template.Find("Charges Currency") != null) cbCurrency.Text = InteGr8_Main.template.Find("Charges Currency").Value;
            if (InteGr8_Main.template.Find("Delivery Date") != null) cbDelivery.Text = InteGr8_Main.template.Find("Delivery Date").Value;
            if (InteGr8_Main.template.Find("Billed Weight") != null) cbBilledWeight.Text = InteGr8_Main.template.Find("Billed Weight").Value;
            if (InteGr8_Main.template.Find("Rate Zone") != null) cbRateZone.Text = InteGr8_Main.template.Find("Rate Zone").Value;
            if (InteGr8_Main.template.Find("Error Code") != null) cbErrorCode.Text = InteGr8_Main.template.Find("Error Code").Value;
            if (InteGr8_Main.template.Find("Error Message") != null) cbErrorMessage.Text = InteGr8_Main.template.Find("Error Message").Value;
        }

        private void cbShipmentsTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // clear all column comboboxes
            cbShipmentsOrderNumberColumn.Items.Clear();
            cbShipmentsStatusColumn.Items.Clear();
            cbShipmentNumber.Items.Clear();
            cbLabelURL.Items.Clear();
            cbCommercialInvoiceURL.Items.Clear();
            cbPrice.Items.Clear();
            cbCurrency.Items.Clear();
            cbDelivery.Items.Clear();
            cbBilledWeight.Items.Clear();
            cbRateZone.Items.Clear();
            cbErrorCode.Items.Clear();
            cbErrorMessage.Items.Clear();

            cbShipmentsOrderNumberColumn.SelectedIndex = -1;
            cbShipmentsStatusColumn.SelectedIndex = -1;

            // read the Packages table columns and populate all the packages comboboxes
            if (cbShipmentsTable.SelectedIndex > 0)
            {
                string[] columns = null;
                using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
                {
                    Conn.Open();
                    using (OdbcDataReader dr = new OdbcCommand("Select Top 0 * From " + cbShipmentsTable.Text, Conn).ExecuteReader(CommandBehavior.SingleRow))
                    {
                        columns = new string[dr.FieldCount];
                        for (int k = 0; k < dr.FieldCount; k++)
                            columns[k] = "[" + dr.GetName(k) + "]";
                    }
                }

                if (columns != null)
                {
                    cbShipmentsOrderNumberColumn.Items.AddRange(columns);
                    cbShipmentsStatusColumn.Items.AddRange(columns);
                    cbShipmentNumber.Items.AddRange(columns);
                    cbLabelURL.Items.AddRange(columns);
                    cbCommercialInvoiceURL.Items.AddRange(columns);
                    cbPrice.Items.AddRange(columns);
                    cbCurrency.Items.AddRange(columns);
                    cbDelivery.Items.AddRange(columns);
                    cbBilledWeight.Items.AddRange(columns);
                    cbRateZone.Items.AddRange(columns);
                    cbErrorCode.Items.AddRange(columns);
                    cbErrorMessage.Items.AddRange(columns);
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            // validate
            // ...

            if (!custom_binding)
            {
                if (cbShipmentsTable.SelectedIndex == 0) link = null;
                else if (cbShipmentsTable.Text.Equals(InteGr8_Main.template.Orders))
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.Orders;
                    link.FieldTable = cbShipmentsTable.Text;
                }
                else
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.OrdersOneToOne;
                    link.FieldTable = cbShipmentsTable.Text;
                    link.FieldTablePK = cbShipmentsOrderNumberColumn.Text;
                    InteGr8_Main.template.Status = cbShipmentsStatusColumn.Text;
                }

                InteGr8_Main.template.UpdateBinding("Shipment Tracking Number", cbShipmentNumber.FindStringExact(cbShipmentNumber.Text) == -1, cbShipmentNumber.Text.Trim().Equals("") ? null : cbShipmentNumber.Text, link);
                InteGr8_Main.template.UpdateBinding("Label URL", cbLabelURL.FindStringExact(cbLabelURL.Text) == -1, cbLabelURL.Text.Trim().Equals("") ? null : cbLabelURL.Text, link);
                InteGr8_Main.template.UpdateBinding("Commercial Invoice URL", cbCommercialInvoiceURL.FindStringExact(cbCommercialInvoiceURL.Text) == -1, cbCommercialInvoiceURL.Text.Trim().Equals("") ? null : cbCommercialInvoiceURL.Text, link);
                InteGr8_Main.template.UpdateBinding("Client Final Charge", cbPrice.FindStringExact(cbPrice.Text) == -1, cbPrice.Text.Trim().Equals("") ? null : cbPrice.Text, link);
                InteGr8_Main.template.UpdateBinding("Charges Currency", cbCurrency.FindStringExact(cbCurrency.Text) == -1, cbCurrency.Text.Trim().Equals("") ? null : cbCurrency.Text, link);
                InteGr8_Main.template.UpdateBinding("Delivery Date", cbDelivery.FindStringExact(cbDelivery.Text) == -1, cbDelivery.Text.Trim().Equals("") ? null : cbDelivery.Text, link);
                InteGr8_Main.template.UpdateBinding("Billed Weight", cbBilledWeight.FindStringExact(cbBilledWeight.Text) == -1, cbBilledWeight.Text.Trim().Equals("") ? null : cbBilledWeight.Text, link);
                InteGr8_Main.template.UpdateBinding("Rate Zone", cbRateZone.FindStringExact(cbRateZone.Text) == -1, cbRateZone.Text.Trim().Equals("") ? null : cbRateZone.Text, link);
                InteGr8_Main.template.UpdateBinding("Error Code", cbErrorCode.FindStringExact(cbErrorCode.Text) == -1, cbErrorCode.Text.Trim().Equals("") ? null : cbErrorCode.Text, link);
                InteGr8_Main.template.UpdateBinding("Error Message", cbErrorMessage.FindStringExact(cbErrorMessage.Text) == -1, cbErrorMessage.Text.Trim().Equals("") ? null : cbErrorMessage.Text, link);
            }
            DialogResult = DialogResult.OK;
        }

        private void frmWizard08_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK) return;
            if (DialogResult == DialogResult.No)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            DialogResult = DialogResult.Abort;
        }

        private void frmWizard08_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_08");
            e.Cancel = true;
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (cbShipmentsTable.SelectedIndex <= 0 || cbShipmentsTable.Text.Trim().Equals("")) return;
            new frmWizard_DataPreview("Shipments", InteGr8_Main.template.Connection, "Select Top 100 * From " + cbShipmentsTable.Text).ShowDialog(this);
        }
    }
}
