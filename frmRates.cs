﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmRates : Form
    {
        private readonly Data data;
        private readonly Data.OrdersTableRow order;

        public string selected_carrier = null;
        public string selected_service = null;

        public frmRates(string order)
        {
            if (InteGr8_Main.data == null || String.IsNullOrEmpty(order)) throw new ArgumentException();
            data = InteGr8_Main.data;
            this.order = data.OrdersTable.FindBy_Order__(order);
            if (this.order == null) throw new ArgumentException();
            InitializeComponent();
        }

        private void frmRates_Load(object sender, EventArgs e)
        {
            // load all carriers from the dataset into the tabs
            Tabs.TabPages.Clear();
            Tabs.TabPages.Add("ALL", "ALL");
            foreach (Data.CarriersRow carrier in data.Carriers)
                Tabs.TabPages.Add(carrier.Carrier_Code, carrier.Name);
            //set the selected tab as the first one, which shows all carriers and services
            Tabs.SelectedIndex = 0;
            //remove empty tabs from tab controller
            removeEmptyTabs();
            
            Tabs_SelectedIndexChanged(null, null);
         }

        private void removeEmptyTabs()
        {
            int lines = data.RepliesTable.Select("[Order #] = '" + order._Order__ + "' And Field = 158").Length;
            System.Collections.ArrayList lTabs = new System.Collections.ArrayList();
            foreach (TabPage t in Tabs.TabPages)
            {
                if (!t.Name.Equals("ALL"))
                {
                    int servNr = 0;
                    for (int i = 0; i < lines; i++)
                    {
                        if (t.Name.Equals(strReply(158, i)) )
                            //if (!String.IsNullOrEmpty(strReply(159, i)) || !String.IsNullOrEmpty(strReply(599, i)))
                            if (!String.IsNullOrEmpty(strReply(159, i)) && String.IsNullOrEmpty(strReply(599, i)))
                                servNr++;
                        //if LTL remove Puro Express, if small package remove Puro Freight
                        if (t.Name.Equals("Purolator Freight") && isSmallPackage(order))
                        {
                            servNr++;
                        }
                        if (t.Name.Equals("Purolator") && !isSmallPackage(order))
                        {
                            servNr++;
                        }
                    }
                    if (servNr == 0)
                        lTabs.Add(t.Name.ToString());
                }
            }
            foreach (string service in lTabs)
            {
                Tabs.TabPages.RemoveByKey(service);
            }
        }

        private bool isSmallPackage(Data.OrdersTableRow order)
        {
            bool isSmallPack = true;
            decimal weight = 0;
            Data.OrderPackagesTableRow[] packs = order.GetOrderPackagesTableRows();
            try
            {
                foreach (Data.OrderPackagesTableRow r in packs)
                {
                    if (data.OrderPackagesTable.Columns.IndexOf("Package Weight") >= 0)
                    {
                        weight += System.Convert.ToDecimal(r["Package Weight"].ToString());
                    }
                    else
                    {
                        if ((data.OrderPackagesTable.Columns.IndexOf("Total Weight") > 0))
                        {
                            weight += System.Convert.ToDecimal(r["Total Weight"].ToString());
                        }
                    }
                }
                if (weight > 150.0m)
                {
                    isSmallPack = false;
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error calculating total packages weight", ex);
            }
            return isSmallPack;
        }
        private string strReplyAll(int code, int index)
        {
            Data.RepliesTableRow reply_row = data.RepliesTable.FindBy_Order__FieldIndex(order._Order__, code, index);
            if (reply_row == null) return null;
            return reply_row.Value;
        }

        private string strReply(int code, int index = 0)
        {
            Data.RepliesTableRow reply_row = data.RepliesTable.FindBy_Order__FieldIndex(order._Order__, code, index);
            if (reply_row == null) return null;
            return reply_row.Value;
        }

        private int intReply(int code, int index = 0)
        {
            return Convert.ToInt32(strReply(code, index));
        }

        private void Tabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Tabs.SelectedTab == null) return;

            // using only one grid, and displaying the services for the current carrier
            Grid.Parent = Tabs.SelectedTab;

            try
            {
                Grid.Rows.Clear();

                // total number of rate lines
                int lines = data.RepliesTable.Select("[Order #] = '" + order._Order__ + "' And Field = 158").Length;
                if (lines == 0) return;
                string err = "";
                //add all carriers on the first tab
                for (int k = 0; k < lines; k++)
                {
                    err = strReply(599, k);
                    if (Tabs.SelectedTab.Name.Equals("ALL"))
                    {
                        
                        if ((intReply(544,k) == 0) && String.IsNullOrEmpty(err))    // 544 = package index -> skip the package rates, only the shipment level rates are shown
                        //if ((intReply(544, k) == 0))    // 544 = package index -> skip the package rates, only the shipment level rates are shown
                            {
                            string code = strReply(159, k); // service code
                            string name = strReply(372, k); // service name
                            string cost = strReply(183, k);
                            if (String.IsNullOrEmpty(cost)) cost = "N/A";
                            string delivery = strReply(161, k);
                            if (String.IsNullOrEmpty(delivery)) delivery = strReply(363, k);
                            if (String.IsNullOrEmpty(delivery)) delivery = "N/A";
                            string error = strReply(599, k);
                            if (error == null) error = "";
                            string carrierName = "";
                            foreach (Data.CarriersRow carrier in data.Carriers)
                                if (carrier.Carrier_Code == strReply(158, k))
                                    carrierName = carrier.Name;
                            //do not show the rates for the carriers not present in carriers.xml(each client has it's carriers to use)
                            if (carrierName.Equals(""))
                                continue;
                            //if is small pack shipment remove Puro Freight and if is LTL shipment remove Puro Express
                            if (carrierName.Equals("Purolator Freight") && isSmallPackage(order))
                                continue;
                            if (carrierName.Equals("Purolator") && !isSmallPackage(order))
                                continue;
                            
                            if (!Grid.Columns.Contains("Carrier"))
                            {
                                DataGridViewColumn newColumn = new DataGridViewColumn(Grid.Columns[0].CellTemplate);
                                //newColumn = Grid.Columns[0];
                                newColumn.Name = "Carrier";
                                newColumn.HeaderText = "Carrier";
                                Grid.Columns.Insert(0, newColumn);
                            }
                            
                                int row_index = Grid.Rows.Add(carrierName, name, cost, delivery, error);
                                Grid.Rows[row_index].Tag = code;
                            
                        }
                    }
                        if (Tabs.SelectedTab.Name.Equals(strReply(158, k)))     // 158 = carrier code
                            //if ((intReply(544, k) == 0) && String.IsNullOrEmpty(err))    // 544 = package index -> skip the package rates, only the shipment level rates are shown
                            if ((intReply(544, k) == 0))    // 544 = package index -> skip the package rates, only the shipment level rates are shown
                            {
                                string code = strReply(159, k); // service code
                                string name = strReply(372, k); // service name
                                string cost = strReply(183, k);
                                if (String.IsNullOrEmpty(cost)) cost = "N/A";
                                string delivery = strReply(161, k);
                                if (String.IsNullOrEmpty(delivery)) delivery = strReply(363, k);
                                if (String.IsNullOrEmpty(delivery)) delivery = "N/A";
                                string error = strReply(599, k);
                                if (error == null) error = "";
                                int row_index = 0;
                                if (Grid.Columns.Contains("Carrier"))
                                {
                                    row_index = Grid.Rows.Add("", name, cost, delivery, error);
                                    Grid.Columns.RemoveAt(0);
                                }
                                else
                                {
                                    row_index = Grid.Rows.Add( name, cost, delivery, error);
                                }
                                Grid.Rows[row_index].Tag = code;
                            }
                        
                    }
                    if (Grid.Rows.Count > 0) Grid.Rows[0].Selected = true;
                }
            
            catch (Exception ex)
            {
                Utils.SendError("Error processing rate results", ex);
                MessageBox.Show(this, "An internal error has occured. An error log was sent to 2Ship support team.", "Integr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //not used now
        private int carrierIndex(string service)
        {
            int serviceLines = data.RepliesTable.Select("[Order #] = '" + order._Order__ + "' And Field = 159").Length;
            //string carrierCode = "";
            string sCode = "";
            int index = 0;
            for (int k = 0; k < serviceLines; k++)
            {
                sCode = strReply(159, k);
                if (sCode.Equals(Grid.SelectedRows[0].Tag) && strReply(599, k).Equals(""))// && (!String.IsNullOrEmpty(strReply(183, k))))
                    //carrierCode = sCode;
                    index = k;
                
            }
            return index;
        }
        private void cmdOk_Click(object sender, EventArgs e)
        {
            if (Tabs.SelectedTab.Name.Equals("ALL"))
            {
                if (Grid.SelectedRows.Count > 0 && Grid.SelectedRows[0].Tag != null && !String.IsNullOrEmpty(Grid.SelectedRows[0].Tag.ToString()))
                {

                   // int k = carrierIndex(Grid.SelectedRows[0].Tag.ToString()); 
                    string s = Grid.SelectedRows[0].Cells[0].Value.ToString();
                    Data.CarriersRow cr = data.Carriers.FindByName(s);

                    //int k = Grid.SelectedRows[0].Index;
                    string carrierCode = cr.Carrier_Code; //strReply(158, k);
                    //string carrierCode = strReplyAll(158, k);
                        selected_carrier = carrierCode;
                    selected_service = carrierCode + " - " + Grid.SelectedRows[0].Tag;
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    Close();
                }
            }
            else
            {
                if (Grid.SelectedRows.Count > 0 && Grid.SelectedRows[0].Tag != null && !String.IsNullOrEmpty(Grid.SelectedRows[0].Tag.ToString()))
                {
                    selected_carrier = Tabs.SelectedTab.Name;
                    selected_service = Tabs.SelectedTab.Name + " - " + Grid.SelectedRows[0].Tag;
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    Close();
                }
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
