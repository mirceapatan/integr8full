﻿namespace InteGr8
{
    partial class frmWizard09
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard09));
            this.btnNext = new System.Windows.Forms.Button();
            this.lblHeaderInfo = new System.Windows.Forms.Label();
            this.lblPackagesIdColumnMessage = new System.Windows.Forms.Label();
            this.lblPackagesMappingLine = new System.Windows.Forms.Label();
            this.cbPackagesIdColumn = new System.Windows.Forms.ComboBox();
            this.lblPackagesMapping = new System.Windows.Forms.Label();
            this.lblPackagesIdColumn = new System.Windows.Forms.Label();
            this.lblHeaderLine = new System.Windows.Forms.Label();
            this.cbPackagesShipmentIdColumn = new System.Windows.Forms.ComboBox();
            this.lblPackagesShipmentIdColumn = new System.Windows.Forms.Label();
            this.lblPackagesTable = new System.Windows.Forms.Label();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.llAdvanced = new System.Windows.Forms.LinkLabel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.lblHeaderIcon = new System.Windows.Forms.Label();
            this.cbShipmentPackagesTable = new System.Windows.Forms.ComboBox();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.btnPreview = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.lblBilledWeightMessage = new System.Windows.Forms.Label();
            this.lblPriceMessage = new System.Windows.Forms.Label();
            this.cbBilledWeight = new System.Windows.Forms.ComboBox();
            this.lblBilledWeight = new System.Windows.Forms.Label();
            this.cbPrice = new System.Windows.Forms.ComboBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.cbPackagesPackageNumberColumn = new System.Windows.Forms.ComboBox();
            this.lblPackagesPackageNumberColumn = new System.Windows.Forms.Label();
            this.lblPackagesPackageNumberColumnMessage = new System.Windows.Forms.Label();
            this.lblPackagesShipmentIdColumnMessage = new System.Windows.Forms.Label();
            this.lblNote = new System.Windows.Forms.Label();
            this.lblPackagesOrderNumberColumnMessage = new System.Windows.Forms.Label();
            this.cbPackagesOrderNumberColumn = new System.Windows.Forms.ComboBox();
            this.lblPackagesOrderNumberColumn = new System.Windows.Forms.Label();
            this.pnlHeader.SuspendLayout();
            this.pnlFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(514, 18);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 9;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblHeaderInfo
            // 
            this.lblHeaderInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderInfo.AutoEllipsis = true;
            this.lblHeaderInfo.Location = new System.Drawing.Point(68, 24);
            this.lblHeaderInfo.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderInfo.Name = "lblHeaderInfo";
            this.lblHeaderInfo.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.lblHeaderInfo.Size = new System.Drawing.Size(533, 64);
            this.lblHeaderInfo.TabIndex = 114;
            this.lblHeaderInfo.Text = resources.GetString("lblHeaderInfo.Text");
            // 
            // lblPackagesIdColumnMessage
            // 
            this.lblPackagesIdColumnMessage.AutoSize = true;
            this.lblPackagesIdColumnMessage.Location = new System.Drawing.Point(364, 130);
            this.lblPackagesIdColumnMessage.Name = "lblPackagesIdColumnMessage";
            this.lblPackagesIdColumnMessage.Size = new System.Drawing.Size(216, 13);
            this.lblPackagesIdColumnMessage.TabIndex = 314;
            this.lblPackagesIdColumnMessage.Text = "unique number assigned by the 2Ship carrier";
            // 
            // lblPackagesMappingLine
            // 
            this.lblPackagesMappingLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPackagesMappingLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblPackagesMappingLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPackagesMappingLine.Location = new System.Drawing.Point(12, 264);
            this.lblPackagesMappingLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblPackagesMappingLine.Name = "lblPackagesMappingLine";
            this.lblPackagesMappingLine.Size = new System.Drawing.Size(577, 2);
            this.lblPackagesMappingLine.TabIndex = 296;
            // 
            // cbPackagesIdColumn
            // 
            this.cbPackagesIdColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPackagesIdColumn.FormattingEnabled = true;
            this.cbPackagesIdColumn.Items.AddRange(new object[] {
            "(none)",
            "Current date of processing",
            "Next day of processing"});
            this.cbPackagesIdColumn.Location = new System.Drawing.Point(184, 127);
            this.cbPackagesIdColumn.Name = "cbPackagesIdColumn";
            this.cbPackagesIdColumn.Size = new System.Drawing.Size(174, 21);
            this.cbPackagesIdColumn.TabIndex = 1;
            // 
            // lblPackagesMapping
            // 
            this.lblPackagesMapping.AutoSize = true;
            this.lblPackagesMapping.Location = new System.Drawing.Point(9, 246);
            this.lblPackagesMapping.Name = "lblPackagesMapping";
            this.lblPackagesMapping.Size = new System.Drawing.Size(493, 13);
            this.lblPackagesMapping.TabIndex = 294;
            this.lblPackagesMapping.Text = "Please map the following 2Ship shipment package variables to your Shipment Packag" +
                "es table columns:";
            // 
            // lblPackagesIdColumn
            // 
            this.lblPackagesIdColumn.AutoSize = true;
            this.lblPackagesIdColumn.Location = new System.Drawing.Point(9, 131);
            this.lblPackagesIdColumn.Name = "lblPackagesIdColumn";
            this.lblPackagesIdColumn.Size = new System.Drawing.Size(165, 13);
            this.lblPackagesIdColumn.TabIndex = 295;
            this.lblPackagesIdColumn.Text = "Shipment Packages Id # Column:";
            // 
            // lblHeaderLine
            // 
            this.lblHeaderLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderLine.BackColor = System.Drawing.SystemColors.Control;
            this.lblHeaderLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeaderLine.Location = new System.Drawing.Point(0, 88);
            this.lblHeaderLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderLine.Name = "lblHeaderLine";
            this.lblHeaderLine.Size = new System.Drawing.Size(601, 2);
            this.lblHeaderLine.TabIndex = 116;
            // 
            // cbPackagesShipmentIdColumn
            // 
            this.cbPackagesShipmentIdColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPackagesShipmentIdColumn.FormattingEnabled = true;
            this.cbPackagesShipmentIdColumn.Location = new System.Drawing.Point(184, 154);
            this.cbPackagesShipmentIdColumn.Name = "cbPackagesShipmentIdColumn";
            this.cbPackagesShipmentIdColumn.Size = new System.Drawing.Size(174, 21);
            this.cbPackagesShipmentIdColumn.TabIndex = 2;
            // 
            // lblPackagesShipmentIdColumn
            // 
            this.lblPackagesShipmentIdColumn.AutoSize = true;
            this.lblPackagesShipmentIdColumn.Location = new System.Drawing.Point(9, 158);
            this.lblPackagesShipmentIdColumn.Name = "lblPackagesShipmentIdColumn";
            this.lblPackagesShipmentIdColumn.Size = new System.Drawing.Size(114, 13);
            this.lblPackagesShipmentIdColumn.TabIndex = 292;
            this.lblPackagesShipmentIdColumn.Text = "Shipment Id # Column:";
            // 
            // lblPackagesTable
            // 
            this.lblPackagesTable.AutoSize = true;
            this.lblPackagesTable.Location = new System.Drawing.Point(8, 104);
            this.lblPackagesTable.Name = "lblPackagesTable";
            this.lblPackagesTable.Size = new System.Drawing.Size(140, 13);
            this.lblPackagesTable.TabIndex = 291;
            this.lblPackagesTable.Text = "Shipments Packages Table:";
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.lblHeaderLine);
            this.pnlHeader.Controls.Add(this.llAdvanced);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.lblHeaderIcon);
            this.pnlHeader.Controls.Add(this.lblHeaderInfo);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(601, 90);
            this.pnlHeader.TabIndex = 289;
            // 
            // llAdvanced
            // 
            this.llAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdvanced.Image = global::InteGr8.Properties.Resources.wrench;
            this.llAdvanced.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llAdvanced.LinkArea = new System.Windows.Forms.LinkArea(6, 13);
            this.llAdvanced.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdvanced.Location = new System.Drawing.Point(497, 0);
            this.llAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.llAdvanced.Name = "llAdvanced";
            this.llAdvanced.Size = new System.Drawing.Size(104, 24);
            this.llAdvanced.TabIndex = 119;
            this.llAdvanced.TabStop = true;
            this.llAdvanced.Text = "      advanced mode";
            this.llAdvanced.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llAdvanced.UseCompatibleTextRendering = true;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(497, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Packages Level Export Information";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeaderIcon
            // 
            this.lblHeaderIcon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeaderIcon.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaderIcon.Image = global::InteGr8.Properties.Resources.data_next;
            this.lblHeaderIcon.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeaderIcon.Location = new System.Drawing.Point(0, 24);
            this.lblHeaderIcon.Name = "lblHeaderIcon";
            this.lblHeaderIcon.Padding = new System.Windows.Forms.Padding(5);
            this.lblHeaderIcon.Size = new System.Drawing.Size(64, 64);
            this.lblHeaderIcon.TabIndex = 115;
            // 
            // cbShipmentPackagesTable
            // 
            this.cbShipmentPackagesTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbShipmentPackagesTable.FormattingEnabled = true;
            this.cbShipmentPackagesTable.Location = new System.Drawing.Point(184, 100);
            this.cbShipmentPackagesTable.Name = "cbShipmentPackagesTable";
            this.cbShipmentPackagesTable.Size = new System.Drawing.Size(174, 21);
            this.cbShipmentPackagesTable.TabIndex = 0;
            this.cbShipmentPackagesTable.SelectedIndexChanged += new System.EventHandler(this.cbShipmentPackagesTable_SelectedIndexChanged);
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.lblFooterLine);
            this.pnlFooter.Controls.Add(this.btnPreview);
            this.pnlFooter.Controls.Add(this.btnPrev);
            this.pnlFooter.Controls.Add(this.btnNext);
            this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFooter.Location = new System.Drawing.Point(0, 360);
            this.pnlFooter.Margin = new System.Windows.Forms.Padding(0);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Padding = new System.Windows.Forms.Padding(9);
            this.pnlFooter.Size = new System.Drawing.Size(601, 53);
            this.pnlFooter.TabIndex = 290;
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFooterLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(12, 5);
            this.lblFooterLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(577, 2);
            this.lblFooterLine.TabIndex = 122;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(11, 18);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(108, 23);
            this.btnPreview.TabIndex = 7;
            this.btnPreview.Text = "Preview Packages";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnPrev.Location = new System.Drawing.Point(433, 18);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 8;
            this.btnPrev.Text = "< Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // lblBilledWeightMessage
            // 
            this.lblBilledWeightMessage.AutoSize = true;
            this.lblBilledWeightMessage.Location = new System.Drawing.Point(315, 309);
            this.lblBilledWeightMessage.Name = "lblBilledWeightMessage";
            this.lblBilledWeightMessage.Size = new System.Drawing.Size(238, 13);
            this.lblBilledWeightMessage.TabIndex = 306;
            this.lblBilledWeightMessage.Text = "the weight used by the carier to bill your package";
            // 
            // lblPriceMessage
            // 
            this.lblPriceMessage.AutoSize = true;
            this.lblPriceMessage.Location = new System.Drawing.Point(315, 282);
            this.lblPriceMessage.Name = "lblPriceMessage";
            this.lblPriceMessage.Size = new System.Drawing.Size(140, 13);
            this.lblPriceMessage.TabIndex = 304;
            this.lblPriceMessage.Text = "your package shipment cost";
            // 
            // cbBilledWeight
            // 
            this.cbBilledWeight.FormattingEnabled = true;
            this.cbBilledWeight.Location = new System.Drawing.Point(133, 306);
            this.cbBilledWeight.Name = "cbBilledWeight";
            this.cbBilledWeight.Size = new System.Drawing.Size(174, 21);
            this.cbBilledWeight.TabIndex = 6;
            // 
            // lblBilledWeight
            // 
            this.lblBilledWeight.AutoSize = true;
            this.lblBilledWeight.Location = new System.Drawing.Point(8, 309);
            this.lblBilledWeight.Name = "lblBilledWeight";
            this.lblBilledWeight.Size = new System.Drawing.Size(114, 13);
            this.lblBilledWeight.TabIndex = 302;
            this.lblBilledWeight.Text = "Package billed weight:";
            // 
            // cbPrice
            // 
            this.cbPrice.FormattingEnabled = true;
            this.cbPrice.Items.AddRange(new object[] {
            "Customer Packaging",
            "Envelope (Letter)",
            "Small Pack"});
            this.cbPrice.Location = new System.Drawing.Point(133, 279);
            this.cbPrice.Name = "cbPrice";
            this.cbPrice.Size = new System.Drawing.Size(174, 21);
            this.cbPrice.TabIndex = 5;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(8, 282);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(79, 13);
            this.lblPrice.TabIndex = 299;
            this.lblPrice.Text = "Package price:";
            // 
            // cbPackagesPackageNumberColumn
            // 
            this.cbPackagesPackageNumberColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPackagesPackageNumberColumn.FormattingEnabled = true;
            this.cbPackagesPackageNumberColumn.Location = new System.Drawing.Point(184, 208);
            this.cbPackagesPackageNumberColumn.Name = "cbPackagesPackageNumberColumn";
            this.cbPackagesPackageNumberColumn.Size = new System.Drawing.Size(174, 21);
            this.cbPackagesPackageNumberColumn.TabIndex = 4;
            // 
            // lblPackagesPackageNumberColumn
            // 
            this.lblPackagesPackageNumberColumn.AutoSize = true;
            this.lblPackagesPackageNumberColumn.Location = new System.Drawing.Point(9, 212);
            this.lblPackagesPackageNumberColumn.Name = "lblPackagesPackageNumberColumn";
            this.lblPackagesPackageNumberColumn.Size = new System.Drawing.Size(101, 13);
            this.lblPackagesPackageNumberColumn.TabIndex = 324;
            this.lblPackagesPackageNumberColumn.Text = "Package # Column:";
            // 
            // lblPackagesPackageNumberColumnMessage
            // 
            this.lblPackagesPackageNumberColumnMessage.AutoSize = true;
            this.lblPackagesPackageNumberColumnMessage.Location = new System.Drawing.Point(364, 211);
            this.lblPackagesPackageNumberColumnMessage.Name = "lblPackagesPackageNumberColumnMessage";
            this.lblPackagesPackageNumberColumnMessage.Size = new System.Drawing.Size(207, 13);
            this.lblPackagesPackageNumberColumnMessage.TabIndex = 325;
            this.lblPackagesPackageNumberColumnMessage.Text = "optional, links to the Order Packages table";
            // 
            // lblPackagesShipmentIdColumnMessage
            // 
            this.lblPackagesShipmentIdColumnMessage.AutoSize = true;
            this.lblPackagesShipmentIdColumnMessage.Location = new System.Drawing.Point(364, 157);
            this.lblPackagesShipmentIdColumnMessage.Name = "lblPackagesShipmentIdColumnMessage";
            this.lblPackagesShipmentIdColumnMessage.Size = new System.Drawing.Size(179, 13);
            this.lblPackagesShipmentIdColumnMessage.TabIndex = 326;
            this.lblPackagesShipmentIdColumnMessage.Text = "optional, links to the Shipments table";
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNote.Location = new System.Drawing.Point(8, 340);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(491, 13);
            this.lblNote.TabIndex = 327;
            this.lblNote.Text = "Note: You will be able to add more variable mappings later, in the Summary Review" +
                " page of this wizard.";
            // 
            // lblPackagesOrderNumberColumnMessage
            // 
            this.lblPackagesOrderNumberColumnMessage.AutoSize = true;
            this.lblPackagesOrderNumberColumnMessage.Location = new System.Drawing.Point(364, 184);
            this.lblPackagesOrderNumberColumnMessage.Name = "lblPackagesOrderNumberColumnMessage";
            this.lblPackagesOrderNumberColumnMessage.Size = new System.Drawing.Size(161, 13);
            this.lblPackagesOrderNumberColumnMessage.TabIndex = 330;
            this.lblPackagesOrderNumberColumnMessage.Text = "optional, links to the Orders table";
            // 
            // cbPackagesOrderNumberColumn
            // 
            this.cbPackagesOrderNumberColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPackagesOrderNumberColumn.FormattingEnabled = true;
            this.cbPackagesOrderNumberColumn.Location = new System.Drawing.Point(184, 181);
            this.cbPackagesOrderNumberColumn.Name = "cbPackagesOrderNumberColumn";
            this.cbPackagesOrderNumberColumn.Size = new System.Drawing.Size(174, 21);
            this.cbPackagesOrderNumberColumn.TabIndex = 3;
            // 
            // lblPackagesOrderNumberColumn
            // 
            this.lblPackagesOrderNumberColumn.AutoSize = true;
            this.lblPackagesOrderNumberColumn.Location = new System.Drawing.Point(9, 185);
            this.lblPackagesOrderNumberColumn.Name = "lblPackagesOrderNumberColumn";
            this.lblPackagesOrderNumberColumn.Size = new System.Drawing.Size(84, 13);
            this.lblPackagesOrderNumberColumn.TabIndex = 329;
            this.lblPackagesOrderNumberColumn.Text = "Order # Column:";
            // 
            // frmWizard09
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(601, 413);
            this.Controls.Add(this.lblPackagesOrderNumberColumnMessage);
            this.Controls.Add(this.cbPackagesOrderNumberColumn);
            this.Controls.Add(this.lblPackagesOrderNumberColumn);
            this.Controls.Add(this.lblNote);
            this.Controls.Add(this.lblPackagesShipmentIdColumnMessage);
            this.Controls.Add(this.lblPackagesPackageNumberColumnMessage);
            this.Controls.Add(this.cbPackagesPackageNumberColumn);
            this.Controls.Add(this.lblPackagesPackageNumberColumn);
            this.Controls.Add(this.lblPackagesIdColumnMessage);
            this.Controls.Add(this.lblPackagesMappingLine);
            this.Controls.Add(this.cbPackagesIdColumn);
            this.Controls.Add(this.lblPackagesMapping);
            this.Controls.Add(this.lblPackagesIdColumn);
            this.Controls.Add(this.cbPackagesShipmentIdColumn);
            this.Controls.Add(this.lblPackagesShipmentIdColumn);
            this.Controls.Add(this.lblPackagesTable);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.cbShipmentPackagesTable);
            this.Controls.Add(this.pnlFooter);
            this.Controls.Add(this.lblBilledWeightMessage);
            this.Controls.Add(this.lblPriceMessage);
            this.Controls.Add(this.cbBilledWeight);
            this.Controls.Add(this.lblBilledWeight);
            this.Controls.Add(this.cbPrice);
            this.Controls.Add(this.lblPrice);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWizard09";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 9 of 11";
            this.Load += new System.EventHandler(this.frmWizard09_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard09_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWizard09_FormClosing);
            this.pnlHeader.ResumeLayout(false);
            this.pnlFooter.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblHeaderInfo;
        private System.Windows.Forms.Label lblPackagesIdColumnMessage;
        private System.Windows.Forms.Label lblPackagesMappingLine;
        private System.Windows.Forms.ComboBox cbPackagesIdColumn;
        private System.Windows.Forms.Label lblPackagesMapping;
        private System.Windows.Forms.Label lblPackagesIdColumn;
        private System.Windows.Forms.Label lblHeaderLine;
        private System.Windows.Forms.ComboBox cbPackagesShipmentIdColumn;
        private System.Windows.Forms.Label lblPackagesShipmentIdColumn;
        private System.Windows.Forms.Label lblPackagesTable;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.LinkLabel llAdvanced;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label lblHeaderIcon;
        private System.Windows.Forms.ComboBox cbShipmentPackagesTable;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Label lblBilledWeightMessage;
        private System.Windows.Forms.Label lblPriceMessage;
        private System.Windows.Forms.ComboBox cbBilledWeight;
        private System.Windows.Forms.Label lblBilledWeight;
        private System.Windows.Forms.ComboBox cbPrice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.ComboBox cbPackagesPackageNumberColumn;
        private System.Windows.Forms.Label lblPackagesPackageNumberColumn;
        private System.Windows.Forms.Label lblPackagesPackageNumberColumnMessage;
        private System.Windows.Forms.Label lblPackagesShipmentIdColumnMessage;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.Label lblPackagesOrderNumberColumnMessage;
        private System.Windows.Forms.ComboBox cbPackagesOrderNumberColumn;
        private System.Windows.Forms.Label lblPackagesOrderNumberColumn;
    }
}