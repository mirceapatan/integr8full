﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class MyCarrier : Form
    {
        Data data_temp = new Data();
        bool first = true;

        public MyCarrier(Data data)
        {
            InitializeComponent();
            data_temp = data;
        }

        private void MyCarrier_Load(object sender, EventArgs e)
        {
            DataSet data = new DataSet();
            BindingSource bs = new BindingSource();
            data.ReadXml("symbology.xml");
            bs.DataSource = data.Tables[0];
            cb_bare_code_symbology.ValueMember = "BarCode_Value";
            cb_bare_code_symbology.DisplayMember = "BarCode_Name";
            cb_bare_code_symbology.DataSource = bs;
            cb_bare_code_symbology.SelectedValue = 5;

            BindingSource bs_carrier_name = new BindingSource();
            bs_carrier_name.DataSource = InteGr8_Main.data.MyCarrierNames;
            cb_carrier_name.DisplayMember = "Name";
            cb_carrier_name.ValueMember = "Default";
            cb_carrier_name.DataSource = bs_carrier_name;
            cb_carrier_name.SelectedValue = true;

            radiob_small_package.Checked = true;
            radiob_ltl.Checked = true;

            Data.MyCarrierRow[] carrier_row = data_temp.MyCarrier.Select() as Data.MyCarrierRow[];
            if (carrier_row.Length > 0)
            {
                txt_billing_account.Text = carrier_row[0].Billing_account;
                cb_carrier_name.SelectedIndex = cb_carrier_name.FindString(carrier_row[0].Carrier_Name);
                txt_packaging.Text = carrier_row[0].Packaging;
                txt_service_name.Text = carrier_row[0].Carrier_Service;
                txt_list_tarrif.Text = carrier_row[0].List_Tarrif;
                txt_discount.Text = carrier_row[0].Discount;
                if (carrier_row[0].Discount_Type == '#')
                {
                    radiob_discount_type_diez.Checked = true;
                }
                if (carrier_row[0].Discount_Type == '%')
                {
                    radiob_discount_type_procent.Checked = true;
                }
                txt_fuel.Text = carrier_row[0].Fuel;
                if (carrier_row[0].Fuel_Type == '#')
                {
                    radiob_fuel_type_diez.Checked = true;
                }
                if (carrier_row[0].Fuel_Type == '%')
                {
                    radiob_fuel_type_procent.Checked = true;
                }
                txt_total_other_surcharges.Text = carrier_row[0].Total_Other_Surcharges;
                //txt_tacking_number.Text = carrier_row[0].Tracking_Number;
                //txt_pro_number.Text = carrier_row[0].Pro_Number;
                radiob_small_package.Checked = carrier_row[0].Small_Package;
                radiob_ltl.Checked = carrier_row[0].LTL;
                radiob_same_day.Checked = carrier_row[0].Same_Day;
                checkbox_reuse.Checked = carrier_row[0].Reuse;
                if (carrier_row[0].Guaranteed_Delivery != "")
                {
                    checkBox_guaranteed_delivery.Checked = true;
                    textBox_guaranteed_delivery.Text = carrier_row[0].Guaranteed_Delivery;
                }
                if (carrier_row[0].Pickup_appointment != "")
                {
                    checkBox_pickup_appointment.Checked = true;
                    textBox_pickup_appointment.Text = carrier_row[0].Pickup_appointment;
                }
                if (carrier_row[0].Delivery_appointment != "")
                {
                    checkBox_delivery_appointment.Checked = true;
                    textBox_delivery_appointment.Text = carrier_row[0].Delivery_appointment;
                }
                if (carrier_row[0].Tailgate_charge != "")
                {
                    checkBox_tailgate_charge.Checked = true;
                    textBox_tailgate_charge.Text = carrier_row[0].Tailgate_charge;
                }
                if (carrier_row[0].Lumper != "")
                {
                    checkBox_lumper.Checked = true;
                    textBox_lumper.Text = carrier_row[0].Lumper;
                }
                if (carrier_row[0].Driver_Assistance != "")
                {
                    checkBox_driver_assistance.Checked = true;
                    textBox_driver_assistance.Text = carrier_row[0].Driver_Assistance;
                }
                if (carrier_row[0].Pallet_Exchange != "")
                {
                    checkBox_pallet_exchange.Checked = true;
                    textBox_pallet_exchange.Text = carrier_row[0].Pallet_Exchange;
                }
                if (carrier_row[0].Heat != "")
                {
                    checkBox_heat.Checked = true;
                    textBox_heat.Text = carrier_row[0].Heat;
                }
                if (carrier_row[0].Reefer != "")
                {
                    checkBox_reefer.Checked = true;
                    textBox_reefer.Text = carrier_row[0].Reefer;
                }
                if (carrier_row[0].Temp_Control != "")
                {
                    checkBox_temp_control.Checked = true;
                    textBox_temp_control.Text = carrier_row[0].Temp_Control;
                }
                if (carrier_row[0].Hazmat != "")
                {
                    checkBox_hazmat.Checked = true;
                    textBox_hazmat.Text = carrier_row[0].Hazmat;
                }
                if (carrier_row[0].Fuel_Surcharge != "")
                {
                    checkBox_fuel_surcharge.Checked = true;
                    textBox_fuel_surcharge.Text = carrier_row[0].Fuel_Surcharge;
                }
                if (carrier_row[0].After_Hours_Delivery != "")
                {
                    checkBox_after_hours_delivery.Checked = true;
                    textBox_after_hours_delivery.Text = carrier_row[0].After_Hours_Delivery;
                }
                if (carrier_row[0].Air_Bag != "")
                {
                    checkBox_air_bag.Checked = true;
                    textBox_air_bag.Text = carrier_row[0].Air_Bag;
                }
                if (carrier_row[0].Bulkhead != "")
                {
                    checkBox_bulkhead.Checked = true;
                    textBox_bulkhead.Text = carrier_row[0].Bulkhead;
                }
                if (carrier_row[0].Commercial_Delivery != "")
                {
                    checkBox_commercial_delivery.Checked = true;
                    textBox_commercial_delivery.Text = carrier_row[0].Commercial_Delivery;
                }
                if (carrier_row[0].Cross_Border != "")
                {
                    checkBox_cross_border.Checked = true;
                    textBox_cross_border.Text = carrier_row[0].Cross_Border;
                }
                if (carrier_row[0].Crane_Permits != "")
                {
                    checkBox_crane_permits.Checked = true;
                    textBox_crane_permits.Text = carrier_row[0].Crane_Permits;
                }
                if (carrier_row[0].Driver_Load_And_Count != "")
                {
                    checkBox_driver_load_and_count.Checked = true;
                    textBox_driver_load_and_count.Text = carrier_row[0].Driver_Load_And_Count;
                }
                if (carrier_row[0].Drop_Pull != "")
                {
                    checkBox_drop_pull.Checked = true;
                    textBox_drop_pull.Text = carrier_row[0].Drop_Pull;
                }
                if (carrier_row[0].Driver_Unload != "")
                {
                    checkBox_driver_unload.Checked = true;
                    textBox_driver_unload.Text = carrier_row[0].Driver_Unload;
                }
                if (carrier_row[0].Driver_Unload_And_Count != "")
                {
                    checkBox_driver_unload_and_count.Checked = true;
                    textBox_driver_unload_and_count.Text = carrier_row[0].Driver_Unload_And_Count;
                }
                if (carrier_row[0].Expedited_Same_DY_SNC != "")
                {
                    checkBox_expedited_same_dy_snc.Checked = true;
                    textBox_expedited_same_dy_snc.Text = carrier_row[0].Expedited_Same_DY_SNC;
                }
                if (carrier_row[0].Forklift != "")
                {
                    checkBox_forklift.Checked = true;
                    textBox_forklift.Text = carrier_row[0].Forklift;
                }
                if (carrier_row[0].Handling_And_Labeling != "")
                {
                    checkBox_handling_and_labeling.Checked = true;
                    textBox_handling_and_labeling.Text = carrier_row[0].Handling_And_Labeling;
                }
                if (carrier_row[0].Inside_Delivery != "")
                {
                    checkBox_inside_delivery.Checked = true;
                    textBox_inside_delivery.Text = carrier_row[0].Inside_Delivery;
                }
                if (carrier_row[0].Limited_Access_Delivery != "")
                {
                    checkBox_limited_access_delivery.Checked = true;
                    textBox_limited_access_delivery.Text = carrier_row[0].Limited_Access_Delivery;
                }
                if (carrier_row[0].Lift_Gate_Destination != "")
                {
                    checkBox_lift_gate_destination.Checked = true;
                    textBox_lift_gate_destination.Text = carrier_row[0].Lift_Gate_Destination;
                }
                if (carrier_row[0].Limited_Delivery_or_Military != "")
                {
                    checkBox_limited_delivery_or_military.Checked = true;
                    textBox_limited_delivery_or_military.Text = carrier_row[0].Limited_Delivery_or_Military;
                }
                if (carrier_row[0].Notification != "")
                {
                    checkBox_notification.Checked = true;
                    textBox_notification.Text = carrier_row[0].Notification;
                }
                if (carrier_row[0].Protective != "")
                {
                    checkBox_protective.Checked = true;
                    textBox_protective.Text = carrier_row[0].Protective;
                }
                if (carrier_row[0].Residential_Pickup != "")
                {
                    checkBox_residential_pickup.Checked = true;
                    textBox_residential_pickup.Text = carrier_row[0].Residential_Pickup;
                }
                if (carrier_row[0].Stop_Off != "")
                {
                    checkBox_stop_off.Checked = true;
                    textBox_stop_off.Text = carrier_row[0].Stop_Off;
                }
                if (carrier_row[0].Team_Required != "")
                {
                    checkBox_team_required.Checked = true;
                    textBox_team_required.Text = carrier_row[0].Team_Required;
                }
                if (carrier_row[0].Live_Loading_or_Unloading != "")
                {
                    checkBox_live_loading_or_unloading.Checked = true;
                    textBox_live_loading_or_unloading.Text = carrier_row[0].Live_Loading_or_Unloading;
                }
                if (carrier_row[0].Appointment != "")
                {
                    checkBox_appointment.Checked = true;
                    textBox_appointment.Text = carrier_row[0].Appointment;
                }
                if (carrier_row[0].Bag_Liner != "")
                {
                    checkBox_bag_liner.Checked = true;
                    textBox_bag_liner.Text = carrier_row[0].Bag_Liner;
                }
                if (carrier_row[0].Bobtail != "")
                {
                    checkBox_bobtail.Checked = true;
                    textBox_bobtail.Text = carrier_row[0].Bobtail;
                }
                if (carrier_row[0].Commercial_Pickup != "")
                {
                    checkBox_commercial_pickup.Checked = true;
                    textBox_commercial_pickup.Text = carrier_row[0].Commercial_Pickup;
                }
                if (carrier_row[0].Cross_Dock != "")
                {
                    checkBox_cross_dock.Checked = true;
                    textBox_cross_dock.Text = carrier_row[0].Cross_Dock;
                }
                if (carrier_row[0].Driver_Load_And_Unload != "")
                {
                    checkBox_driver_load_and_unload.Checked = true;
                    textBox_driver_load_and_unload.Text = carrier_row[0].Driver_Load_And_Unload;
                }
                if (carrier_row[0].Driver_Load != "")
                {
                    checkBox_driver_load.Checked = true;
                    textBox_driver_load.Text = carrier_row[0].Driver_Load;
                }
                if (carrier_row[0].Dry_Run != "")
                {
                    checkBox_dry_run.Checked = true;
                    textBox_dry_run.Text = carrier_row[0].Dry_Run;
                }
                if (carrier_row[0].Emergency_Delivery != "")
                {
                    checkBox_emergency_delivery.Checked = true;
                    textBox_emergency_delivery.Text = carrier_row[0].Emergency_Delivery;
                }
                if (carrier_row[0].Expedited_Shipment != "")
                {
                    checkBox_expedited_shipment.Checked = true;
                    textBox_expedited_shipment.Text = carrier_row[0].Expedited_Shipment;
                }
                if (carrier_row[0].Guaranteed != "")
                {
                    checkBox_guaranteed.Checked = true;
                    textBox_guaranteed.Text = carrier_row[0].Guaranteed;
                }
                if (carrier_row[0].Holiday_Delivery != "")
                {
                    checkBox_holiday_delivery.Checked = true;
                    textBox_holiday_delivery.Text = carrier_row[0].Holiday_Delivery;
                }
                if (carrier_row[0].Inside_Pickup != "")
                {
                    checkBox_inside_pickup.Checked = true;
                    textBox_inside_pickup.Text = carrier_row[0].Inside_Pickup;
                }
                if (carrier_row[0].Lift != "")
                {
                    checkBox_lift.Checked = true;
                    textBox_lift.Text = carrier_row[0].Lift;
                }
                if (carrier_row[0].Lift_Gate_Origin != "")
                {
                    checkBox_lift_gate_origin.Checked = true;
                    textBox_lift_gate_origin.Text = carrier_row[0].Lift_Gate_Origin;
                }
                if (carrier_row[0].Liquor_Permit != "")
                {
                    checkBox_liquor_permit.Checked = true;
                    textBox_liquor_permit.Text = carrier_row[0].Liquor_Permit;
                }
                if (carrier_row[0].Marking_And_Tagging != "")
                {
                    checkBox_marking_and_tagging.Checked = true;
                    textBox_marking_and_tagging.Text = carrier_row[0].Marking_And_Tagging;
                }
                if (carrier_row[0].Premium_Air_Freight != "")
                {
                    checkBox_premium_air_freight.Checked = true;
                    textBox_premium_air_freight.Text = carrier_row[0].Premium_Air_Freight;
                }
                if (carrier_row[0].Pallet != "")
                {
                    checkBox_pallet.Checked = true;
                    textBox_pallet.Text = carrier_row[0].Pallet;
                }
                if (carrier_row[0].Residential_Delivery != "")
                {
                    checkBox_residential_delivery.Checked = true;
                    textBox_residential_delivery.Text = carrier_row[0].Residential_Delivery;
                }
                if (carrier_row[0].Saturday_Pickup_or_Delivery != "")
                {
                    checkBox_saturday_pickup_or_delivery.Checked = true;
                    textBox_saturday_pickup_or_delivery.Text = carrier_row[0].Saturday_Pickup_or_Delivery;
                }
                if (carrier_row[0].Special_Equipment_Needed != "")
                {
                    checkBox_special_equipment_needed.Checked = true;
                    textBox_special_equipment_needed.Text = carrier_row[0].Special_Equipment_Needed;
                }
                if (carrier_row[0].Tradeshow != "")
                {
                    checkBox_tradeshow.Checked = true;
                    textBox_tradeshow.Text = carrier_row[0].Tradeshow;
                }
            }
            else
            {
                radiob_ltl.Checked = true;
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            data_temp.MyCarrier.Clear();
            data_temp.MyCarrier.AddMyCarrierRow(checkbox_reuse.Checked,
                txt_list_tarrif.Text, 
                txt_discount.Text, 
                radiob_discount_type_diez.Checked ? radiob_discount_type_diez.Text[0] : radiob_discount_type_procent.Text[0], 
                txt_fuel.Text, radiob_fuel_type_diez.Checked ? radiob_fuel_type_diez.Text[0] : radiob_fuel_type_procent.Text[0], 
                txt_total_other_surcharges.Text,
                txt_tacking_number.Text,
                txt_pro_number.Text,
                txt_billing_account.Text,
                String.IsNullOrEmpty(cb_bare_code_symbology.SelectedValue.ToString()) ? "" : cb_bare_code_symbology.SelectedValue.ToString(), 
                radiob_small_package.Checked, 
                radiob_ltl.Checked, 
                radiob_same_day.Checked,
                cb_carrier_name.Text,
                txt_service_name.Text,
                txt_packaging.Text,
                checkBox_guaranteed_delivery.Checked ? textBox_guaranteed_delivery.Text : "",
                checkBox_pickup_appointment.Checked ? textBox_pickup_appointment.Text : "",
                checkBox_delivery_appointment.Checked ? textBox_delivery_appointment.Text : "",
                checkBox_tailgate_charge.Checked ? textBox_tailgate_charge.Text : "",
                checkBox_lumper.Checked ? textBox_lumper.Text : "",
                checkBox_driver_assistance.Checked ? textBox_driver_assistance.Text : "",
                checkBox_pallet_exchange.Checked ? textBox_pallet_exchange.Text : "",
                checkBox_heat.Checked ? textBox_heat.Text : "",
                checkBox_reefer.Checked ? textBox_reefer.Text : "",
                checkBox_temp_control.Checked ? textBox_temp_control.Text : "",
                checkBox_hazmat.Checked ? textBox_hazmat.Text : "",
                checkBox_fuel_surcharge.Checked ? textBox_fuel_surcharge.Text : "",
                checkBox_after_hours_delivery.Checked ? textBox_after_hours_delivery.Text : "",
                checkBox_air_bag.Checked ? textBox_air_bag.Text : "",
                checkBox_bulkhead.Checked ? textBox_bulkhead.Text : "",
                checkBox_commercial_delivery.Checked ? textBox_commercial_delivery.Text : "",
                checkBox_cross_border.Checked ? textBox_cross_border.Text : "",
                checkBox_crane_permits.Checked ? textBox_crane_permits.Text : "",
                checkBox_driver_load_and_count.Checked ? textBox_driver_load_and_count.Text : "",
                checkBox_drop_pull.Checked ? textBox_drop_pull.Text : "",
                checkBox_driver_unload.Checked ? textBox_driver_unload.Text : "",
                checkBox_driver_unload_and_count.Checked ? textBox_driver_unload_and_count.Text : "",
                checkBox_expedited_same_dy_snc.Checked ? textBox_expedited_same_dy_snc.Text : "",
                checkBox_forklift.Checked ? textBox_forklift.Text : "",
                checkBox_handling_and_labeling.Checked ? textBox_handling_and_labeling.Text : "",
                checkBox_inside_delivery.Checked ? textBox_inside_delivery.Text : "",
                checkBox_limited_access_delivery.Checked ? textBox_limited_access_delivery.Text : "",
                checkBox_lift_gate_destination.Checked ? textBox_lift_gate_destination.Text : "",
                checkBox_limited_delivery_or_military.Checked ? textBox_limited_delivery_or_military.Text : "",
                checkBox_notification.Checked ? textBox_notification.Text : "",
                checkBox_protective.Checked ? textBox_protective.Text : "",
                checkBox_residential_pickup.Checked ? textBox_residential_pickup.Text : "",
                checkBox_stop_off.Checked ? textBox_stop_off.Text : "",
                checkBox_team_required.Checked ? textBox_team_required.Text : "",
                checkBox_live_loading_or_unloading.Checked ? textBox_live_loading_or_unloading.Text : "",
                checkBox_appointment.Checked ? textBox_appointment.Text : "",
                checkBox_bag_liner.Checked ? textBox_bag_liner.Text : "",
                checkBox_bobtail.Checked ? textBox_bobtail.Text : "",
                checkBox_commercial_pickup.Checked ? textBox_commercial_pickup.Text : "",
                checkBox_cross_dock.Checked ? textBox_cross_dock.Text : "",
                checkBox_driver_load_and_unload.Checked ? textBox_driver_load_and_unload.Text : "",
                checkBox_driver_load.Checked ? textBox_driver_load.Text : "",
                checkBox_dry_run.Checked ? textBox_dry_run.Text : "",
                checkBox_emergency_delivery.Checked ? textBox_emergency_delivery.Text : "",
                checkBox_expedited_shipment.Checked ? textBox_expedited_shipment.Text : "",
                checkBox_guaranteed.Checked ? textBox_guaranteed.Text : "",
                checkBox_holiday_delivery.Checked ? textBox_holiday_delivery.Text : "",
                checkBox_inside_pickup.Checked ? textBox_inside_pickup.Text : "",
                checkBox_lift.Checked ? textBox_lift.Text : "",
                checkBox_lift_gate_origin.Checked ? textBox_lift_gate_origin.Text : "",
                checkBox_liquor_permit.Checked ? textBox_liquor_permit.Text : "",
                checkBox_marking_and_tagging.Checked ? textBox_marking_and_tagging.Text : "",
                checkBox_premium_air_freight.Checked ? textBox_premium_air_freight.Text : "",
                checkBox_pallet.Checked ? textBox_pallet.Text : "",
                checkBox_residential_delivery.Checked ? textBox_residential_delivery.Text : "",
                checkBox_saturday_pickup_or_delivery.Checked ? textBox_saturday_pickup_or_delivery.Text : "",
                checkBox_special_equipment_needed.Checked ? textBox_special_equipment_needed.Text : "",
                checkBox_tradeshow.Checked ? textBox_tradeshow.Text : "",
                txt_scac.Text);
            Data.CarriersRow ror = data_temp.Carriers.FindByCode("999");
            if (ror != null)
            {
                data_temp.Carriers.Columns["IsLTL"].ReadOnly = false;
                ror.IsLTL = radiob_ltl.Checked;
                data_temp.Carriers.Columns["IsLTL"].ReadOnly = true;
            }
            this.Close();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radiob_ltl_CheckedChanged(object sender, EventArgs e)
        {
            if (radiob_ltl.Checked)
            {
                int y = panel_special_services.Location.Y - link_label_special_services.Location.Y;
                label_billing_account.Location = new Point(label_billing_account.Location.X, label_billing_account.Location.Y + y);
                txt_billing_account.Location = new Point(txt_billing_account.Location.X, txt_billing_account.Location.Y + y);
                label_carrier_name.Location = new Point(label_carrier_name.Location.X, label_carrier_name.Location.Y + y);
                cb_carrier_name.Location = new Point(cb_carrier_name.Location.X, cb_carrier_name.Location.Y + y);
                label_service_name.Location = new Point(label_service_name.Location.X, label_service_name.Location.Y + y);
                txt_service_name.Location = new Point(txt_service_name.Location.X, txt_service_name.Location.Y + y);
                label_scac.Location = new Point(label_scac.Location.X, label_scac.Location.Y + y);
                txt_scac.Location = new Point(txt_scac.Location.X, txt_scac.Location.Y + y);
                label_packaging.Location = new Point(label_packaging.Location.X, label_packaging.Location.Y + y);
                txt_packaging.Location = new Point(txt_packaging.Location.X, txt_packaging.Location.Y + y);
                label_list_tarrif.Location = new Point(label_list_tarrif.Location.X, label_list_tarrif.Location.Y + y);
                txt_list_tarrif.Location = new Point(txt_list_tarrif.Location.X, txt_list_tarrif.Location.Y + y);
                label_discount.Location = new Point(label_discount.Location.X, label_discount.Location.Y + y);
                txt_discount.Location = new Point(txt_discount.Location.X, txt_discount.Location.Y + y);
                label_discount_type.Location = new Point(label_discount_type.Location.X, label_discount_type.Location.Y + y);
                groupbox_discount_type.Location = new Point(groupbox_discount_type.Location.X, groupbox_discount_type.Location.Y + y);
                label_fuel.Location = new Point(label_fuel.Location.X, label_fuel.Location.Y + y);
                txt_fuel.Location = new Point(txt_fuel.Location.X, txt_fuel.Location.Y + y);
                label_fuel_type.Location = new Point(label_fuel_type.Location.X, label_fuel_type.Location.Y + y);
                groupbox_fuel_type.Location = new Point(groupbox_fuel_type.Location.X, groupbox_fuel_type.Location.Y + y);
                label_total_other_surcharges.Location = new Point(label_total_other_surcharges.Location.X, label_total_other_surcharges.Location.Y + y);
                txt_total_other_surcharges.Location = new Point(txt_total_other_surcharges.Location.X, txt_total_other_surcharges.Location.Y + y);
                label_bar_code_symbology.Location = new Point(label_bar_code_symbology.Location.X, label_bar_code_symbology.Location.Y + y);
                cb_bare_code_symbology.Location = new Point(cb_bare_code_symbology.Location.X, cb_bare_code_symbology.Location.Y + y);
                checkbox_reuse.Location = new Point(checkbox_reuse.Location.X, checkbox_reuse.Location.Y + y);
                btn_ok.Location = new Point(btn_ok.Location.X, btn_ok.Location.Y + y);
                btn_cancel.Location = new Point(btn_cancel.Location.X, btn_cancel.Location.Y + y);
                label_bottom.Location = new Point(label_bottom.Location.X, label_bottom.Location.Y + y);
            }
            else
            {
                int y;
                if (panel_special_services.Visible)
                {
                    y = panel_special_services.Location.Y - link_label_special_services.Location.Y + panel_special_services.Height + 15;
                }
                else
                {
                    y = panel_special_services.Location.Y - link_label_special_services.Location.Y;
                }
                if (first)
                {
                    first = false;
                    y -= 5;
                }
                label_billing_account.Location = new Point(label_billing_account.Location.X, label_billing_account.Location.Y - y);
                txt_billing_account.Location = new Point(txt_billing_account.Location.X, txt_billing_account.Location.Y - y);
                label_carrier_name.Location = new Point(label_carrier_name.Location.X, label_carrier_name.Location.Y - y);
                cb_carrier_name.Location = new Point(cb_carrier_name.Location.X, cb_carrier_name.Location.Y - y);
                label_service_name.Location = new Point(label_service_name.Location.X, label_service_name.Location.Y - y);
                txt_service_name.Location = new Point(txt_service_name.Location.X, txt_service_name.Location.Y - y);
                label_scac.Location = new Point(label_scac.Location.X, label_scac.Location.Y - y);
                txt_scac.Location = new Point(txt_scac.Location.X, txt_scac.Location.Y - y);
                label_packaging.Location = new Point(label_packaging.Location.X, label_packaging.Location.Y - y);
                txt_packaging.Location = new Point(txt_packaging.Location.X, txt_packaging.Location.Y - y);
                label_list_tarrif.Location = new Point(label_list_tarrif.Location.X, label_list_tarrif.Location.Y - y);
                txt_list_tarrif.Location = new Point(txt_list_tarrif.Location.X, txt_list_tarrif.Location.Y - y);
                label_discount.Location = new Point(label_discount.Location.X, label_discount.Location.Y - y);
                txt_discount.Location = new Point(txt_discount.Location.X, txt_discount.Location.Y - y);
                label_discount_type.Location = new Point(label_discount_type.Location.X, label_discount_type.Location.Y - y);
                groupbox_discount_type.Location = new Point(groupbox_discount_type.Location.X, groupbox_discount_type.Location.Y - y);
                label_fuel.Location = new Point(label_fuel.Location.X, label_fuel.Location.Y - y);
                txt_fuel.Location = new Point(txt_fuel.Location.X, txt_fuel.Location.Y - y);
                label_fuel_type.Location = new Point(label_fuel_type.Location.X, label_fuel_type.Location.Y - y);
                groupbox_fuel_type.Location = new Point(groupbox_fuel_type.Location.X, groupbox_fuel_type.Location.Y - y);
                label_total_other_surcharges.Location = new Point(label_total_other_surcharges.Location.X, label_total_other_surcharges.Location.Y - y);
                txt_total_other_surcharges.Location = new Point(txt_total_other_surcharges.Location.X, txt_total_other_surcharges.Location.Y - y);
                label_bar_code_symbology.Location = new Point(label_bar_code_symbology.Location.X, label_bar_code_symbology.Location.Y - y);
                cb_bare_code_symbology.Location = new Point(cb_bare_code_symbology.Location.X, cb_bare_code_symbology.Location.Y - y);
                checkbox_reuse.Location = new Point(checkbox_reuse.Location.X, checkbox_reuse.Location.Y - y);
                btn_ok.Location = new Point(btn_ok.Location.X, btn_ok.Location.Y - y);
                btn_cancel.Location = new Point(btn_cancel.Location.X, btn_cancel.Location.Y - y);
                label_bottom.Location = new Point(label_bottom.Location.X, label_bottom.Location.Y - y);
            }
            link_label_special_services.Visible = radiob_ltl.Checked;
            panel_special_services.Visible = false;
            label_pro_number.Visible = radiob_ltl.Checked;
            txt_pro_number.Visible = radiob_ltl.Checked;
            label_tracking_number.Visible = !radiob_ltl.Checked;
            txt_tacking_number.Visible = !radiob_ltl.Checked;
            this.Refresh();
        }

        private void link_label_special_services_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!panel_special_services.Visible)
            {
                panel_special_services.Visible = true;
                int y = panel_special_services.Height + 15;
                label_billing_account.Location = new Point(label_billing_account.Location.X, label_billing_account.Location.Y + y);
                txt_billing_account.Location = new Point(txt_billing_account.Location.X, txt_billing_account.Location.Y + y);
                label_carrier_name.Location = new Point(label_carrier_name.Location.X, label_carrier_name.Location.Y + y);
                cb_carrier_name.Location = new Point(cb_carrier_name.Location.X, cb_carrier_name.Location.Y + y);
                label_service_name.Location = new Point(label_service_name.Location.X, label_service_name.Location.Y + y);
                txt_service_name.Location = new Point(txt_service_name.Location.X, txt_service_name.Location.Y + y);
                label_scac.Location = new Point(label_scac.Location.X, label_scac.Location.Y + y);
                txt_scac.Location = new Point(txt_scac.Location.X, txt_scac.Location.Y + y);
                label_packaging.Location = new Point(label_packaging.Location.X, label_packaging.Location.Y + y);
                txt_packaging.Location = new Point(txt_packaging.Location.X, txt_packaging.Location.Y + y);
                label_list_tarrif.Location = new Point(label_list_tarrif.Location.X, label_list_tarrif.Location.Y + y);
                txt_list_tarrif.Location = new Point(txt_list_tarrif.Location.X, txt_list_tarrif.Location.Y + y);
                label_discount.Location = new Point(label_discount.Location.X, label_discount.Location.Y + y);
                txt_discount.Location = new Point(txt_discount.Location.X, txt_discount.Location.Y + y);
                label_discount_type.Location = new Point(label_discount_type.Location.X, label_discount_type.Location.Y + y);
                groupbox_discount_type.Location = new Point(groupbox_discount_type.Location.X, groupbox_discount_type.Location.Y + y);
                label_fuel.Location = new Point(label_fuel.Location.X, label_fuel.Location.Y + y);
                txt_fuel.Location = new Point(txt_fuel.Location.X, txt_fuel.Location.Y + y);
                label_fuel_type.Location = new Point(label_fuel_type.Location.X, label_fuel_type.Location.Y + y);
                groupbox_fuel_type.Location = new Point(groupbox_fuel_type.Location.X, groupbox_fuel_type.Location.Y + y);
                label_total_other_surcharges.Location = new Point(label_total_other_surcharges.Location.X, label_total_other_surcharges.Location.Y + y);
                txt_total_other_surcharges.Location = new Point(txt_total_other_surcharges.Location.X, txt_total_other_surcharges.Location.Y + y);
                label_bar_code_symbology.Location = new Point(label_bar_code_symbology.Location.X, label_bar_code_symbology.Location.Y + y);
                cb_bare_code_symbology.Location = new Point(cb_bare_code_symbology.Location.X, cb_bare_code_symbology.Location.Y + y);
                checkbox_reuse.Location = new Point(checkbox_reuse.Location.X, checkbox_reuse.Location.Y + y);
                btn_ok.Location = new Point(btn_ok.Location.X, btn_ok.Location.Y + y);
                btn_cancel.Location = new Point(btn_cancel.Location.X, btn_cancel.Location.Y + y);
                label_bottom.Location = new Point(label_bottom.Location.X, label_bottom.Location.Y + y);
            }
            else
            {
                int y = panel_special_services.Height + 15;
                label_billing_account.Location = new Point(label_billing_account.Location.X, label_billing_account.Location.Y - y);
                txt_billing_account.Location = new Point(txt_billing_account.Location.X, txt_billing_account.Location.Y - y);
                label_carrier_name.Location = new Point(label_carrier_name.Location.X, label_carrier_name.Location.Y - y);
                cb_carrier_name.Location = new Point(cb_carrier_name.Location.X, cb_carrier_name.Location.Y - y);
                label_service_name.Location = new Point(label_service_name.Location.X, label_service_name.Location.Y - y);
                txt_service_name.Location = new Point(txt_service_name.Location.X, txt_service_name.Location.Y - y);
                label_scac.Location = new Point(label_scac.Location.X, label_scac.Location.Y - y);
                txt_scac.Location = new Point(txt_scac.Location.X, txt_scac.Location.Y - y);
                label_packaging.Location = new Point(label_packaging.Location.X, label_packaging.Location.Y - y);
                txt_packaging.Location = new Point(txt_packaging.Location.X, txt_packaging.Location.Y - y);
                label_list_tarrif.Location = new Point(label_list_tarrif.Location.X, label_list_tarrif.Location.Y - y);
                txt_list_tarrif.Location = new Point(txt_list_tarrif.Location.X, txt_list_tarrif.Location.Y - y);
                label_discount.Location = new Point(label_discount.Location.X, label_discount.Location.Y - y);
                txt_discount.Location = new Point(txt_discount.Location.X, txt_discount.Location.Y - y);
                label_discount_type.Location = new Point(label_discount_type.Location.X, label_discount_type.Location.Y - y);
                groupbox_discount_type.Location = new Point(groupbox_discount_type.Location.X, groupbox_discount_type.Location.Y - y);
                label_fuel.Location = new Point(label_fuel.Location.X, label_fuel.Location.Y - y);
                txt_fuel.Location = new Point(txt_fuel.Location.X, txt_fuel.Location.Y - y);
                label_fuel_type.Location = new Point(label_fuel_type.Location.X, label_fuel_type.Location.Y - y);
                groupbox_fuel_type.Location = new Point(groupbox_fuel_type.Location.X, groupbox_fuel_type.Location.Y - y);
                label_total_other_surcharges.Location = new Point(label_total_other_surcharges.Location.X, label_total_other_surcharges.Location.Y - y);
                txt_total_other_surcharges.Location = new Point(txt_total_other_surcharges.Location.X, txt_total_other_surcharges.Location.Y - y);
                label_bar_code_symbology.Location = new Point(label_bar_code_symbology.Location.X, label_bar_code_symbology.Location.Y - y);
                cb_bare_code_symbology.Location = new Point(cb_bare_code_symbology.Location.X, cb_bare_code_symbology.Location.Y - y);
                checkbox_reuse.Location = new Point(checkbox_reuse.Location.X, checkbox_reuse.Location.Y - y);
                btn_ok.Location = new Point(btn_ok.Location.X, btn_ok.Location.Y - y);
                btn_cancel.Location = new Point(btn_cancel.Location.X, btn_cancel.Location.Y - y);
                label_bottom.Location = new Point(label_bottom.Location.X, label_bottom.Location.Y - y);
                panel_special_services.Visible = false;
            }
            this.Refresh();
        }

        private void cb_carrier_name_SelectedIndexChanged(object sender, EventArgs e)
        {
            Data.MyCarrierNamesRow[] carrrier = InteGr8_Main.data.MyCarrierNames.Select("Name = '" + cb_carrier_name.Text + "'") as Data.MyCarrierNamesRow[];
            if (carrrier.Length > 0)
            {
                txt_scac.Text = carrrier[0].SCAC;
            }
            else
            {
                txt_scac.Text = "";
            }
        }

        private void txt_scac_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txt_scac.Text.Length == 4 && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        private void txt_list_tarrif_KeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (!int.TryParse(e.KeyChar.ToString(), out i) && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        private void txt_discount_KeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (!int.TryParse(e.KeyChar.ToString(), out i) && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        private void txt_fuel_KeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (!int.TryParse(e.KeyChar.ToString(), out i) && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        private void txt_total_other_surcharges_KeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (!int.TryParse(e.KeyChar.ToString(), out i) && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }
    }
}