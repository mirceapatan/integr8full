﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmWebPopup : Form
    {
        public frmWebPopup(Form Owner)
        {
            InitializeComponent();
            this.Show(Owner);
        }

        private void Browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            this.Size = Browser.PreferredSize;
        }

        private void Browser_NewWindow2(object sender, NewWindow2EventArgs e)
        {
            frmWebPopup popup = new frmWebPopup(this);
            e.PPDisp = popup.Browser.Application;
        }
    }
}
