﻿namespace InteGr8
{
    partial class MultiPack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultiPack));
            this.tbPacks = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbTotalWeight = new System.Windows.Forms.TextBox();
            this.pDims = new System.Windows.Forms.Panel();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.tbWidth = new System.Windows.Forms.TextBox();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbDims = new System.Windows.Forms.CheckBox();
            this.ntnAddMulti = new System.Windows.Forms.Button();
            this.pDims.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbPacks
            // 
            this.tbPacks.Location = new System.Drawing.Point(227, 25);
            this.tbPacks.Name = "tbPacks";
            this.tbPacks.Size = new System.Drawing.Size(100, 20);
            this.tbPacks.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "No. of identical packages to add";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Total weight";
            // 
            // tbTotalWeight
            // 
            this.tbTotalWeight.Location = new System.Drawing.Point(227, 56);
            this.tbTotalWeight.Name = "tbTotalWeight";
            this.tbTotalWeight.Size = new System.Drawing.Size(100, 20);
            this.tbTotalWeight.TabIndex = 3;
            // 
            // pDims
            // 
            this.pDims.Controls.Add(this.tbHeight);
            this.pDims.Controls.Add(this.tbWidth);
            this.pDims.Controls.Add(this.tbLength);
            this.pDims.Controls.Add(this.label5);
            this.pDims.Controls.Add(this.label4);
            this.pDims.Controls.Add(this.label3);
            this.pDims.Location = new System.Drawing.Point(12, 106);
            this.pDims.Name = "pDims";
            this.pDims.Size = new System.Drawing.Size(315, 111);
            this.pDims.TabIndex = 4;
            // 
            // tbHeight
            // 
            this.tbHeight.Enabled = false;
            this.tbHeight.Location = new System.Drawing.Point(122, 71);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(100, 20);
            this.tbHeight.TabIndex = 5;
            // 
            // tbWidth
            // 
            this.tbWidth.Enabled = false;
            this.tbWidth.Location = new System.Drawing.Point(122, 43);
            this.tbWidth.Name = "tbWidth";
            this.tbWidth.Size = new System.Drawing.Size(100, 20);
            this.tbWidth.TabIndex = 4;
            // 
            // tbLength
            // 
            this.tbLength.Enabled = false;
            this.tbLength.Location = new System.Drawing.Point(122, 14);
            this.tbLength.Name = "tbLength";
            this.tbLength.Size = new System.Drawing.Size(100, 20);
            this.tbLength.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Height";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Width";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Length";
            // 
            // cbDims
            // 
            this.cbDims.AutoSize = true;
            this.cbDims.Location = new System.Drawing.Point(37, 83);
            this.cbDims.Name = "cbDims";
            this.cbDims.Size = new System.Drawing.Size(80, 17);
            this.cbDims.TabIndex = 5;
            this.cbDims.Text = "Dimensions";
            this.cbDims.UseVisualStyleBackColor = true;
            this.cbDims.CheckedChanged += new System.EventHandler(this.cbDims_CheckedChanged);
            // 
            // ntnAddMulti
            // 
            this.ntnAddMulti.Location = new System.Drawing.Point(251, 224);
            this.ntnAddMulti.Name = "ntnAddMulti";
            this.ntnAddMulti.Size = new System.Drawing.Size(75, 23);
            this.ntnAddMulti.TabIndex = 6;
            this.ntnAddMulti.Text = "Add";
            this.ntnAddMulti.UseVisualStyleBackColor = true;
            this.ntnAddMulti.Click += new System.EventHandler(this.ntnAddMulti_Click);
            // 
            // MultiPack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 257);
            this.Controls.Add(this.ntnAddMulti);
            this.Controls.Add(this.cbDims);
            this.Controls.Add(this.pDims);
            this.Controls.Add(this.tbTotalWeight);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbPacks);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MultiPack";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MultiPackAdd";
            this.Load += new System.EventHandler(this.MultiPack_Load);
            this.pDims.ResumeLayout(false);
            this.pDims.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPacks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbTotalWeight;
        private System.Windows.Forms.Panel pDims;
        private System.Windows.Forms.TextBox tbHeight;
        private System.Windows.Forms.TextBox tbWidth;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbDims;
        private System.Windows.Forms.Button ntnAddMulti;
    }
}