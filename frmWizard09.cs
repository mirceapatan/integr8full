﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard09 : Form
    {
        private bool custom_binding = false;
        private FieldLink link = null;

        public frmWizard09()
        {
            InitializeComponent();
        }

        private void frmWizard09_Load(object sender, EventArgs e)
        {
            // populate the table combobox with the ODBC catalog tables
            cbShipmentPackagesTable.Items.Add("(none)");
            using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
            {
                Conn.Open();
                foreach (DataRow row in Conn.GetSchema("Tables").Rows)
                    cbShipmentPackagesTable.Items.Add(row["TABLE_NAME"]);
            }

            // check that the link is OrdersOneToMany type and each field have the same link
            foreach (FieldBinding binding in InteGr8_Main.template.Bindings)
                if (binding.Field.FieldCategory == Data.FieldsTableDataTable.FieldCategory.ShipmentPackages && binding.Link != null)
                    if (link == null) link = binding.Link;
                    else if (!binding.Link.Equals(link)) custom_binding = true;
            if (link != null && link.Type != FieldLink.LinkType.Orders && link.Type != FieldLink.LinkType.OrdersOneToMany) custom_binding = true;
            if (custom_binding)
            {
                foreach (Control control in this.Controls)
                    if (control is ComboBox) control.Enabled = false;
                MessageBox.Show(this, "Shipment Packages configuration was customized and cannot be edited in this page. You will be able to edit in the summary page. Please click Next to skip this page.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (link == null) cbShipmentPackagesTable.SelectedIndex = 0;
            else
            {
                int index = cbShipmentPackagesTable.FindStringExact(link.FieldTable);
                if (index == -1)
                {
                    cbShipmentPackagesTable.SelectedIndex = 0;
                    MessageBox.Show(this, "Shipment Packages table '" + link.FieldTable + "' not found in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else cbShipmentPackagesTable.SelectedIndex = index;
                if (link.Type != FieldLink.LinkType.Orders && InteGr8_Main.template.ShipmentPackagesParent != null)
                {
                }
                if (link.Type != FieldLink.LinkType.Orders && link.OrdersTableFieldFK != null)
                {
                    cbPackagesShipmentIdColumn.SelectedIndex = cbPackagesShipmentIdColumn.FindStringExact(link.OrdersTableFieldFK);
                    if (cbPackagesShipmentIdColumn.SelectedIndex == -1)
                    {
                        MessageBox.Show(this, "Shipment Packages table foreign key column '" + link.OrdersTableFieldFK + "' not found in the Shipment Packages table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                if (link.Type != FieldLink.LinkType.Orders && InteGr8_Main.template.ShipmentPackagesPackageNumber != null)
                {
                    cbPackagesPackageNumberColumn.SelectedIndex = cbPackagesPackageNumberColumn.FindStringExact(InteGr8_Main.template.ShipmentPackagesPackageNumber);
                    if (cbPackagesPackageNumberColumn.SelectedIndex == -1)
                    {
                        MessageBox.Show(this, "Shipment Packages table Package # primary key column '" + InteGr8_Main.template.ShipmentPackagesPackageNumber + "' not found in the Shipment Packages table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
            if (InteGr8_Main.template.Find("Shipment Tracking Number") != null) cbPackagesIdColumn.Text = InteGr8_Main.template.Find("Shipment Tracking Number").Value;
            if (InteGr8_Main.template.Find("Client Final Charge") != null) cbPrice.Text = InteGr8_Main.template.Find("Client Final Charge").Value;
            if (InteGr8_Main.template.Find("Billed Weight") != null) cbBilledWeight.Text = InteGr8_Main.template.Find("Billed Weight").Value;
        }

        private void cbShipmentPackagesTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // clear all column comboboxes
            cbPackagesIdColumn.Items.Clear();
            cbPackagesShipmentIdColumn.Items.Clear();
            cbPackagesOrderNumberColumn.Items.Clear();
            cbPackagesPackageNumberColumn.Items.Clear();

            cbPrice.Items.Clear();
            cbBilledWeight.Items.Clear();

            bool link = cbShipmentPackagesTable.SelectedIndex > 0;
            cbPackagesIdColumn.Enabled = link;
            cbPackagesShipmentIdColumn.Enabled = link;
            cbPackagesOrderNumberColumn.Enabled = link;
            cbPackagesPackageNumberColumn.Enabled = link;

            cbPackagesIdColumn.SelectedIndex = -1;
            cbPackagesShipmentIdColumn.SelectedIndex = -1;
            cbPackagesOrderNumberColumn.SelectedIndex = -1;
            cbPackagesPackageNumberColumn.SelectedIndex = -1;

            // read the table columns and populate all the column comboboxes
            if (cbShipmentPackagesTable.SelectedIndex > 0)
            {
                string[] columns = null;
                using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
                {
                    Conn.Open();
                    using (OdbcDataReader dr = new OdbcCommand("Select Top 0 * From " + cbShipmentPackagesTable.Text, Conn).ExecuteReader(CommandBehavior.SingleRow))
                    {
                        columns = new string[dr.FieldCount];
                        for (int k = 0; k < dr.FieldCount; k++)
                            columns[k] = "[" + dr.GetName(k) + "]";
                    }
                }

                if (columns != null)
                {
                    cbPackagesIdColumn.Items.AddRange(columns);
                    cbPackagesShipmentIdColumn.Items.AddRange(columns);
                    cbPackagesOrderNumberColumn.Items.AddRange(columns);
                    cbPackagesPackageNumberColumn.Items.AddRange(columns);

                    cbPrice.Items.AddRange(columns);
                    cbBilledWeight.Items.AddRange(columns);
                }
            }
        }

        private void frmWizard09_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK) return;
            if (DialogResult == DialogResult.No)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            DialogResult = DialogResult.Abort;
        }

        private void frmWizard09_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_09");
            e.Cancel = true;
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (cbShipmentPackagesTable.SelectedIndex <= 0 || cbShipmentPackagesTable.Text.Trim().Equals("")) return;
            new frmWizard_DataPreview("Shipment Packages", InteGr8_Main.template.Connection, "Select Top 100 * From " + cbShipmentPackagesTable.Text).ShowDialog(this);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (cbPackagesIdColumn.Enabled && cbPackagesIdColumn.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please select the Packages Id column.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            bool shipment_id = cbPackagesShipmentIdColumn.Enabled && cbPackagesShipmentIdColumn.SelectedIndex != -1;
            bool order_number = cbPackagesOrderNumberColumn.Enabled && cbPackagesOrderNumberColumn.SelectedIndex != -1;
            bool package_number = cbPackagesPackageNumberColumn.Enabled && cbPackagesPackageNumberColumn.SelectedIndex != -1;
            if (!shipment_id && !order_number && !package_number)
            {
                MessageBox.Show(this, "You must select at least one table link.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // validate
            // TODO: check PK & FKs

            if (!custom_binding)
            {
                if (cbShipmentPackagesTable.SelectedIndex == 0) link = null;
                else if (cbShipmentPackagesTable.Text.Equals(InteGr8_Main.template.Orders))
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.Orders;
                    link.FieldTable = cbShipmentPackagesTable.Text;
                }
                else
                {
                    if (link == null) link = new FieldLink();
                    link.Type = FieldLink.LinkType.OrdersOneToMany;
                    link.FieldTable = cbShipmentPackagesTable.Text;
                    link.FieldTablePK = cbPackagesIdColumn.Text;
                    link.FieldTableOrderNumber = shipment_id ? cbPackagesShipmentIdColumn.Text : package_number ? cbPackagesPackageNumberColumn.Text : cbPackagesOrderNumberColumn.Text;
                }

                InteGr8_Main.template.ShipmentPackagesTable = cbShipmentPackagesTable.Text;
                InteGr8_Main.template.ShipmentPackagesTablePackageIdPKColumn = cbPackagesIdColumn.Text;
                InteGr8_Main.template.ShipmentPackagesTablePackageIdPKColumn = cbPackagesIdColumn.Text;
                InteGr8_Main.template.ShipmentPackagesTablePackageIdPKColumn = cbPackagesIdColumn.Text;

                InteGr8_Main.template.ShipmentPackagesPackageNumber = cbPackagesPackageNumberColumn.Text;
                InteGr8_Main.template.UpdateBinding("Shipment Tracking Number", cbPackagesIdColumn.FindStringExact(cbPackagesIdColumn.Text) == -1, cbPackagesIdColumn.Text.Trim().Equals("") ? null : cbPackagesIdColumn.Text, link);
                
                InteGr8_Main.template.UpdateBinding("Client Final Charge", cbPrice.FindStringExact(cbPrice.Text) == -1, cbPrice.Text.Trim().Equals("") ? null : cbPrice.Text, link);
                InteGr8_Main.template.UpdateBinding("Billed Weight", cbBilledWeight.FindStringExact(cbBilledWeight.Text) == -1, cbBilledWeight.Text.Trim().Equals("") ? null : cbBilledWeight.Text, link);
            }
            DialogResult = DialogResult.OK;
        }
    }
}
