﻿namespace InteGr8
{
    partial class frmWizard_Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard_Options));
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblHeaderLine = new System.Windows.Forms.Label();
            this.llAdvanced = new System.Windows.Forms.LinkLabel();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.lblHeaderIcon = new System.Windows.Forms.Label();
            this.lblHeaderInfo = new System.Windows.Forms.Label();
            this.HelpToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.rbInternationalYes = new System.Windows.Forms.RadioButton();
            this.rbShipmentsNo = new System.Windows.Forms.RadioButton();
            this.rbShipmentsYes = new System.Windows.Forms.RadioButton();
            this.rbMPSYes = new System.Windows.Forms.RadioButton();
            this.rbMPSPackageLevel = new System.Windows.Forms.RadioButton();
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.pnlInternational = new System.Windows.Forms.Panel();
            this.rbInternationalNo = new System.Windows.Forms.RadioButton();
            this.lblInternational = new System.Windows.Forms.Label();
            this.pnlMPS = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbMPSShipmentLevel = new System.Windows.Forms.RadioButton();
            this.lblMPSLevel = new System.Windows.Forms.Label();
            this.rbMPSNo = new System.Windows.Forms.RadioButton();
            this.lblMPS = new System.Windows.Forms.Label();
            this.pnlShipments = new System.Windows.Forms.Panel();
            this.lblShipments = new System.Windows.Forms.Label();
            this.pnlHeader.SuspendLayout();
            this.pnlInternational.SuspendLayout();
            this.pnlMPS.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlShipments.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPrev.Location = new System.Drawing.Point(296, 380);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 9;
            this.btnPrev.Text = "< Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(377, 380);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 8;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblHeaderLine
            // 
            this.lblHeaderLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderLine.BackColor = System.Drawing.SystemColors.Control;
            this.lblHeaderLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeaderLine.Location = new System.Drawing.Point(0, 86);
            this.lblHeaderLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderLine.Name = "lblHeaderLine";
            this.lblHeaderLine.Size = new System.Drawing.Size(464, 2);
            this.lblHeaderLine.TabIndex = 116;
            // 
            // llAdvanced
            // 
            this.llAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdvanced.Image = global::InteGr8.Properties.Resources.wrench;
            this.llAdvanced.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llAdvanced.LinkArea = new System.Windows.Forms.LinkArea(6, 13);
            this.llAdvanced.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdvanced.Location = new System.Drawing.Point(360, 0);
            this.llAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.llAdvanced.Name = "llAdvanced";
            this.llAdvanced.Size = new System.Drawing.Size(104, 24);
            this.llAdvanced.TabIndex = 119;
            this.llAdvanced.TabStop = true;
            this.llAdvanced.Text = "      advanced mode";
            this.llAdvanced.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llAdvanced.UseCompatibleTextRendering = true;
            this.llAdvanced.Visible = false;
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.lblHeaderLine);
            this.pnlHeader.Controls.Add(this.llAdvanced);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.lblHeaderIcon);
            this.pnlHeader.Controls.Add(this.lblHeaderInfo);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(464, 88);
            this.pnlHeader.TabIndex = 122;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(360, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Choose Shipping Scenario";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeaderIcon
            // 
            this.lblHeaderIcon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeaderIcon.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaderIcon.Image = global::InteGr8.Properties.Resources.data_next;
            this.lblHeaderIcon.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeaderIcon.Location = new System.Drawing.Point(0, 24);
            this.lblHeaderIcon.Name = "lblHeaderIcon";
            this.lblHeaderIcon.Padding = new System.Windows.Forms.Padding(5);
            this.lblHeaderIcon.Size = new System.Drawing.Size(64, 62);
            this.lblHeaderIcon.TabIndex = 115;
            // 
            // lblHeaderInfo
            // 
            this.lblHeaderInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderInfo.AutoEllipsis = true;
            this.lblHeaderInfo.Location = new System.Drawing.Point(68, 24);
            this.lblHeaderInfo.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderInfo.Name = "lblHeaderInfo";
            this.lblHeaderInfo.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.lblHeaderInfo.Size = new System.Drawing.Size(396, 62);
            this.lblHeaderInfo.TabIndex = 114;
            this.lblHeaderInfo.Text = "Please anwer the following questions, to configure the wizard for your specific s" +
                "hipping requirements.";
            // 
            // rbInternationalYes
            // 
            this.rbInternationalYes.AutoSize = true;
            this.rbInternationalYes.Checked = true;
            this.rbInternationalYes.Location = new System.Drawing.Point(28, 20);
            this.rbInternationalYes.Name = "rbInternationalYes";
            this.rbInternationalYes.Size = new System.Drawing.Size(43, 17);
            this.rbInternationalYes.TabIndex = 0;
            this.rbInternationalYes.TabStop = true;
            this.rbInternationalYes.Text = "Yes";
            this.HelpToolTip.SetToolTip(this.rbInternationalYes, "You will have to map documents / commodities fields for customs");
            this.rbInternationalYes.UseVisualStyleBackColor = true;
            // 
            // rbShipmentsNo
            // 
            this.rbShipmentsNo.AutoSize = true;
            this.rbShipmentsNo.Location = new System.Drawing.Point(30, 40);
            this.rbShipmentsNo.Name = "rbShipmentsNo";
            this.rbShipmentsNo.Size = new System.Drawing.Size(39, 17);
            this.rbShipmentsNo.TabIndex = 7;
            this.rbShipmentsNo.Text = "No";
            this.HelpToolTip.SetToolTip(this.rbShipmentsNo, "You will not be able automatically filter unprocessed orders");
            this.rbShipmentsNo.UseVisualStyleBackColor = true;
            // 
            // rbShipmentsYes
            // 
            this.rbShipmentsYes.AutoSize = true;
            this.rbShipmentsYes.Checked = true;
            this.rbShipmentsYes.Location = new System.Drawing.Point(30, 18);
            this.rbShipmentsYes.Name = "rbShipmentsYes";
            this.rbShipmentsYes.Size = new System.Drawing.Size(43, 17);
            this.rbShipmentsYes.TabIndex = 6;
            this.rbShipmentsYes.TabStop = true;
            this.rbShipmentsYes.Text = "Yes";
            this.HelpToolTip.SetToolTip(this.rbShipmentsYes, "You need to have a Shipments table to map the result fields");
            this.rbShipmentsYes.UseVisualStyleBackColor = true;
            // 
            // rbMPSYes
            // 
            this.rbMPSYes.AutoSize = true;
            this.rbMPSYes.Checked = true;
            this.rbMPSYes.Location = new System.Drawing.Point(30, 18);
            this.rbMPSYes.Name = "rbMPSYes";
            this.rbMPSYes.Size = new System.Drawing.Size(43, 17);
            this.rbMPSYes.TabIndex = 2;
            this.rbMPSYes.TabStop = true;
            this.rbMPSYes.Text = "Yes";
            this.rbMPSYes.UseVisualStyleBackColor = true;
            this.rbMPSYes.CheckedChanged += new System.EventHandler(this.rbMPSYes_CheckedChanged);
            // 
            // rbMPSPackageLevel
            // 
            this.rbMPSPackageLevel.AutoSize = true;
            this.rbMPSPackageLevel.Checked = true;
            this.rbMPSPackageLevel.Location = new System.Drawing.Point(3, 3);
            this.rbMPSPackageLevel.Name = "rbMPSPackageLevel";
            this.rbMPSPackageLevel.Size = new System.Drawing.Size(230, 17);
            this.rbMPSPackageLevel.TabIndex = 3;
            this.rbMPSPackageLevel.TabStop = true;
            this.rbMPSPackageLevel.Text = "Individual package weights and dimensions";
            this.rbMPSPackageLevel.UseVisualStyleBackColor = true;
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(12, 369);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(440, 2);
            this.lblFooterLine.TabIndex = 137;
            // 
            // pnlInternational
            // 
            this.pnlInternational.Controls.Add(this.rbInternationalNo);
            this.pnlInternational.Controls.Add(this.rbInternationalYes);
            this.pnlInternational.Controls.Add(this.lblInternational);
            this.pnlInternational.Location = new System.Drawing.Point(12, 94);
            this.pnlInternational.Name = "pnlInternational";
            this.pnlInternational.Size = new System.Drawing.Size(440, 69);
            this.pnlInternational.TabIndex = 138;
            // 
            // rbInternationalNo
            // 
            this.rbInternationalNo.AutoSize = true;
            this.rbInternationalNo.Location = new System.Drawing.Point(28, 42);
            this.rbInternationalNo.Name = "rbInternationalNo";
            this.rbInternationalNo.Size = new System.Drawing.Size(39, 17);
            this.rbInternationalNo.TabIndex = 1;
            this.rbInternationalNo.Text = "No";
            this.rbInternationalNo.UseVisualStyleBackColor = true;
            // 
            // lblInternational
            // 
            this.lblInternational.AutoSize = true;
            this.lblInternational.Location = new System.Drawing.Point(3, 0);
            this.lblInternational.Name = "lblInternational";
            this.lblInternational.Size = new System.Drawing.Size(136, 13);
            this.lblInternational.TabIndex = 141;
            this.lblInternational.Text = "Do you ship internationally?";
            // 
            // pnlMPS
            // 
            this.pnlMPS.Controls.Add(this.panel1);
            this.pnlMPS.Controls.Add(this.lblMPSLevel);
            this.pnlMPS.Controls.Add(this.rbMPSNo);
            this.pnlMPS.Controls.Add(this.rbMPSYes);
            this.pnlMPS.Controls.Add(this.lblMPS);
            this.pnlMPS.Location = new System.Drawing.Point(12, 169);
            this.pnlMPS.Name = "pnlMPS";
            this.pnlMPS.Size = new System.Drawing.Size(440, 122);
            this.pnlMPS.TabIndex = 139;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbMPSShipmentLevel);
            this.panel1.Controls.Add(this.rbMPSPackageLevel);
            this.panel1.Location = new System.Drawing.Point(75, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(362, 50);
            this.panel1.TabIndex = 140;
            // 
            // rbMPSShipmentLevel
            // 
            this.rbMPSShipmentLevel.AutoSize = true;
            this.rbMPSShipmentLevel.Location = new System.Drawing.Point(3, 25);
            this.rbMPSShipmentLevel.Name = "rbMPSShipmentLevel";
            this.rbMPSShipmentLevel.Size = new System.Drawing.Size(246, 17);
            this.rbMPSShipmentLevel.TabIndex = 4;
            this.rbMPSShipmentLevel.Text = "Total weight and average package dimensions";
            this.rbMPSShipmentLevel.UseVisualStyleBackColor = true;
            // 
            // lblMPSLevel
            // 
            this.lblMPSLevel.AutoSize = true;
            this.lblMPSLevel.Location = new System.Drawing.Point(78, 20);
            this.lblMPSLevel.Name = "lblMPSLevel";
            this.lblMPSLevel.Size = new System.Drawing.Size(226, 13);
            this.lblMPSLevel.TabIndex = 137;
            this.lblMPSLevel.Text = "- Choose how you enter packages information:";
            // 
            // rbMPSNo
            // 
            this.rbMPSNo.AutoSize = true;
            this.rbMPSNo.Location = new System.Drawing.Point(30, 93);
            this.rbMPSNo.Name = "rbMPSNo";
            this.rbMPSNo.Size = new System.Drawing.Size(39, 17);
            this.rbMPSNo.TabIndex = 5;
            this.rbMPSNo.Text = "No";
            this.rbMPSNo.UseVisualStyleBackColor = true;
            this.rbMPSNo.CheckedChanged += new System.EventHandler(this.rbMPSNo_CheckedChanged);
            // 
            // lblMPS
            // 
            this.lblMPS.AutoSize = true;
            this.lblMPS.Location = new System.Drawing.Point(3, 0);
            this.lblMPS.Name = "lblMPS";
            this.lblMPS.Size = new System.Drawing.Size(222, 13);
            this.lblMPS.TabIndex = 134;
            this.lblMPS.Text = "Do you ship multiple packages in a shipment?";
            // 
            // pnlShipments
            // 
            this.pnlShipments.Controls.Add(this.rbShipmentsNo);
            this.pnlShipments.Controls.Add(this.rbShipmentsYes);
            this.pnlShipments.Controls.Add(this.lblShipments);
            this.pnlShipments.Location = new System.Drawing.Point(12, 297);
            this.pnlShipments.Name = "pnlShipments";
            this.pnlShipments.Size = new System.Drawing.Size(440, 65);
            this.pnlShipments.TabIndex = 140;
            // 
            // lblShipments
            // 
            this.lblShipments.AutoSize = true;
            this.lblShipments.Location = new System.Drawing.Point(3, 0);
            this.lblShipments.Name = "lblShipments";
            this.lblShipments.Size = new System.Drawing.Size(316, 13);
            this.lblShipments.TabIndex = 137;
            this.lblShipments.Text = "Do you want to save the shipment results back to your database?";
            // 
            // frmWizard_Options
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(464, 415);
            this.Controls.Add(this.pnlShipments);
            this.Controls.Add(this.pnlMPS);
            this.Controls.Add(this.pnlInternational);
            this.Controls.Add(this.lblFooterLine);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.pnlHeader);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWizard_Options";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 2";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard_Options_HelpButtonClicked);
            this.pnlHeader.ResumeLayout(false);
            this.pnlInternational.ResumeLayout(false);
            this.pnlInternational.PerformLayout();
            this.pnlMPS.ResumeLayout(false);
            this.pnlMPS.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlShipments.ResumeLayout(false);
            this.pnlShipments.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblHeaderLine;
        private System.Windows.Forms.LinkLabel llAdvanced;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label lblHeaderIcon;
        private System.Windows.Forms.Label lblHeaderInfo;
        private System.Windows.Forms.ToolTip HelpToolTip;
        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.Panel pnlInternational;
        private System.Windows.Forms.RadioButton rbInternationalNo;
        private System.Windows.Forms.RadioButton rbInternationalYes;
        private System.Windows.Forms.Label lblInternational;
        private System.Windows.Forms.Panel pnlMPS;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbMPSShipmentLevel;
        private System.Windows.Forms.RadioButton rbMPSPackageLevel;
        private System.Windows.Forms.Label lblMPSLevel;
        private System.Windows.Forms.RadioButton rbMPSNo;
        private System.Windows.Forms.RadioButton rbMPSYes;
        private System.Windows.Forms.Label lblMPS;
        private System.Windows.Forms.Panel pnlShipments;
        private System.Windows.Forms.RadioButton rbShipmentsNo;
        private System.Windows.Forms.RadioButton rbShipmentsYes;
        private System.Windows.Forms.Label lblShipments;
    }
}