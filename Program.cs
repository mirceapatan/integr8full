﻿using InteGr8.Properties;
using InteGr8.WS2Ship;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    static class InteGr8_Main
    {
        // gloabl token initialised by the login form
        //public static Token token = null;

        public static WSKey key = null;
        // global shared dataset instance (the frmMain designer code will create this only one instance)
        public static Data data = null;

        [STAThread]
        static void Main()
        {
            try
            {

                //check if app.uses .Net Framework 4.5 or below, and class ReflectionContext is hidden in version 4.0 and lower
                //.Net Framework 4.5 is not compatible with Windows XP, so for compatibility reasons this has to be rewritten
                //set up the Thread.CurrentThread.Culture to System.Globalization.CultureInfo.InvariantCulture for each thread 
                //if (Type.GetType("System.Reflection.ReflectionContext", false) != null)
                //{
                //MessageBox.Show(System.Environment.Version.ToString(), "", MessageBoxButtons.OK);
                System.Globalization.CultureInfo.DefaultThreadCurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
                System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.InvariantCulture;
                //}              
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                string ClientID = ConfigurationManager.AppSettings["ClientID"].ToString();
                string User = ConfigurationManager.AppSettings["UserLogin"].ToString();
                string Pwd = ConfigurationManager.AppSettings["UserPWD"].ToString();

                //FootPrint:
                //if the variable from Settings, LoginToken is not empty then create token with owner credentials
                //then start running the app frmMain
                //else start login window and generate token normally
                //if (Settings.Default.LoginToken.Equals("") && Settings.Default.LoginClient.Equals(""))
                //{
                    if (!String.IsNullOrEmpty(ClientID) && !String.IsNullOrEmpty(User) && !String.IsNullOrEmpty(Pwd))
                    {
                        try
                        {
                            InteGr8_Main.key = new WSKey(Convert.ToInt32(ClientID), User, Pwd);
                            new frmMain().ShowDialog();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("An internal error has occured - an email will be sent to 2Ship support.", "InteGr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Utils.SendError("InteGr8 error", ex);
                            Application.Exit();
               
                        }
                    }
                    else 
                    if (Settings.Default.UserWSKey.Equals("") )
                    {
                        Application.Run(new frmLogin());
                    }
                    else
                    {
                        new frmMain().ShowDialog();
                    }
                //}
                //else
                //{
                //    string MasterClient = Settings.Default.LoginClient;
                //    Guid MasterToken = new Guid(Settings.Default.LoginToken);
                //    using (ShipService ws = new ShipService())
                //    {
                //        if (ws.CheckInteGr8MasterTokenIsValid(MasterClient, MasterToken))
                //        {
                //            try
                //            {
                //                //InteGr8_Main.token = null;//new Token(clientId, tokenCredentials[3], tokenCredentials[4]);
                //                new frmMain().ShowDialog();
                //            }
                //            catch (Exception ex)
                //            {
                //                MessageBox.Show("An internal error has occured - an email will be sent to 2Ship support.", "InteGr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //                Utils.SendError("InteGr8 error", ex);
                //                Application.Exit();
                //            }
                //        }
                //    }

                //    //string token = getToken(Settings.Default.LoginToken);

                //    //string[] tokenCredentials = Encryption.Decrypt(Settings.Default.Key, token).Split('|');

                //    //int clientId = Convert.ToInt32(tokenCredentials[0]);

                //    //try
                //    //{
                //    //    InteGr8_Main.token = new Token(clientId, tokenCredentials[3], tokenCredentials[4]);
                //    //    new frmMain().ShowDialog();
                //    //}
                //    //catch (Exception ex)
                //    //{
                //    //    MessageBox.Show("An internal error has occured - an email will be sent to 2Ship support.", "InteGr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    //    Utils.SendError("InteGr8 error", ex);
                //    //    Application.Exit();
                //    //}
                //}
                //Application.Run(new frmMain());
            }
            catch (Exception e)
            {
                MessageBox.Show("An internal error has occured - an email will be sent to 2Ship support.", "InteGr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utils.SendError("InteGr8 error", e);
                Application.Exit();
            }
        }

        private static string getToken(string MasterToken)
        {
            try
            {
                if (MasterToken.Length % 3 != 0) throw new Exception("Incorrect length in decrypted token.");
                char[] ascii = new char[3];
                int pos = 0;
                StringBuilder token = new StringBuilder(MasterToken.Length / 3);
                foreach (char c in MasterToken)
                {
                    if (!Char.IsDigit(c)) throw new Exception("Incorrect character in decrypted token.");
                    ascii[pos++] = c;
                    if (pos == 3)
                    {
                        token.Append(Convert.ToChar(Convert.ToByte(new string(ascii))));
                        pos = 0;
                    }
                }
                string[] fields = Encryption.Decrypt(Settings.Default.Key, token.ToString()).Split('|');
                if (fields.Length != 5) throw new Exception("Incorrect data length in decrypted token.");
                return token.ToString();
            }
            catch (Exception e)
            {
                Utils.SendError("Error parsing token from settings", e);
                throw new Exception("Unable to parse token from settings");
            }

        }
    }
}
