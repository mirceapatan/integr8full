﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmReplyOverride : Form
    {
        private readonly Data data1;
        //private readonly Data.OrdersTableRow order;

        public frmReplyOverride()
        {
            if (InteGr8_Main.data == null ) throw new ArgumentException();
            data1 = InteGr8_Main.data;
            //ShipTableBindingSource.SuspendBinding();
            //this.dgvReplyOverride.DataSource = null;
           
           //ShipTableBindingSource.ResumeBinding();
            InitializeComponent();

            this.dgvReplyOverride.DataSource = ShipTableBindingSource;
            this.ShipTableBindingSource.DataSource = this.data1;
          
        }

        private void frmReplyOverride_Load(object sender, EventArgs e)
        {
            try
            {
                foreach (DataColumn col in data1.ShipmentsTable.Columns)
                {
                    //DataGridViewColumn newColumn = new DataGridViewColumn(dgvReplyOverride.Columns[1].CellTemplate);
                    //newColumn.Name = col.ColumnName;
                    //newColumn.HeaderText = col.ColumnName;
                    //if (dgvReplyOverride.Columns.IndexOf(newColumn) < 0)
                   // {
                   //     dgvReplyOverride.Columns.Add(newColumn);
                    //}
                    
                    bool newCol = true;
                    string name = "";
                    foreach (DataGridViewColumn gridCol in dgvReplyOverride.Columns)
                    {
                        if (gridCol.DataPropertyName.Equals(col.ColumnName))
                        {
                            newCol = false;
                        }
                    }
                    if (newCol)
                    {
                         name = col.ColumnName;
                        switch(col.ColumnName)
                        {
                            case "ClientFinalChrgForShipment":
                                {
                                    name = "Client Final Charge For Shipment";
                            break;
                        }
                            case "ClientFrghtForShipment":
                                {
                                    name = "Client Freight For Shipment";
                                    break;
                                }
                            case "ClientTxsForShipment":
                                {
                                    name = "Client Taxes For Shipment";
                                    break;
                                }
                    }
                        dgvReplyOverride.Columns.Add(col.ColumnName, name);
                        dgvReplyOverride.Columns[col.ColumnName].DataPropertyName = col.ColumnName;
                    }
                }

                HideColumns();
            }
            catch (Exception ex)
            {
                Utils.SendError("Error in Rates Override load", ex);
            }

        }

        private bool columnIsVisible(string colName, out bool isEditable)
        {
            bool isVisible = false;
            isEditable = false;
            foreach (Data.ShipmentsEditRow r in data1.ShipmentsEdit)
            {
                if (r.Name.Equals(colName))
                {
                    isVisible = true;
                    if (r.AllowEdit)
                    {
                        isEditable = true;
                    }
                    return isVisible;
                }
            }
            return isVisible;
        }

        private void HideColumns()
        {
            try
            {
                bool isEditable = false;
                foreach (DataGridViewColumn col in dgvReplyOverride.Columns)
                {
                    col.Visible = false;
                    col.ReadOnly = true;
                    if (columnIsVisible(col.DataPropertyName, out isEditable))
                    {
                        col.Visible = true;
                        if (isEditable)
                        {
                            col.ReadOnly = false;
                        }
                    }
                }
               
            }
            catch (Exception ex)
            {
                throw new Exception("Error hiding unnecessary columns", ex);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {

            ShipTableBindingSource.ResetBindings(true);
            InteGr8_Main.data = this.data1;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void dgvReplyOverride_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            bool isEditable = false;
            DataRowView shipment_row = ShipTableBindingSource.Current as DataRowView;
            Data.ShipmentsTableRow currentShipment = shipment_row.Row as Data.ShipmentsTableRow;
            if (currentShipment == null)
                return;
            object order_number = currentShipment["Order #"];
            if (order_number == null || order_number.Equals(""))
                return;
            //only the first row in Shipments table will be visible in grid, so costs could be changed only on that row
            //and they must be updated on the other rows ot that shipment(if they exist, when Packages > 1) as well
            try
            {
                if (data1.ShipmentsTable.Rows.Count > 1)
                {
                    string currentOrderNumber = "";
                    for (int p = 0; p < data1.ShipmentsTable.Rows.Count; p++)
                    {
                        currentOrderNumber = data1.ShipmentsTable[p]["Order #"].ToString();
                        if (order_number.ToString().Equals(currentOrderNumber))
                        {
                            foreach (DataGridViewColumn col in dgvReplyOverride.Columns)
                            {
                                //if (col.Visible && !col.ReadOnly)
                                if (columnIsVisible(col.DataPropertyName, out isEditable))
                                {
                                    if (isEditable)
                                        data1.ShipmentsTable[p][col.DataPropertyName] = currentShipment[col.DataPropertyName];
                                }
                                else
                                {
                                    //update the price as well if it is not visible
                                    if (col.DataPropertyName.Equals("Price") && data1.ShipmentsTable.Columns.IndexOf("ClientFinalChrgForShipment") > -1)
                                    {
                                        data1.ShipmentsTable[p][col.DataPropertyName] = currentShipment["ClientFinalChrgForShipment"];
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.SendError("Error copying shipment level total costs to package level in ShipmentsEdit", ex);
            }
        }

               
    }
}
