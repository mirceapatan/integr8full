﻿namespace InteGr8
{
    partial class frmWizard04
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard04));
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.lblRecipientsTable = new System.Windows.Forms.Label();
            this.cbRecipientsTable = new System.Windows.Forms.ComboBox();
            this.lblRecipientsPrimaryKey = new System.Windows.Forms.Label();
            this.cbRecipientsPrimaryKey = new System.Windows.Forms.ComboBox();
            this.lblOrdersForeignKey = new System.Windows.Forms.Label();
            this.cbOrdersForeignKey = new System.Windows.Forms.ComboBox();
            this.lblRecipientMapping = new System.Windows.Forms.Label();
            this.lblContact = new System.Windows.Forms.Label();
            this.cbContact = new System.Windows.Forms.ComboBox();
            this.cbCompany = new System.Windows.Forms.ComboBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.cbCountry = new System.Windows.Forms.ComboBox();
            this.lblCountry = new System.Windows.Forms.Label();
            this.cbState = new System.Windows.Forms.ComboBox();
            this.lblState = new System.Windows.Forms.Label();
            this.cbCity = new System.Windows.Forms.ComboBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.cbAddress1 = new System.Windows.Forms.ComboBox();
            this.lblAddress1 = new System.Windows.Forms.Label();
            this.cbAddress2 = new System.Windows.Forms.ComboBox();
            this.lblAddress2 = new System.Windows.Forms.Label();
            this.cbPhone = new System.Windows.Forms.ComboBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.cbEmail = new System.Windows.Forms.ComboBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblContactMessage = new System.Windows.Forms.Label();
            this.lblCompanyMessage = new System.Windows.Forms.Label();
            this.lblCountryMessage = new System.Windows.Forms.Label();
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.lblSenderMappingLine = new System.Windows.Forms.Label();
            this.lblStateMessage = new System.Windows.Forms.Label();
            this.lblCityMessage = new System.Windows.Forms.Label();
            this.lblAddress1Message = new System.Windows.Forms.Label();
            this.lblAddress2Message = new System.Windows.Forms.Label();
            this.lblPhoneMessage = new System.Windows.Forms.Label();
            this.lblEmailMessage = new System.Windows.Forms.Label();
            this.lblRecipientsTableMessage = new System.Windows.Forms.Label();
            this.lblPostalMessage = new System.Windows.Forms.Label();
            this.cbPostal = new System.Windows.Forms.ComboBox();
            this.lblPostal = new System.Windows.Forms.Label();
            this.lblLinkTop = new System.Windows.Forms.Label();
            this.lblLinkBottom = new System.Windows.Forms.Label();
            this.lblLinkRight = new System.Windows.Forms.Label();
            this.lblLink = new System.Windows.Forms.Label();
            this.lblResidential = new System.Windows.Forms.Label();
            this.lblResidentialMessage = new System.Windows.Forms.Label();
            this.cbResidential = new System.Windows.Forms.ComboBox();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lblHeaderLine = new System.Windows.Forms.Label();
            this.llAdvanced = new System.Windows.Forms.LinkLabel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.lblHeaderImage = new System.Windows.Forms.Label();
            this.lblHeaderMessage = new System.Windows.Forms.Label();
            this.pnlHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(528, 556);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 17;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnPrev.Location = new System.Drawing.Point(447, 556);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 16;
            this.btnPrev.Text = "< Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(12, 556);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(108, 23);
            this.btnPreview.TabIndex = 15;
            this.btnPreview.Text = "Preview Recipients";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // lblRecipientsTable
            // 
            this.lblRecipientsTable.AutoSize = true;
            this.lblRecipientsTable.Location = new System.Drawing.Point(9, 129);
            this.lblRecipientsTable.Name = "lblRecipientsTable";
            this.lblRecipientsTable.Size = new System.Drawing.Size(86, 13);
            this.lblRecipientsTable.TabIndex = 7;
            this.lblRecipientsTable.Text = "Recipients table:";
            // 
            // cbRecipientsTable
            // 
            this.cbRecipientsTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRecipientsTable.FormattingEnabled = true;
            this.cbRecipientsTable.Location = new System.Drawing.Point(164, 126);
            this.cbRecipientsTable.Name = "cbRecipientsTable";
            this.cbRecipientsTable.Size = new System.Drawing.Size(174, 21);
            this.cbRecipientsTable.TabIndex = 1;
            this.cbRecipientsTable.SelectedIndexChanged += new System.EventHandler(this.cbRecipientsTable_SelectedIndexChanged);
            // 
            // lblRecipientsPrimaryKey
            // 
            this.lblRecipientsPrimaryKey.AutoSize = true;
            this.lblRecipientsPrimaryKey.Location = new System.Drawing.Point(9, 156);
            this.lblRecipientsPrimaryKey.Name = "lblRecipientsPrimaryKey";
            this.lblRecipientsPrimaryKey.Size = new System.Drawing.Size(153, 13);
            this.lblRecipientsPrimaryKey.TabIndex = 9;
            this.lblRecipientsPrimaryKey.Text = "Recipients primary key column:";
            // 
            // cbRecipientsPrimaryKey
            // 
            this.cbRecipientsPrimaryKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRecipientsPrimaryKey.FormattingEnabled = true;
            this.cbRecipientsPrimaryKey.Location = new System.Drawing.Point(164, 153);
            this.cbRecipientsPrimaryKey.Name = "cbRecipientsPrimaryKey";
            this.cbRecipientsPrimaryKey.Size = new System.Drawing.Size(174, 21);
            this.cbRecipientsPrimaryKey.TabIndex = 2;
            // 
            // lblOrdersForeignKey
            // 
            this.lblOrdersForeignKey.AutoSize = true;
            this.lblOrdersForeignKey.Location = new System.Drawing.Point(9, 182);
            this.lblOrdersForeignKey.Name = "lblOrdersForeignKey";
            this.lblOrdersForeignKey.Size = new System.Drawing.Size(133, 13);
            this.lblOrdersForeignKey.TabIndex = 11;
            this.lblOrdersForeignKey.Text = "Orders foreign key column:";
            // 
            // cbOrdersForeignKey
            // 
            this.cbOrdersForeignKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrdersForeignKey.FormattingEnabled = true;
            this.cbOrdersForeignKey.Location = new System.Drawing.Point(164, 179);
            this.cbOrdersForeignKey.Name = "cbOrdersForeignKey";
            this.cbOrdersForeignKey.Size = new System.Drawing.Size(174, 21);
            this.cbOrdersForeignKey.TabIndex = 3;
            // 
            // lblRecipientMapping
            // 
            this.lblRecipientMapping.AutoSize = true;
            this.lblRecipientMapping.Location = new System.Drawing.Point(9, 214);
            this.lblRecipientMapping.Name = "lblRecipientMapping";
            this.lblRecipientMapping.Size = new System.Drawing.Size(430, 13);
            this.lblRecipientMapping.TabIndex = 13;
            this.lblRecipientMapping.Text = "Please map the following 2Ship recipient variables to your columns in the Recipie" +
                "nts table:";
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Location = new System.Drawing.Point(25, 248);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(123, 13);
            this.lblContact.TabIndex = 14;
            this.lblContact.Text = "Recipient contact name:";
            // 
            // cbContact
            // 
            this.cbContact.FormattingEnabled = true;
            this.cbContact.Location = new System.Drawing.Point(164, 245);
            this.cbContact.Name = "cbContact";
            this.cbContact.Size = new System.Drawing.Size(174, 21);
            this.cbContact.TabIndex = 4;
            // 
            // cbCompany
            // 
            this.cbCompany.FormattingEnabled = true;
            this.cbCompany.Location = new System.Drawing.Point(164, 272);
            this.cbCompany.Name = "cbCompany";
            this.cbCompany.Size = new System.Drawing.Size(174, 21);
            this.cbCompany.TabIndex = 5;
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(25, 275);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(130, 13);
            this.lblCompany.TabIndex = 16;
            this.lblCompany.Text = "Recipient company name:";
            // 
            // cbCountry
            // 
            this.cbCountry.FormattingEnabled = true;
            this.cbCountry.Location = new System.Drawing.Point(164, 296);
            this.cbCountry.Name = "cbCountry";
            this.cbCountry.Size = new System.Drawing.Size(174, 21);
            this.cbCountry.TabIndex = 6;
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(25, 302);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(120, 13);
            this.lblCountry.TabIndex = 18;
            this.lblCountry.Text = "Recipient country code:";
            // 
            // cbState
            // 
            this.cbState.FormattingEnabled = true;
            this.cbState.Location = new System.Drawing.Point(164, 323);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(174, 21);
            this.cbState.TabIndex = 7;
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(25, 326);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(108, 13);
            this.lblState.TabIndex = 20;
            this.lblState.Text = "Recipient state code:";
            // 
            // cbCity
            // 
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(164, 350);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(174, 21);
            this.cbCity.TabIndex = 8;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(25, 353);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(74, 13);
            this.lblCity.TabIndex = 22;
            this.lblCity.Text = "Recipient city:";
            // 
            // cbAddress1
            // 
            this.cbAddress1.FormattingEnabled = true;
            this.cbAddress1.Location = new System.Drawing.Point(164, 404);
            this.cbAddress1.Name = "cbAddress1";
            this.cbAddress1.Size = new System.Drawing.Size(174, 21);
            this.cbAddress1.TabIndex = 10;
            // 
            // lblAddress1
            // 
            this.lblAddress1.AutoSize = true;
            this.lblAddress1.Location = new System.Drawing.Point(25, 407);
            this.lblAddress1.Name = "lblAddress1";
            this.lblAddress1.Size = new System.Drawing.Size(123, 13);
            this.lblAddress1.TabIndex = 24;
            this.lblAddress1.Text = "Recipient address line 1:";
            // 
            // cbAddress2
            // 
            this.cbAddress2.FormattingEnabled = true;
            this.cbAddress2.Location = new System.Drawing.Point(164, 431);
            this.cbAddress2.Name = "cbAddress2";
            this.cbAddress2.Size = new System.Drawing.Size(174, 21);
            this.cbAddress2.TabIndex = 11;
            // 
            // lblAddress2
            // 
            this.lblAddress2.AutoSize = true;
            this.lblAddress2.Location = new System.Drawing.Point(25, 434);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(123, 13);
            this.lblAddress2.TabIndex = 26;
            this.lblAddress2.Text = "Recipient address line 2:";
            // 
            // cbPhone
            // 
            this.cbPhone.FormattingEnabled = true;
            this.cbPhone.Location = new System.Drawing.Point(164, 458);
            this.cbPhone.Name = "cbPhone";
            this.cbPhone.Size = new System.Drawing.Size(174, 21);
            this.cbPhone.TabIndex = 12;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(25, 461);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(126, 13);
            this.lblPhone.TabIndex = 28;
            this.lblPhone.Text = "Recipient phone number:";
            // 
            // cbEmail
            // 
            this.cbEmail.FormattingEnabled = true;
            this.cbEmail.Location = new System.Drawing.Point(164, 485);
            this.cbEmail.Name = "cbEmail";
            this.cbEmail.Size = new System.Drawing.Size(174, 21);
            this.cbEmail.TabIndex = 13;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(25, 488);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(122, 13);
            this.lblEmail.TabIndex = 30;
            this.lblEmail.Text = "Recipient email address:";
            // 
            // lblContactMessage
            // 
            this.lblContactMessage.AutoSize = true;
            this.lblContactMessage.Location = new System.Drawing.Point(348, 248);
            this.lblContactMessage.Name = "lblContactMessage";
            this.lblContactMessage.Size = new System.Drawing.Size(222, 13);
            this.lblContactMessage.TabIndex = 32;
            this.lblContactMessage.Text = "required at shipping, if company is not present";
            // 
            // lblCompanyMessage
            // 
            this.lblCompanyMessage.AutoSize = true;
            this.lblCompanyMessage.Location = new System.Drawing.Point(348, 275);
            this.lblCompanyMessage.Name = "lblCompanyMessage";
            this.lblCompanyMessage.Size = new System.Drawing.Size(215, 13);
            this.lblCompanyMessage.TabIndex = 33;
            this.lblCompanyMessage.Text = "required at shipping, if contact is not present";
            // 
            // lblCountryMessage
            // 
            this.lblCountryMessage.AutoSize = true;
            this.lblCountryMessage.Location = new System.Drawing.Point(348, 299);
            this.lblCountryMessage.Name = "lblCountryMessage";
            this.lblCountryMessage.Size = new System.Drawing.Size(174, 13);
            this.lblCountryMessage.TabIndex = 34;
            this.lblCountryMessage.Text = "required, 2 letters ISO country code";
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFooterLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(12, 545);
            this.lblFooterLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(591, 2);
            this.lblFooterLine.TabIndex = 36;
            // 
            // lblSenderMappingLine
            // 
            this.lblSenderMappingLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSenderMappingLine.BackColor = System.Drawing.SystemColors.Window;
            this.lblSenderMappingLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSenderMappingLine.Location = new System.Drawing.Point(12, 232);
            this.lblSenderMappingLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblSenderMappingLine.Name = "lblSenderMappingLine";
            this.lblSenderMappingLine.Size = new System.Drawing.Size(591, 2);
            this.lblSenderMappingLine.TabIndex = 37;
            // 
            // lblStateMessage
            // 
            this.lblStateMessage.AutoSize = true;
            this.lblStateMessage.Location = new System.Drawing.Point(348, 326);
            this.lblStateMessage.Name = "lblStateMessage";
            this.lblStateMessage.Size = new System.Drawing.Size(206, 13);
            this.lblStateMessage.TabIndex = 38;
            this.lblStateMessage.Text = "required, 2 letters state code (for US && CA)";
            // 
            // lblCityMessage
            // 
            this.lblCityMessage.AutoSize = true;
            this.lblCityMessage.Location = new System.Drawing.Point(348, 353);
            this.lblCityMessage.Name = "lblCityMessage";
            this.lblCityMessage.Size = new System.Drawing.Size(99, 13);
            this.lblCityMessage.TabIndex = 39;
            this.lblCityMessage.Text = "required at shipping";
            // 
            // lblAddress1Message
            // 
            this.lblAddress1Message.AutoSize = true;
            this.lblAddress1Message.Location = new System.Drawing.Point(348, 407);
            this.lblAddress1Message.Name = "lblAddress1Message";
            this.lblAddress1Message.Size = new System.Drawing.Size(99, 13);
            this.lblAddress1Message.TabIndex = 40;
            this.lblAddress1Message.Text = "required at shipping";
            // 
            // lblAddress2Message
            // 
            this.lblAddress2Message.AutoSize = true;
            this.lblAddress2Message.Location = new System.Drawing.Point(348, 434);
            this.lblAddress2Message.Name = "lblAddress2Message";
            this.lblAddress2Message.Size = new System.Drawing.Size(44, 13);
            this.lblAddress2Message.TabIndex = 41;
            this.lblAddress2Message.Text = "optional";
            // 
            // lblPhoneMessage
            // 
            this.lblPhoneMessage.AutoSize = true;
            this.lblPhoneMessage.Location = new System.Drawing.Point(348, 461);
            this.lblPhoneMessage.Name = "lblPhoneMessage";
            this.lblPhoneMessage.Size = new System.Drawing.Size(99, 13);
            this.lblPhoneMessage.TabIndex = 42;
            this.lblPhoneMessage.Text = "required at shipping";
            // 
            // lblEmailMessage
            // 
            this.lblEmailMessage.AutoSize = true;
            this.lblEmailMessage.Location = new System.Drawing.Point(348, 488);
            this.lblEmailMessage.Name = "lblEmailMessage";
            this.lblEmailMessage.Size = new System.Drawing.Size(99, 13);
            this.lblEmailMessage.TabIndex = 43;
            this.lblEmailMessage.Text = "required at shipping";
            // 
            // lblRecipientsTableMessage
            // 
            this.lblRecipientsTableMessage.AutoSize = true;
            this.lblRecipientsTableMessage.Location = new System.Drawing.Point(348, 129);
            this.lblRecipientsTableMessage.Name = "lblRecipientsTableMessage";
            this.lblRecipientsTableMessage.Size = new System.Drawing.Size(176, 13);
            this.lblRecipientsTableMessage.TabIndex = 44;
            this.lblRecipientsTableMessage.Text = "this is usually an address book table";
            // 
            // lblPostalMessage
            // 
            this.lblPostalMessage.AutoSize = true;
            this.lblPostalMessage.Location = new System.Drawing.Point(348, 380);
            this.lblPostalMessage.Name = "lblPostalMessage";
            this.lblPostalMessage.Size = new System.Drawing.Size(45, 13);
            this.lblPostalMessage.TabIndex = 49;
            this.lblPostalMessage.Text = "required";
            // 
            // cbPostal
            // 
            this.cbPostal.FormattingEnabled = true;
            this.cbPostal.Location = new System.Drawing.Point(164, 377);
            this.cbPostal.Name = "cbPostal";
            this.cbPostal.Size = new System.Drawing.Size(174, 21);
            this.cbPostal.TabIndex = 9;
            // 
            // lblPostal
            // 
            this.lblPostal.AutoSize = true;
            this.lblPostal.Location = new System.Drawing.Point(25, 380);
            this.lblPostal.Name = "lblPostal";
            this.lblPostal.Size = new System.Drawing.Size(141, 13);
            this.lblPostal.TabIndex = 47;
            this.lblPostal.Text = "Recipient ZIP / postal code:";
            // 
            // lblLinkTop
            // 
            this.lblLinkTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLinkTop.Location = new System.Drawing.Point(344, 162);
            this.lblLinkTop.Name = "lblLinkTop";
            this.lblLinkTop.Size = new System.Drawing.Size(10, 1);
            this.lblLinkTop.TabIndex = 51;
            // 
            // lblLinkBottom
            // 
            this.lblLinkBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLinkBottom.Location = new System.Drawing.Point(344, 189);
            this.lblLinkBottom.Name = "lblLinkBottom";
            this.lblLinkBottom.Size = new System.Drawing.Size(10, 1);
            this.lblLinkBottom.TabIndex = 52;
            // 
            // lblLinkRight
            // 
            this.lblLinkRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLinkRight.Location = new System.Drawing.Point(354, 162);
            this.lblLinkRight.Name = "lblLinkRight";
            this.lblLinkRight.Size = new System.Drawing.Size(1, 28);
            this.lblLinkRight.TabIndex = 53;
            // 
            // lblLink
            // 
            this.lblLink.AutoSize = true;
            this.lblLink.Location = new System.Drawing.Point(362, 169);
            this.lblLink.Name = "lblLink";
            this.lblLink.Size = new System.Drawing.Size(217, 13);
            this.lblLink.TabIndex = 54;
            this.lblLink.Text = "this two columns mapping represents the link";
            // 
            // lblResidential
            // 
            this.lblResidential.AutoSize = true;
            this.lblResidential.Location = new System.Drawing.Point(25, 514);
            this.lblResidential.Name = "lblResidential";
            this.lblResidential.Size = new System.Drawing.Size(105, 13);
            this.lblResidential.TabIndex = 55;
            this.lblResidential.Text = "Recipient residential:";
            // 
            // lblResidentialMessage
            // 
            this.lblResidentialMessage.AutoSize = true;
            this.lblResidentialMessage.Location = new System.Drawing.Point(348, 514);
            this.lblResidentialMessage.Name = "lblResidentialMessage";
            this.lblResidentialMessage.Size = new System.Drawing.Size(190, 13);
            this.lblResidentialMessage.TabIndex = 58;
            this.lblResidentialMessage.Text = "choose yes, if the address is residential";
            // 
            // cbResidential
            // 
            this.cbResidential.FormattingEnabled = true;
            this.cbResidential.Location = new System.Drawing.Point(164, 511);
            this.cbResidential.Name = "cbResidential";
            this.cbResidential.Size = new System.Drawing.Size(174, 21);
            this.cbResidential.TabIndex = 14;
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.lblHeaderLine);
            this.pnlHeader.Controls.Add(this.llAdvanced);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.lblHeaderImage);
            this.pnlHeader.Controls.Add(this.lblHeaderMessage);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(615, 115);
            this.pnlHeader.TabIndex = 117;
            // 
            // lblHeaderLine
            // 
            this.lblHeaderLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderLine.BackColor = System.Drawing.SystemColors.Control;
            this.lblHeaderLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeaderLine.Location = new System.Drawing.Point(0, 113);
            this.lblHeaderLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderLine.Name = "lblHeaderLine";
            this.lblHeaderLine.Size = new System.Drawing.Size(615, 2);
            this.lblHeaderLine.TabIndex = 116;
            // 
            // llAdvanced
            // 
            this.llAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdvanced.Image = global::InteGr8.Properties.Resources.wrench;
            this.llAdvanced.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llAdvanced.LinkArea = new System.Windows.Forms.LinkArea(6, 13);
            this.llAdvanced.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdvanced.Location = new System.Drawing.Point(511, 0);
            this.llAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.llAdvanced.Name = "llAdvanced";
            this.llAdvanced.Size = new System.Drawing.Size(104, 24);
            this.llAdvanced.TabIndex = 119;
            this.llAdvanced.TabStop = true;
            this.llAdvanced.Text = "      advanced mode";
            this.llAdvanced.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llAdvanced.UseCompatibleTextRendering = true;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(513, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Recipient Information";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeaderImage
            // 
            this.lblHeaderImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeaderImage.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaderImage.Image = global::InteGr8.Properties.Resources.data_next;
            this.lblHeaderImage.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeaderImage.Location = new System.Drawing.Point(0, 24);
            this.lblHeaderImage.Name = "lblHeaderImage";
            this.lblHeaderImage.Padding = new System.Windows.Forms.Padding(5);
            this.lblHeaderImage.Size = new System.Drawing.Size(64, 89);
            this.lblHeaderImage.TabIndex = 115;
            // 
            // lblHeaderMessage
            // 
            this.lblHeaderMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderMessage.AutoEllipsis = true;
            this.lblHeaderMessage.Location = new System.Drawing.Point(68, 24);
            this.lblHeaderMessage.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderMessage.Name = "lblHeaderMessage";
            this.lblHeaderMessage.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.lblHeaderMessage.Size = new System.Drawing.Size(547, 89);
            this.lblHeaderMessage.TabIndex = 114;
            this.lblHeaderMessage.Text = resources.GetString("lblHeaderMessage.Text");
            // 
            // frmWizard04
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(615, 591);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.cbResidential);
            this.Controls.Add(this.lblResidentialMessage);
            this.Controls.Add(this.lblResidential);
            this.Controls.Add(this.lblLink);
            this.Controls.Add(this.lblLinkRight);
            this.Controls.Add(this.lblLinkBottom);
            this.Controls.Add(this.lblLinkTop);
            this.Controls.Add(this.lblPostalMessage);
            this.Controls.Add(this.cbPostal);
            this.Controls.Add(this.lblPostal);
            this.Controls.Add(this.lblRecipientsTableMessage);
            this.Controls.Add(this.lblEmailMessage);
            this.Controls.Add(this.lblPhoneMessage);
            this.Controls.Add(this.lblAddress2Message);
            this.Controls.Add(this.lblAddress1Message);
            this.Controls.Add(this.lblCityMessage);
            this.Controls.Add(this.lblStateMessage);
            this.Controls.Add(this.lblSenderMappingLine);
            this.Controls.Add(this.lblFooterLine);
            this.Controls.Add(this.lblCountryMessage);
            this.Controls.Add(this.lblCompanyMessage);
            this.Controls.Add(this.lblContactMessage);
            this.Controls.Add(this.cbEmail);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.cbPhone);
            this.Controls.Add(this.lblPhone);
            this.Controls.Add(this.cbAddress2);
            this.Controls.Add(this.lblAddress2);
            this.Controls.Add(this.cbAddress1);
            this.Controls.Add(this.lblAddress1);
            this.Controls.Add(this.cbCity);
            this.Controls.Add(this.lblCity);
            this.Controls.Add(this.cbState);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.cbCountry);
            this.Controls.Add(this.lblCountry);
            this.Controls.Add(this.cbCompany);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.cbContact);
            this.Controls.Add(this.lblContact);
            this.Controls.Add(this.lblRecipientMapping);
            this.Controls.Add(this.cbOrdersForeignKey);
            this.Controls.Add(this.lblOrdersForeignKey);
            this.Controls.Add(this.cbRecipientsPrimaryKey);
            this.Controls.Add(this.lblRecipientsPrimaryKey);
            this.Controls.Add(this.cbRecipientsTable);
            this.Controls.Add(this.lblRecipientsTable);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnNext);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(623, 618);
            this.Name = "frmWizard04";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 4 of 11";
            this.Load += new System.EventHandler(this.frmWizard04_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard04_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWizard04_FormClosing);
            this.pnlHeader.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Label lblRecipientsTable;
        private System.Windows.Forms.ComboBox cbRecipientsTable;
        private System.Windows.Forms.Label lblRecipientsPrimaryKey;
        private System.Windows.Forms.ComboBox cbRecipientsPrimaryKey;
        private System.Windows.Forms.Label lblOrdersForeignKey;
        private System.Windows.Forms.ComboBox cbOrdersForeignKey;
        private System.Windows.Forms.Label lblRecipientMapping;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.ComboBox cbContact;
        private System.Windows.Forms.ComboBox cbCompany;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.ComboBox cbCountry;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.ComboBox cbState;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.ComboBox cbCity;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.ComboBox cbAddress1;
        private System.Windows.Forms.Label lblAddress1;
        private System.Windows.Forms.ComboBox cbAddress2;
        private System.Windows.Forms.Label lblAddress2;
        private System.Windows.Forms.ComboBox cbPhone;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.ComboBox cbEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblContactMessage;
        private System.Windows.Forms.Label lblCompanyMessage;
        private System.Windows.Forms.Label lblCountryMessage;
        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.Label lblSenderMappingLine;
        private System.Windows.Forms.Label lblStateMessage;
        private System.Windows.Forms.Label lblCityMessage;
        private System.Windows.Forms.Label lblAddress1Message;
        private System.Windows.Forms.Label lblAddress2Message;
        private System.Windows.Forms.Label lblPhoneMessage;
        private System.Windows.Forms.Label lblEmailMessage;
        private System.Windows.Forms.Label lblRecipientsTableMessage;
        private System.Windows.Forms.Label lblPostalMessage;
        private System.Windows.Forms.ComboBox cbPostal;
        private System.Windows.Forms.Label lblPostal;
        private System.Windows.Forms.Label lblLinkTop;
        private System.Windows.Forms.Label lblLinkBottom;
        private System.Windows.Forms.Label lblLinkRight;
        private System.Windows.Forms.Label lblLink;
        private System.Windows.Forms.Label lblResidential;
        private System.Windows.Forms.Label lblResidentialMessage;
        private System.Windows.Forms.ComboBox cbResidential;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lblHeaderLine;
        private System.Windows.Forms.LinkLabel llAdvanced;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label lblHeaderImage;
        private System.Windows.Forms.Label lblHeaderMessage;
    }
}