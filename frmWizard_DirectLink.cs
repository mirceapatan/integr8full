﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    public partial class frmWizard_DirectLink : Form
    {
        public Link Link = null;
        private Table Parent_Table = null, Child_Table = null;

        #region Constructors

        private frmWizard_DirectLink()
        {
            InitializeComponent();

            // populate the table types
            foreach (string table_type in TableType.Names)
            {
                cbParentType.Items.Add(table_type);
                cbChildType.Items.Add(table_type);
            }
            cbParentType.Text = TableType.Other.ToString();
            cbChildType.Text = TableType.Other.ToString();
            btnRemove.Enabled = true;
        }

        // edit an existing link
        public frmWizard_DirectLink(Link Link)
            : this()
        {
            if (Link == null) throw new ArgumentException();

            this.Link = Link;
            this.Parent_Table = Link.Parent;
            this.Child_Table = Link.Child;

            cbParentTable.Text = Link.Parent.Name;
            cbChildTable.Text = Link.Child.Name;

            if (cbParentTable.FindStringExact(Link.Parent.Name) == -1) throw new ArgumentException("The Parent table '" + Link.Parent.Name + "' was not found in the datasource.");
            if (cbChildTable.FindStringExact(Link.Child.Name) == -1) throw new ArgumentException("The Child table '" + Link.Child.Name + "' was not found in the datasource.");
            
            cbParentTable.Enabled = false;
            cbChildTable.Enabled = false;
            btnRemove.Visible = true;
        }

        // create a new link
        public frmWizard_DirectLink(Table Parent, Table Child)
            : this()
        {
            if (Parent == null || Child == null) throw new ArgumentException();
            if (cbParentTable.FindStringExact(Parent.Name) == -1) throw new ArgumentException("The Parent table '" + Parent.Name + "' was not found in the datasource.");
            if (cbChildTable.FindStringExact(Child.Name) == -1) throw new ArgumentException("The Child table '" + Child.Name + "' was not found in the datasource.");

            this.Parent_Table = Parent;
            this.Child_Table = Child;

            cbParentTable.Text = Parent.Name;
            cbChildTable.Text = Child.Name;
            cbParentTable.Enabled = false;
            cbChildTable.Enabled = false;
        }

        public frmWizard_DirectLink(string Parent, Table Child)
            : this()
        {
            if (String.IsNullOrEmpty(Parent) || Child == null) throw new ArgumentException();
            if (cbParentTable.FindStringExact(Parent) == -1) throw new ArgumentException("The Parent table '" + Parent + "' was not found in the datasource.");
            if (cbChildTable.FindStringExact(Child.Name) == -1) throw new ArgumentException("The Child table '" + Child.Name + "' was not found in the datasource.");

            this.Child_Table = Child;

            cbParentTable.Text = Parent;
            cbParentTable.Enabled = false;
            cbChildTable.Text = Child.Name;
            cbChildTable.Enabled = false;
        }

        public frmWizard_DirectLink(Table Parent, string Child)
            : this()
        {
            if (Parent == null || String.IsNullOrEmpty(Child)) throw new ArgumentException();
            if (cbParentTable.FindStringExact(Parent.Name) == -1) throw new ArgumentException("The Parent table '" + Parent.Name + "' was not found in the datasource.");
            if (cbChildTable.FindStringExact(Child) == -1) throw new ArgumentException("The Child table '" + Child + "' was not found in the datasource.");

            this.Parent_Table = Parent;

            cbParentTable.Text = Parent.Name;
            cbChildTable.Text = Child;
            cbChildTable.Enabled = false;
            cbParentTable.Enabled = false;
        }

        public frmWizard_DirectLink(string Parent, string Child)
            : this()
        {
            if (String.IsNullOrEmpty(Parent) || String.IsNullOrEmpty(Child)) throw new ArgumentException();
            if (cbParentTable.FindStringExact(Parent) == -1) throw new ArgumentException("The Parent table '" + Parent + "' was not found in the datasource.");
            if (cbChildTable.FindStringExact(Child) == -1) throw new ArgumentException("The Child table '" + Child + "' was not found in the datasource.");

            cbParentTable.Text = Parent;
            cbParentTable.Enabled = false;
            cbChildTable.Text = Child;
            cbChildTable.Enabled = false;
        }

        #endregion

        private void SelectionChanged()
        {
            Table ParentTable = Parent_Table, ChildTable = Child_Table;
            if (ParentTable == null)
            {
                List<Table> Parents = Template.FindTables(cbParentTable.Text);
                if (Parents.Count == 1) ParentTable = Parents[0];
            }
            if (ParentTable != null)
            {
                cbParentType.Text = ParentTable.Type.ToString();
                cbParentType.Enabled = false;
                foreach (string Column in ParentTable.PK)
                    lbParentPK.SelectedItems.Add(Column);
                lbParentPK.Enabled = false;
            }

            if (ChildTable == null)
            {
                List<Table> Children = Template.FindTables(cbChildTable.Text);
                if (Children.Count == 1) ChildTable = Children[0];
            }
            if (ChildTable != null)
            {
                cbChildType.Text = ChildTable.Type.ToString();
                cbChildType.Enabled = false;
                foreach (string Column in ChildTable.PK)
                    lbChildPK.SelectedItems.Add(Column);
                lbChildPK.Enabled = false;
            }

            if (ParentTable != null && ChildTable != null)
            {
                Link Link = Template.FindLink(ParentTable, ChildTable);
                if (Link != null)
                    foreach (string Column in Link.FK)
                        lbChildFK.SelectedItems.Add(Column);
            }
        }

        private void cbParent_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbParentType.SelectedText = TableType.Other;
            lbParentPK.Items.Clear();
            cbParentType.Enabled = true;
            lbParentPK.Enabled = true;

            lbParentPK.Items.AddRange(Template.ReadConn.Get_Columns(cbParentTable.Text).ToArray());
            if (lbParentPK.Items.Count > 0)
            {
                if (Parent_Table != null)
                    foreach (string PK in Parent_Table.PK)
                        lbParentPK.SelectedItems.Add(PK);
                else lbParentPK.SelectedIndex = 0;
            }
            SelectionChanged();
        }

        private void cbChild_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbChildPK.Items.Clear();
            lbChildFK.Items.Clear();
            cbChildType.Enabled = true;
            lbChildPK.Enabled = true;

            lbChildPK.Items.AddRange((cbChildType.Text.Equals(TableType.Shipments) ? Template.WriteConn : Template.ReadConn).Get_Columns(cbChildTable.Text).ToArray());
            lbChildFK.Items.AddRange((cbChildType.Text.Equals(TableType.Shipments) ? Template.WriteConn : Template.ReadConn).Get_Columns(cbChildTable.Text).ToArray());
            if (lbChildPK.Items.Count > 0)
            {
                if (Child_Table != null)
                    foreach (string PK in Child_Table.PK)
                        lbChildPK.SelectedItems.Add(PK);
                else lbChildPK.SelectedIndex = 0;
            }
            SelectionChanged();
        }

        private bool Save()
        {
            if (cbParentTable.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please select the Parent table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (cbChildTable.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please select the Child table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (lbParentPK.SelectedItems.Count == 0)
            {
                MessageBox.Show(this, "Please select the Primary Key columns in the Parent table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (lbChildPK.SelectedItems.Count == 0)
            {
                MessageBox.Show(this, "Please select the Primary Key columns in the Child table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (lbChildFK.SelectedItems.Count == 0)
            {
                MessageBox.Show(this, "Please select the Foreign Key columns in the Child table.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            if (Link == null)
            {
                List<string> ParentPK = new List<string>();
                foreach (string Column in lbParentPK.SelectedItems)
                    ParentPK.Add(Column);
                Table Parent = Template.CreateTable(cbParentTable.Text, TableType.Parse(cbParentType.Text), ParentPK);

                List<string> ChildPK = new List<string>();
                foreach (string Column in lbChildPK.SelectedItems)
                    ChildPK.Add(Column);
                Table Child = Template.CreateTable(cbChildTable.Text, TableType.Parse(cbChildType.Text), ChildPK);

                List<string> ChildFK = new List<string>();
                foreach (string Column in lbChildFK.SelectedItems)
                    ChildFK.Add(Column);

                Link = Template.CreateLink(Parent, Child, ChildFK);
            }
            else
            {
                Link.FK.Clear();
                foreach (string Column in lbChildFK.SelectedItems)
                    Link.FK.Add(Column);
            }
            
            return true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (!Save()) return;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            Template.Links.Remove(Link);
            DialogResult = DialogResult.No;
            Close();
        }
        private string getTableName(string name)
        {
            string tableName = "[" + name.TrimStart('[').TrimEnd(']') + "]";
            return tableName;
        }

        private void llParentPreview_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (cbParentTable.SelectedIndex != -1)
                new frmWizard_DataPreview(cbParentTable.Text, Template.ReadConn.ConnStr, "Select Top 100 * From " + getTableName(cbParentTable.Text) + " Order By 1").ShowDialog(this);
        }

        private void llChildPreview_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (cbChildTable.SelectedIndex != -1)
            {
                
                new frmWizard_DataPreview(cbChildTable.Text, Template.ReadConn.ConnStr, "Select Top 100 * From " + getTableName(cbChildTable.Text) + " Order By 1").ShowDialog(this);
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            // ...
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSaveAdd_Click(object sender, EventArgs e)
        {
            if (!Save()) return;

            cbParentTable.Enabled = true;
            cbParentType.Enabled = true;
            cbChildTable.Enabled = true;
            cbChildType.Enabled = true;
            lbParentPK.Enabled = true;
            lbChildPK.Enabled = true;
            lbChildFK.Enabled = true;

            cbParentTable.SelectedIndex = -1;
            cbChildTable.SelectedIndex = -1;
        }

        private void cbParentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // repopulate the tables with the ODBC catalog tables
            cbParentTable.Items.AddRange((cbParentType.Text.Equals(TableType.Shipments) ? Template.WriteConn : Template.ReadConn).Get_Catalog(true, true).ToArray());
        }

        private void cbChildType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // repopulate the tables with the ODBC catalog tables
            cbChildTable.Items.AddRange((cbChildType.Text.Equals(TableType.Shipments) ? Template.WriteConn : Template.ReadConn).Get_Catalog(true, true).ToArray());
        }
    }
}
