﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InteGr8
{
    // TODO: if the 2Ship field is a result field (Category == Shipments || Category == ShipmentPackages)
    // then automatically put & restrict the Field Table to be the Shipments or ShipmentPackages table only
    public partial class frmWizardSummary : Form
    {
        public frmWizardSummary()
        {
            InitializeComponent();
        }

        private void frmWizardSummary_Load(object sender, EventArgs e)
        {
            // populate the Orders table combobox with the ODBC catalog tables
            TableColumn.Items.Add("(none)");
            using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
            {
                Conn.Open();
                foreach (DataRow row in Conn.GetSchema("Tables").Rows)
                {
                    cbOrdersTable.Items.Add(row["TABLE_NAME"]);
                    TableColumn.Items.Add(row["TABLE_NAME"]);
                }
            }
            if (cbOrdersTable.Items.Count > 0) cbOrdersTable.SelectedIndex = 0;

            FieldColumn.DataSource = InteGr8_Main.data.FieldsTable;
            FieldColumn.DataPropertyName = "Id";
            FieldColumn.DisplayMember = "Name";

            LinkColumn.ValueType = typeof(FieldLink);

            // load the template data
            if (InteGr8_Main.template.Orders != null)
            {
                if (cbOrdersTable.FindStringExact(InteGr8_Main.template.Orders) == -1)
                {
                    MessageBox.Show(this, "The Orders table '" + InteGr8_Main.template.Orders + "' was not found in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                cbOrdersTable.Text = InteGr8_Main.template.Orders;
            }
            if (InteGr8_Main.template.OrderNumber != null)
            {
                if (cbOrdersTable.FindStringExact(InteGr8_Main.template.Orders) == -1)
                {
                    MessageBox.Show(this, "The Orders table '" + InteGr8_Main.template.Orders + "' was not found in the datasource.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                cbOrdersTable.Text = InteGr8_Main.template.Orders;
            }
            foreach (FieldBinding binding in InteGr8_Main.template.Bindings)
            {
                int index = dgvBindings.Rows.Add();
                dgvBindings.Rows[index].Cells["FieldColumn"].Value = binding.Field.Name;
                dgvBindings.Rows[index].Cells["TableColumn"].Value = binding.Link == null ? "" : binding.Link.FieldTable;
                dgvBindings.Rows[index].Cells["LinkColumn"].Value = binding.Link != null && binding.Link.Type == FieldLink.LinkType.Orders ? null : binding.Link;
                if (binding.Column != null) ColumnColumn.Items.Add(binding.Column);
                dgvBindings.Rows[index].Cells["ColumnColumn"].Value = binding.Column;
                dgvBindings.Rows[index].Cells["DefaultColumn"].Value = binding.DefaultValue;
            }
        }

        private void cbOrdersTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // repopulate the Order table columns
            cbOrderNumberColumn.Items.Clear();
            dgvBindings.Enabled = false;
            btnNext.Enabled = false;
            if (cbOrdersTable.Text.Trim().Equals("")) return;

            using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
            {
                Conn.Open();
                using (OdbcDataReader dr = new OdbcCommand("Select Top 0 * From " + cbOrdersTable.Text, Conn).ExecuteReader(CommandBehavior.SingleRow))
                    for (int k = 0; k < dr.FieldCount; k++)
                        cbOrderNumberColumn.Items.Add("[" + dr.GetName(k) + "]");
            }

            if (cbOrderNumberColumn.Items.Count > 0)
            {
                cbOrderNumberColumn.SelectedIndex = 0;
                dgvBindings.Enabled = true;
                btnNext.Enabled = true;
            }
        }

        private void cbOrderNumberColumn_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvBindings.Rows.Clear();
        }

        private void dgvBindings_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == TableColumn.DisplayIndex)
            {
                dgvBindings.Rows[e.RowIndex].Cells[ColumnColumn.DisplayIndex].Value = "";
                object table = dgvBindings.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                FieldLink link = null;
                if (table != null && !table.ToString().Trim().Equals("") && !table.Equals("(none)") && !table.Equals(cbOrdersTable.Text))
                {
                    link = new FieldLink();
                    link.Type = FieldLink.LinkType.OrdersOneToOne;
                }
                dgvBindings.Rows[e.RowIndex].Cells[LinkColumn.DisplayIndex].Value = link;
            }
        }

        private void dgvBindings_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvBindings.CurrentCellAddress.X == ColumnColumn.DisplayIndex && e.Control != null)
            {
                ComboBox cb = e.Control as ComboBox;
                string cell_value = cb.Text;
                cb.Items.Clear();
                cb.DropDownStyle = ComboBoxStyle.DropDown;
                object table = dgvBindings.Rows[dgvBindings.CurrentCellAddress.Y].Cells[TableColumn.DisplayIndex].Value;
                if (table != null && !table.ToString().Trim().Equals("") && !table.Equals("(none)"))
                    using (OdbcConnection Conn = new OdbcConnection(InteGr8_Main.template.Connection))
                    {
                        Conn.Open();
                        using (OdbcDataReader dr = new OdbcCommand("Select Top 0 * From " + table, Conn).ExecuteReader(CommandBehavior.SingleRow))
                            for (int k = 0; k < dr.FieldCount; k++)
                                cb.Items.Add("[" + dr.GetName(k) + "]");
                    }
                cb.Text = cell_value;
            }
        }

        private void dgvBindings_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == ColumnColumn.DisplayIndex && !ColumnColumn.Items.Contains(e.FormattedValue))
                // add the new item to the gridview combo (the combo columns are never used, but will give errors otherwise)
                ColumnColumn.Items.Add(e.FormattedValue);
        }

        private void dgvBindings_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == LinkColumn.DisplayIndex)
            {
                // the user clicked on the link
                if (dgvBindings.SelectedRows == null || dgvBindings.SelectedRows.Count != 1) return;
                object table = dgvBindings.SelectedRows[0].Cells[TableColumn.DisplayIndex].Value;
                if (table == null || table.ToString().Trim().Equals("")) return;
                //frmLinkEditor editor = new frmLinkEditor(cbOrdersTable.Text, table.ToString(), dgvBindings.Rows[e.RowIndex].Cells[e.ColumnIndex].Value as FieldLink);
                //if (editor.ShowDialog(this) == DialogResult.OK) dgvBindings.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = editor.Link;
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (dgvBindings.SelectedRows == null || dgvBindings.SelectedRows.Count != 1) return;
            object table = dgvBindings.SelectedRows[0].Cells[TableColumn.DisplayIndex].Value;
            if (table == null || table.ToString().Trim().Equals("")) return;
            new frmWizard_DataPreview(table.ToString(), InteGr8_Main.template.Connection, "Select Top 100 * From " + table).ShowDialog(this);
        }

        private void frmWizardSummary_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK) return;
            if (DialogResult == DialogResult.No)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to close the wizard?", "InteGr8", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            DialogResult = DialogResult.Abort;
        }

        private void frmWizardSummary_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            Help.ShowWizardHelp("Wizard_Summary");
            e.Cancel = true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            // validate - some fields are required
            // ...

            foreach (DataGridViewRow Row in dgvBindings.Rows)
            {
                if (Row.IsNewRow) continue;
                object field = Row.Cells["FieldColumn"].Value;
                object table = Row.Cells["TableColumn"].Value;
                FieldLink link = Row.Cells["LinkColumn"].Value as FieldLink;
                object column = Row.Cells["ColumnColumn"].Value;
                object default_value = Row.Cells["DefaultColumn"].Value;

                if (field == null || field.ToString().Trim().Equals(""))
                {
                    MessageBox.Show(this, "The 2Ship Field at row '" + Row.Index + "' is missing.", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if ((column == null || column.ToString().Trim().Equals("")) && (default_value == null || default_value.ToString().Trim().Equals("")))
                {
                    MessageBox.Show(this, "The Column and Default Value at row '" + Row.Index + "' are both missing. Please enter at least one of them", "InteGr8", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                InteGr8_Main.template.UpdateBinding(field.ToString(), column == null || column.ToString().Trim().Equals(""), column == null || column.ToString().Trim().Equals("") ? default_value.ToString() : column.ToString(), link);
            }

            InteGr8_Main.template.Orders = cbOrdersTable.Text;
            InteGr8_Main.template.OrderNumber = cbOrderNumberColumn.Text;

            DialogResult = DialogResult.OK;
        }
    }
}
