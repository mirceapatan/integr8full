﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net.Mail;
using System.Web;
using System.Collections;
using System.IO;
using InteGr8.Properties;
using System.Net;
using System.Drawing.Printing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Management;
using System.Runtime.InteropServices;
using System.Threading;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace InteGr8
{
    public static class Utils
    {
        private static readonly string ErrorsSender = "Error@InteGr8.com";
        private static readonly string ErrorsRecipient = "errorint@innerve8.com";
        public static bool sent = false;
       
        private static readonly string now = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");

        public static readonly string log = Path.Combine(ConfigurationManager.AppSettings["UploaderErrorPath"], now + "_log.txt");
        public static readonly string orders_ok = Path.Combine(ConfigurationManager.AppSettings["UploadedPath"], now + "_processed.txt");
        public static readonly string orders_error = Path.Combine(ConfigurationManager.AppSettings["UploaderErrorPath"], now + "_error.txt");

        public static readonly string download_log = Path.Combine(ConfigurationManager.AppSettings["DownloaderErrorPath"], now + "_log.txt");
        private static readonly string download_orders_ok = Path.Combine(ConfigurationManager.AppSettings["DownloadedPath"], now + "_processed.txt");
        private static readonly string download_orders_error = Path.Combine(ConfigurationManager.AppSettings["DownloaderErrorPath"], now + "_error.txt");


//log files region
        public static void Write(string log, string subject, string body = null)
        {
            if (String.IsNullOrEmpty(body))
                body = "";
            else
                body = ": " + body;
            Console.Write(subject + body);
            File.AppendAllText(log, subject + body);
        }

        public static void WriteLine(string log, string subject, string body = null)
        {
            if (String.IsNullOrEmpty(body))
                body = "";
            else
                body = ": " + body;
            Console.WriteLine(subject + body);
            File.AppendAllText(log, subject + body + "\r\n");
        }

        // writes a two column line into a tab separated values file
        public static void LogTSV(string log, string value1, string value2)
        {
            if (String.IsNullOrEmpty(value1))
                value1 = "";
            else
                value1 = value1.Replace("\r", " ").Replace("\n", " ").Replace("\t", " ");

            if (String.IsNullOrEmpty(value2))
                value2 = "";
            else
                value2 = value2.Replace("\r", " ").Replace("\n", " ").Replace("\t", " ");

            File.AppendAllText(log, value1 + "\t" + value2 + "\r\n");
        }

        public static string BuildErrorLog(string subject, Exception e)
        {
            StringBuilder Message = new StringBuilder();
            StringBuilder StackTrace = new StringBuilder();
            Exception inner = e;
            while (inner != null)
            {
                if (!String.IsNullOrEmpty(inner.Message))
                    Message.Append(inner.Message).Append("\r\n");
                if (inner.StackTrace != null)
                    StackTrace.Append(inner.StackTrace).Append("\r\n");
                inner = inner.InnerException;
            }
            return Message.ToString() + StackTrace.ToString();
        }
        
//email region

        public static void SendError(string subject)
        {
            SendError("Error", subject, null);
        }

        public static void SendError(string subject, Exception e)
        {
            SendError(subject, e.Message + "\r\n\r\n" + e.StackTrace, e);
        }

        public static void SendError(string subject, string body, Exception e)
        {
            if (String.IsNullOrEmpty(subject)) 
                throw new ArgumentException("Empty subject received", "subject");
                //SendEmail(ErrorsSender, ErrorsRecipient, subject, body);
                System.IO.File.WriteAllText("Error.txt", subject + "\r\n\r\n" + body + "\n\n");
        }

        public static void SendClientEmail(string subject, string message)
        {
            if (Settings.Default.SMTP_Disabled || ErrorsSender == null) return;
            foreach (Data.EmailsRow row in InteGr8_Main.data.Emails.Rows)
            {
                if (row.Email == null) continue;
                MailMessage mmessage = null;
                try 
                {
                    sent = false;
                    mmessage = new MailMessage(new MailAddress(ErrorsSender), new MailAddress(row.Email));
                    mmessage.Subject = Template.Name + " - " + System.Environment.MachineName + " - " + (String.IsNullOrEmpty(subject.Trim()) ? "MISSING SUBJECT" : subject);
                    mmessage.Body = String.IsNullOrEmpty(message.Trim()) ? "MISSING BODY" : message;
                    mmessage.IsBodyHtml = true;
                    mmessage.BodyEncoding = System.Text.Encoding.UTF8;
                    mmessage.SubjectEncoding = System.Text.Encoding.UTF8;
                    SmtpClient smtp = new SmtpClient(Settings.Default.SMTP_Server, Settings.Default.SMTP_Port);
                    smtp.Credentials = new System.Net.NetworkCredential(Settings.Default.SMTP_User, Settings.Default.SMTP_Password);
                    smtp.EnableSsl = Settings.Default.SMTP_SSL;
                    smtp.SendCompleted += new SendCompletedEventHandler(smtp_SendCompleted);
                    smtp.SendAsync(mmessage, mmessage);
                }
                catch (Exception ex)
                {
                    SendError("Error sending email(s) to client", ex);
                    MessageBox.Show("An error occured while sending the email(s)\n Error message: " + ex.Message);
                }
            }
        }

        public static void SendEmail(string sender, string recipient, string subject, string message)
        {
            if (Settings.Default.SMTP_Disabled || sender == null || recipient == null) return;
            MailMessage mmessage = null;
            try
            {
                sent = false;
                mmessage = new MailMessage(new MailAddress(sender), new MailAddress(recipient));
                mmessage.Subject = Template.Name + " - " + System.Environment.MachineName + " - " +(String.IsNullOrEmpty(subject.Trim()) ? "MISSING SUBJECT" : subject);
                mmessage.Body = String.IsNullOrEmpty(message.Trim()) ? "MISSING BODY" : message;
                mmessage.IsBodyHtml = true;
                mmessage.BodyEncoding = System.Text.Encoding.UTF8;
                mmessage.SubjectEncoding = System.Text.Encoding.UTF8;
                SmtpClient smtp = new SmtpClient(Settings.Default.SMTP_Server, Settings.Default.SMTP_Port);
                smtp.Credentials = new System.Net.NetworkCredential(Settings.Default.SMTP_User, Settings.Default.SMTP_Password);
                smtp.EnableSsl = Settings.Default.SMTP_SSL;
                smtp.SendCompleted += new SendCompletedEventHandler(smtp_SendCompleted);
                smtp.SendAsync(mmessage, mmessage);
            }
            catch (Exception ex)
            {
                if (ex.StackTrace.Contains("Utils.SendEmail(")) return;
                if (mmessage != null) LogEmailError(ex, mmessage);
            }
        }

        private static void LogEmailError(Exception e, MailMessage email)
        {
            if (email == null) throw new ArgumentException("email parameter is null");
            if (email.Subject == null) email.Subject = "Error Log Email";
            // write the entire email to an email error log and do not throw any error
            // this email error log file can be used later to resend all emails with errors
            try
            {
                string guid = Guid.NewGuid().ToString("N");

                // build error log
                StringBuilder description = new StringBuilder();
                StringBuilder trace = new StringBuilder();
                StringBuilder data = new StringBuilder();
                Exception inner = e;
                while (inner != null)
                {
                    description.Append(inner.Message).Append("<BR/>");
                    if (inner.StackTrace != null) trace.Append(inner.StackTrace).Append("<BR/>");
                    if (inner.Data != null)
                        foreach (DictionaryEntry de in inner.Data)
                            data.Append(de.Key + " = " + de.Value + "<BR/>");
                    inner = inner.InnerException;
                }

                // write to file
                using (StreamWriter txt = new StreamWriter(Settings.Default.Log_Folder + "\\Email Error " + guid + ".htm"))
                {
                    txt.WriteLine("<B>Error sending email</B> from '" + email.From.Address + "' to '" + email.To[0].Address + "' with subject '" + email.Subject + "'<BR/>");
                    txt.WriteLine("<BR/><B>Error Description:</B><BR/>" + description);
                    txt.WriteLine("<BR/><B>Error Stack Trace:</B><BR/>" + trace);
                    txt.WriteLine("<BR/><B>Additional Info:</B><BR/>" + data);
                    txt.WriteLine("<BR/><B>Email body:</B><BR/>" + email.Body);
                }
                //stop all recursive email sending
                // send error email - stop recursive sending and logging if a previous email error was sent
                //if (!email.Subject.StartsWith("Email error ")) Utils.SendError("Email error " + guid, "Email log saved to '" + Settings.Default.Log_Folder + "\\Email%20Error%20" + guid, e);
            }
            catch (Exception ex)
            {
                StringBuilder description = new StringBuilder();
                StringBuilder trace = new StringBuilder();
                StringBuilder data = new StringBuilder();
                Exception inner = ex;
                while (inner != null)
                {
                    description.Append(inner.Message).Append("<BR/>");
                    if (inner.StackTrace != null) trace.Append(inner.StackTrace).Append("<BR/>");
                    if (inner.Data != null)
                        foreach (DictionaryEntry de in inner.Data)
                            data.Append(de.Key + " = " + de.Value + "<BR/>");
                    inner = inner.InnerException;
                }

                using (StreamWriter txt = new StreamWriter(Settings.Default.Log_Folder + "\\Email Error " + "exception" + ".htm"))
                {
                    txt.WriteLine("<B>Error sending email</B> from '" + email.From.Address + "' to '" + email.To[0].Address + "' with subject '" + email.Subject + "'<BR/>");
                    txt.WriteLine("<BR/><B>Error Description:</B><BR/>" + description);
                    txt.WriteLine("<BR/><B>Error Stack Trace:</B><BR/>" + trace);
                    txt.WriteLine("<BR/><B>Additional Info:</B><BR/>" + data);
                    txt.WriteLine("<BR/><B>Email body:</B><BR/>" + email.Body);
                }
                //stop sending email of exception from here to prevent recursive email sending
                //Utils.SendError("Error log file save error", description.ToString() + trace.ToString() + data.ToString(), ex);
            }
            sent = true;
        }

        private static void smtp_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            MailMessage email = e.UserState as MailMessage;
            if (e.Error != null) LogEmailError(e.Error, email);
            sent = true;
        }

        public static string download(string url, string folderPath)
        {
            if (url.Equals("")) throw new ArgumentException("url");

            System.Uri pdfFileUri = new System.Uri(url);

            if (!Directory.Exists(folderPath)) throw new ArgumentException("folderPath");

            String pdfFilePath = Path.Combine(folderPath, pdfFileUri.Segments[pdfFileUri.Segments.Length - 1]);
            if (File.Exists(pdfFilePath)) return pdfFilePath;
            
            FileStream fileStream = null;
            WebRequest request = null;
            HttpWebResponse response = null;
            Stream dataStream = null;
            try
            {
                fileStream = new FileStream(pdfFilePath, FileMode.Create);
                request = WebRequest.Create(pdfFileUri);
                response = (HttpWebResponse)request.GetResponse();
                dataStream = response.GetResponseStream();
                dataStream.ReadTimeout = 5000;
                int readBytesCount = 0;
                byte[] buffer = new byte[64 * 1024];
                do
                {
                    readBytesCount = dataStream.Read(buffer, 0, buffer.Length);
                    fileStream.Write(buffer, 0, readBytesCount);
                } while (readBytesCount > 0);
                return pdfFilePath;
            }
            catch (Exception ex)
            {
                throw new Exception("Download failed: " + ex.Message, ex);
            }
            finally
            {
                if (dataStream != null) dataStream.Close();
                if (fileStream != null) fileStream.Close();
                if (response != null) response.Close();
            }
        }

        [DllImport("shell32.dll")]
        public static extern long FindExecutable(string lpFile, string lpDirectory, [Out] StringBuilder lpResult);

        public static bool IsAdobe(bool messages)
        {
            string file = Path.GetTempFileName() + ".pdf";
            
            Document document = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
            FileStream stream = new FileStream(file, FileMode.Create);
            PdfWriter.GetInstance(document, stream);
            stream.Close();

            StringBuilder objResultBuffer = new StringBuilder(1024);
            long lngResult = 0;

            lngResult = Utils.FindExecutable(file, string.Empty, objResultBuffer);

            if (lngResult >= 32)
            {
                if (!objResultBuffer.ToString().ToLower().Contains("adobe"))
                {
                    if (messages)
                    {
                        MessageBox.Show("Please install Adobe Reader or associate it to pdf files, for the autoprint feature to work\r\nYou can download it from: http://get.adobe.com/reader/");
                    }
                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }
                    return false;
                }
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                return true;
            }
            else
            {
                if (messages)
                {
                    MessageBox.Show("There is no program associated with pdf files");
                }
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                return false;
            }
        }

        [DllImport("user32.dll")]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        private const int SW_SHOWMINIMIZED = 2;
        const int SW_MINIMIZE = 6;
        //const int SW_SHOW = 5;

        public static void printPDF(string pdfFilePath, string folderPath, string printerName)
        {

            try
            {
                //validate the printer
                using (PrintDocument doc = new PrintDocument())
                {
                    // Specify the printer to use.
                    doc.PrinterSettings.PrinterName = printerName;
                    if (!doc.PrinterSettings.IsValid) throw new Exception("Invalid printer");
                }

                using (Process Print = new Process())
                {
                    try
                    {
                        //System.IO.File.AppendAllText("Error.txt", " Set up printing process, search for exe: " + DateTime.Now + "\r\n");

                        StringBuilder objResultBuffer = new StringBuilder(1024);
                        long lngResult = 0;

                        lngResult = FindExecutable(Path.Combine(folderPath, pdfFilePath), "", objResultBuffer);
                       
                        //System.IO.File.AppendAllText("Error.txt", " Program found: " + DateTime.Now + "\r\n");

                        if (lngResult >= 32)
                        {
                            Print.StartInfo.FileName = objResultBuffer.ToString();
                            //Print.StartInfo.FileName = Properties.Settings.Default.AcrobatPath;
                            Print.StartInfo.Arguments = " /t /h \"" + Path.Combine(folderPath, pdfFilePath) + "\" \"" + printerName + "\"";
                            Print.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                            Print.StartInfo.CreateNoWindow = true;
                           // System.IO.File.AppendAllText("Error.txt", " Print start: " + DateTime.Now + "\r\n");
         
                            Print.Start();
                            //System.IO.File.AppendAllText("Error.txt", " Print pdfs sent to printer, wait for print to exit: " + DateTime.Now + "\r\n");
         
                            //Print.WaitForExit(15000);
                            Print.WaitForExit(5000);
                            //System.IO.File.AppendAllText("Error.txt", " Closing print process: " + DateTime.Now + "\r\n");
                            //System.IO.File.AppendAllText("Error.txt", "\r\n***************************\r\n\r\n");
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(objResultBuffer.ToString()))
                            {
                                if (!objResultBuffer.ToString().ToLower().Contains("adobe"))
                                {
                                    MessageBox.Show("Please install Adobe Reader");
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error at printing process: " + ex.Message, ex);
                    }
                    finally
                    {
                        //Process[] procs = Process.GetProcessesByName("acrord32");
                        //if (procs == null || procs.Length <= 0)
                        //{
                            //procs = Process.GetProcessesByName("Acrobat");
                            // The 2nd argument should be the title of window you want to hide.
                            //IntPtr hWnd = FindWindow(null, "Acrobat");
                            //ShowWindowAsync(hWnd, SW_SHOWMINIMIZED);
                            //if (hWnd == IntPtr.Zero)
                            //{
                            //    //ShowWindow(hWnd, SW_SHOW);
                            //    ShowWindow(hWnd, SW_MINIMIZE); // Minimize the window
                            //}

                        //}
                        //else
                        //{
                            // The 2nd argument should be the title of window you want to hide.
                            //hWnd = FindWindow(null, "acrord32");
                            //ShowWindowAsync(hWnd, SW_SHOWMINIMIZED);
                                
                        //if (hWnd == IntPtr.Zero)
                        //    {
                        //        //ShowWindow(hWnd, SW_SHOW);
                        //        ShowWindow(hWnd, SW_MINIMIZE); // Minimize the window
                        //    }
                        //}

                        //do not close adobe until the app closes - could speed up the printing
                            Process[] procs = Process.GetProcessesByName("acrord32");
                            //System.IO.File.AppendAllText("Error.txt", procs.Length.ToString() + "\r\n");
                            if (procs == null || procs.Length <= 0)
                            {
                                procs = Process.GetProcessesByName("Acrobat");
                            }
                        
                        for (int i = procs.Length - 1; i >= 0; i--)
                        {
                            if (!Print.HasExited)
                            {
                                System.IO.File.AppendAllText("Error.txt", "********************************************************");
                             
                                System.IO.File.AppendAllText("Error.txt", "Print process exited, closing main window for process name: " + procs[i].ProcessName + "\r\n");
                               procs[i].CloseMainWindow();
                               System.IO.File.AppendAllText("Error.txt", "Main window closed for process name: " + procs[i].ProcessName + "\r\n");
                             
                               //hWnd = FindWindow(null, procs[i].ProcessName);
                               //System.IO.File.AppendAllText("Error.txt", "Process name: " + procs[i].ProcessName + " - code: " + hWnd + "\r\n");
                             
                               //bool isMin = ShowWindowAsync(hWnd, SW_SHOWMINIMIZED);
                               //System.IO.File.AppendAllText("Error.txt", "Window minimized for process name: " + procs[i].ProcessName + " with result: " + isMin + "\r\n");
                               //hWnd = FindWindow(null, procs[i].ProcessName);
                               //System.IO.File.AppendAllText("Error.txt", "process name: " + procs[i].ProcessName + " - second code: " + hWnd + "\r\n");
                             
                        //        Thread.Sleep(1000);
                        //        if (!procs[i].HasExited)
                        //        {
                        //            procs[i].Kill();
                        //            procs[i].WaitForExit();
                        //        }
                            }
                        //    //else
                        //    //{
                        //    //    Print.WaitForExit();
                        //    //}
                        }
                       
                        Print.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error at printing: " + ex.Message, ex);
            }
        }

        public static void closeAdobe()
        {
            Process[] procs = Process.GetProcessesByName("acrord32");
            if (procs == null || procs.Length <= 0)
            {
                procs = Process.GetProcessesByName("Acrobat");
            }
            for (int i = procs.Length - 1; i >= 0; i--)
            {
                
                    procs[i].CloseMainWindow();
                    //Thread.Sleep(500);
                    if (!procs[i].HasExited)
                    {
                        //procs[i].Kill();
                         procs[i].WaitForExit();
                    }
            }
        }

        public static void openPDF(string url, string folderPath)
        {
            Process.Start(download(url, folderPath));
        }
    }
}
