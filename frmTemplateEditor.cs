﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Text.RegularExpressions;

namespace InteGr8
{
    public partial class frmTemplateEditor : Form
    {
        public enum Action { Add, Edit };

        public readonly Action action;

        // a local working copy dataset to hold only the edited template data
        private readonly Data data = new Data();

        // convenience variable for quick access to the template (there is only one template in the dataset)
        private readonly Data.TemplatesRow Template;

        // the currently edited command (set by the currently selected radio button)
        private Data.StatementsRow Command = null;

        // commands help text
        private readonly string[] CommandsHelp = 
        {
            "The Orders import command is an sql select statement that reads the following shipment level information from your database: Order # (unique), Sender Company, Sender Contact, Sender Tel, Sender Country, Sender State / Province, Sender ZIP / Postal Code, Sender City, Sender Address 1, Recipient Company, Recipient Contact, Recipient Tel, Recipient Country, Recipient State / Province, Recipient ZIP / Postal Code, Recipient City, Recipient Address 1, Ship Date, Carrier, Service, Packaging. You can add any other additional fields. The orders should be filtered by the Status column in the Shipments table to avoid duplicate processing.",
            "The Order Packages import command is an sql select statement that reads the following package information from your database: Order # (link to the parent-shipment), Package # (unique within the shipment), Package Weight, Package Weight Type, Package Length, Package Width, Package Height, Package Dimensions Type, Package Insurance Amount, Package Insurance Currency, Package Reference. You can add any other additional fields. The orders should be filtered by the Status column in the Shipments table to avoid duplicate processing.",
            "The Order Products import command is an sql select statement that reads the following product level information from your database: Order # (link to the parent-shipment), Product # (unique within the shipment), Description, Is Document, Country Manufacture, Quantity, UM, Weight, Weight Type, Unit Value, Value Currency, Total Customs Value, Harmonized Code, Total Weight. You can add any other additional fields. The orders should be filtered by the Status column in the Shipments table to avoid duplicate processing.",
            "The Shipments export command is an sql insert statement that writes the following shipment level information to your database: Order #, Status, Shipment # (unique), Label URL, Commercial Invoice URL, Price, Delivery, Error Code, Error Message. You can specifiy any other additional fields to be saved, if required. The Order # and Status columns are used to link back the results to the original orders and avoid reprocessing the sucessfully executed orders.",
            "The Shipment Packages export command is an sql insert statement that writes the following packages level information to your database: Order #, Package #, Shipment Package # (unique), Price. You can specifiy any other additional fields to be saved, if required. The Package # and Shipment Package # columns are used to link the package to the parent shipment."
        };

        public frmTemplateEditor()
        {
            InitializeComponent();
            action = Action.Add;
            Template = data.Templates.AddTemplatesRow("", "", "", "", "", false, false, false, false, null, false);
            data.Statements.AddStatementsRow(Template, "Orders", "");
            data.Statements.AddStatementsRow(Template, "Order_Packages", "");
            data.Statements.AddStatementsRow(Template, "Order_Products", "");
            data.Statements.AddStatementsRow(Template, "Shipments", "");
            data.Statements.AddStatementsRow(Template, "Shipment_Packages", "");
        }

        public frmTemplateEditor(string Template)
        {
            InitializeComponent();
            action = Action.Edit;
            Data.TemplatesRow TemplateRow = InteGr8_Main.data.Templates.FindByName(Template);
            if (Template == null) throw new Exception("Template not found.");
            data.Templates.ImportRow(TemplateRow);
            foreach (Data.StatementsRow statement in TemplateRow.GetStatementsRows())
            {
                data.Statements.ImportRow(statement);
                foreach (Data.ParametersRow parameter in statement.GetParametersRows())
                    data.Parameters.ImportRow(parameter);
            }
            this.Template = data.Templates[0];
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                dgvFilters.AutoGenerateColumns = false;

                if (action == Action.Add)
                {
                    txtDSN.ReadOnly = false;
                    txtLogin.ReadOnly = false;
                    txtPassword.ReadOnly = false;
                }
                else
                {
                    // show template data
                    this.Text = "InteGr8 Template Editor - " + Template.Name;
                    txtTemplate.Text = Template.Name;
                    txtDescription.Text = Template.Description;

                    // parse connection string
                    try
                    {
                        OdbcConnectionStringBuilder conn_builder = new OdbcConnectionStringBuilder(Template.Connection);
                        txtDSN.Text = conn_builder.Dsn;
                        object obj = null;
                        if (conn_builder.TryGetValue("Uid", out obj)) txtLogin.Text = obj.ToString();
                        if (conn_builder.TryGetValue("Pwd", out obj)) txtPassword.Text = obj.ToString();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, "Incorrect connection string: '" + Template.Connection + "':\n" + ex.Message, "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }

                    // try to connect
                    try
                    {
                        using (OdbcConnection Conn = new OdbcConnection(Template.Connection))
                            Conn.Open();
                        btnConnection.Text = "Connected";
                        btnConnection.Enabled = false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, "Unable to connect to the datasource '" + Template.Name + "':\n" + ex.Message, "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }

                    // enable controls
                    SplitContainer.Enabled = true;
                    btnSave.Enabled = true;
                    btnSQL.Enabled = true;
                }

                // show the current command (Orders)
                rbCommand_CheckedChanged(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error loading template '" + Template + "'.", "Internal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utils.SendError("InteGr8 error in frmTemplateEditor.frmMain_Load", "Error loading template '" + Template + "'.", ex);
                // this.Close();
            }
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            try
            {
                using (OdbcConnection Conn = new OdbcConnection("DSN=" + txtDSN.Text + "; Uid=" + txtLogin.Text + "; Pwd=" + txtPassword.Text))
                    Conn.Open();
                btnConnection.Text = "Connected";
                btnConnection.Enabled = false;
                SplitContainer.Enabled = true;
                btnSave.Enabled = true;
                btnSQL.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Connection failed.");
            }
        }

        private void rbCommand_CheckedChanged(object sender, EventArgs e)
        {
            // save the previous command changes
            if (Command != null) Command.Sql = txtSQL.Text;

            // show the chosen command
            try
            {
                string table = null;
                int index = -1;
                if (rbOrders.Checked)
                {
                    table = "Orders";
                    index = 0;
                }
                else if (rbOrderPackages.Checked)
                {
                    table = "Order_Packages";
                    index = 1;
                }
                else if (rbOrderProducts.Checked)
                {
                    table = "Order_Products";
                    index = 2;
                }
                else if (rbShipments.Checked)
                {
                    table = "Shipments";
                    index = 3;
                }
                else if (rbShipmentPackages.Checked)
                {
                    table = "Shipment_Packages";
                    index = 4;
                }
                
                Command = data.Statements.FindByTemplate_Table(Template.Name, table);
                if (Command == null) throw new Exception("Command not found for table '" + table + " in template '" + Template.Name + "'.");
                
                txtSQL.Text = Command.Sql;

                data.Parameters.DefaultView.RowFilter = "Table = '" + table + "'";
                dgvFilters.DataSource = data.Parameters;
                dgvFilters.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

                lblCommandInfo.Text = CommandsHelp[index];
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error loading command.", "Internal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utils.SendError("InteGr8 error in frmTemplateEditor.rbCommand_CheckedChanged", "Error loading command.", ex);
                // this.Close();
            }
        }

        private void btnSQL_Click(object sender, EventArgs e)
        {
            string conn = Template.Connection.Equals("") ? "DSN=" + txtDSN.Text + "; Uid=" + txtLogin.Text + "; Pwd=" + txtPassword.Text : Template.Connection;

            // AQB does not recognize odbc escape syntax, for example { fn Convert( { fn CURDATE() }, SQL_VARCHAR) }
            // see http://www.activedbsoft.com/helpdesk/index.php?pg=forums.posts&id=3051&pc=2
            // bc of this, { fn is replaced with __fn__ and the final } is replaced with a space
            // this fix is only working for the functions
            // there are some other escape sequences as well, 
            // see http://msdn.microsoft.com/en-us/library/ms711838(VS.85).aspx
            // so the example above will become: __fn__Convert(__fn__CURDATE() , SQL_VARCHAR) 
            string sql = txtSQL.Text;
            sql = new Regex(@"\s*{\s*fn\s*", RegexOptions.IgnoreCase).Replace(sql, "__fn__");
            sql = new Regex(@"\s*}\s*").Replace(sql, " ");
            
            frmSQLDesigner sql_wizard = new frmSQLDesigner(conn, sql);
            if (sql_wizard.ShowDialog(this) == DialogResult.OK)
            {
                // put back the odbc escape sequences 
                sql = sql_wizard.SQL;
                if (sql.Contains("__fn__"))
                {
                    // insert a '}' after the last matching pair of closed round bracket after each __fn__ call
                    int pos = sql.IndexOf("__fn__", 0);
                    do
                    {
                        int first_close = sql.IndexOf(')', pos);
                        int count_open = 0; // how many open brackets are until the first closing bracket
                        for (int c = pos; c < first_close; c++)
                            if (sql[c].Equals('(')) count_open++; //  TODO: optimize with sql.IndexOf('(');

                        // find the last closing bracket (count_open occurences)
                        int last_close = pos;
                        int count_closed = 0;
                        do
                        {
                            last_close = sql.IndexOf(')', last_close) + 1;
                            count_closed++;
                        }
                        while (count_closed < count_open);

                        // insert the '}' escape sequence
                        sql = sql.Insert(last_close, " } ");

                        // go to the next function call
                        pos = sql.IndexOf("__fn__", pos + 1);
                    }
                    while (pos > 0);

                    // put in the '{ fn' escape
                    sql = sql.Replace("__fn__", "{ fn ");
                }

                Command.Sql = sql;
                txtSQL.Text = sql;

                // parse the parameters and reload them into the dataset
                foreach (Data.ParametersRow row in Command.GetParametersRows())
                    data.Parameters.RemoveParametersRow(row);
                string[] Params = sql_wizard.Parameters;
                for (int p = 0; p < Params.Length; p++)
                {
                    string param = Params[p];
                    int x = param.IndexOf(" <");
                    string name = param.Substring(0, x);
                    string type = param.Substring(x + 2, param.Length - x - 3);
                    x = type.IndexOf(" ");
                    int length = Convert.ToInt32(type.Substring(x + 1, type.Length - x - 1));
                    type = type.Substring(0, x);

                    data.Parameters.AddParametersRow(Template.Name, Command._Table, name, p + 1, "");
                }
                dgvFilters.Refresh();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // validate data
            if (txtTemplate.Text.Trim().Equals(""))
            {
                MessageBox.Show(this, "Please enter a template name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtDSN.Text.Trim().Equals(""))
            {
                MessageBox.Show(this, "Please enter the ODBC System DSN.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            foreach(Data.StatementsRow command in Template.GetStatementsRows())
                if (command.Sql.Trim().Equals(""))
                {
                    MessageBox.Show(this, "Please enter the SQL code for the '" + Command._Table + "' command .", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            // save the current statement changes (parameter changes are automatically saved by the grid)
            Command.Sql = txtSQL.Text;

            try
            {
                data.CheckTemplates();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error validating the template: " + ex.Message, "Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // merge the changes back to the original dataset
            try
            {
                // delete the original template first (this will delete the child records too)
                InteGr8_Main.data.Templates.FindByName(Template.Name).Delete(); 

                Template.Name = txtTemplate.Text;
                Template.Description = txtDescription.Text;
                Template.DSN = txtDSN.Text;
                Template.Login = txtLogin.Text;
                Template.Password = txtPassword.Text;

                InteGr8_Main.data.Merge(this.data);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error saving the template.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utils.SendError("InteGr8 Error in frmTemplateEditor.btnSave_Click.", "Error merging the template back to the main dataset.", ex);
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void frmTemplateEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing || this.DialogResult == DialogResult.OK) return;
            e.Cancel = MessageBox.Show(this, "Are you sure you want to cancel?", "Cancel Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No;
        }

        private void dgvFilters_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            if (e.Row.IsNewRow)
            {
                e.Row.Cells["ParameterTemplate"].Value = Template.Name;
                e.Row.Cells["ParameterTable"].Value = Command._Table;
                e.Row.Cells["ParameterPos"].Value = dgvFilters.Rows.Count;
                e.Row.Cells["ParameterDefault"].Value = "";
            }
        }

        private void dgvFilters_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvFilters.Columns[e.ColumnIndex].Name.Equals("ParameterDefault") && dgvFilters.Rows[e.RowIndex].Cells["ParameterDefault"].Value.Equals(DBNull.Value))
                dgvFilters.Rows[e.RowIndex].Cells["ParameterDefault"].Value = "";
        }

        private void dgvFilters_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null) MessageBox.Show(this, e.Exception.Message, "Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.Cancel = true;
        }

    }
}
