﻿namespace InteGr8
{
    partial class frmGetWeight
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetWeight));
            this.lblPackageWeight = new System.Windows.Forms.Label();
            this.tbGetWeight = new System.Windows.Forms.TextBox();
            this.btnGetWeightOK = new System.Windows.Forms.Button();
            this.btnGetWeightCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblPackageWeight
            // 
            this.lblPackageWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPackageWeight.AutoSize = true;
            this.lblPackageWeight.Location = new System.Drawing.Point(68, 31);
            this.lblPackageWeight.Name = "lblPackageWeight";
            this.lblPackageWeight.Size = new System.Drawing.Size(84, 13);
            this.lblPackageWeight.TabIndex = 0;
            this.lblPackageWeight.Text = "Package weight";
            // 
            // tbGetWeight
            // 
            this.tbGetWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGetWeight.Location = new System.Drawing.Point(71, 60);
            this.tbGetWeight.Name = "tbGetWeight";
            this.tbGetWeight.Size = new System.Drawing.Size(145, 20);
            this.tbGetWeight.TabIndex = 1;
            // 
            // btnGetWeightOK
            // 
            this.btnGetWeightOK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetWeightOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnGetWeightOK.Location = new System.Drawing.Point(71, 109);
            this.btnGetWeightOK.Name = "btnGetWeightOK";
            this.btnGetWeightOK.Size = new System.Drawing.Size(96, 26);
            this.btnGetWeightOK.TabIndex = 2;
            this.btnGetWeightOK.Text = "OK";
            this.btnGetWeightOK.UseVisualStyleBackColor = true;
            this.btnGetWeightOK.Click += new System.EventHandler(this.btnGetWeightOK_Click);
            // 
            // btnGetWeightCancel
            // 
            this.btnGetWeightCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetWeightCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnGetWeightCancel.Location = new System.Drawing.Point(190, 109);
            this.btnGetWeightCancel.Name = "btnGetWeightCancel";
            this.btnGetWeightCancel.Size = new System.Drawing.Size(99, 26);
            this.btnGetWeightCancel.TabIndex = 3;
            this.btnGetWeightCancel.Text = "Cancel";
            this.btnGetWeightCancel.UseVisualStyleBackColor = true;
            // 
            // frmGetWeight
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 164);
            this.Controls.Add(this.btnGetWeightCancel);
            this.Controls.Add(this.btnGetWeightOK);
            this.Controls.Add(this.tbGetWeight);
            this.Controls.Add(this.lblPackageWeight);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmGetWeight";
            this.Text = "Package Weight";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPackageWeight;
        private System.Windows.Forms.TextBox tbGetWeight;
        private System.Windows.Forms.Button btnGetWeightOK;
        private System.Windows.Forms.Button btnGetWeightCancel;
    }
}