﻿namespace InteGr8
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            this.OrdersHSplit = new System.Windows.Forms.SplitContainer();
            this.OrdersPanel = new System.Windows.Forms.TableLayoutPanel();
            this.OrdersToolbar = new System.Windows.Forms.ToolStrip();
            this.OrdersRefreshButton = new System.Windows.Forms.ToolStripButton();
            this.OrdersToolbarSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.SelectAllOrdersButton = new System.Windows.Forms.ToolStripButton();
            this.UnselectAllOrdersButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.labelCarrier = new System.Windows.Forms.ToolStripLabel();
            this.cmbMenuCarrier = new System.Windows.Forms.ToolStripComboBox();
            this.labelService = new System.Windows.Forms.ToolStripLabel();
            this.cmbMenuService = new System.Windows.Forms.ToolStripComboBox();
            this.btnApplyCarrierServiceToSelected = new System.Windows.Forms.ToolStripButton();
            this.tsCarrierAndServiceApply = new System.Windows.Forms.ToolStripSeparator();
            this.cmdRateCheapestService = new System.Windows.Forms.ToolStripButton();
            this.cmdRateFastestService = new System.Windows.Forms.ToolStripButton();
            this.cmdRateAllServices = new System.Windows.Forms.ToolStripButton();
            this.cmdRateCurrentService = new System.Windows.Forms.ToolStripButton();
            this.tbCurrentRate = new System.Windows.Forms.ToolStripTextBox();
            this.DimensionalWeightLabel = new System.Windows.Forms.ToolStripLabel();
            this.tbDimensionalWeight = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.OrdersProcessCurrentButton = new System.Windows.Forms.ToolStripButton();
            this.OrdersProcessAllButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.OrdersEditCurrentButton = new System.Windows.Forms.ToolStripButton();
            this.GetEditResults = new System.Windows.Forms.ToolStripButton();
            this.OrdersToolbarSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.OrderSearchTextbox = new System.Windows.Forms.ToolStripTextBox();
            this.OrderSearchButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnManualOrders = new System.Windows.Forms.ToolStripButton();
            this.OrdersLabel = new System.Windows.Forms.Label();
            this.OrdersGrid = new System.Windows.Forms.DataGridView();
            this.chkOrderSelected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtOrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbBillTo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BillingTypesBS = new System.Windows.Forms.BindingSource(this.components);
            this.data = new InteGr8.Data();
            this.cmbOrderCarrier = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CarriersBS = new System.Windows.Forms.BindingSource(this.components);
            this.cmbOrderService = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ServicesBS = new System.Windows.Forms.BindingSource(this.components);
            this.cmbOrderPackaging = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.packagingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtOrderShipDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtOrderRecipientCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtOrderSenderCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrdersTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.FilterTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.FilterLinkLabel = new System.Windows.Forms.LinkLabel();
            this.OrderFiltersLabel = new System.Windows.Forms.Label();
            this.cmbFilterColumns = new System.Windows.Forms.ComboBox();
            this.filterExpressionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelOperator = new System.Windows.Forms.Label();
            this.tbValue1 = new System.Windows.Forms.TextBox();
            this.btnAcceptFilter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbMultiOrder = new System.Windows.Forms.CheckBox();
            this.dtpShipDate = new InteGr8.MyDateTimePicker();
            this.OrdersVSplitBottom = new System.Windows.Forms.SplitContainer();
            this.OrderPackagesPanel = new System.Windows.Forms.TableLayoutPanel();
            this.panelShipType = new System.Windows.Forms.Panel();
            this.btnAddMultiPacks = new System.Windows.Forms.Button();
            this.cmbPackageDims = new System.Windows.Forms.ComboBox();
            this.packageDimsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetWeight = new System.Windows.Forms.Button();
            this.lblTotalWeight = new System.Windows.Forms.Label();
            this.buttonRemovePackage = new System.Windows.Forms.Button();
            this.button_PackagesAdd = new System.Windows.Forms.Button();
            this.rbShipmentLevel = new System.Windows.Forms.RadioButton();
            this.rbPackageLevel = new System.Windows.Forms.RadioButton();
            this.tabPackageType = new System.Windows.Forms.TabControl();
            this.tabPagePackages = new System.Windows.Forms.TabPage();
            this.OrderPackagesGrid = new System.Windows.Forms.DataGridView();
            this.txtPackageOrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPackageNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPackageLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPackageWidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPackageHeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPackageWeightType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPackageDimensionsType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPackageInsuranceAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPackageInsuranceCurrency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPackageReference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fKOrdersTableOrderPackagesTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPageSkids = new System.Windows.Forms.TabPage();
            this.OrderSkidsGrid = new System.Windows.Forms.DataGridView();
            this.orderDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.useSkidsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidCountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidShipmentLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidSkidLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidsCountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidWeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidWeightTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidInsuranceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidInsuranceCurrencyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidLengthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidWidthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidHeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidDimensionsTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidReference1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidReference2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidPONumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidShipmentIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidInvoiceNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipmentPONumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipmentReferenceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidFreightClassDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidNumberOfPackagesInSkidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skidDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bOLCommentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fkOrdersTableOrderSkidsTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPageItems = new System.Windows.Forms.TabPage();
            this.LTLItemsGrid = new System.Windows.Forms.DataGridView();
            this.orderDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemsCountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemTotalWeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemTotalWeightTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemTotalLengthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemTotalWidthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemTotalHeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemTotalDimsTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemFreightClassIDDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lTLItemQuantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lTLItemUMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fkOrdersTableLTLItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelPackageFields = new System.Windows.Forms.Panel();
            this.btnSetFieldValue = new System.Windows.Forms.Button();
            this.rbSetFieldAll = new System.Windows.Forms.RadioButton();
            this.rbSetForMissing = new System.Windows.Forms.RadioButton();
            this.rbFieldCurrent = new System.Windows.Forms.RadioButton();
            this.txtPackageFieldValue = new System.Windows.Forms.TextBox();
            this.lblPackageFieldValue = new System.Windows.Forms.Label();
            this.cbPackageField = new System.Windows.Forms.ComboBox();
            this.lblPackageField = new System.Windows.Forms.Label();
            this.ProductsPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ProductsGrid = new System.Windows.Forms.DataGridView();
            this.txtProductOrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtProductNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkProductIsDocument = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtProductCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtProductQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtProductMU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtProductUnitWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtProductUnitValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtProductCustomsValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtProductTotalWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtProductHarmonizedCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fKOrdersTableOrderProductsTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pnlCommoditiesTop = new System.Windows.Forms.Panel();
            this.btnScanProducts = new System.Windows.Forms.Button();
            this.cmdCommodityDelete = new System.Windows.Forms.Button();
            this.ProductsLabel = new System.Windows.Forms.Label();
            this.filterLeftOperandNameBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CarrierPackagingsBS = new System.Windows.Forms.BindingSource(this.components);
            this.CarrierPackagingsBSFiltered = new System.Windows.Forms.BindingSource(this.components);
            this.ServicesBSFiltered = new System.Windows.Forms.BindingSource(this.components);
            this.fKOrdersTableRequestsTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MainPanel = new System.Windows.Forms.ToolStripContainer();
            this.MainStatusbar = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainPages = new System.Windows.Forms.TabControl();
            this.OrdersPage = new System.Windows.Forms.TabPage();
            this.TwoShipPage = new System.Windows.Forms.TabPage();
            //this.Browser = new InteGr8.ExtendedWebBrowser();
            this.ShipmentsPage = new System.Windows.Forms.TabPage();
            this.ShipmentsToolBar = new System.Windows.Forms.ToolStrip();
            this.ShipmentsLabelButton = new System.Windows.Forms.ToolStripButton();
            this.ShipmentsCIButton = new System.Windows.Forms.ToolStripButton();
            this.ShipmentsBOLPrint = new System.Windows.Forms.ToolStripButton();
            this.ShipmentsReturnLabel = new System.Windows.Forms.ToolStripButton();
            this.btn_shipments_packslip = new System.Windows.Forms.ToolStripButton();
            this.btn_shipments_contentslabel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_select_all = new System.Windows.Forms.ToolStripButton();
            this.btn_unselect_all = new System.Windows.Forms.ToolStripButton();
            this.btnEditRates = new System.Windows.Forms.ToolStripButton();
            this.btn_close = new System.Windows.Forms.ToolStripButton();
            this.btn_aggregate = new System.Windows.Forms.ToolStripButton();
            this.ShipmentsSave = new System.Windows.Forms.ToolStripButton();
            this.ShipmentsDelete = new System.Windows.Forms.ToolStripButton();
            this.btn_clear = new System.Windows.Forms.ToolStripButton();
            this.ShipmentsGrid = new System.Windows.Forms.DataGridView();
            this.ShipmentsSelectedColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Shipmentsorder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Package_Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Shipmentsstatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Shipment_Tracking_Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipmentslabelURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commercialInvoiceURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipmentsCarrier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipmentsService = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.delivery = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CarrierCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServiceCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipmentsTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ProcessedOrdersPage = new System.Windows.Forms.TabPage();
            this.HistoryGrid = new System.Windows.Forms.DataGridView();
            this.Selected_ord = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.HistoryOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HistoryTN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Token = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CIURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOLURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnLabelURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientCompany = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientAddress1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientAddress2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientZIP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Carrier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Service = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HistoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.HistoryToolbar = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHistoryLabel = new System.Windows.Forms.ToolStripButton();
            this.btnHistoryCI = new System.Windows.Forms.ToolStripButton();
            this.btnHistoryBOLPrint = new System.Windows.Forms.ToolStripButton();
            this.btnHistoryReturnLabel = new System.Windows.Forms.ToolStripButton();
            this.btn_history_packslip = new System.Windows.Forms.ToolStripButton();
            this.btn_selectall = new System.Windows.Forms.ToolStripButton();
            this.btn_unselectall = new System.Windows.Forms.ToolStripButton();
            this.HistoryDeleteButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.HistorySearchTextbox = new System.Windows.Forms.ToolStripTextBox();
            this.HistorySearchButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.TemplatesToolstrip = new System.Windows.Forms.ToolStrip();
            this.TemplatesLabel = new System.Windows.Forms.ToolStripLabel();
            this.TemplatesDropdown = new System.Windows.Forms.ToolStripComboBox();
            this.TemplatesSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.TemplatesNew = new System.Windows.Forms.ToolStripButton();
            this.TemplatesEdit = new System.Windows.Forms.ToolStripButton();
            this.TemplatesDelete = new System.Windows.Forms.ToolStripButton();
            this.TemplatesSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.TemplatesImport = new System.Windows.Forms.ToolStripButton();
            this.TemplatesExport = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.Options = new System.Windows.Forms.ToolStripButton();
            this.tsButtonPackslip = new System.Windows.Forms.ToolStripButton();
            this.ordersTableAdapter = new InteGr8.DataTableAdapters.OrdersTableAdapter();
            this.orderPackagesTableAdapter = new InteGr8.DataTableAdapters.OrderPackagesTableAdapter();
            this.orderProductsTableAdapter = new InteGr8.DataTableAdapters.OrderProductsTableAdapter();
            this.TaskBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.FillBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.chkShipmentSelected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtShipmentOrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.skidFreightClassDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bg_print = new System.ComponentModel.BackgroundWorker();
            this.bgworker_save = new System.ComponentModel.BackgroundWorker();
            this.bsMenuServicesFiltered = new System.Windows.Forms.BindingSource(this.components);
            this.bsMenuServices = new System.Windows.Forms.BindingSource(this.components);
            this.bsMenuCarriers = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersHSplit)).BeginInit();
            this.OrdersHSplit.Panel1.SuspendLayout();
            this.OrdersHSplit.Panel2.SuspendLayout();
            this.OrdersHSplit.SuspendLayout();
            this.OrdersPanel.SuspendLayout();
            this.OrdersToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingTypesBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.data)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarriersBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServicesBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packagingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersTableBindingSource)).BeginInit();
            this.FilterTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filterExpressionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersVSplitBottom)).BeginInit();
            this.OrdersVSplitBottom.Panel1.SuspendLayout();
            this.OrdersVSplitBottom.Panel2.SuspendLayout();
            this.OrdersVSplitBottom.SuspendLayout();
            this.OrderPackagesPanel.SuspendLayout();
            this.panelShipType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.packageDimsBindingSource)).BeginInit();
            this.tabPackageType.SuspendLayout();
            this.tabPagePackages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrderPackagesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOrdersTableOrderPackagesTableBindingSource)).BeginInit();
            this.tabPageSkids.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrderSkidsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fkOrdersTableOrderSkidsTableBindingSource)).BeginInit();
            this.tabPageItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LTLItemsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fkOrdersTableLTLItemsBindingSource)).BeginInit();
            this.panelPackageFields.SuspendLayout();
            this.ProductsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProductsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOrdersTableOrderProductsTableBindingSource)).BeginInit();
            this.pnlCommoditiesTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filterLeftOperandNameBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarrierPackagingsBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarrierPackagingsBSFiltered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServicesBSFiltered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOrdersTableRequestsTableBindingSource)).BeginInit();
            this.MainPanel.BottomToolStripPanel.SuspendLayout();
            this.MainPanel.ContentPanel.SuspendLayout();
            this.MainPanel.TopToolStripPanel.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this.MainStatusbar.SuspendLayout();
            this.MainPages.SuspendLayout();
            this.OrdersPage.SuspendLayout();
            this.TwoShipPage.SuspendLayout();
            this.ShipmentsPage.SuspendLayout();
            this.ShipmentsToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShipmentsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipmentsTableBindingSource)).BeginInit();
            this.ProcessedOrdersPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryBindingSource)).BeginInit();
            this.HistoryToolbar.SuspendLayout();
            this.TemplatesToolstrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsMenuServicesFiltered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsMenuServices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsMenuCarriers)).BeginInit();
            this.SuspendLayout();
            // 
            // OrdersHSplit
            // 
            this.OrdersHSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrdersHSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.OrdersHSplit.Location = new System.Drawing.Point(3, 3);
            this.OrdersHSplit.Name = "OrdersHSplit";
            this.OrdersHSplit.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // OrdersHSplit.Panel1
            // 
            this.OrdersHSplit.Panel1.Controls.Add(this.OrdersPanel);
            this.OrdersHSplit.Panel1MinSize = 150;
            // 
            // OrdersHSplit.Panel2
            // 
            this.OrdersHSplit.Panel2.Controls.Add(this.OrdersVSplitBottom);
            this.OrdersHSplit.Panel2MinSize = 150;
            this.OrdersHSplit.Size = new System.Drawing.Size(1008, 410);
            this.OrdersHSplit.SplitterDistance = 166;
            this.OrdersHSplit.SplitterWidth = 5;
            this.OrdersHSplit.TabIndex = 0;
            // 
            // OrdersPanel
            // 
            this.OrdersPanel.ColumnCount = 1;
            this.OrdersPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.OrdersPanel.Controls.Add(this.OrdersToolbar, 0, 0);
            this.OrdersPanel.Controls.Add(this.OrdersLabel, 0, 2);
            this.OrdersPanel.Controls.Add(this.OrdersGrid, 0, 3);
            this.OrdersPanel.Controls.Add(this.FilterTableLayoutPanel, 0, 1);
            this.OrdersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrdersPanel.Location = new System.Drawing.Point(0, 0);
            this.OrdersPanel.Name = "OrdersPanel";
            this.OrdersPanel.RowCount = 5;
            this.OrdersPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.OrdersPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.OrdersPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.OrdersPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.OrdersPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.OrdersPanel.Size = new System.Drawing.Size(1008, 166);
            this.OrdersPanel.TabIndex = 1;
            // 
            // OrdersToolbar
            // 
            this.OrdersToolbar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrdersToolbar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.OrdersToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OrdersRefreshButton,
            this.OrdersToolbarSeparator1,
            this.SelectAllOrdersButton,
            this.UnselectAllOrdersButton,
            this.toolStripSeparator5,
            this.labelCarrier,
            this.cmbMenuCarrier,
            this.labelService,
            this.cmbMenuService,
            this.btnApplyCarrierServiceToSelected,
            this.tsCarrierAndServiceApply,
            this.cmdRateCheapestService,
            this.cmdRateFastestService,
            this.cmdRateAllServices,
            this.cmdRateCurrentService,
            this.tbCurrentRate,
            this.DimensionalWeightLabel,
            this.tbDimensionalWeight,
            this.toolStripSeparator3,
            this.OrdersProcessCurrentButton,
            this.OrdersProcessAllButton,
            this.toolStripSeparator6,
            this.OrdersEditCurrentButton,
            this.GetEditResults,
            this.OrdersToolbarSeparator2,
            this.toolStripLabel2,
            this.OrderSearchTextbox,
            this.OrderSearchButton,
            this.toolStripSeparator1,
            this.btnManualOrders});
            this.OrdersToolbar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.OrdersToolbar.Location = new System.Drawing.Point(0, 0);
            this.OrdersToolbar.Name = "OrdersToolbar";
            this.OrdersToolbar.Padding = new System.Windows.Forms.Padding(0);
            this.OrdersToolbar.Size = new System.Drawing.Size(1008, 62);
            this.OrdersToolbar.Stretch = true;
            this.OrdersToolbar.TabIndex = 0;
            // 
            // OrdersRefreshButton
            // 
            this.OrdersRefreshButton.Image = ((System.Drawing.Image)(resources.GetObject("OrdersRefreshButton.Image")));
            this.OrdersRefreshButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OrdersRefreshButton.Name = "OrdersRefreshButton";
            this.OrdersRefreshButton.Size = new System.Drawing.Size(71, 28);
            this.OrdersRefreshButton.Text = "Reload";
            this.OrdersRefreshButton.ToolTipText = "Re-read all unprocessed orders from your database";
            this.OrdersRefreshButton.Click += new System.EventHandler(this.OrdersRefreshButton_Click);
            // 
            // OrdersToolbarSeparator1
            // 
            this.OrdersToolbarSeparator1.Name = "OrdersToolbarSeparator1";
            this.OrdersToolbarSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // SelectAllOrdersButton
            // 
            this.SelectAllOrdersButton.Image = ((System.Drawing.Image)(resources.GetObject("SelectAllOrdersButton.Image")));
            this.SelectAllOrdersButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SelectAllOrdersButton.Name = "SelectAllOrdersButton";
            this.SelectAllOrdersButton.Size = new System.Drawing.Size(81, 28);
            this.SelectAllOrdersButton.Text = "Select all";
            this.SelectAllOrdersButton.Click += new System.EventHandler(this.SelectAllOrdersButton_Click);
            // 
            // UnselectAllOrdersButton
            // 
            this.UnselectAllOrdersButton.Image = ((System.Drawing.Image)(resources.GetObject("UnselectAllOrdersButton.Image")));
            this.UnselectAllOrdersButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UnselectAllOrdersButton.Name = "UnselectAllOrdersButton";
            this.UnselectAllOrdersButton.Size = new System.Drawing.Size(95, 28);
            this.UnselectAllOrdersButton.Text = "Unselect all";
            this.UnselectAllOrdersButton.Click += new System.EventHandler(this.UnselectAllOrdersButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 23);
            // 
            // labelCarrier
            // 
            this.labelCarrier.Name = "labelCarrier";
            this.labelCarrier.Size = new System.Drawing.Size(42, 15);
            this.labelCarrier.Text = "Carrier";
            this.labelCarrier.Visible = false;
            // 
            // cmbMenuCarrier
            // 
            this.cmbMenuCarrier.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbMenuCarrier.Name = "cmbMenuCarrier";
            this.cmbMenuCarrier.Size = new System.Drawing.Size(121, 23);
            this.cmbMenuCarrier.Visible = false;
            this.cmbMenuCarrier.SelectedIndexChanged += new System.EventHandler(this.cmbMenuCarrier_SelectedIndexChanged);
            this.cmbMenuCarrier.Click += new System.EventHandler(this.cmbMenuCarrier_Click);
            // 
            // labelService
            // 
            this.labelService.Name = "labelService";
            this.labelService.Size = new System.Drawing.Size(44, 15);
            this.labelService.Text = "Service";
            this.labelService.Visible = false;
            // 
            // cmbMenuService
            // 
            this.cmbMenuService.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbMenuService.Name = "cmbMenuService";
            this.cmbMenuService.Size = new System.Drawing.Size(150, 23);
            this.cmbMenuService.Visible = false;
            // 
            // btnApplyCarrierServiceToSelected
            // 
            this.btnApplyCarrierServiceToSelected.Image = ((System.Drawing.Image)(resources.GetObject("btnApplyCarrierServiceToSelected.Image")));
            this.btnApplyCarrierServiceToSelected.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnApplyCarrierServiceToSelected.Name = "btnApplyCarrierServiceToSelected";
            this.btnApplyCarrierServiceToSelected.Size = new System.Drawing.Size(127, 28);
            this.btnApplyCarrierServiceToSelected.Text = "Apply to Selected";
            this.btnApplyCarrierServiceToSelected.ToolTipText = "Apply selected carrrier & service to selected orders";
            this.btnApplyCarrierServiceToSelected.Visible = false;
            this.btnApplyCarrierServiceToSelected.Click += new System.EventHandler(this.btnApplyCarrierServiceToSelected_Click);
            // 
            // tsCarrierAndServiceApply
            // 
            this.tsCarrierAndServiceApply.Name = "tsCarrierAndServiceApply";
            this.tsCarrierAndServiceApply.Size = new System.Drawing.Size(6, 23);
            this.tsCarrierAndServiceApply.Visible = false;
            // 
            // cmdRateCheapestService
            // 
            this.cmdRateCheapestService.Image = ((System.Drawing.Image)(resources.GetObject("cmdRateCheapestService.Image")));
            this.cmdRateCheapestService.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdRateCheapestService.Name = "cmdRateCheapestService";
            this.cmdRateCheapestService.Size = new System.Drawing.Size(84, 28);
            this.cmdRateCheapestService.Text = "Cheapest";
            this.cmdRateCheapestService.ToolTipText = "Rate all selected orders and automatically select the cheapest carrier service";
            this.cmdRateCheapestService.Click += new System.EventHandler(this.cmdRateCheapestService_Click);
            // 
            // cmdRateFastestService
            // 
            this.cmdRateFastestService.Image = ((System.Drawing.Image)(resources.GetObject("cmdRateFastestService.Image")));
            this.cmdRateFastestService.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdRateFastestService.Name = "cmdRateFastestService";
            this.cmdRateFastestService.Size = new System.Drawing.Size(71, 28);
            this.cmdRateFastestService.Text = "Fastest";
            this.cmdRateFastestService.ToolTipText = "Rate all selected orders and automatically select the fastest carrier service";
            this.cmdRateFastestService.Click += new System.EventHandler(this.cmdRateFastestService_Click);
            // 
            // cmdRateAllServices
            // 
            this.cmdRateAllServices.Image = ((System.Drawing.Image)(resources.GetObject("cmdRateAllServices.Image")));
            this.cmdRateAllServices.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdRateAllServices.Name = "cmdRateAllServices";
            this.cmdRateAllServices.Size = new System.Drawing.Size(93, 28);
            this.cmdRateAllServices.Text = "All services";
            this.cmdRateAllServices.ToolTipText = "Rate and display all available carrier services for the current order";
            this.cmdRateAllServices.Click += new System.EventHandler(this.cmdRateAllServices_Click);
            // 
            // cmdRateCurrentService
            // 
            this.cmdRateCurrentService.Image = ((System.Drawing.Image)(resources.GetObject("cmdRateCurrentService.Image")));
            this.cmdRateCurrentService.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdRateCurrentService.Name = "cmdRateCurrentService";
            this.cmdRateCurrentService.Size = new System.Drawing.Size(101, 28);
            this.cmdRateCurrentService.Text = "Rate Current";
            this.cmdRateCurrentService.Click += new System.EventHandler(this.cmdRateCurrentService_Click);
            // 
            // tbCurrentRate
            // 
            this.tbCurrentRate.Enabled = false;
            this.tbCurrentRate.Name = "tbCurrentRate";
            this.tbCurrentRate.Size = new System.Drawing.Size(100, 23);
            // 
            // DimensionalWeightLabel
            // 
            this.DimensionalWeightLabel.Name = "DimensionalWeightLabel";
            this.DimensionalWeightLabel.Size = new System.Drawing.Size(91, 15);
            this.DimensionalWeightLabel.Text = "Charged weight";
            // 
            // tbDimensionalWeight
            // 
            this.tbDimensionalWeight.Enabled = false;
            this.tbDimensionalWeight.Name = "tbDimensionalWeight";
            this.tbDimensionalWeight.Size = new System.Drawing.Size(100, 23);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // OrdersProcessCurrentButton
            // 
            this.OrdersProcessCurrentButton.Image = ((System.Drawing.Image)(resources.GetObject("OrdersProcessCurrentButton.Image")));
            this.OrdersProcessCurrentButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OrdersProcessCurrentButton.Name = "OrdersProcessCurrentButton";
            this.OrdersProcessCurrentButton.Size = new System.Drawing.Size(58, 28);
            this.OrdersProcessCurrentButton.Text = "Ship";
            this.OrdersProcessCurrentButton.ToolTipText = "Process only the current order";
            this.OrdersProcessCurrentButton.Click += new System.EventHandler(this.OrdersProcessCurrentButton_Click);
            // 
            // OrdersProcessAllButton
            // 
            this.OrdersProcessAllButton.Image = ((System.Drawing.Image)(resources.GetObject("OrdersProcessAllButton.Image")));
            this.OrdersProcessAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OrdersProcessAllButton.Name = "OrdersProcessAllButton";
            this.OrdersProcessAllButton.Size = new System.Drawing.Size(73, 28);
            this.OrdersProcessAllButton.Text = "Ship all";
            this.OrdersProcessAllButton.ToolTipText = "Process all selected orders";
            this.OrdersProcessAllButton.Click += new System.EventHandler(this.OrdersProcessAllButton_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 23);
            // 
            // OrdersEditCurrentButton
            // 
            this.OrdersEditCurrentButton.Image = ((System.Drawing.Image)(resources.GetObject("OrdersEditCurrentButton.Image")));
            this.OrdersEditCurrentButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OrdersEditCurrentButton.Name = "OrdersEditCurrentButton";
            this.OrdersEditCurrentButton.Size = new System.Drawing.Size(55, 28);
            this.OrdersEditCurrentButton.Text = "Edit";
            this.OrdersEditCurrentButton.ToolTipText = "Open the current order for editing into the embedded 2Ship webpage";
            this.OrdersEditCurrentButton.Click += new System.EventHandler(this.OrdersEditCurrentButton_Click);
            // 
            // GetEditResults
            // 
            this.GetEditResults.Image = ((System.Drawing.Image)(resources.GetObject("GetEditResults.Image")));
            this.GetEditResults.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.GetEditResults.Name = "GetEditResults";
            this.GetEditResults.Size = new System.Drawing.Size(113, 28);
            this.GetEditResults.Text = "Get edit results";
            this.GetEditResults.ToolTipText = "Get back the shipment results for all selected orders that were previously edited" +
    " in 2Ship";
            this.GetEditResults.Click += new System.EventHandler(this.GetEditResults_Click);
            // 
            // OrdersToolbarSeparator2
            // 
            this.OrdersToolbarSeparator2.Name = "OrdersToolbarSeparator2";
            this.OrdersToolbarSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(45, 15);
            this.toolStripLabel2.Text = "Search:";
            // 
            // OrderSearchTextbox
            // 
            this.OrderSearchTextbox.AutoToolTip = true;
            this.OrderSearchTextbox.Name = "OrderSearchTextbox";
            this.OrderSearchTextbox.Size = new System.Drawing.Size(80, 23);
            this.OrderSearchTextbox.ToolTipText = "Enter text to search for";
            this.OrderSearchTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OrderSearchTextbox_KeyPress);
            this.OrderSearchTextbox.TextChanged += new System.EventHandler(this.OrderSearchTextbox_TextChanged);
            // 
            // OrderSearchButton
            // 
            this.OrderSearchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OrderSearchButton.Image = ((System.Drawing.Image)(resources.GetObject("OrderSearchButton.Image")));
            this.OrderSearchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OrderSearchButton.Name = "OrderSearchButton";
            this.OrderSearchButton.Size = new System.Drawing.Size(28, 28);
            this.OrderSearchButton.Text = "Search for the entered processed order # in your database";
            this.OrderSearchButton.ToolTipText = "Search for the entered text in the current orders list";
            this.OrderSearchButton.Click += new System.EventHandler(this.OrderSearchButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // btnManualOrders
            // 
            this.btnManualOrders.Image = ((System.Drawing.Image)(resources.GetObject("btnManualOrders.Image")));
            this.btnManualOrders.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnManualOrders.Name = "btnManualOrders";
            this.btnManualOrders.Size = new System.Drawing.Size(110, 28);
            this.btnManualOrders.Text = "Add shipment";
            this.btnManualOrders.ToolTipText = "Add a manual shipment";
            this.btnManualOrders.Visible = false;
            this.btnManualOrders.Click += new System.EventHandler(this.btnManualOrders_Click);
            // 
            // OrdersLabel
            // 
            this.OrdersLabel.AutoEllipsis = true;
            this.OrdersLabel.AutoSize = true;
            this.OrdersLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrdersLabel.Location = new System.Drawing.Point(5, 131);
            this.OrdersLabel.Margin = new System.Windows.Forms.Padding(5);
            this.OrdersLabel.Name = "OrdersLabel";
            this.OrdersLabel.Size = new System.Drawing.Size(998, 13);
            this.OrdersLabel.TabIndex = 3;
            this.OrdersLabel.Text = "Orders waiting to be shipped:";
            this.OrdersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // OrdersGrid
            // 
            this.OrdersGrid.AllowUserToAddRows = false;
            this.OrdersGrid.AllowUserToDeleteRows = false;
            this.OrdersGrid.AllowUserToOrderColumns = true;
            this.OrdersGrid.AllowUserToResizeRows = false;
            this.OrdersGrid.AutoGenerateColumns = false;
            this.OrdersGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrdersGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.OrdersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrdersGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chkOrderSelected,
            this.txtOrderNumber,
            this.cmbBillTo,
            this.cmbOrderCarrier,
            this.cmbOrderService,
            this.cmbOrderPackaging,
            this.txtOrderShipDate,
            this.Status,
            this.txtOrderRecipientCountry,
            this.txtOrderSenderCountry});
            this.OrdersGrid.DataSource = this.OrdersTableBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.OrdersGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.OrdersGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrdersGrid.Location = new System.Drawing.Point(3, 152);
            this.OrdersGrid.MultiSelect = false;
            this.OrdersGrid.Name = "OrdersGrid";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrdersGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.OrdersGrid.RowHeadersWidth = 75;
            this.OrdersGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.OrdersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.OrdersGrid.Size = new System.Drawing.Size(1002, 11);
            this.OrdersGrid.TabIndex = 0;
            this.OrdersGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.OrdersGrid_CellBeginEdit);
            this.OrdersGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.OrdersGrid_CellEndEdit);
            this.OrdersGrid.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.OrdersGrid_CellMouseDoubleClick);
            this.OrdersGrid.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.OrdersGrid_ColumnHeaderMouseClick);
            this.OrdersGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.OrdersGrid_DataError);
            this.OrdersGrid.SelectionChanged += new System.EventHandler(this.OrdersGrid_SelectionChanged);
            // 
            // chkOrderSelected
            // 
            this.chkOrderSelected.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.chkOrderSelected.DataPropertyName = "Selected";
            this.chkOrderSelected.FalseValue = "false";
            this.chkOrderSelected.HeaderText = "Select";
            this.chkOrderSelected.Name = "chkOrderSelected";
            this.chkOrderSelected.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chkOrderSelected.TrueValue = "true";
            this.chkOrderSelected.Width = 62;
            // 
            // txtOrderNumber
            // 
            this.txtOrderNumber.DataPropertyName = "Order #";
            this.txtOrderNumber.HeaderText = "Order #";
            this.txtOrderNumber.Name = "txtOrderNumber";
            this.txtOrderNumber.ReadOnly = true;
            // 
            // cmbBillTo
            // 
            this.cmbBillTo.DataPropertyName = "Billing Type";
            this.cmbBillTo.DataSource = this.BillingTypesBS;
            this.cmbBillTo.DisplayMember = "Name";
            this.cmbBillTo.DisplayStyleForCurrentCellOnly = true;
            this.cmbBillTo.HeaderText = "Bill To";
            this.cmbBillTo.Name = "cmbBillTo";
            this.cmbBillTo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cmbBillTo.ValueMember = "Id";
            // 
            // BillingTypesBS
            // 
            this.BillingTypesBS.DataMember = "Billing_Types";
            this.BillingTypesBS.DataSource = this.data;
            this.BillingTypesBS.PositionChanged += new System.EventHandler(this.BillingTypesBS_PositionChanged);
            // 
            // data
            // 
            this.data.DataSetName = "Data";
            this.data.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cmbOrderCarrier
            // 
            this.cmbOrderCarrier.DataPropertyName = "Carrier";
            this.cmbOrderCarrier.DataSource = this.CarriersBS;
            this.cmbOrderCarrier.DisplayMember = "Name";
            this.cmbOrderCarrier.DisplayStyleForCurrentCellOnly = true;
            this.cmbOrderCarrier.HeaderText = "Carrier";
            this.cmbOrderCarrier.Name = "cmbOrderCarrier";
            this.cmbOrderCarrier.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cmbOrderCarrier.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cmbOrderCarrier.ValueMember = "Carrier_Code";
            // 
            // CarriersBS
            // 
            this.CarriersBS.DataMember = "Carriers";
            this.CarriersBS.DataSource = this.data;
            // 
            // cmbOrderService
            // 
            this.cmbOrderService.DataPropertyName = "Service";
            this.cmbOrderService.DataSource = this.ServicesBS;
            this.cmbOrderService.DisplayMember = "Name";
            this.cmbOrderService.DisplayStyleForCurrentCellOnly = true;
            this.cmbOrderService.HeaderText = "Service";
            this.cmbOrderService.Name = "cmbOrderService";
            this.cmbOrderService.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cmbOrderService.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cmbOrderService.ValueMember = "Unique_Code";
            // 
            // ServicesBS
            // 
            this.ServicesBS.DataMember = "Services";
            this.ServicesBS.DataSource = this.data;
            // 
            // cmbOrderPackaging
            // 
            this.cmbOrderPackaging.DataPropertyName = "Packaging";
            this.cmbOrderPackaging.DataSource = this.packagingsBindingSource;
            this.cmbOrderPackaging.DisplayMember = "Name";
            this.cmbOrderPackaging.DisplayStyleForCurrentCellOnly = true;
            this.cmbOrderPackaging.HeaderText = "Packaging";
            this.cmbOrderPackaging.Name = "cmbOrderPackaging";
            this.cmbOrderPackaging.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cmbOrderPackaging.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cmbOrderPackaging.ValueMember = "Packaging_Code";
            // 
            // packagingsBindingSource
            // 
            this.packagingsBindingSource.DataMember = "Packagings";
            this.packagingsBindingSource.DataSource = this.data;
            // 
            // txtOrderShipDate
            // 
            this.txtOrderShipDate.DataPropertyName = "Ship Date";
            this.txtOrderShipDate.HeaderText = "Ship Date";
            this.txtOrderShipDate.Name = "txtOrderShipDate";
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            // 
            // txtOrderRecipientCountry
            // 
            this.txtOrderRecipientCountry.DataPropertyName = "Recipient Country";
            this.txtOrderRecipientCountry.HeaderText = "Recipient Country";
            this.txtOrderRecipientCountry.Name = "txtOrderRecipientCountry";
            // 
            // txtOrderSenderCountry
            // 
            this.txtOrderSenderCountry.DataPropertyName = "Sender Country";
            this.txtOrderSenderCountry.HeaderText = "Sender Country";
            this.txtOrderSenderCountry.Name = "txtOrderSenderCountry";
            // 
            // OrdersTableBindingSource
            // 
            this.OrdersTableBindingSource.DataMember = "OrdersTable";
            this.OrdersTableBindingSource.DataSource = this.data;
            this.OrdersTableBindingSource.Filter = "Status <> \'Shipped\'";
            // 
            // FilterTableLayoutPanel
            // 
            this.FilterTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FilterTableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.FilterTableLayoutPanel.ColumnCount = 5;
            this.FilterTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.85828F));
            this.FilterTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.886228F));
            this.FilterTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.83034F));
            this.FilterTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.45309F));
            this.FilterTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.81781F));
            this.FilterTableLayoutPanel.Controls.Add(this.FilterLinkLabel, 3, 0);
            this.FilterTableLayoutPanel.Controls.Add(this.OrderFiltersLabel, 0, 0);
            this.FilterTableLayoutPanel.Controls.Add(this.cmbFilterColumns, 0, 1);
            this.FilterTableLayoutPanel.Controls.Add(this.labelOperator, 1, 1);
            this.FilterTableLayoutPanel.Controls.Add(this.tbValue1, 2, 1);
            this.FilterTableLayoutPanel.Controls.Add(this.btnAcceptFilter, 3, 1);
            this.FilterTableLayoutPanel.Controls.Add(this.label1, 2, 0);
            this.FilterTableLayoutPanel.Controls.Add(this.cbMultiOrder, 4, 1);
            this.FilterTableLayoutPanel.Controls.Add(this.dtpShipDate, 4, 0);
            this.FilterTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FilterTableLayoutPanel.Location = new System.Drawing.Point(3, 65);
            this.FilterTableLayoutPanel.Name = "FilterTableLayoutPanel";
            this.FilterTableLayoutPanel.RowCount = 2;
            this.FilterTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.FilterTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.FilterTableLayoutPanel.Size = new System.Drawing.Size(1002, 58);
            this.FilterTableLayoutPanel.TabIndex = 2;
            this.FilterTableLayoutPanel.Visible = false;
            // 
            // FilterLinkLabel
            // 
            this.FilterLinkLabel.AutoSize = true;
            this.FilterLinkLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FilterLinkLabel.Location = new System.Drawing.Point(627, 0);
            this.FilterLinkLabel.Margin = new System.Windows.Forms.Padding(0);
            this.FilterLinkLabel.Name = "FilterLinkLabel";
            this.FilterLinkLabel.Size = new System.Drawing.Size(235, 29);
            this.FilterLinkLabel.TabIndex = 5;
            this.FilterLinkLabel.TabStop = true;
            this.FilterLinkLabel.Text = "Hover here to see the entire filter expression";
            this.FilterLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OrderFiltersLabel
            // 
            this.OrderFiltersLabel.AutoEllipsis = true;
            this.OrderFiltersLabel.AutoSize = true;
            this.OrderFiltersLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrderFiltersLabel.Location = new System.Drawing.Point(0, 0);
            this.OrderFiltersLabel.Margin = new System.Windows.Forms.Padding(0);
            this.OrderFiltersLabel.Name = "OrderFiltersLabel";
            this.OrderFiltersLabel.Size = new System.Drawing.Size(209, 29);
            this.OrderFiltersLabel.TabIndex = 1;
            this.OrderFiltersLabel.Text = "Filter orders by:";
            this.OrderFiltersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbFilterColumns
            // 
            this.cmbFilterColumns.DataSource = this.filterExpressionBindingSource;
            this.cmbFilterColumns.DisplayMember = "Left_Operand";
            this.cmbFilterColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbFilterColumns.FormattingEnabled = true;
            this.cmbFilterColumns.Location = new System.Drawing.Point(3, 32);
            this.cmbFilterColumns.Name = "cmbFilterColumns";
            this.cmbFilterColumns.Size = new System.Drawing.Size(203, 21);
            this.cmbFilterColumns.TabIndex = 3;
            this.cmbFilterColumns.ValueMember = "Left_Operand";
            this.cmbFilterColumns.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cmbFilterColumns_Format);
            // 
            // labelOperator
            // 
            this.labelOperator.AutoSize = true;
            this.labelOperator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelOperator.Location = new System.Drawing.Point(212, 29);
            this.labelOperator.Name = "labelOperator";
            this.labelOperator.Size = new System.Drawing.Size(63, 29);
            this.labelOperator.TabIndex = 4;
            this.labelOperator.Text = "=";
            this.labelOperator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbValue1
            // 
            this.tbValue1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tbValue1.Location = new System.Drawing.Point(332, 32);
            this.tbValue1.Name = "tbValue1";
            this.tbValue1.Size = new System.Drawing.Size(240, 20);
            this.tbValue1.TabIndex = 6;
            this.tbValue1.TextChanged += new System.EventHandler(this.tbValue1_TextChanged);
            this.tbValue1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbValue1_KeyPress);
            // 
            // btnAcceptFilter
            // 
            this.btnAcceptFilter.Location = new System.Drawing.Point(630, 32);
            this.btnAcceptFilter.Name = "btnAcceptFilter";
            this.btnAcceptFilter.Size = new System.Drawing.Size(162, 23);
            this.btnAcceptFilter.TabIndex = 7;
            this.btnAcceptFilter.Text = "OK";
            this.btnAcceptFilter.UseVisualStyleBackColor = true;
            this.btnAcceptFilter.Click += new System.EventHandler(this.btnAcceptFilter_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(280, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(345, 29);
            this.label1.TabIndex = 8;
            this.label1.Text = "Filter value:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbMultiOrder
            // 
            this.cbMultiOrder.AutoSize = true;
            this.cbMultiOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbMultiOrder.Location = new System.Drawing.Point(865, 32);
            this.cbMultiOrder.Name = "cbMultiOrder";
            this.cbMultiOrder.Size = new System.Drawing.Size(134, 23);
            this.cbMultiOrder.TabIndex = 10;
            this.cbMultiOrder.Text = "Multi-order shipment";
            this.cbMultiOrder.UseVisualStyleBackColor = true;
            this.cbMultiOrder.CheckedChanged += new System.EventHandler(this.cbMultiOrder_CheckedChanged);
            // 
            // dtpShipDate
            // 
            this.dtpShipDate.BackDisabledColor = System.Drawing.SystemColors.Control;
            this.dtpShipDate.Image = null;
            this.dtpShipDate.Location = new System.Drawing.Point(865, 3);
            this.dtpShipDate.Name = "dtpShipDate";
            this.dtpShipDate.Size = new System.Drawing.Size(134, 20);
            this.dtpShipDate.TabIndex = 11;
            this.dtpShipDate.TextColor = System.Drawing.Color.Black;
            this.dtpShipDate.ValueChanged += new System.EventHandler(this.dtpShipDate_ValueChanged);
            // 
            // OrdersVSplitBottom
            // 
            this.OrdersVSplitBottom.BackColor = System.Drawing.Color.Transparent;
            this.OrdersVSplitBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrdersVSplitBottom.Location = new System.Drawing.Point(0, 0);
            this.OrdersVSplitBottom.Name = "OrdersVSplitBottom";
            // 
            // OrdersVSplitBottom.Panel1
            // 
            this.OrdersVSplitBottom.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.OrdersVSplitBottom.Panel1.Controls.Add(this.OrderPackagesPanel);
            this.OrdersVSplitBottom.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // OrdersVSplitBottom.Panel2
            // 
            this.OrdersVSplitBottom.Panel2.Controls.Add(this.ProductsPanel);
            this.OrdersVSplitBottom.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OrdersVSplitBottom.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OrdersVSplitBottom.Size = new System.Drawing.Size(1008, 239);
            this.OrdersVSplitBottom.SplitterDistance = 656;
            this.OrdersVSplitBottom.SplitterWidth = 5;
            this.OrdersVSplitBottom.TabIndex = 0;
            // 
            // OrderPackagesPanel
            // 
            this.OrderPackagesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.OrderPackagesPanel.BackColor = System.Drawing.Color.Transparent;
            this.OrderPackagesPanel.ColumnCount = 1;
            this.OrderPackagesPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.OrderPackagesPanel.Controls.Add(this.panelShipType, 0, 0);
            this.OrderPackagesPanel.Controls.Add(this.tabPackageType, 0, 2);
            this.OrderPackagesPanel.Controls.Add(this.panelPackageFields, 0, 1);
            this.OrderPackagesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrderPackagesPanel.Location = new System.Drawing.Point(0, 0);
            this.OrderPackagesPanel.MinimumSize = new System.Drawing.Size(184, 0);
            this.OrderPackagesPanel.Name = "OrderPackagesPanel";
            this.OrderPackagesPanel.RowCount = 3;
            this.OrderPackagesPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.OrderPackagesPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.OrderPackagesPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.OrderPackagesPanel.Size = new System.Drawing.Size(656, 239);
            this.OrderPackagesPanel.TabIndex = 1;
            // 
            // panelShipType
            // 
            this.panelShipType.Controls.Add(this.btnAddMultiPacks);
            this.panelShipType.Controls.Add(this.cmbPackageDims);
            this.panelShipType.Controls.Add(this.label2);
            this.panelShipType.Controls.Add(this.btnGetWeight);
            this.panelShipType.Controls.Add(this.lblTotalWeight);
            this.panelShipType.Controls.Add(this.buttonRemovePackage);
            this.panelShipType.Controls.Add(this.button_PackagesAdd);
            this.panelShipType.Controls.Add(this.rbShipmentLevel);
            this.panelShipType.Controls.Add(this.rbPackageLevel);
            this.panelShipType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelShipType.Location = new System.Drawing.Point(3, 3);
            this.panelShipType.Name = "panelShipType";
            this.panelShipType.Size = new System.Drawing.Size(650, 24);
            this.panelShipType.TabIndex = 3;
            // 
            // btnAddMultiPacks
            // 
            this.btnAddMultiPacks.Location = new System.Drawing.Point(79, 1);
            this.btnAddMultiPacks.Name = "btnAddMultiPacks";
            this.btnAddMultiPacks.Size = new System.Drawing.Size(75, 23);
            this.btnAddMultiPacks.TabIndex = 8;
            this.btnAddMultiPacks.Text = "Add multiple";
            this.btnAddMultiPacks.UseVisualStyleBackColor = true;
            this.btnAddMultiPacks.Click += new System.EventHandler(this.btnAddMultiPacks_Click);
            // 
            // cmbPackageDims
            // 
            this.cmbPackageDims.DataSource = this.packageDimsBindingSource;
            this.cmbPackageDims.DisplayMember = "PackageName";
            this.cmbPackageDims.FormattingEnabled = true;
            this.cmbPackageDims.Location = new System.Drawing.Point(412, 4);
            this.cmbPackageDims.Name = "cmbPackageDims";
            this.cmbPackageDims.Size = new System.Drawing.Size(171, 21);
            this.cmbPackageDims.TabIndex = 7;
            this.cmbPackageDims.ValueMember = "PackageDimCode";
            this.cmbPackageDims.SelectedIndexChanged += new System.EventHandler(this.cmbPackageDims_SelectedIndexChanged);
            // 
            // packageDimsBindingSource
            // 
            this.packageDimsBindingSource.DataMember = "PackageDimensions";
            this.packageDimsBindingSource.DataSource = this.data;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(301, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Package Dims Code";
            // 
            // btnGetWeight
            // 
            this.btnGetWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetWeight.Location = new System.Drawing.Point(554, 0);
            this.btnGetWeight.Name = "btnGetWeight";
            this.btnGetWeight.Size = new System.Drawing.Size(93, 22);
            this.btnGetWeight.TabIndex = 5;
            this.btnGetWeight.Text = "Get Weight";
            this.btnGetWeight.UseVisualStyleBackColor = true;
            this.btnGetWeight.Click += new System.EventHandler(this.btnGetWeight_Click);
            // 
            // lblTotalWeight
            // 
            this.lblTotalWeight.AutoSize = true;
            this.lblTotalWeight.Location = new System.Drawing.Point(200, 5);
            this.lblTotalWeight.Name = "lblTotalWeight";
            this.lblTotalWeight.Size = new System.Drawing.Size(68, 13);
            this.lblTotalWeight.TabIndex = 4;
            this.lblTotalWeight.Text = "Total Weight";
            // 
            // buttonRemovePackage
            // 
            this.buttonRemovePackage.Location = new System.Drawing.Point(41, 1);
            this.buttonRemovePackage.Name = "buttonRemovePackage";
            this.buttonRemovePackage.Size = new System.Drawing.Size(32, 23);
            this.buttonRemovePackage.TabIndex = 3;
            this.buttonRemovePackage.Text = "-";
            this.ToolTip.SetToolTip(this.buttonRemovePackage, "Remove the selected package, skid or item from the grid");
            this.buttonRemovePackage.UseVisualStyleBackColor = true;
            this.buttonRemovePackage.Click += new System.EventHandler(this.buttonRemovePackage_Click);
            // 
            // button_PackagesAdd
            // 
            this.button_PackagesAdd.Location = new System.Drawing.Point(3, 1);
            this.button_PackagesAdd.Name = "button_PackagesAdd";
            this.button_PackagesAdd.Size = new System.Drawing.Size(32, 23);
            this.button_PackagesAdd.TabIndex = 2;
            this.button_PackagesAdd.Text = "+";
            this.ToolTip.SetToolTip(this.button_PackagesAdd, "Add a new package, skid or item to the grid");
            this.button_PackagesAdd.UseVisualStyleBackColor = true;
            this.button_PackagesAdd.Click += new System.EventHandler(this.button_PackagesAdd_Click);
            // 
            // rbShipmentLevel
            // 
            this.rbShipmentLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbShipmentLevel.AutoSize = true;
            this.rbShipmentLevel.Location = new System.Drawing.Point(552, 3);
            this.rbShipmentLevel.Name = "rbShipmentLevel";
            this.rbShipmentLevel.Size = new System.Drawing.Size(98, 17);
            this.rbShipmentLevel.TabIndex = 1;
            this.rbShipmentLevel.Text = "Shipment Level";
            this.rbShipmentLevel.UseVisualStyleBackColor = true;
            this.rbShipmentLevel.Visible = false;
            // 
            // rbPackageLevel
            // 
            this.rbPackageLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbPackageLevel.AutoSize = true;
            this.rbPackageLevel.Checked = true;
            this.rbPackageLevel.Location = new System.Drawing.Point(504, 3);
            this.rbPackageLevel.Name = "rbPackageLevel";
            this.rbPackageLevel.Size = new System.Drawing.Size(97, 17);
            this.rbPackageLevel.TabIndex = 0;
            this.rbPackageLevel.TabStop = true;
            this.rbPackageLevel.Text = "Package Level";
            this.rbPackageLevel.UseVisualStyleBackColor = true;
            this.rbPackageLevel.Visible = false;
            this.rbPackageLevel.CheckedChanged += new System.EventHandler(this.rbPackageLevel_CheckedChanged);
            // 
            // tabPackageType
            // 
            this.tabPackageType.Controls.Add(this.tabPagePackages);
            this.tabPackageType.Controls.Add(this.tabPageSkids);
            this.tabPackageType.Controls.Add(this.tabPageItems);
            this.tabPackageType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPackageType.Location = new System.Drawing.Point(3, 63);
            this.tabPackageType.Name = "tabPackageType";
            this.tabPackageType.SelectedIndex = 0;
            this.tabPackageType.Size = new System.Drawing.Size(650, 199);
            this.tabPackageType.TabIndex = 2;
            this.tabPackageType.SelectedIndexChanged += new System.EventHandler(this.tabPackageType_SelectedIndexChanged);
            // 
            // tabPagePackages
            // 
            this.tabPagePackages.Controls.Add(this.OrderPackagesGrid);
            this.tabPagePackages.Location = new System.Drawing.Point(4, 22);
            this.tabPagePackages.Name = "tabPagePackages";
            this.tabPagePackages.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePackages.Size = new System.Drawing.Size(642, 173);
            this.tabPagePackages.TabIndex = 0;
            this.tabPagePackages.Text = "Small Packages";
            this.tabPagePackages.UseVisualStyleBackColor = true;
            // 
            // OrderPackagesGrid
            // 
            this.OrderPackagesGrid.AllowUserToAddRows = false;
            this.OrderPackagesGrid.AllowUserToDeleteRows = false;
            this.OrderPackagesGrid.AllowUserToOrderColumns = true;
            this.OrderPackagesGrid.AllowUserToResizeRows = false;
            this.OrderPackagesGrid.AutoGenerateColumns = false;
            this.OrderPackagesGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrderPackagesGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.OrderPackagesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderPackagesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtPackageOrderNumber,
            this.txtPackageNumber,
            this.txtPackageLength,
            this.txtPackageWidth,
            this.txtPackageHeight,
            this.txtPackageWeightType,
            this.txtPackageDimensionsType,
            this.txtPackageInsuranceAmount,
            this.txtPackageInsuranceCurrency,
            this.txtPackageReference});
            this.OrderPackagesGrid.DataSource = this.fKOrdersTableOrderPackagesTableBindingSource;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.OrderPackagesGrid.DefaultCellStyle = dataGridViewCellStyle8;
            this.OrderPackagesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrderPackagesGrid.Location = new System.Drawing.Point(3, 3);
            this.OrderPackagesGrid.MultiSelect = false;
            this.OrderPackagesGrid.Name = "OrderPackagesGrid";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrderPackagesGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.OrderPackagesGrid.RowHeadersVisible = false;
            this.OrderPackagesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.OrderPackagesGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.OrderPackagesGrid.Size = new System.Drawing.Size(636, 167);
            this.OrderPackagesGrid.TabIndex = 1;
            this.OrderPackagesGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.OrderPackagesGrid_CellEndEdit);
            this.OrderPackagesGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.OrderPackagesGrid_DataError);
            this.OrderPackagesGrid.SelectionChanged += new System.EventHandler(this.OrderPackagesGrid_SelectionChanged);
            // 
            // txtPackageOrderNumber
            // 
            this.txtPackageOrderNumber.DataPropertyName = "Order #";
            this.txtPackageOrderNumber.Frozen = true;
            this.txtPackageOrderNumber.HeaderText = "Order #";
            this.txtPackageOrderNumber.Name = "txtPackageOrderNumber";
            this.txtPackageOrderNumber.ReadOnly = true;
            this.txtPackageOrderNumber.Visible = false;
            // 
            // txtPackageNumber
            // 
            this.txtPackageNumber.DataPropertyName = "Package #";
            this.txtPackageNumber.Frozen = true;
            this.txtPackageNumber.HeaderText = "Package #";
            this.txtPackageNumber.Name = "txtPackageNumber";
            this.txtPackageNumber.ReadOnly = true;
            // 
            // txtPackageLength
            // 
            this.txtPackageLength.DataPropertyName = "Package Length";
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.txtPackageLength.DefaultCellStyle = dataGridViewCellStyle5;
            this.txtPackageLength.HeaderText = "Length";
            this.txtPackageLength.Name = "txtPackageLength";
            // 
            // txtPackageWidth
            // 
            this.txtPackageWidth.DataPropertyName = "Package Width";
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.txtPackageWidth.DefaultCellStyle = dataGridViewCellStyle6;
            this.txtPackageWidth.HeaderText = "Width";
            this.txtPackageWidth.Name = "txtPackageWidth";
            // 
            // txtPackageHeight
            // 
            this.txtPackageHeight.DataPropertyName = "Package Height";
            dataGridViewCellStyle7.Format = "N0";
            this.txtPackageHeight.DefaultCellStyle = dataGridViewCellStyle7;
            this.txtPackageHeight.HeaderText = "Height";
            this.txtPackageHeight.Name = "txtPackageHeight";
            // 
            // txtPackageWeightType
            // 
            this.txtPackageWeightType.DataPropertyName = "Package Weight Type";
            this.txtPackageWeightType.HeaderText = "Weight UM";
            this.txtPackageWeightType.Name = "txtPackageWeightType";
            // 
            // txtPackageDimensionsType
            // 
            this.txtPackageDimensionsType.DataPropertyName = "Package Dimensions Type";
            this.txtPackageDimensionsType.HeaderText = "Dim UM";
            this.txtPackageDimensionsType.Name = "txtPackageDimensionsType";
            // 
            // txtPackageInsuranceAmount
            // 
            this.txtPackageInsuranceAmount.DataPropertyName = "Package Insurance Amount";
            this.txtPackageInsuranceAmount.HeaderText = "Insurance $";
            this.txtPackageInsuranceAmount.Name = "txtPackageInsuranceAmount";
            // 
            // txtPackageInsuranceCurrency
            // 
            this.txtPackageInsuranceCurrency.DataPropertyName = "Package Insurance Currency";
            this.txtPackageInsuranceCurrency.HeaderText = "Currency";
            this.txtPackageInsuranceCurrency.Name = "txtPackageInsuranceCurrency";
            // 
            // txtPackageReference
            // 
            this.txtPackageReference.DataPropertyName = "Package Reference";
            this.txtPackageReference.HeaderText = "Reference";
            this.txtPackageReference.Name = "txtPackageReference";
            // 
            // fKOrdersTableOrderPackagesTableBindingSource
            // 
            this.fKOrdersTableOrderPackagesTableBindingSource.DataMember = "FK_OrdersTable_OrderPackagesTable";
            this.fKOrdersTableOrderPackagesTableBindingSource.DataSource = this.OrdersTableBindingSource;
            // 
            // tabPageSkids
            // 
            this.tabPageSkids.Controls.Add(this.OrderSkidsGrid);
            this.tabPageSkids.Location = new System.Drawing.Point(4, 22);
            this.tabPageSkids.Name = "tabPageSkids";
            this.tabPageSkids.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSkids.Size = new System.Drawing.Size(642, 173);
            this.tabPageSkids.TabIndex = 1;
            this.tabPageSkids.Text = "Skids";
            this.tabPageSkids.UseVisualStyleBackColor = true;
            // 
            // OrderSkidsGrid
            // 
            this.OrderSkidsGrid.AllowUserToAddRows = false;
            this.OrderSkidsGrid.AllowUserToDeleteRows = false;
            this.OrderSkidsGrid.AllowUserToOrderColumns = true;
            this.OrderSkidsGrid.AllowUserToResizeRows = false;
            this.OrderSkidsGrid.AutoGenerateColumns = false;
            this.OrderSkidsGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrderSkidsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.OrderSkidsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderSkidsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.orderDataGridViewTextBoxColumn1,
            this.skidDataGridViewTextBoxColumn,
            this.useSkidsDataGridViewTextBoxColumn,
            this.skidCountDataGridViewTextBoxColumn,
            this.skidShipmentLevelDataGridViewTextBoxColumn,
            this.skidSkidLevelDataGridViewTextBoxColumn,
            this.skidsCountDataGridViewTextBoxColumn,
            this.skidWeightDataGridViewTextBoxColumn,
            this.skidWeightTypeDataGridViewTextBoxColumn,
            this.skidInsuranceDataGridViewTextBoxColumn,
            this.skidInsuranceCurrencyDataGridViewTextBoxColumn,
            this.skidLengthDataGridViewTextBoxColumn,
            this.skidWidthDataGridViewTextBoxColumn,
            this.skidHeightDataGridViewTextBoxColumn,
            this.skidDimensionsTypeDataGridViewTextBoxColumn,
            this.skidReference1DataGridViewTextBoxColumn,
            this.skidReference2DataGridViewTextBoxColumn,
            this.skidPONumberDataGridViewTextBoxColumn,
            this.skidShipmentIDDataGridViewTextBoxColumn,
            this.skidInvoiceNumberDataGridViewTextBoxColumn,
            this.shipmentPONumberDataGridViewTextBoxColumn,
            this.shipmentReferenceDataGridViewTextBoxColumn,
            this.skidFreightClassDataGridViewTextBoxColumn,
            this.skidNumberOfPackagesInSkidDataGridViewTextBoxColumn,
            this.skidDescriptionDataGridViewTextBoxColumn,
            this.bOLCommentDataGridViewTextBoxColumn});
            this.OrderSkidsGrid.DataSource = this.fkOrdersTableOrderSkidsTableBindingSource;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.OrderSkidsGrid.DefaultCellStyle = dataGridViewCellStyle11;
            this.OrderSkidsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrderSkidsGrid.Location = new System.Drawing.Point(3, 3);
            this.OrderSkidsGrid.Name = "OrderSkidsGrid";
            this.OrderSkidsGrid.Size = new System.Drawing.Size(636, 167);
            this.OrderSkidsGrid.TabIndex = 0;
            this.OrderSkidsGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.OrderSkidsGrid_CellEndEdit);
            this.OrderSkidsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.OrderSkidsGrid_DataError);
            // 
            // orderDataGridViewTextBoxColumn1
            // 
            this.orderDataGridViewTextBoxColumn1.DataPropertyName = "Order #";
            this.orderDataGridViewTextBoxColumn1.HeaderText = "Order #";
            this.orderDataGridViewTextBoxColumn1.Name = "orderDataGridViewTextBoxColumn1";
            // 
            // skidDataGridViewTextBoxColumn
            // 
            this.skidDataGridViewTextBoxColumn.DataPropertyName = "Skid #";
            this.skidDataGridViewTextBoxColumn.HeaderText = "Skid #";
            this.skidDataGridViewTextBoxColumn.Name = "skidDataGridViewTextBoxColumn";
            // 
            // useSkidsDataGridViewTextBoxColumn
            // 
            this.useSkidsDataGridViewTextBoxColumn.DataPropertyName = "Use Skids";
            this.useSkidsDataGridViewTextBoxColumn.HeaderText = "Use Skids";
            this.useSkidsDataGridViewTextBoxColumn.Name = "useSkidsDataGridViewTextBoxColumn";
            // 
            // skidCountDataGridViewTextBoxColumn
            // 
            this.skidCountDataGridViewTextBoxColumn.DataPropertyName = "Skid Count";
            this.skidCountDataGridViewTextBoxColumn.HeaderText = "Skid Count";
            this.skidCountDataGridViewTextBoxColumn.Name = "skidCountDataGridViewTextBoxColumn";
            // 
            // skidShipmentLevelDataGridViewTextBoxColumn
            // 
            this.skidShipmentLevelDataGridViewTextBoxColumn.DataPropertyName = "Skid Shipment Level";
            this.skidShipmentLevelDataGridViewTextBoxColumn.HeaderText = "Skid Shipment Level";
            this.skidShipmentLevelDataGridViewTextBoxColumn.Name = "skidShipmentLevelDataGridViewTextBoxColumn";
            // 
            // skidSkidLevelDataGridViewTextBoxColumn
            // 
            this.skidSkidLevelDataGridViewTextBoxColumn.DataPropertyName = "Skid Skid Level";
            this.skidSkidLevelDataGridViewTextBoxColumn.HeaderText = "Skid Skid Level";
            this.skidSkidLevelDataGridViewTextBoxColumn.Name = "skidSkidLevelDataGridViewTextBoxColumn";
            // 
            // skidsCountDataGridViewTextBoxColumn
            // 
            this.skidsCountDataGridViewTextBoxColumn.DataPropertyName = "Skids Count";
            this.skidsCountDataGridViewTextBoxColumn.HeaderText = "Skids Count";
            this.skidsCountDataGridViewTextBoxColumn.Name = "skidsCountDataGridViewTextBoxColumn";
            this.skidsCountDataGridViewTextBoxColumn.Visible = false;
            // 
            // skidWeightDataGridViewTextBoxColumn
            // 
            this.skidWeightDataGridViewTextBoxColumn.DataPropertyName = "Skid Weight";
            this.skidWeightDataGridViewTextBoxColumn.HeaderText = "Skid Weight";
            this.skidWeightDataGridViewTextBoxColumn.Name = "skidWeightDataGridViewTextBoxColumn";
            // 
            // skidWeightTypeDataGridViewTextBoxColumn
            // 
            this.skidWeightTypeDataGridViewTextBoxColumn.DataPropertyName = "Skid Weight Type";
            this.skidWeightTypeDataGridViewTextBoxColumn.HeaderText = "Skid Weight Type";
            this.skidWeightTypeDataGridViewTextBoxColumn.Name = "skidWeightTypeDataGridViewTextBoxColumn";
            // 
            // skidInsuranceDataGridViewTextBoxColumn
            // 
            this.skidInsuranceDataGridViewTextBoxColumn.DataPropertyName = "Skid Insurance";
            this.skidInsuranceDataGridViewTextBoxColumn.HeaderText = "Skid Insurance";
            this.skidInsuranceDataGridViewTextBoxColumn.Name = "skidInsuranceDataGridViewTextBoxColumn";
            // 
            // skidInsuranceCurrencyDataGridViewTextBoxColumn
            // 
            this.skidInsuranceCurrencyDataGridViewTextBoxColumn.DataPropertyName = "Skid Insurance Currency";
            this.skidInsuranceCurrencyDataGridViewTextBoxColumn.HeaderText = "Skid Insurance Currency";
            this.skidInsuranceCurrencyDataGridViewTextBoxColumn.Name = "skidInsuranceCurrencyDataGridViewTextBoxColumn";
            // 
            // skidLengthDataGridViewTextBoxColumn
            // 
            this.skidLengthDataGridViewTextBoxColumn.DataPropertyName = "Skid Length";
            this.skidLengthDataGridViewTextBoxColumn.HeaderText = "Skid Length";
            this.skidLengthDataGridViewTextBoxColumn.Name = "skidLengthDataGridViewTextBoxColumn";
            // 
            // skidWidthDataGridViewTextBoxColumn
            // 
            this.skidWidthDataGridViewTextBoxColumn.DataPropertyName = "Skid Width";
            this.skidWidthDataGridViewTextBoxColumn.HeaderText = "Skid Width";
            this.skidWidthDataGridViewTextBoxColumn.Name = "skidWidthDataGridViewTextBoxColumn";
            // 
            // skidHeightDataGridViewTextBoxColumn
            // 
            this.skidHeightDataGridViewTextBoxColumn.DataPropertyName = "Skid Height";
            this.skidHeightDataGridViewTextBoxColumn.HeaderText = "Skid Height";
            this.skidHeightDataGridViewTextBoxColumn.Name = "skidHeightDataGridViewTextBoxColumn";
            // 
            // skidDimensionsTypeDataGridViewTextBoxColumn
            // 
            this.skidDimensionsTypeDataGridViewTextBoxColumn.DataPropertyName = "Skid Dimensions Type";
            this.skidDimensionsTypeDataGridViewTextBoxColumn.HeaderText = "Skid Dimensions Type";
            this.skidDimensionsTypeDataGridViewTextBoxColumn.Name = "skidDimensionsTypeDataGridViewTextBoxColumn";
            // 
            // skidReference1DataGridViewTextBoxColumn
            // 
            this.skidReference1DataGridViewTextBoxColumn.DataPropertyName = "Skid Reference1";
            this.skidReference1DataGridViewTextBoxColumn.HeaderText = "Skid Reference1";
            this.skidReference1DataGridViewTextBoxColumn.Name = "skidReference1DataGridViewTextBoxColumn";
            // 
            // skidReference2DataGridViewTextBoxColumn
            // 
            this.skidReference2DataGridViewTextBoxColumn.DataPropertyName = "Skid Reference2";
            this.skidReference2DataGridViewTextBoxColumn.HeaderText = "Skid Reference2";
            this.skidReference2DataGridViewTextBoxColumn.Name = "skidReference2DataGridViewTextBoxColumn";
            // 
            // skidPONumberDataGridViewTextBoxColumn
            // 
            this.skidPONumberDataGridViewTextBoxColumn.DataPropertyName = "Skid PO Number";
            this.skidPONumberDataGridViewTextBoxColumn.HeaderText = "Skid PO Number";
            this.skidPONumberDataGridViewTextBoxColumn.Name = "skidPONumberDataGridViewTextBoxColumn";
            // 
            // skidShipmentIDDataGridViewTextBoxColumn
            // 
            this.skidShipmentIDDataGridViewTextBoxColumn.DataPropertyName = "Skid Shipment ID";
            this.skidShipmentIDDataGridViewTextBoxColumn.HeaderText = "Skid Shipment ID";
            this.skidShipmentIDDataGridViewTextBoxColumn.Name = "skidShipmentIDDataGridViewTextBoxColumn";
            // 
            // skidInvoiceNumberDataGridViewTextBoxColumn
            // 
            this.skidInvoiceNumberDataGridViewTextBoxColumn.DataPropertyName = "Skid Invoice Number";
            this.skidInvoiceNumberDataGridViewTextBoxColumn.HeaderText = "Skid Invoice Number";
            this.skidInvoiceNumberDataGridViewTextBoxColumn.Name = "skidInvoiceNumberDataGridViewTextBoxColumn";
            // 
            // shipmentPONumberDataGridViewTextBoxColumn
            // 
            this.shipmentPONumberDataGridViewTextBoxColumn.DataPropertyName = "ShipmentPONumber";
            this.shipmentPONumberDataGridViewTextBoxColumn.HeaderText = "ShipmentPONumber";
            this.shipmentPONumberDataGridViewTextBoxColumn.Name = "shipmentPONumberDataGridViewTextBoxColumn";
            // 
            // shipmentReferenceDataGridViewTextBoxColumn
            // 
            this.shipmentReferenceDataGridViewTextBoxColumn.DataPropertyName = "Shipment Reference";
            this.shipmentReferenceDataGridViewTextBoxColumn.HeaderText = "Shipment Reference";
            this.shipmentReferenceDataGridViewTextBoxColumn.Name = "shipmentReferenceDataGridViewTextBoxColumn";
            // 
            // skidFreightClassDataGridViewTextBoxColumn
            // 
            this.skidFreightClassDataGridViewTextBoxColumn.DataPropertyName = "Skid Freight Class";
            this.skidFreightClassDataGridViewTextBoxColumn.HeaderText = "Skid Freight Class";
            this.skidFreightClassDataGridViewTextBoxColumn.Name = "skidFreightClassDataGridViewTextBoxColumn";
            // 
            // skidNumberOfPackagesInSkidDataGridViewTextBoxColumn
            // 
            this.skidNumberOfPackagesInSkidDataGridViewTextBoxColumn.DataPropertyName = "Skid Number of Packages in Skid";
            this.skidNumberOfPackagesInSkidDataGridViewTextBoxColumn.HeaderText = "Skid Number of Packages in Skid";
            this.skidNumberOfPackagesInSkidDataGridViewTextBoxColumn.Name = "skidNumberOfPackagesInSkidDataGridViewTextBoxColumn";
            // 
            // skidDescriptionDataGridViewTextBoxColumn
            // 
            this.skidDescriptionDataGridViewTextBoxColumn.DataPropertyName = "Skid Description";
            this.skidDescriptionDataGridViewTextBoxColumn.HeaderText = "Skid Description";
            this.skidDescriptionDataGridViewTextBoxColumn.Name = "skidDescriptionDataGridViewTextBoxColumn";
            // 
            // bOLCommentDataGridViewTextBoxColumn
            // 
            this.bOLCommentDataGridViewTextBoxColumn.DataPropertyName = "BOL Comment";
            this.bOLCommentDataGridViewTextBoxColumn.HeaderText = "BOL Comment";
            this.bOLCommentDataGridViewTextBoxColumn.Name = "bOLCommentDataGridViewTextBoxColumn";
            // 
            // fkOrdersTableOrderSkidsTableBindingSource
            // 
            this.fkOrdersTableOrderSkidsTableBindingSource.DataMember = "FK_OrdersTable_OrdersSkidsTable";
            this.fkOrdersTableOrderSkidsTableBindingSource.DataSource = this.OrdersTableBindingSource;
            // 
            // tabPageItems
            // 
            this.tabPageItems.Controls.Add(this.LTLItemsGrid);
            this.tabPageItems.Location = new System.Drawing.Point(4, 22);
            this.tabPageItems.Name = "tabPageItems";
            this.tabPageItems.Size = new System.Drawing.Size(642, 173);
            this.tabPageItems.TabIndex = 2;
            this.tabPageItems.Text = "Items";
            this.tabPageItems.UseVisualStyleBackColor = true;
            // 
            // LTLItemsGrid
            // 
            this.LTLItemsGrid.AllowUserToAddRows = false;
            this.LTLItemsGrid.AllowUserToDeleteRows = false;
            this.LTLItemsGrid.AllowUserToOrderColumns = true;
            this.LTLItemsGrid.AllowUserToResizeRows = false;
            this.LTLItemsGrid.AutoGenerateColumns = false;
            this.LTLItemsGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.LTLItemsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.LTLItemsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LTLItemsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.orderDataGridViewTextBoxColumn3,
            this.lTLItemDataGridViewTextBoxColumn,
            this.lTLItemsCountDataGridViewTextBoxColumn,
            this.lTLItemDescriptionDataGridViewTextBoxColumn,
            this.lTLItemTotalWeightDataGridViewTextBoxColumn,
            this.lTLItemTotalWeightTypeDataGridViewTextBoxColumn,
            this.lTLItemTotalLengthDataGridViewTextBoxColumn,
            this.lTLItemTotalWidthDataGridViewTextBoxColumn,
            this.lTLItemTotalHeightDataGridViewTextBoxColumn,
            this.lTLItemTotalDimsTypeDataGridViewTextBoxColumn,
            this.lTLItemFreightClassIDDataGridViewComboBoxColumn,
            this.lTLItemQuantityDataGridViewTextBoxColumn,
            this.lTLItemUMDataGridViewTextBoxColumn});
            this.LTLItemsGrid.DataSource = this.fkOrdersTableLTLItemsBindingSource;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.LTLItemsGrid.DefaultCellStyle = dataGridViewCellStyle13;
            this.LTLItemsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LTLItemsGrid.Location = new System.Drawing.Point(0, 0);
            this.LTLItemsGrid.Name = "LTLItemsGrid";
            this.LTLItemsGrid.Size = new System.Drawing.Size(642, 173);
            this.LTLItemsGrid.TabIndex = 0;
            this.LTLItemsGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.LTLItemsGrid_CellEndEdit);
            this.LTLItemsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.LTLItemsGrid_DataError);
            // 
            // orderDataGridViewTextBoxColumn3
            // 
            this.orderDataGridViewTextBoxColumn3.DataPropertyName = "Order #";
            this.orderDataGridViewTextBoxColumn3.Frozen = true;
            this.orderDataGridViewTextBoxColumn3.HeaderText = "Order #";
            this.orderDataGridViewTextBoxColumn3.Name = "orderDataGridViewTextBoxColumn3";
            this.orderDataGridViewTextBoxColumn3.Visible = false;
            // 
            // lTLItemDataGridViewTextBoxColumn
            // 
            this.lTLItemDataGridViewTextBoxColumn.DataPropertyName = "LTL Item #";
            this.lTLItemDataGridViewTextBoxColumn.Frozen = true;
            this.lTLItemDataGridViewTextBoxColumn.HeaderText = "LTL Item #";
            this.lTLItemDataGridViewTextBoxColumn.Name = "lTLItemDataGridViewTextBoxColumn";
            // 
            // lTLItemsCountDataGridViewTextBoxColumn
            // 
            this.lTLItemsCountDataGridViewTextBoxColumn.DataPropertyName = "LTL Items Count";
            this.lTLItemsCountDataGridViewTextBoxColumn.HeaderText = "LTL Items Count";
            this.lTLItemsCountDataGridViewTextBoxColumn.Name = "lTLItemsCountDataGridViewTextBoxColumn";
            // 
            // lTLItemDescriptionDataGridViewTextBoxColumn
            // 
            this.lTLItemDescriptionDataGridViewTextBoxColumn.DataPropertyName = "LTL Item Description";
            this.lTLItemDescriptionDataGridViewTextBoxColumn.HeaderText = "LTL Item Description";
            this.lTLItemDescriptionDataGridViewTextBoxColumn.Name = "lTLItemDescriptionDataGridViewTextBoxColumn";
            // 
            // lTLItemTotalWeightDataGridViewTextBoxColumn
            // 
            this.lTLItemTotalWeightDataGridViewTextBoxColumn.DataPropertyName = "LTL Item Total Weight";
            this.lTLItemTotalWeightDataGridViewTextBoxColumn.HeaderText = "LTL Item Total Weight";
            this.lTLItemTotalWeightDataGridViewTextBoxColumn.Name = "lTLItemTotalWeightDataGridViewTextBoxColumn";
            // 
            // lTLItemTotalWeightTypeDataGridViewTextBoxColumn
            // 
            this.lTLItemTotalWeightTypeDataGridViewTextBoxColumn.DataPropertyName = "LTL Item Total Weight Type";
            this.lTLItemTotalWeightTypeDataGridViewTextBoxColumn.HeaderText = "LTL Item Total Weight Type";
            this.lTLItemTotalWeightTypeDataGridViewTextBoxColumn.Name = "lTLItemTotalWeightTypeDataGridViewTextBoxColumn";
            // 
            // lTLItemTotalLengthDataGridViewTextBoxColumn
            // 
            this.lTLItemTotalLengthDataGridViewTextBoxColumn.DataPropertyName = "LTL Item Total Length";
            this.lTLItemTotalLengthDataGridViewTextBoxColumn.HeaderText = "LTL Item Total Length";
            this.lTLItemTotalLengthDataGridViewTextBoxColumn.Name = "lTLItemTotalLengthDataGridViewTextBoxColumn";
            // 
            // lTLItemTotalWidthDataGridViewTextBoxColumn
            // 
            this.lTLItemTotalWidthDataGridViewTextBoxColumn.DataPropertyName = "LTL Item Total Width";
            this.lTLItemTotalWidthDataGridViewTextBoxColumn.HeaderText = "LTL Item Total Width";
            this.lTLItemTotalWidthDataGridViewTextBoxColumn.Name = "lTLItemTotalWidthDataGridViewTextBoxColumn";
            // 
            // lTLItemTotalHeightDataGridViewTextBoxColumn
            // 
            this.lTLItemTotalHeightDataGridViewTextBoxColumn.DataPropertyName = "LTL Item Total Height";
            this.lTLItemTotalHeightDataGridViewTextBoxColumn.HeaderText = "LTL Item Total Height";
            this.lTLItemTotalHeightDataGridViewTextBoxColumn.Name = "lTLItemTotalHeightDataGridViewTextBoxColumn";
            // 
            // lTLItemTotalDimsTypeDataGridViewTextBoxColumn
            // 
            this.lTLItemTotalDimsTypeDataGridViewTextBoxColumn.DataPropertyName = "LTL Item Total Dims Type";
            this.lTLItemTotalDimsTypeDataGridViewTextBoxColumn.HeaderText = "LTL Item Total Dims Type";
            this.lTLItemTotalDimsTypeDataGridViewTextBoxColumn.Name = "lTLItemTotalDimsTypeDataGridViewTextBoxColumn";
            // 
            // lTLItemFreightClassIDDataGridViewComboBoxColumn
            // 
            this.lTLItemFreightClassIDDataGridViewComboBoxColumn.DataPropertyName = "LTL Item Freight Class ID";
            this.lTLItemFreightClassIDDataGridViewComboBoxColumn.HeaderText = "LTL Item Freight Class ID";
            this.lTLItemFreightClassIDDataGridViewComboBoxColumn.Items.AddRange(new object[] {
            "50.0",
            "55.0",
            "60.0",
            "65.0",
            "70.0",
            "77.5",
            "85.0",
            "92.0",
            "100.0",
            "110.0",
            "125.0",
            "150.0",
            "175.0",
            "200.0",
            "250.0",
            "300.0"});
            this.lTLItemFreightClassIDDataGridViewComboBoxColumn.Name = "lTLItemFreightClassIDDataGridViewComboBoxColumn";
            this.lTLItemFreightClassIDDataGridViewComboBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.lTLItemFreightClassIDDataGridViewComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // lTLItemQuantityDataGridViewTextBoxColumn
            // 
            this.lTLItemQuantityDataGridViewTextBoxColumn.DataPropertyName = "LTL Item Quantity";
            this.lTLItemQuantityDataGridViewTextBoxColumn.HeaderText = "LTL Item Quantity";
            this.lTLItemQuantityDataGridViewTextBoxColumn.Name = "lTLItemQuantityDataGridViewTextBoxColumn";
            // 
            // lTLItemUMDataGridViewTextBoxColumn
            // 
            this.lTLItemUMDataGridViewTextBoxColumn.DataPropertyName = "LTL Item UM";
            this.lTLItemUMDataGridViewTextBoxColumn.HeaderText = "LTL Item UM";
            this.lTLItemUMDataGridViewTextBoxColumn.Name = "lTLItemUMDataGridViewTextBoxColumn";
            // 
            // fkOrdersTableLTLItemsBindingSource
            // 
            this.fkOrdersTableLTLItemsBindingSource.DataMember = "FK_OrdersTable_OrderLTLItems";
            this.fkOrdersTableLTLItemsBindingSource.DataSource = this.OrdersTableBindingSource;
            // 
            // panelPackageFields
            // 
            this.panelPackageFields.Controls.Add(this.btnSetFieldValue);
            this.panelPackageFields.Controls.Add(this.rbSetFieldAll);
            this.panelPackageFields.Controls.Add(this.rbSetForMissing);
            this.panelPackageFields.Controls.Add(this.rbFieldCurrent);
            this.panelPackageFields.Controls.Add(this.txtPackageFieldValue);
            this.panelPackageFields.Controls.Add(this.lblPackageFieldValue);
            this.panelPackageFields.Controls.Add(this.cbPackageField);
            this.panelPackageFields.Controls.Add(this.lblPackageField);
            this.panelPackageFields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPackageFields.Location = new System.Drawing.Point(3, 33);
            this.panelPackageFields.Name = "panelPackageFields";
            this.panelPackageFields.Size = new System.Drawing.Size(650, 24);
            this.panelPackageFields.TabIndex = 4;
            // 
            // btnSetFieldValue
            // 
            this.btnSetFieldValue.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSetFieldValue.Location = new System.Drawing.Point(554, 0);
            this.btnSetFieldValue.Name = "btnSetFieldValue";
            this.btnSetFieldValue.Size = new System.Drawing.Size(96, 24);
            this.btnSetFieldValue.TabIndex = 7;
            this.btnSetFieldValue.Text = "Set Value";
            this.btnSetFieldValue.UseVisualStyleBackColor = true;
            this.btnSetFieldValue.Click += new System.EventHandler(this.btnSetFieldValue_Click);
            // 
            // rbSetFieldAll
            // 
            this.rbSetFieldAll.AutoSize = true;
            this.rbSetFieldAll.Location = new System.Drawing.Point(456, 2);
            this.rbSetFieldAll.Name = "rbSetFieldAll";
            this.rbSetFieldAll.Size = new System.Drawing.Size(55, 17);
            this.rbSetFieldAll.TabIndex = 6;
            this.rbSetFieldAll.TabStop = true;
            this.rbSetFieldAll.Text = "Set All";
            this.rbSetFieldAll.UseVisualStyleBackColor = true;
            // 
            // rbSetForMissing
            // 
            this.rbSetForMissing.AutoSize = true;
            this.rbSetForMissing.Location = new System.Drawing.Point(371, 2);
            this.rbSetForMissing.Name = "rbSetForMissing";
            this.rbSetForMissing.Size = new System.Drawing.Size(79, 17);
            this.rbSetForMissing.TabIndex = 5;
            this.rbSetForMissing.TabStop = true;
            this.rbSetForMissing.Text = "Set Missing";
            this.rbSetForMissing.UseVisualStyleBackColor = true;
            // 
            // rbFieldCurrent
            // 
            this.rbFieldCurrent.AutoSize = true;
            this.rbFieldCurrent.Location = new System.Drawing.Point(287, 2);
            this.rbFieldCurrent.Name = "rbFieldCurrent";
            this.rbFieldCurrent.Size = new System.Drawing.Size(78, 17);
            this.rbFieldCurrent.TabIndex = 4;
            this.rbFieldCurrent.TabStop = true;
            this.rbFieldCurrent.Text = "Set Current";
            this.rbFieldCurrent.UseVisualStyleBackColor = true;
            // 
            // txtPackageFieldValue
            // 
            this.txtPackageFieldValue.Location = new System.Drawing.Point(185, 1);
            this.txtPackageFieldValue.Name = "txtPackageFieldValue";
            this.txtPackageFieldValue.Size = new System.Drawing.Size(82, 20);
            this.txtPackageFieldValue.TabIndex = 3;
            // 
            // lblPackageFieldValue
            // 
            this.lblPackageFieldValue.AutoSize = true;
            this.lblPackageFieldValue.Location = new System.Drawing.Point(145, 4);
            this.lblPackageFieldValue.Name = "lblPackageFieldValue";
            this.lblPackageFieldValue.Size = new System.Drawing.Size(34, 13);
            this.lblPackageFieldValue.TabIndex = 2;
            this.lblPackageFieldValue.Text = "Value";
            // 
            // cbPackageField
            // 
            this.cbPackageField.FormattingEnabled = true;
            this.cbPackageField.Location = new System.Drawing.Point(43, 1);
            this.cbPackageField.Name = "cbPackageField";
            this.cbPackageField.Size = new System.Drawing.Size(96, 21);
            this.cbPackageField.TabIndex = 1;
            // 
            // lblPackageField
            // 
            this.lblPackageField.AutoSize = true;
            this.lblPackageField.Location = new System.Drawing.Point(7, 4);
            this.lblPackageField.Name = "lblPackageField";
            this.lblPackageField.Size = new System.Drawing.Size(29, 13);
            this.lblPackageField.TabIndex = 0;
            this.lblPackageField.Text = "Field";
            // 
            // ProductsPanel
            // 
            this.ProductsPanel.ColumnCount = 1;
            this.ProductsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ProductsPanel.Controls.Add(this.ProductsGrid, 0, 1);
            this.ProductsPanel.Controls.Add(this.pnlCommoditiesTop, 0, 0);
            this.ProductsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProductsPanel.Location = new System.Drawing.Point(0, 0);
            this.ProductsPanel.Name = "ProductsPanel";
            this.ProductsPanel.RowCount = 2;
            this.ProductsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.ProductsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ProductsPanel.Size = new System.Drawing.Size(347, 239);
            this.ProductsPanel.TabIndex = 1;
            // 
            // ProductsGrid
            // 
            this.ProductsGrid.AllowUserToAddRows = false;
            this.ProductsGrid.AllowUserToDeleteRows = false;
            this.ProductsGrid.AllowUserToOrderColumns = true;
            this.ProductsGrid.AllowUserToResizeRows = false;
            this.ProductsGrid.AutoGenerateColumns = false;
            this.ProductsGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProductsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.ProductsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtProductOrderNumber,
            this.txtProductNumber,
            this.txtProductDescription,
            this.chkProductIsDocument,
            this.txtProductCountry,
            this.txtProductQuantity,
            this.txtProductMU,
            this.txtProductUnitWeight,
            this.txtProductUnitValue,
            this.txtProductCustomsValue,
            this.txtProductTotalWeight,
            this.txtProductHarmonizedCode});
            this.ProductsGrid.DataSource = this.fKOrdersTableOrderProductsTableBindingSource;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ProductsGrid.DefaultCellStyle = dataGridViewCellStyle15;
            this.ProductsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProductsGrid.Location = new System.Drawing.Point(3, 26);
            this.ProductsGrid.MultiSelect = false;
            this.ProductsGrid.Name = "ProductsGrid";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProductsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.ProductsGrid.RowHeadersVisible = false;
            this.ProductsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ProductsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProductsGrid.Size = new System.Drawing.Size(341, 227);
            this.ProductsGrid.TabIndex = 0;
            this.ProductsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ProductsGrid_DataError);
            // 
            // txtProductOrderNumber
            // 
            this.txtProductOrderNumber.DataPropertyName = "Order #";
            this.txtProductOrderNumber.Frozen = true;
            this.txtProductOrderNumber.HeaderText = "Order #";
            this.txtProductOrderNumber.Name = "txtProductOrderNumber";
            this.txtProductOrderNumber.ReadOnly = true;
            this.txtProductOrderNumber.Visible = false;
            // 
            // txtProductNumber
            // 
            this.txtProductNumber.DataPropertyName = "Product #";
            this.txtProductNumber.Frozen = true;
            this.txtProductNumber.HeaderText = "Product #";
            this.txtProductNumber.Name = "txtProductNumber";
            this.txtProductNumber.ReadOnly = true;
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.DataPropertyName = "Product Description";
            this.txtProductDescription.HeaderText = "Description";
            this.txtProductDescription.Name = "txtProductDescription";
            // 
            // chkProductIsDocument
            // 
            this.chkProductIsDocument.DataPropertyName = "Product Is Document";
            this.chkProductIsDocument.HeaderText = "Is Document";
            this.chkProductIsDocument.Name = "chkProductIsDocument";
            // 
            // txtProductCountry
            // 
            this.txtProductCountry.DataPropertyName = "Product Manufacture Country";
            this.txtProductCountry.HeaderText = "Country";
            this.txtProductCountry.Name = "txtProductCountry";
            // 
            // txtProductQuantity
            // 
            this.txtProductQuantity.DataPropertyName = "Product Quantity";
            this.txtProductQuantity.HeaderText = "Quantity";
            this.txtProductQuantity.Name = "txtProductQuantity";
            // 
            // txtProductMU
            // 
            this.txtProductMU.DataPropertyName = "Product Quantity MU";
            this.txtProductMU.HeaderText = "MU";
            this.txtProductMU.Name = "txtProductMU";
            // 
            // txtProductUnitWeight
            // 
            this.txtProductUnitWeight.DataPropertyName = "Product Unit Weight";
            this.txtProductUnitWeight.HeaderText = "Unit Weight";
            this.txtProductUnitWeight.Name = "txtProductUnitWeight";
            // 
            // txtProductUnitValue
            // 
            this.txtProductUnitValue.DataPropertyName = "Product Unit Value";
            this.txtProductUnitValue.HeaderText = "Unit Value";
            this.txtProductUnitValue.Name = "txtProductUnitValue";
            // 
            // txtProductCustomsValue
            // 
            this.txtProductCustomsValue.DataPropertyName = "Total Customs Value";
            this.txtProductCustomsValue.HeaderText = "Total Value";
            this.txtProductCustomsValue.Name = "txtProductCustomsValue";
            this.txtProductCustomsValue.Visible = false;
            // 
            // txtProductTotalWeight
            // 
            this.txtProductTotalWeight.DataPropertyName = "Total Weight";
            this.txtProductTotalWeight.HeaderText = "Total Weight";
            this.txtProductTotalWeight.Name = "txtProductTotalWeight";
            this.txtProductTotalWeight.Visible = false;
            // 
            // txtProductHarmonizedCode
            // 
            this.txtProductHarmonizedCode.DataPropertyName = "Product Harmonized Code";
            this.txtProductHarmonizedCode.HeaderText = "Harmonized Code";
            this.txtProductHarmonizedCode.Name = "txtProductHarmonizedCode";
            // 
            // fKOrdersTableOrderProductsTableBindingSource
            // 
            this.fKOrdersTableOrderProductsTableBindingSource.DataMember = "FK_OrdersTable_OrderProductsTable";
            this.fKOrdersTableOrderProductsTableBindingSource.DataSource = this.OrdersTableBindingSource;
            // 
            // pnlCommoditiesTop
            // 
            this.pnlCommoditiesTop.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlCommoditiesTop.Controls.Add(this.btnScanProducts);
            this.pnlCommoditiesTop.Controls.Add(this.cmdCommodityDelete);
            this.pnlCommoditiesTop.Controls.Add(this.ProductsLabel);
            this.pnlCommoditiesTop.Location = new System.Drawing.Point(0, 0);
            this.pnlCommoditiesTop.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCommoditiesTop.Name = "pnlCommoditiesTop";
            this.pnlCommoditiesTop.Size = new System.Drawing.Size(347, 23);
            this.pnlCommoditiesTop.TabIndex = 1;
            // 
            // btnScanProducts
            // 
            this.btnScanProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnScanProducts.Location = new System.Drawing.Point(244, -1);
            this.btnScanProducts.Name = "btnScanProducts";
            this.btnScanProducts.Size = new System.Drawing.Size(75, 23);
            this.btnScanProducts.TabIndex = 4;
            this.btnScanProducts.Text = "Scan Products";
            this.ToolTip.SetToolTip(this.btnScanProducts, "Scan products into packages");
            this.btnScanProducts.UseVisualStyleBackColor = true;
            this.btnScanProducts.Click += new System.EventHandler(this.btnScanProducts_Click);
            // 
            // cmdCommodityDelete
            // 
            this.cmdCommodityDelete.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdCommodityDelete.Image = ((System.Drawing.Image)(resources.GetObject("cmdCommodityDelete.Image")));
            this.cmdCommodityDelete.Location = new System.Drawing.Point(324, 0);
            this.cmdCommodityDelete.Name = "cmdCommodityDelete";
            this.cmdCommodityDelete.Size = new System.Drawing.Size(23, 23);
            this.cmdCommodityDelete.TabIndex = 3;
            this.ToolTip.SetToolTip(this.cmdCommodityDelete, "Delete product");
            this.cmdCommodityDelete.UseVisualStyleBackColor = true;
            this.cmdCommodityDelete.Click += new System.EventHandler(this.cmdCommodityDelete_Click);
            // 
            // ProductsLabel
            // 
            this.ProductsLabel.AutoEllipsis = true;
            this.ProductsLabel.AutoSize = true;
            this.ProductsLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ProductsLabel.Location = new System.Drawing.Point(0, 0);
            this.ProductsLabel.Name = "ProductsLabel";
            this.ProductsLabel.Size = new System.Drawing.Size(134, 13);
            this.ProductsLabel.TabIndex = 2;
            this.ProductsLabel.Text = "Documents / Commodities:";
            this.ProductsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CarrierPackagingsBS
            // 
            this.CarrierPackagingsBS.DataSource = this.BillingTypesBS;
            // 
            // CarrierPackagingsBSFiltered
            // 
            this.CarrierPackagingsBSFiltered.DataSource = this.BillingTypesBS;
            // 
            // ServicesBSFiltered
            // 
            this.ServicesBSFiltered.DataSource = this.ServicesBS;
            this.ServicesBSFiltered.Filter = "";
            // 
            // fKOrdersTableRequestsTableBindingSource
            // 
            this.fKOrdersTableRequestsTableBindingSource.DataMember = "FK_OrdersTable_RequestsTable";
            this.fKOrdersTableRequestsTableBindingSource.DataSource = this.OrdersTableBindingSource;
            this.fKOrdersTableRequestsTableBindingSource.Sort = "Field, Index";
            // 
            // MainPanel
            // 
            // 
            // MainPanel.BottomToolStripPanel
            // 
            this.MainPanel.BottomToolStripPanel.Controls.Add(this.MainStatusbar);
            // 
            // MainPanel.ContentPanel
            // 
            this.MainPanel.ContentPanel.Controls.Add(this.MainPages);
            this.MainPanel.ContentPanel.Size = new System.Drawing.Size(1022, 446);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(1022, 499);
            this.MainPanel.TabIndex = 1;
            this.MainPanel.Text = "toolStripContainer1";
            // 
            // MainPanel.TopToolStripPanel
            // 
            this.MainPanel.TopToolStripPanel.Controls.Add(this.TemplatesToolstrip);
            // 
            // MainStatusbar
            // 
            this.MainStatusbar.Dock = System.Windows.Forms.DockStyle.None;
            this.MainStatusbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.MainStatusbar.Location = new System.Drawing.Point(0, 0);
            this.MainStatusbar.Name = "MainStatusbar";
            this.MainStatusbar.Size = new System.Drawing.Size(1022, 22);
            this.MainStatusbar.TabIndex = 1;
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(26, 17);
            this.StatusLabel.Text = "Idle";
            // 
            // MainPages
            // 
            this.MainPages.Controls.Add(this.OrdersPage);
            this.MainPages.Controls.Add(this.TwoShipPage);
            this.MainPages.Controls.Add(this.ShipmentsPage);
            this.MainPages.Controls.Add(this.ProcessedOrdersPage);
            this.MainPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPages.Location = new System.Drawing.Point(0, 0);
            this.MainPages.Name = "MainPages";
            this.MainPages.Padding = new System.Drawing.Point(6, 5);
            this.MainPages.SelectedIndex = 0;
            this.MainPages.Size = new System.Drawing.Size(1022, 446);
            this.MainPages.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.MainPages.TabIndex = 1;
            this.MainPages.SelectedIndexChanged += new System.EventHandler(this.MainPages_SelectedIndexChanged);
            this.MainPages.Selected += new System.Windows.Forms.TabControlEventHandler(this.MainPages_Selected);
            // 
            // OrdersPage
            // 
            this.OrdersPage.Controls.Add(this.OrdersHSplit);
            this.OrdersPage.Location = new System.Drawing.Point(4, 26);
            this.OrdersPage.Name = "OrdersPage";
            this.OrdersPage.Padding = new System.Windows.Forms.Padding(3);
            this.OrdersPage.Size = new System.Drawing.Size(1014, 416);
            this.OrdersPage.TabIndex = 0;
            this.OrdersPage.Text = "Current Orders";
            this.OrdersPage.UseVisualStyleBackColor = true;
            // 
            // TwoShipPage
            // 
            this.TwoShipPage.Controls.Add(this.chromeBrowser);//.Browser);
            this.TwoShipPage.Location = new System.Drawing.Point(4, 26);
            this.TwoShipPage.Name = "TwoShipPage";
            this.TwoShipPage.Padding = new System.Windows.Forms.Padding(3);
            this.TwoShipPage.Size = new System.Drawing.Size(1014, 416);
            this.TwoShipPage.TabIndex = 2;
            this.TwoShipPage.Text = "2Ship Order Edit";
            this.TwoShipPage.UseVisualStyleBackColor = true;
            // 
            // Browser
            // 
            InitializeChromium();
            this.chromeBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chromeBrowser.Location = new System.Drawing.Point(3, 3);
            this.chromeBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.chromeBrowser.Name = "Browser";
            this.chromeBrowser.Size = new System.Drawing.Size(1008, 410);
            this.chromeBrowser.TabIndex = 0;
            //this.chromeBrowser.NewWindow2 += new System.EventHandler<InteGr8.NewWindow2EventArgs>(this.Browser_NewWindow2);
            //this.chromeBrowser.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.Browser_Navigated);
            // 
            // ShipmentsPage
            // 
            this.ShipmentsPage.Controls.Add(this.ShipmentsToolBar);
            this.ShipmentsPage.Controls.Add(this.ShipmentsGrid);
            this.ShipmentsPage.Location = new System.Drawing.Point(4, 26);
            this.ShipmentsPage.Name = "ShipmentsPage";
            this.ShipmentsPage.Padding = new System.Windows.Forms.Padding(3);
            this.ShipmentsPage.Size = new System.Drawing.Size(1014, 416);
            this.ShipmentsPage.TabIndex = 1;
            this.ShipmentsPage.Text = "2Ship Shipments";
            this.ShipmentsPage.UseVisualStyleBackColor = true;
            // 
            // ShipmentsToolBar
            // 
            this.ShipmentsToolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.ShipmentsToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShipmentsLabelButton,
            this.ShipmentsCIButton,
            this.ShipmentsBOLPrint,
            this.ShipmentsReturnLabel,
            this.btn_shipments_packslip,
            this.btn_shipments_contentslabel,
            this.toolStripSeparator2,
            this.btn_select_all,
            this.btn_unselect_all,
            this.btnEditRates,
            this.btn_close,
            this.btn_aggregate,
            this.ShipmentsSave,
            this.ShipmentsDelete,
            this.btn_clear});
            this.ShipmentsToolBar.Location = new System.Drawing.Point(3, 3);
            this.ShipmentsToolBar.Name = "ShipmentsToolBar";
            this.ShipmentsToolBar.Padding = new System.Windows.Forms.Padding(0);
            this.ShipmentsToolBar.Size = new System.Drawing.Size(1008, 31);
            this.ShipmentsToolBar.TabIndex = 5;
            // 
            // ShipmentsLabelButton
            // 
            this.ShipmentsLabelButton.Image = ((System.Drawing.Image)(resources.GetObject("ShipmentsLabelButton.Image")));
            this.ShipmentsLabelButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ShipmentsLabelButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ShipmentsLabelButton.Name = "ShipmentsLabelButton";
            this.ShipmentsLabelButton.Size = new System.Drawing.Size(63, 28);
            this.ShipmentsLabelButton.Text = "Label";
            this.ShipmentsLabelButton.ToolTipText = "Open the shipment label";
            this.ShipmentsLabelButton.Click += new System.EventHandler(this.ShipmentsLabelButton_Click);
            // 
            // ShipmentsCIButton
            // 
            this.ShipmentsCIButton.Image = ((System.Drawing.Image)(resources.GetObject("ShipmentsCIButton.Image")));
            this.ShipmentsCIButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ShipmentsCIButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ShipmentsCIButton.Name = "ShipmentsCIButton";
            this.ShipmentsCIButton.Size = new System.Drawing.Size(141, 28);
            this.ShipmentsCIButton.Text = "Commercial Invoice";
            this.ShipmentsCIButton.ToolTipText = "Open the commercial invoice pdf file";
            this.ShipmentsCIButton.Click += new System.EventHandler(this.ShipmentsCIButton_Click);
            // 
            // ShipmentsBOLPrint
            // 
            this.ShipmentsBOLPrint.Image = ((System.Drawing.Image)(resources.GetObject("ShipmentsBOLPrint.Image")));
            this.ShipmentsBOLPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ShipmentsBOLPrint.Name = "ShipmentsBOLPrint";
            this.ShipmentsBOLPrint.Size = new System.Drawing.Size(57, 28);
            this.ShipmentsBOLPrint.Text = "BOL";
            this.ShipmentsBOLPrint.ToolTipText = "Show shipment BOL";
            this.ShipmentsBOLPrint.Click += new System.EventHandler(this.ShipmentsBOLPrint_Click);
            // 
            // ShipmentsReturnLabel
            // 
            this.ShipmentsReturnLabel.Image = ((System.Drawing.Image)(resources.GetObject("ShipmentsReturnLabel.Image")));
            this.ShipmentsReturnLabel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ShipmentsReturnLabel.Name = "ShipmentsReturnLabel";
            this.ShipmentsReturnLabel.Size = new System.Drawing.Size(101, 28);
            this.ShipmentsReturnLabel.Text = "Return Label";
            this.ShipmentsReturnLabel.Click += new System.EventHandler(this.ShipmentsReturnLabel_Click);
            // 
            // btn_shipments_packslip
            // 
            this.btn_shipments_packslip.Image = ((System.Drawing.Image)(resources.GetObject("btn_shipments_packslip.Image")));
            this.btn_shipments_packslip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_shipments_packslip.Name = "btn_shipments_packslip";
            this.btn_shipments_packslip.Size = new System.Drawing.Size(78, 28);
            this.btn_shipments_packslip.Text = "Packslip";
            this.btn_shipments_packslip.Click += new System.EventHandler(this.btn_shipments_packslip_Click);
            // 
            // btn_shipments_contentslabel
            // 
            this.btn_shipments_contentslabel.Image = ((System.Drawing.Image)(resources.GetObject("btn_shipments_contentslabel.Image")));
            this.btn_shipments_contentslabel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_shipments_contentslabel.Name = "btn_shipments_contentslabel";
            this.btn_shipments_contentslabel.Size = new System.Drawing.Size(114, 28);
            this.btn_shipments_contentslabel.Text = "Contents Label";
            this.btn_shipments_contentslabel.Visible = false;
            this.btn_shipments_contentslabel.Click += new System.EventHandler(this.btn_shipments_contentslabel_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // btn_select_all
            // 
            this.btn_select_all.Image = ((System.Drawing.Image)(resources.GetObject("btn_select_all.Image")));
            this.btn_select_all.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_select_all.Name = "btn_select_all";
            this.btn_select_all.Size = new System.Drawing.Size(81, 28);
            this.btn_select_all.Text = "Select all";
            this.btn_select_all.Click += new System.EventHandler(this.btn_select_all_Click);
            // 
            // btn_unselect_all
            // 
            this.btn_unselect_all.Image = ((System.Drawing.Image)(resources.GetObject("btn_unselect_all.Image")));
            this.btn_unselect_all.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_unselect_all.Name = "btn_unselect_all";
            this.btn_unselect_all.Size = new System.Drawing.Size(95, 28);
            this.btn_unselect_all.Text = "Unselect all";
            this.btn_unselect_all.Click += new System.EventHandler(this.btn_unselect_all_Click);
            // 
            // btnEditRates
            // 
            this.btnEditRates.Image = ((System.Drawing.Image)(resources.GetObject("btnEditRates.Image")));
            this.btnEditRates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditRates.Name = "btnEditRates";
            this.btnEditRates.Size = new System.Drawing.Size(55, 28);
            this.btnEditRates.Text = "Edit";
            this.btnEditRates.Click += new System.EventHandler(this.btnEditRates_Click);
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.Aqua;
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(64, 28);
            this.btn_close.Text = "Close";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_aggregate
            // 
            this.btn_aggregate.BackColor = System.Drawing.Color.Yellow;
            this.btn_aggregate.Image = ((System.Drawing.Image)(resources.GetObject("btn_aggregate.Image")));
            this.btn_aggregate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_aggregate.Name = "btn_aggregate";
            this.btn_aggregate.Size = new System.Drawing.Size(90, 28);
            this.btn_aggregate.Text = "Aggregate";
            this.btn_aggregate.ToolTipText = "Aggregate";
            this.btn_aggregate.Click += new System.EventHandler(this.btn_aggregate_Click);
            // 
            // ShipmentsSave
            // 
            this.ShipmentsSave.BackColor = System.Drawing.Color.LimeGreen;
            this.ShipmentsSave.Image = ((System.Drawing.Image)(resources.GetObject("ShipmentsSave.Image")));
            this.ShipmentsSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ShipmentsSave.Name = "ShipmentsSave";
            this.ShipmentsSave.Size = new System.Drawing.Size(59, 28);
            this.ShipmentsSave.Text = "Save";
            this.ShipmentsSave.ToolTipText = "Commit all the shipments into your database";
            this.ShipmentsSave.Click += new System.EventHandler(this.ShipmentsSave_Click);
            // 
            // ShipmentsDelete
            // 
            this.ShipmentsDelete.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ShipmentsDelete.Image = ((System.Drawing.Image)(resources.GetObject("ShipmentsDelete.Image")));
            this.ShipmentsDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ShipmentsDelete.Name = "ShipmentsDelete";
            this.ShipmentsDelete.Size = new System.Drawing.Size(68, 28);
            this.ShipmentsDelete.Text = "Delete";
            this.ShipmentsDelete.ToolTipText = "Delete the current shipment from 2Ship";
            this.ShipmentsDelete.Click += new System.EventHandler(this.ShipmentsDelete_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.Image = ((System.Drawing.Image)(resources.GetObject("btn_clear.Image")));
            this.btn_clear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(83, 28);
            this.btn_clear.Text = "Clear List";
            this.btn_clear.Visible = false;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // ShipmentsGrid
            // 
            this.ShipmentsGrid.AllowUserToAddRows = false;
            this.ShipmentsGrid.AllowUserToDeleteRows = false;
            this.ShipmentsGrid.AllowUserToOrderColumns = true;
            this.ShipmentsGrid.AllowUserToResizeRows = false;
            this.ShipmentsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ShipmentsGrid.AutoGenerateColumns = false;
            this.ShipmentsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ShipmentsGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ShipmentsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.ShipmentsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ShipmentsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ShipmentsSelectedColumn,
            this.Shipmentsorder,
            this.Package_Index,
            this.Shipmentsstatus,
            this.Shipment_Tracking_Number,
            this.ShipmentslabelURL,
            this.commercialInvoiceURL,
            this.ShipmentsCarrier,
            this.ShipmentsService,
            this.price,
            this.delivery,
            this.errorCode,
            this.errorMessage,
            this.CarrierCode,
            this.ServiceCode});
            this.ShipmentsGrid.DataSource = this.shipmentsTableBindingSource;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.DarkOrange;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ShipmentsGrid.DefaultCellStyle = dataGridViewCellStyle18;
            this.ShipmentsGrid.Location = new System.Drawing.Point(0, 40);
            this.ShipmentsGrid.MultiSelect = false;
            this.ShipmentsGrid.Name = "ShipmentsGrid";
            this.ShipmentsGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ShipmentsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.ShipmentsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            this.ShipmentsGrid.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.ShipmentsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ShipmentsGrid.Size = new System.Drawing.Size(988, 359);
            this.ShipmentsGrid.TabIndex = 3;
            this.ShipmentsGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ShipmentsGrid_CellContentClick);
            this.ShipmentsGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ShipmentsGrid_CellEndEdit);
            this.ShipmentsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ShipmentsGrid_DataError);
            this.ShipmentsGrid.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.ShipmentsGrid_RowPostPaint);
            this.ShipmentsGrid.Paint += new System.Windows.Forms.PaintEventHandler(this.ShipmentsGrid_Paint);
            // 
            // ShipmentsSelectedColumn
            // 
            this.ShipmentsSelectedColumn.DataPropertyName = "Selected";
            this.ShipmentsSelectedColumn.FalseValue = "false";
            this.ShipmentsSelectedColumn.HeaderText = "Select";
            this.ShipmentsSelectedColumn.MinimumWidth = 10;
            this.ShipmentsSelectedColumn.Name = "ShipmentsSelectedColumn";
            this.ShipmentsSelectedColumn.TrueValue = "true";
            // 
            // Shipmentsorder
            // 
            this.Shipmentsorder.DataPropertyName = "Order #";
            this.Shipmentsorder.HeaderText = "Order #";
            this.Shipmentsorder.Name = "Shipmentsorder";
            this.Shipmentsorder.ReadOnly = true;
            // 
            // Package_Index
            // 
            this.Package_Index.DataPropertyName = "Packages";
            this.Package_Index.HeaderText = "Packages";
            this.Package_Index.Name = "Package_Index";
            this.Package_Index.ReadOnly = true;
            // 
            // Shipmentsstatus
            // 
            this.Shipmentsstatus.DataPropertyName = "Status";
            this.Shipmentsstatus.HeaderText = "Status";
            this.Shipmentsstatus.Name = "Shipmentsstatus";
            // 
            // Shipment_Tracking_Number
            // 
            this.Shipment_Tracking_Number.DataPropertyName = "Shipment Tracking Number";
            this.Shipment_Tracking_Number.HeaderText = "Tracking #";
            this.Shipment_Tracking_Number.Name = "Shipment_Tracking_Number";
            this.Shipment_Tracking_Number.ReadOnly = true;
            // 
            // ShipmentslabelURL
            // 
            this.ShipmentslabelURL.DataPropertyName = "Label URL";
            this.ShipmentslabelURL.HeaderText = "Label URL";
            this.ShipmentslabelURL.Name = "ShipmentslabelURL";
            this.ShipmentslabelURL.ReadOnly = true;
            // 
            // commercialInvoiceURL
            // 
            this.commercialInvoiceURL.DataPropertyName = "Commercial Invoice URL";
            this.commercialInvoiceURL.HeaderText = "Commercial Invoice URL";
            this.commercialInvoiceURL.Name = "commercialInvoiceURL";
            this.commercialInvoiceURL.ReadOnly = true;
            // 
            // ShipmentsCarrier
            // 
            this.ShipmentsCarrier.DataPropertyName = "Carrier";
            this.ShipmentsCarrier.HeaderText = "Carrier";
            this.ShipmentsCarrier.Name = "ShipmentsCarrier";
            this.ShipmentsCarrier.ReadOnly = true;
            // 
            // ShipmentsService
            // 
            this.ShipmentsService.DataPropertyName = "Service";
            this.ShipmentsService.HeaderText = "Service";
            this.ShipmentsService.Name = "ShipmentsService";
            this.ShipmentsService.ReadOnly = true;
            // 
            // price
            // 
            this.price.DataPropertyName = "Price";
            this.price.HeaderText = "Price";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // delivery
            // 
            this.delivery.DataPropertyName = "Delivery";
            this.delivery.HeaderText = "Delivery";
            this.delivery.Name = "delivery";
            this.delivery.ReadOnly = true;
            // 
            // errorCode
            // 
            this.errorCode.DataPropertyName = "Error Code";
            this.errorCode.HeaderText = "Error Code";
            this.errorCode.Name = "errorCode";
            this.errorCode.ReadOnly = true;
            // 
            // errorMessage
            // 
            this.errorMessage.DataPropertyName = "Error Message";
            this.errorMessage.HeaderText = "Error Message";
            this.errorMessage.Name = "errorMessage";
            this.errorMessage.ReadOnly = true;
            // 
            // CarrierCode
            // 
            this.CarrierCode.DataPropertyName = "Carrier Code";
            this.CarrierCode.HeaderText = "Carrier Code";
            this.CarrierCode.Name = "CarrierCode";
            this.CarrierCode.Visible = false;
            // 
            // ServiceCode
            // 
            this.ServiceCode.DataPropertyName = "Service Code";
            this.ServiceCode.HeaderText = "Service Code";
            this.ServiceCode.Name = "ServiceCode";
            this.ServiceCode.Visible = false;
            // 
            // shipmentsTableBindingSource
            // 
            this.shipmentsTableBindingSource.DataMember = "ShipmentsTable";
            this.shipmentsTableBindingSource.DataSource = this.data;
            this.shipmentsTableBindingSource.Filter = "Package_Index = 0";
            // 
            // ProcessedOrdersPage
            // 
            this.ProcessedOrdersPage.Controls.Add(this.HistoryGrid);
            this.ProcessedOrdersPage.Controls.Add(this.HistoryToolbar);
            this.ProcessedOrdersPage.Location = new System.Drawing.Point(4, 26);
            this.ProcessedOrdersPage.Name = "ProcessedOrdersPage";
            this.ProcessedOrdersPage.Padding = new System.Windows.Forms.Padding(3);
            this.ProcessedOrdersPage.Size = new System.Drawing.Size(1014, 416);
            this.ProcessedOrdersPage.TabIndex = 3;
            this.ProcessedOrdersPage.Text = "Processed Orders";
            this.ProcessedOrdersPage.UseVisualStyleBackColor = true;
            // 
            // HistoryGrid
            // 
            this.HistoryGrid.AllowUserToAddRows = false;
            this.HistoryGrid.AllowUserToDeleteRows = false;
            this.HistoryGrid.AllowUserToOrderColumns = true;
            this.HistoryGrid.AllowUserToResizeRows = false;
            this.HistoryGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HistoryGrid.AutoGenerateColumns = false;
            this.HistoryGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HistoryGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.HistoryGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HistoryGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selected_ord,
            this.HistoryOrder,
            this.HistoryTN,
            this.OrderNumber,
            this.Token,
            this.ProNumber,
            this.CIURL,
            this.BOLURL,
            this.ReturnLabelURL,
            this.RecipientCompany,
            this.RecipientAddress1,
            this.RecipientAddress2,
            this.RecipientCity,
            this.RecipientZIP,
            this.RecipientState,
            this.RecipientCountry,
            this.Carrier,
            this.Service});
            this.HistoryGrid.DataMember = "HistoryTable";
            this.HistoryGrid.DataSource = this.HistoryBindingSource;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.HistoryGrid.DefaultCellStyle = dataGridViewCellStyle22;
            this.HistoryGrid.Location = new System.Drawing.Point(3, 34);
            this.HistoryGrid.MultiSelect = false;
            this.HistoryGrid.Name = "HistoryGrid";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HistoryGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.HistoryGrid.RowHeadersVisible = false;
            this.HistoryGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.HistoryGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HistoryGrid.Size = new System.Drawing.Size(985, 359);
            this.HistoryGrid.TabIndex = 7;
            // 
            // Selected_ord
            // 
            this.Selected_ord.DataPropertyName = "Select";
            this.Selected_ord.FalseValue = "false";
            this.Selected_ord.HeaderText = "Select";
            this.Selected_ord.Name = "Selected_ord";
            this.Selected_ord.TrueValue = "true";
            // 
            // HistoryOrder
            // 
            this.HistoryOrder.DataPropertyName = "Order #";
            this.HistoryOrder.HeaderText = "Order #";
            this.HistoryOrder.Name = "HistoryOrder";
            this.HistoryOrder.ReadOnly = true;
            this.HistoryOrder.Width = 150;
            // 
            // HistoryTN
            // 
            this.HistoryTN.DataPropertyName = "Tracking #";
            this.HistoryTN.HeaderText = "Tracking #";
            this.HistoryTN.Name = "HistoryTN";
            this.HistoryTN.ReadOnly = true;
            this.HistoryTN.Width = 150;
            // 
            // OrderNumber
            // 
            this.OrderNumber.DataPropertyName = "Order Number";
            this.OrderNumber.HeaderText = "Order Number";
            this.OrderNumber.Name = "OrderNumber";
            this.OrderNumber.ReadOnly = true;
            this.OrderNumber.Visible = false;
            // 
            // Token
            // 
            this.Token.DataPropertyName = "Token";
            this.Token.HeaderText = "Token";
            this.Token.Name = "Token";
            this.Token.ReadOnly = true;
            this.Token.Visible = false;
            // 
            // ProNumber
            // 
            this.ProNumber.DataPropertyName = "PRO #";
            this.ProNumber.HeaderText = "PRO #";
            this.ProNumber.Name = "ProNumber";
            this.ProNumber.ReadOnly = true;
            // 
            // CIURL
            // 
            this.CIURL.DataPropertyName = "Commercial Invoice URL";
            this.CIURL.HeaderText = "Commercial Invoice URL";
            this.CIURL.Name = "CIURL";
            this.CIURL.ReadOnly = true;
            this.CIURL.Visible = false;
            // 
            // BOLURL
            // 
            this.BOLURL.DataPropertyName = "Bill of Lading";
            this.BOLURL.HeaderText = "Bill of Lading URL";
            this.BOLURL.Name = "BOLURL";
            this.BOLURL.ReadOnly = true;
            this.BOLURL.Visible = false;
            // 
            // ReturnLabelURL
            // 
            this.ReturnLabelURL.DataPropertyName = "Return Label URL";
            this.ReturnLabelURL.HeaderText = "Return Label URL";
            this.ReturnLabelURL.Name = "ReturnLabelURL";
            this.ReturnLabelURL.ReadOnly = true;
            this.ReturnLabelURL.Visible = false;
            // 
            // RecipientCompany
            // 
            this.RecipientCompany.DataPropertyName = "Recipient Company";
            this.RecipientCompany.HeaderText = "Recipient Company";
            this.RecipientCompany.Name = "RecipientCompany";
            this.RecipientCompany.ReadOnly = true;
            this.RecipientCompany.Visible = false;
            // 
            // RecipientAddress1
            // 
            this.RecipientAddress1.DataPropertyName = "Recipient Address 1";
            this.RecipientAddress1.HeaderText = "Recipient Address 1";
            this.RecipientAddress1.Name = "RecipientAddress1";
            this.RecipientAddress1.ReadOnly = true;
            this.RecipientAddress1.Visible = false;
            // 
            // RecipientAddress2
            // 
            this.RecipientAddress2.DataPropertyName = "Recipient Address 2";
            this.RecipientAddress2.HeaderText = "Recipient Address 2";
            this.RecipientAddress2.Name = "RecipientAddress2";
            this.RecipientAddress2.ReadOnly = true;
            this.RecipientAddress2.Visible = false;
            // 
            // RecipientCity
            // 
            this.RecipientCity.DataPropertyName = "Recipient City";
            this.RecipientCity.HeaderText = "Recipient City";
            this.RecipientCity.Name = "RecipientCity";
            this.RecipientCity.ReadOnly = true;
            this.RecipientCity.Visible = false;
            // 
            // RecipientZIP
            // 
            this.RecipientZIP.DataPropertyName = "Recipient ZIP";
            this.RecipientZIP.HeaderText = "Recipient ZIP";
            this.RecipientZIP.Name = "RecipientZIP";
            this.RecipientZIP.ReadOnly = true;
            this.RecipientZIP.Visible = false;
            // 
            // RecipientState
            // 
            this.RecipientState.DataPropertyName = "Recipient State/Province";
            this.RecipientState.HeaderText = "Recipient State/Province";
            this.RecipientState.Name = "RecipientState";
            this.RecipientState.ReadOnly = true;
            this.RecipientState.Visible = false;
            // 
            // RecipientCountry
            // 
            this.RecipientCountry.DataPropertyName = "Recipient Contry";
            this.RecipientCountry.HeaderText = "Recipient Contry";
            this.RecipientCountry.Name = "RecipientCountry";
            this.RecipientCountry.ReadOnly = true;
            this.RecipientCountry.Visible = false;
            // 
            // Carrier
            // 
            this.Carrier.DataPropertyName = "Carrier";
            this.Carrier.HeaderText = "Carrier";
            this.Carrier.Name = "Carrier";
            this.Carrier.ReadOnly = true;
            // 
            // Service
            // 
            this.Service.DataPropertyName = "Service";
            this.Service.HeaderText = "Service";
            this.Service.Name = "Service";
            this.Service.ReadOnly = true;
            // 
            // HistoryBindingSource
            // 
            this.HistoryBindingSource.AllowNew = true;
            this.HistoryBindingSource.DataSource = this.data;
            this.HistoryBindingSource.Position = 0;
            // 
            // HistoryToolbar
            // 
            this.HistoryToolbar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.HistoryToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator7,
            this.btnHistoryLabel,
            this.btnHistoryCI,
            this.btnHistoryBOLPrint,
            this.btnHistoryReturnLabel,
            this.btn_history_packslip,
            this.btn_selectall,
            this.btn_unselectall,
            this.HistoryDeleteButton,
            this.toolStripLabel1,
            this.HistorySearchTextbox,
            this.HistorySearchButton,
            this.toolStripButton1});
            this.HistoryToolbar.Location = new System.Drawing.Point(3, 3);
            this.HistoryToolbar.Name = "HistoryToolbar";
            this.HistoryToolbar.Padding = new System.Windows.Forms.Padding(0);
            this.HistoryToolbar.Size = new System.Drawing.Size(1008, 31);
            this.HistoryToolbar.TabIndex = 6;
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 31);
            // 
            // btnHistoryLabel
            // 
            this.btnHistoryLabel.Enabled = false;
            this.btnHistoryLabel.Image = ((System.Drawing.Image)(resources.GetObject("btnHistoryLabel.Image")));
            this.btnHistoryLabel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHistoryLabel.Name = "btnHistoryLabel";
            this.btnHistoryLabel.Size = new System.Drawing.Size(63, 28);
            this.btnHistoryLabel.Text = "Label";
            this.btnHistoryLabel.Click += new System.EventHandler(this.btnHistoryLabel_Click);
            // 
            // btnHistoryCI
            // 
            this.btnHistoryCI.Enabled = false;
            this.btnHistoryCI.Image = ((System.Drawing.Image)(resources.GetObject("btnHistoryCI.Image")));
            this.btnHistoryCI.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHistoryCI.Name = "btnHistoryCI";
            this.btnHistoryCI.Size = new System.Drawing.Size(141, 28);
            this.btnHistoryCI.Text = "Commercial Invoice";
            this.btnHistoryCI.Click += new System.EventHandler(this.btnHistoryCI_Click);
            // 
            // btnHistoryBOLPrint
            // 
            this.btnHistoryBOLPrint.Enabled = false;
            this.btnHistoryBOLPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnHistoryBOLPrint.Image")));
            this.btnHistoryBOLPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHistoryBOLPrint.Name = "btnHistoryBOLPrint";
            this.btnHistoryBOLPrint.Size = new System.Drawing.Size(104, 28);
            this.btnHistoryBOLPrint.Text = "Bill of Lading";
            this.btnHistoryBOLPrint.Click += new System.EventHandler(this.btnHistoryBOLPrint_Click);
            // 
            // btnHistoryReturnLabel
            // 
            this.btnHistoryReturnLabel.Enabled = false;
            this.btnHistoryReturnLabel.Image = ((System.Drawing.Image)(resources.GetObject("btnHistoryReturnLabel.Image")));
            this.btnHistoryReturnLabel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHistoryReturnLabel.Name = "btnHistoryReturnLabel";
            this.btnHistoryReturnLabel.Size = new System.Drawing.Size(101, 28);
            this.btnHistoryReturnLabel.Text = "Return Label";
            this.btnHistoryReturnLabel.Click += new System.EventHandler(this.btnHistoryReturnLabel_Click);
            // 
            // btn_history_packslip
            // 
            this.btn_history_packslip.Image = ((System.Drawing.Image)(resources.GetObject("btn_history_packslip.Image")));
            this.btn_history_packslip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_history_packslip.Name = "btn_history_packslip";
            this.btn_history_packslip.Size = new System.Drawing.Size(78, 28);
            this.btn_history_packslip.Text = "Packslip";
            this.btn_history_packslip.Click += new System.EventHandler(this.btn_history_packslip_Click);
            // 
            // btn_selectall
            // 
            this.btn_selectall.Image = ((System.Drawing.Image)(resources.GetObject("btn_selectall.Image")));
            this.btn_selectall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_selectall.Name = "btn_selectall";
            this.btn_selectall.Size = new System.Drawing.Size(81, 28);
            this.btn_selectall.Text = "Select all";
            this.btn_selectall.Click += new System.EventHandler(this.btn_selectall_Click);
            // 
            // btn_unselectall
            // 
            this.btn_unselectall.Image = ((System.Drawing.Image)(resources.GetObject("btn_unselectall.Image")));
            this.btn_unselectall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_unselectall.Name = "btn_unselectall";
            this.btn_unselectall.Size = new System.Drawing.Size(95, 28);
            this.btn_unselectall.Text = "Unselect all";
            this.btn_unselectall.Click += new System.EventHandler(this.btn_unselectall_Click);
            // 
            // HistoryDeleteButton
            // 
            this.HistoryDeleteButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.HistoryDeleteButton.Enabled = false;
            this.HistoryDeleteButton.Image = ((System.Drawing.Image)(resources.GetObject("HistoryDeleteButton.Image")));
            this.HistoryDeleteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HistoryDeleteButton.Name = "HistoryDeleteButton";
            this.HistoryDeleteButton.Size = new System.Drawing.Size(68, 28);
            this.HistoryDeleteButton.Text = "Delete";
            this.HistoryDeleteButton.ToolTipText = "Delete the shipment from both 2Ship and your database";
            this.HistoryDeleteButton.Click += new System.EventHandler(this.HistoryDeleteButton_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(102, 28);
            this.toolStripLabel1.Text = "Search by order #:";
            // 
            // HistorySearchTextbox
            // 
            this.HistorySearchTextbox.Name = "HistorySearchTextbox";
            this.HistorySearchTextbox.Size = new System.Drawing.Size(44, 31);
            this.HistorySearchTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HistorySearchTextbox_KeyPress);
            // 
            // HistorySearchButton
            // 
            this.HistorySearchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.HistorySearchButton.Image = ((System.Drawing.Image)(resources.GetObject("HistorySearchButton.Image")));
            this.HistorySearchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HistorySearchButton.Name = "HistorySearchButton";
            this.HistorySearchButton.Size = new System.Drawing.Size(28, 28);
            this.HistorySearchButton.Text = "Search for the entered processed order # in your database";
            this.HistorySearchButton.Click += new System.EventHandler(this.HistorySearchButton_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(84, 28);
            this.toolStripButton1.Text = "Packages";
            this.toolStripButton1.ToolTipText = "Viewe Packages";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // TemplatesToolstrip
            // 
            this.TemplatesToolstrip.Dock = System.Windows.Forms.DockStyle.None;
            this.TemplatesToolstrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.TemplatesToolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TemplatesLabel,
            this.TemplatesDropdown,
            this.TemplatesSeparator1,
            this.TemplatesNew,
            this.TemplatesEdit,
            this.TemplatesDelete,
            this.TemplatesSeparator2,
            this.TemplatesImport,
            this.TemplatesExport,
            this.toolStripSeparator4,
            this.Options,
            this.tsButtonPackslip});
            this.TemplatesToolstrip.Location = new System.Drawing.Point(3, 0);
            this.TemplatesToolstrip.Name = "TemplatesToolstrip";
            this.TemplatesToolstrip.Size = new System.Drawing.Size(635, 31);
            this.TemplatesToolstrip.TabIndex = 2;
            // 
            // TemplatesLabel
            // 
            this.TemplatesLabel.Name = "TemplatesLabel";
            this.TemplatesLabel.Size = new System.Drawing.Size(57, 28);
            this.TemplatesLabel.Text = "Template";
            // 
            // TemplatesDropdown
            // 
            this.TemplatesDropdown.AutoSize = false;
            this.TemplatesDropdown.AutoToolTip = true;
            this.TemplatesDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TemplatesDropdown.Name = "TemplatesDropdown";
            this.TemplatesDropdown.Size = new System.Drawing.Size(300, 23);
            this.TemplatesDropdown.ToolTipText = "Please select a template";
            this.TemplatesDropdown.SelectedIndexChanged += new System.EventHandler(this.TemplatesDropdown_SelectedIndexChanged);
            // 
            // TemplatesSeparator1
            // 
            this.TemplatesSeparator1.Name = "TemplatesSeparator1";
            this.TemplatesSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // TemplatesNew
            // 
            this.TemplatesNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TemplatesNew.Image = ((System.Drawing.Image)(resources.GetObject("TemplatesNew.Image")));
            this.TemplatesNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TemplatesNew.Name = "TemplatesNew";
            this.TemplatesNew.Size = new System.Drawing.Size(28, 28);
            this.TemplatesNew.Text = "TemplatesNew";
            this.TemplatesNew.ToolTipText = "Create new template...";
            this.TemplatesNew.Click += new System.EventHandler(this.TemplatesNew_Click);
            // 
            // TemplatesEdit
            // 
            this.TemplatesEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TemplatesEdit.Image = ((System.Drawing.Image)(resources.GetObject("TemplatesEdit.Image")));
            this.TemplatesEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TemplatesEdit.Name = "TemplatesEdit";
            this.TemplatesEdit.Size = new System.Drawing.Size(28, 28);
            this.TemplatesEdit.Text = "TemplatesEdit";
            this.TemplatesEdit.ToolTipText = "Edit template...";
            this.TemplatesEdit.Click += new System.EventHandler(this.TemplatesEdit_Click);
            // 
            // TemplatesDelete
            // 
            this.TemplatesDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TemplatesDelete.Image = ((System.Drawing.Image)(resources.GetObject("TemplatesDelete.Image")));
            this.TemplatesDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TemplatesDelete.Name = "TemplatesDelete";
            this.TemplatesDelete.Size = new System.Drawing.Size(28, 28);
            this.TemplatesDelete.Text = "TemplatesDelete";
            this.TemplatesDelete.ToolTipText = "Delete template";
            this.TemplatesDelete.Click += new System.EventHandler(this.TemplatesDelete_Click);
            // 
            // TemplatesSeparator2
            // 
            this.TemplatesSeparator2.Name = "TemplatesSeparator2";
            this.TemplatesSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // TemplatesImport
            // 
            this.TemplatesImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TemplatesImport.Image = ((System.Drawing.Image)(resources.GetObject("TemplatesImport.Image")));
            this.TemplatesImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TemplatesImport.Name = "TemplatesImport";
            this.TemplatesImport.Size = new System.Drawing.Size(28, 28);
            this.TemplatesImport.Text = "TemplatesImport";
            this.TemplatesImport.ToolTipText = "Import template";
            this.TemplatesImport.Click += new System.EventHandler(this.TemplatesImport_Click);
            // 
            // TemplatesExport
            // 
            this.TemplatesExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TemplatesExport.Image = ((System.Drawing.Image)(resources.GetObject("TemplatesExport.Image")));
            this.TemplatesExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TemplatesExport.Name = "TemplatesExport";
            this.TemplatesExport.Size = new System.Drawing.Size(28, 28);
            this.TemplatesExport.Text = "TemplatesExport";
            this.TemplatesExport.ToolTipText = "Export template";
            this.TemplatesExport.Click += new System.EventHandler(this.TemplatesExport_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            // 
            // Options
            // 
            this.Options.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Options.Image = ((System.Drawing.Image)(resources.GetObject("Options.Image")));
            this.Options.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Options.Name = "Options";
            this.Options.Size = new System.Drawing.Size(28, 28);
            this.Options.Text = "Options";
            this.Options.Click += new System.EventHandler(this.Options_Click);
            // 
            // tsButtonPackslip
            // 
            this.tsButtonPackslip.Image = ((System.Drawing.Image)(resources.GetObject("tsButtonPackslip.Image")));
            this.tsButtonPackslip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButtonPackslip.Name = "tsButtonPackslip";
            this.tsButtonPackslip.Size = new System.Drawing.Size(78, 28);
            this.tsButtonPackslip.Text = "Packslip";
            this.tsButtonPackslip.Click += new System.EventHandler(this.tsButtonPackslip_Click);
            // 
            // ordersTableAdapter
            // 
            this.ordersTableAdapter.ClearBeforeFill = true;
            // 
            // orderPackagesTableAdapter
            // 
            this.orderPackagesTableAdapter.ClearBeforeFill = true;
            // 
            // orderProductsTableAdapter
            // 
            this.orderProductsTableAdapter.ClearBeforeFill = true;
            // 
            // TaskBackgroundWorker
            // 
            this.TaskBackgroundWorker.WorkerReportsProgress = true;
            this.TaskBackgroundWorker.WorkerSupportsCancellation = true;
            this.TaskBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.TaskBackgroundWorker_DoWork);
            this.TaskBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.TaskBackgroundWorker_ProgressChanged);
            this.TaskBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.TaskBackgroundWorker_RunWorkerCompleted);
            // 
            // FillBackgroundWorker
            // 
            this.FillBackgroundWorker.WorkerReportsProgress = true;
            this.FillBackgroundWorker.WorkerSupportsCancellation = true;
            this.FillBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.FillBackgroundWorker_DoWork);
            this.FillBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.FillBackgroundWorker_ProgressChanged);
            this.FillBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.FillBackgroundWorker_RunWorkerCompleted);
            // 
            // ToolTip
            // 
            this.ToolTip.AutoPopDelay = 10000;
            this.ToolTip.InitialDelay = 500;
            this.ToolTip.IsBalloon = true;
            this.ToolTip.ReshowDelay = 100;
            // 
            // chkShipmentSelected
            // 
            this.chkShipmentSelected.DataPropertyName = "Selected";
            this.chkShipmentSelected.HeaderText = "Selected";
            this.chkShipmentSelected.Name = "chkShipmentSelected";
            // 
            // txtShipmentOrderNumber
            // 
            this.txtShipmentOrderNumber.DataPropertyName = "Order #";
            this.txtShipmentOrderNumber.HeaderText = "Order #";
            this.txtShipmentOrderNumber.Name = "txtShipmentOrderNumber";
            this.txtShipmentOrderNumber.ReadOnly = true;
            // 
            // Selected
            // 
            this.Selected.DataPropertyName = "Selected";
            this.Selected.HeaderText = "Selected";
            this.Selected.Name = "Selected";
            // 
            // skidFreightClassDataGridViewComboBoxColumn
            // 
            this.skidFreightClassDataGridViewComboBoxColumn.DataPropertyName = "Skid Freight Class";
            this.skidFreightClassDataGridViewComboBoxColumn.Frozen = true;
            this.skidFreightClassDataGridViewComboBoxColumn.HeaderText = "Skid Freight Class";
            this.skidFreightClassDataGridViewComboBoxColumn.Items.AddRange(new object[] {
            "50.0",
            "55.0",
            "60.0",
            "65.0",
            "70.0",
            "77.5",
            "85.0",
            "92.0",
            "100.0",
            "110.0",
            "125.0",
            "150.0",
            "175.0",
            "200.0",
            "250.0",
            "300.0"});
            this.skidFreightClassDataGridViewComboBoxColumn.Name = "skidFreightClassDataGridViewComboBoxColumn";
            this.skidFreightClassDataGridViewComboBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.skidFreightClassDataGridViewComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // bg_print
            // 
            this.bg_print.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bg_print_DoWork);
            this.bg_print.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bg_print_RunWorkerCompleted);
            // 
            // bgworker_save
            // 
            this.bgworker_save.WorkerSupportsCancellation = true;
            this.bgworker_save.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgworker_save_DoWork);
            this.bgworker_save.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgworker_save_RunWorkerCompleted);
            // 
            // bsMenuServicesFiltered
            // 
            this.bsMenuServicesFiltered.DataSource = this.bsMenuServices;
            // 
            // bsMenuServices
            // 
            this.bsMenuServices.DataMember = "Services";
            this.bsMenuServices.DataSource = this.data;
            // 
            // bsMenuCarriers
            // 
            this.bsMenuCarriers.DataMember = "Carriers";
            this.bsMenuCarriers.DataSource = this.data;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 499);
            this.Controls.Add(this.MainPanel);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmMain";
            this.Text = "2Ship - InteGr8";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.OrdersHSplit.Panel1.ResumeLayout(false);
            this.OrdersHSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersHSplit)).EndInit();
            this.OrdersHSplit.ResumeLayout(false);
            this.OrdersPanel.ResumeLayout(false);
            this.OrdersPanel.PerformLayout();
            this.OrdersToolbar.ResumeLayout(false);
            this.OrdersToolbar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingTypesBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.data)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarriersBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServicesBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packagingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersTableBindingSource)).EndInit();
            this.FilterTableLayoutPanel.ResumeLayout(false);
            this.FilterTableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filterExpressionBindingSource)).EndInit();
            this.OrdersVSplitBottom.Panel1.ResumeLayout(false);
            this.OrdersVSplitBottom.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersVSplitBottom)).EndInit();
            this.OrdersVSplitBottom.ResumeLayout(false);
            this.OrderPackagesPanel.ResumeLayout(false);
            this.panelShipType.ResumeLayout(false);
            this.panelShipType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.packageDimsBindingSource)).EndInit();
            this.tabPackageType.ResumeLayout(false);
            this.tabPagePackages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrderPackagesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOrdersTableOrderPackagesTableBindingSource)).EndInit();
            this.tabPageSkids.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrderSkidsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fkOrdersTableOrderSkidsTableBindingSource)).EndInit();
            this.tabPageItems.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LTLItemsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fkOrdersTableLTLItemsBindingSource)).EndInit();
            this.panelPackageFields.ResumeLayout(false);
            this.panelPackageFields.PerformLayout();
            this.ProductsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProductsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOrdersTableOrderProductsTableBindingSource)).EndInit();
            this.pnlCommoditiesTop.ResumeLayout(false);
            this.pnlCommoditiesTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filterLeftOperandNameBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarrierPackagingsBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarrierPackagingsBSFiltered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServicesBSFiltered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOrdersTableRequestsTableBindingSource)).EndInit();
            this.MainPanel.BottomToolStripPanel.ResumeLayout(false);
            this.MainPanel.BottomToolStripPanel.PerformLayout();
            this.MainPanel.ContentPanel.ResumeLayout(false);
            this.MainPanel.TopToolStripPanel.ResumeLayout(false);
            this.MainPanel.TopToolStripPanel.PerformLayout();
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.MainStatusbar.ResumeLayout(false);
            this.MainStatusbar.PerformLayout();
            this.MainPages.ResumeLayout(false);
            this.OrdersPage.ResumeLayout(false);
            this.TwoShipPage.ResumeLayout(false);
            this.ShipmentsPage.ResumeLayout(false);
            this.ShipmentsPage.PerformLayout();
            this.ShipmentsToolBar.ResumeLayout(false);
            this.ShipmentsToolBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShipmentsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipmentsTableBindingSource)).EndInit();
            this.ProcessedOrdersPage.ResumeLayout(false);
            this.ProcessedOrdersPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryBindingSource)).EndInit();
            this.HistoryToolbar.ResumeLayout(false);
            this.HistoryToolbar.PerformLayout();
            this.TemplatesToolstrip.ResumeLayout(false);
            this.TemplatesToolstrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsMenuServicesFiltered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsMenuServices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsMenuCarriers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer OrdersHSplit;
        private System.Windows.Forms.SplitContainer OrdersVSplitBottom;
        private System.Windows.Forms.DataGridView ProductsGrid;
        private System.Windows.Forms.TableLayoutPanel OrdersPanel;
        private System.Windows.Forms.TableLayoutPanel OrderPackagesPanel;
        private System.Windows.Forms.TableLayoutPanel ProductsPanel;
        private System.Windows.Forms.ToolStripContainer MainPanel;
        private System.Windows.Forms.StatusStrip MainStatusbar;
        private System.Windows.Forms.TabControl MainPages;
        private System.Windows.Forms.TabPage OrdersPage;
        private System.Windows.Forms.TabPage ShipmentsPage;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.BindingSource OrdersTableBindingSource;
        private InteGr8.DataTableAdapters.OrdersTableAdapter ordersTableAdapter;
        private System.Windows.Forms.BindingSource fKOrdersTableOrderPackagesTableBindingSource;
        private InteGr8.DataTableAdapters.OrderPackagesTableAdapter orderPackagesTableAdapter;
        private InteGr8.DataTableAdapters.OrderProductsTableAdapter orderProductsTableAdapter;
        //private InteGr8.DataTableAdapters.ShipmentsTableAdapter shipmentsTableAdapter;
        private System.ComponentModel.BackgroundWorker TaskBackgroundWorker;
        internal Data data;
        private System.Windows.Forms.ToolStrip TemplatesToolstrip;
        private System.Windows.Forms.ToolStripLabel TemplatesLabel;
        private System.Windows.Forms.ToolStripComboBox TemplatesDropdown;
        private System.Windows.Forms.ToolStripSeparator TemplatesSeparator1;
        private System.Windows.Forms.ToolStripButton TemplatesNew;
        private System.Windows.Forms.ToolStripButton TemplatesEdit;
        private System.Windows.Forms.ToolStripButton TemplatesDelete;
        private System.Windows.Forms.ToolStripSeparator TemplatesSeparator2;
        private System.Windows.Forms.ToolStripButton TemplatesImport;
        private System.Windows.Forms.ToolStripButton TemplatesExport;
        private System.Windows.Forms.Label OrdersLabel;
        private System.Windows.Forms.ToolStrip OrdersToolbar;
        private System.Windows.Forms.ToolStripButton OrdersRefreshButton;
        private System.Windows.Forms.ToolStripSeparator OrdersToolbarSeparator1;
        private System.Windows.Forms.ToolStripButton OrdersProcessAllButton;
        private System.Windows.Forms.ToolStripSeparator OrdersToolbarSeparator2;
        private System.Windows.Forms.ToolStripButton OrdersProcessCurrentButton;
        private System.Windows.Forms.ToolStripButton OrdersEditCurrentButton;
        private System.Windows.Forms.Label OrderFiltersLabel;
        private System.ComponentModel.BackgroundWorker FillBackgroundWorker;
        private System.Windows.Forms.ToolStripButton SelectAllOrdersButton;
        private System.Windows.Forms.ToolStripButton UnselectAllOrdersButton;
        private System.Windows.Forms.DataGridView ShipmentsGrid;
        private System.Windows.Forms.ToolStrip ShipmentsToolBar;
        private System.Windows.Forms.ToolStripButton ShipmentsLabelButton;
        private System.Windows.Forms.ToolStripButton ShipmentsCIButton;
        private System.Windows.Forms.ToolStripButton ShipmentsSave;
        private System.Windows.Forms.BindingSource fKOrdersTableRequestsTableBindingSource;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.TableLayoutPanel FilterTableLayoutPanel;
        private System.Windows.Forms.ToolTip ToolTip;
        private System.Windows.Forms.BindingSource filterExpressionBindingSource;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkShipmentSelected;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtShipmentOrderNumber;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.ToolStripButton ShipmentsDelete;
        private System.Windows.Forms.TabPage TwoShipPage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton Options;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton GetEditResults;
        private System.Windows.Forms.TabPage ProcessedOrdersPage;
        private System.Windows.Forms.ToolStrip HistoryToolbar;
        private System.Windows.Forms.ToolStripButton HistoryDeleteButton;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox HistorySearchTextbox;
        private System.Windows.Forms.ToolStripButton HistorySearchButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.DataGridView HistoryGrid;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox OrderSearchTextbox;
        private System.Windows.Forms.ToolStripButton OrderSearchButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductOrderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductDescription;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkProductIsDocument;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductMU;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductUnitWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductUnitValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductCustomsValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductTotalWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProductHarmonizedCode;
        private System.Windows.Forms.BindingSource CarriersBS;
        private System.Windows.Forms.BindingSource ServicesBSFiltered;
        private System.Windows.Forms.BindingSource CarrierPackagingsBS;
        private System.Windows.Forms.BindingSource ServicesBS;
        private System.Windows.Forms.BindingSource CarrierPackagingsBSFiltered;
        private System.Windows.Forms.ToolStripButton cmdRateCheapestService;
        private System.Windows.Forms.ToolStripButton cmdRateFastestService;
        private System.Windows.Forms.ToolStripButton cmdRateAllServices;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.Panel pnlCommoditiesTop;
        private System.Windows.Forms.Button cmdCommodityDelete;
        private System.Windows.Forms.Label ProductsLabel;
        private System.Windows.Forms.BindingSource BillingTypesBS;
        private System.Windows.Forms.BindingSource packagingsBindingSource;
        private System.Windows.Forms.ToolStripButton ShipmentsBOLPrint;
        private System.Windows.Forms.BindingSource fkOrdersTableOrderSkidsTableBindingSource;
        private System.Windows.Forms.BindingSource fkOrdersTableLTLItemsBindingSource;
        private System.Windows.Forms.DataGridView OrdersGrid;
        private System.Windows.Forms.TabControl tabPackageType;
        private System.Windows.Forms.TabPage tabPagePackages;
        private System.Windows.Forms.DataGridView OrderPackagesGrid;
        private System.Windows.Forms.TabPage tabPageSkids;
        private System.Windows.Forms.DataGridView OrderSkidsGrid;
        private System.Windows.Forms.TabPage tabPageItems;
        private System.Windows.Forms.DataGridView LTLItemsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemsCountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemTotalWeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemTotalWeightTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemTotalLengthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemTotalWidthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemTotalHeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemTotalDimsTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn lTLItemFreightClassIDDataGridViewComboBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemQuantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lTLItemUMDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripButton ShipmentsReturnLabel;
        private System.Windows.Forms.Panel panelShipType;
        private System.Windows.Forms.Button button_PackagesAdd;
        private System.Windows.Forms.RadioButton rbShipmentLevel;
        private System.Windows.Forms.RadioButton rbPackageLevel;
        private System.Windows.Forms.Button buttonRemovePackage;
        private System.Windows.Forms.Label lblTotalWeight;
        private System.Windows.Forms.DataGridViewComboBoxColumn skidFreightClassDataGridViewComboBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn useSkidsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidCountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidShipmentLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidSkidLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidsCountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidWeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidWeightTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidInsuranceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidInsuranceCurrencyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidLengthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidWidthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidHeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidDimensionsTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidReference1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidReference2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidPONumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidShipmentIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidInvoiceNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipmentPONumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipmentReferenceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidFreightClassDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidNumberOfPackagesInSkidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skidDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bOLCommentDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripButton btnEditRates;
        private System.Windows.Forms.ToolStripButton btnHistoryLabel;
        private System.Windows.Forms.ToolStripButton btnHistoryCI;
        private System.Windows.Forms.ToolStripButton btnHistoryBOLPrint;
        private System.Windows.Forms.ToolStripButton btnHistoryReturnLabel;
        private System.Windows.Forms.BindingSource filterLeftOperandNameBindingSource;
        private System.Windows.Forms.ComboBox cmbFilterColumns;
        private System.Windows.Forms.Label labelOperator;
        private System.Windows.Forms.Button btnAcceptFilter;
        private System.Windows.Forms.LinkLabel FilterLinkLabel;
        private System.Windows.Forms.Button btnGetWeight;
        private System.Windows.Forms.ToolStripButton btnManualOrders;
        private System.Windows.Forms.Button btnScanProducts;
        private System.Windows.Forms.ToolStripButton btn_unselect_all;
        private System.Windows.Forms.ToolStripButton btn_select_all;
        private System.ComponentModel.BackgroundWorker bg_print;
        private System.Windows.Forms.ToolStripButton btn_selectall;
        private System.Windows.Forms.ToolStripButton btn_unselectall;
        private System.Windows.Forms.ToolStripButton btn_shipments_packslip;
        private System.Windows.Forms.ToolStripButton btn_history_packslip;
        private System.Windows.Forms.ToolStripButton btn_shipments_contentslabel;
        //private ExtendedWebBrowser Browser;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkOrderSelected;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtOrderNumber;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbBillTo;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbOrderCarrier;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbOrderService;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbOrderPackaging;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtOrderShipDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtOrderRecipientCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtOrderSenderCountry;
        public System.Windows.Forms.BindingSource shipmentsTableBindingSource;
        private System.ComponentModel.BackgroundWorker bgworker_save;
        private System.Windows.Forms.BindingSource HistoryBindingSource;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        public System.Windows.Forms.BindingSource fKOrdersTableOrderProductsTableBindingSource;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected_ord;
        private System.Windows.Forms.DataGridViewTextBoxColumn HistoryOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn HistoryTN;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Token;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn LabelURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn CIURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn BOLURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnLabelURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientAddress1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientAddress2;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientZIP;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientState;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn Carrier;
        private System.Windows.Forms.DataGridViewTextBoxColumn Service;
        private System.Windows.Forms.ToolStripButton btn_aggregate;
        private System.Windows.Forms.ToolStripButton btn_close;
        public System.Windows.Forms.TextBox tbValue1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton btn_clear;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ShipmentsSelectedColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Shipmentsorder;
        private System.Windows.Forms.DataGridViewTextBoxColumn Package_Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn Shipmentsstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Shipment_Tracking_Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipmentslabelURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn commercialInvoiceURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipmentsCarrier;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipmentsService;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn delivery;
        private System.Windows.Forms.DataGridViewTextBoxColumn errorCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn errorMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn CarrierCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceCode;
        private System.Windows.Forms.ToolStripLabel labelCarrier;
        private System.Windows.Forms.ToolStripComboBox cmbMenuCarrier;
        private System.Windows.Forms.ToolStripLabel labelService;
        private System.Windows.Forms.ToolStripComboBox cmbMenuService;
        private System.Windows.Forms.ToolStripButton btnApplyCarrierServiceToSelected;
        private System.Windows.Forms.BindingSource bsMenuServicesFiltered;
        private System.Windows.Forms.BindingSource bsMenuServices;
        private System.Windows.Forms.BindingSource bsMenuCarriers;
        private System.Windows.Forms.Panel panelPackageFields;
        private System.Windows.Forms.Button btnSetFieldValue;
        private System.Windows.Forms.RadioButton rbSetFieldAll;
        private System.Windows.Forms.RadioButton rbSetForMissing;
        private System.Windows.Forms.RadioButton rbFieldCurrent;
        private System.Windows.Forms.TextBox txtPackageFieldValue;
        private System.Windows.Forms.Label lblPackageFieldValue;
        private System.Windows.Forms.ComboBox cbPackageField;
        private System.Windows.Forms.Label lblPackageField;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator tsCarrierAndServiceApply;
        private System.Windows.Forms.ToolStripButton tsButtonPackslip;
        private System.Windows.Forms.BindingSource packageDimsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageOrderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageWidth;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageHeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageWeightType;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageDimensionsType;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageInsuranceAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageInsuranceCurrency;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPackageReference;
        private System.Windows.Forms.ComboBox cmbPackageDims;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripButton cmdRateCurrentService;
        private System.Windows.Forms.ToolStripTextBox tbCurrentRate;
        private System.Windows.Forms.ToolStripLabel DimensionalWeightLabel;
        private System.Windows.Forms.ToolStripTextBox tbDimensionalWeight;
        private System.Windows.Forms.CheckBox cbMultiOrder;
        private System.Windows.Forms.Button btnAddMultiPacks;
        private MyDateTimePicker dtpShipDate;
    }
}

