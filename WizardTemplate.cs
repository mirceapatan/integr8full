﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Odbc;
using System.Data;
using System.Data.Common;
using System.Configuration;

namespace InteGr8
{
    public class TableType
    {
        // simulates an enum with user friendly names
        
        # region static members

        public const string Other = "Other";
        public const string Orders = "Orders";           // PK = [Order #], FKs = [Senders(Sender Id), Recipients(Recipient Id)]
        public const string Senders = "Senders";         // PK = [Sender Id], Orders FKs = null
        public const string Recipients = "Recipients";   // PK = [Recipient Id], Orders FKs = null
        public const string Packages = "Packages";       // PK = [Order #, Package #], FKs = [Orders(Order #)]
        public const string Products = "Products";       // PK = [Order #, Product #], FKs = [Orders(Order #)]
        public const string Shipments = "Shipments";     // PK = [Shipment Id], FKs = [Orders(Order #)]
        public const string Skids = "Skids";       // PK = [Order #, Skid #], FKs = [Orders(Order #)]
        public const string Items = "Items";       // PK = [Order #, Item #], FKs = [Orders(Order #)]

        public static readonly TableType OtherType = new TableType(Other);
        public static readonly TableType OrdersType = new TableType(Orders);
        public static readonly TableType SendersType = new TableType(Senders);
        public static readonly TableType RecipientsType = new TableType(Recipients);
        public static readonly TableType PackagesType = new TableType(Packages);
        public static readonly TableType ProductsType = new TableType(Products);
        public static readonly TableType ShipmentsType = new TableType(Shipments);
        public static readonly TableType SkidsType = new TableType(Skids);
        public static readonly TableType ItemsType = new TableType(Items);

        private static readonly string[] All = { Other, Orders, Recipients, Senders, Packages, Products, Shipments, Skids, Items };
        public static IEnumerable<string> Names { get { return All; } }

        public static TableType Parse(string Type)
        {
            if (Type == null) throw new ArgumentException();
            switch (Type)
            {
                case Other: return OtherType;
                case Orders: return OrdersType;
                case Senders: return SendersType;
                case Recipients: return RecipientsType;
                case Packages: return PackagesType;
                case Products: return ProductsType;
                case Shipments: return ShipmentsType;
                case Skids: return SkidsType;
                case Items: return ItemsType;
                default: throw new ArgumentException();
            }
        }

        public static TableType Parse(Data.FieldsTableDataTable.FieldCategory Category)
        {
            switch (Category)
            {
                case Data.FieldsTableDataTable.FieldCategory.Orders: return OrdersType;
                case Data.FieldsTableDataTable.FieldCategory.Senders: return SendersType;
                case Data.FieldsTableDataTable.FieldCategory.Recipients: return RecipientsType;
                case Data.FieldsTableDataTable.FieldCategory.Packages: return PackagesType;
                case Data.FieldsTableDataTable.FieldCategory.Products: return ProductsType;
                case Data.FieldsTableDataTable.FieldCategory.Shipments: return ShipmentsType;
                case Data.FieldsTableDataTable.FieldCategory.Skids: return SkidsType;
                case Data.FieldsTableDataTable.FieldCategory.Items: return ItemsType;
                default: throw new ArgumentException();
            }
        }

        # endregion

        # region instance members

        public readonly string Type;

        // restrict outside instantiations
        private TableType(string Type)
        {
            switch (Type)
            {
                case Other:
                case Orders:
                case Senders:
                case Recipients:
                case Packages:
                case Products:
                case Shipments:
                case Skids:
                case Items:
                    break;
                default: throw new ArgumentException();
            }
            this.Type = Type;
        }

        public override string ToString()
        {
            return Type;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is TableType)) return false;
            TableType Type = obj as TableType;
            return this.Type.Equals(Type.Type);
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode();
        }

        # endregion
    }

    public class Table
    {
        // The Name & Type combination is unique
        // the same table name can be added multiple times with different types
        // for ex the X table can represent the Orders table and the Senders table at the same time
        
        public readonly string Name;
        public readonly TableType Type;
        public readonly List<string> PK;

        public Table(string Name, TableType Type, List<string> PK)
        {
            if (String.IsNullOrEmpty(Name) || Name.Trim().Equals("")) throw new ArgumentException("Incorrect table name.");
            if (Type.ToString() != TableType.Other && (PK == null || PK.Count == 0)) throw new ArgumentException("PK is required.");
            
            this.Name = Name;
            this.Type = Type;
            this.PK = PK;
        }

        public string PK_Name()
        {
            switch (Type.ToString())
            {
                case TableType.Other: return "Order #";
                case TableType.Senders: return "Sender Id";
                case TableType.Recipients: return "Recipient Id";
                case TableType.Packages: return "Order #, Package #";
                case TableType.Products: return "Order #, Product #";
                case TableType.Shipments: return "Order #, Tracking #, Status";
                case TableType.Skids: return "Order #, Skid #";
                case TableType.Items: return "Order #, Item #";
                default: return Name + " PK";
            }
        }

        public override string ToString()
        {
            return Name + " (" + Type + ")";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Table)) return false;
            Table Table = obj as Table;
            return this.Name.Equals(Table.Name) && this.Type.Equals(Table.Type);
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode();
        }
    }

    public class Link
    {
        // The Parent & Child combination is unique
        
        public readonly Table Parent;
        public readonly Table Child;
        public readonly List<string> FK;

        public Link(Table Parent, Table Child, List<string> FK)
        {
            if (Parent == null || Child == null || Parent.Equals(Child) || FK == null || FK.Count == 0 || Parent.PK.Count != FK.Count) throw new ArgumentException("Incorrect Link parameters");
            this.Parent = Parent;
            this.Child = Child;
            this.FK = FK;
        }

        public override string ToString()
        {
            return Parent.ToString() + " -> " + Child.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Link)) return false;
            Link Link = obj as Link;
            return Link.Parent.Equals(Parent) && Link.Child.Equals(Child) || Link.Parent.Equals(Child) && Link.Child.Equals(Parent);
        }

        public override int GetHashCode()
        {
            return Parent.GetHashCode() * Child.GetHashCode();
        }
    }

    /*
    public class IndirectLink
    {
        public readonly Link LeftLink;
        public readonly Link RightLink;

        public IndirectLink(Link LeftLink, Link RightLink)
        {
            if (LeftLink == null || RightLink == null || LeftLink.Child != RightLink.Child) throw new ArgumentException("Incorrect IndirectLink parameters");
            this.LeftLink = LeftLink;
            this.RightLink = RightLink;
        }

        public override string ToString()
        {
            return LeftLink.Parent.ToString() + " -> " + LeftLink.Child.ToString() + " <- " + RightLink.Parent.ToString();
        }
    }
    */

    public class Binding
    {
        public readonly Data.FieldsTableRow Field;  // not null
        public Table Table;         // can be null
        public Link Link = null;    // only used if Table is diff than the field category table
        public string Column;       // can be null
        public object Default;

        public Binding(Data.FieldsTableRow Field, Table Table, Link Link, string Column, object Default)
        {
            if (Field == null || Column != null && Table == null || Link != null && Table == null) throw new ArgumentException();
            this.Field = Field;
            this.Table = Table;
            this.Link = Link;
            this.Column = Column;
            this.Default = Default;

            if (this.Column != null && this.Column.Trim().Equals("")) this.Column = null;
        }

        public override string ToString()
        {
            return Field.Name + " -> " + Table + "." + Column + (Default == null ? "" : "(" + Default.ToString() + ")");
        }
    }

    public class Connection
    {
        // IMPORTANT: the DSN should contain the default database!
        public readonly string DSN;
        public readonly string Login;
        public readonly string Password;
        public readonly bool SupportsDelete;
        public readonly string ConnStr;

        // Quote character used for identifiers
        public readonly char Quote;
        public readonly char StartQuote = '[';
        public readonly char EndQuote = ']';

        // internal cache for catalog
        private readonly List<string> tables = new List<string>();
        private readonly List<string> views = new List<string>();

        public Connection(string DSN, string Login, string Password)
        {
            if (String.IsNullOrEmpty(DSN)) throw new ArgumentException();

            this.DSN = DSN;
            this.Login = Login ?? "";
            this.Password = Password ?? "";

            ConnStr = "DSN=" + DSN + "; " + (Login.Equals("") ? "" : "Uid=" + Login + "; ") + (Password.Equals("") ? "" : "Pwd=" + Password + "; ");
            
            using (OdbcConnection Conn = new OdbcConnection(ConnStr))
            {
                Conn.Open();
                
                // Description from MSDN: http://msdn.microsoft.com/en-us/library/ms254501.aspx
                DataTable table = Conn.GetSchema(DbMetaDataCollectionNames.DataSourceInformation);
                if (table != null && table.Rows.Count == 1)
                {
                    if (!String.IsNullOrEmpty(table.Rows[0][DbMetaDataColumnNames.QuotedIdentifierPattern].ToString()))
                        Quote = System.Text.RegularExpressions.Regex.Unescape(table.Rows[0][DbMetaDataColumnNames.QuotedIdentifierPattern].ToString())[0];
                    else
                        Quote = '`';
                }
                else
                    Quote = '\'';

                foreach (DataRow row in Conn.GetSchema(OdbcMetaDataCollectionNames.Tables).Rows)
                    tables.Add(row["TABLE_NAME"].ToString());

                foreach (DataRow row in Conn.GetSchema(OdbcMetaDataCollectionNames.Views).Rows)
                    views.Add(row["TABLE_NAME"].ToString());
                
                SupportsDelete = !Conn.DataSource.Equals("TEXT");
            }
        }

        public IEnumerable<string> Get_Catalog(bool Tables, bool Views)
        {
            if (!Tables && !Views) return new string[] { };
            if (Tables && Views)
            {
                List<string> catalog = new List<string>();
                catalog.AddRange(tables);
                catalog.AddRange(views);
                return catalog;
            }
            else
            {
                return Tables ? tables : views;
            }
        }

        public IEnumerable<string> Get_Columns(string Table)
        {
            List<string> columns = new List<string>();
            try
            {
                using (OdbcConnection Conn = new OdbcConnection(ConnStr))
                {
                    Conn.Open();
                    if (Quote.Equals('?'))
                    {
                        using (OdbcDataReader dr = new OdbcCommand("Select Top 1 * From " + Table + " Order By 1", Conn).ExecuteReader(CommandBehavior.SingleRow))
                            for (int k = 0; k < dr.FieldCount; k++)
                                columns.Add(dr.GetName(k));
                    }
                    else
                    {
                        using (OdbcDataReader dr = new OdbcCommand("Select Top 1 * From " + Quote + Table + Quote + " Order By 1", Conn).ExecuteReader(CommandBehavior.SingleRow))
                            for (int k = 0; k < dr.FieldCount; k++)
                                columns.Add(dr.GetName(k));
                    }
                }
            }
            catch (Exception )
            {
                // ignore

            }
            return columns.ToArray();
        }
    }

    public enum Filter_Expression_Join_Type { AND, OR, NONE };

    public class Filter_Operator
    {
        # region static members

        public static readonly Filter_Operator Equal = new Filter_Operator("=");
        public static readonly Filter_Operator Not_Equal = new Filter_Operator("<>");
        public static readonly Filter_Operator Less_Than = new Filter_Operator("<");
        public static readonly Filter_Operator Less_Or_Equal_Than = new Filter_Operator("<=");
        public static readonly Filter_Operator Greater_Than = new Filter_Operator(">");
        public static readonly Filter_Operator Greter_Or_Equal_Than = new Filter_Operator(">=");
        public static readonly Filter_Operator Between = new Filter_Operator("BETWEEN"); // this one has three operands
        public static readonly Filter_Operator Like = new Filter_Operator("LIKE");

        public static readonly Filter_Operator[] All = 
        {
            Equal,
            Not_Equal,
            Less_Than,
            Less_Or_Equal_Than,
            Greater_Than,
            Greter_Or_Equal_Than,
            Between,
            Like
        };

        public static Filter_Operator Parse(string Symbol)
        {
            foreach(Filter_Operator op in All)
                if (op.Symbol.Equals(Symbol))
                    return op;
            return null;
        }

        # endregion

        #region instance members

        public readonly string Symbol;

        private Filter_Operator(string Symbol)
        {
            this.Symbol = Symbol;
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Filter_Operator)) return false;
            Filter_Operator op = obj as Filter_Operator;
            return this.Symbol.Equals(op.Symbol);
        }

        public override string ToString()
        {
            return Symbol;
        }

        public override int GetHashCode()
        {
            return Symbol.GetHashCode();
        }

        #endregion
    }

    public class Filter_Expression
    {
        #region private fields

        // [table].[column] for standard expression, or @name for custom expression
        private string left_operand;        
        
        // null for custom expression
        private Filter_Operator op = null;
        
        // null for custom expression
        private string right_operand_1 = null;

        // used for BETWEEN only and is the same kind as Right_Operand_1, constant or not (null for custom expression)
        private string right_operand_2 = null;
        
        // true if the right operand(s) have constant values (only used for standard expression)
        private bool is_constant = false;

        // NONE for custom expression and for the last standard expression in a filter
        private Filter_Expression_Join_Type join_type = Filter_Expression_Join_Type.NONE;

        #endregion

        #region Validators

        private static bool Validate_Left_Operand_Custom(string value)
        {
            if (String.IsNullOrEmpty(value) || value.Trim().Equals(String.Empty)) return false;
            if (!value[0].Equals('@')) return false;
            value = value.Trim();
          //  foreach(char c in value.Substring(1))
           //     if (!Char.IsLetterOrDigit(c)) return false;
            return true;
        }

        private static bool Validate_Left_Operand_Standard(string value)
        {
            if (String.IsNullOrEmpty(value) || value.Trim().Equals(String.Empty)) return false;
            if (!value[0].Equals('[') || !value[value.Length - 1].Equals(']')) return false;
            int start = value.IndexOf(']', 1);
            if (start == -1 || value.Length - start < 3 || !value[start + 1].Equals('.') || !value[start + 2].Equals('[')) return false;
            start += 3;
            int end = value.IndexOf(']', start);
            if (end != value.Length - 1) return false;
            return true;
        }

        private static bool Validate_Right_Operand_1(string value, out bool Is_Constant)
        {
            Is_Constant = false;
            if (value[0].Equals('['))
            {
                if (!value[value.Length - 1].Equals(']')) return false;
                int start = value.IndexOf(']', 1);
                if (start == -1 || value.Length - start < 3 || !value[start + 1].Equals('.') || !value[start + 2].Equals('[')) return false;
                start += 3;
                int end = value.IndexOf(']', start);
                if (end != value.Length - 1) return false;
            }
            else if (value[0].Equals('\''))
            {
                Is_Constant = true;
                if (!value[value.Length - 1].Equals('\'')) return false;
            }
            //else if (Char.IsDigit((Char)value[0]))
            //{
            //    Is_Constant = false;
            //}
            else return false;
            return true;
        }

        private static bool Validate_Right_Operand_2(string value, bool Is_Constant)
        {
            if (value[0].Equals('['))
            {
                if (Is_Constant) return false;
                if (!value[value.Length - 1].Equals(']')) return false;
                int start = value.IndexOf(']', 1);
                if (start == -1 || value.Length - start < 3 || !value[start + 1].Equals('.') || !value[start + 2].Equals('[')) return false;
                start += 3;
                int end = value.IndexOf(']', start);
                if (end != value.Length - 1) return false;
            }
            else if (value[0].Equals('\''))
            {
                if (!Is_Constant) return false;
                if (!value[value.Length - 1].Equals('\'')) return false;
            }
            else return false;
            return true;
        }

        public static string Validate(string Left_Operand, string Operator, string Right_Operand_1, string Right_Operand_2, string Join_Type, out bool Is_Constant)
        {
            Is_Constant = false;
            if (!Validate_Left_Operand_Standard(Left_Operand)) throw new ArgumentException("Left_Operand must be a column in this form: [table].[column].");
            if (String.IsNullOrEmpty(Operator) || Operator.Trim().Equals(String.Empty) || Filter_Operator.Parse(Operator) == null) return "Operator is incorrect.";
            if (!Validate_Right_Operand_1(Right_Operand_1, out Is_Constant)) throw new ArgumentException("Right operand must be a column in the form [table].[column] or a constant in the form 'value'.");
            if (Operator.Equals(Filter_Operator.Between) && !Validate_Right_Operand_2(Right_Operand_2, Is_Constant)) throw new ArgumentException("Second right operand must be present for BETWEEN and must have the same form as the first right operand");
            if (String.IsNullOrEmpty(Join_Type) || Join_Type.Trim().Equals(String.Empty) || Enum.Parse(typeof(Filter_Expression_Join_Type), Join_Type) == null) return "Join Type is incorrect.";
            return null;
        }

        #endregion

        #region Properties

        public bool Is_Custom 
        { 
            get
            {
                return left_operand[0].Equals('@');
            }
        } 

        public string Left_Operand
        {
            get
            {
                return left_operand;
            }
            set
            {
                if (Is_Custom && !Validate_Left_Operand_Custom(value)) throw new ArgumentException("Left_Operand must be an identifier in this form: @name.");
                if (!Is_Custom && !Validate_Left_Operand_Standard(value)) throw new ArgumentException("Left_Operand must be a column in this form: [table].[column].");
                left_operand = value;
            }
        }

        public Filter_Operator Operator
        {
            get
            {
                return op;
            }
            set
            {
                //* leave the operator for custom filters as well(because it cannot be edited from the application)
                //if (Is_Custom) op = null;
                //else
                {
                    if (value == null) throw new ArgumentException("Operator cannot be null.");
                    op = value;
                    if (op.Equals(Filter_Operator.Between))
                    {
                        right_operand_2 = "''";
                    }
                    else right_operand_2 = null;
                }
            }
        }

        public string Right_Operand_1
        {
            get
            {
                return right_operand_1;
            }
            set
            {
                if (Is_Custom) right_operand_1 = null;
                else
                {
                    if (!Validate_Right_Operand_1(value, out is_constant)) throw new ArgumentException("Right operand must be a column in the form [table].[column] or a constant in the form 'value'.");
                    right_operand_1 = value;
                }
            }
        }

        public string Right_Operand_1_No_Quotes
        {
            get
            {
                if (!is_constant) return right_operand_1;
                return right_operand_1.Substring(1, right_operand_1.Length - 2);
            }
            set
            {
                //if (Is_Custom) right_operand_1 = null;
                //else
                //{
                    if (value == null) value = "";
                    if (!Validate_Right_Operand_1('\'' + value + '\'', out is_constant)) throw new ArgumentException("Right operand must be a column in the form [table].[column] or a constant in the form 'value'.");
                    right_operand_1 = '\'' + value + '\'';
                //}
            }
        }

        public string Right_Operand_2
        {
            get
            {
                return right_operand_2;
            }
            set
            {
                if (Is_Custom) right_operand_2 = null;
                else
                {
                    if (op.Equals(Filter_Operator.Between))
                    {
                        if (!Validate_Right_Operand_2(value, is_constant)) throw new ArgumentException("Second right operand must be present for BETWEEN and must have the same form as the first right operand");
                        right_operand_2 = value;
                    }
                    else
                    {
                        right_operand_2 = null;
                    }
                }
            }
        }

        public string Right_Operand_2_No_Quotes
        {
            get
            {
                if (!is_constant) return right_operand_2;
                if (right_operand_2 == null) return null;
                return right_operand_2.Substring(1, right_operand_2.Length - 2);
            }
            set
            {
                if (Is_Custom) right_operand_2 = null;
                else
                {
                    if (op.Equals(Filter_Operator.Between))
                    {
                        if (value == null) value = "";
                        if (!Validate_Right_Operand_2('\'' + value + '\'', is_constant)) throw new ArgumentException("Second right operand must be present for BETWEEN and must have the same form as the first right operand.");
                        right_operand_2 = '\'' + value + '\'';
                    }
                    else
                    {
                        right_operand_2 = null;
                    }
                }
            }
        }

        public Filter_Expression_Join_Type Join_Type
        {
            get
            {
                return join_type;
            }
            set
            {
                if (Is_Custom) join_type = Filter_Expression_Join_Type.NONE;
                else join_type = value;
            }
        }

        public bool Is_Constant
        {
            get
            {
                return is_constant;
            }
        }

        public string Expression_Test
        {
            get
            {
                StringBuilder expression = new StringBuilder();
                expression.Append('(').Append(left_operand).Append(' ').Append(op).Append(' ').Append(right_operand_1);
                if (op.Equals(Filter_Operator.Between)) expression.Append(" AND ").Append(right_operand_2);
                expression.Append(')');
                if (join_type != Filter_Expression_Join_Type.NONE) expression.Append(' ').Append(join_type.ToString()).Append(' ');
                return expression.ToString();
            }
        }

        public string Name
        {
            // for custom expressions returns the identifier without the starting @
            // for standard expressions returns the column name that is being filtered 
            // (the left operand but without the table name and brackets)
            get
            {
                if (Is_Custom) return left_operand.Substring(1);
                int index = left_operand.Substring(1).IndexOf('[') + 1;
                return left_operand.Substring(index, left_operand.Length - index - 2);
            }
        }

        #endregion

        public Filter_Expression(string Left_Operand, Filter_Operator Operator, string Right_Operand_1, string Right_Operand2, Filter_Expression_Join_Type Join_Type)
        {
            if (!Validate_Left_Operand_Standard(Left_Operand)) throw new ArgumentException("Left_Operand must be a column in this form: [table].[column].");
            left_operand = Left_Operand;
            this.Operator = Operator;
            this.Right_Operand_1 = Right_Operand_1;
            this.Right_Operand_2 = Right_Operand_2;
            this.Join_Type = Join_Type;
        }

        public Filter_Expression(string Expression)
        {
            if (String.IsNullOrEmpty(Expression) || Expression.Trim().Equals(String.Empty)) throw new ArgumentException("Expression is null or empty.");

            if (Expression[0].Equals('@'))
            {
                if (!Validate_Left_Operand_Custom(Expression)) throw new ArgumentException("Left_Operand must be a valid identifier in this form: @name.");
                left_operand = Expression;
                op = Filter_Operator.Equal;
                if (right_operand_1 == null || right_operand_1 == "")
                {
                    right_operand_1 = "0";
                }
                return;
            }

            // parse the standard expression into operator, operands and join type
           
            // left operand must be in this form: [table].[column]
            if (!Expression[0].Equals('(')) throw new ArgumentException("Expression must start with '('.");
            int start = -1;
            int end = -1;
           
            if (Template.ReadConn.Quote.ToString() != "[")
            {
                 start = Expression.IndexOf(Template.ReadConn.Quote);
            }
            else
            {
                 start = Expression.IndexOf('[');
            }
            //int start = Expression.IndexOf('`');
            //int start = Expression.IndexOf(Template.ReadConn.Quote);
            if (start == -1) throw new ArgumentException("Left operand must be a column and must start with '" + Template.ReadConn.Quote + "'.");
            if (Template.ReadConn.Quote.ToString() != "[")
            {
                end = Expression.IndexOf(Template.ReadConn.Quote, start + 1);
            }
            else
            {
                end = Expression.IndexOf(']', start);
            }
            //int end = Expression.IndexOf('`', start + 1);
            //int end = Expression.IndexOf(Template.ReadConn.Quote, start + 1);
            if (end == -1 || end >= Expression.Length - 1) throw new ArgumentException("Left operand parsing error 1.");
            if (!Expression[end + 1].Equals('.')) throw new ArgumentException("Left operand parsing error 2.");

            if (Template.ReadConn.Quote.ToString() != "[")
            {
                if (!Expression[end + 2].Equals(Template.ReadConn.Quote)) throw new ArgumentException("Left operand parsing error 3.");
            }
            else
            {
                if (!Expression[end + 2].Equals('[')) throw new ArgumentException("Left operand parsing error 3.");
            }
            // if (!Expression[end + 2].Equals('`')) throw new ArgumentException("Left operand parsing error 3.");
            //if (!Expression[end + 2].Equals(Template.ReadConn.Quote)) throw new ArgumentException("Left operand parsing error 3.");
           
            // end = Expression.IndexOf('`', end + 3);
            if (Template.ReadConn.Quote.ToString() != "[")
            {
                end = Expression.IndexOf(Template.ReadConn.Quote, end + 3);
            }
            else
            {
                end = Expression.IndexOf(']', end + 2);
            }
            if (end == -1 || end >= Expression.Length - 1) throw new ArgumentException("Left operand parsing error 4.");
            left_operand = Expression.Substring(1, end);

            // operator must be start and end with a space
            start = end + 1;
            if (!Expression[start].Equals(' ')) throw new ArgumentException("Operator parsing error 1.");
            if (start >= Expression.Length - 3) throw new ArgumentException("Operator parsing error 2.");
            end = Expression.IndexOf(' ', start + 1);
            if (end >= Expression.Length - 2) throw new ArgumentException("Operator parsing error 3.");
            if (!Expression[end].Equals(' ')) throw new ArgumentException("Operator parsing error 4.");
            op = Filter_Operator.Parse(Expression.Substring(start + 1, end - start - 1));
            if (op == null) throw new ArgumentException("Operator not found.");

            // Right operand must be a column in form of [table].[column] or a constant in form of 'value'
            start = end + 1;


            if (Expression[start].Equals('[') || Expression[start].Equals(Template.ReadConn.Quote))
            {
                // right operand is a column in this form: [table].[column]
                is_constant = false;
                if (Template.ReadConn.Quote.ToString() != "[")
                {
                    end = Expression.IndexOf(Template.ReadConn.Quote, start + 1);
                }
                else
                {
                    end = Expression.IndexOf(']', start);
                }
                if (end == -1 || end >= Expression.Length - 1) throw new ArgumentException("Right operand 1 parsing error 2.");
                if (!Expression[end + 1].Equals('.')) throw new ArgumentException("Right operand 1 parsing error 3.");
                if (Template.ReadConn.Quote.ToString() != "[")
                {
                    if (!Expression[end + 2].Equals(Template.ReadConn.Quote)) throw new ArgumentException("Right operand 1 parsing error 4.");
                }
                else
                {
                    if (!Expression[end + 2].Equals('[')) throw new ArgumentException("Right operand 1 parsing error 4.");
                }
                if (Template.ReadConn.Quote.ToString() != "[")
                {
                    end = Expression.IndexOf(Template.ReadConn.Quote, end + 1);
                }
                else
                {
                    end = Expression.IndexOf(']', end);
                }
                if (end == -1 || end >= Expression.Length - 1) throw new ArgumentException("Right operand 1 parsing error 5.");
                right_operand_1 = Expression.Substring(start, end);
            }
            
            else if (Expression[start].Equals('\''))
            {
                // right operand is a constant in this form: 'value'
                is_constant = true;
                end = Expression.IndexOf('\'', start + 1);
                if (end == -1 || end >= Expression.Length - 1) throw new ArgumentException("Right constant 1 parsing error.");
                right_operand_1 = Expression.Substring(start, end - start + 1);
            }
                // right operand is numeric
            else if (  Char.IsDigit(((Char)Expression[start])))
            {
                is_constant = false;
            }
            else throw new ArgumentException("Right operand 1 parsing error.");
            //start = end + 1;
            // ar trebui start = end + 1
            // if the operator is BETWEEN then the second right operator is expected in the same form as the first right operand
            // and the two right operands are separated by " AND "
            if (op.Equals(Filter_Operator.Between))
            {
                if (!Expression[start].Equals('['))
                {
                    // right operand 2 is a column in this form: [table].[column]
                    if (is_constant) throw new ArgumentException("Right operands must be both columns or constants.");
                    end = Expression.IndexOf(']', start);
                    if (end == -1 || end >= Expression.Length - 1) throw new ArgumentException("Right operand 2 parsing error 2.");
                    if (!Expression[end + 1].Equals('.')) throw new ArgumentException("Right operand 2 parsing error 3.");
                    if (!Expression[end + 2].Equals('[')) throw new ArgumentException("Right operand 2 parsing error 4.");
                    end = Expression.IndexOf(']', end);
                    if (end == -1 || end >= Expression.Length - 1) throw new ArgumentException("Right operand 2 parsing error 5.");
                    right_operand_2 = Expression.Substring(start, end);
                }
                else if (!Expression[start].Equals('\''))
                {
                    // right operand 2 is a constant in this form: 'value'
                    if (!is_constant) throw new ArgumentException("Right operands must be both columns or constants.");
                    end = Expression.IndexOf('\'', start + 1);
                    if (end == -1 || end >= Expression.Length - 1) throw new ArgumentException("Right constant 2 parsing error.");
                    right_operand_2 = Expression.Substring(start, end);
                }
                else throw new ArgumentException("Right operand 2 parsing error.");
            }

            // join type 
            start = end + 2;
            //start is not at the right position for manual filter

            if (Expression.Length > start && !Expression[start].Equals(' ')) throw new ArgumentException("Join type parsing error.");
            start++;
            if (Expression.Length - start == 4 && Expression.Substring(start, 3).Equals("AND"))
            {
                join_type = Filter_Expression_Join_Type.AND;
            }
            else if (Expression.Length - start == 3 && Expression.Substring(start, 2).Equals("OR"))
            {
                join_type = Filter_Expression_Join_Type.OR;
            }
            else
            {
                join_type = Filter_Expression_Join_Type.NONE;
            }
        }

        public override string ToString()
        {
            return Expression_Test;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Filter_Expression)) return false;
            return this.Expression_Test.Equals((obj as Filter_Expression).Expression_Test);
        }

        public override int GetHashCode()
        {
            return left_operand.GetHashCode();
        }
    }

    public static class Select_Filter
    {
        private static string custom_filter = null;

        private static readonly List<Filter_Expression> expressions = new List<Filter_Expression>();

        public static bool Is_Empty { get { return custom_filter != null && String.IsNullOrEmpty(custom_filter) || expressions.Count == 0; } } 

        public static bool Is_Custom 
        { 
            get 
            { 
                return custom_filter != null; 
            }
            set
            {
                custom_filter = value ? "" : null;
            }
        }

        public static string Filter_No_Status
        {
            get
            {
                StringBuilder str = new StringBuilder();
                if (Is_Custom)
                {
                    // there are no default values, use empty strings
                    str.Append(custom_filter);
                    string parameter;
                    int start = -1, end;
                    char c;
                    int k = 0;
                    do
                    {
                        start = custom_filter.IndexOf('@', start + 1);
                        if (start != -1)
                        {
                            end = start + 1;
                            do
                            {
                                c = custom_filter[end++];
                            }
                            while (end < custom_filter.Length && c != ' ' && c != '\n' && c != '\r' && c != '\t');
                            parameter = custom_filter.Substring(start, end - start);
                            string RightOperandValue = (expressions[k].Right_Operand_1_No_Quotes == null || expressions[k].Right_Operand_1_No_Quotes == "" ) ? "0" : expressions[k].Right_Operand_1_No_Quotes;
                            k++;
                            str.Replace(parameter, RightOperandValue);
                            //str.Replace(parameter, "''", start, 1);
                        }
                    }
                    while (start != -1);
                }
                else
                {
                    for (int k = 0; k < expressions.Count; k++)
                        str.Append(expressions[k].Expression_Test).Append('\n');
                }

                return str.ToString();
            }
            set
            {
                if (Is_Custom)
                {
                    custom_filter = value ?? "";
                    string parameter;
                    int start = -1, end;
                    //char c;
                   // do
                   // {
                     end = custom_filter.IndexOf('=', start + 1);

                     if (end != -1)
                     {
                         start = custom_filter.IndexOf('(');
                         parameter = custom_filter.Substring(start + 1, end - 2);
                         parameter = "@" + parameter;
                         expressions.Add(new Filter_Expression(parameter));
                         //end = start + 1;
                         //do
                         //{
                         //    c = custom_filter[end++];
                         //}
                         //while (end < custom_filter.Length && c != ' ' && c != '\n' && c != '\r' && c != '\t');
                         //parameter = custom_filter.Substring(start, end - start);
                         //expressions.Add(new Filter_Expression(parameter));
                     }
                    //}
                    //while (start != -1);
                    // add here the value inserted too...


                }
                else
                {
                    expressions.Clear();
                    value = value.ToString();
                    if (!value.Trim().Equals(String.Empty))
                        foreach (string expression in value.Split('\n'))
                            if (!expression.Trim().Equals(String.Empty)) expressions.Add(new Filter_Expression(expression));
                    if (expressions.Count > 0) expressions[expressions.Count - 1].Join_Type = Filter_Expression_Join_Type.NONE;
                }
            }
        }

        public static string Filter_With_Status
        {
            get
            {
                if (Orders_Status_Filter.Equals(String.Empty)) return Filter_No_Status;
                if (Filter_No_Status.Equals(String.Empty)) return Orders_Status_Filter;
                StringBuilder str = new StringBuilder();
                str.Append('(').Append(Filter_No_Status).Append(") AND ").Append(Orders_Status_Filter);
                return str.ToString();
            }
        }

        public static string Orders_Status_Filter
        {
            get
            {
                if (!Template.SaveShipments || !InteGr8.Properties.Settings.Default.UseAutomaticOrdersFiltering) return "";
                Link link = Template.FindLink(Template.Orders, Template.Shipments);
                if (link == null) link = Template.FindLink(Template.Shipments, Template.Orders);
                if (link == null) throw new Exception("Internal error.");
                if (Template.Orders.PK.Count != 1) throw new Exception("Automatic filtering is not supported with multiple columns key on Orders table.");
                StringBuilder str = new StringBuilder();
                str.Append(Template.ReadConn.Quote + Template.Orders.Name + Template.ReadConn.Quote)
                    .Append('.')
                    .Append(Template.ReadConn.Quote + Template.Orders.PK[0] + Template.ReadConn.Quote)
                    .Append(" NOT IN (SELECT ")
                    .Append(Template.ReadConn.Quote + link.FK[0] + Template.ReadConn.Quote)
                    .Append(" FROM ")
                    .Append(Template.ReadConn.Quote + Template.Shipments.Name + Template.ReadConn.Quote)
                    .Append(')');
                return str.ToString();
            }
        }

        public static IEnumerable<Filter_Expression> Expressions 
        { 
            get 
            {
                return expressions; 
            } 
        }

        public static void Add(Filter_Expression Expression)
        {
            if (Is_Custom) throw new InvalidOperationException();
            if (expressions.Count > 0) expressions[expressions.Count - 1].Join_Type = Expression.Join_Type;
            expressions.Add(Expression);
            expressions[expressions.Count - 1].Join_Type = Filter_Expression_Join_Type.NONE;
        }

        public static void Remove(Filter_Expression Expression)
        {
            if (Is_Custom) throw new InvalidOperationException();
            expressions.Remove(Expression);
            if (expressions.Count > 0) expressions[expressions.Count - 1].Join_Type = Filter_Expression_Join_Type.NONE;
        }
    }

    public static class Orders_Select_Statement
    {
        private static string custom_sql = null;

        public static bool Is_Empty { get { return Select.Equals(String.Empty); } }

        public static bool Is_Custom 
        { 
            get 
            { 
                return custom_sql != null; 
            }
        }
        
        public static string Select
        {
            get
            {
                if (Is_Custom)
                {
                    // replace the parameters with empty string constants
                    return custom_sql;
                }
                return "SELECT\n" + Columns + "FROM\n" + Tables; // +(Select_Filter.Is_Empty ? "" : "\nWHERE\n" + Select_Filter.Filter);
            }
            set
            {
                custom_sql = value;
            }
        }

        public static string Columns
        {
            get
            {
                StringBuilder columns = new StringBuilder(1000);
                Table table = Template.Orders;

                foreach (Binding Binding in Template.Bindings)
                {
                    // skip the other field types
                    TableType field_type = TableType.Parse(Binding.Field.FieldCategory);
                    if (field_type != TableType.OrdersType && field_type != TableType.SendersType && field_type != TableType.RecipientsType) 
                        continue;

                    columns.Append('\t');
                    string default_value = Binding.Default == null || Binding.Default.ToString().Trim().Equals("") ? null : "'" + Binding.Default.ToString().Replace("'", "") + "'";
                    if (Binding.Column == null || Binding.Table == null) 
                        columns.Append(default_value);
                    else
                    {
                        if (default_value != null) columns.Append("IsNull(");
                        columns.Append(Template.ReadConn.Quote + Binding.Table.Name + Template.ReadConn.Quote + '.' + Template.ReadConn.Quote + Binding.Column + Template.ReadConn.Quote);
                        if (default_value != null) columns.Append(", " + default_value + ")");
                    }
                    columns.Append(" AS " + Template.ReadConn.Quote + Binding.Field.Name + Template.ReadConn.Quote + ",\n");
                }
                if (columns.Length == 0) return "";
                columns.Remove(columns.Length - 2, 1);

                return columns.ToString();
            }
        }

        public static string Tables
        {
            get
            {
                Table table = Template.Orders;
                StringBuilder tables = new StringBuilder(1000);
                tables.Append(Template.ReadConn.Quote + Template.Orders.Name + Template.ReadConn.Quote);
                List<string> joins = new List<string>(100);
                foreach (Binding Binding in Template.Bindings)
                {
                    if (Binding.Table == null || Binding.Table == Template.Orders || Binding.Link == null) continue; // no table to join
                    if (joins.Contains(Binding.Table.Name)) continue;  // table already joined

                    // skip the other field types
                    TableType field_type = TableType.Parse(Binding.Field.FieldCategory);
                    if (field_type != TableType.OrdersType && field_type != TableType.SendersType && field_type != TableType.RecipientsType) continue;

                    if (Binding.Link.Parent.Type == TableType.OtherType || Binding.Link.Child.Type == TableType.OtherType)
                        throw new NotSupportedException("Sorry, many-to-many relationships are not supported yet.");

                    bool OrdersIsParent = Binding.Link.Parent == Template.Orders;
                    tables.Append("\n\tLEFT JOIN " + Template.ReadConn.Quote + (OrdersIsParent ? Binding.Link.Child : Binding.Link.Parent).Name + Template.ReadConn.Quote + " ON ");
                    for (int k = 0; k < Binding.Link.Parent.PK.Count; k++)
                        tables.Append(Template.ReadConn.Quote + Binding.Link.Parent.Name + Template.ReadConn.Quote + '.' + Template.ReadConn.Quote + Binding.Link.Parent.PK[k] + Template.ReadConn.Quote + " = " + Template.ReadConn.Quote + Binding.Link.Child.Name + Template.ReadConn.Quote + '.' + Template.ReadConn.Quote + Binding.Link.FK[k] + Template.ReadConn.Quote + " AND ");
                    tables.Remove(tables.Length - 5, 5); // remove the last AND from the join conditions

                    joins.Add(Binding.Table.Name);
                }

                return tables.ToString();
            }
        }
    }

    public static class Packages_Select_Statement
    {
        private static string custom_sql = null;

        public static bool Is_Empty { get { return Select.Equals(String.Empty); } }

        public static bool Is_Custom
        {
            get
            {
                return custom_sql != null;
            }
        }

        public static string Select
        {
            get
            {
                if (Is_Custom)
                {
                    // replace the parameters with empty string constants
                    return custom_sql;
                }
                return "SELECT\n" + Columns + "FROM\n" + Tables; // +"\nWHERE\n" + Orders_Filter;
            }
            set
            {
                custom_sql = value;
            }
        }

        public static string Columns
        {
            get
            {
                StringBuilder columns = new StringBuilder(1000);
                Table table = Template.Orders;
                foreach (Binding Binding in Template.Bindings)
                {
                    // skip the other field types
                    TableType field_type = TableType.Parse(Binding.Field.FieldCategory);
                    if (field_type != TableType.PackagesType) continue;

                    string default_value = Binding.Default == null || Binding.Default.ToString().Trim().Equals("") ? null : "'" + Binding.Default.ToString().Replace("'", "") + "'";
                    if (Binding.Column == null || Binding.Table == null) columns.Append(default_value);
                    else
                    {
                        if (default_value != null) columns.Append("IsNull(");
                        columns.Append(Template.ReadConn.Quote + Binding.Table.Name + Template.ReadConn.Quote + '.' + Template.ReadConn.Quote + Binding.Column + Template.ReadConn.Quote);
                        if (default_value != null) columns.Append(", " + default_value + ")");
                    }

                    columns.Append(" AS " + Template.ReadConn.Quote + Binding.Field.Name + Template.ReadConn.Quote + ",\n");
                }
                if (columns.Length == 0) return null;
                columns.Remove(columns.Length - 2, 1);
                return columns.ToString();
            }
        }

        public static string Tables
        {
            get
            {
                Table table = Template.Orders;
                StringBuilder tables = new StringBuilder(1000);
                tables.Append(Template.ReadConn.Quote + Template.Orders.Name + Template.ReadConn.Quote);
                List<string> joins = new List<string>(100);
                foreach (Binding Binding in Template.Bindings)
                {
                    if (Binding.Table == null || Binding.Table == Template.Orders || Binding.Link == null) continue; // no table to join
                    if (joins.Contains(Binding.Table.Name)) continue;  // table already joined

                    // skip the other field types
                    TableType field_type = TableType.Parse(Binding.Field.FieldCategory);
                    if (field_type != TableType.PackagesType) continue;

                    if (Binding.Link.Parent.Type == TableType.OtherType || Binding.Link.Child.Type == TableType.OtherType)
                        throw new NotSupportedException("Sorry, many-to-many relationships are not supported yet.");

                    bool OrdersIsParent = Binding.Link.Parent == Template.Orders;
                    tables.Append("\nLEFT JOIN " + Template.ReadConn.Quote + (OrdersIsParent ? Binding.Link.Child : Binding.Link.Parent).Name + Template.ReadConn.Quote + " ON ");
                    for (int k = 0; k < Binding.Link.Parent.PK.Count; k++)
                        tables.Append(Template.ReadConn.Quote + Binding.Link.Parent.Name + Template.ReadConn.Quote + '.' + Template.ReadConn.Quote + Binding.Link.Parent.PK[k] + Template.ReadConn.Quote + " = " + Template.ReadConn.Quote + Binding.Link.Child.Name + Template.ReadConn.Quote + '.' + Template.ReadConn.Quote + Binding.Link.FK[k] + Template.ReadConn.Quote + " AND ");
                    tables.Remove(tables.Length - 5, 5); // remove the last AND from the join conditions

                    joins.Add(Binding.Table.Name);
                }
                return tables.ToString();
            }
        }
    }

    public static class Products_Select_Statement
    {
        private static string custom_sql = null;

        public static bool Is_Empty { get { return Select.Equals(String.Empty); } }

        public static bool Is_Custom
        {
            get
            {
                return custom_sql != null;
            }
        }

        public static string Select
        {
            get
            {
                if (Is_Custom)
                {
                    // replace the parameters with empty string constants
                    return custom_sql;
                }
                return "SELECT\n" + Columns + "FROM\n" + Tables; // +"\nWHERE\n" + Orders_Filter;
            }
            set
            {
                custom_sql = value;
            }
        }

        public static string Columns
        {
            get
            {
                StringBuilder columns = new StringBuilder(1000);
                Table table = Template.Orders;
                foreach (Binding Binding in Template.Bindings)
                {
                    // skip the other field types
                    TableType field_type = TableType.Parse(Binding.Field.FieldCategory);
                    if (field_type != TableType.ProductsType) continue;

                    string default_value = Binding.Default == null || Binding.Default.ToString().Trim().Equals("") ? null : "'" + Binding.Default.ToString().Replace("'", "") + "'";
                    if (Binding.Column == null || Binding.Table == null) columns.Append(default_value);
                    else
                    {
                        if (default_value != null) columns.Append("IsNull(");
                        columns.Append(Template.ReadConn.Quote + Binding.Table.Name + Template.ReadConn.Quote + '.' + Template.ReadConn.Quote + Binding.Column + Template.ReadConn.Quote);
                        if (default_value != null) columns.Append(", " + default_value + ")");
                    }
                    columns.Append(" AS " + Template.ReadConn.Quote + Binding.Field.Name + Template.ReadConn.Quote + ",\n");
                }
                if (columns.Length == 0) return null;
                columns.Remove(columns.Length - 2, 1);
                return columns.ToString();
            }
        }

        public static string Tables
        {
            get
            {
                Table table = Template.Orders;
                StringBuilder tables = new StringBuilder(1000);
                tables.Append(Template.ReadConn.Quote + Template.Orders.Name + Template.ReadConn.Quote);
                List<string> joins = new List<string>(100);
                foreach (Binding Binding in Template.Bindings)
                {
                    if (Binding.Table == null || Binding.Table == Template.Orders || Binding.Link == null) continue; // no table to join
                    if (joins.Contains(Binding.Table.Name)) continue;  // table already joined

                    // skip the other field types
                    TableType field_type = TableType.Parse(Binding.Field.FieldCategory);
                    if (field_type != TableType.ProductsType) continue;

                    if (Binding.Link.Parent.Type == TableType.OtherType || Binding.Link.Child.Type == TableType.OtherType)
                        throw new NotSupportedException("Sorry, many-to-many relationships are not supported yet.");

                    bool OrdersIsParent = Binding.Link.Parent == Template.Orders;
                    tables.Append("\nLEFT JOIN " + Template.ReadConn.Quote + (OrdersIsParent ? Binding.Link.Child : Binding.Link.Parent).Name + Template.ReadConn.Quote + " ON ");
                    for (int k = 0; k < Binding.Link.Parent.PK.Count; k++)
                        tables.Append(Template.ReadConn.Quote + Binding.Link.Parent.Name + Template.ReadConn.Quote + '.' + Template.ReadConn.Quote + Binding.Link.Parent.PK[k] + Template.ReadConn.Quote + " = " + Template.ReadConn.Quote + Binding.Link.Child.Name + Template.ReadConn.Quote + '.' + Template.ReadConn.Quote + Binding.Link.FK[k] + Template.ReadConn.Quote + " AND ");
                    tables.Remove(tables.Length - 5, 5); // remove the last AND from the join conditions

                    joins.Add(Binding.Table.Name);
                }
                return tables.ToString();
            }
        }
    }

    public static class Shipments_Insert_Statement
    {
        private static string custom_sql = null;
        private static string update_sql = null;
        private static string delete_sql = null;

        public static bool Is_Empty { get { return Insert.Equals(String.Empty); } }

        public static bool Is_Custom
        {
            get
            {
                return custom_sql != null;
            }
        }

        public static string Insert
        {
            get
            {
                if (Is_Custom) return custom_sql;
                return "INSERT INTO " + Template.WriteConn.Quote + Template.Shipments.Name + Template.WriteConn.Quote + "\n(\n" + Columns + "\n)\nVALUES\n(\n" + Values + "\n)";
            }
            set
            {
                custom_sql = value;
            }
        }

        public static string Update
        {
            get
            {
                return update_sql;
            }
            set
            {
                update_sql = value;
            }
        }
        public static string Delete
        {
            get
            {
                return delete_sql;
            }
            set
            {
                delete_sql = value;
            }
        }
        public static string Columns
        {
            get
            {
                StringBuilder columns = new StringBuilder(1000);
                foreach (Binding Binding in Template.Bindings)
                    if (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments)
                        columns.Append("\t" + Template.WriteConn.Quote + Binding.Column + Template.WriteConn.Quote + ",\n");
                if (columns.Length == 0) return "";
                
                columns.Remove(columns.Length - 2, 2);
                
                return columns.ToString();
            }
        }

        public static string Values
        {
            get
            {
                StringBuilder values = new StringBuilder(1000);
                foreach (Binding Binding in Template.Bindings)
                    if (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments)
                        values.Append("\t@" + Binding.Field.Name.Replace(" ", "") + ",\n");
                if (values.Length == 0) return "";
                values.Remove(values.Length - 2, 2);
                return values.ToString();
            }
        }
    }


    public static class Shipments_Delete_Statement
    {
        private static string custom_sql = null;

        public static bool Is_Empty { get { return Delete.Equals(String.Empty); } }

        public static bool Is_Custom
        {
            get
            {
                return custom_sql != null;
            }
        }

        public static string Delete
        {
            get
            {
                if (Is_Custom) return custom_sql;
                string[] cols = Columns.Split('\t');
                string[] val = Values.Split('\t');
                StringBuilder columnsValues = new StringBuilder(1000);
                for (int i = 0; i < cols.Length; i++)
                     columnsValues.Append( cols[i] + " = " + val[i]);
                return "DELETE FROM " + Template.WriteConn.Quote + Template.Shipments.Name + Template.WriteConn.Quote + "\n(\n" + "where " + columnsValues;
            }
            set
            {
                custom_sql = value;
            }
        }

        public static string Columns
        {
            get
            {
                StringBuilder columns = new StringBuilder(1000);
                foreach (Binding Binding in Template.Bindings)
                    if (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments)
                        columns.Append("\t" + Template.WriteConn.Quote + Binding.Column + Template.WriteConn.Quote );
                if (columns.Length == 0) return "";
                columns.Remove(columns.Length - 2, 2);
                return columns.ToString();
            }
        }

        public static string Values
        {
            get
            {
                StringBuilder values = new StringBuilder(1000);
                foreach (Binding Binding in Template.Bindings)
                    if (Binding.Field.FieldCategory == InteGr8.Data.FieldsTableDataTable.FieldCategory.Shipments)
                        values.Append("\t" + "'" + Binding.Field.Name.Replace(" ", "") + "'" + ",\n");
                if (values.Length == 0) return "";
                values.Remove(values.Length - 2, 2);
                return values.ToString();
            }
        }
    }



    public static class Template
    {
        // used by the wizard windows
        
        public static string Name;
        public static string Description;

        public static Connection ReadConn;

        public static Connection BackupConn;

        private static Connection writeConn = null;
        public static Connection WriteConn
        {
            get
            {
                if (writeConn != null)
                    return writeConn;
                return ReadConn;
            }
            set
            {
                writeConn = value;
            }
        }

        // shipping options
        public static bool International = true;
        public static bool MPS = true;
        public static bool MPSPackageLevel = true;
        public static bool SaveShipments = true;
        public static bool SaveShipmentPackages = true;
        public static bool AutoSave_AtShipping = true;
        public static bool Aggregate = true;
        public static bool Check_Save = false;
        public static string Save_Select = "";
        public static bool Backup_Orders = true;
        public static string Ship_Link = ConfigurationManager.AppSettings["2ShipURL"];//"http://ship.2ship.com";
        public static string Backup_Schedule = "";
        public static string Backup_Month = "";
        public static bool Backup_at_Scheadule = false;
        public static string Check_Delete = "";
        //settings proprieties
        public static bool UseAutoprint = false;
        public static string AutoprintLabelsFolder = "";
        public static string AutoprintCIFolder = "";
        public static string AutoprintLabelsPrinter = "";
        public static string AutoprintCIPrinter = "";
        public static bool PickUpMonday = true;
        public static bool PickUpTuesday = true;
        public static bool PickUpWednesday = true;
        public static bool PickUpThursday = true;
        public static bool PickUpFriday = true;
        public static bool PickUpSaturday = true;
        public static bool PickUpSunday = true;
        public static string AutoprintBOLFolder = "";
        public static string AutoprintReturnLabelFolder = "";
        public static string AutoprintPackSlipFolder = "";
        public static string AutoprintContentsLabelFolder = "";
        public static string AutoprintBOLPrinter = "";
        public static string AutoprintReturnLabelPrinter = "";
        public static string AutoprintPackSlipPrinter = "";
        public static string AutoprintContentsLabelPrinter = "";


        // predefined special tables
        public static Table Orders { get { return FindTable(TableType.OrdersType); } }
        public static Table Senders { get { return FindTable(TableType.SendersType); } }
        public static Table Recipients { get { return FindTable(TableType.RecipientsType); } }
        public static Table Packages { get { return FindTable(TableType.PackagesType); } }
        public static Table Products { get { return FindTable(TableType.ProductsType); } }
        public static Table Shipments { get { return FindTable(TableType.ShipmentsType); } }
        
        public static readonly List<Table> Tables = new List<Table>();
        
        public static readonly List<Link> Links = new List<Link>();
        // public static readonly List<IndirectLink> IndirectLinks = new List<IndirectLink>();
        
        public static readonly List<Binding> Bindings = new List<Binding>();
        
        public static readonly Dictionary<string, string[]> Required_Fields = new Dictionary<string, string[]>();
        
        static Template()
        {
            // setup required fields
            Required_Fields.Add(TableType.Orders, new string[] { "Order #", "Carrier", "Service", "Packaging" });
            Required_Fields.Add(TableType.Senders, new string[] { "Sender Country", "Sender ZIP / Postal Code" });
            Required_Fields.Add(TableType.Recipients, new string[] { "Recipient Country", "Recipient ZIP / Postal Code" });
            Required_Fields.Add(TableType.Packages, new string[] { "Order #", "Package #", "Packages", "Total Weight", "Package Weight", "Package Weight Type" });
            Required_Fields.Add(TableType.Products, new string[] { "Order #", "Product #", "Product Description", "Product Manufacture Country", "Product Unit Weight" });
            Required_Fields.Add(TableType.Shipments, new string[] { "Order #", "Shipment Tracking Number" });
        }

        // factory methods
        public static Table CreateTable(string Name, TableType Type, List<string> PK)
        {
            Table Table = FindTable(Name, Type);
            if (Table != null)
            {
                // table already exists, update the PK
                Table.PK.Clear();
                Table.PK.AddRange(PK);
                return Table; 
            }

            List<Table> duplicates = FindTables(Name);
            if (duplicates.Count > 0)
                foreach(Table table in duplicates)
                {
                    if (table.Type.ToString() == TableType.Other || Type.ToString() == TableType.Other && table.Type.ToString() != TableType.Other) throw new ArgumentException("A table can not be a special table and a custom table at the same time.");
                    if (table.PK.Count != PK.Count) throw new ArgumentException("The table was already added with a different PK.");
                    foreach (string Column in table.PK)
                        if (PK.IndexOf(Column) == -1) throw new ArgumentException("The table was already added with a different PK.");
                }
            return new Table(Name, Type, PK);
        }

        public static void AddTable(Table Table)
        {
            if (Table.Type == TableType.OtherType)
            {
                Table existing_table = FindTable(Table.Name, Table.Type);
                if (existing_table == Table) return;
                if (existing_table != null)
                {
                    Table.PK.Clear();
                    Table.PK.AddRange(Table.PK);
                }
                else Tables.Add(Table);
            }
            else
            {
                Table existing_table = FindTable(Table.Type);
                if (existing_table == Table) return;
                if (existing_table != null)
                {
                    // remove all references to the old table in the links
                    List<Link> link_references = new List<Link>();
                    foreach (Link link in Links)
                        if (link.Parent == existing_table || link.Child == existing_table)
                            link_references.Add(link);
                    foreach(Link link in link_references)
                        Links.Remove(link);

                    // remove all references to the old table in the bindings
                    List<Binding> binding_references = new List<Binding>();
                    foreach (Binding binding in Bindings)
                        if (binding.Table == existing_table)
                            binding_references.Add(binding);
                    foreach (Binding binding in binding_references)
                        Bindings.Remove(binding);

                    // remove the table
                    Tables.Remove(existing_table);
                }
                Tables.Add(Table);
            }
        }

        public static Link CreateLink(Table Parent, Table Child, List<string> FK)
        {
            Link Link = FindLink(Parent, Child);
            if (Link != null) return Link; // link already added
            return new Link(Parent, Child, FK);
        }
        
        public static void AddLink(Link Link)
        {
            if (FindTable(Link.Parent.Name, Link.Parent.Type) == null) AddTable(Link.Parent);
            if (FindTable(Link.Child.Name, Link.Child.Type) == null) AddTable(Link.Child);
            if (FindLink(Link.Parent, Link.Child) != null) return; // link already added
            Links.Add(Link);
        }

        public static Binding CreateBinding(TableType Type, string Field, string TableName, string Column, object Default)
        {
            if (String.IsNullOrEmpty(Field)) 
                throw new ArgumentException();
            
            Table table = null;
            Link Link = null;

            if (TableName != null)
            {
                // find the table
                table = FindTable(TableName, Type);
                if (table == null) 
                    throw new ArgumentException("Table not found");
            }

            Data.FieldsTableRow FieldRow;
            if (table == null) 
                FieldRow = InteGr8_Main.data.FieldsTable.FindByName(Field);
            else
            {
                FieldRow = InteGr8_Main.data.FieldsTable.FindByCategoryName(table.Type.Type, Field);
                if (FieldRow == null) 
                    FieldRow = InteGr8_Main.data.FieldsTable.FindByName(Field);
            }
            if (FieldRow == null) 
                throw new ArgumentException("Field '" + Field + "' not found.");

            if (table != null && !Orders.Name.Equals(table.Name) && table.Type != TableType.OrdersType)
            {
                // find the link
                TableType FieldType = TableType.Parse(FieldRow.FieldCategory);
                if (table.Type == TableType.OtherType)
                {
                    Table LinkTable = null;
                    switch (FieldType.ToString())
                    {
                        case TableType.Orders: LinkTable = Orders; break;
                        case TableType.Senders: LinkTable = Senders; break;
                        case TableType.Recipients: LinkTable = Recipients; break;
                        case TableType.Packages: LinkTable = Packages; break;
                        case TableType.Products: LinkTable = Products; break;
                        case TableType.Shipments: LinkTable = Shipments; break;
                        default: throw new Exception();
                    }
                    Link = FindLink(table, LinkTable);
                    if (Link == null) 
                        Link = FindLink(LinkTable, table);
                    if (Link == null) 
                        throw new ArgumentException("Table link is missing or invalid.");
                }
                else
                {
                    Link = FindLink(table, Orders);
                    if (Link == null) 
                        Link = FindLink(Orders, table);
                    if (Link == null) 
                        throw new ArgumentException("Table link is missing or invalid.");
                }
            }

            return new Binding(FieldRow, table, Link, Column, Default);
        }

        public static void AddBinding(Binding Binding)
        {
            if (Binding == null) throw new ArgumentException();
            if (FindBinding(Binding.Field.Category, Binding.Field.Name) == null) Bindings.Add(Binding);
        }

        public static List<Table> FindTables(string Name)
        {
            if (String.IsNullOrEmpty(Name)) throw new ArgumentException();
            List<Table> list = new List<Table>();
            foreach (Table Table in Tables)
                if (Table.Name.Equals(Name)) list.Add(Table);
            return list;
        }

        public static Table FindTable(TableType Type)
        {
            if (Type.ToString() == TableType.Other) throw new ArgumentException();
            foreach (Table Table in Tables)
                if (Table.Type == Type) return Table;
            return null;
        }
        
        public static Table FindTable(string Name, TableType Type)
        {
            if (String.IsNullOrEmpty(Name)) throw new ArgumentException();
            foreach (Table Table in Tables)
                if (Table.Name.Equals(Name) && Table.Type == Type) return Table;
            return null;
        }

        public static Link FindLink(Table Parent, Table Child)
        {
            if (Parent == null || Child == null) return null;
            foreach (Link Link in Links)
                if (Link.Parent.Equals(Parent) && Link.Child.Equals(Child)) return Link;
            return null;
        }

        public static Binding FindBinding(string Category, string Name)
        {
            foreach (Binding binding in Bindings)
                if (binding.Field != null && binding.Field.Category.Equals(Category) && binding.Field.Name.Equals(Name)) return binding;
            return null;
        }

        public static void Validate()
        {
            // every special table must be linked to the Orders table, and all other tables must be linked to a special table
            bool found;
            foreach (Table Table in Tables)
            {
                found = false;
                switch (Table.Type.ToString())
                {
                    case TableType.Orders:
                        break;
                    case TableType.Senders:
                    case TableType.Recipients:
                    case TableType.Packages:
                        foreach (Link Link in Links)
                            if (Link.Parent == Orders && Link.Child == Table || Link.Parent == Table && Link.Child == Orders)
                            {
                                found = true;
                                break;
                            }
                        if (!found) throw new ArgumentException(Table.ToString() + " is not linked to the Orders table.");
                        break;
                    case TableType.Other:
                        foreach (Link Link in Links)
                            if (Link.Parent == Table || Link.Child == Table)
                            {
                                found = true;
                                break;
                            }
                        if (!found) throw new ArgumentException(Table.ToString() + " is not linked to a special table.");
                        break;
                }
            }

            // check required fields
            Dictionary<string, string> all_fields = new Dictionary<string, string>();
            Dictionary<string, string> required_fields = new Dictionary<string, string>();
            
            required_fields.Add("Orders", "Order #");
            
            required_fields.Add("Senders", "Sender Country");
            required_fields.Add("Senders", "Sender ZIP / Postal Code");
            
            required_fields.Add("Recipients", "Recipient Country");
            required_fields.Add("Recipients", "Recipient ZIP / Postal Code");

            required_fields.Add("Packages", "Order #");
            required_fields.Add("Packages", "Package #");
            required_fields.Add("Packages", "Package Weight");
            required_fields.Add("Packages", "Package Weight Type");

            required_fields.Add("Products", "Order #");
            required_fields.Add("Products", "Product #");
            required_fields.Add("Products", "Description");
            required_fields.Add("Products", "Country Manufacture");
            required_fields.Add("Products", "Weight");
            required_fields.Add("Products", "Weight Type");
            
            required_fields.Add("Shipments", "Order #");
            required_fields.Add("Shipments", "Tracking #");
            required_fields.Add("Shipments", "Status");

            StringBuilder errors = new StringBuilder();
            foreach (Binding binding in Bindings)
                all_fields.Add(binding.Field.FieldCategory.ToString(), binding.Field.Name);
            foreach (KeyValuePair<string, string> key_value in required_fields)
                if (!all_fields.ContainsKey(key_value.Key) && !all_fields.ContainsValue(key_value.Value)) 
                    errors.Append(key_value.Key + "." + key_value.Value + ".\n");
            if (errors.Length > 0) throw new Exception("The following fields are required:\n" + errors.ToString());

            // check for duplicate bindings
            List<InteGr8.Data.FieldsTableRow> bindings = new List<InteGr8.Data.FieldsTableRow>();
            foreach (Binding binding in Template.Bindings)
                if (bindings.Contains(binding.Field)) throw new Exception("The following field was mapped more than one time: " + binding.Field.Name);
                else bindings.Add(binding.Field);
        }

        // load the given template from the dataset into the template
        public static void Load(string Name)
        {
            Bindings.Clear();
            Links.Clear();
            Tables.Clear();

            if (Name == null)
            {
                // clear the template (used when starting to create a new template)
                Template.Name = "";
                Description = "";
                ReadConn = null;
                WriteConn = null;
                International = true;
                MPS = true;
                MPSPackageLevel = true;
                SaveShipments = true;
                SaveShipmentPackages = true;
                AutoSave_AtShipping = true;
                Select_Filter.Is_Custom = false;
                Select_Filter.Filter_No_Status = "";
                Orders_Select_Statement.Select = null;
                Packages_Select_Statement.Select = null;
                Products_Select_Statement.Select = null;
                Shipments_Insert_Statement.Insert = null;
                return;
            }

            Data data = InteGr8_Main.data;
            Data.TemplatesRow TemplateRow = data.Templates.FindByName(Name);
            if (TemplateRow == null) throw new ArgumentException("Template not found.");

            Template.Name = TemplateRow.Name;
            Description = TemplateRow.Description;
            ReadConn = new Connection(TemplateRow.DSN, TemplateRow.Login, TemplateRow.Password);
            WriteConn = new Connection(TemplateRow.DSN2, TemplateRow.Login2, TemplateRow.Password2);
            BackupConn = new Connection(TemplateRow.DSN3, TemplateRow.Login3, TemplateRow.Password3);
            International = TemplateRow.International;
            MPS = TemplateRow.MPS;
            MPSPackageLevel = TemplateRow.MPS_Package_Level;
            SaveShipments = TemplateRow.Save_Shipments;
            SaveShipmentPackages = TemplateRow.Save_ShipmentPackages;
            AutoSave_AtShipping = TemplateRow.AutoSave_AtShip;
            Aggregate = TemplateRow.Aggregate;
            Check_Save = TemplateRow.Check_Save;
            Save_Select = TemplateRow.Save_Select;
            Backup_Orders = TemplateRow.Backup_Orders;
            Ship_Link = TemplateRow.Ship_Link;
            Backup_Schedule = TemplateRow.Backup_Schedule;
            Backup_Month = TemplateRow.Backup_Month;
            Backup_at_Scheadule = TemplateRow.Backup_at_Scheadule;
            Check_Delete = TemplateRow.Check_Delete;
            Select_Filter.Is_Custom = TemplateRow.Custom_Filter;
            Select_Filter.Filter_No_Status = TemplateRow.Filter;

            Orders_Select_Statement.Select = TemplateRow.IsOrders_SelectNull() ? null : TemplateRow.Orders_Select;
            Packages_Select_Statement.Select = TemplateRow.IsPackages_SelectNull() ? null : TemplateRow.Packages_Select;
            Products_Select_Statement.Select = TemplateRow.IsProducts_SelectNull() ? null : TemplateRow.Products_Select;
            Shipments_Insert_Statement.Insert = TemplateRow.IsShipments_InsertNull() ? null : TemplateRow.Shipments_Insert;
            Shipments_Insert_Statement.Update = TemplateRow.Shipments_Update;
            Shipments_Insert_Statement.Delete = TemplateRow.Check_Delete;
            Shipments_Delete_Statement.Delete = TemplateRow.IsShipments_DeleteNull() ? null : TemplateRow.Shipments_Delete;
            foreach (Data.TablesRow TableRow in TemplateRow.GetTablesRows())
            {
                List<string> PK = new List<string>();
                foreach (Data.PKRow PKRow in TableRow.GetPKRows())
                    PK.Add(PKRow.Name);
                AddTable(CreateTable(TableRow.Name, TableType.Parse(TableRow.Type), PK));
            }
            foreach (Data.LinksRow LinkRow in TemplateRow.GetLinksRows())
            {
                List<string> FK = new List<string>();
                foreach (Data.FKRow FKRow in LinkRow.GetFKRows())
                    FK.Add(FKRow.Name);
                AddLink(CreateLink(FindTable(LinkRow.TablesRowByFK_Tables_Links_Parent.Name, TableType.Parse(LinkRow.TablesRowByFK_Tables_Links_Parent.Type)), FindTable(LinkRow.TablesRowByFK_Tables_Links_Child.Name, TableType.Parse(LinkRow.TablesRowByFK_Tables_Links_Child.Type)), FK));
            }
            foreach (Data.BindingsRow BindingRow in TemplateRow.GetBindingsRows())
            {
                AddBinding(CreateBinding(TableType.Parse(BindingRow.TablesRow.Type), BindingRow.FieldsTableRow.Name, BindingRow.TablesRow.Name, BindingRow.Column, BindingRow.Default));
            }
        }

        // saves the template to xml
        public static void Save()
        {
            Data data = InteGr8_Main.data;
            Data.TemplatesRow TemplateRow = data.Templates.FindByName(Name);
            if (TemplateRow != null) TemplateRow.Delete(); // this will delete everything else (all relations are setup with cascading delete)

            TemplateRow = data.Templates.AddTemplatesRow(
                Name, 
                Description, 
                ReadConn.DSN,
                ReadConn.Login,
                ReadConn.Password,
                International, 
                MPS, 
                MPSPackageLevel, 
                SaveShipments,
                Select_Filter.Filter_No_Status, 
                Select_Filter.Is_Custom, 
                Orders_Select_Statement.Is_Custom ? Orders_Select_Statement.Select : null,
                Packages_Select_Statement.Is_Custom ? Packages_Select_Statement.Select : null,
                Products_Select_Statement.Is_Custom ? Products_Select_Statement.Select : null,
                Shipments_Insert_Statement.Is_Custom ? Shipments_Insert_Statement.Insert : null,
                WriteConn.DSN,
                WriteConn.Login,
                WriteConn.Password,
                SaveShipmentPackages,
                AutoSave_AtShipping,
                Shipments_Delete_Statement.Is_Custom ? Shipments_Delete_Statement.Delete : null,
                BackupConn.DSN,
                BackupConn.Login,
                BackupConn.Password,
                Aggregate,
                Check_Save,
                Shipments_Insert_Statement.Update,
                Save_Select,
                Backup_Orders,
                Ship_Link,
                Backup_Schedule,
                Backup_Month,
                Backup_at_Scheadule,
                Shipments_Insert_Statement.Delete);

            foreach (Table Table in Tables)
            {
                Data.TablesRow TableRow = data._Tables.AddTablesRow(Table.Name, Table.Type.ToString(), TemplateRow);
                foreach (string Column in Table.PK)
                    data.PK.AddPKRow(TableRow, Column);
            }
            foreach (Link Link in Links)
            {
                Data.LinksRow LinkRow = data.Links.AddLinksRow(data._Tables.FindByTemplateNameType(Name, Link.Parent.Name, Link.Parent.Type.ToString()), data._Tables.FindByTemplateNameType(Name, Link.Child.Name, Link.Child.Type.ToString()));
                foreach(string Column in Link.FK)
                    data.FK.AddFKRow(LinkRow, Column);
            }
            foreach (Binding Binding in Bindings)
            {
                Data.LinksRow LinkRow = null;
                if (Binding.Link != null) LinkRow = data.Links.FindByParentChild(data._Tables.FindByTemplateNameType(Name, Binding.Link.Parent.Name, Binding.Link.Parent.Type.ToString()).Id, data._Tables.FindByTemplateNameType(Name, Binding.Link.Child.Name, Binding.Link.Child.Type.ToString()).Id);
                data.Bindings.AddBindingsRow(Binding.Field, data._Tables.FindByTemplateNameType(Name, Binding.Table.Name, Binding.Table.Type.ToString()), LinkRow, Binding.Column, Binding.Default == null ? null : Binding.Default.ToString());
            }
            
            data.Templates.WriteXml(Name + ".xml", true);
        }
    }
}
