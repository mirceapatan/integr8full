﻿namespace InteGr8
{
    partial class frmReplyOverride
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReplyOverride));
            this.dgvReplyOverride = new System.Windows.Forms.DataGridView();
            this.selectedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.orderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipmentTrackingNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deliveryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commercialInvoiceURLDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelURLDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageIndexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.data = new InteGr8.Data();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            //this.shipmentsTableAdapter = new InteGr8.DataTableAdapters.ShipmentsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReplyOverride)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShipTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.data)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvReplyOverride
            // 
            this.dgvReplyOverride.AllowUserToAddRows = false;
            this.dgvReplyOverride.AllowUserToDeleteRows = false;
            this.dgvReplyOverride.AllowUserToOrderColumns = true;
            this.dgvReplyOverride.AllowUserToResizeRows = false;
            this.dgvReplyOverride.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReplyOverride.AutoGenerateColumns = false;
            this.dgvReplyOverride.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReplyOverride.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvReplyOverride.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReplyOverride.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectedDataGridViewCheckBoxColumn,
            this.orderDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.shipmentTrackingNumberDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.deliveryDataGridViewTextBoxColumn,
            this.commercialInvoiceURLDataGridViewTextBoxColumn,
            this.labelURLDataGridViewTextBoxColumn,
            this.errorCodeDataGridViewTextBoxColumn,
            this.errorMessageDataGridViewTextBoxColumn,
            this.packageIndexDataGridViewTextBoxColumn,
            this.packageDataGridViewTextBoxColumn});
            this.dgvReplyOverride.DataSource = this.ShipTableBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvReplyOverride.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvReplyOverride.Location = new System.Drawing.Point(1, 12);
            this.dgvReplyOverride.MultiSelect = false;
            this.dgvReplyOverride.Name = "dgvReplyOverride";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReplyOverride.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvReplyOverride.RowHeadersVisible = false;
            this.dgvReplyOverride.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReplyOverride.Size = new System.Drawing.Size(565, 261);
            this.dgvReplyOverride.TabIndex = 0;
            this.dgvReplyOverride.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReplyOverride_CellEndEdit);
            // 
            // selectedDataGridViewCheckBoxColumn
            // 
            this.selectedDataGridViewCheckBoxColumn.DataPropertyName = "Selected";
            this.selectedDataGridViewCheckBoxColumn.HeaderText = "Selected";
            this.selectedDataGridViewCheckBoxColumn.Name = "selectedDataGridViewCheckBoxColumn";
            this.selectedDataGridViewCheckBoxColumn.Visible = false;
            // 
            // orderDataGridViewTextBoxColumn
            // 
            this.orderDataGridViewTextBoxColumn.DataPropertyName = "Order #";
            this.orderDataGridViewTextBoxColumn.HeaderText = "Order #";
            this.orderDataGridViewTextBoxColumn.Name = "orderDataGridViewTextBoxColumn";
            this.orderDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            // 
            // shipmentTrackingNumberDataGridViewTextBoxColumn
            // 
            this.shipmentTrackingNumberDataGridViewTextBoxColumn.DataPropertyName = "Shipment Tracking Number";
            this.shipmentTrackingNumberDataGridViewTextBoxColumn.HeaderText = "Shipment Tracking Number";
            this.shipmentTrackingNumberDataGridViewTextBoxColumn.Name = "shipmentTrackingNumberDataGridViewTextBoxColumn";
            this.shipmentTrackingNumberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "Price";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // deliveryDataGridViewTextBoxColumn
            // 
            this.deliveryDataGridViewTextBoxColumn.DataPropertyName = "Delivery";
            this.deliveryDataGridViewTextBoxColumn.HeaderText = "Delivery";
            this.deliveryDataGridViewTextBoxColumn.Name = "deliveryDataGridViewTextBoxColumn";
            this.deliveryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // commercialInvoiceURLDataGridViewTextBoxColumn
            // 
            this.commercialInvoiceURLDataGridViewTextBoxColumn.DataPropertyName = "Commercial Invoice URL";
            this.commercialInvoiceURLDataGridViewTextBoxColumn.HeaderText = "Commercial Invoice URL";
            this.commercialInvoiceURLDataGridViewTextBoxColumn.Name = "commercialInvoiceURLDataGridViewTextBoxColumn";
            this.commercialInvoiceURLDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // labelURLDataGridViewTextBoxColumn
            // 
            this.labelURLDataGridViewTextBoxColumn.DataPropertyName = "Label URL";
            this.labelURLDataGridViewTextBoxColumn.HeaderText = "Label URL";
            this.labelURLDataGridViewTextBoxColumn.Name = "labelURLDataGridViewTextBoxColumn";
            this.labelURLDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // errorCodeDataGridViewTextBoxColumn
            // 
            this.errorCodeDataGridViewTextBoxColumn.DataPropertyName = "Error Code";
            this.errorCodeDataGridViewTextBoxColumn.HeaderText = "Error Code";
            this.errorCodeDataGridViewTextBoxColumn.Name = "errorCodeDataGridViewTextBoxColumn";
            this.errorCodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // errorMessageDataGridViewTextBoxColumn
            // 
            this.errorMessageDataGridViewTextBoxColumn.DataPropertyName = "Error Message";
            this.errorMessageDataGridViewTextBoxColumn.HeaderText = "Error Message";
            this.errorMessageDataGridViewTextBoxColumn.Name = "errorMessageDataGridViewTextBoxColumn";
            this.errorMessageDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // packageIndexDataGridViewTextBoxColumn
            // 
            this.packageIndexDataGridViewTextBoxColumn.DataPropertyName = "Package_Index";
            this.packageIndexDataGridViewTextBoxColumn.HeaderText = "Package_Index";
            this.packageIndexDataGridViewTextBoxColumn.Name = "packageIndexDataGridViewTextBoxColumn";
            this.packageIndexDataGridViewTextBoxColumn.Visible = false;
            // 
            // packageDataGridViewTextBoxColumn
            // 
            this.packageDataGridViewTextBoxColumn.DataPropertyName = "Package #";
            this.packageDataGridViewTextBoxColumn.HeaderText = "Package #";
            this.packageDataGridViewTextBoxColumn.Name = "packageDataGridViewTextBoxColumn";
            // 
            // ShipTableBindingSource
            // 
            this.ShipTableBindingSource.DataMember = "ShipmentsTable";
            this.ShipTableBindingSource.DataSource = this.data;
            this.ShipTableBindingSource.Filter = "Package_Index = 0";
            // 
            // data
            // 
            this.data.DataSetName = "Data";
            this.data.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(393, 279);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(85, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(484, 279);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(82, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // shipmentsTableAdapter
            // 
            //this.shipmentsTableAdapter.ClearBeforeFill = true;
            // 
            // frmReplyOverride
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 314);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.dgvReplyOverride);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmReplyOverride";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "2Ship Reply Override";
            this.Load += new System.EventHandler(this.frmReplyOverride_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReplyOverride)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShipTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.data)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvReplyOverride;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.BindingSource ShipTableBindingSource;
        private Data data;
        //private DataTableAdapters.ShipmentsTableAdapter shipmentsTableAdapter;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipmentTrackingNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deliveryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commercialInvoiceURLDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn labelURLDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn errorCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn errorMessageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageIndexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageDataGridViewTextBoxColumn;
    }
}