﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ActiveDatabaseSoftware.ActiveQueryBuilder;

namespace InteGr8
{
    public partial class frmSQLDesigner : Form
    {
        private List<string> Params = new List<string>();

        public frmSQLDesigner(string ConnStr, string SQL)
        {
            InitializeComponent();
            OdbcProvider.Connection = new System.Data.Odbc.OdbcConnection(ConnStr);
            QueryBuilderControl.RefreshMetadata();
            txtSQL.Text = SQL;
            txtSQL_Validating(this, new CancelEventArgs());
        }

        private void TextSQLBuilder_SQLUpdated(object sender, EventArgs e)
        {
            txtSQL.Text = TextSQLBuilder.SQL;
        }

        private void txtSQL_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                QueryBuilderControl.SQL = txtSQL.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message + "\nPlease review the SQL code and try again.", "Invalid SQL code.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int param_count = SQL.Split('?').Length - 1;
            if (param_count == 0) return;

            Params.Clear();
            foreach (SubQuery sq in QueryBuilderControl.SubQueries)
            {
                for (int k = 0; k < sq.Count; k++)
                {
                    CriteriaList filter;
                    if (sq[k] is SubQuery) filter = (sq[k] as SubQuery).ActiveUnionSubquery.CriteriaList;
                    else if (sq[k] is UnionSubQuery) filter = (sq[k] as UnionSubQuery).CriteriaList;
                    else continue;
                    foreach (CriteriaItem itm in filter.Items)
                    {
                        if (itm.ConditionCount == 0) continue;
                        if (itm.ConditionStrings.Count == 1 && itm.ConditionStrings[0].Split('?').Length == 2)
                            Params.Add(itm.ExpressionField.Name.QualifiedNameWithoutQuotes + " <" + itm.ExpressionField.FieldType + " " + itm.ExpressionField.Size + ">"); // one parameter
                        else
                            for (int i = 0; i < itm.ConditionStrings.Count; i++)
                                if (itm.ConditionStrings[i].Contains('?'))
                                    for (int p = 0; p < itm.ConditionStrings[0].Split('?').Length - 1; p++)
                                        Params.Add(itm.ExpressionField.Name.QualifiedNameWithoutQuotes + "  " + (itm.ConditionStrings.Count * i + p + 1).ToString() + " <" + itm.ExpressionField.FieldType + " " + itm.ExpressionField.Size + ">");
                    }
                }
            }
        }

        public string SQL
        {
            get
            {
                return TextSQLBuilder.SQL;
            }
        }

        public string[] Parameters
        {
            get
            {
                return Params.ToArray();
            }
        }
    }
}
