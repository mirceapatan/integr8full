﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmAddManualOrder : Form
    {
        private Data data;
        public frmAddManualOrder()
        {
            InitializeComponent();
            this.data = InteGr8_Main.data;
        }

        private void frmAddManualOrder_Load(object sender, EventArgs e)
        {
            try
            {
                carriersBindingSource.DataSource = data;
                servicesBindingSource.DataSource = data;
                PackagingsBindingSource.DataSource = data;
                Billing_TypesBindingSource.DataSource = data;

                //if (data.SenderAddress.Columns.IndexOf("Sender Contact") >= 0)
                //{
                //    tbSenderContact.Text = data.SenderAddress.Rows[0]["Sender Contact"].ToString();
                //}
                //if (data.SenderAddress.Columns.IndexOf("Sender Company") >= 0)
                //{
                //    tbSenderCompany.Text = data.SenderAddress.Rows[0]["Sender Company"].ToString();
                //}
                //if (data.SenderAddress.Columns.IndexOf("Sender Address 1") >= 0)
                //{
                //    tbSenderAddress1.Text = data.SenderAddress.Rows[0]["Sender Address 1"].ToString();
                //}
                //if (data.SenderAddress.Columns.IndexOf("Sender Address 2") >= 0)
                //{
                //    tbSenderAddress2.Text = data.SenderAddress.Rows[0]["Sender Address 2"].ToString();
                //}
                //if (data.SenderAddress.Columns.IndexOf("Sender City") >= 0)
                //{
                //    tbSenderCity.Text = data.SenderAddress.Rows[0]["Sender City"].ToString();
                //}
                //if (data.SenderAddress.Columns.IndexOf("Sender State / Province") >= 0)
                //{
                //    tbSenderStateProv.Text = data.SenderAddress.Rows[0]["Sender State / Province"].ToString();
                //}
                //if (data.SenderAddress.Columns.IndexOf("Sender ZIP / Postal Code") >= 0)
                //{
                //    tbSenderZIPPostalCode.Text = data.SenderAddress.Rows[0]["Sender ZIP / Postal Code"].ToString();
                //}
                //if (data.SenderAddress.Columns.IndexOf("Sender Country") >= 0)
                //{
                //    tbSenderCountry.Text = data.SenderAddress.Rows[0]["Sender Country"].ToString();
                //}
                //if (data.SenderAddress.Columns.IndexOf("Sender Tel") >= 0)
                //{
                //    tbSenderTelNo.Text = data.SenderAddress.Rows[0]["Sender Tel"].ToString();
                //}
                //if (data.SenderAddress.Columns.IndexOf("Sender Email") >= 0)
                //{
                //    tbSenderEmail.Text = data.SenderAddress.Rows[0]["Sender Email"].ToString();
                //}
                //foreach (DataColumn c in data.OrdersTable.Columns)
                //{
                    //Label lName = new Label();
                    //lName.Text = c.ColumnName;
                    //TextBox tName = new TextBox();
                    //tName.Name = "txt" + c.ColumnName;
                    
                //}
                //foreach (DataColumn c in data.OrderPackagesTable.Columns)
                //{
                    //Label lName = new Label();
                    //lName.Text = c.ColumnName;
                    //TextBox tName = new TextBox();
                    //tName.Name = "txt" + c.ColumnName;
                    //tabPackages.Controls.Add(lName);
                    //tabPackages.Controls.Add(tName);
                    
                //}
               // foreach (DataColumn c in data.OrderProductsTable.Columns)
                //{
                    //Label lName = new Label();
                    //lName.Text = c.ColumnName;
                    //TextBox tName = new TextBox();
                    //tName.Name = "txt" + c.ColumnName;
                    //tabProducts.Controls.Add(lName);
                    //tabProducts.Controls.Add(tName);
                    
                //}
                
            }
            catch (Exception ex)
            {
                Utils.SendError("Error generating manual order form", ex);
            }
        }

        private void btnManualOrderOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbOrderNo.Text.Equals("") || tbPackageNo.Text.Equals("") || tbProductNo.Text.Equals(""))
                {
                    MessageBox.Show("The fields 'Order #', 'Package #', 'Product #' cannot be empty", "Required fields", MessageBoxButtons.OK);
                }
                // add order
                string Carrier = String.IsNullOrEmpty(cmbCarrier.SelectedValue.ToString()) ? null : cmbCarrier.SelectedValue.ToString();
                string Service = String.IsNullOrEmpty(cmbService.SelectedValue.ToString()) ? null : cmbService.SelectedValue.ToString();

                //foreach (Data.Carriers_MappingRow carrierMappingRow in data.Carriers_Mapping)
                //{
                //    if (carrierMappingRow.Carrier_Code.Equals(Carrier))
                //    {
                //        Carrier = carrierMappingRow.Carrier_Code;
                //    }
                //}

                Data.OrdersTableRow newOrder = data.OrdersTable.AddOrdersTableRow(
                    tbOrderNo.Text,
                    tbSenderCountry.Text,
                    tbRecCountry.Text,
                    null, //cmbCarrier.SelectedValue == null ? null : cmbCarrier.SelectedValue,
                    null, //cmbService.SelectedValue.ToString() == null ? null : cmbService.SelectedValue.ToString(),
                    "1", //Packaging
                    tbSenderStateProv.Text,
                    tbSenderZIPPostalCode.Text,
                    tbRecStateProv.Text,
                    tbRecZIPPostalCode.Text,
                    DateTime.Today,
                    "", //Status
                    false, //Selected
                    null); //Billing

                newOrder.Service = Service;
                newOrder.Carrier = Carrier;
                newOrder.Packaging = cmbPackaging.SelectedValue.ToString();
                newOrder.Billing_Type = Convert.ToInt32(cmbBillingType.SelectedValue);
                if (data.OrdersTable.Columns.IndexOf("Sender Company") >= 0)
                {
                    newOrder["Sender Company"] = tbSenderCompany.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Sender Address 1") >= 0)
                {
                    newOrder["Sender Address 1"] = tbSenderAddress1.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Sender Address 2") >= 0)
                {
                    newOrder["Sender Address 2"] = tbSenderAddress2.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Sender City") >= 0)
                {
                    newOrder["Sender City"] = tbSenderCity.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Sender Tel") >= 0)
                {
                    newOrder["Sender Tel"] = tbSenderTelNo.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Sender Email") >= 0)
                {
                    newOrder["Sender Email"] = tbSenderEmail.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Sender Contact") >= 0)
                {
                    newOrder["Sender Contact"] = tbSenderContact.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Recipient Company") >= 0)
                {
                    newOrder["Recipient Company"] = tbRecCompany.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Recipient Address 1") >= 0)
                {
                    newOrder["Recipient Address 1"] = tbRecAddress1.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Recipient Address 2") >= 0)
                {
                    newOrder["Recipient Address 2"] = tbRecAddress2.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Recipient City") >= 0)
                {
                    newOrder["Recipient City"] = tbRecCity.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Recipient Tel") >= 0)
                {
                    newOrder["Recipient Tel"] = tbRecTelNo.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Recipient Email") >= 0)
                {
                    newOrder["Recipient Email"] = tbRecEmail.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Recipient Residential") >= 0)
                {
                    newOrder["Recipient Residential"] = cbRecResidential.Checked;
                }

                if (data.OrdersTable.Columns.IndexOf("Shipment Reference Info") >= 0)
                {
                    newOrder["Shipment Reference Info"] = String.IsNullOrEmpty(tbShipmentRefInfo.Text) ? "0" : tbShipmentRefInfo.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Shipment PO Number") >= 0)
                {
                    newOrder["Shipment PO Number"] = String.IsNullOrEmpty(tbShipmentPONumber.Text) ? "0" : tbShipmentPONumber.Text;
                }
                if (data.OrdersTable.Columns.IndexOf("Order number") >= 0)
                {
                    newOrder["Order number"] = tbOrderNo.Text;
                }
                // add package
                decimal PackageWeight = Convert.ToDecimal(tbPackageWeight.Text);
                string weightUM = String.IsNullOrEmpty(cmbPackageWeightUM.SelectedText) ? "LBS" : cmbPackageWeightUM.SelectedText;
                decimal PackageLength = String.IsNullOrEmpty(tbPackageLength.Text) ? 0m : Convert.ToDecimal(tbPackageLength.Text);
                decimal PackageWidth = String.IsNullOrEmpty(tbPackageWidth.Text) ? 0m : Convert.ToDecimal(tbPackageWidth.Text);
                decimal PackageHeight = String.IsNullOrEmpty(tbPackageHeight.Text) ? 0m : Convert.ToDecimal(tbPackageHeight.Text);
                decimal PackageInsurance = String.IsNullOrEmpty(tbPackageIns.Text) ? 0m : Convert.ToDecimal(tbPackageIns.Text);
                Data.OrderPackagesTableRow newOrderPackage = data.OrderPackagesTable.AddOrderPackagesTableRow(
                                                                newOrder,
                                                                tbPackageNo.Text,
                                                                PackageWeight,
                                                                weightUM,
                                                                PackageLength,
                                                                PackageWidth,
                                                                PackageHeight,
                                                                cmbPackageDimsUM.SelectedText == null ? "I" : cmbPackageDimsUM.Text,
                                                                PackageInsurance,
                                                                cmbPackageInsCurr.SelectedText,
                                                                tbPackageRefInfo.Text);
                if (data.OrderPackagesTable.Columns.IndexOf("Package Reference Info 1") >= 0)
                {
                    newOrderPackage["Package Reference Info 1"] = String.IsNullOrEmpty(tbPackageRefInfo.Text) ? "0" : tbPackageRefInfo.Text;
                }
                if (data.OrderPackagesTable.Columns.IndexOf("Package PO Number") >= 0)
                {
                    newOrderPackage["Package Po Number"] = String.IsNullOrEmpty(tbPackagePONumber.Text) ? "0" : tbPackagePONumber.Text;
                }

                //add skid
                Data.OrdersSkidsTableRow addRow = data.OrdersSkidsTable.AddOrdersSkidsTableRow(
                                                    newOrder,
                                                    tbPackageNo.Text,
                                                    "1",
                                                    "1",
                                                    "false",
                                                    "true",
                                                    "1",
                                                    PackageWeight.ToString(),
                                                    weightUM,
                                                    PackageInsurance.ToString(),
                                                    cmbPackageInsCurr.SelectedText,
                                                    PackageLength.ToString(),
                                                    PackageWidth.ToString(),
                                                    PackageHeight.ToString(),
                                                    cmbPackageDimsUM.SelectedText == null ? "I" : cmbPackageDimsUM.Text,
                                                    String.IsNullOrEmpty(tbPackageRefInfo.Text) ? "0" : tbPackageRefInfo.Text,
                                                    "",
                                                    String.IsNullOrEmpty(tbPackagePONumber.Text) ? "0" : tbPackagePONumber.Text,
                                                    "",
                                                    "",
                                                    String.IsNullOrEmpty(tbShipmentPONumber.Text) ? "0" : tbShipmentPONumber.Text,
                                                    String.IsNullOrEmpty(tbShipmentRefInfo.Text) ? "0" : tbShipmentRefInfo.Text,
                                                    "125.0",
                                                    "1",
                                                    "",
                                                    "");
                //add LTLItem
                Data.OrderLTLItemsRow addItemRow = data.OrderLTLItems.AddOrderLTLItemsRow(
                                                    "1",
                                                    "",
                                                    PackageWeight.ToString(),
                                                    weightUM,
                                                    PackageLength.ToString(),
                                                    PackageWidth.ToString(),
                                                    PackageHeight.ToString(),
                                                    cmbPackageDimsUM.SelectedText == null ? "I" : cmbPackageDimsUM.Text,
                                                    "125.0",
                                                    "1",
                                                    "",
                                                   newOrder, "1");
                // add product
                decimal ProductQuantity = String.IsNullOrEmpty(tbProductQuantity.Text) ? 0m : Convert.ToDecimal(tbProductQuantity.Text);
                Data.OrderProductsTableRow newOrderProduct = data.OrderProductsTable.AddOrderProductsTableRow(
                                                                newOrder,
                                                                tbProductNo.Text,
                                                                tbProductDescription.Text,
                                                                cbProductIsDocument.Checked,
                                                                tbProductCountry.Text,
                                                                ProductQuantity,
                                                                tbProductMU.Text,
                                                                String.IsNullOrEmpty(tbProductUnitWeight.Text) ? 0m : Convert.ToDecimal(tbProductUnitWeight.Text),
                                                                String.IsNullOrEmpty(tbProductUnitValue.Text) ? 0m : Convert.ToDecimal(tbProductUnitValue.Text),
                                                                String.IsNullOrEmpty(tbProductTotalValue.Text) ? 0m : Convert.ToDecimal(tbProductTotalValue.Text),
                                                                tbProductHarmonizedCode.Text,
                                                                String.IsNullOrEmpty(tbProductTotalWeight.Text) ? 0m : Convert.ToDecimal(tbProductTotalWeight.Text));

                //InteGr8_Main.data = data;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();

            }


            catch (Exception ex)
            {
                Utils.SendError("Exception at adding new order (manual)", ex);
            }
        }

        private void btnManualOrderCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
