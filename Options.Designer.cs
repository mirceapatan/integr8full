﻿namespace InteGr8
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.AutoprintPage = new System.Windows.Forms.TabPage();
            this.chbox_contentsLabel = new System.Windows.Forms.CheckBox();
            this.chbox_packslip = new System.Windows.Forms.CheckBox();
            this.chbox_rlabels = new System.Windows.Forms.CheckBox();
            this.chbox_bol = new System.Windows.Forms.CheckBox();
            this.chbox_CI = new System.Windows.Forms.CheckBox();
            this.chbox_labels = new System.Windows.Forms.CheckBox();
            this.btnContentsLabelFolder = new System.Windows.Forms.Button();
            this.txtContentsLabelFolder = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnPackSlipFolder = new System.Windows.Forms.Button();
            this.txtPackSlipFolder = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnReturnLabelFolder = new System.Windows.Forms.Button();
            this.txtReturnLabelFolder = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnBOLFolder = new System.Windows.Forms.Button();
            this.txtBOLFolder = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbboxContentsLabelPrinter = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbboxPackSlipPrinter = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbboxReturnLabelPrinter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbboxBOLPrinter = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbboxCIPrinter = new System.Windows.Forms.ComboBox();
            this.lblReportsPrinter = new System.Windows.Forms.Label();
            this.cbboxLabelsPrinter = new System.Windows.Forms.ComboBox();
            this.lblLabelsPrinter = new System.Windows.Forms.Label();
            this.btnCIFolder = new System.Windows.Forms.Button();
            this.txtCIFolder = new System.Windows.Forms.TextBox();
            this.lblReportsFolder = new System.Windows.Forms.Label();
            this.btnLabelsFolder = new System.Windows.Forms.Button();
            this.txtLabelsFolder = new System.Windows.Forms.TextBox();
            this.lblLabelsFolder = new System.Windows.Forms.Label();
            this.PickUpDaysPage = new System.Windows.Forms.TabPage();
            this.chbSunday = new System.Windows.Forms.CheckBox();
            this.chbSaturday = new System.Windows.Forms.CheckBox();
            this.chbFriday = new System.Windows.Forms.CheckBox();
            this.chbThursday = new System.Windows.Forms.CheckBox();
            this.chbWednesday = new System.Windows.Forms.CheckBox();
            this.chbTuesday = new System.Windows.Forms.CheckBox();
            this.chbMonday = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MyCarrier = new System.Windows.Forms.TabPage();
            this.label_carrier_scac_code = new System.Windows.Forms.Label();
            this.label_carrier_name = new System.Windows.Forms.Label();
            this.txt_carrier_scac_code = new System.Windows.Forms.TextBox();
            this.checkbox_default = new System.Windows.Forms.CheckBox();
            this.txt_carrier_name = new System.Windows.Forms.TextBox();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.listbox_carriers = new System.Windows.Forms.ListBox();
            this.btn_edit_mycarrier = new System.Windows.Forms.Button();
            this.backup = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.delete_hour = new System.Windows.Forms.DateTimePicker();
            this.emails = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.PackageDimensions = new System.Windows.Forms.TabPage();
            this.btnSavePackageDims = new System.Windows.Forms.Button();
            this.btnAddPackageDim = new System.Windows.Forms.Button();
            this.tbPackageHeight = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.tbPackageWidth = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.tbPackageLength = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.tbPackageDimName = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.tbPackageDimCode = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.cbpackageDimensions = new System.Windows.Forms.ComboBox();
            this.ShipmentOptions = new System.Windows.Forms.TabPage();
            this.pFedEx = new System.Windows.Forms.Panel();
            this.tFedExShipOptions = new System.Windows.Forms.TabControl();
            this.FedExShipmentOptions = new System.Windows.Forms.TabPage();
            this.SignatureOptionPanel = new System.Windows.Forms.Panel();
            this.tbReleaseNumber = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.cbSignatureRequired = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.cbSignatureOption = new System.Windows.Forms.CheckBox();
            this.HomeDeliveryPanel = new System.Windows.Forms.Panel();
            this.tbHomeDeliveryPhone = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.dtpHomeDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.label40 = new System.Windows.Forms.Label();
            this.cbHomeDeliveryType = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cbHomeDelivery = new System.Windows.Forms.CheckBox();
            this.cbSaturdayPickup = new System.Windows.Forms.CheckBox();
            this.cbSaturdayDelivery = new System.Windows.Forms.CheckBox();
            this.cbInsidePickup = new System.Windows.Forms.CheckBox();
            this.cbInsideDelivery = new System.Windows.Forms.CheckBox();
            this.DryIcePanel = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.cbDryIceUnit = new System.Windows.Forms.ComboBox();
            this.tbDryIceWeight = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cbDryIce = new System.Windows.Forms.CheckBox();
            this.HoldAtLocationPanel = new System.Windows.Forms.Panel();
            this.tbHoldEmail = new System.Windows.Forms.TextBox();
            this.tbHoldPhone = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.tbHoldAddress2 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tbHoldAddress1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tbHoldZIP = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tbHoldCity = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cbHoldState = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cbHoldCountry = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbHoldCompany = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbHoldContact = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.cbHoldAtLocation = new System.Windows.Forms.CheckBox();
            this.CODPanel = new System.Windows.Forms.Panel();
            this.tbReturnTel = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbZIP = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbCity = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbStateProvince = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cbCountry = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbReturnAddress2 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbReturnAddress1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbReturnCompany = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbReturnContact = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cbReferenceIndicator = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbTranspCharges = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbFedExCollectionType = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbFedExCurrency = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbFedExAmount = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbCOD = new System.Windows.Forms.CheckBox();
            this.PuroShipmentOptions = new System.Windows.Forms.TabPage();
            this.pPuroDangGoods = new System.Windows.Forms.Panel();
            this.cbPuroDGClass = new System.Windows.Forms.ComboBox();
            this.lPuroDangGoodClass = new System.Windows.Forms.Label();
            this.cbPuroDGMode = new System.Windows.Forms.ComboBox();
            this.lPuroDangGoods = new System.Windows.Forms.Label();
            this.cbPuroDangGoods = new System.Windows.Forms.CheckBox();
            this.cbChainOfSignature = new System.Windows.Forms.CheckBox();
            this.cbPuroExceptionHandling = new System.Windows.Forms.CheckBox();
            this.cbPuroSaturdayPickup = new System.Windows.Forms.CheckBox();
            this.cbPuroSaturdayDelivery = new System.Windows.Forms.CheckBox();
            this.cbHoldForPickUp = new System.Windows.Forms.CheckBox();
            this.cbExpressCheque = new System.Windows.Forms.CheckBox();
            this.cbPuroSignatureRequired = new System.Windows.Forms.CheckBox();
            this.cbOSNR = new System.Windows.Forms.CheckBox();
            this.pExpressCheque = new System.Windows.Forms.Panel();
            this.tbPuroPhone = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.tbPuroEmail = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.tbPuroZIP = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.tbPuroAddress2 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.tbPuroAddress1 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.tbPuroCity = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.cbPuroState = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this.cbPuroCountry = new System.Windows.Forms.ComboBox();
            this.label52 = new System.Windows.Forms.Label();
            this.tbDepartment = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.tbCompany = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.cbMethodOfPayment = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.tbAmount = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.LoomisShipmentOptions = new System.Windows.Forms.TabPage();
            this.rtbLoomisInstructions = new System.Windows.Forms.RichTextBox();
            this.labelInstructions = new System.Windows.Forms.Label();
            this.tbLoomisAdditionalInfo2 = new System.Windows.Forms.TextBox();
            this.labelAdditionalInfo2 = new System.Windows.Forms.Label();
            this.tbLoomisAdditionalInfo1 = new System.Windows.Forms.TextBox();
            this.labelAdditionalInfo1 = new System.Windows.Forms.Label();
            this.labelAdditionalInfo = new System.Windows.Forms.Label();
            this.cbLoomisNoSignatureRequired = new System.Windows.Forms.CheckBox();
            this.cbLoomisSaturdayDelivery = new System.Windows.Forms.CheckBox();
            this.pLoomisReturnCheck = new System.Windows.Forms.Panel();
            this.cbLoomisMethodOfPayment = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.tbLoomisAmount = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.cbLoomisReturnCheck = new System.Windows.Forms.CheckBox();
            this.cbLoomisFragile = new System.Windows.Forms.CheckBox();
            this.cbLoomisNotPackaged = new System.Windows.Forms.CheckBox();
            this.UPSShipmentOptions = new System.Windows.Forms.TabPage();
            this.pUPSDeliveryConfirmation = new System.Windows.Forms.Panel();
            this.label67 = new System.Windows.Forms.Label();
            this.cbUPSDeliveryConfirmationType = new System.Windows.Forms.ComboBox();
            this.cbUPSLargePackage = new System.Windows.Forms.CheckBox();
            this.cbUPSDeliveryConfirmation = new System.Windows.Forms.CheckBox();
            this.cbUPSSaturdayPickup = new System.Windows.Forms.CheckBox();
            this.cbUPSSaturdayDelivery = new System.Windows.Forms.CheckBox();
            this.CAPostShipmentOptions = new System.Windows.Forms.TabPage();
            this.cbCAPostLeaveAtDoor = new System.Windows.Forms.CheckBox();
            this.cbCAPostDoNotSafeDrop = new System.Windows.Forms.CheckBox();
            this.cbCaPostCardForPickup = new System.Windows.Forms.CheckBox();
            this.labelCAPostNonDeliveryHandling = new System.Windows.Forms.Label();
            this.pCAPostCOD = new System.Windows.Forms.Panel();
            this.tbCAPostCODPhone = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.tbCAPostCODEmail = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.tbCAPostCODZIP = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.tbCAPostCODAddress2 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.tbCAPostCODAddress1 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.tbCAPostCODCity = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.cbCAPostCODState = new System.Windows.Forms.ComboBox();
            this.label79 = new System.Windows.Forms.Label();
            this.cbCAPostCODCountry = new System.Windows.Forms.ComboBox();
            this.label78 = new System.Windows.Forms.Label();
            this.tbCAPostCODCompany = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.tbCAPostCODName = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.cbCAPostMethodOfPayment = new System.Windows.Forms.ComboBox();
            this.label75 = new System.Windows.Forms.Label();
            this.tbCAPostAmount = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.cbCAPostCOD = new System.Windows.Forms.CheckBox();
            this.cbCAPostUnpackaged = new System.Windows.Forms.CheckBox();
            this.cbCAPostDeliveryConfirmation = new System.Windows.Forms.CheckBox();
            this.cbCAPostSignatureRequired = new System.Windows.Forms.CheckBox();
            this.label73 = new System.Windows.Forms.Label();
            this.cbCAPostOffice = new System.Windows.Forms.CheckBox();
            this.cbCAPostAge19 = new System.Windows.Forms.CheckBox();
            this.cbCAPostAge18 = new System.Windows.Forms.CheckBox();
            this.label72 = new System.Windows.Forms.Label();
            this.CanparShipmentOptions = new System.Windows.Forms.TabPage();
            this.tbCanparCODAmount = new System.Windows.Forms.TextBox();
            this.labelCanparCODAmount = new System.Windows.Forms.Label();
            this.cbCanparSaturdayDelivery = new System.Windows.Forms.CheckBox();
            this.cbCanparExtraCare = new System.Windows.Forms.CheckBox();
            this.cbCanparCOD = new System.Windows.Forms.CheckBox();
            this.DicomShipmentOptions = new System.Windows.Forms.TabPage();
            this.cbDicomSignatureRequired = new System.Windows.Forms.CheckBox();
            this.cbDicomOvernight = new System.Windows.Forms.CheckBox();
            this.cbDicomWeekend = new System.Windows.Forms.CheckBox();
            this.cbDicomDangerousGoods = new System.Windows.Forms.CheckBox();
            this.PackageOptions = new System.Windows.Forms.TabPage();
            this.btnSavePackageOption = new System.Windows.Forms.Button();
            this.cbPackageIndex = new System.Windows.Forms.ComboBox();
            this.pUPSPackageOptions = new System.Windows.Forms.Panel();
            this.cbUPSPackageAdditionalHandling = new System.Windows.Forms.CheckBox();
            this.cbUPSPackageCOD = new System.Windows.Forms.CheckBox();
            this.pUPSPackageCOD = new System.Windows.Forms.Panel();
            this.cbUPSCollectionTypePackage = new System.Windows.Forms.ComboBox();
            this.label70 = new System.Windows.Forms.Label();
            this.cbUPSPackageCODCurrency = new System.Windows.Forms.ComboBox();
            this.label69 = new System.Windows.Forms.Label();
            this.tbUPSPackageCODAmount = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.cbUPSPackageDeliveryConfirmationType = new System.Windows.Forms.ComboBox();
            this.cbUPSPackageDeliveryConfirmation = new System.Windows.Forms.CheckBox();
            this.labelUPSPackageConfirmationType = new System.Windows.Forms.Label();
            this.pPuroPackageOptions = new System.Windows.Forms.Panel();
            this.cbPuroExceptionHandlingPackage = new System.Windows.Forms.CheckBox();
            this.pFedExPackageOptions = new System.Windows.Forms.Panel();
            this.cbPackageDryIce = new System.Windows.Forms.CheckBox();
            this.pDryIcePackageLevel = new System.Windows.Forms.Panel();
            this.cbPackageDryIceUnit = new System.Windows.Forms.ComboBox();
            this.tbPackageDryIceWeight = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.cbPackageNonStandardContainer = new System.Windows.Forms.CheckBox();
            this.label44 = new System.Windows.Forms.Label();
            this.Loomis30ShipOptions = new System.Windows.Forms.TabPage();
            this.rtbLInstr = new System.Windows.Forms.RichTextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.cbLSpecialHandling = new System.Windows.Forms.CheckBox();
            this.cbLNSR = new System.Windows.Forms.CheckBox();
            this.cbLNonStdPack = new System.Windows.Forms.CheckBox();
            this.cbLSatDel = new System.Windows.Forms.CheckBox();
            this.cbLFragile = new System.Windows.Forms.CheckBox();
            this.cbLDangGoods = new System.Windows.Forms.CheckBox();
            this.ICSShipOptions = new System.Windows.Forms.TabPage();
            this.tbICSSI = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.cbICSNSR = new System.Windows.Forms.CheckBox();
            this.NationexShipOptions = new System.Windows.Forms.TabPage();
            this.cbNationexFP = new System.Windows.Forms.CheckBox();
            this.cbNationexRR = new System.Windows.Forms.CheckBox();
            this.cbNationexNC = new System.Windows.Forms.CheckBox();
            this.cbNationexDG = new System.Windows.Forms.CheckBox();
            this.ATSShipOptions = new System.Windows.Forms.TabPage();
            this.dtpATSEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtpATSStartDate = new System.Windows.Forms.DateTimePicker();
            this.cbATSReleaseOptions = new System.Windows.Forms.ComboBox();
            this.cbATSTR = new System.Windows.Forms.CheckBox();
            this.cbATSTemp = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this.cbATSTailGate = new System.Windows.Forms.CheckBox();
            this.cbATSAppt = new System.Windows.Forms.CheckBox();
            this.cbATSHardcopyPOD = new System.Windows.Forms.CheckBox();
            this.cbATSDG = new System.Windows.Forms.CheckBox();
            this.cbATSBackDoor = new System.Windows.Forms.CheckBox();
            this.cmdOk = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.FolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.TabControl.SuspendLayout();
            this.AutoprintPage.SuspendLayout();
            this.PickUpDaysPage.SuspendLayout();
            this.MyCarrier.SuspendLayout();
            this.backup.SuspendLayout();
            this.emails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.PackageDimensions.SuspendLayout();
            this.ShipmentOptions.SuspendLayout();
            this.pFedEx.SuspendLayout();
            this.tFedExShipOptions.SuspendLayout();
            this.FedExShipmentOptions.SuspendLayout();
            this.SignatureOptionPanel.SuspendLayout();
            this.HomeDeliveryPanel.SuspendLayout();
            this.DryIcePanel.SuspendLayout();
            this.HoldAtLocationPanel.SuspendLayout();
            this.CODPanel.SuspendLayout();
            this.PuroShipmentOptions.SuspendLayout();
            this.pPuroDangGoods.SuspendLayout();
            this.pExpressCheque.SuspendLayout();
            this.LoomisShipmentOptions.SuspendLayout();
            this.pLoomisReturnCheck.SuspendLayout();
            this.UPSShipmentOptions.SuspendLayout();
            this.pUPSDeliveryConfirmation.SuspendLayout();
            this.CAPostShipmentOptions.SuspendLayout();
            this.pCAPostCOD.SuspendLayout();
            this.CanparShipmentOptions.SuspendLayout();
            this.DicomShipmentOptions.SuspendLayout();
            this.PackageOptions.SuspendLayout();
            this.pUPSPackageOptions.SuspendLayout();
            this.pUPSPackageCOD.SuspendLayout();
            this.pPuroPackageOptions.SuspendLayout();
            this.pFedExPackageOptions.SuspendLayout();
            this.pDryIcePackageLevel.SuspendLayout();
            this.Loomis30ShipOptions.SuspendLayout();
            this.ICSShipOptions.SuspendLayout();
            this.NationexShipOptions.SuspendLayout();
            this.ATSShipOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.AutoprintPage);
            this.TabControl.Controls.Add(this.PickUpDaysPage);
            this.TabControl.Controls.Add(this.MyCarrier);
            this.TabControl.Controls.Add(this.backup);
            this.TabControl.Controls.Add(this.emails);
            this.TabControl.Controls.Add(this.PackageDimensions);
            this.TabControl.Controls.Add(this.ShipmentOptions);
            this.TabControl.Location = new System.Drawing.Point(12, 12);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(595, 361);
            this.TabControl.TabIndex = 0;
            // 
            // AutoprintPage
            // 
            this.AutoprintPage.Controls.Add(this.chbox_contentsLabel);
            this.AutoprintPage.Controls.Add(this.chbox_packslip);
            this.AutoprintPage.Controls.Add(this.chbox_rlabels);
            this.AutoprintPage.Controls.Add(this.chbox_bol);
            this.AutoprintPage.Controls.Add(this.chbox_CI);
            this.AutoprintPage.Controls.Add(this.chbox_labels);
            this.AutoprintPage.Controls.Add(this.btnContentsLabelFolder);
            this.AutoprintPage.Controls.Add(this.txtContentsLabelFolder);
            this.AutoprintPage.Controls.Add(this.label8);
            this.AutoprintPage.Controls.Add(this.btnPackSlipFolder);
            this.AutoprintPage.Controls.Add(this.txtPackSlipFolder);
            this.AutoprintPage.Controls.Add(this.label9);
            this.AutoprintPage.Controls.Add(this.btnReturnLabelFolder);
            this.AutoprintPage.Controls.Add(this.txtReturnLabelFolder);
            this.AutoprintPage.Controls.Add(this.label6);
            this.AutoprintPage.Controls.Add(this.btnBOLFolder);
            this.AutoprintPage.Controls.Add(this.txtBOLFolder);
            this.AutoprintPage.Controls.Add(this.label7);
            this.AutoprintPage.Controls.Add(this.cbboxContentsLabelPrinter);
            this.AutoprintPage.Controls.Add(this.label4);
            this.AutoprintPage.Controls.Add(this.cbboxPackSlipPrinter);
            this.AutoprintPage.Controls.Add(this.label5);
            this.AutoprintPage.Controls.Add(this.cbboxReturnLabelPrinter);
            this.AutoprintPage.Controls.Add(this.label2);
            this.AutoprintPage.Controls.Add(this.cbboxBOLPrinter);
            this.AutoprintPage.Controls.Add(this.label3);
            this.AutoprintPage.Controls.Add(this.cbboxCIPrinter);
            this.AutoprintPage.Controls.Add(this.lblReportsPrinter);
            this.AutoprintPage.Controls.Add(this.cbboxLabelsPrinter);
            this.AutoprintPage.Controls.Add(this.lblLabelsPrinter);
            this.AutoprintPage.Controls.Add(this.btnCIFolder);
            this.AutoprintPage.Controls.Add(this.txtCIFolder);
            this.AutoprintPage.Controls.Add(this.lblReportsFolder);
            this.AutoprintPage.Controls.Add(this.btnLabelsFolder);
            this.AutoprintPage.Controls.Add(this.txtLabelsFolder);
            this.AutoprintPage.Controls.Add(this.lblLabelsFolder);
            this.AutoprintPage.Location = new System.Drawing.Point(4, 22);
            this.AutoprintPage.Name = "AutoprintPage";
            this.AutoprintPage.Padding = new System.Windows.Forms.Padding(10);
            this.AutoprintPage.Size = new System.Drawing.Size(587, 335);
            this.AutoprintPage.TabIndex = 0;
            this.AutoprintPage.Text = "Autoprint";
            this.AutoprintPage.UseVisualStyleBackColor = true;
            // 
            // chbox_contentsLabel
            // 
            this.chbox_contentsLabel.AutoSize = true;
            this.chbox_contentsLabel.Location = new System.Drawing.Point(501, 145);
            this.chbox_contentsLabel.Name = "chbox_contentsLabel";
            this.chbox_contentsLabel.Size = new System.Drawing.Size(67, 17);
            this.chbox_contentsLabel.TabIndex = 36;
            this.chbox_contentsLabel.Text = "Disabled";
            this.chbox_contentsLabel.UseVisualStyleBackColor = true;
            this.chbox_contentsLabel.CheckedChanged += new System.EventHandler(this.chbox_contentsLabel_CheckedChanged);
            // 
            // chbox_packslip
            // 
            this.chbox_packslip.AutoSize = true;
            this.chbox_packslip.Location = new System.Drawing.Point(501, 119);
            this.chbox_packslip.Name = "chbox_packslip";
            this.chbox_packslip.Size = new System.Drawing.Size(67, 17);
            this.chbox_packslip.TabIndex = 35;
            this.chbox_packslip.Text = "Disabled";
            this.chbox_packslip.UseVisualStyleBackColor = true;
            this.chbox_packslip.CheckedChanged += new System.EventHandler(this.chbox_packslip_CheckedChanged);
            // 
            // chbox_rlabels
            // 
            this.chbox_rlabels.AutoSize = true;
            this.chbox_rlabels.Location = new System.Drawing.Point(501, 93);
            this.chbox_rlabels.Name = "chbox_rlabels";
            this.chbox_rlabels.Size = new System.Drawing.Size(67, 17);
            this.chbox_rlabels.TabIndex = 34;
            this.chbox_rlabels.Text = "Disabled";
            this.chbox_rlabels.UseVisualStyleBackColor = true;
            this.chbox_rlabels.CheckedChanged += new System.EventHandler(this.chbox_rlabels_CheckedChanged);
            // 
            // chbox_bol
            // 
            this.chbox_bol.AutoSize = true;
            this.chbox_bol.Location = new System.Drawing.Point(501, 67);
            this.chbox_bol.Name = "chbox_bol";
            this.chbox_bol.Size = new System.Drawing.Size(67, 17);
            this.chbox_bol.TabIndex = 33;
            this.chbox_bol.Text = "Disabled";
            this.chbox_bol.UseVisualStyleBackColor = true;
            this.chbox_bol.CheckedChanged += new System.EventHandler(this.chbox_bol_CheckedChanged);
            // 
            // chbox_CI
            // 
            this.chbox_CI.AutoSize = true;
            this.chbox_CI.Location = new System.Drawing.Point(501, 41);
            this.chbox_CI.Name = "chbox_CI";
            this.chbox_CI.Size = new System.Drawing.Size(67, 17);
            this.chbox_CI.TabIndex = 32;
            this.chbox_CI.Text = "Disabled";
            this.chbox_CI.UseVisualStyleBackColor = true;
            this.chbox_CI.CheckedChanged += new System.EventHandler(this.chbox_CI_CheckedChanged);
            // 
            // chbox_labels
            // 
            this.chbox_labels.AutoSize = true;
            this.chbox_labels.Location = new System.Drawing.Point(501, 15);
            this.chbox_labels.Name = "chbox_labels";
            this.chbox_labels.Size = new System.Drawing.Size(67, 17);
            this.chbox_labels.TabIndex = 31;
            this.chbox_labels.Text = "Disabled";
            this.chbox_labels.UseVisualStyleBackColor = true;
            this.chbox_labels.CheckedChanged += new System.EventHandler(this.chbox_labels_CheckedChanged);
            // 
            // btnContentsLabelFolder
            // 
            this.btnContentsLabelFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContentsLabelFolder.Enabled = false;
            this.btnContentsLabelFolder.Location = new System.Drawing.Point(462, 142);
            this.btnContentsLabelFolder.Name = "btnContentsLabelFolder";
            this.btnContentsLabelFolder.Size = new System.Drawing.Size(26, 21);
            this.btnContentsLabelFolder.TabIndex = 30;
            this.btnContentsLabelFolder.Text = "...";
            this.btnContentsLabelFolder.UseVisualStyleBackColor = true;
            this.btnContentsLabelFolder.Click += new System.EventHandler(this.btnContentsLabelFolder_Click);
            // 
            // txtContentsLabelFolder
            // 
            this.txtContentsLabelFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContentsLabelFolder.Enabled = false;
            this.txtContentsLabelFolder.Location = new System.Drawing.Point(146, 143);
            this.txtContentsLabelFolder.Name = "txtContentsLabelFolder";
            this.txtContentsLabelFolder.Size = new System.Drawing.Size(310, 20);
            this.txtContentsLabelFolder.TabIndex = 29;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Contents Label folder:";
            // 
            // btnPackSlipFolder
            // 
            this.btnPackSlipFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPackSlipFolder.Enabled = false;
            this.btnPackSlipFolder.Location = new System.Drawing.Point(462, 116);
            this.btnPackSlipFolder.Name = "btnPackSlipFolder";
            this.btnPackSlipFolder.Size = new System.Drawing.Size(26, 21);
            this.btnPackSlipFolder.TabIndex = 27;
            this.btnPackSlipFolder.Text = "...";
            this.btnPackSlipFolder.UseVisualStyleBackColor = true;
            this.btnPackSlipFolder.Click += new System.EventHandler(this.btnPackSlipFolder_Click);
            // 
            // txtPackSlipFolder
            // 
            this.txtPackSlipFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPackSlipFolder.Enabled = false;
            this.txtPackSlipFolder.Location = new System.Drawing.Point(146, 117);
            this.txtPackSlipFolder.Name = "txtPackSlipFolder";
            this.txtPackSlipFolder.Size = new System.Drawing.Size(310, 20);
            this.txtPackSlipFolder.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Packslip folder:";
            // 
            // btnReturnLabelFolder
            // 
            this.btnReturnLabelFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReturnLabelFolder.Enabled = false;
            this.btnReturnLabelFolder.Location = new System.Drawing.Point(462, 90);
            this.btnReturnLabelFolder.Name = "btnReturnLabelFolder";
            this.btnReturnLabelFolder.Size = new System.Drawing.Size(26, 21);
            this.btnReturnLabelFolder.TabIndex = 24;
            this.btnReturnLabelFolder.Text = "...";
            this.btnReturnLabelFolder.UseVisualStyleBackColor = true;
            this.btnReturnLabelFolder.Click += new System.EventHandler(this.btnReturnLabelFolder_Click);
            // 
            // txtReturnLabelFolder
            // 
            this.txtReturnLabelFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReturnLabelFolder.Enabled = false;
            this.txtReturnLabelFolder.Location = new System.Drawing.Point(146, 91);
            this.txtReturnLabelFolder.Name = "txtReturnLabelFolder";
            this.txtReturnLabelFolder.Size = new System.Drawing.Size(310, 20);
            this.txtReturnLabelFolder.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Return Label folder:";
            // 
            // btnBOLFolder
            // 
            this.btnBOLFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBOLFolder.Enabled = false;
            this.btnBOLFolder.Location = new System.Drawing.Point(462, 64);
            this.btnBOLFolder.Name = "btnBOLFolder";
            this.btnBOLFolder.Size = new System.Drawing.Size(26, 21);
            this.btnBOLFolder.TabIndex = 21;
            this.btnBOLFolder.Text = "...";
            this.btnBOLFolder.UseVisualStyleBackColor = true;
            this.btnBOLFolder.Click += new System.EventHandler(this.btnBOLFolder_Click);
            // 
            // txtBOLFolder
            // 
            this.txtBOLFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBOLFolder.Enabled = false;
            this.txtBOLFolder.Location = new System.Drawing.Point(146, 65);
            this.txtBOLFolder.Name = "txtBOLFolder";
            this.txtBOLFolder.Size = new System.Drawing.Size(310, 20);
            this.txtBOLFolder.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "BOL folder:";
            // 
            // cbboxContentsLabelPrinter
            // 
            this.cbboxContentsLabelPrinter.Enabled = false;
            this.cbboxContentsLabelPrinter.FormattingEnabled = true;
            this.cbboxContentsLabelPrinter.Location = new System.Drawing.Point(146, 304);
            this.cbboxContentsLabelPrinter.Name = "cbboxContentsLabelPrinter";
            this.cbboxContentsLabelPrinter.Size = new System.Drawing.Size(248, 21);
            this.cbboxContentsLabelPrinter.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 307);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Contents Label printer:";
            // 
            // cbboxPackSlipPrinter
            // 
            this.cbboxPackSlipPrinter.Enabled = false;
            this.cbboxPackSlipPrinter.FormattingEnabled = true;
            this.cbboxPackSlipPrinter.Location = new System.Drawing.Point(146, 277);
            this.cbboxPackSlipPrinter.Name = "cbboxPackSlipPrinter";
            this.cbboxPackSlipPrinter.Size = new System.Drawing.Size(248, 21);
            this.cbboxPackSlipPrinter.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 280);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Packslip printer:";
            // 
            // cbboxReturnLabelPrinter
            // 
            this.cbboxReturnLabelPrinter.Enabled = false;
            this.cbboxReturnLabelPrinter.FormattingEnabled = true;
            this.cbboxReturnLabelPrinter.Location = new System.Drawing.Point(146, 250);
            this.cbboxReturnLabelPrinter.Name = "cbboxReturnLabelPrinter";
            this.cbboxReturnLabelPrinter.Size = new System.Drawing.Size(248, 21);
            this.cbboxReturnLabelPrinter.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Return Label printer:";
            // 
            // cbboxBOLPrinter
            // 
            this.cbboxBOLPrinter.Enabled = false;
            this.cbboxBOLPrinter.FormattingEnabled = true;
            this.cbboxBOLPrinter.Location = new System.Drawing.Point(146, 223);
            this.cbboxBOLPrinter.Name = "cbboxBOLPrinter";
            this.cbboxBOLPrinter.Size = new System.Drawing.Size(248, 21);
            this.cbboxBOLPrinter.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "BOL printer:";
            // 
            // cbboxCIPrinter
            // 
            this.cbboxCIPrinter.Enabled = false;
            this.cbboxCIPrinter.FormattingEnabled = true;
            this.cbboxCIPrinter.Location = new System.Drawing.Point(146, 196);
            this.cbboxCIPrinter.Name = "cbboxCIPrinter";
            this.cbboxCIPrinter.Size = new System.Drawing.Size(248, 21);
            this.cbboxCIPrinter.TabIndex = 9;
            // 
            // lblReportsPrinter
            // 
            this.lblReportsPrinter.AutoSize = true;
            this.lblReportsPrinter.Location = new System.Drawing.Point(24, 199);
            this.lblReportsPrinter.Name = "lblReportsPrinter";
            this.lblReportsPrinter.Size = new System.Drawing.Size(52, 13);
            this.lblReportsPrinter.TabIndex = 8;
            this.lblReportsPrinter.Text = "CI printer:";
            // 
            // cbboxLabelsPrinter
            // 
            this.cbboxLabelsPrinter.Enabled = false;
            this.cbboxLabelsPrinter.FormattingEnabled = true;
            this.cbboxLabelsPrinter.Location = new System.Drawing.Point(146, 169);
            this.cbboxLabelsPrinter.Name = "cbboxLabelsPrinter";
            this.cbboxLabelsPrinter.Size = new System.Drawing.Size(248, 21);
            this.cbboxLabelsPrinter.TabIndex = 7;
            // 
            // lblLabelsPrinter
            // 
            this.lblLabelsPrinter.AutoSize = true;
            this.lblLabelsPrinter.Location = new System.Drawing.Point(24, 172);
            this.lblLabelsPrinter.Name = "lblLabelsPrinter";
            this.lblLabelsPrinter.Size = new System.Drawing.Size(73, 13);
            this.lblLabelsPrinter.TabIndex = 6;
            this.lblLabelsPrinter.Text = "Labels printer:";
            // 
            // btnCIFolder
            // 
            this.btnCIFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCIFolder.Enabled = false;
            this.btnCIFolder.Location = new System.Drawing.Point(462, 38);
            this.btnCIFolder.Name = "btnCIFolder";
            this.btnCIFolder.Size = new System.Drawing.Size(26, 21);
            this.btnCIFolder.TabIndex = 5;
            this.btnCIFolder.Text = "...";
            this.btnCIFolder.UseVisualStyleBackColor = true;
            this.btnCIFolder.Click += new System.EventHandler(this.btnCIFolder_Click);
            // 
            // txtCIFolder
            // 
            this.txtCIFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCIFolder.Enabled = false;
            this.txtCIFolder.Location = new System.Drawing.Point(146, 39);
            this.txtCIFolder.Name = "txtCIFolder";
            this.txtCIFolder.Size = new System.Drawing.Size(310, 20);
            this.txtCIFolder.TabIndex = 4;
            // 
            // lblReportsFolder
            // 
            this.lblReportsFolder.AutoSize = true;
            this.lblReportsFolder.Location = new System.Drawing.Point(24, 42);
            this.lblReportsFolder.Name = "lblReportsFolder";
            this.lblReportsFolder.Size = new System.Drawing.Size(49, 13);
            this.lblReportsFolder.TabIndex = 3;
            this.lblReportsFolder.Text = "CI folder:";
            // 
            // btnLabelsFolder
            // 
            this.btnLabelsFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLabelsFolder.Enabled = false;
            this.btnLabelsFolder.Location = new System.Drawing.Point(462, 12);
            this.btnLabelsFolder.Name = "btnLabelsFolder";
            this.btnLabelsFolder.Size = new System.Drawing.Size(26, 21);
            this.btnLabelsFolder.TabIndex = 2;
            this.btnLabelsFolder.Text = "...";
            this.btnLabelsFolder.UseVisualStyleBackColor = true;
            this.btnLabelsFolder.Click += new System.EventHandler(this.btnLabelsFolder_Click);
            // 
            // txtLabelsFolder
            // 
            this.txtLabelsFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLabelsFolder.Enabled = false;
            this.txtLabelsFolder.Location = new System.Drawing.Point(146, 13);
            this.txtLabelsFolder.Name = "txtLabelsFolder";
            this.txtLabelsFolder.Size = new System.Drawing.Size(310, 20);
            this.txtLabelsFolder.TabIndex = 1;
            // 
            // lblLabelsFolder
            // 
            this.lblLabelsFolder.AutoSize = true;
            this.lblLabelsFolder.Location = new System.Drawing.Point(24, 16);
            this.lblLabelsFolder.Name = "lblLabelsFolder";
            this.lblLabelsFolder.Size = new System.Drawing.Size(70, 13);
            this.lblLabelsFolder.TabIndex = 0;
            this.lblLabelsFolder.Text = "Labels folder:";
            // 
            // PickUpDaysPage
            // 
            this.PickUpDaysPage.Controls.Add(this.chbSunday);
            this.PickUpDaysPage.Controls.Add(this.chbSaturday);
            this.PickUpDaysPage.Controls.Add(this.chbFriday);
            this.PickUpDaysPage.Controls.Add(this.chbThursday);
            this.PickUpDaysPage.Controls.Add(this.chbWednesday);
            this.PickUpDaysPage.Controls.Add(this.chbTuesday);
            this.PickUpDaysPage.Controls.Add(this.chbMonday);
            this.PickUpDaysPage.Controls.Add(this.label1);
            this.PickUpDaysPage.Location = new System.Drawing.Point(4, 22);
            this.PickUpDaysPage.Name = "PickUpDaysPage";
            this.PickUpDaysPage.Size = new System.Drawing.Size(587, 335);
            this.PickUpDaysPage.TabIndex = 1;
            this.PickUpDaysPage.Text = "PickUp Days";
            this.PickUpDaysPage.UseVisualStyleBackColor = true;
            // 
            // chbSunday
            // 
            this.chbSunday.AutoSize = true;
            this.chbSunday.Location = new System.Drawing.Point(152, 72);
            this.chbSunday.Name = "chbSunday";
            this.chbSunday.Size = new System.Drawing.Size(62, 17);
            this.chbSunday.TabIndex = 7;
            this.chbSunday.Text = "Sunday";
            this.chbSunday.UseVisualStyleBackColor = true;
            // 
            // chbSaturday
            // 
            this.chbSaturday.AutoSize = true;
            this.chbSaturday.Location = new System.Drawing.Point(152, 49);
            this.chbSaturday.Name = "chbSaturday";
            this.chbSaturday.Size = new System.Drawing.Size(68, 17);
            this.chbSaturday.TabIndex = 6;
            this.chbSaturday.Text = "Saturday";
            this.chbSaturday.UseVisualStyleBackColor = true;
            // 
            // chbFriday
            // 
            this.chbFriday.AutoSize = true;
            this.chbFriday.Location = new System.Drawing.Point(22, 141);
            this.chbFriday.Name = "chbFriday";
            this.chbFriday.Size = new System.Drawing.Size(54, 17);
            this.chbFriday.TabIndex = 5;
            this.chbFriday.Text = "Friday";
            this.chbFriday.UseVisualStyleBackColor = true;
            // 
            // chbThursday
            // 
            this.chbThursday.AutoSize = true;
            this.chbThursday.Location = new System.Drawing.Point(22, 118);
            this.chbThursday.Name = "chbThursday";
            this.chbThursday.Size = new System.Drawing.Size(70, 17);
            this.chbThursday.TabIndex = 4;
            this.chbThursday.Text = "Thursday";
            this.chbThursday.UseVisualStyleBackColor = true;
            // 
            // chbWednesday
            // 
            this.chbWednesday.AutoSize = true;
            this.chbWednesday.Location = new System.Drawing.Point(22, 95);
            this.chbWednesday.Name = "chbWednesday";
            this.chbWednesday.Size = new System.Drawing.Size(83, 17);
            this.chbWednesday.TabIndex = 3;
            this.chbWednesday.Text = "Wednesday";
            this.chbWednesday.UseVisualStyleBackColor = true;
            // 
            // chbTuesday
            // 
            this.chbTuesday.AutoSize = true;
            this.chbTuesday.Location = new System.Drawing.Point(22, 72);
            this.chbTuesday.Name = "chbTuesday";
            this.chbTuesday.Size = new System.Drawing.Size(67, 17);
            this.chbTuesday.TabIndex = 2;
            this.chbTuesday.Text = "Tuesday";
            this.chbTuesday.UseVisualStyleBackColor = true;
            // 
            // chbMonday
            // 
            this.chbMonday.AutoSize = true;
            this.chbMonday.Location = new System.Drawing.Point(22, 49);
            this.chbMonday.Name = "chbMonday";
            this.chbMonday.Size = new System.Drawing.Size(64, 17);
            this.chbMonday.TabIndex = 1;
            this.chbMonday.Text = "Monday";
            this.chbMonday.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select the pick up days:";
            // 
            // MyCarrier
            // 
            this.MyCarrier.Controls.Add(this.label_carrier_scac_code);
            this.MyCarrier.Controls.Add(this.label_carrier_name);
            this.MyCarrier.Controls.Add(this.txt_carrier_scac_code);
            this.MyCarrier.Controls.Add(this.checkbox_default);
            this.MyCarrier.Controls.Add(this.txt_carrier_name);
            this.MyCarrier.Controls.Add(this.btn_delete);
            this.MyCarrier.Controls.Add(this.btn_add);
            this.MyCarrier.Controls.Add(this.listbox_carriers);
            this.MyCarrier.Controls.Add(this.btn_edit_mycarrier);
            this.MyCarrier.Location = new System.Drawing.Point(4, 22);
            this.MyCarrier.Name = "MyCarrier";
            this.MyCarrier.Size = new System.Drawing.Size(587, 335);
            this.MyCarrier.TabIndex = 2;
            this.MyCarrier.Text = "MyCarrier";
            this.MyCarrier.UseVisualStyleBackColor = true;
            // 
            // label_carrier_scac_code
            // 
            this.label_carrier_scac_code.AutoSize = true;
            this.label_carrier_scac_code.Location = new System.Drawing.Point(447, 63);
            this.label_carrier_scac_code.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_carrier_scac_code.Name = "label_carrier_scac_code";
            this.label_carrier_scac_code.Size = new System.Drawing.Size(95, 13);
            this.label_carrier_scac_code.TabIndex = 8;
            this.label_carrier_scac_code.Text = "Carrier SCAC code";
            // 
            // label_carrier_name
            // 
            this.label_carrier_name.AutoSize = true;
            this.label_carrier_name.Location = new System.Drawing.Point(447, 17);
            this.label_carrier_name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_carrier_name.Name = "label_carrier_name";
            this.label_carrier_name.Size = new System.Drawing.Size(68, 13);
            this.label_carrier_name.TabIndex = 7;
            this.label_carrier_name.Text = "Carrier Name";
            // 
            // txt_carrier_scac_code
            // 
            this.txt_carrier_scac_code.Location = new System.Drawing.Point(442, 82);
            this.txt_carrier_scac_code.Margin = new System.Windows.Forms.Padding(2);
            this.txt_carrier_scac_code.Name = "txt_carrier_scac_code";
            this.txt_carrier_scac_code.Size = new System.Drawing.Size(125, 20);
            this.txt_carrier_scac_code.TabIndex = 6;
            this.txt_carrier_scac_code.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_carrier_scac_code_KeyPress);
            // 
            // checkbox_default
            // 
            this.checkbox_default.AutoSize = true;
            this.checkbox_default.Location = new System.Drawing.Point(442, 145);
            this.checkbox_default.Margin = new System.Windows.Forms.Padding(2);
            this.checkbox_default.Name = "checkbox_default";
            this.checkbox_default.Size = new System.Drawing.Size(60, 17);
            this.checkbox_default.TabIndex = 5;
            this.checkbox_default.Text = "Default";
            this.checkbox_default.UseVisualStyleBackColor = true;
            this.checkbox_default.CheckedChanged += new System.EventHandler(this.checkbox_default_CheckedChanged);
            // 
            // txt_carrier_name
            // 
            this.txt_carrier_name.Location = new System.Drawing.Point(442, 38);
            this.txt_carrier_name.Margin = new System.Windows.Forms.Padding(2);
            this.txt_carrier_name.Name = "txt_carrier_name";
            this.txt_carrier_name.Size = new System.Drawing.Size(125, 20);
            this.txt_carrier_name.TabIndex = 4;
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(447, 110);
            this.btn_delete.Margin = new System.Windows.Forms.Padding(2);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(56, 19);
            this.btn_delete.TabIndex = 3;
            this.btn_delete.Text = "Delete";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(508, 110);
            this.btn_add.Margin = new System.Windows.Forms.Padding(2);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(56, 19);
            this.btn_add.TabIndex = 2;
            this.btn_add.Text = "Add";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // listbox_carriers
            // 
            this.listbox_carriers.FormattingEnabled = true;
            this.listbox_carriers.Location = new System.Drawing.Point(13, 11);
            this.listbox_carriers.Margin = new System.Windows.Forms.Padding(2);
            this.listbox_carriers.Name = "listbox_carriers";
            this.listbox_carriers.Size = new System.Drawing.Size(415, 316);
            this.listbox_carriers.TabIndex = 1;
            this.listbox_carriers.SelectedIndexChanged += new System.EventHandler(this.listbox_carriers_SelectedIndexChanged);
            // 
            // btn_edit_mycarrier
            // 
            this.btn_edit_mycarrier.Location = new System.Drawing.Point(434, 301);
            this.btn_edit_mycarrier.Name = "btn_edit_mycarrier";
            this.btn_edit_mycarrier.Size = new System.Drawing.Size(142, 23);
            this.btn_edit_mycarrier.TabIndex = 0;
            this.btn_edit_mycarrier.Text = "Edit MyCarrier Settings";
            this.btn_edit_mycarrier.UseVisualStyleBackColor = true;
            this.btn_edit_mycarrier.Click += new System.EventHandler(this.btn_edit_mycarrier_Click);
            // 
            // backup
            // 
            this.backup.Controls.Add(this.label10);
            this.backup.Controls.Add(this.delete_hour);
            this.backup.Location = new System.Drawing.Point(4, 22);
            this.backup.Name = "backup";
            this.backup.Padding = new System.Windows.Forms.Padding(3);
            this.backup.Size = new System.Drawing.Size(587, 335);
            this.backup.TabIndex = 3;
            this.backup.Text = "Backup";
            this.backup.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Delete hour";
            // 
            // delete_hour
            // 
            this.delete_hour.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.delete_hour.Location = new System.Drawing.Point(105, 58);
            this.delete_hour.Name = "delete_hour";
            this.delete_hour.Size = new System.Drawing.Size(200, 20);
            this.delete_hour.TabIndex = 0;
            this.delete_hour.Value = new System.DateTime(2013, 10, 14, 16, 13, 52, 0);
            // 
            // emails
            // 
            this.emails.Controls.Add(this.dataGridView1);
            this.emails.Location = new System.Drawing.Point(4, 22);
            this.emails.Name = "emails";
            this.emails.Size = new System.Drawing.Size(587, 335);
            this.emails.TabIndex = 4;
            this.emails.Text = "Emails";
            this.emails.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(587, 335);
            this.dataGridView1.TabIndex = 0;
            // 
            // PackageDimensions
            // 
            this.PackageDimensions.Controls.Add(this.btnSavePackageDims);
            this.PackageDimensions.Controls.Add(this.btnAddPackageDim);
            this.PackageDimensions.Controls.Add(this.tbPackageHeight);
            this.PackageDimensions.Controls.Add(this.label93);
            this.PackageDimensions.Controls.Add(this.tbPackageWidth);
            this.PackageDimensions.Controls.Add(this.label92);
            this.PackageDimensions.Controls.Add(this.tbPackageLength);
            this.PackageDimensions.Controls.Add(this.label91);
            this.PackageDimensions.Controls.Add(this.tbPackageDimName);
            this.PackageDimensions.Controls.Add(this.label90);
            this.PackageDimensions.Controls.Add(this.tbPackageDimCode);
            this.PackageDimensions.Controls.Add(this.label89);
            this.PackageDimensions.Controls.Add(this.label88);
            this.PackageDimensions.Controls.Add(this.cbpackageDimensions);
            this.PackageDimensions.Location = new System.Drawing.Point(4, 22);
            this.PackageDimensions.Name = "PackageDimensions";
            this.PackageDimensions.Size = new System.Drawing.Size(587, 335);
            this.PackageDimensions.TabIndex = 6;
            this.PackageDimensions.Text = "Package Dimensions";
            this.PackageDimensions.UseVisualStyleBackColor = true;
            // 
            // btnSavePackageDims
            // 
            this.btnSavePackageDims.Location = new System.Drawing.Point(445, 244);
            this.btnSavePackageDims.Name = "btnSavePackageDims";
            this.btnSavePackageDims.Size = new System.Drawing.Size(75, 23);
            this.btnSavePackageDims.TabIndex = 13;
            this.btnSavePackageDims.Text = "Save";
            this.btnSavePackageDims.UseVisualStyleBackColor = true;
            this.btnSavePackageDims.Click += new System.EventHandler(this.btnSavePackageDims_Click);
            // 
            // btnAddPackageDim
            // 
            this.btnAddPackageDim.Location = new System.Drawing.Point(345, 245);
            this.btnAddPackageDim.Name = "btnAddPackageDim";
            this.btnAddPackageDim.Size = new System.Drawing.Size(82, 23);
            this.btnAddPackageDim.TabIndex = 12;
            this.btnAddPackageDim.Text = "Add ";
            this.btnAddPackageDim.UseVisualStyleBackColor = true;
            this.btnAddPackageDim.Click += new System.EventHandler(this.btnAddPackageDim_Click);
            // 
            // tbPackageHeight
            // 
            this.tbPackageHeight.Location = new System.Drawing.Point(152, 249);
            this.tbPackageHeight.Name = "tbPackageHeight";
            this.tbPackageHeight.Size = new System.Drawing.Size(100, 20);
            this.tbPackageHeight.TabIndex = 11;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(17, 249);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(38, 13);
            this.label93.TabIndex = 10;
            this.label93.Text = "Height";
            // 
            // tbPackageWidth
            // 
            this.tbPackageWidth.Location = new System.Drawing.Point(152, 208);
            this.tbPackageWidth.Name = "tbPackageWidth";
            this.tbPackageWidth.Size = new System.Drawing.Size(100, 20);
            this.tbPackageWidth.TabIndex = 9;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(17, 215);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(35, 13);
            this.label92.TabIndex = 8;
            this.label92.Text = "Width";
            // 
            // tbPackageLength
            // 
            this.tbPackageLength.Location = new System.Drawing.Point(152, 170);
            this.tbPackageLength.Name = "tbPackageLength";
            this.tbPackageLength.Size = new System.Drawing.Size(100, 20);
            this.tbPackageLength.TabIndex = 7;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(17, 177);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(40, 13);
            this.label91.TabIndex = 6;
            this.label91.Text = "Length";
            // 
            // tbPackageDimName
            // 
            this.tbPackageDimName.Location = new System.Drawing.Point(152, 132);
            this.tbPackageDimName.Name = "tbPackageDimName";
            this.tbPackageDimName.Size = new System.Drawing.Size(158, 20);
            this.tbPackageDimName.TabIndex = 5;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(17, 135);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(35, 13);
            this.label90.TabIndex = 4;
            this.label90.Text = "Name";
            // 
            // tbPackageDimCode
            // 
            this.tbPackageDimCode.Location = new System.Drawing.Point(152, 88);
            this.tbPackageDimCode.Name = "tbPackageDimCode";
            this.tbPackageDimCode.Size = new System.Drawing.Size(158, 20);
            this.tbPackageDimCode.TabIndex = 3;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(17, 88);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(32, 13);
            this.label89.TabIndex = 2;
            this.label89.Text = "Code";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(17, 45);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(102, 13);
            this.label88.TabIndex = 1;
            this.label88.Text = "Package Dimension";
            // 
            // cbpackageDimensions
            // 
            this.cbpackageDimensions.DisplayMember = "PackageDimCode";
            this.cbpackageDimensions.FormattingEnabled = true;
            this.cbpackageDimensions.Location = new System.Drawing.Point(152, 42);
            this.cbpackageDimensions.Name = "cbpackageDimensions";
            this.cbpackageDimensions.Size = new System.Drawing.Size(158, 21);
            this.cbpackageDimensions.TabIndex = 0;
            this.cbpackageDimensions.ValueMember = "PackageDimCode";
            this.cbpackageDimensions.SelectedIndexChanged += new System.EventHandler(this.cbpackageDimensions_SelectedIndexChanged);
            // 
            // ShipmentOptions
            // 
            this.ShipmentOptions.Controls.Add(this.pFedEx);
            this.ShipmentOptions.Location = new System.Drawing.Point(4, 22);
            this.ShipmentOptions.Name = "ShipmentOptions";
            this.ShipmentOptions.Size = new System.Drawing.Size(587, 335);
            this.ShipmentOptions.TabIndex = 5;
            this.ShipmentOptions.Text = "Shipment Options";
            this.ShipmentOptions.UseVisualStyleBackColor = true;
            // 
            // pFedEx
            // 
            this.pFedEx.Controls.Add(this.tFedExShipOptions);
            this.pFedEx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pFedEx.Location = new System.Drawing.Point(0, 0);
            this.pFedEx.Name = "pFedEx";
            this.pFedEx.Size = new System.Drawing.Size(587, 335);
            this.pFedEx.TabIndex = 0;
            // 
            // tFedExShipOptions
            // 
            this.tFedExShipOptions.Controls.Add(this.FedExShipmentOptions);
            this.tFedExShipOptions.Controls.Add(this.PuroShipmentOptions);
            this.tFedExShipOptions.Controls.Add(this.LoomisShipmentOptions);
            this.tFedExShipOptions.Controls.Add(this.UPSShipmentOptions);
            this.tFedExShipOptions.Controls.Add(this.CAPostShipmentOptions);
            this.tFedExShipOptions.Controls.Add(this.CanparShipmentOptions);
            this.tFedExShipOptions.Controls.Add(this.DicomShipmentOptions);
            this.tFedExShipOptions.Controls.Add(this.PackageOptions);
            this.tFedExShipOptions.Controls.Add(this.Loomis30ShipOptions);
            this.tFedExShipOptions.Controls.Add(this.ICSShipOptions);
            this.tFedExShipOptions.Controls.Add(this.NationexShipOptions);
            this.tFedExShipOptions.Controls.Add(this.ATSShipOptions);
            this.tFedExShipOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.tFedExShipOptions.Location = new System.Drawing.Point(0, 0);
            this.tFedExShipOptions.Name = "tFedExShipOptions";
            this.tFedExShipOptions.SelectedIndex = 0;
            this.tFedExShipOptions.Size = new System.Drawing.Size(587, 332);
            this.tFedExShipOptions.TabIndex = 0;
            // 
            // FedExShipmentOptions
            // 
            this.FedExShipmentOptions.AutoScroll = true;
            this.FedExShipmentOptions.Controls.Add(this.SignatureOptionPanel);
            this.FedExShipmentOptions.Controls.Add(this.cbSignatureOption);
            this.FedExShipmentOptions.Controls.Add(this.HomeDeliveryPanel);
            this.FedExShipmentOptions.Controls.Add(this.cbHomeDelivery);
            this.FedExShipmentOptions.Controls.Add(this.cbSaturdayPickup);
            this.FedExShipmentOptions.Controls.Add(this.cbSaturdayDelivery);
            this.FedExShipmentOptions.Controls.Add(this.cbInsidePickup);
            this.FedExShipmentOptions.Controls.Add(this.cbInsideDelivery);
            this.FedExShipmentOptions.Controls.Add(this.DryIcePanel);
            this.FedExShipmentOptions.Controls.Add(this.cbDryIce);
            this.FedExShipmentOptions.Controls.Add(this.HoldAtLocationPanel);
            this.FedExShipmentOptions.Controls.Add(this.cbHoldAtLocation);
            this.FedExShipmentOptions.Controls.Add(this.CODPanel);
            this.FedExShipmentOptions.Controls.Add(this.cbCOD);
            this.FedExShipmentOptions.Location = new System.Drawing.Point(4, 22);
            this.FedExShipmentOptions.Name = "FedExShipmentOptions";
            this.FedExShipmentOptions.Padding = new System.Windows.Forms.Padding(3);
            this.FedExShipmentOptions.Size = new System.Drawing.Size(579, 306);
            this.FedExShipmentOptions.TabIndex = 0;
            this.FedExShipmentOptions.Text = "Shipment Options";
            this.FedExShipmentOptions.UseVisualStyleBackColor = true;
            // 
            // SignatureOptionPanel
            // 
            this.SignatureOptionPanel.Controls.Add(this.tbReleaseNumber);
            this.SignatureOptionPanel.Controls.Add(this.label43);
            this.SignatureOptionPanel.Controls.Add(this.cbSignatureRequired);
            this.SignatureOptionPanel.Controls.Add(this.label42);
            this.SignatureOptionPanel.Location = new System.Drawing.Point(3, 1171);
            this.SignatureOptionPanel.Name = "SignatureOptionPanel";
            this.SignatureOptionPanel.Size = new System.Drawing.Size(552, 71);
            this.SignatureOptionPanel.TabIndex = 13;
            // 
            // tbReleaseNumber
            // 
            this.tbReleaseNumber.Location = new System.Drawing.Point(112, 41);
            this.tbReleaseNumber.Name = "tbReleaseNumber";
            this.tbReleaseNumber.Size = new System.Drawing.Size(220, 20);
            this.tbReleaseNumber.TabIndex = 3;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(13, 40);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(87, 13);
            this.label43.TabIndex = 2;
            this.label43.Text = "Release number:";
            // 
            // cbSignatureRequired
            // 
            this.cbSignatureRequired.FormattingEnabled = true;
            this.cbSignatureRequired.Items.AddRange(new object[] {
            "SERVICE_DEFAULT",
            "NO_SIGNATURE_REQUIRED",
            "ADULT",
            "DIRECT",
            "INDIRECT"});
            this.cbSignatureRequired.Location = new System.Drawing.Point(112, 13);
            this.cbSignatureRequired.Name = "cbSignatureRequired";
            this.cbSignatureRequired.Size = new System.Drawing.Size(220, 21);
            this.cbSignatureRequired.TabIndex = 1;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(10, 13);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(95, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "Select your option:";
            // 
            // cbSignatureOption
            // 
            this.cbSignatureOption.AutoSize = true;
            this.cbSignatureOption.Location = new System.Drawing.Point(7, 1148);
            this.cbSignatureOption.Name = "cbSignatureOption";
            this.cbSignatureOption.Size = new System.Drawing.Size(105, 17);
            this.cbSignatureOption.TabIndex = 12;
            this.cbSignatureOption.Text = "Signature Option";
            this.cbSignatureOption.UseVisualStyleBackColor = true;
            this.cbSignatureOption.CheckedChanged += new System.EventHandler(this.cbSignatureOption_CheckedChanged);
            // 
            // HomeDeliveryPanel
            // 
            this.HomeDeliveryPanel.Controls.Add(this.tbHomeDeliveryPhone);
            this.HomeDeliveryPanel.Controls.Add(this.label41);
            this.HomeDeliveryPanel.Controls.Add(this.dtpHomeDeliveryDate);
            this.HomeDeliveryPanel.Controls.Add(this.label40);
            this.HomeDeliveryPanel.Controls.Add(this.cbHomeDeliveryType);
            this.HomeDeliveryPanel.Controls.Add(this.label39);
            this.HomeDeliveryPanel.Location = new System.Drawing.Point(3, 1070);
            this.HomeDeliveryPanel.Name = "HomeDeliveryPanel";
            this.HomeDeliveryPanel.Size = new System.Drawing.Size(552, 72);
            this.HomeDeliveryPanel.TabIndex = 11;
            // 
            // tbHomeDeliveryPhone
            // 
            this.tbHomeDeliveryPhone.Location = new System.Drawing.Point(105, 41);
            this.tbHomeDeliveryPhone.Name = "tbHomeDeliveryPhone";
            this.tbHomeDeliveryPhone.Size = new System.Drawing.Size(177, 20);
            this.tbHomeDeliveryPhone.TabIndex = 6;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(7, 39);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(38, 13);
            this.label41.TabIndex = 5;
            this.label41.Text = "Phone";
            // 
            // dtpHomeDeliveryDate
            // 
            this.dtpHomeDeliveryDate.Location = new System.Drawing.Point(338, 13);
            this.dtpHomeDeliveryDate.Name = "dtpHomeDeliveryDate";
            this.dtpHomeDeliveryDate.Size = new System.Drawing.Size(200, 20);
            this.dtpHomeDeliveryDate.TabIndex = 4;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(302, 16);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(30, 13);
            this.label40.TabIndex = 2;
            this.label40.Text = "Date";
            // 
            // cbHomeDeliveryType
            // 
            this.cbHomeDeliveryType.FormattingEnabled = true;
            this.cbHomeDeliveryType.Items.AddRange(new object[] {
            "APPOINTMENT",
            "EVENING",
            "DATE_CERTAIN"});
            this.cbHomeDeliveryType.Location = new System.Drawing.Point(105, 13);
            this.cbHomeDeliveryType.Name = "cbHomeDeliveryType";
            this.cbHomeDeliveryType.Size = new System.Drawing.Size(177, 21);
            this.cbHomeDeliveryType.TabIndex = 1;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(4, 13);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(95, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Select your option:";
            // 
            // cbHomeDelivery
            // 
            this.cbHomeDelivery.AutoSize = true;
            this.cbHomeDelivery.Location = new System.Drawing.Point(7, 1047);
            this.cbHomeDelivery.Name = "cbHomeDelivery";
            this.cbHomeDelivery.Size = new System.Drawing.Size(293, 17);
            this.cbHomeDelivery.TabIndex = 10;
            this.cbHomeDelivery.Text = "Home Delivery Premium (Only for Ground Home Delivery)";
            this.cbHomeDelivery.UseVisualStyleBackColor = true;
            this.cbHomeDelivery.CheckedChanged += new System.EventHandler(this.cbHomeDelivery_CheckedChanged);
            // 
            // cbSaturdayPickup
            // 
            this.cbSaturdayPickup.AutoSize = true;
            this.cbSaturdayPickup.Location = new System.Drawing.Point(7, 1024);
            this.cbSaturdayPickup.Name = "cbSaturdayPickup";
            this.cbSaturdayPickup.Size = new System.Drawing.Size(104, 17);
            this.cbSaturdayPickup.TabIndex = 9;
            this.cbSaturdayPickup.Text = "Saturday Pickup";
            this.cbSaturdayPickup.UseVisualStyleBackColor = true;
            // 
            // cbSaturdayDelivery
            // 
            this.cbSaturdayDelivery.AutoSize = true;
            this.cbSaturdayDelivery.Location = new System.Drawing.Point(7, 1001);
            this.cbSaturdayDelivery.Name = "cbSaturdayDelivery";
            this.cbSaturdayDelivery.Size = new System.Drawing.Size(109, 17);
            this.cbSaturdayDelivery.TabIndex = 8;
            this.cbSaturdayDelivery.Text = "Saturday Delivery";
            this.cbSaturdayDelivery.UseVisualStyleBackColor = true;
            // 
            // cbInsidePickup
            // 
            this.cbInsidePickup.AutoSize = true;
            this.cbInsidePickup.Location = new System.Drawing.Point(7, 978);
            this.cbInsidePickup.Name = "cbInsidePickup";
            this.cbInsidePickup.Size = new System.Drawing.Size(209, 17);
            this.cbInsidePickup.TabIndex = 7;
            this.cbInsidePickup.Text = "Inside Pickup (Olny for freight services)";
            this.cbInsidePickup.UseVisualStyleBackColor = true;
            // 
            // cbInsideDelivery
            // 
            this.cbInsideDelivery.AutoSize = true;
            this.cbInsideDelivery.Location = new System.Drawing.Point(7, 955);
            this.cbInsideDelivery.Name = "cbInsideDelivery";
            this.cbInsideDelivery.Size = new System.Drawing.Size(214, 17);
            this.cbInsideDelivery.TabIndex = 6;
            this.cbInsideDelivery.Text = "Inside Delivery (Olny for freight services)";
            this.cbInsideDelivery.UseVisualStyleBackColor = true;
            // 
            // DryIcePanel
            // 
            this.DryIcePanel.Controls.Add(this.label38);
            this.DryIcePanel.Controls.Add(this.cbDryIceUnit);
            this.DryIcePanel.Controls.Add(this.tbDryIceWeight);
            this.DryIcePanel.Controls.Add(this.label37);
            this.DryIcePanel.Location = new System.Drawing.Point(3, 845);
            this.DryIcePanel.Name = "DryIcePanel";
            this.DryIcePanel.Size = new System.Drawing.Size(552, 104);
            this.DryIcePanel.TabIndex = 5;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(13, 80);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(269, 13);
            this.label38.TabIndex = 3;
            this.label38.Text = "Note: The total weight should include the dry ice weight";
            // 
            // cbDryIceUnit
            // 
            this.cbDryIceUnit.FormattingEnabled = true;
            this.cbDryIceUnit.Items.AddRange(new object[] {
            "KGS",
            "LBS"});
            this.cbDryIceUnit.Location = new System.Drawing.Point(14, 52);
            this.cbDryIceUnit.Name = "cbDryIceUnit";
            this.cbDryIceUnit.Size = new System.Drawing.Size(247, 21);
            this.cbDryIceUnit.TabIndex = 2;
            // 
            // tbDryIceWeight
            // 
            this.tbDryIceWeight.Location = new System.Drawing.Point(13, 25);
            this.tbDryIceWeight.Name = "tbDryIceWeight";
            this.tbDryIceWeight.Size = new System.Drawing.Size(248, 20);
            this.tbDryIceWeight.TabIndex = 1;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(10, 9);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(75, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Dry Ice weight";
            // 
            // cbDryIce
            // 
            this.cbDryIce.AutoSize = true;
            this.cbDryIce.Location = new System.Drawing.Point(6, 822);
            this.cbDryIce.Name = "cbDryIce";
            this.cbDryIce.Size = new System.Drawing.Size(60, 17);
            this.cbDryIce.TabIndex = 4;
            this.cbDryIce.Text = "Dry Ice";
            this.cbDryIce.UseVisualStyleBackColor = true;
            this.cbDryIce.CheckedChanged += new System.EventHandler(this.cbDryIce_CheckedChanged);
            // 
            // HoldAtLocationPanel
            // 
            this.HoldAtLocationPanel.AutoScroll = true;
            this.HoldAtLocationPanel.Controls.Add(this.tbHoldEmail);
            this.HoldAtLocationPanel.Controls.Add(this.tbHoldPhone);
            this.HoldAtLocationPanel.Controls.Add(this.label36);
            this.HoldAtLocationPanel.Controls.Add(this.label35);
            this.HoldAtLocationPanel.Controls.Add(this.tbHoldAddress2);
            this.HoldAtLocationPanel.Controls.Add(this.label34);
            this.HoldAtLocationPanel.Controls.Add(this.tbHoldAddress1);
            this.HoldAtLocationPanel.Controls.Add(this.label33);
            this.HoldAtLocationPanel.Controls.Add(this.tbHoldZIP);
            this.HoldAtLocationPanel.Controls.Add(this.label32);
            this.HoldAtLocationPanel.Controls.Add(this.tbHoldCity);
            this.HoldAtLocationPanel.Controls.Add(this.label31);
            this.HoldAtLocationPanel.Controls.Add(this.cbHoldState);
            this.HoldAtLocationPanel.Controls.Add(this.label30);
            this.HoldAtLocationPanel.Controls.Add(this.cbHoldCountry);
            this.HoldAtLocationPanel.Controls.Add(this.label29);
            this.HoldAtLocationPanel.Controls.Add(this.tbHoldCompany);
            this.HoldAtLocationPanel.Controls.Add(this.label28);
            this.HoldAtLocationPanel.Controls.Add(this.tbHoldContact);
            this.HoldAtLocationPanel.Controls.Add(this.label27);
            this.HoldAtLocationPanel.Controls.Add(this.label26);
            this.HoldAtLocationPanel.Location = new System.Drawing.Point(6, 505);
            this.HoldAtLocationPanel.Name = "HoldAtLocationPanel";
            this.HoldAtLocationPanel.Size = new System.Drawing.Size(558, 311);
            this.HoldAtLocationPanel.TabIndex = 3;
            // 
            // tbHoldEmail
            // 
            this.tbHoldEmail.Location = new System.Drawing.Point(124, 278);
            this.tbHoldEmail.Name = "tbHoldEmail";
            this.tbHoldEmail.Size = new System.Drawing.Size(181, 20);
            this.tbHoldEmail.TabIndex = 20;
            // 
            // tbHoldPhone
            // 
            this.tbHoldPhone.Location = new System.Drawing.Point(124, 251);
            this.tbHoldPhone.Name = "tbHoldPhone";
            this.tbHoldPhone.Size = new System.Drawing.Size(181, 20);
            this.tbHoldPhone.TabIndex = 19;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(16, 283);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(32, 13);
            this.label36.TabIndex = 18;
            this.label36.Text = "Email";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(16, 259);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(38, 13);
            this.label35.TabIndex = 17;
            this.label35.Text = "Phone";
            // 
            // tbHoldAddress2
            // 
            this.tbHoldAddress2.Location = new System.Drawing.Point(124, 224);
            this.tbHoldAddress2.Name = "tbHoldAddress2";
            this.tbHoldAddress2.Size = new System.Drawing.Size(181, 20);
            this.tbHoldAddress2.TabIndex = 16;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(16, 231);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(54, 13);
            this.label34.TabIndex = 15;
            this.label34.Text = "Address 2";
            // 
            // tbHoldAddress1
            // 
            this.tbHoldAddress1.Location = new System.Drawing.Point(124, 197);
            this.tbHoldAddress1.Name = "tbHoldAddress1";
            this.tbHoldAddress1.Size = new System.Drawing.Size(181, 20);
            this.tbHoldAddress1.TabIndex = 14;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(17, 205);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(54, 13);
            this.label33.TabIndex = 13;
            this.label33.Text = "Address 1";
            // 
            // tbHoldZIP
            // 
            this.tbHoldZIP.Location = new System.Drawing.Point(124, 168);
            this.tbHoldZIP.Name = "tbHoldZIP";
            this.tbHoldZIP.Size = new System.Drawing.Size(181, 20);
            this.tbHoldZIP.TabIndex = 12;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(16, 171);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(92, 13);
            this.label32.TabIndex = 11;
            this.label32.Text = "ZIP / Postal Code";
            // 
            // tbHoldCity
            // 
            this.tbHoldCity.Location = new System.Drawing.Point(124, 140);
            this.tbHoldCity.Name = "tbHoldCity";
            this.tbHoldCity.Size = new System.Drawing.Size(181, 20);
            this.tbHoldCity.TabIndex = 10;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(17, 143);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(24, 13);
            this.label31.TabIndex = 9;
            this.label31.Text = "City";
            // 
            // cbHoldState
            // 
            this.cbHoldState.FormattingEnabled = true;
            this.cbHoldState.Location = new System.Drawing.Point(124, 112);
            this.cbHoldState.Name = "cbHoldState";
            this.cbHoldState.Size = new System.Drawing.Size(181, 21);
            this.cbHoldState.TabIndex = 8;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(17, 111);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(85, 13);
            this.label30.TabIndex = 7;
            this.label30.Text = "State / Province";
            // 
            // cbHoldCountry
            // 
            this.cbHoldCountry.FormattingEnabled = true;
            this.cbHoldCountry.Location = new System.Drawing.Point(124, 84);
            this.cbHoldCountry.Name = "cbHoldCountry";
            this.cbHoldCountry.Size = new System.Drawing.Size(181, 21);
            this.cbHoldCountry.TabIndex = 6;
            this.cbHoldCountry.SelectedIndexChanged += new System.EventHandler(this.cbHoldCountry_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(17, 84);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 13);
            this.label29.TabIndex = 5;
            this.label29.Text = "Country";
            // 
            // tbHoldCompany
            // 
            this.tbHoldCompany.Location = new System.Drawing.Point(124, 54);
            this.tbHoldCompany.Name = "tbHoldCompany";
            this.tbHoldCompany.Size = new System.Drawing.Size(181, 20);
            this.tbHoldCompany.TabIndex = 4;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(17, 54);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Company";
            // 
            // tbHoldContact
            // 
            this.tbHoldContact.Location = new System.Drawing.Point(124, 28);
            this.tbHoldContact.Name = "tbHoldContact";
            this.tbHoldContact.Size = new System.Drawing.Size(181, 20);
            this.tbHoldContact.TabIndex = 2;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(17, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "Contact";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(11, 4);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(238, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "The addess where your package should be held:";
            // 
            // cbHoldAtLocation
            // 
            this.cbHoldAtLocation.AutoSize = true;
            this.cbHoldAtLocation.Location = new System.Drawing.Point(7, 482);
            this.cbHoldAtLocation.Name = "cbHoldAtLocation";
            this.cbHoldAtLocation.Size = new System.Drawing.Size(138, 17);
            this.cbHoldAtLocation.TabIndex = 2;
            this.cbHoldAtLocation.Text = "Hold At FedEx Location";
            this.cbHoldAtLocation.UseVisualStyleBackColor = true;
            this.cbHoldAtLocation.CheckedChanged += new System.EventHandler(this.cbHoldAtLocation_CheckedChanged);
            // 
            // CODPanel
            // 
            this.CODPanel.AutoScroll = true;
            this.CODPanel.AutoSize = true;
            this.CODPanel.Controls.Add(this.tbReturnTel);
            this.CODPanel.Controls.Add(this.label25);
            this.CODPanel.Controls.Add(this.tbZIP);
            this.CODPanel.Controls.Add(this.label24);
            this.CODPanel.Controls.Add(this.tbCity);
            this.CODPanel.Controls.Add(this.label23);
            this.CODPanel.Controls.Add(this.cbStateProvince);
            this.CODPanel.Controls.Add(this.label22);
            this.CODPanel.Controls.Add(this.cbCountry);
            this.CODPanel.Controls.Add(this.label21);
            this.CODPanel.Controls.Add(this.tbReturnAddress2);
            this.CODPanel.Controls.Add(this.label20);
            this.CODPanel.Controls.Add(this.tbReturnAddress1);
            this.CODPanel.Controls.Add(this.label19);
            this.CODPanel.Controls.Add(this.tbReturnCompany);
            this.CODPanel.Controls.Add(this.label18);
            this.CODPanel.Controls.Add(this.tbReturnContact);
            this.CODPanel.Controls.Add(this.label17);
            this.CODPanel.Controls.Add(this.label16);
            this.CODPanel.Controls.Add(this.cbReferenceIndicator);
            this.CODPanel.Controls.Add(this.label15);
            this.CODPanel.Controls.Add(this.cbTranspCharges);
            this.CODPanel.Controls.Add(this.label14);
            this.CODPanel.Controls.Add(this.cbFedExCollectionType);
            this.CODPanel.Controls.Add(this.label13);
            this.CODPanel.Controls.Add(this.cbFedExCurrency);
            this.CODPanel.Controls.Add(this.label12);
            this.CODPanel.Controls.Add(this.tbFedExAmount);
            this.CODPanel.Controls.Add(this.label11);
            this.CODPanel.Location = new System.Drawing.Point(7, 30);
            this.CODPanel.Name = "CODPanel";
            this.CODPanel.Size = new System.Drawing.Size(558, 446);
            this.CODPanel.TabIndex = 1;
            // 
            // tbReturnTel
            // 
            this.tbReturnTel.Location = new System.Drawing.Point(123, 419);
            this.tbReturnTel.Name = "tbReturnTel";
            this.tbReturnTel.Size = new System.Drawing.Size(181, 20);
            this.tbReturnTel.TabIndex = 28;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(13, 427);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(25, 13);
            this.label25.TabIndex = 27;
            this.label25.Text = "Tel.";
            // 
            // tbZIP
            // 
            this.tbZIP.Location = new System.Drawing.Point(123, 393);
            this.tbZIP.Name = "tbZIP";
            this.tbZIP.Size = new System.Drawing.Size(181, 20);
            this.tbZIP.TabIndex = 26;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 401);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 13);
            this.label24.TabIndex = 25;
            this.label24.Text = "ZIP / Postal Code";
            // 
            // tbCity
            // 
            this.tbCity.Location = new System.Drawing.Point(123, 366);
            this.tbCity.Name = "tbCity";
            this.tbCity.Size = new System.Drawing.Size(181, 20);
            this.tbCity.TabIndex = 24;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(18, 374);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(24, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "City";
            // 
            // cbStateProvince
            // 
            this.cbStateProvince.FormattingEnabled = true;
            this.cbStateProvince.Location = new System.Drawing.Point(123, 340);
            this.cbStateProvince.Name = "cbStateProvince";
            this.cbStateProvince.Size = new System.Drawing.Size(181, 21);
            this.cbStateProvince.TabIndex = 22;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(13, 338);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(85, 13);
            this.label22.TabIndex = 21;
            this.label22.Text = "State / Province";
            // 
            // cbCountry
            // 
            this.cbCountry.FormattingEnabled = true;
            this.cbCountry.Location = new System.Drawing.Point(123, 312);
            this.cbCountry.Name = "cbCountry";
            this.cbCountry.Size = new System.Drawing.Size(181, 21);
            this.cbCountry.TabIndex = 20;
            this.cbCountry.SelectedIndexChanged += new System.EventHandler(this.cbCountry_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 312);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = "Country";
            // 
            // tbReturnAddress2
            // 
            this.tbReturnAddress2.Location = new System.Drawing.Point(123, 285);
            this.tbReturnAddress2.Name = "tbReturnAddress2";
            this.tbReturnAddress2.Size = new System.Drawing.Size(181, 20);
            this.tbReturnAddress2.TabIndex = 18;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 285);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(51, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "Address2";
            // 
            // tbReturnAddress1
            // 
            this.tbReturnAddress1.Location = new System.Drawing.Point(123, 258);
            this.tbReturnAddress1.Name = "tbReturnAddress1";
            this.tbReturnAddress1.Size = new System.Drawing.Size(181, 20);
            this.tbReturnAddress1.TabIndex = 16;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 258);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "Address1";
            // 
            // tbReturnCompany
            // 
            this.tbReturnCompany.Location = new System.Drawing.Point(123, 230);
            this.tbReturnCompany.Name = "tbReturnCompany";
            this.tbReturnCompany.Size = new System.Drawing.Size(181, 20);
            this.tbReturnCompany.TabIndex = 14;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 230);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Company";
            // 
            // tbReturnContact
            // 
            this.tbReturnContact.Location = new System.Drawing.Point(123, 201);
            this.tbReturnContact.Name = "tbReturnContact";
            this.tbReturnContact.Size = new System.Drawing.Size(181, 20);
            this.tbReturnContact.TabIndex = 12;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 201);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 13);
            this.label17.TabIndex = 11;
            this.label17.Text = "Contact";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(13, 173);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(137, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Collect Return Address";
            // 
            // cbReferenceIndicator
            // 
            this.cbReferenceIndicator.FormattingEnabled = true;
            this.cbReferenceIndicator.Items.AddRange(new object[] {
            "INVOICE",
            "PO",
            "REFERENCE",
            "TRACKING"});
            this.cbReferenceIndicator.Location = new System.Drawing.Point(123, 141);
            this.cbReferenceIndicator.Name = "cbReferenceIndicator";
            this.cbReferenceIndicator.Size = new System.Drawing.Size(181, 21);
            this.cbReferenceIndicator.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(101, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "Reference Indicator";
            // 
            // cbTranspCharges
            // 
            this.cbTranspCharges.FormattingEnabled = true;
            this.cbTranspCharges.Items.AddRange(new object[] {
            "ADD_ACCOUNT_COD_SURCHARGE",
            "ADD_ACCOUNT_NET_CHARGE",
            "ADD_ACCOUNT_NET_FREIGHT",
            "ADD_ACCOUNT_TOTAL_CUSTOMER_CHARGE",
            "ADD_LIST_COD_SURCHARGE",
            "ADD_LIST_NET_CHARGE",
            "ADD_LIST_NET_FREIGHT",
            "ADD_LIST_TOTAL_CUSTOMER_CHARGE"});
            this.cbTranspCharges.Location = new System.Drawing.Point(123, 106);
            this.cbTranspCharges.Name = "cbTranspCharges";
            this.cbTranspCharges.Size = new System.Drawing.Size(181, 21);
            this.cbTranspCharges.TabIndex = 7;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(113, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Freight charges to add";
            // 
            // cbFedExCollectionType
            // 
            this.cbFedExCollectionType.FormattingEnabled = true;
            this.cbFedExCollectionType.Items.AddRange(new object[] {
            "ANY",
            "CASH",
            "GUARANTEED_FUNDS"});
            this.cbFedExCollectionType.Location = new System.Drawing.Point(123, 75);
            this.cbFedExCollectionType.Name = "cbFedExCollectionType";
            this.cbFedExCollectionType.Size = new System.Drawing.Size(181, 21);
            this.cbFedExCollectionType.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 78);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Collection Type";
            // 
            // cbFedExCurrency
            // 
            this.cbFedExCurrency.FormattingEnabled = true;
            this.cbFedExCurrency.Items.AddRange(new object[] {
            "USD",
            "CAD"});
            this.cbFedExCurrency.Location = new System.Drawing.Point(123, 44);
            this.cbFedExCurrency.Name = "cbFedExCurrency";
            this.cbFedExCurrency.Size = new System.Drawing.Size(181, 21);
            this.cbFedExCurrency.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Currency";
            // 
            // tbFedExAmount
            // 
            this.tbFedExAmount.Location = new System.Drawing.Point(123, 14);
            this.tbFedExAmount.Name = "tbFedExAmount";
            this.tbFedExAmount.Size = new System.Drawing.Size(181, 20);
            this.tbFedExAmount.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Amount";
            // 
            // cbCOD
            // 
            this.cbCOD.AutoSize = true;
            this.cbCOD.Location = new System.Drawing.Point(7, 7);
            this.cbCOD.Name = "cbCOD";
            this.cbCOD.Size = new System.Drawing.Size(116, 17);
            this.cbCOD.TabIndex = 0;
            this.cbCOD.Text = "Collect On Delivery";
            this.cbCOD.UseVisualStyleBackColor = true;
            this.cbCOD.CheckedChanged += new System.EventHandler(this.cbCOD_CheckedChanged);
            // 
            // PuroShipmentOptions
            // 
            this.PuroShipmentOptions.AutoScroll = true;
            this.PuroShipmentOptions.Controls.Add(this.pPuroDangGoods);
            this.PuroShipmentOptions.Controls.Add(this.cbPuroDangGoods);
            this.PuroShipmentOptions.Controls.Add(this.cbChainOfSignature);
            this.PuroShipmentOptions.Controls.Add(this.cbPuroExceptionHandling);
            this.PuroShipmentOptions.Controls.Add(this.cbPuroSaturdayPickup);
            this.PuroShipmentOptions.Controls.Add(this.cbPuroSaturdayDelivery);
            this.PuroShipmentOptions.Controls.Add(this.cbHoldForPickUp);
            this.PuroShipmentOptions.Controls.Add(this.cbExpressCheque);
            this.PuroShipmentOptions.Controls.Add(this.cbPuroSignatureRequired);
            this.PuroShipmentOptions.Controls.Add(this.cbOSNR);
            this.PuroShipmentOptions.Controls.Add(this.pExpressCheque);
            this.PuroShipmentOptions.Location = new System.Drawing.Point(4, 22);
            this.PuroShipmentOptions.Name = "PuroShipmentOptions";
            this.PuroShipmentOptions.Size = new System.Drawing.Size(579, 306);
            this.PuroShipmentOptions.TabIndex = 2;
            this.PuroShipmentOptions.Text = "Shipment Options";
            this.PuroShipmentOptions.UseVisualStyleBackColor = true;
            // 
            // pPuroDangGoods
            // 
            this.pPuroDangGoods.Controls.Add(this.cbPuroDGClass);
            this.pPuroDangGoods.Controls.Add(this.lPuroDangGoodClass);
            this.pPuroDangGoods.Controls.Add(this.cbPuroDGMode);
            this.pPuroDangGoods.Controls.Add(this.lPuroDangGoods);
            this.pPuroDangGoods.Location = new System.Drawing.Point(3, 626);
            this.pPuroDangGoods.Name = "pPuroDangGoods";
            this.pPuroDangGoods.Size = new System.Drawing.Size(559, 100);
            this.pPuroDangGoods.TabIndex = 23;
            // 
            // cbPuroDGClass
            // 
            this.cbPuroDGClass.FormattingEnabled = true;
            this.cbPuroDGClass.Location = new System.Drawing.Point(196, 53);
            this.cbPuroDGClass.Name = "cbPuroDGClass";
            this.cbPuroDGClass.Size = new System.Drawing.Size(168, 21);
            this.cbPuroDGClass.TabIndex = 3;
            // 
            // lPuroDangGoodClass
            // 
            this.lPuroDangGoodClass.AutoSize = true;
            this.lPuroDangGoodClass.Location = new System.Drawing.Point(4, 56);
            this.lPuroDangGoodClass.Name = "lPuroDangGoodClass";
            this.lPuroDangGoodClass.Size = new System.Drawing.Size(157, 13);
            this.lPuroDangGoodClass.TabIndex = 2;
            this.lPuroDangGoodClass.Text = "Dangerous Goods Classification";
            // 
            // cbPuroDGMode
            // 
            this.cbPuroDGMode.FormattingEnabled = true;
            this.cbPuroDGMode.Location = new System.Drawing.Point(196, 16);
            this.cbPuroDGMode.Name = "cbPuroDGMode";
            this.cbPuroDGMode.Size = new System.Drawing.Size(168, 21);
            this.cbPuroDGMode.TabIndex = 1;
            // 
            // lPuroDangGoods
            // 
            this.lPuroDangGoods.AutoSize = true;
            this.lPuroDangGoods.Location = new System.Drawing.Point(4, 16);
            this.lPuroDangGoods.Name = "lPuroDangGoods";
            this.lPuroDangGoods.Size = new System.Drawing.Size(123, 13);
            this.lPuroDangGoods.TabIndex = 0;
            this.lPuroDangGoods.Text = "Dangerous Goods Mode";
            // 
            // cbPuroDangGoods
            // 
            this.cbPuroDangGoods.AutoSize = true;
            this.cbPuroDangGoods.Location = new System.Drawing.Point(3, 603);
            this.cbPuroDangGoods.Name = "cbPuroDangGoods";
            this.cbPuroDangGoods.Size = new System.Drawing.Size(112, 17);
            this.cbPuroDangGoods.TabIndex = 22;
            this.cbPuroDangGoods.Text = "Dangerous Goods";
            this.cbPuroDangGoods.UseVisualStyleBackColor = true;
            this.cbPuroDangGoods.CheckedChanged += new System.EventHandler(this.cbPuroDangGoods_CheckedChanged);
            // 
            // cbChainOfSignature
            // 
            this.cbChainOfSignature.AutoSize = true;
            this.cbChainOfSignature.Location = new System.Drawing.Point(3, 580);
            this.cbChainOfSignature.Name = "cbChainOfSignature";
            this.cbChainOfSignature.Size = new System.Drawing.Size(115, 17);
            this.cbChainOfSignature.TabIndex = 21;
            this.cbChainOfSignature.Text = "Chain Of Signature";
            this.cbChainOfSignature.UseVisualStyleBackColor = true;
            // 
            // cbPuroExceptionHandling
            // 
            this.cbPuroExceptionHandling.AutoSize = true;
            this.cbPuroExceptionHandling.Location = new System.Drawing.Point(3, 557);
            this.cbPuroExceptionHandling.Name = "cbPuroExceptionHandling";
            this.cbPuroExceptionHandling.Size = new System.Drawing.Size(118, 17);
            this.cbPuroExceptionHandling.TabIndex = 20;
            this.cbPuroExceptionHandling.Text = "Exception Handling";
            this.cbPuroExceptionHandling.UseVisualStyleBackColor = true;
            // 
            // cbPuroSaturdayPickup
            // 
            this.cbPuroSaturdayPickup.AutoSize = true;
            this.cbPuroSaturdayPickup.Location = new System.Drawing.Point(3, 534);
            this.cbPuroSaturdayPickup.Name = "cbPuroSaturdayPickup";
            this.cbPuroSaturdayPickup.Size = new System.Drawing.Size(106, 17);
            this.cbPuroSaturdayPickup.TabIndex = 19;
            this.cbPuroSaturdayPickup.Text = "Saturday PickUp";
            this.cbPuroSaturdayPickup.UseVisualStyleBackColor = true;
            // 
            // cbPuroSaturdayDelivery
            // 
            this.cbPuroSaturdayDelivery.AutoSize = true;
            this.cbPuroSaturdayDelivery.Location = new System.Drawing.Point(3, 511);
            this.cbPuroSaturdayDelivery.Name = "cbPuroSaturdayDelivery";
            this.cbPuroSaturdayDelivery.Size = new System.Drawing.Size(109, 17);
            this.cbPuroSaturdayDelivery.TabIndex = 18;
            this.cbPuroSaturdayDelivery.Text = "Saturday Delivery";
            this.cbPuroSaturdayDelivery.UseVisualStyleBackColor = true;
            // 
            // cbHoldForPickUp
            // 
            this.cbHoldForPickUp.AutoSize = true;
            this.cbHoldForPickUp.Location = new System.Drawing.Point(3, 488);
            this.cbHoldForPickUp.Name = "cbHoldForPickUp";
            this.cbHoldForPickUp.Size = new System.Drawing.Size(104, 17);
            this.cbHoldForPickUp.TabIndex = 3;
            this.cbHoldForPickUp.Text = "Hold For PickUp";
            this.cbHoldForPickUp.UseVisualStyleBackColor = true;
            // 
            // cbExpressCheque
            // 
            this.cbExpressCheque.AutoSize = true;
            this.cbExpressCheque.Location = new System.Drawing.Point(4, 63);
            this.cbExpressCheque.Name = "cbExpressCheque";
            this.cbExpressCheque.Size = new System.Drawing.Size(103, 17);
            this.cbExpressCheque.TabIndex = 17;
            this.cbExpressCheque.Text = "Express Cheque";
            this.cbExpressCheque.UseVisualStyleBackColor = true;
            this.cbExpressCheque.CheckedChanged += new System.EventHandler(this.cbExpressCheque_CheckedChanged);
            // 
            // cbPuroSignatureRequired
            // 
            this.cbPuroSignatureRequired.AutoSize = true;
            this.cbPuroSignatureRequired.Location = new System.Drawing.Point(4, 40);
            this.cbPuroSignatureRequired.Name = "cbPuroSignatureRequired";
            this.cbPuroSignatureRequired.Size = new System.Drawing.Size(117, 17);
            this.cbPuroSignatureRequired.TabIndex = 16;
            this.cbPuroSignatureRequired.Text = "Signature Required";
            this.cbPuroSignatureRequired.UseVisualStyleBackColor = true;
            // 
            // cbOSNR
            // 
            this.cbOSNR.AutoSize = true;
            this.cbOSNR.Location = new System.Drawing.Point(4, 17);
            this.cbOSNR.Name = "cbOSNR";
            this.cbOSNR.Size = new System.Drawing.Size(256, 17);
            this.cbOSNR.TabIndex = 15;
            this.cbOSNR.Text = "Origin Approved Signature Not Required (OSNR)";
            this.cbOSNR.UseVisualStyleBackColor = true;
            // 
            // pExpressCheque
            // 
            this.pExpressCheque.AutoScroll = true;
            this.pExpressCheque.Controls.Add(this.tbPuroPhone);
            this.pExpressCheque.Controls.Add(this.label59);
            this.pExpressCheque.Controls.Add(this.tbPuroEmail);
            this.pExpressCheque.Controls.Add(this.label58);
            this.pExpressCheque.Controls.Add(this.tbPuroZIP);
            this.pExpressCheque.Controls.Add(this.label57);
            this.pExpressCheque.Controls.Add(this.tbPuroAddress2);
            this.pExpressCheque.Controls.Add(this.label56);
            this.pExpressCheque.Controls.Add(this.tbPuroAddress1);
            this.pExpressCheque.Controls.Add(this.label55);
            this.pExpressCheque.Controls.Add(this.tbPuroCity);
            this.pExpressCheque.Controls.Add(this.label54);
            this.pExpressCheque.Controls.Add(this.cbPuroState);
            this.pExpressCheque.Controls.Add(this.label53);
            this.pExpressCheque.Controls.Add(this.cbPuroCountry);
            this.pExpressCheque.Controls.Add(this.label52);
            this.pExpressCheque.Controls.Add(this.tbDepartment);
            this.pExpressCheque.Controls.Add(this.label51);
            this.pExpressCheque.Controls.Add(this.tbCompany);
            this.pExpressCheque.Controls.Add(this.label50);
            this.pExpressCheque.Controls.Add(this.tbName);
            this.pExpressCheque.Controls.Add(this.label49);
            this.pExpressCheque.Controls.Add(this.label48);
            this.pExpressCheque.Controls.Add(this.cbMethodOfPayment);
            this.pExpressCheque.Controls.Add(this.label47);
            this.pExpressCheque.Controls.Add(this.tbAmount);
            this.pExpressCheque.Controls.Add(this.label46);
            this.pExpressCheque.Location = new System.Drawing.Point(3, 86);
            this.pExpressCheque.Name = "pExpressCheque";
            this.pExpressCheque.Size = new System.Drawing.Size(562, 396);
            this.pExpressCheque.TabIndex = 14;
            // 
            // tbPuroPhone
            // 
            this.tbPuroPhone.Location = new System.Drawing.Point(132, 370);
            this.tbPuroPhone.Name = "tbPuroPhone";
            this.tbPuroPhone.Size = new System.Drawing.Size(208, 20);
            this.tbPuroPhone.TabIndex = 26;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(21, 370);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(78, 13);
            this.label59.TabIndex = 25;
            this.label59.Text = "Phone Number";
            // 
            // tbPuroEmail
            // 
            this.tbPuroEmail.Location = new System.Drawing.Point(132, 340);
            this.tbPuroEmail.Name = "tbPuroEmail";
            this.tbPuroEmail.Size = new System.Drawing.Size(208, 20);
            this.tbPuroEmail.TabIndex = 24;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(18, 343);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(73, 13);
            this.label58.TabIndex = 23;
            this.label58.Text = "Email Address";
            // 
            // tbPuroZIP
            // 
            this.tbPuroZIP.Location = new System.Drawing.Point(132, 312);
            this.tbPuroZIP.Name = "tbPuroZIP";
            this.tbPuroZIP.Size = new System.Drawing.Size(208, 20);
            this.tbPuroZIP.TabIndex = 22;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(18, 315);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(92, 13);
            this.label57.TabIndex = 21;
            this.label57.Text = "ZIP / Postal Code";
            // 
            // tbPuroAddress2
            // 
            this.tbPuroAddress2.Location = new System.Drawing.Point(132, 283);
            this.tbPuroAddress2.Name = "tbPuroAddress2";
            this.tbPuroAddress2.Size = new System.Drawing.Size(208, 20);
            this.tbPuroAddress2.TabIndex = 20;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(18, 286);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(54, 13);
            this.label56.TabIndex = 19;
            this.label56.Text = "Address 2";
            // 
            // tbPuroAddress1
            // 
            this.tbPuroAddress1.Location = new System.Drawing.Point(132, 257);
            this.tbPuroAddress1.Name = "tbPuroAddress1";
            this.tbPuroAddress1.Size = new System.Drawing.Size(208, 20);
            this.tbPuroAddress1.TabIndex = 18;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(18, 260);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(54, 13);
            this.label55.TabIndex = 17;
            this.label55.Text = "Address 1";
            // 
            // tbPuroCity
            // 
            this.tbPuroCity.Location = new System.Drawing.Point(132, 231);
            this.tbPuroCity.Name = "tbPuroCity";
            this.tbPuroCity.Size = new System.Drawing.Size(208, 20);
            this.tbPuroCity.TabIndex = 16;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(18, 234);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(24, 13);
            this.label54.TabIndex = 15;
            this.label54.Text = "City";
            // 
            // cbPuroState
            // 
            this.cbPuroState.FormattingEnabled = true;
            this.cbPuroState.Location = new System.Drawing.Point(132, 204);
            this.cbPuroState.Name = "cbPuroState";
            this.cbPuroState.Size = new System.Drawing.Size(208, 21);
            this.cbPuroState.TabIndex = 14;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(18, 207);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(85, 13);
            this.label53.TabIndex = 13;
            this.label53.Text = "State / Province";
            // 
            // cbPuroCountry
            // 
            this.cbPuroCountry.FormattingEnabled = true;
            this.cbPuroCountry.Location = new System.Drawing.Point(132, 177);
            this.cbPuroCountry.Name = "cbPuroCountry";
            this.cbPuroCountry.Size = new System.Drawing.Size(208, 21);
            this.cbPuroCountry.TabIndex = 12;
            this.cbPuroCountry.SelectedIndexChanged += new System.EventHandler(this.cbPuroCountry_SelectedIndexChanged);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(18, 185);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(43, 13);
            this.label52.TabIndex = 11;
            this.label52.Text = "Country";
            // 
            // tbDepartment
            // 
            this.tbDepartment.Location = new System.Drawing.Point(132, 151);
            this.tbDepartment.Name = "tbDepartment";
            this.tbDepartment.Size = new System.Drawing.Size(208, 20);
            this.tbDepartment.TabIndex = 10;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(18, 154);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(62, 13);
            this.label51.TabIndex = 9;
            this.label51.Text = "Department";
            // 
            // tbCompany
            // 
            this.tbCompany.Location = new System.Drawing.Point(132, 125);
            this.tbCompany.Name = "tbCompany";
            this.tbCompany.Size = new System.Drawing.Size(208, 20);
            this.tbCompany.TabIndex = 8;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(18, 128);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(51, 13);
            this.label50.TabIndex = 7;
            this.label50.Text = "Company";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(132, 97);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(208, 20);
            this.tbName.TabIndex = 6;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(18, 100);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(35, 13);
            this.label49.TabIndex = 5;
            this.label49.Text = "Name";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(10, 67);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(94, 13);
            this.label48.TabIndex = 4;
            this.label48.Text = "Return Address";
            // 
            // cbMethodOfPayment
            // 
            this.cbMethodOfPayment.FormattingEnabled = true;
            this.cbMethodOfPayment.Location = new System.Drawing.Point(132, 37);
            this.cbMethodOfPayment.Name = "cbMethodOfPayment";
            this.cbMethodOfPayment.Size = new System.Drawing.Size(208, 21);
            this.cbMethodOfPayment.TabIndex = 3;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(10, 37);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(99, 13);
            this.label47.TabIndex = 2;
            this.label47.Text = "Method of Payment";
            // 
            // tbAmount
            // 
            this.tbAmount.Location = new System.Drawing.Point(132, 7);
            this.tbAmount.Name = "tbAmount";
            this.tbAmount.Size = new System.Drawing.Size(208, 20);
            this.tbAmount.TabIndex = 1;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(10, 10);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(43, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "Amount";
            // 
            // LoomisShipmentOptions
            // 
            this.LoomisShipmentOptions.AutoScroll = true;
            this.LoomisShipmentOptions.Controls.Add(this.rtbLoomisInstructions);
            this.LoomisShipmentOptions.Controls.Add(this.labelInstructions);
            this.LoomisShipmentOptions.Controls.Add(this.tbLoomisAdditionalInfo2);
            this.LoomisShipmentOptions.Controls.Add(this.labelAdditionalInfo2);
            this.LoomisShipmentOptions.Controls.Add(this.tbLoomisAdditionalInfo1);
            this.LoomisShipmentOptions.Controls.Add(this.labelAdditionalInfo1);
            this.LoomisShipmentOptions.Controls.Add(this.labelAdditionalInfo);
            this.LoomisShipmentOptions.Controls.Add(this.cbLoomisNoSignatureRequired);
            this.LoomisShipmentOptions.Controls.Add(this.cbLoomisSaturdayDelivery);
            this.LoomisShipmentOptions.Controls.Add(this.pLoomisReturnCheck);
            this.LoomisShipmentOptions.Controls.Add(this.cbLoomisReturnCheck);
            this.LoomisShipmentOptions.Controls.Add(this.cbLoomisFragile);
            this.LoomisShipmentOptions.Controls.Add(this.cbLoomisNotPackaged);
            this.LoomisShipmentOptions.Location = new System.Drawing.Point(4, 22);
            this.LoomisShipmentOptions.Name = "LoomisShipmentOptions";
            this.LoomisShipmentOptions.Size = new System.Drawing.Size(579, 306);
            this.LoomisShipmentOptions.TabIndex = 3;
            this.LoomisShipmentOptions.Text = "Shipment Options";
            this.LoomisShipmentOptions.UseVisualStyleBackColor = true;
            this.LoomisShipmentOptions.Click += new System.EventHandler(this.LoomisShipmentOptions_Click);
            // 
            // rtbLoomisInstructions
            // 
            this.rtbLoomisInstructions.Location = new System.Drawing.Point(11, 333);
            this.rtbLoomisInstructions.Name = "rtbLoomisInstructions";
            this.rtbLoomisInstructions.Size = new System.Drawing.Size(308, 93);
            this.rtbLoomisInstructions.TabIndex = 12;
            this.rtbLoomisInstructions.Text = "";
            // 
            // labelInstructions
            // 
            this.labelInstructions.AutoSize = true;
            this.labelInstructions.Location = new System.Drawing.Point(7, 317);
            this.labelInstructions.Name = "labelInstructions";
            this.labelInstructions.Size = new System.Drawing.Size(61, 13);
            this.labelInstructions.TabIndex = 11;
            this.labelInstructions.Text = "Instructions";
            // 
            // tbLoomisAdditionalInfo2
            // 
            this.tbLoomisAdditionalInfo2.Location = new System.Drawing.Point(38, 283);
            this.tbLoomisAdditionalInfo2.Name = "tbLoomisAdditionalInfo2";
            this.tbLoomisAdditionalInfo2.Size = new System.Drawing.Size(281, 20);
            this.tbLoomisAdditionalInfo2.TabIndex = 10;
            // 
            // labelAdditionalInfo2
            // 
            this.labelAdditionalInfo2.AutoSize = true;
            this.labelAdditionalInfo2.Location = new System.Drawing.Point(11, 286);
            this.labelAdditionalInfo2.Name = "labelAdditionalInfo2";
            this.labelAdditionalInfo2.Size = new System.Drawing.Size(16, 13);
            this.labelAdditionalInfo2.TabIndex = 9;
            this.labelAdditionalInfo2.Text = "2.";
            // 
            // tbLoomisAdditionalInfo1
            // 
            this.tbLoomisAdditionalInfo1.Location = new System.Drawing.Point(38, 256);
            this.tbLoomisAdditionalInfo1.Name = "tbLoomisAdditionalInfo1";
            this.tbLoomisAdditionalInfo1.Size = new System.Drawing.Size(281, 20);
            this.tbLoomisAdditionalInfo1.TabIndex = 8;
            // 
            // labelAdditionalInfo1
            // 
            this.labelAdditionalInfo1.AutoSize = true;
            this.labelAdditionalInfo1.Location = new System.Drawing.Point(11, 259);
            this.labelAdditionalInfo1.Name = "labelAdditionalInfo1";
            this.labelAdditionalInfo1.Size = new System.Drawing.Size(16, 13);
            this.labelAdditionalInfo1.TabIndex = 7;
            this.labelAdditionalInfo1.Text = "1.";
            // 
            // labelAdditionalInfo
            // 
            this.labelAdditionalInfo.AutoSize = true;
            this.labelAdditionalInfo.Location = new System.Drawing.Point(4, 239);
            this.labelAdditionalInfo.Name = "labelAdditionalInfo";
            this.labelAdditionalInfo.Size = new System.Drawing.Size(108, 13);
            this.labelAdditionalInfo.TabIndex = 6;
            this.labelAdditionalInfo.Text = "Additional Information";
            // 
            // cbLoomisNoSignatureRequired
            // 
            this.cbLoomisNoSignatureRequired.AutoSize = true;
            this.cbLoomisNoSignatureRequired.Location = new System.Drawing.Point(4, 215);
            this.cbLoomisNoSignatureRequired.Name = "cbLoomisNoSignatureRequired";
            this.cbLoomisNoSignatureRequired.Size = new System.Drawing.Size(134, 17);
            this.cbLoomisNoSignatureRequired.TabIndex = 5;
            this.cbLoomisNoSignatureRequired.Text = "No Signature Required";
            this.cbLoomisNoSignatureRequired.UseVisualStyleBackColor = true;
            // 
            // cbLoomisSaturdayDelivery
            // 
            this.cbLoomisSaturdayDelivery.AutoSize = true;
            this.cbLoomisSaturdayDelivery.Location = new System.Drawing.Point(4, 191);
            this.cbLoomisSaturdayDelivery.Name = "cbLoomisSaturdayDelivery";
            this.cbLoomisSaturdayDelivery.Size = new System.Drawing.Size(109, 17);
            this.cbLoomisSaturdayDelivery.TabIndex = 4;
            this.cbLoomisSaturdayDelivery.Text = "Saturday Delivery";
            this.cbLoomisSaturdayDelivery.UseVisualStyleBackColor = true;
            // 
            // pLoomisReturnCheck
            // 
            this.pLoomisReturnCheck.Controls.Add(this.cbLoomisMethodOfPayment);
            this.pLoomisReturnCheck.Controls.Add(this.label62);
            this.pLoomisReturnCheck.Controls.Add(this.tbLoomisAmount);
            this.pLoomisReturnCheck.Controls.Add(this.label61);
            this.pLoomisReturnCheck.Controls.Add(this.label60);
            this.pLoomisReturnCheck.Location = new System.Drawing.Point(4, 84);
            this.pLoomisReturnCheck.Name = "pLoomisReturnCheck";
            this.pLoomisReturnCheck.Size = new System.Drawing.Size(572, 100);
            this.pLoomisReturnCheck.TabIndex = 3;
            // 
            // cbLoomisMethodOfPayment
            // 
            this.cbLoomisMethodOfPayment.FormattingEnabled = true;
            this.cbLoomisMethodOfPayment.Location = new System.Drawing.Point(147, 57);
            this.cbLoomisMethodOfPayment.Name = "cbLoomisMethodOfPayment";
            this.cbLoomisMethodOfPayment.Size = new System.Drawing.Size(217, 21);
            this.cbLoomisMethodOfPayment.TabIndex = 4;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(4, 60);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(99, 13);
            this.label62.TabIndex = 3;
            this.label62.Text = "Method of Payment";
            // 
            // tbLoomisAmount
            // 
            this.tbLoomisAmount.Location = new System.Drawing.Point(147, 30);
            this.tbLoomisAmount.Name = "tbLoomisAmount";
            this.tbLoomisAmount.Size = new System.Drawing.Size(217, 20);
            this.tbLoomisAmount.TabIndex = 2;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(7, 30);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(43, 13);
            this.label61.TabIndex = 1;
            this.label61.Text = "Amount";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(4, 4);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(392, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "This is a prepaid shipment option, enabling it will enforce use of billing type S" +
    "ender";
            // 
            // cbLoomisReturnCheck
            // 
            this.cbLoomisReturnCheck.AutoSize = true;
            this.cbLoomisReturnCheck.Location = new System.Drawing.Point(4, 59);
            this.cbLoomisReturnCheck.Name = "cbLoomisReturnCheck";
            this.cbLoomisReturnCheck.Size = new System.Drawing.Size(92, 17);
            this.cbLoomisReturnCheck.TabIndex = 2;
            this.cbLoomisReturnCheck.Text = "Return Check";
            this.cbLoomisReturnCheck.UseVisualStyleBackColor = true;
            this.cbLoomisReturnCheck.CheckedChanged += new System.EventHandler(this.cbLoomisReturnCheck_CheckedChanged);
            // 
            // cbLoomisFragile
            // 
            this.cbLoomisFragile.AutoSize = true;
            this.cbLoomisFragile.Location = new System.Drawing.Point(4, 36);
            this.cbLoomisFragile.Name = "cbLoomisFragile";
            this.cbLoomisFragile.Size = new System.Drawing.Size(57, 17);
            this.cbLoomisFragile.TabIndex = 1;
            this.cbLoomisFragile.Text = "Fragile";
            this.cbLoomisFragile.UseVisualStyleBackColor = true;
            // 
            // cbLoomisNotPackaged
            // 
            this.cbLoomisNotPackaged.AutoSize = true;
            this.cbLoomisNotPackaged.Location = new System.Drawing.Point(3, 12);
            this.cbLoomisNotPackaged.Name = "cbLoomisNotPackaged";
            this.cbLoomisNotPackaged.Size = new System.Drawing.Size(95, 17);
            this.cbLoomisNotPackaged.TabIndex = 0;
            this.cbLoomisNotPackaged.Text = "Not Packaged";
            this.cbLoomisNotPackaged.UseVisualStyleBackColor = true;
            // 
            // UPSShipmentOptions
            // 
            this.UPSShipmentOptions.Controls.Add(this.pUPSDeliveryConfirmation);
            this.UPSShipmentOptions.Controls.Add(this.cbUPSLargePackage);
            this.UPSShipmentOptions.Controls.Add(this.cbUPSDeliveryConfirmation);
            this.UPSShipmentOptions.Controls.Add(this.cbUPSSaturdayPickup);
            this.UPSShipmentOptions.Controls.Add(this.cbUPSSaturdayDelivery);
            this.UPSShipmentOptions.Location = new System.Drawing.Point(4, 22);
            this.UPSShipmentOptions.Name = "UPSShipmentOptions";
            this.UPSShipmentOptions.Size = new System.Drawing.Size(579, 306);
            this.UPSShipmentOptions.TabIndex = 4;
            this.UPSShipmentOptions.Text = "Shipment Options";
            this.UPSShipmentOptions.UseVisualStyleBackColor = true;
            // 
            // pUPSDeliveryConfirmation
            // 
            this.pUPSDeliveryConfirmation.Controls.Add(this.label67);
            this.pUPSDeliveryConfirmation.Controls.Add(this.cbUPSDeliveryConfirmationType);
            this.pUPSDeliveryConfirmation.Location = new System.Drawing.Point(4, 86);
            this.pUPSDeliveryConfirmation.Name = "pUPSDeliveryConfirmation";
            this.pUPSDeliveryConfirmation.Size = new System.Drawing.Size(572, 31);
            this.pUPSDeliveryConfirmation.TabIndex = 6;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(17, 9);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(92, 13);
            this.label67.TabIndex = 3;
            this.label67.Text = "Confirmation Type";
            // 
            // cbUPSDeliveryConfirmationType
            // 
            this.cbUPSDeliveryConfirmationType.FormattingEnabled = true;
            this.cbUPSDeliveryConfirmationType.Location = new System.Drawing.Point(138, 6);
            this.cbUPSDeliveryConfirmationType.Name = "cbUPSDeliveryConfirmationType";
            this.cbUPSDeliveryConfirmationType.Size = new System.Drawing.Size(227, 21);
            this.cbUPSDeliveryConfirmationType.TabIndex = 4;
            // 
            // cbUPSLargePackage
            // 
            this.cbUPSLargePackage.AutoSize = true;
            this.cbUPSLargePackage.Location = new System.Drawing.Point(4, 123);
            this.cbUPSLargePackage.Name = "cbUPSLargePackage";
            this.cbUPSLargePackage.Size = new System.Drawing.Size(143, 17);
            this.cbUPSLargePackage.TabIndex = 5;
            this.cbUPSLargePackage.Text = "Large Package Indicator";
            this.cbUPSLargePackage.UseVisualStyleBackColor = true;
            // 
            // cbUPSDeliveryConfirmation
            // 
            this.cbUPSDeliveryConfirmation.AutoSize = true;
            this.cbUPSDeliveryConfirmation.Location = new System.Drawing.Point(4, 62);
            this.cbUPSDeliveryConfirmation.Name = "cbUPSDeliveryConfirmation";
            this.cbUPSDeliveryConfirmation.Size = new System.Drawing.Size(125, 17);
            this.cbUPSDeliveryConfirmation.TabIndex = 2;
            this.cbUPSDeliveryConfirmation.Text = "Delivery Confirmation";
            this.cbUPSDeliveryConfirmation.UseVisualStyleBackColor = true;
            this.cbUPSDeliveryConfirmation.CheckedChanged += new System.EventHandler(this.cbUPSDeliveryConfirmation_CheckedChanged);
            // 
            // cbUPSSaturdayPickup
            // 
            this.cbUPSSaturdayPickup.AutoSize = true;
            this.cbUPSSaturdayPickup.Location = new System.Drawing.Point(4, 38);
            this.cbUPSSaturdayPickup.Name = "cbUPSSaturdayPickup";
            this.cbUPSSaturdayPickup.Size = new System.Drawing.Size(104, 17);
            this.cbUPSSaturdayPickup.TabIndex = 1;
            this.cbUPSSaturdayPickup.Text = "Saturday Pickup";
            this.cbUPSSaturdayPickup.UseVisualStyleBackColor = true;
            // 
            // cbUPSSaturdayDelivery
            // 
            this.cbUPSSaturdayDelivery.AutoSize = true;
            this.cbUPSSaturdayDelivery.Location = new System.Drawing.Point(4, 14);
            this.cbUPSSaturdayDelivery.Name = "cbUPSSaturdayDelivery";
            this.cbUPSSaturdayDelivery.Size = new System.Drawing.Size(109, 17);
            this.cbUPSSaturdayDelivery.TabIndex = 0;
            this.cbUPSSaturdayDelivery.Text = "Saturday Delivery";
            this.cbUPSSaturdayDelivery.UseVisualStyleBackColor = true;
            // 
            // CAPostShipmentOptions
            // 
            this.CAPostShipmentOptions.AutoScroll = true;
            this.CAPostShipmentOptions.Controls.Add(this.cbCAPostLeaveAtDoor);
            this.CAPostShipmentOptions.Controls.Add(this.cbCAPostDoNotSafeDrop);
            this.CAPostShipmentOptions.Controls.Add(this.cbCaPostCardForPickup);
            this.CAPostShipmentOptions.Controls.Add(this.labelCAPostNonDeliveryHandling);
            this.CAPostShipmentOptions.Controls.Add(this.pCAPostCOD);
            this.CAPostShipmentOptions.Controls.Add(this.cbCAPostCOD);
            this.CAPostShipmentOptions.Controls.Add(this.cbCAPostUnpackaged);
            this.CAPostShipmentOptions.Controls.Add(this.cbCAPostDeliveryConfirmation);
            this.CAPostShipmentOptions.Controls.Add(this.cbCAPostSignatureRequired);
            this.CAPostShipmentOptions.Controls.Add(this.label73);
            this.CAPostShipmentOptions.Controls.Add(this.cbCAPostOffice);
            this.CAPostShipmentOptions.Controls.Add(this.cbCAPostAge19);
            this.CAPostShipmentOptions.Controls.Add(this.cbCAPostAge18);
            this.CAPostShipmentOptions.Controls.Add(this.label72);
            this.CAPostShipmentOptions.Location = new System.Drawing.Point(4, 22);
            this.CAPostShipmentOptions.Name = "CAPostShipmentOptions";
            this.CAPostShipmentOptions.Size = new System.Drawing.Size(579, 306);
            this.CAPostShipmentOptions.TabIndex = 5;
            this.CAPostShipmentOptions.Text = "Shipment Options";
            this.CAPostShipmentOptions.UseVisualStyleBackColor = true;
            // 
            // cbCAPostLeaveAtDoor
            // 
            this.cbCAPostLeaveAtDoor.AutoSize = true;
            this.cbCAPostLeaveAtDoor.Location = new System.Drawing.Point(19, 645);
            this.cbCAPostLeaveAtDoor.Name = "cbCAPostLeaveAtDoor";
            this.cbCAPostLeaveAtDoor.Size = new System.Drawing.Size(162, 17);
            this.cbCAPostLeaveAtDoor.TabIndex = 12;
            this.cbCAPostLeaveAtDoor.Text = "Leave at Door (Do Not Card)";
            this.cbCAPostLeaveAtDoor.UseVisualStyleBackColor = true;
            // 
            // cbCAPostDoNotSafeDrop
            // 
            this.cbCAPostDoNotSafeDrop.AutoSize = true;
            this.cbCAPostDoNotSafeDrop.Location = new System.Drawing.Point(19, 622);
            this.cbCAPostDoNotSafeDrop.Name = "cbCAPostDoNotSafeDrop";
            this.cbCAPostDoNotSafeDrop.Size = new System.Drawing.Size(117, 17);
            this.cbCAPostDoNotSafeDrop.TabIndex = 11;
            this.cbCAPostDoNotSafeDrop.Text = "Do NOT Safe Drop";
            this.cbCAPostDoNotSafeDrop.UseVisualStyleBackColor = true;
            // 
            // cbCaPostCardForPickup
            // 
            this.cbCaPostCardForPickup.AutoSize = true;
            this.cbCaPostCardForPickup.Location = new System.Drawing.Point(19, 599);
            this.cbCaPostCardForPickup.Name = "cbCaPostCardForPickup";
            this.cbCaPostCardForPickup.Size = new System.Drawing.Size(99, 17);
            this.cbCaPostCardForPickup.TabIndex = 10;
            this.cbCaPostCardForPickup.Text = "Card for Pickup";
            this.cbCaPostCardForPickup.UseVisualStyleBackColor = true;
            // 
            // labelCAPostNonDeliveryHandling
            // 
            this.labelCAPostNonDeliveryHandling.AutoSize = true;
            this.labelCAPostNonDeliveryHandling.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCAPostNonDeliveryHandling.Location = new System.Drawing.Point(5, 575);
            this.labelCAPostNonDeliveryHandling.Name = "labelCAPostNonDeliveryHandling";
            this.labelCAPostNonDeliveryHandling.Size = new System.Drawing.Size(198, 13);
            this.labelCAPostNonDeliveryHandling.TabIndex = 1;
            this.labelCAPostNonDeliveryHandling.Text = "Non Delivery Handling (Domestic)";
            // 
            // pCAPostCOD
            // 
            this.pCAPostCOD.Controls.Add(this.tbCAPostCODPhone);
            this.pCAPostCOD.Controls.Add(this.label85);
            this.pCAPostCOD.Controls.Add(this.tbCAPostCODEmail);
            this.pCAPostCOD.Controls.Add(this.label84);
            this.pCAPostCOD.Controls.Add(this.tbCAPostCODZIP);
            this.pCAPostCOD.Controls.Add(this.label83);
            this.pCAPostCOD.Controls.Add(this.tbCAPostCODAddress2);
            this.pCAPostCOD.Controls.Add(this.label82);
            this.pCAPostCOD.Controls.Add(this.tbCAPostCODAddress1);
            this.pCAPostCOD.Controls.Add(this.label81);
            this.pCAPostCOD.Controls.Add(this.tbCAPostCODCity);
            this.pCAPostCOD.Controls.Add(this.label80);
            this.pCAPostCOD.Controls.Add(this.cbCAPostCODState);
            this.pCAPostCOD.Controls.Add(this.label79);
            this.pCAPostCOD.Controls.Add(this.cbCAPostCODCountry);
            this.pCAPostCOD.Controls.Add(this.label78);
            this.pCAPostCOD.Controls.Add(this.tbCAPostCODCompany);
            this.pCAPostCOD.Controls.Add(this.label77);
            this.pCAPostCOD.Controls.Add(this.tbCAPostCODName);
            this.pCAPostCOD.Controls.Add(this.label76);
            this.pCAPostCOD.Controls.Add(this.cbCAPostMethodOfPayment);
            this.pCAPostCOD.Controls.Add(this.label75);
            this.pCAPostCOD.Controls.Add(this.tbCAPostAmount);
            this.pCAPostCOD.Controls.Add(this.label74);
            this.pCAPostCOD.Location = new System.Drawing.Point(19, 232);
            this.pCAPostCOD.Name = "pCAPostCOD";
            this.pCAPostCOD.Size = new System.Drawing.Size(527, 330);
            this.pCAPostCOD.TabIndex = 9;
            // 
            // tbCAPostCODPhone
            // 
            this.tbCAPostCODPhone.Location = new System.Drawing.Point(142, 299);
            this.tbCAPostCODPhone.Name = "tbCAPostCODPhone";
            this.tbCAPostCODPhone.Size = new System.Drawing.Size(211, 20);
            this.tbCAPostCODPhone.TabIndex = 23;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(15, 302);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(38, 13);
            this.label85.TabIndex = 22;
            this.label85.Text = "Phone";
            // 
            // tbCAPostCODEmail
            // 
            this.tbCAPostCODEmail.Location = new System.Drawing.Point(142, 273);
            this.tbCAPostCODEmail.Name = "tbCAPostCODEmail";
            this.tbCAPostCODEmail.Size = new System.Drawing.Size(211, 20);
            this.tbCAPostCODEmail.TabIndex = 21;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(15, 276);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(32, 13);
            this.label84.TabIndex = 20;
            this.label84.Text = "Email";
            // 
            // tbCAPostCODZIP
            // 
            this.tbCAPostCODZIP.Location = new System.Drawing.Point(142, 246);
            this.tbCAPostCODZIP.Name = "tbCAPostCODZIP";
            this.tbCAPostCODZIP.Size = new System.Drawing.Size(211, 20);
            this.tbCAPostCODZIP.TabIndex = 19;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(15, 254);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(92, 13);
            this.label83.TabIndex = 18;
            this.label83.Text = "ZIP / Postal Code";
            // 
            // tbCAPostCODAddress2
            // 
            this.tbCAPostCODAddress2.Location = new System.Drawing.Point(142, 222);
            this.tbCAPostCODAddress2.Name = "tbCAPostCODAddress2";
            this.tbCAPostCODAddress2.Size = new System.Drawing.Size(211, 20);
            this.tbCAPostCODAddress2.TabIndex = 17;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(15, 230);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(54, 13);
            this.label82.TabIndex = 16;
            this.label82.Text = "Address 2";
            // 
            // tbCAPostCODAddress1
            // 
            this.tbCAPostCODAddress1.Location = new System.Drawing.Point(142, 197);
            this.tbCAPostCODAddress1.Name = "tbCAPostCODAddress1";
            this.tbCAPostCODAddress1.Size = new System.Drawing.Size(211, 20);
            this.tbCAPostCODAddress1.TabIndex = 15;
            this.tbCAPostCODAddress1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(15, 200);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(51, 13);
            this.label81.TabIndex = 14;
            this.label81.Text = "Address1";
            // 
            // tbCAPostCODCity
            // 
            this.tbCAPostCODCity.Location = new System.Drawing.Point(142, 172);
            this.tbCAPostCODCity.Name = "tbCAPostCODCity";
            this.tbCAPostCODCity.Size = new System.Drawing.Size(211, 20);
            this.tbCAPostCODCity.TabIndex = 13;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(15, 172);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(24, 13);
            this.label80.TabIndex = 12;
            this.label80.Text = "City";
            // 
            // cbCAPostCODState
            // 
            this.cbCAPostCODState.FormattingEnabled = true;
            this.cbCAPostCODState.Location = new System.Drawing.Point(142, 143);
            this.cbCAPostCODState.Name = "cbCAPostCODState";
            this.cbCAPostCODState.Size = new System.Drawing.Size(211, 21);
            this.cbCAPostCODState.TabIndex = 11;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(15, 146);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(85, 13);
            this.label79.TabIndex = 10;
            this.label79.Text = "State / Province";
            // 
            // cbCAPostCODCountry
            // 
            this.cbCAPostCODCountry.FormattingEnabled = true;
            this.cbCAPostCODCountry.Location = new System.Drawing.Point(142, 116);
            this.cbCAPostCODCountry.Name = "cbCAPostCODCountry";
            this.cbCAPostCODCountry.Size = new System.Drawing.Size(211, 21);
            this.cbCAPostCODCountry.TabIndex = 9;
            this.cbCAPostCODCountry.SelectedIndexChanged += new System.EventHandler(this.cbCAPostCODCountry_SelectedIndexChanged);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(15, 119);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(43, 13);
            this.label78.TabIndex = 8;
            this.label78.Text = "Country";
            // 
            // tbCAPostCODCompany
            // 
            this.tbCAPostCODCompany.Location = new System.Drawing.Point(142, 90);
            this.tbCAPostCODCompany.Name = "tbCAPostCODCompany";
            this.tbCAPostCODCompany.Size = new System.Drawing.Size(211, 20);
            this.tbCAPostCODCompany.TabIndex = 7;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(15, 93);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(51, 13);
            this.label77.TabIndex = 6;
            this.label77.Text = "Company";
            // 
            // tbCAPostCODName
            // 
            this.tbCAPostCODName.Location = new System.Drawing.Point(142, 64);
            this.tbCAPostCODName.Name = "tbCAPostCODName";
            this.tbCAPostCODName.Size = new System.Drawing.Size(211, 20);
            this.tbCAPostCODName.TabIndex = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(15, 67);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "Name";
            // 
            // cbCAPostMethodOfPayment
            // 
            this.cbCAPostMethodOfPayment.FormattingEnabled = true;
            this.cbCAPostMethodOfPayment.Location = new System.Drawing.Point(142, 37);
            this.cbCAPostMethodOfPayment.Name = "cbCAPostMethodOfPayment";
            this.cbCAPostMethodOfPayment.Size = new System.Drawing.Size(211, 21);
            this.cbCAPostMethodOfPayment.TabIndex = 3;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(15, 37);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(99, 13);
            this.label75.TabIndex = 2;
            this.label75.Text = "Method of Payment";
            // 
            // tbCAPostAmount
            // 
            this.tbCAPostAmount.Location = new System.Drawing.Point(142, 9);
            this.tbCAPostAmount.Name = "tbCAPostAmount";
            this.tbCAPostAmount.Size = new System.Drawing.Size(211, 20);
            this.tbCAPostAmount.TabIndex = 1;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(15, 9);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(43, 13);
            this.label74.TabIndex = 0;
            this.label74.Text = "Amount";
            // 
            // cbCAPostCOD
            // 
            this.cbCAPostCOD.AutoSize = true;
            this.cbCAPostCOD.Location = new System.Drawing.Point(19, 218);
            this.cbCAPostCOD.Name = "cbCAPostCOD";
            this.cbCAPostCOD.Size = new System.Drawing.Size(114, 17);
            this.cbCAPostCOD.TabIndex = 8;
            this.cbCAPostCOD.Text = "Collect on Delivery";
            this.cbCAPostCOD.UseVisualStyleBackColor = true;
            this.cbCAPostCOD.CheckedChanged += new System.EventHandler(this.cbCAPostCOD_CheckedChanged);
            // 
            // cbCAPostUnpackaged
            // 
            this.cbCAPostUnpackaged.AutoSize = true;
            this.cbCAPostUnpackaged.Location = new System.Drawing.Point(19, 194);
            this.cbCAPostUnpackaged.Name = "cbCAPostUnpackaged";
            this.cbCAPostUnpackaged.Size = new System.Drawing.Size(88, 17);
            this.cbCAPostUnpackaged.TabIndex = 7;
            this.cbCAPostUnpackaged.Text = "Unpackaged";
            this.cbCAPostUnpackaged.UseVisualStyleBackColor = true;
            // 
            // cbCAPostDeliveryConfirmation
            // 
            this.cbCAPostDeliveryConfirmation.AutoSize = true;
            this.cbCAPostDeliveryConfirmation.Location = new System.Drawing.Point(19, 170);
            this.cbCAPostDeliveryConfirmation.Name = "cbCAPostDeliveryConfirmation";
            this.cbCAPostDeliveryConfirmation.Size = new System.Drawing.Size(125, 17);
            this.cbCAPostDeliveryConfirmation.TabIndex = 6;
            this.cbCAPostDeliveryConfirmation.Text = "Delivery Confirmation";
            this.cbCAPostDeliveryConfirmation.UseVisualStyleBackColor = true;
            // 
            // cbCAPostSignatureRequired
            // 
            this.cbCAPostSignatureRequired.AutoSize = true;
            this.cbCAPostSignatureRequired.Location = new System.Drawing.Point(19, 146);
            this.cbCAPostSignatureRequired.Name = "cbCAPostSignatureRequired";
            this.cbCAPostSignatureRequired.Size = new System.Drawing.Size(117, 17);
            this.cbCAPostSignatureRequired.TabIndex = 5;
            this.cbCAPostSignatureRequired.Text = "Signature Required";
            this.cbCAPostSignatureRequired.UseVisualStyleBackColor = true;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(3, 116);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(103, 13);
            this.label73.TabIndex = 4;
            this.label73.Text = "Service Features";
            // 
            // cbCAPostOffice
            // 
            this.cbCAPostOffice.AutoSize = true;
            this.cbCAPostOffice.Location = new System.Drawing.Point(19, 85);
            this.cbCAPostOffice.Name = "cbCAPostOffice";
            this.cbCAPostOffice.Size = new System.Drawing.Size(126, 17);
            this.cbCAPostOffice.TabIndex = 3;
            this.cbCAPostOffice.Text = "Deliver to Post Office";
            this.cbCAPostOffice.UseVisualStyleBackColor = true;
            // 
            // cbCAPostAge19
            // 
            this.cbCAPostAge19.AutoSize = true;
            this.cbCAPostAge19.Location = new System.Drawing.Point(19, 62);
            this.cbCAPostAge19.Name = "cbCAPostAge19";
            this.cbCAPostAge19.Size = new System.Drawing.Size(134, 17);
            this.cbCAPostAge19.TabIndex = 2;
            this.cbCAPostAge19.Text = "Proof of Age (19 years)";
            this.cbCAPostAge19.UseVisualStyleBackColor = true;
            // 
            // cbCAPostAge18
            // 
            this.cbCAPostAge18.AutoSize = true;
            this.cbCAPostAge18.Location = new System.Drawing.Point(19, 39);
            this.cbCAPostAge18.Name = "cbCAPostAge18";
            this.cbCAPostAge18.Size = new System.Drawing.Size(134, 17);
            this.cbCAPostAge18.TabIndex = 1;
            this.cbCAPostAge18.Text = "Proof of Age (18 years)";
            this.cbCAPostAge18.UseVisualStyleBackColor = true;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(3, 12);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(123, 13);
            this.label72.TabIndex = 0;
            this.label72.Text = "Delivery Instructions";
            // 
            // CanparShipmentOptions
            // 
            this.CanparShipmentOptions.Controls.Add(this.tbCanparCODAmount);
            this.CanparShipmentOptions.Controls.Add(this.labelCanparCODAmount);
            this.CanparShipmentOptions.Controls.Add(this.cbCanparSaturdayDelivery);
            this.CanparShipmentOptions.Controls.Add(this.cbCanparExtraCare);
            this.CanparShipmentOptions.Controls.Add(this.cbCanparCOD);
            this.CanparShipmentOptions.Location = new System.Drawing.Point(4, 22);
            this.CanparShipmentOptions.Name = "CanparShipmentOptions";
            this.CanparShipmentOptions.Size = new System.Drawing.Size(579, 306);
            this.CanparShipmentOptions.TabIndex = 6;
            this.CanparShipmentOptions.Text = "Shipment Options";
            this.CanparShipmentOptions.UseVisualStyleBackColor = true;
            // 
            // tbCanparCODAmount
            // 
            this.tbCanparCODAmount.Location = new System.Drawing.Point(112, 110);
            this.tbCanparCODAmount.Name = "tbCanparCODAmount";
            this.tbCanparCODAmount.Size = new System.Drawing.Size(202, 20);
            this.tbCanparCODAmount.TabIndex = 5;
            // 
            // labelCanparCODAmount
            // 
            this.labelCanparCODAmount.AutoSize = true;
            this.labelCanparCODAmount.Location = new System.Drawing.Point(33, 113);
            this.labelCanparCODAmount.Name = "labelCanparCODAmount";
            this.labelCanparCODAmount.Size = new System.Drawing.Size(43, 13);
            this.labelCanparCODAmount.TabIndex = 4;
            this.labelCanparCODAmount.Text = "Amount";
            // 
            // cbCanparSaturdayDelivery
            // 
            this.cbCanparSaturdayDelivery.AutoSize = true;
            this.cbCanparSaturdayDelivery.Location = new System.Drawing.Point(13, 12);
            this.cbCanparSaturdayDelivery.Name = "cbCanparSaturdayDelivery";
            this.cbCanparSaturdayDelivery.Size = new System.Drawing.Size(109, 17);
            this.cbCanparSaturdayDelivery.TabIndex = 3;
            this.cbCanparSaturdayDelivery.Text = "Saturday Delivery";
            this.cbCanparSaturdayDelivery.UseVisualStyleBackColor = true;
            // 
            // cbCanparExtraCare
            // 
            this.cbCanparExtraCare.AutoSize = true;
            this.cbCanparExtraCare.Location = new System.Drawing.Point(13, 45);
            this.cbCanparExtraCare.Name = "cbCanparExtraCare";
            this.cbCanparExtraCare.Size = new System.Drawing.Size(75, 17);
            this.cbCanparExtraCare.TabIndex = 2;
            this.cbCanparExtraCare.Text = "Extra Care";
            this.cbCanparExtraCare.UseVisualStyleBackColor = true;
            // 
            // cbCanparCOD
            // 
            this.cbCanparCOD.AutoSize = true;
            this.cbCanparCOD.Location = new System.Drawing.Point(13, 82);
            this.cbCanparCOD.Name = "cbCanparCOD";
            this.cbCanparCOD.Size = new System.Drawing.Size(114, 17);
            this.cbCanparCOD.TabIndex = 0;
            this.cbCanparCOD.Text = "Collect on Delivery";
            this.cbCanparCOD.UseVisualStyleBackColor = true;
            this.cbCanparCOD.CheckedChanged += new System.EventHandler(this.cbCanparCOD_CheckedChanged);
            // 
            // DicomShipmentOptions
            // 
            this.DicomShipmentOptions.Controls.Add(this.cbDicomSignatureRequired);
            this.DicomShipmentOptions.Controls.Add(this.cbDicomOvernight);
            this.DicomShipmentOptions.Controls.Add(this.cbDicomWeekend);
            this.DicomShipmentOptions.Controls.Add(this.cbDicomDangerousGoods);
            this.DicomShipmentOptions.Location = new System.Drawing.Point(4, 22);
            this.DicomShipmentOptions.Name = "DicomShipmentOptions";
            this.DicomShipmentOptions.Size = new System.Drawing.Size(579, 306);
            this.DicomShipmentOptions.TabIndex = 7;
            this.DicomShipmentOptions.Text = "Shipment Options";
            this.DicomShipmentOptions.UseVisualStyleBackColor = true;
            // 
            // cbDicomSignatureRequired
            // 
            this.cbDicomSignatureRequired.AutoSize = true;
            this.cbDicomSignatureRequired.Location = new System.Drawing.Point(17, 119);
            this.cbDicomSignatureRequired.Name = "cbDicomSignatureRequired";
            this.cbDicomSignatureRequired.Size = new System.Drawing.Size(117, 17);
            this.cbDicomSignatureRequired.TabIndex = 3;
            this.cbDicomSignatureRequired.Text = "Signature Required";
            this.cbDicomSignatureRequired.UseVisualStyleBackColor = true;
            // 
            // cbDicomOvernight
            // 
            this.cbDicomOvernight.AutoSize = true;
            this.cbDicomOvernight.Location = new System.Drawing.Point(17, 83);
            this.cbDicomOvernight.Name = "cbDicomOvernight";
            this.cbDicomOvernight.Size = new System.Drawing.Size(72, 17);
            this.cbDicomOvernight.TabIndex = 2;
            this.cbDicomOvernight.Text = "Overnight";
            this.cbDicomOvernight.UseVisualStyleBackColor = true;
            // 
            // cbDicomWeekend
            // 
            this.cbDicomWeekend.AutoSize = true;
            this.cbDicomWeekend.Location = new System.Drawing.Point(17, 48);
            this.cbDicomWeekend.Name = "cbDicomWeekend";
            this.cbDicomWeekend.Size = new System.Drawing.Size(73, 17);
            this.cbDicomWeekend.TabIndex = 1;
            this.cbDicomWeekend.Text = "Weekend";
            this.cbDicomWeekend.UseVisualStyleBackColor = true;
            // 
            // cbDicomDangerousGoods
            // 
            this.cbDicomDangerousGoods.AutoSize = true;
            this.cbDicomDangerousGoods.Location = new System.Drawing.Point(17, 15);
            this.cbDicomDangerousGoods.Name = "cbDicomDangerousGoods";
            this.cbDicomDangerousGoods.Size = new System.Drawing.Size(112, 17);
            this.cbDicomDangerousGoods.TabIndex = 0;
            this.cbDicomDangerousGoods.Text = "Dangerous Goods";
            this.cbDicomDangerousGoods.UseVisualStyleBackColor = true;
            // 
            // PackageOptions
            // 
            this.PackageOptions.AutoScroll = true;
            this.PackageOptions.Controls.Add(this.btnSavePackageOption);
            this.PackageOptions.Controls.Add(this.cbPackageIndex);
            this.PackageOptions.Controls.Add(this.pUPSPackageOptions);
            this.PackageOptions.Controls.Add(this.pPuroPackageOptions);
            this.PackageOptions.Controls.Add(this.pFedExPackageOptions);
            this.PackageOptions.Controls.Add(this.label44);
            this.PackageOptions.Location = new System.Drawing.Point(4, 22);
            this.PackageOptions.Name = "PackageOptions";
            this.PackageOptions.Padding = new System.Windows.Forms.Padding(3);
            this.PackageOptions.Size = new System.Drawing.Size(579, 306);
            this.PackageOptions.TabIndex = 1;
            this.PackageOptions.Text = "Package Options";
            this.PackageOptions.UseVisualStyleBackColor = true;
            // 
            // btnSavePackageOption
            // 
            this.btnSavePackageOption.Location = new System.Drawing.Point(465, 19);
            this.btnSavePackageOption.Name = "btnSavePackageOption";
            this.btnSavePackageOption.Size = new System.Drawing.Size(75, 23);
            this.btnSavePackageOption.TabIndex = 16;
            this.btnSavePackageOption.Text = "Save";
            this.btnSavePackageOption.UseVisualStyleBackColor = true;
            this.btnSavePackageOption.Click += new System.EventHandler(this.btnSavePackageOption_Click);
            // 
            // cbPackageIndex
            // 
            this.cbPackageIndex.FormattingEnabled = true;
            this.cbPackageIndex.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49"});
            this.cbPackageIndex.Location = new System.Drawing.Point(108, 21);
            this.cbPackageIndex.Name = "cbPackageIndex";
            this.cbPackageIndex.Size = new System.Drawing.Size(87, 21);
            this.cbPackageIndex.TabIndex = 15;
            this.cbPackageIndex.SelectedIndexChanged += new System.EventHandler(this.cbPackageIndex_SelectedIndexChanged);
            // 
            // pUPSPackageOptions
            // 
            this.pUPSPackageOptions.Controls.Add(this.cbUPSPackageAdditionalHandling);
            this.pUPSPackageOptions.Controls.Add(this.cbUPSPackageCOD);
            this.pUPSPackageOptions.Controls.Add(this.pUPSPackageCOD);
            this.pUPSPackageOptions.Controls.Add(this.cbUPSPackageDeliveryConfirmationType);
            this.pUPSPackageOptions.Controls.Add(this.cbUPSPackageDeliveryConfirmation);
            this.pUPSPackageOptions.Controls.Add(this.labelUPSPackageConfirmationType);
            this.pUPSPackageOptions.Location = new System.Drawing.Point(3, 255);
            this.pUPSPackageOptions.Name = "pUPSPackageOptions";
            this.pUPSPackageOptions.Size = new System.Drawing.Size(556, 219);
            this.pUPSPackageOptions.TabIndex = 14;
            // 
            // cbUPSPackageAdditionalHandling
            // 
            this.cbUPSPackageAdditionalHandling.AutoSize = true;
            this.cbUPSPackageAdditionalHandling.Location = new System.Drawing.Point(6, 13);
            this.cbUPSPackageAdditionalHandling.Name = "cbUPSPackageAdditionalHandling";
            this.cbUPSPackageAdditionalHandling.Size = new System.Drawing.Size(117, 17);
            this.cbUPSPackageAdditionalHandling.TabIndex = 9;
            this.cbUPSPackageAdditionalHandling.Text = "Additional Handling";
            this.cbUPSPackageAdditionalHandling.UseVisualStyleBackColor = true;
            // 
            // cbUPSPackageCOD
            // 
            this.cbUPSPackageCOD.AutoSize = true;
            this.cbUPSPackageCOD.Location = new System.Drawing.Point(6, 36);
            this.cbUPSPackageCOD.Name = "cbUPSPackageCOD";
            this.cbUPSPackageCOD.Size = new System.Drawing.Size(114, 17);
            this.cbUPSPackageCOD.TabIndex = 6;
            this.cbUPSPackageCOD.Text = "Collect on Delivery";
            this.cbUPSPackageCOD.UseVisualStyleBackColor = true;
            this.cbUPSPackageCOD.CheckedChanged += new System.EventHandler(this.cbUPSPackageCOD_CheckedChanged);
            // 
            // pUPSPackageCOD
            // 
            this.pUPSPackageCOD.Controls.Add(this.cbUPSCollectionTypePackage);
            this.pUPSPackageCOD.Controls.Add(this.label70);
            this.pUPSPackageCOD.Controls.Add(this.cbUPSPackageCODCurrency);
            this.pUPSPackageCOD.Controls.Add(this.label69);
            this.pUPSPackageCOD.Controls.Add(this.tbUPSPackageCODAmount);
            this.pUPSPackageCOD.Controls.Add(this.label68);
            this.pUPSPackageCOD.Location = new System.Drawing.Point(3, 59);
            this.pUPSPackageCOD.Name = "pUPSPackageCOD";
            this.pUPSPackageCOD.Size = new System.Drawing.Size(534, 102);
            this.pUPSPackageCOD.TabIndex = 7;
            // 
            // cbUPSCollectionTypePackage
            // 
            this.cbUPSCollectionTypePackage.FormattingEnabled = true;
            this.cbUPSCollectionTypePackage.Items.AddRange(new object[] {
            "0 - Check, Cashiers Check or Money Order",
            "8 - Cashiers Check or Money Order"});
            this.cbUPSCollectionTypePackage.Location = new System.Drawing.Point(134, 63);
            this.cbUPSCollectionTypePackage.Name = "cbUPSCollectionTypePackage";
            this.cbUPSCollectionTypePackage.Size = new System.Drawing.Size(306, 21);
            this.cbUPSCollectionTypePackage.TabIndex = 5;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(20, 66);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(80, 13);
            this.label70.TabIndex = 4;
            this.label70.Text = "Collection Type";
            // 
            // cbUPSPackageCODCurrency
            // 
            this.cbUPSPackageCODCurrency.FormattingEnabled = true;
            this.cbUPSPackageCODCurrency.Items.AddRange(new object[] {
            "CAD",
            "USD"});
            this.cbUPSPackageCODCurrency.Location = new System.Drawing.Point(134, 35);
            this.cbUPSPackageCODCurrency.Name = "cbUPSPackageCODCurrency";
            this.cbUPSPackageCODCurrency.Size = new System.Drawing.Size(198, 21);
            this.cbUPSPackageCODCurrency.TabIndex = 3;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(20, 38);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(49, 13);
            this.label69.TabIndex = 2;
            this.label69.Text = "Currency";
            // 
            // tbUPSPackageCODAmount
            // 
            this.tbUPSPackageCODAmount.Location = new System.Drawing.Point(134, 3);
            this.tbUPSPackageCODAmount.Name = "tbUPSPackageCODAmount";
            this.tbUPSPackageCODAmount.Size = new System.Drawing.Size(198, 20);
            this.tbUPSPackageCODAmount.TabIndex = 1;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(20, 6);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(43, 13);
            this.label68.TabIndex = 0;
            this.label68.Text = "Amount";
            // 
            // cbUPSPackageDeliveryConfirmationType
            // 
            this.cbUPSPackageDeliveryConfirmationType.FormattingEnabled = true;
            this.cbUPSPackageDeliveryConfirmationType.Location = new System.Drawing.Point(137, 195);
            this.cbUPSPackageDeliveryConfirmationType.Name = "cbUPSPackageDeliveryConfirmationType";
            this.cbUPSPackageDeliveryConfirmationType.Size = new System.Drawing.Size(211, 21);
            this.cbUPSPackageDeliveryConfirmationType.TabIndex = 11;
            // 
            // cbUPSPackageDeliveryConfirmation
            // 
            this.cbUPSPackageDeliveryConfirmation.AutoSize = true;
            this.cbUPSPackageDeliveryConfirmation.Location = new System.Drawing.Point(6, 167);
            this.cbUPSPackageDeliveryConfirmation.Name = "cbUPSPackageDeliveryConfirmation";
            this.cbUPSPackageDeliveryConfirmation.Size = new System.Drawing.Size(125, 17);
            this.cbUPSPackageDeliveryConfirmation.TabIndex = 8;
            this.cbUPSPackageDeliveryConfirmation.Text = "Delivery Confirmation";
            this.cbUPSPackageDeliveryConfirmation.UseVisualStyleBackColor = true;
            this.cbUPSPackageDeliveryConfirmation.CheckedChanged += new System.EventHandler(this.cbUPSPackageDeliveryConfirmation_CheckedChanged);
            // 
            // labelUPSPackageConfirmationType
            // 
            this.labelUPSPackageConfirmationType.AutoSize = true;
            this.labelUPSPackageConfirmationType.Location = new System.Drawing.Point(23, 198);
            this.labelUPSPackageConfirmationType.Name = "labelUPSPackageConfirmationType";
            this.labelUPSPackageConfirmationType.Size = new System.Drawing.Size(92, 13);
            this.labelUPSPackageConfirmationType.TabIndex = 10;
            this.labelUPSPackageConfirmationType.Text = "Confirmation Type";
            // 
            // pPuroPackageOptions
            // 
            this.pPuroPackageOptions.Controls.Add(this.cbPuroExceptionHandlingPackage);
            this.pPuroPackageOptions.Location = new System.Drawing.Point(10, 202);
            this.pPuroPackageOptions.Name = "pPuroPackageOptions";
            this.pPuroPackageOptions.Size = new System.Drawing.Size(546, 47);
            this.pPuroPackageOptions.TabIndex = 13;
            // 
            // cbPuroExceptionHandlingPackage
            // 
            this.cbPuroExceptionHandlingPackage.AutoSize = true;
            this.cbPuroExceptionHandlingPackage.Location = new System.Drawing.Point(3, 13);
            this.cbPuroExceptionHandlingPackage.Name = "cbPuroExceptionHandlingPackage";
            this.cbPuroExceptionHandlingPackage.Size = new System.Drawing.Size(118, 17);
            this.cbPuroExceptionHandlingPackage.TabIndex = 5;
            this.cbPuroExceptionHandlingPackage.Text = "Exception Handling";
            this.cbPuroExceptionHandlingPackage.UseVisualStyleBackColor = true;
            // 
            // pFedExPackageOptions
            // 
            this.pFedExPackageOptions.Controls.Add(this.cbPackageDryIce);
            this.pFedExPackageOptions.Controls.Add(this.pDryIcePackageLevel);
            this.pFedExPackageOptions.Controls.Add(this.cbPackageNonStandardContainer);
            this.pFedExPackageOptions.Location = new System.Drawing.Point(6, 50);
            this.pFedExPackageOptions.Name = "pFedExPackageOptions";
            this.pFedExPackageOptions.Size = new System.Drawing.Size(550, 149);
            this.pFedExPackageOptions.TabIndex = 12;
            // 
            // cbPackageDryIce
            // 
            this.cbPackageDryIce.AutoSize = true;
            this.cbPackageDryIce.Location = new System.Drawing.Point(3, 8);
            this.cbPackageDryIce.Name = "cbPackageDryIce";
            this.cbPackageDryIce.Size = new System.Drawing.Size(60, 17);
            this.cbPackageDryIce.TabIndex = 2;
            this.cbPackageDryIce.Text = "Dry Ice";
            this.cbPackageDryIce.UseVisualStyleBackColor = true;
            this.cbPackageDryIce.CheckedChanged += new System.EventHandler(this.cbPackageDryIce_CheckedChanged);
            // 
            // pDryIcePackageLevel
            // 
            this.pDryIcePackageLevel.Controls.Add(this.cbPackageDryIceUnit);
            this.pDryIcePackageLevel.Controls.Add(this.tbPackageDryIceWeight);
            this.pDryIcePackageLevel.Controls.Add(this.label45);
            this.pDryIcePackageLevel.Location = new System.Drawing.Point(3, 31);
            this.pDryIcePackageLevel.Name = "pDryIcePackageLevel";
            this.pDryIcePackageLevel.Size = new System.Drawing.Size(563, 89);
            this.pDryIcePackageLevel.TabIndex = 3;
            // 
            // cbPackageDryIceUnit
            // 
            this.cbPackageDryIceUnit.FormattingEnabled = true;
            this.cbPackageDryIceUnit.Items.AddRange(new object[] {
            "KGS",
            "LBS"});
            this.cbPackageDryIceUnit.Location = new System.Drawing.Point(7, 59);
            this.cbPackageDryIceUnit.Name = "cbPackageDryIceUnit";
            this.cbPackageDryIceUnit.Size = new System.Drawing.Size(268, 21);
            this.cbPackageDryIceUnit.TabIndex = 2;
            // 
            // tbPackageDryIceWeight
            // 
            this.tbPackageDryIceWeight.Location = new System.Drawing.Point(7, 32);
            this.tbPackageDryIceWeight.Name = "tbPackageDryIceWeight";
            this.tbPackageDryIceWeight.Size = new System.Drawing.Size(268, 20);
            this.tbPackageDryIceWeight.TabIndex = 1;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(4, 15);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(78, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "Dry Ice weight:";
            // 
            // cbPackageNonStandardContainer
            // 
            this.cbPackageNonStandardContainer.AutoSize = true;
            this.cbPackageNonStandardContainer.Location = new System.Drawing.Point(3, 129);
            this.cbPackageNonStandardContainer.Name = "cbPackageNonStandardContainer";
            this.cbPackageNonStandardContainer.Size = new System.Drawing.Size(137, 17);
            this.cbPackageNonStandardContainer.TabIndex = 4;
            this.cbPackageNonStandardContainer.Text = "Non standard container";
            this.cbPackageNonStandardContainer.UseVisualStyleBackColor = true;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(7, 24);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(79, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "Package Index";
            // 
            // Loomis30ShipOptions
            // 
            this.Loomis30ShipOptions.Controls.Add(this.rtbLInstr);
            this.Loomis30ShipOptions.Controls.Add(this.label63);
            this.Loomis30ShipOptions.Controls.Add(this.cbLSpecialHandling);
            this.Loomis30ShipOptions.Controls.Add(this.cbLNSR);
            this.Loomis30ShipOptions.Controls.Add(this.cbLNonStdPack);
            this.Loomis30ShipOptions.Controls.Add(this.cbLSatDel);
            this.Loomis30ShipOptions.Controls.Add(this.cbLFragile);
            this.Loomis30ShipOptions.Controls.Add(this.cbLDangGoods);
            this.Loomis30ShipOptions.Location = new System.Drawing.Point(4, 22);
            this.Loomis30ShipOptions.Name = "Loomis30ShipOptions";
            this.Loomis30ShipOptions.Size = new System.Drawing.Size(579, 306);
            this.Loomis30ShipOptions.TabIndex = 8;
            this.Loomis30ShipOptions.Text = "Shipment Options";
            this.Loomis30ShipOptions.UseVisualStyleBackColor = true;
            // 
            // rtbLInstr
            // 
            this.rtbLInstr.Location = new System.Drawing.Point(19, 190);
            this.rtbLInstr.Name = "rtbLInstr";
            this.rtbLInstr.Size = new System.Drawing.Size(534, 96);
            this.rtbLInstr.TabIndex = 7;
            this.rtbLInstr.Text = "";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(16, 174);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(61, 13);
            this.label63.TabIndex = 6;
            this.label63.Text = "Instructions";
            // 
            // cbLSpecialHandling
            // 
            this.cbLSpecialHandling.AutoSize = true;
            this.cbLSpecialHandling.Location = new System.Drawing.Point(289, 127);
            this.cbLSpecialHandling.Name = "cbLSpecialHandling";
            this.cbLSpecialHandling.Size = new System.Drawing.Size(106, 17);
            this.cbLSpecialHandling.TabIndex = 5;
            this.cbLSpecialHandling.Text = "Special Handling";
            this.cbLSpecialHandling.UseVisualStyleBackColor = true;
            // 
            // cbLNSR
            // 
            this.cbLNSR.AutoSize = true;
            this.cbLNSR.Location = new System.Drawing.Point(289, 86);
            this.cbLNSR.Name = "cbLNSR";
            this.cbLNSR.Size = new System.Drawing.Size(134, 17);
            this.cbLNSR.TabIndex = 4;
            this.cbLNSR.Text = "No Signature Required";
            this.cbLNSR.UseVisualStyleBackColor = true;
            // 
            // cbLNonStdPack
            // 
            this.cbLNonStdPack.AutoSize = true;
            this.cbLNonStdPack.Location = new System.Drawing.Point(289, 41);
            this.cbLNonStdPack.Name = "cbLNonStdPack";
            this.cbLNonStdPack.Size = new System.Drawing.Size(143, 17);
            this.cbLNonStdPack.TabIndex = 3;
            this.cbLNonStdPack.Text = "Non Standard Packages";
            this.cbLNonStdPack.UseVisualStyleBackColor = true;
            // 
            // cbLSatDel
            // 
            this.cbLSatDel.AutoSize = true;
            this.cbLSatDel.Location = new System.Drawing.Point(16, 127);
            this.cbLSatDel.Name = "cbLSatDel";
            this.cbLSatDel.Size = new System.Drawing.Size(109, 17);
            this.cbLSatDel.TabIndex = 2;
            this.cbLSatDel.Text = "Saturday Delivery";
            this.cbLSatDel.UseVisualStyleBackColor = true;
            // 
            // cbLFragile
            // 
            this.cbLFragile.AutoSize = true;
            this.cbLFragile.Location = new System.Drawing.Point(16, 86);
            this.cbLFragile.Name = "cbLFragile";
            this.cbLFragile.Size = new System.Drawing.Size(57, 17);
            this.cbLFragile.TabIndex = 1;
            this.cbLFragile.Text = "Fragile";
            this.cbLFragile.UseVisualStyleBackColor = true;
            // 
            // cbLDangGoods
            // 
            this.cbLDangGoods.AutoSize = true;
            this.cbLDangGoods.Location = new System.Drawing.Point(16, 41);
            this.cbLDangGoods.Name = "cbLDangGoods";
            this.cbLDangGoods.Size = new System.Drawing.Size(112, 17);
            this.cbLDangGoods.TabIndex = 0;
            this.cbLDangGoods.Text = "Dangerous Goods";
            this.cbLDangGoods.UseVisualStyleBackColor = true;
            // 
            // ICSShipOptions
            // 
            this.ICSShipOptions.Controls.Add(this.tbICSSI);
            this.ICSShipOptions.Controls.Add(this.label64);
            this.ICSShipOptions.Controls.Add(this.cbICSNSR);
            this.ICSShipOptions.Location = new System.Drawing.Point(4, 22);
            this.ICSShipOptions.Name = "ICSShipOptions";
            this.ICSShipOptions.Size = new System.Drawing.Size(579, 306);
            this.ICSShipOptions.TabIndex = 9;
            this.ICSShipOptions.Text = "Shipment Options";
            this.ICSShipOptions.UseVisualStyleBackColor = true;
            // 
            // tbICSSI
            // 
            this.tbICSSI.Location = new System.Drawing.Point(151, 61);
            this.tbICSSI.Name = "tbICSSI";
            this.tbICSSI.Size = new System.Drawing.Size(280, 20);
            this.tbICSSI.TabIndex = 2;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(18, 61);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(93, 13);
            this.label64.TabIndex = 1;
            this.label64.Text = "Special instruction";
            // 
            // cbICSNSR
            // 
            this.cbICSNSR.AutoSize = true;
            this.cbICSNSR.Location = new System.Drawing.Point(18, 27);
            this.cbICSNSR.Name = "cbICSNSR";
            this.cbICSNSR.Size = new System.Drawing.Size(117, 17);
            this.cbICSNSR.TabIndex = 0;
            this.cbICSNSR.Text = "Signature Required";
            this.cbICSNSR.UseVisualStyleBackColor = true;
            // 
            // NationexShipOptions
            // 
            this.NationexShipOptions.Controls.Add(this.cbNationexFP);
            this.NationexShipOptions.Controls.Add(this.cbNationexRR);
            this.NationexShipOptions.Controls.Add(this.cbNationexNC);
            this.NationexShipOptions.Controls.Add(this.cbNationexDG);
            this.NationexShipOptions.Location = new System.Drawing.Point(4, 22);
            this.NationexShipOptions.Name = "NationexShipOptions";
            this.NationexShipOptions.Size = new System.Drawing.Size(579, 306);
            this.NationexShipOptions.TabIndex = 10;
            this.NationexShipOptions.Text = "Shipment Options";
            this.NationexShipOptions.UseVisualStyleBackColor = true;
            // 
            // cbNationexFP
            // 
            this.cbNationexFP.AutoSize = true;
            this.cbNationexFP.Location = new System.Drawing.Point(34, 146);
            this.cbNationexFP.Name = "cbNationexFP";
            this.cbNationexFP.Size = new System.Drawing.Size(100, 17);
            this.cbNationexFP.TabIndex = 3;
            this.cbNationexFP.Text = "Frost Protection";
            this.cbNationexFP.UseVisualStyleBackColor = true;
            // 
            // cbNationexRR
            // 
            this.cbNationexRR.AutoSize = true;
            this.cbNationexRR.Location = new System.Drawing.Point(34, 109);
            this.cbNationexRR.Name = "cbNationexRR";
            this.cbNationexRR.Size = new System.Drawing.Size(82, 17);
            this.cbNationexRR.TabIndex = 2;
            this.cbNationexRR.Text = "Rural Rutes";
            this.cbNationexRR.UseVisualStyleBackColor = true;
            // 
            // cbNationexNC
            // 
            this.cbNationexNC.AutoSize = true;
            this.cbNationexNC.Location = new System.Drawing.Point(34, 73);
            this.cbNationexNC.Name = "cbNationexNC";
            this.cbNationexNC.Size = new System.Drawing.Size(102, 17);
            this.cbNationexNC.TabIndex = 1;
            this.cbNationexNC.Text = "NonConveyable";
            this.cbNationexNC.UseVisualStyleBackColor = true;
            // 
            // cbNationexDG
            // 
            this.cbNationexDG.AutoSize = true;
            this.cbNationexDG.Location = new System.Drawing.Point(34, 38);
            this.cbNationexDG.Name = "cbNationexDG";
            this.cbNationexDG.Size = new System.Drawing.Size(112, 17);
            this.cbNationexDG.TabIndex = 0;
            this.cbNationexDG.Text = "Dangerous Goods";
            this.cbNationexDG.UseVisualStyleBackColor = true;
            // 
            // ATSShipOptions
            // 
            this.ATSShipOptions.Controls.Add(this.dtpATSEndDate);
            this.ATSShipOptions.Controls.Add(this.dtpATSStartDate);
            this.ATSShipOptions.Controls.Add(this.cbATSReleaseOptions);
            this.ATSShipOptions.Controls.Add(this.cbATSTR);
            this.ATSShipOptions.Controls.Add(this.cbATSTemp);
            this.ATSShipOptions.Controls.Add(this.label65);
            this.ATSShipOptions.Controls.Add(this.cbATSTailGate);
            this.ATSShipOptions.Controls.Add(this.cbATSAppt);
            this.ATSShipOptions.Controls.Add(this.cbATSHardcopyPOD);
            this.ATSShipOptions.Controls.Add(this.cbATSDG);
            this.ATSShipOptions.Controls.Add(this.cbATSBackDoor);
            this.ATSShipOptions.Location = new System.Drawing.Point(4, 22);
            this.ATSShipOptions.Name = "ATSShipOptions";
            this.ATSShipOptions.Size = new System.Drawing.Size(579, 306);
            this.ATSShipOptions.TabIndex = 11;
            this.ATSShipOptions.Text = "Shipment Options";
            this.ATSShipOptions.UseVisualStyleBackColor = true;
            // 
            // dtpATSEndDate
            // 
            this.dtpATSEndDate.Location = new System.Drawing.Point(135, 274);
            this.dtpATSEndDate.Name = "dtpATSEndDate";
            this.dtpATSEndDate.Size = new System.Drawing.Size(200, 20);
            this.dtpATSEndDate.TabIndex = 10;
            // 
            // dtpATSStartDate
            // 
            this.dtpATSStartDate.Location = new System.Drawing.Point(135, 248);
            this.dtpATSStartDate.Name = "dtpATSStartDate";
            this.dtpATSStartDate.Size = new System.Drawing.Size(200, 20);
            this.dtpATSStartDate.TabIndex = 9;
            // 
            // cbATSReleaseOptions
            // 
            this.cbATSReleaseOptions.FormattingEnabled = true;
            this.cbATSReleaseOptions.Location = new System.Drawing.Point(132, 221);
            this.cbATSReleaseOptions.Name = "cbATSReleaseOptions";
            this.cbATSReleaseOptions.Size = new System.Drawing.Size(203, 21);
            this.cbATSReleaseOptions.TabIndex = 8;
            // 
            // cbATSTR
            // 
            this.cbATSTR.AutoSize = true;
            this.cbATSTR.Location = new System.Drawing.Point(29, 221);
            this.cbATSTR.Name = "cbATSTR";
            this.cbATSTR.Size = new System.Drawing.Size(91, 17);
            this.cbATSTR.TabIndex = 7;
            this.cbATSTR.Text = "Time Release";
            this.cbATSTR.UseVisualStyleBackColor = true;
            this.cbATSTR.CheckedChanged += new System.EventHandler(this.cbATSTR_CheckedChanged);
            // 
            // cbATSTemp
            // 
            this.cbATSTemp.FormattingEnabled = true;
            this.cbATSTemp.Location = new System.Drawing.Point(132, 188);
            this.cbATSTemp.Name = "cbATSTemp";
            this.cbATSTemp.Size = new System.Drawing.Size(203, 21);
            this.cbATSTemp.TabIndex = 6;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(26, 188);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(67, 13);
            this.label65.TabIndex = 5;
            this.label65.Text = "Temperature";
            // 
            // cbATSTailGate
            // 
            this.cbATSTailGate.AutoSize = true;
            this.cbATSTailGate.Location = new System.Drawing.Point(29, 155);
            this.cbATSTailGate.Name = "cbATSTailGate";
            this.cbATSTailGate.Size = new System.Drawing.Size(112, 17);
            this.cbATSTailGate.TabIndex = 4;
            this.cbATSTailGate.Text = "TailGate Required";
            this.cbATSTailGate.UseVisualStyleBackColor = true;
            // 
            // cbATSAppt
            // 
            this.cbATSAppt.AutoSize = true;
            this.cbATSAppt.Location = new System.Drawing.Point(29, 119);
            this.cbATSAppt.Name = "cbATSAppt";
            this.cbATSAppt.Size = new System.Drawing.Size(214, 17);
            this.cbATSAppt.TabIndex = 3;
            this.cbATSAppt.Text = "Appointment (Pre-Booked and Request)";
            this.cbATSAppt.UseVisualStyleBackColor = true;
            // 
            // cbATSHardcopyPOD
            // 
            this.cbATSHardcopyPOD.AutoSize = true;
            this.cbATSHardcopyPOD.Location = new System.Drawing.Point(29, 84);
            this.cbATSHardcopyPOD.Name = "cbATSHardcopyPOD";
            this.cbATSHardcopyPOD.Size = new System.Drawing.Size(144, 17);
            this.cbATSHardcopyPOD.TabIndex = 2;
            this.cbATSHardcopyPOD.Text = "Hardcopy POD Required";
            this.cbATSHardcopyPOD.UseVisualStyleBackColor = true;
            // 
            // cbATSDG
            // 
            this.cbATSDG.AutoSize = true;
            this.cbATSDG.Location = new System.Drawing.Point(29, 52);
            this.cbATSDG.Name = "cbATSDG";
            this.cbATSDG.Size = new System.Drawing.Size(112, 17);
            this.cbATSDG.TabIndex = 1;
            this.cbATSDG.Text = "Dangerous Goods";
            this.cbATSDG.UseVisualStyleBackColor = true;
            // 
            // cbATSBackDoor
            // 
            this.cbATSBackDoor.AutoSize = true;
            this.cbATSBackDoor.Location = new System.Drawing.Point(29, 15);
            this.cbATSBackDoor.Name = "cbATSBackDoor";
            this.cbATSBackDoor.Size = new System.Drawing.Size(117, 17);
            this.cbATSBackDoor.TabIndex = 0;
            this.cbATSBackDoor.Text = "Back Door Receipt";
            this.cbATSBackDoor.UseVisualStyleBackColor = true;
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.Location = new System.Drawing.Point(528, 384);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 1;
            this.cmdOk.Text = "Ok";
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(447, 384);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 2;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // Options
            // 
            this.AcceptButton = this.cmdOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(619, 418);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdOk);
            this.Controls.Add(this.TabControl);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(635, 456);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(635, 456);
            this.Name = "Options";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Options";
            this.Load += new System.EventHandler(this.Options_Load);
            this.TabControl.ResumeLayout(false);
            this.AutoprintPage.ResumeLayout(false);
            this.AutoprintPage.PerformLayout();
            this.PickUpDaysPage.ResumeLayout(false);
            this.PickUpDaysPage.PerformLayout();
            this.MyCarrier.ResumeLayout(false);
            this.MyCarrier.PerformLayout();
            this.backup.ResumeLayout(false);
            this.backup.PerformLayout();
            this.emails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.PackageDimensions.ResumeLayout(false);
            this.PackageDimensions.PerformLayout();
            this.ShipmentOptions.ResumeLayout(false);
            this.pFedEx.ResumeLayout(false);
            this.tFedExShipOptions.ResumeLayout(false);
            this.FedExShipmentOptions.ResumeLayout(false);
            this.FedExShipmentOptions.PerformLayout();
            this.SignatureOptionPanel.ResumeLayout(false);
            this.SignatureOptionPanel.PerformLayout();
            this.HomeDeliveryPanel.ResumeLayout(false);
            this.HomeDeliveryPanel.PerformLayout();
            this.DryIcePanel.ResumeLayout(false);
            this.DryIcePanel.PerformLayout();
            this.HoldAtLocationPanel.ResumeLayout(false);
            this.HoldAtLocationPanel.PerformLayout();
            this.CODPanel.ResumeLayout(false);
            this.CODPanel.PerformLayout();
            this.PuroShipmentOptions.ResumeLayout(false);
            this.PuroShipmentOptions.PerformLayout();
            this.pPuroDangGoods.ResumeLayout(false);
            this.pPuroDangGoods.PerformLayout();
            this.pExpressCheque.ResumeLayout(false);
            this.pExpressCheque.PerformLayout();
            this.LoomisShipmentOptions.ResumeLayout(false);
            this.LoomisShipmentOptions.PerformLayout();
            this.pLoomisReturnCheck.ResumeLayout(false);
            this.pLoomisReturnCheck.PerformLayout();
            this.UPSShipmentOptions.ResumeLayout(false);
            this.UPSShipmentOptions.PerformLayout();
            this.pUPSDeliveryConfirmation.ResumeLayout(false);
            this.pUPSDeliveryConfirmation.PerformLayout();
            this.CAPostShipmentOptions.ResumeLayout(false);
            this.CAPostShipmentOptions.PerformLayout();
            this.pCAPostCOD.ResumeLayout(false);
            this.pCAPostCOD.PerformLayout();
            this.CanparShipmentOptions.ResumeLayout(false);
            this.CanparShipmentOptions.PerformLayout();
            this.DicomShipmentOptions.ResumeLayout(false);
            this.DicomShipmentOptions.PerformLayout();
            this.PackageOptions.ResumeLayout(false);
            this.PackageOptions.PerformLayout();
            this.pUPSPackageOptions.ResumeLayout(false);
            this.pUPSPackageOptions.PerformLayout();
            this.pUPSPackageCOD.ResumeLayout(false);
            this.pUPSPackageCOD.PerformLayout();
            this.pPuroPackageOptions.ResumeLayout(false);
            this.pPuroPackageOptions.PerformLayout();
            this.pFedExPackageOptions.ResumeLayout(false);
            this.pFedExPackageOptions.PerformLayout();
            this.pDryIcePackageLevel.ResumeLayout(false);
            this.pDryIcePackageLevel.PerformLayout();
            this.Loomis30ShipOptions.ResumeLayout(false);
            this.Loomis30ShipOptions.PerformLayout();
            this.ICSShipOptions.ResumeLayout(false);
            this.ICSShipOptions.PerformLayout();
            this.NationexShipOptions.ResumeLayout(false);
            this.NationexShipOptions.PerformLayout();
            this.ATSShipOptions.ResumeLayout(false);
            this.ATSShipOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage AutoprintPage;
        private System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Label lblLabelsPrinter;
        private System.Windows.Forms.Button btnCIFolder;
        private System.Windows.Forms.TextBox txtCIFolder;
        private System.Windows.Forms.Label lblReportsFolder;
        private System.Windows.Forms.Button btnLabelsFolder;
        private System.Windows.Forms.TextBox txtLabelsFolder;
        private System.Windows.Forms.Label lblLabelsFolder;
        private System.Windows.Forms.ComboBox cbboxCIPrinter;
        private System.Windows.Forms.Label lblReportsPrinter;
        private System.Windows.Forms.ComboBox cbboxLabelsPrinter;
        private System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog;
        private System.Windows.Forms.TabPage PickUpDaysPage;
        private System.Windows.Forms.CheckBox chbTuesday;
        private System.Windows.Forms.CheckBox chbMonday;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbSunday;
        private System.Windows.Forms.CheckBox chbSaturday;
        private System.Windows.Forms.CheckBox chbFriday;
        private System.Windows.Forms.CheckBox chbThursday;
        private System.Windows.Forms.CheckBox chbWednesday;
        private System.Windows.Forms.Button btnContentsLabelFolder;
        private System.Windows.Forms.TextBox txtContentsLabelFolder;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnPackSlipFolder;
        private System.Windows.Forms.TextBox txtPackSlipFolder;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnReturnLabelFolder;
        private System.Windows.Forms.TextBox txtReturnLabelFolder;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnBOLFolder;
        private System.Windows.Forms.TextBox txtBOLFolder;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbboxContentsLabelPrinter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbboxPackSlipPrinter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbboxReturnLabelPrinter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbboxBOLPrinter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chbox_contentsLabel;
        private System.Windows.Forms.CheckBox chbox_packslip;
        private System.Windows.Forms.CheckBox chbox_rlabels;
        private System.Windows.Forms.CheckBox chbox_bol;
        private System.Windows.Forms.CheckBox chbox_CI;
        private System.Windows.Forms.CheckBox chbox_labels;
        private System.Windows.Forms.TabPage MyCarrier;
        private System.Windows.Forms.Button btn_edit_mycarrier;
        private System.Windows.Forms.TabPage backup;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker delete_hour;
        public System.Windows.Forms.TabPage emails;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ListBox listbox_carriers;
        private System.Windows.Forms.TextBox txt_carrier_name;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.ErrorProvider error;
        private System.Windows.Forms.CheckBox checkbox_default;
        private System.Windows.Forms.Label label_carrier_scac_code;
        private System.Windows.Forms.Label label_carrier_name;
        private System.Windows.Forms.TextBox txt_carrier_scac_code;
        private System.Windows.Forms.TabPage ShipmentOptions;
        private System.Windows.Forms.Panel pFedEx;
        private System.Windows.Forms.TabControl tFedExShipOptions;
        private System.Windows.Forms.TabPage FedExShipmentOptions;
        private System.Windows.Forms.CheckBox cbInsidePickup;
        private System.Windows.Forms.CheckBox cbInsideDelivery;
        private System.Windows.Forms.Panel DryIcePanel;
        private System.Windows.Forms.CheckBox cbDryIce;
        private System.Windows.Forms.Panel HoldAtLocationPanel;
        private System.Windows.Forms.CheckBox cbHoldAtLocation;
        private System.Windows.Forms.Panel CODPanel;
        private System.Windows.Forms.CheckBox cbCOD;
        private System.Windows.Forms.TabPage PackageOptions;
        private System.Windows.Forms.Panel HomeDeliveryPanel;
        private System.Windows.Forms.CheckBox cbHomeDelivery;
        private System.Windows.Forms.CheckBox cbSaturdayPickup;
        private System.Windows.Forms.CheckBox cbSaturdayDelivery;
        private System.Windows.Forms.Panel SignatureOptionPanel;
        private System.Windows.Forms.CheckBox cbSignatureOption;
        private System.Windows.Forms.ComboBox cbFedExCollectionType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbFedExCurrency;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbFedExAmount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbReferenceIndicator;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbTranspCharges;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cbCountry;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbReturnAddress2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbReturnAddress1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbReturnCompany;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbReturnContact;
        private System.Windows.Forms.ComboBox cbStateProvince;
        private System.Windows.Forms.TextBox tbCity;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbReturnTel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbZIP;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbHoldCompany;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbHoldContact;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbHoldCity;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cbHoldState;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cbHoldCountry;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbHoldAddress2;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tbHoldAddress1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbHoldZIP;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tbHoldEmail;
        private System.Windows.Forms.TextBox tbHoldPhone;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cbDryIceUnit;
        private System.Windows.Forms.TextBox tbDryIceWeight;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbHomeDeliveryPhone;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.DateTimePicker dtpHomeDeliveryDate;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox cbHomeDeliveryType;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbReleaseNumber;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cbSignatureRequired;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.CheckBox cbPackageDryIce;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Panel pDryIcePackageLevel;
        private System.Windows.Forms.ComboBox cbPackageDryIceUnit;
        private System.Windows.Forms.TextBox tbPackageDryIceWeight;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.CheckBox cbPackageNonStandardContainer;
        private System.Windows.Forms.TabPage PuroShipmentOptions;
        private System.Windows.Forms.CheckBox cbExpressCheque;
        private System.Windows.Forms.CheckBox cbPuroSignatureRequired;
        private System.Windows.Forms.CheckBox cbOSNR;
        private System.Windows.Forms.Panel pExpressCheque;
        private System.Windows.Forms.TabPage LoomisShipmentOptions;
        private System.Windows.Forms.TextBox tbAmount;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox tbDepartment;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox tbCompany;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ComboBox cbMethodOfPayment;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox cbPuroCountry;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.ComboBox cbPuroState;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox tbPuroAddress2;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox tbPuroAddress1;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox tbPuroCity;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox tbPuroPhone;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox tbPuroEmail;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox tbPuroZIP;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.CheckBox cbChainOfSignature;
        private System.Windows.Forms.CheckBox cbPuroExceptionHandling;
        private System.Windows.Forms.CheckBox cbPuroSaturdayPickup;
        private System.Windows.Forms.CheckBox cbPuroSaturdayDelivery;
        private System.Windows.Forms.CheckBox cbHoldForPickUp;
        private System.Windows.Forms.CheckBox cbPuroExceptionHandlingPackage;
        private System.Windows.Forms.CheckBox cbLoomisReturnCheck;
        private System.Windows.Forms.CheckBox cbLoomisFragile;
        private System.Windows.Forms.CheckBox cbLoomisNotPackaged;
        private System.Windows.Forms.Panel pLoomisReturnCheck;
        private System.Windows.Forms.ComboBox cbLoomisMethodOfPayment;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox tbLoomisAmount;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox tbLoomisAdditionalInfo2;
        private System.Windows.Forms.Label labelAdditionalInfo2;
        private System.Windows.Forms.TextBox tbLoomisAdditionalInfo1;
        private System.Windows.Forms.Label labelAdditionalInfo1;
        private System.Windows.Forms.Label labelAdditionalInfo;
        private System.Windows.Forms.CheckBox cbLoomisNoSignatureRequired;
        private System.Windows.Forms.CheckBox cbLoomisSaturdayDelivery;
        private System.Windows.Forms.RichTextBox rtbLoomisInstructions;
        private System.Windows.Forms.Label labelInstructions;
        private System.Windows.Forms.TabPage UPSShipmentOptions;
        private System.Windows.Forms.CheckBox cbUPSLargePackage;
        private System.Windows.Forms.ComboBox cbUPSDeliveryConfirmationType;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.CheckBox cbUPSDeliveryConfirmation;
        private System.Windows.Forms.CheckBox cbUPSSaturdayPickup;
        private System.Windows.Forms.CheckBox cbUPSSaturdayDelivery;
        private System.Windows.Forms.CheckBox cbUPSPackageCOD;
        private System.Windows.Forms.Panel pUPSPackageCOD;
        private System.Windows.Forms.ComboBox cbUPSCollectionTypePackage;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.ComboBox cbUPSPackageCODCurrency;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox tbUPSPackageCODAmount;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.CheckBox cbUPSPackageAdditionalHandling;
        private System.Windows.Forms.CheckBox cbUPSPackageDeliveryConfirmation;
        private System.Windows.Forms.ComboBox cbUPSPackageDeliveryConfirmationType;
        private System.Windows.Forms.Label labelUPSPackageConfirmationType;
        private System.Windows.Forms.TabPage CAPostShipmentOptions;
        private System.Windows.Forms.Panel pCAPostCOD;
        private System.Windows.Forms.CheckBox cbCAPostCOD;
        private System.Windows.Forms.CheckBox cbCAPostUnpackaged;
        private System.Windows.Forms.CheckBox cbCAPostDeliveryConfirmation;
        private System.Windows.Forms.CheckBox cbCAPostSignatureRequired;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.CheckBox cbCAPostOffice;
        private System.Windows.Forms.CheckBox cbCAPostAge19;
        private System.Windows.Forms.CheckBox cbCAPostAge18;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox tbCAPostCODAddress1;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox tbCAPostCODCity;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.ComboBox cbCAPostCODState;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox tbCAPostCODCompany;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox tbCAPostCODName;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox cbCAPostMethodOfPayment;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox tbCAPostAmount;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox tbCAPostCODZIP;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox tbCAPostCODAddress2;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox tbCAPostCODEmail;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox tbCAPostCODPhone;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.CheckBox cbCAPostLeaveAtDoor;
        private System.Windows.Forms.CheckBox cbCAPostDoNotSafeDrop;
        private System.Windows.Forms.CheckBox cbCaPostCardForPickup;
        private System.Windows.Forms.Label labelCAPostNonDeliveryHandling;
        private System.Windows.Forms.TabPage CanparShipmentOptions;
        private System.Windows.Forms.TextBox tbCanparCODAmount;
        private System.Windows.Forms.Label labelCanparCODAmount;
        private System.Windows.Forms.CheckBox cbCanparSaturdayDelivery;
        private System.Windows.Forms.CheckBox cbCanparExtraCare;
        private System.Windows.Forms.CheckBox cbCanparCOD;
        private System.Windows.Forms.TabPage DicomShipmentOptions;
        private System.Windows.Forms.CheckBox cbDicomSignatureRequired;
        private System.Windows.Forms.CheckBox cbDicomOvernight;
        private System.Windows.Forms.CheckBox cbDicomWeekend;
        private System.Windows.Forms.CheckBox cbDicomDangerousGoods;
        private System.Windows.Forms.TabPage PackageDimensions;
        private System.Windows.Forms.TextBox tbPackageHeight;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox tbPackageWidth;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox tbPackageLength;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox tbPackageDimName;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox tbPackageDimCode;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.ComboBox cbpackageDimensions;
        private System.Windows.Forms.Button btnSavePackageDims;
        private System.Windows.Forms.Button btnAddPackageDim;
        private System.Windows.Forms.Panel pUPSDeliveryConfirmation;
        private System.Windows.Forms.Panel pFedExPackageOptions;
        private System.Windows.Forms.Panel pPuroPackageOptions;
        private System.Windows.Forms.Panel pUPSPackageOptions;
        private System.Windows.Forms.ComboBox cbPackageIndex;
        private System.Windows.Forms.Button btnSavePackageOption;
        private System.Windows.Forms.ComboBox cbCAPostCODCountry;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.CheckBox cbPuroDangGoods;
        private System.Windows.Forms.Panel pPuroDangGoods;
        private System.Windows.Forms.ComboBox cbPuroDGClass;
        private System.Windows.Forms.Label lPuroDangGoodClass;
        private System.Windows.Forms.ComboBox cbPuroDGMode;
        private System.Windows.Forms.Label lPuroDangGoods;
        private System.Windows.Forms.TabPage Loomis30ShipOptions;
        private System.Windows.Forms.RichTextBox rtbLInstr;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.CheckBox cbLSpecialHandling;
        private System.Windows.Forms.CheckBox cbLNSR;
        private System.Windows.Forms.CheckBox cbLNonStdPack;
        private System.Windows.Forms.CheckBox cbLSatDel;
        private System.Windows.Forms.CheckBox cbLFragile;
        private System.Windows.Forms.CheckBox cbLDangGoods;
        private System.Windows.Forms.TabPage ICSShipOptions;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.CheckBox cbICSNSR;
        private System.Windows.Forms.TabPage NationexShipOptions;
        private System.Windows.Forms.TabPage ATSShipOptions;
        private System.Windows.Forms.TextBox tbICSSI;
        private System.Windows.Forms.CheckBox cbNationexFP;
        private System.Windows.Forms.CheckBox cbNationexRR;
        private System.Windows.Forms.CheckBox cbNationexNC;
        private System.Windows.Forms.CheckBox cbNationexDG;
        private System.Windows.Forms.DateTimePicker dtpATSEndDate;
        private System.Windows.Forms.DateTimePicker dtpATSStartDate;
        private System.Windows.Forms.ComboBox cbATSReleaseOptions;
        private System.Windows.Forms.CheckBox cbATSTR;
        private System.Windows.Forms.ComboBox cbATSTemp;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.CheckBox cbATSTailGate;
        private System.Windows.Forms.CheckBox cbATSAppt;
        private System.Windows.Forms.CheckBox cbATSHardcopyPOD;
        private System.Windows.Forms.CheckBox cbATSDG;
        private System.Windows.Forms.CheckBox cbATSBackDoor;
    }
}