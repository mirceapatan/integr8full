﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class CloseCarriers : Form
    {
        public List<string> carriers = new List<string>();
        public CloseCarriers(List<string> temp_carriers)
        {
            InitializeComponent();
            carriers.AddRange(temp_carriers);
        }

        private void CloseCarriers_Load(object sender, EventArgs e)
        {
            foreach (string i in carriers)
            {
                dgv_carriers.Rows.Add(InteGr8_Main.data.Carriers.FindByCode(i).Name, i);
            }
            carriers.Clear();
            dgv_carriers.ClearSelection();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dgv_carriers.SelectedRows)
            {
                carriers.Add(r.Cells["CarrierCode"].Value.ToString());
            }
        }
    }
}
