﻿namespace InteGr8
{
    partial class frmWizard_Filter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard_Filter));
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.chkEdit = new System.Windows.Forms.CheckBox();
            this.lblManualEdit = new System.Windows.Forms.Label();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.btnRemoveExpression = new System.Windows.Forms.Button();
            this.btnAddExpression = new System.Windows.Forms.Button();
            this.dgvFilter = new System.Windows.Forms.DataGridView();
            this.Left_Operand_Column = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Operator_Column = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Right_Operand_1_Column = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Right_Operand_2_Column = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Join_Type_Column = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.filterExpressionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pnlHeader.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterExpressionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(564, 402);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 118;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPrev.Location = new System.Drawing.Point(483, 402);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 120;
            this.btnPrev.Text = "Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnTest
            // 
            this.btnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTest.Location = new System.Drawing.Point(118, 402);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(100, 23);
            this.btnTest.TabIndex = 121;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.label1);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.label2);
            this.pnlHeader.Controls.Add(this.label3);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(651, 90);
            this.pnlHeader.TabIndex = 122;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(0, 88);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(651, 2);
            this.label1.TabIndex = 116;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(651, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Filter Setup";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.BackColor = System.Drawing.SystemColors.Window;
            this.label2.Image = global::InteGr8.Properties.Resources.data_next;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label2.Location = new System.Drawing.Point(0, 24);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(64, 64);
            this.label2.TabIndex = 115;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoEllipsis = true;
            this.label3.Location = new System.Drawing.Point(68, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.label3.Size = new System.Drawing.Size(583, 64);
            this.label3.TabIndex = 114;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // txtFilter
            // 
            this.txtFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilter.BackColor = System.Drawing.Color.White;
            this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFilter.Location = new System.Drawing.Point(12, 339);
            this.txtFilter.Multiline = true;
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.ReadOnly = true;
            this.txtFilter.Size = new System.Drawing.Size(627, 57);
            this.txtFilter.TabIndex = 124;
            // 
            // chkEdit
            // 
            this.chkEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkEdit.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEdit.Location = new System.Drawing.Point(12, 402);
            this.chkEdit.Name = "chkEdit";
            this.chkEdit.Size = new System.Drawing.Size(100, 24);
            this.chkEdit.TabIndex = 127;
            this.chkEdit.Text = "Manual Edit";
            this.chkEdit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEdit.UseVisualStyleBackColor = true;
            this.chkEdit.CheckedChanged += new System.EventHandler(this.chkEdit_CheckedChanged);
            // 
            // lblManualEdit
            // 
            this.lblManualEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblManualEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblManualEdit.Location = new System.Drawing.Point(12, 94);
            this.lblManualEdit.Name = "lblManualEdit";
            this.lblManualEdit.Size = new System.Drawing.Size(627, 59);
            this.lblManualEdit.TabIndex = 128;
            this.lblManualEdit.Text = resources.GetString("lblManualEdit.Text");
            // 
            // pnlGrid
            // 
            this.pnlGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlGrid.Controls.Add(this.btnRemoveExpression);
            this.pnlGrid.Controls.Add(this.btnAddExpression);
            this.pnlGrid.Controls.Add(this.dgvFilter);
            this.pnlGrid.Location = new System.Drawing.Point(12, 157);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(627, 176);
            this.pnlGrid.TabIndex = 129;
            // 
            // btnRemoveExpression
            // 
            this.btnRemoveExpression.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemoveExpression.Location = new System.Drawing.Point(81, 150);
            this.btnRemoveExpression.Name = "btnRemoveExpression";
            this.btnRemoveExpression.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveExpression.TabIndex = 126;
            this.btnRemoveExpression.Text = "Delete";
            this.btnRemoveExpression.UseVisualStyleBackColor = true;
            this.btnRemoveExpression.Click += new System.EventHandler(this.btnRemoveExpression_Click);
            // 
            // btnAddExpression
            // 
            this.btnAddExpression.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddExpression.Location = new System.Drawing.Point(0, 150);
            this.btnAddExpression.Name = "btnAddExpression";
            this.btnAddExpression.Size = new System.Drawing.Size(75, 23);
            this.btnAddExpression.TabIndex = 125;
            this.btnAddExpression.Text = "Add";
            this.btnAddExpression.UseVisualStyleBackColor = true;
            this.btnAddExpression.Click += new System.EventHandler(this.btnAddExpression_Click);
            // 
            // dgvFilter
            // 
            this.dgvFilter.AllowUserToAddRows = false;
            this.dgvFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFilter.AutoGenerateColumns = false;
            this.dgvFilter.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFilter.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFilter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Left_Operand_Column,
            this.Operator_Column,
            this.Right_Operand_1_Column,
            this.Right_Operand_2_Column,
            this.Join_Type_Column});
            this.dgvFilter.DataSource = this.filterExpressionBindingSource;
            this.dgvFilter.Location = new System.Drawing.Point(-3, 15);
            this.dgvFilter.MultiSelect = false;
            this.dgvFilter.Name = "dgvFilter";
            this.dgvFilter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFilter.Size = new System.Drawing.Size(627, 145);
            this.dgvFilter.TabIndex = 124;
            this.dgvFilter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilter_CellEndEdit);
            this.dgvFilter.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilter_CellEndEdit);
            this.dgvFilter.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvFilter_CellValidating);
            this.dgvFilter.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvFilter_EditingControlShowing);
            // 
            // Left_Operand_Column
            // 
            this.Left_Operand_Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Left_Operand_Column.DataPropertyName = "Left_Operand";
            this.Left_Operand_Column.DisplayStyleForCurrentCellOnly = true;
            this.Left_Operand_Column.FillWeight = 33F;
            this.Left_Operand_Column.HeaderText = "Table Column";
            this.Left_Operand_Column.Name = "Left_Operand_Column";
            this.Left_Operand_Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Left_Operand_Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Operator_Column
            // 
            this.Operator_Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Operator_Column.DataPropertyName = "Operator";
            this.Operator_Column.DisplayStyleForCurrentCellOnly = true;
            this.Operator_Column.HeaderText = "Operator";
            this.Operator_Column.Name = "Operator_Column";
            this.Operator_Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Operator_Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Operator_Column.Width = 73;
            // 
            // Right_Operand_1_Column
            // 
            this.Right_Operand_1_Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Right_Operand_1_Column.DataPropertyName = "Right_Operand_1_No_Quotes";
            this.Right_Operand_1_Column.DisplayStyleForCurrentCellOnly = true;
            this.Right_Operand_1_Column.FillWeight = 33F;
            this.Right_Operand_1_Column.HeaderText = "Operand 1";
            this.Right_Operand_1_Column.Name = "Right_Operand_1_Column";
            this.Right_Operand_1_Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Right_Operand_1_Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Right_Operand_2_Column
            // 
            this.Right_Operand_2_Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Right_Operand_2_Column.DataPropertyName = "Right_Operand_2_No_Quotes";
            this.Right_Operand_2_Column.DisplayStyleForCurrentCellOnly = true;
            this.Right_Operand_2_Column.FillWeight = 33F;
            this.Right_Operand_2_Column.HeaderText = "Operand 2";
            this.Right_Operand_2_Column.Name = "Right_Operand_2_Column";
            this.Right_Operand_2_Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Right_Operand_2_Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Join_Type_Column
            // 
            this.Join_Type_Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Join_Type_Column.DataPropertyName = "Join_Type";
            this.Join_Type_Column.DisplayStyleForCurrentCellOnly = true;
            this.Join_Type_Column.HeaderText = "Join Type";
            this.Join_Type_Column.Name = "Join_Type_Column";
            this.Join_Type_Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Join_Type_Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Join_Type_Column.Width = 78;
            // 
            // filterExpressionBindingSource
            // 
            this.filterExpressionBindingSource.AllowNew = true;
            this.filterExpressionBindingSource.DataSource = typeof(InteGr8.Filter_Expression);
            // 
            // frmWizard_Filter
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(651, 437);
            this.Controls.Add(this.pnlGrid);
            this.Controls.Add(this.lblManualEdit);
            this.Controls.Add(this.chkEdit);
            this.Controls.Add(this.txtFilter);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnNext);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWizard_Filter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 10";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard_Filter_HelpButtonClicked);
            this.pnlHeader.ResumeLayout(false);
            this.pnlGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterExpressionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.CheckBox chkEdit;
        private System.Windows.Forms.Label lblManualEdit;
        private System.Windows.Forms.BindingSource filterExpressionBindingSource;
        private System.Windows.Forms.Panel pnlGrid;
        private System.Windows.Forms.DataGridView dgvFilter;
        private System.Windows.Forms.DataGridViewComboBoxColumn Left_Operand_Column;
        private System.Windows.Forms.DataGridViewComboBoxColumn Operator_Column;
        private System.Windows.Forms.DataGridViewComboBoxColumn Right_Operand_1_Column;
        private System.Windows.Forms.DataGridViewComboBoxColumn Right_Operand_2_Column;
        private System.Windows.Forms.DataGridViewComboBoxColumn Join_Type_Column;
        private System.Windows.Forms.Button btnAddExpression;
        private System.Windows.Forms.Button btnRemoveExpression;
    }
}