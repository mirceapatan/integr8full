using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Web.Services;
using InteGr8.WS2Ship;

namespace InteGr8
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            int client;
            if (!Int32.TryParse(txtClientID.Text, out client))
            {
                MessageBox.Show("Client id has to be numeric", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            try
            {
                //InteGr8_Main.token = new Token(client, txtUser.Text, txtPass.Text);
                InteGr8_Main.key = new WSKey(client, txtUser.Text, txtPass.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
               return;
            }

            Hide();
            try
            {
                frmMain f = new frmMain();
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An internal error has occured - an email will be sent to 2Ship support.", "InteGr8 Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Utils.sent = false;
                Utils.SendError("InteGr8 error", ex);
                while (!Utils.sent)
                {
                    System.Threading.Thread.Sleep(500);
                }
                Application.Exit();
            }
            Application.Exit();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}