﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class ProductsScan : Form
    {
        private Data data = new Data();
        private Data.OrdersTableRow order;
        private Data.OrderProductsTableRow[] productsList;
        private package currentPackage;
        private string current_item = "";
        private decimal scannedItemsCount = 0;
        private decimal currentItemsTotal = 0;
        public delegate void row_state();
        public row_state refresh_row = null;
        DataTable serial_num = new DataTable();
        bool serial_scanned = false;
        BindingSource bs_serial = new BindingSource();
        List<package> packages = new List<package>();
        bool bypass = false;

        public ProductsScan(string ordID, BindingSource productsbindingsource)
        {
            InitializeComponent();
            if (InteGr8_Main.data == null || String.IsNullOrEmpty(ordID)) throw new ArgumentException();
            data = InteGr8_Main.data;
            bs_serial.DataSource = data.ProductsSerialNumbers;
            dgw_serial.DataSource = bs_serial;
            bs_serial.Filter = "[Order #] = '" + ordID + "'";
            serial_num = data.Tables["Serial_Nr"];
            this.order = data.OrdersTable.FindBy_Order__(ordID);
            if (this.order == null) throw new ArgumentException();
            this.productsList = order.GetOrderProductsTableRows();
            if (this.productsList == null) throw new ArgumentException();
            foreach (Data.OrderPackagesTableRow r in order.GetOrderPackagesTableRows())
            {
                package temp = new package();
                temp.pack_nr = r._Package__;
                Data.ProductsInPackagesRow[] productsinpackges = data.ProductsInPackages.Select("[Order #] = '" + order._Order__ + "' AND [Package #] = '" + r._Package__ + "'") as Data.ProductsInPackagesRow[];
                foreach (Data.ProductsInPackagesRow t in productsinpackges)
                {
                    int qty = Convert.ToInt32(t.Product_Quantity);
                    for (int i = 0; i < qty; i++)
                    {
                        temp.products.Add(t._Product__);
                    }
                }
                packages.Add(temp);
            }
            currentPackage = packages[packages.Count - 1];
            txtSerialNum.Enabled = false;
            lb_carton_nr.Text = currentPackage.pack_nr;
            if (packages.Count <= 1)
            {
                btn_prev_pakage.Enabled = false;
            }
            dataGrid_products.DataSource = productsbindingsource;
        }

        bool scanned = true;

        private void txtScanCode_TextChanged(object sender, EventArgs e)
        {
            if (txtScanCode.Text.Length != 0)
            {
                if (txtScanCode.Text.Length > 1 && scanned)
                {
                    ScanNewItemCode();
                }
                else
                {
                    scanned = false;
                }
            }
            else
            {
                scanned = true;
            }
        }

        private void ScanNewItemCode()
        {
            labelWarningMessage.ForeColor = Color.Red;
            labelWarningMessage.Text = "";
            if (!String.IsNullOrEmpty(txtScanCode.Text))
            {
                current_item = txtScanCode.Text.TrimEnd();
                bool ok1 = true;

                foreach (Data.OrderProductsTableRow p in productsList)
                {
                    if (p["Scan_Id"].ToString().TrimEnd().Equals(current_item))
                    {
                        currentItemsTotal = Convert.ToDecimal(p["AllocatedQuantity"]);
                        scannedItemsCount = Convert.ToDecimal(p["Scanned Items"]) + 1;
                        if (scannedItemsCount > currentItemsTotal)
                        {
                            //message allert
                            System.Media.SystemSounds.Hand.Play();
                            labelWarningMessage.Text = "All items scanned!";
                            ok1 = false;
                            continue;
                        }

                        //serial number check
                        if ((p["Serial_Lot"].ToString().TrimEnd() == "1" || p["Serial_Lot"].ToString().TrimEnd() == "3") && !serial_scanned)
                        {
                            labelWarningMessage.Text = "This product requires a serial number !";
                            txtScanCode.Enabled = false;
                            btnNewPackage.Enabled = false;
                            txtSerialNum.Enabled = true;
                            scannedItemsCount--;
                            txtSerialNum.Focus();
                            return;
                        }
                        else
                        {
                            if (!serial_scanned)
                            {
                                if (bypass)
                                {
                                    if (radioButton_bypassfull_row.Checked)
                                    {
                                        Data.ProductsSerialNumbersRow[] rows = data.ProductsSerialNumbers.Select("[Serial_Id] = " + p["Serial_Id"].ToString().TrimEnd()) as Data.ProductsSerialNumbersRow[];
                                        int count = rows.Length;
                                        while (count < Convert.ToInt32(p["AllocatedQuantity"]))
                                        {
                                            data.ProductsSerialNumbers.AddProductsSerialNumbersRow(p._Product__.TrimEnd(), p._Order__.TrimEnd(), "", p["ItemNumber"].ToString().TrimEnd(), lb_carton_nr.Text, p["Serial_Id"].ToString().TrimEnd());
                                            count++;
                                        }
                                    }
                                    else
                                    {
                                        data.ProductsSerialNumbers.AddProductsSerialNumbersRow(p._Product__.TrimEnd(), p._Order__.TrimEnd(), "", p["ItemNumber"].ToString().TrimEnd(), lb_carton_nr.Text, p["Serial_Id"].ToString().TrimEnd());
                                    }
                                }
                                else
                                {
                                    data.ProductsSerialNumbers.AddProductsSerialNumbersRow(p._Product__.TrimEnd(), p._Order__.TrimEnd(), "", p["ItemNumber"].ToString().TrimEnd(), lb_carton_nr.Text, p["Serial_Id"].ToString().TrimEnd());
                                }
                            }
                        }

                        if (radioButton_bypassfull_row.Checked && bypass)
                        {
                            p["Scanned Items"] = Convert.ToDouble(p["AllocatedQuantity"]);
                            scannedItemsCount = Convert.ToDecimal(p["AllocatedQuantity"]);
                            p["BackOrder"] = 0;
                            p["Product Quantity"] = 0;
                            labelWarningMessage.ForeColor = Color.Green;
                            labelWarningMessage.Text = "Items Scanned";
                            int count = currentPackage.products.Select(pp => pp == p._Product__).Count();
                            while (count < Convert.ToInt32(p["AllocatedQuantity"]))
                            {
                                currentPackage.products.Add(p._Product__);
                                count++;
                            }
                        }
                        else
                        {
                            double temp = 0;
                            p["Scanned Items"] = scannedItemsCount;
                            temp = Convert.ToDouble(p["AllocatedQuantity"]) - Convert.ToDouble(p["Scanned Items"]);
                            p["BackOrder"] = temp;
                            p["Product Quantity"] = temp;
                            labelWarningMessage.ForeColor = Color.Green;
                            labelWarningMessage.Text = "Item Scanned";
                            currentPackage.products.Add(p._Product__);
                        }

                        if (scannedItemsCount == currentItemsTotal)
                        {
                            current_item = "";
                            refresh_row();
                            refresh_products_rows();
                        }
                        Console.Beep();
                        all_complete(true);
                        ok1 = false;
                        break;
                    }
                    if (ok1)
                    {
                        //message allert
                        System.Media.SystemSounds.Hand.Play();
                        labelWarningMessage.Text = "Wrong product scanned !";
                    }
                }
                txtScanCode.Text = "";
                serial_scanned = false;
                bypass = false;
            }
        }

        public bool all_complete(bool message)
        {
            bool ok = false;
            foreach (Data.OrderProductsTableRow p in productsList)
            {
                if (Convert.ToDecimal(p["AllocatedQuantity"].ToString().Trim()) != Convert.ToDecimal(p["Scanned Items"].ToString().Trim()))
                {
                    ok = true;
                    break;
                }
            }
            if (!ok)
            {
                if (message)
                {
                    MessageBox.Show("All products scanned !");
                }
                txtScanCode.Enabled = false;
                btnNewPackage.Enabled = false;
                return true;
            }
            return false;
        }

        private void txtScanCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                labelWarningMessage.Text = "";
                ScanNewItemCode();
            }
        }

        private void btnNewPackage_Click(object sender, EventArgs e)
        {
            data.OrderPackagesTable.AddOrderPackagesTableRow(order, (order.GetOrderPackagesTableRows().Count() + 1).ToString(), 0, "LBS", 0, 0, 0, "I", 0, "CAD", "");
            package temp = new package();
            temp.pack_nr = order.GetOrderPackagesTableRows().Count().ToString();
            packages.Add(temp);

            currentPackage = packages[packages.Count - 1];
            lb_carton_nr.Text = currentPackage.pack_nr;
            btn_prev_pakage.Enabled = true;
            txtScanCode.Focus();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (Data.OrderProductsTableRow p in productsList)
            {
                if (Convert.ToDecimal(p["AllocatedQuantity"].ToString().Trim()) != Convert.ToDecimal(p["Scanned Items"].ToString().Trim()))
                {
                    if (DialogResult.Yes == MessageBox.Show("Not all items have been scanned ! Do you want to close ?", "InteGr8", MessageBoxButtons.YesNo))
                    {
                        break;
                    }
                    else
                    {
                        return;
                    }
                }
            }

            this.Close();
        }

        private void ProductsScan_FormClosing(object sender, FormClosingEventArgs e)
        {
            Data.OrderPackagesTableRow[] temp = data.OrderPackagesTable.Select("[Order #] = '" + order._Order__ + "'") as Data.OrderPackagesTableRow[];
            foreach (Data.OrderPackagesTableRow row in temp)
            {
                foreach (package pack in packages)
                {
                    if (row._Package__ == pack.pack_nr)
                    {
                        decimal total_qty = 0;
                        foreach (Data.OrderProductsTableRow p in productsList)
                        {
                            int qty = 0;
                            foreach (string s in pack.products)
                            {
                                if (s == p._Product__)
                                {
                                    qty++;
                                }
                            }
                            Data.ProductsInPackagesRow[] old = data.ProductsInPackages.Select("[Order #] = '" + order._Order__ + "' AND [Package #] = '" + pack.pack_nr + "' AND [Product #] = '" + p._Product__ + "'") as Data.ProductsInPackagesRow[];
                            if (old != null && old.Count() > 0)
                            {
                                data.ProductsInPackages.Rows.Remove(old[0]);
                            }
                            if (qty > 0)
                            {
                                Data.ProductsInPackagesRow row_temp = data.ProductsInPackages.AddProductsInPackagesRow(pack.pack_nr, p._Product__, qty.ToString(), p.Product_Description, order._Order__);
                            }
                            total_qty += Convert.ToDecimal(p["Product Unit Weight"]) * qty;
                        }
                        if (row["Package Weight"] == null || Convert.ToDecimal(row["Package Weight"].ToString()) == 0)
                        {
                            row["Package Weight"] = total_qty;
                        }
                    }
                }
            }
            data.AcceptChanges();
            data.Dispose();
            refresh_row();
        }


        bool serial_scann = true;
        private void txtSerialNum_TextChanged(object sender, EventArgs e)
        {
            if (txtSerialNum.Text.Length != 0)
            {
                if (txtSerialNum.Text.Length > 1 && serial_scann)
                {
                    ScanNewSerialNumb();
                }
                else
                {
                    serial_scann = false;
                }
            }
            else
            {
                serial_scann = true;
            }
        }

        private void ScanNewSerialNumb()
        {
            if (!rescan_serial)
            {
                if (!String.IsNullOrEmpty(txtSerialNum.Text))
                {
                    labelWarningMessage.Text = "";
                    foreach (Data.OrderProductsTableRow p in productsList)
                    {
                        if (p["Scan_Id"].ToString().Trim().Equals(txtScanCode.Text.TrimEnd()))
                        {
                            foreach (DataGridViewRow r in dgw_serial.Rows)
                            {
                                if (r.Cells["Serial Number"].Value.ToString() == txtSerialNum.Text)
                                {
                                    labelWarningMessage.ForeColor = Color.Red;
                                    labelWarningMessage.Text = "Serial number already Scanned";
                                    System.Media.SystemSounds.Hand.Play();
                                    return;
                                }
                            }
                            Data.ProductsSerialNumbersRow row = data.ProductsSerialNumbers.AddProductsSerialNumbersRow(p._Product__.TrimEnd(), p._Order__.TrimEnd(), txtSerialNum.Text, p["ItemNumber"].ToString().TrimEnd(), lb_carton_nr.Text, p["Serial_Id"].ToString().TrimEnd());
                            Console.Beep();
                            labelWarningMessage.ForeColor = Color.Green;
                            labelWarningMessage.Text = "Serial number Scanned";
                            txtSerialNum.Text = "";
                            txtScanCode.Enabled = true;
                            btnNewPackage.Enabled = true;
                            txtSerialNum.Enabled = false;
                            serial_scanned = true;
                            ScanNewItemCode();
                            txtScanCode.Focus();
                            break;
                        }
                    }
                }
            }
            else
            {
                Console.Beep();
                Data.ProductsSerialNumbersRow[] row = data.ProductsSerialNumbers.Select("[Package #] = '" + dgw_serial.SelectedRows[0].Cells["Package #"].Value + "' AND [item_num] = '" + dgw_serial.SelectedRows[0].Cells["item_num"].Value + "' AND [Serial Number] = '" + dgw_serial.SelectedRows[0].Cells["Serial Number"].Value + "'") as Data.ProductsSerialNumbersRow[];
                dgw_serial.SelectedRows[0].Cells["Serial Number"].Value = txtSerialNum.Text;
                row = data.ProductsSerialNumbers.Select("[Package #] = '" + dgw_serial.SelectedRows[0].Cells["Package #"].Value + "' AND [item_num] = '" + dgw_serial.SelectedRows[0].Cells["item_num"].Value + "' AND [Serial Number] = '" + dgw_serial.SelectedRows[0].Cells["Serial Number"].Value + "'") as Data.ProductsSerialNumbersRow[];
                labelWarningMessage.ForeColor = Color.Green;
                labelWarningMessage.Text = "Serial number Scanned";
                txtSerialNum.Text = "";
                txtScanCode.Enabled = true;
                btnNewPackage.Enabled = true;
                txtSerialNum.Enabled = false;
                rescan_serial = false;
                txtScanCode.Focus();
                all_complete(false);
            }
        }

        private void txtSerialNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                labelWarningMessage.Text = "";
                ScanNewSerialNumb();
            }
        }

        bool rescan_serial = false;
        string rescan_serial_item_num = "";

        private void btn_serial_Click(object sender, EventArgs e)
        {
            if (dgw_serial.Rows.Count > 0)
            {
                if (dgw_serial.SelectedRows.Count > 0)
                {
                    txtScanCode.Enabled = false;
                    txtSerialNum.Enabled = true;
                    rescan_serial_item_num = dgw_serial.SelectedRows[0].Cells[0].Value.ToString().TrimEnd();
                    Data.ProductsSerialNumbersRow[] row = data.ProductsSerialNumbers.Select("item_num = " + dgw_serial.SelectedRows[0].Cells[0].Value.ToString().TrimEnd() + " AND [Serial Number] = " + dgw_serial.SelectedRows[0].Cells[1].Value.ToString().TrimEnd()) as Data.ProductsSerialNumbersRow[];
                    data.ProductsSerialNumbers.Rows.Remove(row[0]);
                }
            }
        }

        private void btn_prev_pakage_Click(object sender, EventArgs e)
        {
            currentPackage = packages[packages.Count - 2];
            lb_carton_nr.Text = currentPackage.pack_nr;
        }

        private void btn_next_pakage_Click(object sender, EventArgs e)
        {
            currentPackage = packages[packages.Count - 1];
            lb_carton_nr.Text = currentPackage.pack_nr;
        }

        private void ProductsScan_Load(object sender, EventArgs e)
        {
            all_complete(true);
            foreach (DataGridViewColumn c in dgw_serial.Columns)
            {
                c.Visible = false;
                if (c.Name == "Package #")
                {
                    c.Visible = true;
                    c.DisplayIndex = 0;
                    c.Width = 48;
                }
                if (c.Name == "item_num")
                {
                    c.Visible = true;
                    c.HeaderText = "Item Number";
                    c.DisplayIndex = 1;
                }
                if (c.Name == "Serial Number")
                {
                    c.Visible = true;
                    c.DisplayIndex = 2;
                }
            }
            refresh_products_rows();
        }

        private void dgw_serial_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if ((txtScanCode.Enabled) || (!txtScanCode.Enabled && !txtSerialNum.Enabled))
            {
                rescan_serial = true;
                txtScanCode.Enabled = false;
                txtSerialNum.Enabled = true;
                btnNewPackage.Enabled = false;
                txtSerialNum.Focus();
            }
        }

        private void btn_bypass_Click(object sender, EventArgs e)
        {
            bypass = true;
            if (dataGrid_products.SelectedRows[0].Cells["Serial_Lot"].Value.ToString().TrimEnd() == "1" || dataGrid_products.SelectedRows[0].Cells["Serial_Lot"].Value.ToString().TrimEnd() == "3")
            {
                radioButton_bypassone_item.Checked = true;
            }
            txtScanCode.Text = dataGrid_products.SelectedRows[0].Cells["Scan_Id"].Value.ToString().TrimEnd();
        }

        private void refresh_products_rows()
        {
            try
            {
                foreach (DataGridViewRow row in dataGrid_products.Rows)
                {
                    if (Convert.ToDecimal(row.Cells["AllocatedQuantity"].Value.ToString().Trim()) == Convert.ToDecimal(row.Cells["Scanned Items"].Value.ToString().Trim()))
                    {
                        row.DefaultCellStyle.BackColor = Color.LimeGreen;
                        row.DefaultCellStyle.SelectionForeColor = Color.Yellow;
                    }
                }
            }
            catch (Exception e)
            {
                //ignore
            }
        }

        private void ProductsScan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.G && e.Control)
            {
                e.SuppressKeyPress = true;
                radioButton_bypassfull_row.Checked = true;
                btn_bypass_Click(null, null);
            }
            if (e.KeyCode == Keys.B && e.Control)
            {
                e.SuppressKeyPress = true;
                radioButton_bypassone_item.Checked = true;
                btn_bypass_Click(null, null);
            }
        }
    }

    public class package
    {
        public string pack_nr = "";
        public List<string> products = new List<string>();
    }
}