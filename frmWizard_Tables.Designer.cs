﻿namespace InteGr8
{
    partial class frmWizard_Tables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard_Tables));
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lblHeaderLine = new System.Windows.Forms.Label();
            this.llAdvanced = new System.Windows.Forms.LinkLabel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.lblHeaderImage = new System.Windows.Forms.Label();
            this.lblHeaderMessage = new System.Windows.Forms.Label();
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.chkShowViews = new System.Windows.Forms.CheckBox();
            this.chkShowTables = new System.Windows.Forms.CheckBox();
            this.lblTables = new System.Windows.Forms.Label();
            this.lbTables = new System.Windows.Forms.ListBox();
            this.lblColumns = new System.Windows.Forms.Label();
            this.lbColumns = new System.Windows.Forms.ListBox();
            this.llIndirectLink = new System.Windows.Forms.LinkLabel();
            this.llDirectLink = new System.Windows.Forms.LinkLabel();
            this.lblFooterLine = new System.Windows.Forms.Label();
            this.pnlHeader.SuspendLayout();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(543, 411);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 6;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnPrev.Location = new System.Drawing.Point(462, 411);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 5;
            this.btnPrev.Text = "< Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(13, 411);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(91, 23);
            this.btnPreview.TabIndex = 4;
            this.btnPreview.Text = "Preview Data";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.Window;
            this.pnlHeader.Controls.Add(this.lblHeaderLine);
            this.pnlHeader.Controls.Add(this.llAdvanced);
            this.pnlHeader.Controls.Add(this.lblHeaderTitle);
            this.pnlHeader.Controls.Add(this.lblHeaderImage);
            this.pnlHeader.Controls.Add(this.lblHeaderMessage);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(629, 90);
            this.pnlHeader.TabIndex = 115;
            // 
            // lblHeaderLine
            // 
            this.lblHeaderLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderLine.BackColor = System.Drawing.SystemColors.Control;
            this.lblHeaderLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeaderLine.Location = new System.Drawing.Point(0, 88);
            this.lblHeaderLine.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderLine.Name = "lblHeaderLine";
            this.lblHeaderLine.Size = new System.Drawing.Size(629, 2);
            this.lblHeaderLine.TabIndex = 116;
            // 
            // llAdvanced
            // 
            this.llAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdvanced.Image = global::InteGr8.Properties.Resources.wrench;
            this.llAdvanced.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llAdvanced.LinkArea = new System.Windows.Forms.LinkArea(6, 13);
            this.llAdvanced.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdvanced.Location = new System.Drawing.Point(525, 0);
            this.llAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.llAdvanced.Name = "llAdvanced";
            this.llAdvanced.Size = new System.Drawing.Size(104, 24);
            this.llAdvanced.TabIndex = 119;
            this.llAdvanced.TabStop = true;
            this.llAdvanced.Text = "      advanced mode";
            this.llAdvanced.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llAdvanced.UseCompatibleTextRendering = true;
            this.llAdvanced.Visible = false;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblHeaderTitle.Size = new System.Drawing.Size(527, 24);
            this.lblHeaderTitle.TabIndex = 118;
            this.lblHeaderTitle.Text = "Database Tables Mapping";
            this.lblHeaderTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeaderImage
            // 
            this.lblHeaderImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeaderImage.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaderImage.Image = global::InteGr8.Properties.Resources.data_table;
            this.lblHeaderImage.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeaderImage.Location = new System.Drawing.Point(0, 24);
            this.lblHeaderImage.Name = "lblHeaderImage";
            this.lblHeaderImage.Padding = new System.Windows.Forms.Padding(5);
            this.lblHeaderImage.Size = new System.Drawing.Size(64, 64);
            this.lblHeaderImage.TabIndex = 115;
            // 
            // lblHeaderMessage
            // 
            this.lblHeaderMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeaderMessage.AutoEllipsis = true;
            this.lblHeaderMessage.Location = new System.Drawing.Point(68, 24);
            this.lblHeaderMessage.Margin = new System.Windows.Forms.Padding(0);
            this.lblHeaderMessage.Name = "lblHeaderMessage";
            this.lblHeaderMessage.Padding = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.lblHeaderMessage.Size = new System.Drawing.Size(561, 64);
            this.lblHeaderMessage.TabIndex = 114;
            this.lblHeaderMessage.Text = "Please select your database tables and their primary key columns to be used by In" +
                "teGr8.";
            // 
            // SplitContainer
            // 
            this.SplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SplitContainer.Location = new System.Drawing.Point(12, 96);
            this.SplitContainer.Name = "SplitContainer";
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.Controls.Add(this.chkShowViews);
            this.SplitContainer.Panel1.Controls.Add(this.chkShowTables);
            this.SplitContainer.Panel1.Controls.Add(this.lblTables);
            this.SplitContainer.Panel1.Controls.Add(this.lbTables);
            this.SplitContainer.Panel1MinSize = 200;
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.Controls.Add(this.lblColumns);
            this.SplitContainer.Panel2.Controls.Add(this.lbColumns);
            this.SplitContainer.Panel2MinSize = 100;
            this.SplitContainer.Size = new System.Drawing.Size(605, 260);
            this.SplitContainer.SplitterDistance = 302;
            this.SplitContainer.TabIndex = 5;
            // 
            // chkShowViews
            // 
            this.chkShowViews.AutoSize = true;
            this.chkShowViews.Checked = true;
            this.chkShowViews.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowViews.Location = new System.Drawing.Point(87, 239);
            this.chkShowViews.Name = "chkShowViews";
            this.chkShowViews.Size = new System.Drawing.Size(83, 17);
            this.chkShowViews.TabIndex = 10;
            this.chkShowViews.Text = "Show views";
            this.chkShowViews.UseVisualStyleBackColor = true;
            this.chkShowViews.CheckedChanged += new System.EventHandler(this.chkShow_CheckedChanged);
            // 
            // chkShowTables
            // 
            this.chkShowTables.AutoSize = true;
            this.chkShowTables.Checked = true;
            this.chkShowTables.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowTables.Location = new System.Drawing.Point(0, 239);
            this.chkShowTables.Name = "chkShowTables";
            this.chkShowTables.Size = new System.Drawing.Size(84, 17);
            this.chkShowTables.TabIndex = 9;
            this.chkShowTables.Text = "Show tables";
            this.chkShowTables.UseVisualStyleBackColor = true;
            this.chkShowTables.CheckedChanged += new System.EventHandler(this.chkShow_CheckedChanged);
            // 
            // lblTables
            // 
            this.lblTables.AutoSize = true;
            this.lblTables.Location = new System.Drawing.Point(-2, 0);
            this.lblTables.Name = "lblTables";
            this.lblTables.Size = new System.Drawing.Size(118, 13);
            this.lblTables.TabIndex = 8;
            this.lblTables.Text = "Select the Orders table:";
            // 
            // lbTables
            // 
            this.lbTables.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTables.FormattingEnabled = true;
            this.lbTables.IntegralHeight = false;
            this.lbTables.Location = new System.Drawing.Point(0, 16);
            this.lbTables.Name = "lbTables";
            this.lbTables.Size = new System.Drawing.Size(299, 220);
            this.lbTables.Sorted = true;
            this.lbTables.TabIndex = 0;
            this.lbTables.SelectedIndexChanged += new System.EventHandler(this.lbTables_SelectedIndexChanged);
            // 
            // lblColumns
            // 
            this.lblColumns.AutoSize = true;
            this.lblColumns.Location = new System.Drawing.Point(-4, 0);
            this.lblColumns.Name = "lblColumns";
            this.lblColumns.Size = new System.Drawing.Size(199, 13);
            this.lblColumns.TabIndex = 123;
            this.lblColumns.Text = "Select the unique Primary Key column(s):";
            // 
            // lbColumns
            // 
            this.lbColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbColumns.FormattingEnabled = true;
            this.lbColumns.IntegralHeight = false;
            this.lbColumns.Location = new System.Drawing.Point(0, 16);
            this.lbColumns.Name = "lbColumns";
            this.lbColumns.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbColumns.Size = new System.Drawing.Size(299, 241);
            this.lbColumns.TabIndex = 1;
            // 
            // llIndirectLink
            // 
            this.llIndirectLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llIndirectLink.AutoSize = true;
            this.llIndirectLink.LinkArea = new System.Windows.Forms.LinkArea(0, 13);
            this.llIndirectLink.Location = new System.Drawing.Point(13, 381);
            this.llIndirectLink.Name = "llIndirectLink";
            this.llIndirectLink.Size = new System.Drawing.Size(322, 17);
            this.llIndirectLink.TabIndex = 3;
            this.llIndirectLink.TabStop = true;
            this.llIndirectLink.Text = "Indirect link with the Orders table through an intermediary table.";
            this.llIndirectLink.UseCompatibleTextRendering = true;
            this.llIndirectLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llIndirectLink_LinkClicked);
            // 
            // llDirectLink
            // 
            this.llDirectLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llDirectLink.AutoSize = true;
            this.llDirectLink.LinkArea = new System.Windows.Forms.LinkArea(0, 11);
            this.llDirectLink.Location = new System.Drawing.Point(12, 359);
            this.llDirectLink.Name = "llDirectLink";
            this.llDirectLink.Size = new System.Drawing.Size(164, 17);
            this.llDirectLink.TabIndex = 2;
            this.llDirectLink.TabStop = true;
            this.llDirectLink.Text = "Direct link with the Orders table.";
            this.llDirectLink.UseCompatibleTextRendering = true;
            this.llDirectLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llDirectLink_LinkClicked);
            // 
            // lblFooterLine
            // 
            this.lblFooterLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFooterLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFooterLine.Location = new System.Drawing.Point(12, 401);
            this.lblFooterLine.Name = "lblFooterLine";
            this.lblFooterLine.Size = new System.Drawing.Size(607, 2);
            this.lblFooterLine.TabIndex = 125;
            // 
            // frmWizard_Tables
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnPrev;
            this.ClientSize = new System.Drawing.Size(629, 446);
            this.Controls.Add(this.lblFooterLine);
            this.Controls.Add(this.llIndirectLink);
            this.Controls.Add(this.llDirectLink);
            this.Controls.Add(this.SplitContainer);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnNext);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(500, 300);
            this.Name = "frmWizard_Tables";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InteGr8 Template Wizard - Page 2";
            this.Load += new System.EventHandler(this.frmWizard02_Load);
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmWizard02_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWizard02_FormClosing);
            this.pnlHeader.ResumeLayout(false);
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel1.PerformLayout();
            this.SplitContainer.Panel2.ResumeLayout(false);
            this.SplitContainer.Panel2.PerformLayout();
            this.SplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lblHeaderLine;
        private System.Windows.Forms.LinkLabel llAdvanced;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.Label lblHeaderImage;
        private System.Windows.Forms.Label lblHeaderMessage;
        private System.Windows.Forms.SplitContainer SplitContainer;
        private System.Windows.Forms.Label lblTables;
        private System.Windows.Forms.ListBox lbTables;
        private System.Windows.Forms.Label lblColumns;
        private System.Windows.Forms.ListBox lbColumns;
        private System.Windows.Forms.LinkLabel llIndirectLink;
        private System.Windows.Forms.LinkLabel llDirectLink;
        private System.Windows.Forms.Label lblFooterLine;
        private System.Windows.Forms.CheckBox chkShowViews;
        private System.Windows.Forms.CheckBox chkShowTables;
    }
}