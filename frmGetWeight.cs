﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InteGr8
{
    public partial class frmGetWeight : Form
    {
        private readonly Data data;
        public string enteredPackageWeight;
        public frmGetWeight()
        {
            InitializeComponent();
            if (InteGr8_Main.data == null) throw new ArgumentException();
            data = InteGr8_Main.data;
        }

        private void btnGetWeightOK_Click(object sender, EventArgs e)
        {
            enteredPackageWeight = tbGetWeight.Text.ToString().Equals("") ? "0" : tbGetWeight.Text.ToString();
        }
    }
}
